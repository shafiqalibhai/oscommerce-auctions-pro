<?php
/*
  $Id: address_book.php,v 1.58 2003/06/09 23:03:52 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

// Modifications by Asymmetrics
//----------------------------------------------------------------------------
// Copyright (c) 2007-2010 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Catalog: Address Book page
------------------------------------------------------------------------------
// I-Metrics Layer
//----------------------------------------------------------------------------
// Modifications:
// - 07/05/2007: PHP5 Register Globals and Long Arrays Off support added
// - 07/08/2007: PHP5 Long Arrays Off support added
// - 07/12/2007: Moved HTML Header/Footer to a common section
// - 08/31/2007: HTML Body Common Sections Added
// - 11/17/2009: Removed table driven layout
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------

*/

  require('includes/application_top.php');

  if (!tep_session_is_registered('customer_id')) {
    $navigation->set_snapshot();
    tep_redirect(tep_href_link(FILENAME_LOGIN, '', 'SSL'));
  }

  require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_ADDRESS_BOOK);

  $breadcrumb->add(NAVBAR_TITLE_1, tep_href_link(FILENAME_ACCOUNT, '', 'SSL'));
  $breadcrumb->add(NAVBAR_TITLE_2, tep_href_link(FILENAME_ADDRESS_BOOK, '', 'SSL'));
?>
<?php require('includes/objects/html_start_sub1.php'); ?>
<?php require('includes/objects/html_start_sub2.php'); ?>
<?php require('includes/objects/html_body_header.php'); ?>
      <div><h2><?php echo PRIMARY_ADDRESS_TITLE; ?></h2></div>
      <div class="bounder">
        <div class="floatend orders_overview"><?php echo tep_address_label($customer_id, $customer_default_address_id, true, ' ', '<br />'); ?></div>
        <?php echo PRIMARY_ADDRESS_DESCRIPTION; ?>
      </div>
      <div><table class="tabledata">
<?php
  $rows = 0;
  $addresses_query = tep_db_query("select address_book_id, entry_firstname as firstname, entry_lastname as lastname, entry_company as company, entry_street_address as street_address, entry_suburb as suburb, entry_city as city, entry_postcode as postcode, entry_state as state, entry_zone_id as zone_id, entry_country_id as country_id from " . TABLE_ADDRESS_BOOK . " where customers_id = '" . (int)$customer_id . "' order by firstname, lastname");
  while ($addresses = tep_db_fetch_array($addresses_query)) {
    $format_id = tep_get_address_format_id($addresses['country_id']);
    $class = $rows%2?'oddrow':'altrow';
?>
        <tr class="<?php echo $class; ?>">
          <td class="heavy"><?php echo tep_output_string_protected($addresses['firstname'] . ' ' . $addresses['lastname']); ?></b><?php if ($addresses['address_book_id'] == $customer_default_address_id) echo '&nbsp;<small><i>' . PRIMARY_ADDRESS . '</i></small>'; ?></td>
          <td><?php echo tep_address_format($format_id, $addresses, true, ' ', '<br />'); ?></td>
          <td class="ralign"><?php echo '<a href="' . tep_href_link(FILENAME_ADDRESS_BOOK_PROCESS, 'ab_id=' . $addresses['address_book_id'], 'SSL') . '" class="tbutton2">' . SMALL_IMAGE_BUTTON_EDIT . '</a> <a href="' . tep_href_link(FILENAME_ADDRESS_BOOK_PROCESS, 'action=delete&ab_id=' . $addresses['address_book_id'], 'SSL') . '" class="tbutton2">' . SMALL_IMAGE_BUTTON_DELETE . '</a>'; ?></td>
        </tr>
<?php
    $rows++;
  }
?>
      </table></div>
      <div class="vspacer"><?php echo sprintf(TEXT_MAXIMUM_ENTRIES, MAX_ADDRESS_BOOK_ENTRIES); ?></div>
      <div class="buttonsRow vpad vspacer">
        <div class="floater lspacer"><?php echo '<a href="' . tep_href_link(FILENAME_ADDRESS_BOOK_PROCESS, '', 'SSL') . '" class="mbutton">' . IMAGE_BUTTON_ADDRESS_BOOK . '</a>'; ?></div>
        <div class="floatend rspacer"><?php echo '<a href="' . tep_href_link(FILENAME_ACCOUNT, '', 'SSL') . '" class="mbutton3">' . IMAGE_BUTTON_ACCOUNT . '</a>'; ?></div>
      </div>
<?php require('includes/objects/html_end.php'); ?>
