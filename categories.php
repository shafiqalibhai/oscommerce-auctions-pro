<?php
/*
  $Id: index.php,v 1.1 2003/06/11 17:37:59 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

//----------------------------------------------------------------------------
// Copyright (c) 2007 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Catalog: Account page
//----------------------------------------------------------------------------
// I-Metrics Layer
//----------------------------------------------------------------------------
// Modifications:
// - 07/05/2007: PHP5 Register Globals off and Long Arrays Off support added
// - 07/12/2007: Moved HTML Header/Footer to a common section
// - 08/31/2007: HTML Body Common Sections Added
// - 03/07/2008: Code Restructured to use common sections
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/

  require('includes/application_top.php');

  if($current_manufacturer_id > 0) {
    //tep_redirect(tep_href_link(FILENAME_SHOP_BY_BRAND, '', 'NONSSL'), '301');
    tep_redirect(tep_href_link('', '', 'NONSSL'), '301');
  }

  $category_depth = 'top';
  if (isset($cPath) && tep_not_null($cPath)) {
    $categories_products_query = tep_db_query("select count(*) as total from " . TABLE_PRODUCTS_TO_CATEGORIES . " where categories_id = '" . (int)$current_category_id . "'");
    $cateqories_products = tep_db_fetch_array($categories_products_query);
    if ($cateqories_products['total'] > 0) {
      $category_depth = 'products'; // display products
    } else {
      $category_parent_query = tep_db_query("select count(*) as total from " . TABLE_CATEGORIES . " where parent_id = '" . (int)$current_category_id . "'");
      $category_parent = tep_db_fetch_array($category_parent_query);
      if ($category_parent['total'] > 0) {
        $category_depth = 'nested'; // navigate through the categories
        if( $current_category_id <= 0 ) {
          tep_redirect(tep_href_link('', '', 'NONSSL', false));
        }
      } else {
        $category_depth = 'products'; // category has no products, but display the 'no products' message
      }
    }
  }

  require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_CATEGORIES);
?>
<?php require('includes/objects/html_start_sub1.php'); ?>
<?php require('includes/objects/html_start_sub2.php'); ?>
<?php 
  $heading_row = true;
  require('includes/objects/html_body_header.php'); 
?>
<!-- body_text //-->
<?php
  if ($category_depth == 'nested') {
    $category_query = tep_db_query("select cd.categories_name, cd.categories_description, c.logo_image from " . TABLE_CATEGORIES . " c, " . TABLE_CATEGORIES_DESCRIPTION . " cd where c.categories_id = '" . (int)$current_category_id . "' and cd.categories_id = '" . (int)$current_category_id . "' and cd.language_id = '" . (int)$languages_id . "'");
    $category = tep_db_fetch_array($category_query);

?>
      <div><h1><?php echo strtoupper($category['categories_name']); ?></h1></div>
<?php
    if( tep_not_null($category['categories_description']) ) {
?>
      <div class="description"><?php echo $category['categories_description'] ?></div>
<?php
    }
?>
<?php require(DIR_WS_MODULES . 'categories_listing.php'); ?>
<?php require(DIR_WS_MODULES . 'new_products.php'); ?>
<?php
  } elseif ($category_depth == 'products') {
//    require(DIR_WS_MODULES . 'best_sellers.php');
    $category_query = tep_db_query("select cd.categories_name, cd.categories_description, c.logo_image from " . TABLE_CATEGORIES . " c, " . TABLE_CATEGORIES_DESCRIPTION . " cd where c.categories_id = '" . (int)$current_category_id . "' and cd.categories_id = '" . (int)$current_category_id . "' and cd.language_id = '" . (int)$languages_id . "'");
    $category = tep_db_fetch_array($category_query);
?>
      <div><h1><?php echo strtoupper($category['categories_name']) ?></h1></div>
<?php
    if( tep_not_null($category['categories_description']) ) {
?>
      <div class="description"><?php echo $category['categories_description']; ?></div>
<?php
    }
    include(DIR_WS_MODULES . FILENAME_PRODUCT_LISTING);
  }
?>
<!-- body_text_eof //-->
<?php require('includes/objects/html_end.php'); ?>
