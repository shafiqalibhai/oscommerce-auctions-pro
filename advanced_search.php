<?php
/*
  $Id: advanced_search.php,v 1.50 2003/06/05 23:25:46 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

// Modifications by Asymmetrics
//----------------------------------------------------------------------------
// Copyright (c) 2006-2010 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
//----------------------------------------------------------------------------
// Catalog: Advanced Search Form
//----------------------------------------------------------------------------
// I-Metrics Layer
//----------------------------------------------------------------------------
// Modifications:
// - 07/05/2007: PHP5 Register Globals and Long Arrays Off support added
// - 07/08/2007: PHP5 Long Arrays Off support added
// - 07/12/2007: Moved HTML Header/Footer to a common section
// - 08/31/2007: HTML Body Common Sections Added
// - 01/11/2008: HTML Cache functions, email templates, rma added
// - 02/17/2008: Removed unused functions, added active countriess
// - 02/27/2008: Support functions for products, categories added
// - 03/05/2008: Parameter validators added
// - 01/09/2009: Added search security control
// - 05/20/2010: Ported stylesheet from I-Metrics CMS
// - 05/20/2010: Removed jscript dependencies
// - 10/07/2010: Added CSS support
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/

  require('includes/application_top.php');

  require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_ADVANCED_SEARCH);

  $breadcrumb->add(NAVBAR_TITLE_1, tep_href_link(FILENAME_ADVANCED_SEARCH));
  $form_name = 'search_submit';
?>
<?php require('includes/objects/html_start_sub1.php'); ?>
<?php require('includes/objects/html_start_sub2.php'); ?>
<?php
  $heading_row = true;
  require('includes/objects/html_body_header.php');
?>
      <div><h1><?php echo HEADING_TITLE_1; ?></h1></div>
      <div><?php echo tep_draw_form($form_name, tep_href_link(FILENAME_ADVANCED_SEARCH_RESULT)); ?><fieldset><legend><?php echo TEXT_INFO_SEARCH_INTRO; ?></legend>
        <div>
          <div class="floater"><label for="search_keywords"><?php echo TEXT_INFO_SEARCH_KEYWORDS; ?></label></div>
          <div class="floatend"><?php echo '<a href="' . tep_href_link(FILENAME_POPUP_SEARCH_HELP) . '" target="_blank">' . TEXT_SEARCH_HELP_LINK . '</a>'; ?></div>
        </div>
        <div class="cleaner"><?php echo tep_draw_input_field('keywords', '', 'id="search_keywords" class="wider"'); ?></div>
        <div class="vpad"></div>
        <div class="floater sixties">
          <div class="vpad"><label for="search_categories" class="quarter floater"><?php echo ENTRY_CATEGORIES; ?></label><?php echo tep_draw_pull_down_menu('categories_id', tep_get_categories(array(array('id' => '', 'text' => TEXT_ALL_CATEGORIES))), '', 'id="search_categories"'); ?></div>
          <div class="vpad"><label for="search_pfrom" class="quarter floater"><?php echo ENTRY_PRICE_FROM; ?></label><?php echo tep_draw_input_field('pfrom', '', 'id="search_pfrom"'); ?></div>
          <div class="vpad"><label for="search_pto" class="quarter floater"><?php echo ENTRY_PRICE_TO; ?></label><?php echo tep_draw_input_field('pto', '', 'id="search_pto"'); ?></div>
          <div class="vpad"><label for="search_dfrom" class="quarter floater"><?php echo ENTRY_DATE_FROM; ?></label><?php echo tep_draw_input_field('dfrom', '', 'id="search_dfrom"') . '&nbsp;' . DOB_FORMAT_STRING; ?></div>
          <div class="vpad"><label for="search_dto" class="quarter floater"><?php echo ENTRY_DATE_FROM; ?></label><?php echo tep_draw_input_field('dfrom', '', 'id="search_dto"') . '&nbsp;' . DOB_FORMAT_STRING; ?></div>
        </div>
        <div class="floater forties">
          <div class="cleaner vpad">
            <div class="floater"><?php echo tep_draw_checkbox_field('inc_subcat', '1', true, 'id="search_subs"'); ?></div>
            <div class="floater"><label for="search_subs"><?php echo ENTRY_INCLUDE_SUBCATEGORIES; ?></label></div>
          </div>
          <div class="cleaner vpad">
            <div class="floater"><?php echo tep_draw_checkbox_field('search_in_description', '1', false, 'id="search_descriptions"'); ?></div>
            <div class="floater"><label for="search_descriptions"><?php echo TEXT_SEARCH_IN_DESCRIPTION; ?></label></div>
          </div>
        </div>
        <div class="buttonsRow vpad tspacer">
          <div class="lspacer"><?php echo tep_image_submit('button_submit.gif', IMAGE_BUTTON_CONTINUE, 'name="' . $form_name . '"'); ?></div>
        </div>
      </fieldset></form></div>
<?php require('includes/objects/html_end.php'); ?>
