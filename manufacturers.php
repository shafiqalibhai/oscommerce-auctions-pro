<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2007 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Catalog: All Manufacturers page
//----------------------------------------------------------------------------
// I-Metrics Layer
//----------------------------------------------------------------------------
// Modifications:
// - 07/05/2007: PHP5 Register Globals and Long Arrays Off support added
// - 07/08/2007: PHP5 Long Arrays Off support added
// - 07/12/2007: Moved HTML Header/Footer to a common section
// - 08/31/2007: HTML Body Common Sections Added
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
  require('includes/application_top.php');

  if( $current_manufacturer_id <= 0 ) {
    tep_redirect(tep_href_link(FILENAME_SHOP_BY_BRAND, '', 'NONSSL'), '301');
  }
  unset($listing_sql_raw);

  $select_string = $g_filters->get_filters_string_query();
  $listing_sql = "select p.products_id, " . $select_string . " p.products_image, p.products_price, p.products_model, p.products_status, p.products_tax_class_id from " . TABLE_PRODUCTS . " p where p.manufacturers_id = '" . (int)$current_manufacturer_id . "' and p.products_display='1' order by p.products_status desc, p.products_ordered desc";
  require(DIR_WS_LANGUAGES . $language . '/' . basename($PHP_SELF));

  $breadcrumb->add(NAVBAR_TITLE, tep_href_link(basename($PHP_SELF), 'manufacturers_id=' . $current_manufacturer_id));
?>
<?php require('includes/objects/html_start_sub1.php'); ?>
<?php require('includes/objects/html_start_sub2.php'); ?>
<?php
  $mfg_query = tep_db_query("select manufacturers_name from " . TABLE_MANUFACTURERS . " where manufacturers_id = '"  . (int)$current_manufacturer_id . "'");
  $mfg_array = tep_db_fetch_array($mfg_query);
  $heading_row = true;
  require('includes/objects/html_body_header.php'); 
  
?>
          <tr>
            <td class="all_borders"><table border="0" width="100%" cellspacing="0" cellpadding="0">
              <tr class="contentBoxContents">
                <td class="pageHeading"><h1><?php echo strtoupper($mfg_array['manufacturers_name'] . ' ' . TEXT_PRODUCTS_POSTFIX); ?></h1></td>
                <td class="pageHeading" align="right">
<?php
      if(defined('HEADING_IMAGE') ) {
        echo tep_image(DIR_WS_IMAGES . HEADING_IMAGE, HEADING_TITLE, HEADING_IMAGE_WIDTH, HEADING_IMAGE_HEIGHT); 
      } else {
        echo '&nbsp;';
      }
?>
                </td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td><?php echo tep_draw_separator('pixel_trans.gif', '100%', '10'); ?></td>
          </tr>

<?php
  $listing_split = new splitPageResults($listing_sql, MAX_DISPLAY_SEARCH_RESULTS, 'p.products_id');
  if ( ($listing_split->number_of_rows > 0) && ( (PREV_NEXT_BAR_LOCATION == '1') || (PREV_NEXT_BAR_LOCATION == '3') ) ) {
?>
    <tr>
      <td class="all_borders"><table border="0" width="100%" cellspacing="0" cellpadding="2">
        <tr>
          <td class="smallText"><?php echo $listing_split->display_count(TEXT_DISPLAY_NUMBER_OF_PRODUCTS); ?></td>
          <td class="smallText" align="right"><?php echo TEXT_RESULT_PAGE . ' ' . $listing_split->display_links(MAX_DISPLAY_PAGE_LINKS); ?></td>
        </tr>
      </table></td>
    </tr>
<?php
  }
  // Initialize vars
  $total_items = array();
  tep_query_to_array($listing_split->sql_query, $total_items);
  $j=count($total_items);

  if( $j > 0 ) {
?>
      <tr>
        <td>
<?php
    require('includes/objects/product_listing_common.php');
?>
        </td>
      </tr>
<?php
  }
  if (($listing_split->number_of_rows > 0) && ((PREV_NEXT_BAR_LOCATION == '2') || (PREV_NEXT_BAR_LOCATION == '3'))) {
?>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="2">
          <tr>
            <td class="smallText"><?php echo $listing_split->display_count(TEXT_DISPLAY_NUMBER_OF_PRODUCTS_NEW); ?></td>
            <td align="right" class="smallText"><?php echo TEXT_RESULT_PAGE . ' ' . $listing_split->display_links(MAX_DISPLAY_PAGE_LINKS); ?></td>
          </tr>
        </table></td>
      </tr>
<?php
  }
?>
<?php require('includes/objects/html_end.php'); ?>
