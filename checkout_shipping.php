<?php
/*
  $Id: checkout_shipping.php,v 1.16 2003/06/09 23:03:53 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

//----------------------------------------------------------------------------
// Copyright (c) 2007 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Catalog: Checkout Shipping page
// I-Metrics Variant
//----------------------------------------------------------------------------
// Modifications:
// - 07/05/2007: PHP5 Register Globals and Long Arrays Off support added
// - 07/08/2007: PHP5 Long Arrays Off support added
// - 07/12/2007: Moved HTML Header/Footer to a common section
// - 08/31/2007: HTML Body Common Sections Added
//----------------------------------------------------------------------------
// Released under the GNU General Public License v3.00
//----------------------------------------------------------------------------
*/
  require('includes/application_top.php');
  require('includes/classes/http_client.php');

// if the customer is not logged on, redirect them to the login page
  if (!tep_session_is_registered('customer_id')) {
    $navigation->set_snapshot();
    tep_redirect(tep_href_link(FILENAME_LOGIN, '', 'SSL'));
  }

// if there is nothing in the customers cart, redirect them to the shopping cart page
  if ($cart->count_contents() < 1) {
    tep_redirect(tep_href_link(FILENAME_SHOPPING_CART));
  }

// if no shipping destination address was selected, use the customers own address as default
  if (!tep_session_is_registered('sendto')) {
    tep_session_register('sendto');
    $sendto = $customer_default_address_id;
  } else {
// verify the selected shipping address
    $check_address_query = tep_db_query("select count(*) as total from " . TABLE_ADDRESS_BOOK . " where customers_id = '" . (int)$customer_id . "' and address_book_id = '" . (int)$sendto . "'");
    $check_address = tep_db_fetch_array($check_address_query);

    if ($check_address['total'] != '1') {
      $sendto = $customer_default_address_id;
      if (tep_session_is_registered('shipping')) tep_session_unregister('shipping');
    }
  }

  $action = (isset($_GET['action']) ? $_GET['action'] : '');

  require(DIR_WS_CLASSES . 'order.php');
  $order = new order;

// register a random ID in the session to check throughout the checkout procedure
// against alterations in the shopping cart contents
  if (!tep_session_is_registered('cartID')) tep_session_register('cartID');
  $cartID = $cart->cartID;

// if the order contains only virtual products, forward the customer to the billing page as
// a shipping address is not needed
  if ($order->content_type == 'virtual') {
    if (!tep_session_is_registered('shipping')) tep_session_register('shipping');
    $shipping = false;
    $sendto = false;
    tep_redirect(tep_href_link(FILENAME_CHECKOUT_PAYMENT, '', 'SSL'));
  }

  $total_weight = $cart->show_weight();
  $total_count = $cart->count_contents();

// load all enabled shipping modules
  require(DIR_WS_CLASSES . 'shipping.php');
  $shipping_modules = new shipping;

  if ( defined('MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING') && (MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING == 'true') ) {
    $pass = false;

    switch (MODULE_ORDER_TOTAL_SHIPPING_DESTINATION) {
      case 'national':
        if ($order->delivery['country_id'] == STORE_COUNTRY) {
          $pass = true;
        }
        break;
      case 'international':
        if ($order->delivery['country_id'] != STORE_COUNTRY) {
          $pass = true;
        }
        break;
      case 'both':
        $pass = true;
        break;
    }

    $free_shipping = false;
    if ( ($pass == true) && ($order->info['total'] >= MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING_OVER) ) {
      $free_shipping = true;

      include(DIR_WS_LANGUAGES . $language . '/modules/order_total/ot_shipping.php');
    }
  } else {
    $free_shipping = false;
  }              

// process the selected shipping method
  switch($action) {
    case 'process':
      if (!tep_session_is_registered('comments')) tep_session_register('comments');
      $comments = isset($_POST['comments'])?tep_db_prepare_input($_POST['comments']):'';

      if (!tep_session_is_registered('shipping')) tep_session_register('shipping');

      if ( (tep_count_shipping_modules() > 0) || ($free_shipping == true) ) {
        if ( (isset($_POST['shipping'])) && (strpos($_POST['shipping'], '_')) ) {
          $shipping = $_POST['shipping'];

          list($module, $method) = explode('_', $shipping);
          if ( is_object($$module) || ($shipping == 'free_free') ) {
            if ($shipping == 'free_free') {
              $quote[0]['methods'][0]['title'] = FREE_SHIPPING_TITLE;
              $quote[0]['methods'][0]['cost'] = '0';
            } else {
              $quote = $shipping_modules->quote($method, $module);
            }
            if (isset($quote['error'])) {
              tep_session_unregister('shipping');
            } else {
              if ( (isset($quote[0]['methods'][0]['title'])) && (isset($quote[0]['methods'][0]['cost'])) ) {
                $shipping = array('id' => $shipping,
                                  'title' => (($free_shipping == true) ?  $quote[0]['methods'][0]['title'] : $quote[0]['module'] . ' (' . $quote[0]['methods'][0]['title'] . ')'),
                                  'cost' => $quote[0]['methods'][0]['cost']);

                tep_redirect(tep_href_link(FILENAME_CHECKOUT_PAYMENT, '', 'SSL'));
              }
            }
          } else {
            tep_session_unregister('shipping');
          }
        }
      } else {
        $shipping = false;
        tep_redirect(tep_href_link(FILENAME_CHECKOUT_PAYMENT, '', 'SSL'));
      }
      break;
    default:
      break;
  }

// get all available shipping quotes
  $quotes = $shipping_modules->quote();

// if no shipping method has been selected, automatically select the cheapest method.
// if the modules status was changed when none were available, to save on implementing
// a javascript force-selection method, also automatically select the cheapest shipping
// method if more than one module is now enabled
  if ( !tep_session_is_registered('shipping') || ( tep_session_is_registered('shipping') && ($shipping == false) && (tep_count_shipping_modules() > 1) ) ) $shipping = $shipping_modules->cheapest();

  require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_CHECKOUT_SHIPPING);

  $breadcrumb->add(NAVBAR_TITLE_1, tep_href_link(FILENAME_CHECKOUT_SHIPPING, '', 'SSL'));
  $breadcrumb->add(NAVBAR_TITLE_2, tep_href_link(FILENAME_CHECKOUT_SHIPPING, '', 'SSL'));
?>
<?php require('includes/objects/html_start_sub1.php'); ?>
<script language="javascript"><!--
var selected;

function selectRowEffect(object, buttonSelect) {
  if (!selected) {
    if (document.getElementById) {
      selected = document.getElementById('defaultSelected');
    } else {
      selected = document.all['defaultSelected'];
    }
  }

  if (selected) selected.className = 'moduleRow';
  object.className = 'moduleRowSelected';
  selected = object;

// one button is not an array
  if (document.checkout_address.shipping[0]) {
    document.checkout_address.shipping[buttonSelect].checked=true;
  } else {
    document.checkout_address.shipping.checked=true;
  }
}

function rowOverEffect(object) {
  if (object.className == 'moduleRow') object.className = 'moduleRowOver';
}

function rowOutEffect(object) {
  if (object.className == 'moduleRowOver') object.className = 'moduleRow';
}
//--></script>
<?php require('includes/objects/html_start_sub2.php'); ?>
<?php
  $heading_row = true;
  require('includes/objects/html_body_header.php');
?>
      <div><h1><?php echo HEADING_TITLE; ?></h1></div>
      <div class="heavy"><?php echo TABLE_HEADING_SHIPPING_ADDRESS; ?></div>

      <div class="bounder">
        <div class="floater hpad">
          <div class="bounder">
            <div class="vpad"><?php echo TEXT_CHOOSE_SHIPPING_DESTINATION; ?></div>
            <div><?php echo '<a href="' . tep_href_link(FILENAME_CHECKOUT_SHIPPING_ADDRESS, '', 'SSL') . '" class="tbutton2">' . IMAGE_BUTTON_CHANGE_ADDRESS . '</a>'; ?></div>
          </div>
        </div>
        <div class="floatend hpad">
          <div class="bounder">
            <div class="heavy floater calign"><?php echo TITLE_SHIPPING_ADDRESS . '<br />' . tep_image(DIR_WS_IMAGES . 'arrow_south_east.gif'); ?></div>
            <div class="floater hpad"><?php echo tep_address_label($customer_id, $sendto, true, ' ', '<br>'); ?></div>
          </div>
        </div>
      </div>
      <div class="bounder tspacer"><?php echo tep_draw_form('checkout_shipping', tep_href_link($g_script,  'action=process', 'SSL'), 'post'); ?><fieldset><legend><?php echo TEXT_INFO_FORM; ?></legend>
<?php
  if (tep_count_shipping_modules() > 0) {
    if (sizeof($quotes) > 1 && sizeof($quotes[0]) > 1) {
?>
        <div class="heavy"><?php echo TEXT_CHOOSE_SHIPPING_METHOD; ?></div>

<?php
    } elseif ($free_shipping == false) {
?>
        <div><?php echo TEXT_ENTER_SHIPPING_INFORMATION; ?></div>
<?php
    }
    if ($free_shipping == true) {
?>
        <div class="heavy"><?php echo FREE_SHIPPING_TITLE; ?></div>

<?php
    } else {
      $radio_buttons = 0;
      for ($i=0, $n=sizeof($quotes); $i<$n; $i++) {
        $icon = (isset($quotes[$i]['icon']) && !empty($quotes[$i]['icon']))?$quotes[$i]['icon']:'';
?>
        <div class="bounder">
          <div class="floater quarter3 heavy"><?php echo $quotes[$i]['module']; ?></div>
          <div class="floatend quarter"><?php echo $icon; ?></div>
        </div>
<?php
        if (isset($quotes[$i]['error'])) {
?>
        <div class="lpad"><?php echo $quotes[$i]['error']; ?></div>
<?php
        } else {
?>
        <div><table class="tabledata">
<?php
         for ($j=0, $n2=sizeof($quotes[$i]['methods']); $j<$n2; $j++) {
            // set the radio button to be checked if it is the method chosen
            $checked = (($quotes[$i]['id'] . '_' . $quotes[$i]['methods'][$j]['id'] == $shipping['id']) ? true : false);

            if ( ($checked == true) || ($n == 1 && $n2 == 1) ) {
              echo '                  <tr id="defaultSelected" class="moduleRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="selectRowEffect(this, ' . $radio_buttons . ')">' . "\n";
            } else {
              echo '                  <tr class="moduleRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="selectRowEffect(this, ' . $radio_buttons . ')">' . "\n";
            }
?>
            <td class="lpad" width="75%"><?php echo $quotes[$i]['methods'][$j]['title']; ?></td>
<?php
            if ( ($n > 1) || ($n2 > 1) ) {
?>
            <td class="ralign"><?php echo $currencies->format(tep_add_tax($quotes[$i]['methods'][$j]['cost'], (isset($quotes[$i]['tax']) ? $quotes[$i]['tax'] : 0))); ?></td>
            <td class="ralign"><?php echo tep_draw_radio_field('shipping', $quotes[$i]['id'] . '_' . $quotes[$i]['methods'][$j]['id'], $checked); ?></td>
<?php
            } else {
?>
            <td class="lpad ralign" colspan="2"><?php echo $currencies->format(tep_add_tax($quotes[$i]['methods'][$j]['cost'], (isset($quotes[$i]['tax']) ? $quotes[$i]['tax'] : 0))) . tep_draw_hidden_field('shipping', $quotes[$i]['id'] . '_' . $quotes[$i]['methods'][$j]['id']); ?></td>
<?php
            }
?>
          </tr>
<?php
            $radio_buttons++;
          }
        }
?>
        </table></div>
<?php
      }
    }
?>

<?php
  }
?>
        <div class="heavy vspacer"><?php echo TABLE_HEADING_COMMENTS; ?></div>
        <div><?php echo tep_draw_textarea_field('comments', 'soft', '60', '5'); ?></div>
        <div class="buttonsRow vpad tspacer">
          <div class="floater lspacer"><div class="bounder">
            <div class="heavy"><?php echo TITLE_CONTINUE_CHECKOUT_PROCEDURE; ?></div>
            <div><?php echo TEXT_CONTINUE_CHECKOUT_PROCEDURE; ?></div>
          </div></div>
          <div class="floatend rspacer"><?php echo '<a href="#" class="mbutton2 bsubmit">' . IMAGE_BUTTON_CONTINUE . '</a>'; ?></div>
        </div>
      </fieldset></form></div>
      <div class="bounder vspacer"><table border="0" width="100%" cellspacing="0" cellpadding="0">
        <tr>
          <td width="25%"><table border="0" width="100%" cellspacing="0" cellpadding="0">
            <tr>
              <td width="50%" align="right"><?php echo tep_image(DIR_WS_IMAGES . 'checkout_bullet.gif'); ?></td>
              <td width="50%"><?php echo tep_draw_separator('pixel_silver.gif', '100%', '1'); ?></td>
            </tr>
          </table></td>
          <td width="25%"><?php echo tep_draw_separator('pixel_silver.gif', '100%', '1'); ?></td>
          <td width="25%"><?php echo tep_draw_separator('pixel_silver.gif', '100%', '1'); ?></td>
          <td width="25%"><table border="0" width="100%" cellspacing="0" cellpadding="0">
            <tr>
              <td width="50%"><?php echo tep_draw_separator('pixel_silver.gif', '100%', '1'); ?></td>
              <td width="50%"><?php echo tep_draw_separator('pixel_silver.gif', '1', '5'); ?></td>
            </tr>
          </table></td>
        </tr>
        <tr>
          <td align="center" width="25%" class="checkoutBarCurrent"><?php echo CHECKOUT_BAR_DELIVERY; ?></td>
          <td align="center" width="25%" class="checkoutBarTo"><?php echo CHECKOUT_BAR_PAYMENT; ?></td>
          <td align="center" width="25%" class="checkoutBarTo"><?php echo CHECKOUT_BAR_CONFIRMATION; ?></td>
          <td align="center" width="25%" class="checkoutBarTo"><?php echo CHECKOUT_BAR_FINISHED; ?></td>
        </tr>
      </table></div>
<?php require('includes/objects/html_end.php'); ?>
