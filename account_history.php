<?php
/*
  $Id: account_history.php,v 1.63 2003/06/09 23:03:52 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

//----------------------------------------------------------------------------
// Copyright (c) 2007 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Catalog: Account History List page
//----------------------------------------------------------------------------
// I-Metrics Layer
//----------------------------------------------------------------------------
// Modifications:
// - 07/05/2007: PHP5 Register Globals and Long Arrays Off support added
// - 07/08/2007: PHP5 Long Arrays Off support added
// - 07/12/2007: Moved HTML Header/Footer to a common section
// - 08/31/2007: HTML Body Common Sections Added
// - 11/17/2009: Converted Tables to CSS
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
  require('includes/application_top.php');

  if (!tep_session_is_registered('customer_id')) {
    $navigation->set_snapshot();
    tep_redirect(tep_href_link(FILENAME_LOGIN, '', 'SSL'));
  }

  require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_ACCOUNT_HISTORY);

  $breadcrumb->add(NAVBAR_TITLE_1, tep_href_link(FILENAME_ACCOUNT, '', 'SSL'));
  $breadcrumb->add(NAVBAR_TITLE_2, tep_href_link(FILENAME_ACCOUNT_HISTORY, '', 'SSL'));
?>
<?php require('includes/objects/html_start_sub1.php'); ?>
<?php require('includes/objects/html_start_sub2.php'); ?>
<?php require('includes/objects/html_body_header.php'); ?>
<?php
  $history_query_raw = "select o.orders_id, o.date_purchased, o.delivery_name, o.billing_name, ot.text as order_total, s.orders_status_name from " . TABLE_ORDERS . " o, " . TABLE_ORDERS_TOTAL . " ot, " . TABLE_ORDERS_STATUS . " s where o.customers_id = '" . (int)$customer_id . "' and o.orders_id = ot.orders_id and ot.class = 'ot_total' and o.orders_status = s.orders_status_id and s.language_id = '" . (int)$languages_id . "' order by orders_id DESC";
  $history_split = new splitPageResults($history_query_raw, MAX_DISPLAY_ORDER_HISTORY);

  if( $history_split->number_of_rows ) {
?>
    <div><table class="tabledata">
      <tr class="buttonsRow">
        <th><?php echo TABLE_HEADING_ORDER; ?></th>
        <th><?php echo TABLE_HEADING_ORDER_STATUS; ?></th>
        <th><?php echo TABLE_HEADING_ORDER_DATE; ?></th>
        <th><?php echo TABLE_HEADING_ORDER_TYPE; ?></th>
        <th class="calign"><?php echo TABLE_HEADING_ORDER_PRODUCTS; ?></th>
        <th class="calign"><?php echo TABLE_HEADING_ORDER_VIEW; ?></th>
      </tr>
<?php
    $history_query = tep_db_query($history_split->sql_query);
    while ($history = tep_db_fetch_array($history_query)) {
      $products_query = tep_db_query("select count(*) as count from " . TABLE_ORDERS_PRODUCTS . " where orders_id = '" . (int)$history['orders_id'] . "'");
      $products = tep_db_fetch_array($products_query);

      if (tep_not_null($history['delivery_name'])) {
        $order_type = TEXT_ORDER_SHIPPED_TO;
        $order_name = $history['delivery_name'];
      } else {
        $order_type = TEXT_ORDER_BILLED_TO;
        $order_name = $history['billing_name'];
      }
?>
      <tr>
        <td><?php echo $history['orders_id']; ?></td>
        <td class="heavy"><?php echo $history['orders_status_name']; ?></td>
        <td class="heavy"><?php echo tep_date_long($history['date_purchased']); ?></td>
        <td><?php echo $order_type . ' ' . tep_output_string_protected($order_name); ?></td>
        <td class="calign"><?php echo $products['count']; ?></td>
        <td class="calign"><?php echo '<a href="' . tep_href_link(FILENAME_ACCOUNT_HISTORY_INFO, 'order_id=' . $history['orders_id'], 'SSL') . '" class="tbutton2">' . SMALL_IMAGE_BUTTON_VIEW . '</a>'; ?></td>
      </tr>

<?php
    }
?>
    </table></div>
    <div class="bounder vpad vspacer">
      <div class="floater lspacer"><?php echo $history_split->display_count(TEXT_DISPLAY_NUMBER_OF_ORDERS); ?></div>
      <div class="floatend rspacer"><?php echo TEXT_RESULT_PAGE . ' ' . $history_split->display_links(MAX_DISPLAY_PAGE_LINKS); ?></div>
    </div>
<?php
  } else {
?>
    <div class="heavy vpad vspacer"><?php echo TEXT_NO_PURCHASES; ?></div>
<?php
  }
?>
    <div class="buttonsRow vpad vspacer">
      <div class="lspacer"><?php echo '<a href="' . tep_href_link(FILENAME_ACCOUNT, '', 'SSL') . '" class="mbutton">' . IMAGE_BUTTON_BACK . '</a>'; ?></div>
    </div>
<?php require('includes/objects/html_end.php'); ?>
