<?php
/*
  $Id: popup_image.php,v 1.18 2003/06/05 23:26:23 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

  require('includes/application_top.php');
  $navigation->remove_current_page();

  $image_flag = false;
  $products_query = tep_db_query("select pd.products_name, p.products_image from " . TABLE_PRODUCTS . " p left join " . TABLE_PRODUCTS_DESCRIPTION . " pd on p.products_id = pd.products_id where p.products_id = '" . (int)$HTTP_GET_VARS['pID'] . "' and pd.language_id = '" . (int)$languages_id . "'");
  if($swap_image = tep_db_fetch_array($products_query) ) {
    $current_image = tep_image(DIR_WS_IMAGES . $swap_image['products_image'], $swap_image['products_name'], '','','class="small_popup_images"');
    $plain_image = $swap_image['products_image'];
  } else {
    tep_redirect(tep_href_link('', '', 'NONSSL', false));
    //die('Error - Invalid Image Specified');
  }

  if( !tep_not_null($plain_image) || !file_exists(DIR_WS_IMAGES . $plain_image) || filesize(DIR_WS_IMAGES . $plain_image) <= 0 ) {
    $current_image = tep_image(DIR_WS_IMAGES . 'image_not_available.jpg', $swap_image['products_name']);
    $plain_image = 'image_not_available.jpg';
  }

  list($img_width, $img_height, , ) = getimagesize(DIR_WS_IMAGES . $plain_image);
  $img_width += SMALL_IMAGE_WIDTH + 15;

//  $products_query = tep_db_query("select pd.products_name, p.products_image from " . TABLE_PRODUCTS . " p left join " . TABLE_PRODUCTS_DESCRIPTION . " pd on p.products_id = pd.products_id where p.products_status = '1' and p.products_id = '" . (int)$_GET['pID'] . "' and pd.language_id = '" . (int)$languages_id . "'");
//  $products = tep_db_fetch_array($products_query);
?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<meta name="robots" content="noindex,nofollow">
<title><?php echo $swap_image['products_name']; ?></title>
<base href="<?php echo (($request_type == 'SSL') ? HTTPS_SERVER : HTTP_SERVER) . DIR_WS_CATALOG; ?>">
<script language="javascript"><!--
  function resize() {
    NS = (navigator.appName=="Netscape")?true:false; 
    iWidth = (NS)?window.innerWidth:document.body.clientWidth;
    iHeight = (NS)?window.innerHeight:document.body.clientHeight;
    iWidth = document.images[0].width - iWidth;
    iHeight = document.images[0].height - iHeight;
    window.resizeBy(iWidth+20, iHeight+20);
    self.focus(); 
  }
//--></script>
</head>
<body onload="resize();">
  <table width="100%" height="100%">
    <tr>
      <td align="center"><?php echo $current_image; ?></td>
     </tr>
  </table>
</body>
</html>
<?php require('includes/application_bottom.php'); ?>
