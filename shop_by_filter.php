<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2008 Asymmetric Software. Innovation & Excellence.
// Author: Mark Samios
// http://www.asymmetrics.com
// ---------------------------------------------------------------------------
// Catalog: Shop by filter main script
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
  define('FILTER_PER_ROW', 3);
  require('includes/application_top.php');
  $g_products_per_row = FILTER_PER_ROW;
  unset($listing_sql_raw);

  $params_array = $g_filters->get_filters_from_parameters();
  $link_params = isset($cPath_array)?($cPath . '&'):'';
  foreach($params_array as $key => $value) {
    $link_params = $key . '=' . $value . '&';
  }

  if( strlen($link_params) ) {
    substr($link_params, 0, -1);
  }
  //$params_array = $g_filters->get_excluded_filters_from_parameters();
  $listing_sql_raw = tep_get_collection($current_category_id, $params_array);

  require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_SHOP_BY_FILTER);

  $breadcrumb->add(NAVBAR_TITLE, tep_href_link(FILENAME_SHOP_BY_FILTER));
?>
<?php require('includes/objects/html_start_sub1.php'); ?>
<?php require('includes/objects/html_start_sub2.php'); ?>
<?php
  $heading_row = true;
  require('includes/objects/html_body_header.php');
?>
<?php
  if( isset($listing_sql_raw) && tep_not_null($listing_sql_raw) ) {
?>
      <tr>
        <td class="all_borders"><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td class="pageHeading"><h1>
<?php
    $heading_title = HEADING_TITLE;
    if( $current_category_id ) {
      $category = tep_get_category_name($current_category_id);
      $heading_title = strtoupper($category) .' &nbsp;-&nbsp;';
    } else {
      $heading_title .= ' &nbsp;-&nbsp;';
    }
    foreach($params_array as $key => $value) {
      $index = $g_filters->db_names_array[$key];
      $name = $g_filters->get_filters_name($index);
      $select = $g_filters->get_option_name($index, $value);
      $heading_title .= strtoupper($name) . ':&nbsp;' . strtoupper($select) . '&nbsp;';
   }
   echo $heading_title;
?>
            </h1></td>
            <td class="pageHeading" align="right">&nbsp;</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><?php echo tep_draw_separator('pixel_trans.gif', '100%', '10'); ?></td>
      </tr>
<?php
    $listing_sql_split = new splitPageResults($listing_sql_raw, MAX_DISPLAY_PRODUCTS_NEW, 'p.products_id');

    if (($listing_sql_split->number_of_rows > 0) && ((PREV_NEXT_BAR_LOCATION == '1') || (PREV_NEXT_BAR_LOCATION == '3'))) {
?>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="2">
          <tr>
            <td class="smallText"><?php echo $listing_sql_split->display_count(TEXT_DISPLAY_NUMBER_OF_PRODUCTS_NEW); ?></td>
            <td align="right" class="smallText"><?php echo TEXT_RESULT_PAGE . ' ' . $listing_sql_split->display_links(MAX_DISPLAY_PAGE_LINKS, $link_params); ?></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><?php echo tep_draw_separator('pixel_trans.gif', '100%', '10'); ?></td>
      </tr>
<?php
    }
    // Initialize vars
    $total_items = array();
    tep_query_to_array($listing_sql_split->sql_query, $total_items);
    $j=count($total_items);
    if( $j > 0 ) {
?>
      <tr>
        <td>
<?php
    $g_filter_flag = 1;
    require('includes/objects/product_listing_common.php');
?>
        </td>
      </tr>
<?php
    } else {
?>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="2">
          <tr>
            <td class="main"><?php echo TEXT_NO_PRODUCTS; ?></td>
          </tr>
          <tr>
            <td><?php echo tep_draw_separator('pixel_trans.gif', '100%', '10'); ?></td>
          </tr>
        </table></td>
      </tr>
<?php
    }
    if (($listing_sql_split->number_of_rows > 0) && ((PREV_NEXT_BAR_LOCATION == '2') || (PREV_NEXT_BAR_LOCATION == '3'))) {
?>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="2">
          <tr>
            <td class="smallText"><?php echo $listing_sql_split->display_count(TEXT_DISPLAY_NUMBER_OF_PRODUCTS_NEW); ?></td>
            <td align="right" class="smallText"><?php echo TEXT_RESULT_PAGE . ' ' . $listing_sql_split->display_links(MAX_DISPLAY_PAGE_LINKS, $link_params); ?></td>
          </tr>
        </table></td>
      </tr>
<?php
    }
  } else {

  }
  $category_depth = 'custom';
?>
<?php require('includes/objects/html_end.php'); ?>
