<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2007-2012 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Catalog: Account Coupons
//----------------------------------------------------------------------------
// I-Metrics Layer
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
  require('includes/application_top.php');

  if (!tep_session_is_registered('customer_id')) {
    $navigation->set_snapshot();
    tep_redirect(tep_href_link(FILENAME_LOGIN, '', 'SSL'));
  }
  require_once(DIR_WS_CLASSES . 'coupons.php');
  require_once(DIR_WS_LANGUAGES . $language . '/' . FILENAME_ACCOUNT_COUPONS);

  $clear_query = tep_db_query("delete from " . TABLE_COUPONS_REDEEM . " where (unix_timestamp(now()) - unix_timestamp(date_added)) > 7776000");

  $action = isset($_GET['action'])?tep_db_prepare_input($_GET['action']):'';
  switch($action) {
    case 'redeem_bids':
      $coupon_code = isset($_POST['coupon_code'])?tep_db_prepare_input($_POST['coupon_code']):'';
      if( empty($coupon_code) ) {
        $messageStack->add_session(tep_get_script_name($g_script), ERROR_COUPON_CODE_EMPTY);
        tep_redirect(tep_href_link($g_script,  '', 'SSL'));
      }
      $cCoupons = new coupons();
      $coupon_array = $cCoupons->get($coupon_code);

      if( !isset($coupon_array['coupons_id']) ) {
        $messageStack->add_session(tep_get_script_name($g_script), ERROR_COUPON_CODE_INVALID);
        tep_redirect(tep_href_link($g_script,  '', 'SSL'));
      }

      if( !$cCoupons->check($coupon_array['coupons_name'], $customer_id) ) {
        $messageStack->add_session(tep_get_script_name($g_script), ERROR_COUPON_CODE_INVALID);
        tep_redirect(tep_href_link($g_script,  '', 'SSL'));
      }

      //$cCoupons->update($coupon_array['coupons_id'], $customer_id);
      $cCoupons->update($coupon_array, $customer_id);
      switch($coupon_array['coupons_type']) {
        case 15:
          tep_db_query("update " . TABLE_CUSTOMERS . " set customers_bids = customers_bids+'" . (int)$coupon_array['coupons_amount'] . "' where customers_id='" . (int)$customer_id . "'");
          break;
        default:
          break;
      }
      $messageStack->add_session(tep_get_script_name($g_script), sprintf(SUCCESS_COUPON_REDEEMED, $coupon_array['coupons_name']), 'success');
      tep_redirect(tep_href_link($g_script,  '', 'SSL'));
      break;
    default:
      break;
  }

  $breadcrumb->add(NAVBAR_TITLE_1, tep_href_link(FILENAME_ACCOUNT, '', 'SSL'));
  $breadcrumb->add(NAVBAR_TITLE_2, tep_href_link(FILENAME_ACCOUNT_COUPONS, '', 'SSL'));
?>
<?php require('includes/objects/html_start_sub1.php'); ?>
<?php require('includes/objects/html_start_sub2.php'); ?>
<?php
  $heading_row = true;
?>
<?php require('includes/objects/html_body_header.php'); ?>
<?php
  $customer_query = tep_db_query("select customers_bids from " . TABLE_CUSTOMERS . " where customers_id = '" . (int)$customer_id . "'");
  $customer_array = tep_db_fetch_array($customer_query);
  $bids_string = '<a href="' . tep_href_link(FILENAME_DEFAULT, 'cPath=9', 'NONSSL') . '" class="deco_none"><span class="pad notice">' . sprintf(TEXT_INFO_AUCTION_BIDS_LEFT, '<span id="bids_notice">' . $customer_array['customers_bids'] . '</span>') . '</span></a>';
?>
    <div class="bounder">
      <div class="floater"><h1><?php echo HEADING_TITLE; ?></h1></div>
      <div class="floatend" style="padding-top: 14px;"><?php echo $bids_string; ?></div>
    </div>
    <div><?php echo TEXT_INFO_COUPON_REDEEM_FORM; ?></div>
    <div class="formArea vpad"><?php echo tep_draw_form('coupon_form', tep_href_link($g_script,  'action=redeem_bids', 'SSL'), 'post'); ?><fieldset><legend><?php echo TEXT_INFO_REDEEM_LEGEND; ?></legend>
      <label class="floater" for="coupon_code"><?php echo TEXT_INFO_ENTER_CODE; ?></label>
      <span class="lspacer"><?php echo tep_draw_input_field('coupon_code', '', 'id="coupon_code"'); ?></span>
      <span class="lspacer"><?php echo '<a href="#" class="tbutton2 bsubmit">' . TEXT_INFO_REDEEM . '</a>'; ?></span>
    </fieldset></form></div>
<?php
  $coupons_query_raw = "select coupons_id, coupons_amount, date_added from " . TABLE_COUPONS_REDEEM . " where customers_id = '" . (int)$customer_id . "' order by date_added DESC";
  $coupons_split = new splitPageResults($coupons_query_raw, MAX_DISPLAY_SEARCH_RESULTS);
  $cCoupons = new coupons();
  if( $coupons_split->number_of_rows ) {
?>
</br></br>

    <div><table class="tabledata">
      <tr class="buttonsRow">
        <th><?php echo TABLE_HEADING_NAME; ?></th>
        <th><?php echo TABLE_HEADING_TYPE; ?></th>
        <th><?php echo TABLE_HEADING_AMOUNT; ?></th>
        <th><?php echo TABLE_HEADING_DATE_ADDED; ?></th>
      </tr>
<?php
    $coupons_query = tep_db_query($coupons_split->sql_query);
    while( $coupons_array = tep_db_fetch_array($coupons_query)) {

      $details_query = tep_db_query("select coupons_name, coupons_type from " . TABLE_COUPONS . " where coupons_id = '" . (int)$coupons_array['coupons_id'] . "'");
      $details_array = tep_db_fetch_array($details_query);
      $type_name = $cCoupons->types[$details_array['coupons_type']];

      switch($details_array['coupons_type']) {
        case 15:
          $amount = sprintf(TEXT_INFO_BIDS, (int)$coupons_array['coupons_amount']);
          break;
        default:
          $amount = $currencies->display_price($details_array['coupons_amount']);
          break;
      }
?>
      <tr>
        <td class="heavy"><?php echo $details_array['coupons_name']; ?></td>
        <td><?php echo $type_name; ?></td>
        <td class="heavy"><?php echo $amount; ?></td>
        <td><?php echo tep_date_short($coupons_array['date_added']); ?></td>
      </tr>
<?php
    }
?>
    </table></div>
    <div class="bounder vpad vspacer">
      <div class="floater lspacer"><?php echo $coupons_split->display_count(TEXT_DISPLAY_NUMBER_OF_ENTRIES); ?></div>
      <div class="floatend rspacer"><?php echo TEXT_RESULT_PAGE . ' ' . $coupons_split->display_links(MAX_DISPLAY_PAGE_LINKS); ?></div>
    </div>
<?php
  } else {
?>
    <div class="heavy vpad vspacer"><?php echo TEXT_INFO_NO_COUPONS; ?></div>
<?php
  }
?>
    <div class="buttonsRow vpad vspacer">
      <div class="lspacer"><?php echo '<a href="' . tep_href_link(FILENAME_ACCOUNT, '', 'SSL') . '" class="mbutton">' . IMAGE_BUTTON_BACK . '</a>'; ?></div>
    </div>
<?php require('includes/objects/html_end.php'); ?>
