<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2010 Asymmetric Software. Innovation & Excellence.
// Author: Mark Samios
// http://www.asymmetrics.com
// Catalog: Handles jQuery events on the home page
//----------------------------------------------------------------------------
// I-Metrics Layer
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// http://www.oscommerce.com
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
  require('includes/application_top.php');
  $navigation->remove_current_page();
  $module = isset($_POST['module'])?tep_db_prepare_input(basename($_POST['module'])):'invalid';
  $module .= '.php';
  if( $g_ajax && is_file(DIR_WS_INCLUDES . 'ajax_modules/' . $module) ) {
    require(DIR_WS_INCLUDES . 'ajax_modules/' . $module);
  } else {
    echo 'error : ajax_modules/' . $module;
  }
?>
<?php require('includes/application_bottom.php'); ?>
