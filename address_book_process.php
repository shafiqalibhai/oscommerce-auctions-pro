<?php
/*
  $Id: address_book_process.php,v 1.79 2003/06/09 23:03:52 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

//----------------------------------------------------------------------------
// Copyright (c) 2007-2008 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Catalog: Address Book Process page
//----------------------------------------------------------------------------
// I-Metrics Layer
//----------------------------------------------------------------------------
// Modifications:
// - 07/05/2007: PHP5 Register Globals and Long Arrays Off support added
// - 07/08/2007: PHP5 Long Arrays Off support added
// - 07/12/2007: Moved HTML Header/Footer to a common section
// - 08/31/2007: HTML Body Common Sections Added
//----------------------------------------------------------------------------
// Released under the GNU General Public License v3.00
//----------------------------------------------------------------------------
*/

  require('includes/application_top.php');

  if (!tep_session_is_registered('customer_id')) {
    $navigation->set_snapshot();
    tep_redirect(tep_href_link(FILENAME_LOGIN, '', 'SSL'));
  }

// needs to be included earlier to set the success message in the messageStack
  require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_ADDRESS_BOOK_PROCESS);

  $action = isset($_GET['action'])?$_GET['action']:'';
  //$country = isset($_POST['country'])?$_POST['country']:STORE_COUNTRY;
  //$state = isset($_POST['state'])?$_POST['state']:STORE_ZONE;
  $country = isset($_POST['country'])?$_POST['country']:tep_default_country_zone();
  $state = isset($_POST['state'])?$_POST['state']:tep_default_country_zone(1,0,$country);

  if( isset($_POST['old_country']) && $_POST['old_country'] != $country) {
    $action='change_country';
  } elseif( isset($_POST['change_country_x']) || isset($_POST['change_country_y'])) {
    $action='change_country';
  }

  switch( $action ) {
    case 'delete_confirm':
      if( $customer_default_address_id == (int)$_GET['ab_id'] ) {
        $messageStack->add_session(tep_get_script_name(FILENAME_ADDRESS_BOOK), WARNING_PRIMARY_ADDRESS_DELETION, 'warning');
        tep_redirect(tep_href_link(FILENAME_ADDRESS_BOOK, '', 'SSL'));
      }
      $check_query = tep_db_query("select address_book_id from " . TABLE_ADDRESS_BOOK . " where customers_id = '" . (int)$customer_id . "'");
      if( !tep_db_num_rows($check_query) ) {
        $messageStack->add_session(tep_get_script_name(FILENAME_ADDRESS_BOOK), ERROR_NONEXISTING_ADDRESS_BOOK_ENTRY);
        tep_redirect(tep_href_link(FILENAME_ADDRESS_BOOK, '', 'SSL'));
      }
      if( tep_db_num_rows($check_query) == 1 ) {
        $messageStack->add_session(tep_get_script_name(FILENAME_ADDRESS_BOOK), WARNING_PRIMARY_ADDRESS_DELETION, 'warning');
        tep_redirect(tep_href_link(FILENAME_ADDRESS_BOOK, '', 'SSL'));
      }

      tep_db_query("delete from " . TABLE_ADDRESS_BOOK . " where address_book_id = '" . (int)$_GET['ab_id'] . "' and customers_id = '" . (int)$customer_id . "'");
/*
      $check_query = tep_db_query("select address_book_id from " . TABLE_ADDRESS_BOOK . " where customers_id = '" . (int)$customer_id . "'");
      if( !tep_db_num_rows($check_query) ) {
        tep_db_query("update " . TABLE_REVIEWS . " set customers_id = null where customers_id = '" . (int)$customer_id . "'");
        tep_db_query("delete from " . TABLE_CUSTOMERS_BASKET . " where customers_id = '" . (int)$customer_id . "'");
        tep_db_query("delete from " . TABLE_CUSTOMERS_BASKET_ATTRIBUTES . " where customers_id = '" . (int)$customer_id . "'");
        tep_db_query("delete from " . TABLE_CUSTOMERS_BASKET_FIELDS . " where customers_id = '" . (int)$customer_id . "'");
        tep_db_query("delete from " . TABLE_CUSTOMERS_BASKET_GROUP_FIELDS . " where customers_id = '" . (int)$customer_id . "'");
        tep_db_query("delete from " . TABLE_WISHLIST . " where customers_id = '" . (int)$customer_id . "'");
        tep_db_query("delete from " . TABLE_WISHLIST_ATTRIBUTES . " where customers_id = '" . (int)$customer_id . "'");
        tep_db_query("delete from " . TABLE_WISHLIST_FIELDS . " where customers_id = '" . (int)$customer_id . "'");
        tep_db_query("delete from " . TABLE_WHOS_ONLINE . " where customer_id = '" . (int)$customer_id . "'");
        tep_db_query("delete from " . TABLE_CUSTOMERS_INFO . " where customers_id = '" . (int)$customer_id . "'");
        tep_db_query("delete from " . TABLE_CUSTOMERS . " where customers_id = '" . (int)$customer_id . "'");

        $messageStack->add(tep_get_script_name(FILENAME_ACCOUNT_REMOVED), WARNING_ACCOUNT_ENTRIES_REMOVED, 'warning');
        $messageStack->add(tep_get_script_name(FILENAME_ACCOUNT_REMOVED), SUCCESS_ACCOUNT_REMOVED, 'success');
        $navigation->clear_snapshot();
        tep_session_destroy();
        $session_started = false;
        tep_redirect(tep_href_link(FILENAME_ADDRESS_BOOK, '', 'SSL'));
      }
*/
      $messageStack->add_session(tep_get_script_name(FILENAME_ADDRESS_BOOK), SUCCESS_ADDRESS_BOOK_ENTRY_DELETED, 'success');
      if( $customer_default_address_id == (int)$_GET['ab_id'] ) {
        $messageStack->add_session(tep_get_script_name(FILENAME_ADDRESS_BOOK), WARNING_PRIMARY_ADDRESS_DELETION, 'warning');
      }

      $check_query = tep_db_query("select address_book_id, entry_country_id, entry_zone_id from " . TABLE_ADDRESS_BOOK . " where customers_id = '" . (int)$customer_id . "' limit 1");
      $check_array = tep_db_fetch_array($check_query);

      if( $customer_default_address_id == (int)$_GET['ab_id'] ) {
        tep_db_query("update " . TABLE_CUSTOMERS . " set customers_default_address_id = '" . (int)$check_array['address_book_id'] . "' where customers_id= '" . (int)$customer_id . "'");
      }

      $customer_default_address_id = $check_array['address_book_id'];
      $customer_country_id = $check_array['entry_country_id'];
      $customer_zone_id = $check_array['entry_zone_id'];

      if( isset($billto) && $billto == (int)$_GET['ab_id'] ) {
        tep_session_unregister('payment');
        $billto = $check_array['address_book_id'];
      }
      if( isset($sendto) && $sendto == (int)$_GET['ab_id'] ) {
        tep_session_unregister('shipping');
        $sendto = $check_array['address_book_id'];
      }

      tep_redirect(tep_href_link(FILENAME_ADDRESS_BOOK, '', 'SSL'));
      break;

    case 'update':
      $check_query = tep_db_query("select address_book_id from " . TABLE_ADDRESS_BOOK . " where address_book_id = '" . (int)$_GET['ab_id'] . "' and customers_id = '" . (int)$customer_id . "'");
      if( !tep_db_num_rows($check_query) ) {
        $messageStack->add(tep_get_script_name(), ERROR_NONEXISTING_ADDRESS_BOOK_ENTRY);
        $error = true;
        break;
      } else {
        $address_id = (int)$_GET['ab_id'];
      }
    case 'insert':
      $error = false;
      if( $action == 'insert') {
        if (tep_count_customer_address_book_entries() >= MAX_ADDRESS_BOOK_ENTRIES) {
          $messageStack->add(tep_get_script_name(), ERROR_ADDRESS_BOOK_FULL);
          $error = true;
          break;
        }
      }

      $firstname = tep_db_prepare_input($_POST['firstname']);
      if (strlen($firstname) < ENTRY_FIRST_NAME_MIN_LENGTH) {
        $messageStack->add(tep_get_script_name(), ENTRY_FIRST_NAME_ERROR);
        $error = true;
      }

      $lastname = tep_db_prepare_input($_POST['lastname']);
      if (strlen($lastname) < ENTRY_LAST_NAME_MIN_LENGTH) {
        $messageStack->add(tep_get_script_name(), ENTRY_LAST_NAME_ERROR);
        $error = true;
      }

      $street_address = tep_db_prepare_input($_POST['street_address']);
      if (strlen($street_address) < ENTRY_STREET_ADDRESS_MIN_LENGTH) {
        $messageStack->add(tep_get_script_name(), ENTRY_STREET_ADDRESS_ERROR);
        $error = true;
      }

      if (ACCOUNT_SUBURB == 'true') {
        $suburb = tep_db_prepare_input($_POST['suburb']);
      }

      $city = tep_db_prepare_input($_POST['city']);
      if (strlen($city) < ENTRY_CITY_MIN_LENGTH) {
        $messageStack->add(tep_get_script_name(), ENTRY_CITY_ERROR);
        $error = true;
      }

      $postcode = tep_db_prepare_input($_POST['postcode']);
      if (strlen($postcode) < ENTRY_POSTCODE_MIN_LENGTH) {
        $messageStack->add(tep_get_script_name(), ENTRY_POST_CODE_ERROR);
        $error = true;
      }

      if (ACCOUNT_STATE == 'true') {
        $check_query = tep_db_query("select z.zone_id from " . TABLE_ZONES . " z left join " . TABLE_COUNTRIES . " c on (c.countries_id=z.zone_country_id) where z.status_id='1' and c.status_id='1' and z.zone_id = '" . (int)$state . "' and c.countries_id = '" . (int)$_POST['country'] . "'");
        if(!tep_db_num_rows($check_query) ) {
          $messageStack->add(tep_get_script_name(), ENTRY_STATE_ERROR_SELECT);
          $error = true;
        }
      }

      $check_query = tep_db_query("select countries_id from " . TABLE_COUNTRIES . " where status_id = '1' and countries_id = '" . (int)$country . "'");
      if(!tep_db_num_rows($check_query) ) {
        $messageStack->add(tep_get_script_name(), ENTRY_COUNTRY_ERROR);
        $error = true;
        $country = STORE_COUNTRY;
      }

      if (ACCOUNT_COMPANY == 'true') {
        $company = tep_db_prepare_input($_POST['company']);
      }
      $primary = isset($_POST['primary'])?$_POST['primary']:'';
      if ($error == false) {
        $sql_data_array = array(
                                'customers_id' => (int)$customer_id,
                                'entry_firstname' => $firstname,
                                'entry_lastname' => $lastname,
                                'entry_street_address' => $street_address,
                                'entry_postcode' => $postcode,
                                'entry_city' => $city,
                                'entry_country_id' => $country
                               );

        if (ACCOUNT_GENDER == 'true') $sql_data_array['entry_gender'] = $gender;
        if (ACCOUNT_COMPANY == 'true') $sql_data_array['entry_company'] = $company;
        if (ACCOUNT_SUBURB == 'true') $sql_data_array['entry_suburb'] = $suburb;

        if (ACCOUNT_STATE == 'true') {
          $sql_data_array['entry_zone_id'] = (int)$state;
          $sql_data_array['entry_state'] = '';
        }

        if( $action == 'insert' ) {
          tep_db_perform(TABLE_ADDRESS_BOOK, $sql_data_array);
          $address_id = tep_db_insert_id();
        } else {
          tep_db_perform(TABLE_ADDRESS_BOOK, $sql_data_array, 'update', "customers_id = '" . (int)$customer_id . "' and address_book_id = '" . (int)$address_id . "'");
        }

        if( isset($_POST['primary']) ) {
          $sql_data_array = array();
          $customer_default_address_id = $sql_data_array['customers_default_address_id'] = (int)$address_id;
          tep_db_perform(TABLE_CUSTOMERS, $sql_data_array, 'update', "customers_id= '" . (int)$customer_id . "'");
          $customer_first_name = $firstname;
          $customer_country_id = $country;
          $customer_zone_id = $state;
          tep_session_unregister('shipping');
          tep_session_unregister('sendto');
          tep_session_unregister('payment');
          tep_session_unregister('billto');
        }

        if (sizeof($navigation->snapshot) > 0) {
          $messageStack->add_session(tep_get_script_name($navigation->snapshot['page']), SUCCESS_ADDRESS_BOOK_ENTRY_UPDATED, 'success');
          $origin_href = tep_href_link($navigation->snapshot['page'], tep_array_to_string($navigation->snapshot['get'], array(tep_session_name())), $navigation->snapshot['mode'], true, false);
          $navigation->clear_snapshot();
          tep_redirect($origin_href);
        } else {
          $messageStack->add_session(tep_get_script_name(FILENAME_ACCOUNT), SUCCESS_ADDRESS_BOOK_ENTRY_UPDATED, 'success');
          tep_redirect(tep_href_link(FILENAME_ACCOUNT));
        }
      }
      break;
    case 'change_country':
      // Standard
      $street_address = $_POST['street_address'];
      $postcode = $_POST['postcode'];
      $city = $_POST['city'];
      $firstname = $_POST['firstname'];
      $lastname = $_POST['lastname'];
      // Optional
      $gender = isset($_POST['gender'])?$_POST['gender']:'';
      $suburb = isset($_POST['suburb'])?$_POST['suburb']:'';
      $company = isset($_POST['company'])?$_POST['company']:'';
      break;
    case 'delete':
      //break;
    default:
      if( isset($_GET['ab_id']) ) {
        $default_id = (int)$_GET['ab_id'];
        $check_query = tep_db_query("select count(*) as total from " . TABLE_ADDRESS_BOOK . " where address_book_id = '" . (int)$default_id . "' and customers_id = '" . (int)$customer_id . "'");
        $check_array = tep_db_fetch_array($check_query);
        if( !$check_array['total'] ) {
          $default_id = $customer_default_address_id;
          $primary = 1;
        }
      } else {
        $default_id = $customer_default_address_id;
        $primary = 1;
      }
      $check_query = tep_db_query("select * from " . TABLE_ADDRESS_BOOK . " where address_book_id = '" . (int)$default_id . "' and customers_id = '" . (int)$customer_id . "'");
      $check_array = tep_db_fetch_array($check_query);

      $firstname = $check_array['entry_firstname'];
      $lastname = $check_array['entry_lastname'];
      $gender = $check_array['entry_gender'];
      $company = $check_array['entry_company'];
      $street_address = $check_array['entry_street_address'];
      $suburb = $check_array['entry_suburb'];
      $city = $check_array['entry_city'];
      $postcode = $check_array['entry_postcode'];
      $state = $check_array['entry_zone_id'];
      $country = $check_array['entry_country_id'];
      if( isset($_GET['ab_id']) && $customer_default_address_id == $_GET['ab_id'] ) {
        $primary = 'on';
      }
      $entry = $check_array;
      $ab_id = $default_id;
      break;
  }

  $breadcrumb->add(NAVBAR_TITLE_1, tep_href_link(FILENAME_ACCOUNT, '', 'SSL'));
  $breadcrumb->add(NAVBAR_TITLE_2, tep_href_link(FILENAME_ADDRESS_BOOK, '', 'SSL'));
?>
<?php require('includes/objects/html_start_sub1.php'); ?>
<?php require('includes/objects/html_start_sub2.php'); ?>
<?php
  $heading_row = true;
  require('includes/objects/html_body_header.php');
?>
<?php
  if($action == 'delete') {
?>
      <div><h1><?php echo DELETE_ADDRESS_TITLE; ?></h1></div>
      <div class="bounder">
        <div class="floater"><?php echo DELETE_ADDRESS_DESCRIPTION; ?></div>
        <div class="floatend orders_overview"><?php echo tep_address_label($customer_id, $ab_id, true, ' ', '<br />'); ?></div>
      </div>
      <div class="buttonsRow vpad vspacer">
        <div class="floater lspacer"><?php echo '<a href="' . tep_href_link($g_script, 'ab_id=' . $ab_id . '&action=delete_confirm', 'SSL') . '" class="mbutton2">' . IMAGE_BUTTON_DELETE . '</a>'; ?></div>
        <div class="floater lspacer"><?php echo '<a href="' . tep_href_link(FILENAME_ADDRESS_BOOK, '', 'SSL') . '" class="mbutton">' . IMAGE_BUTTON_CANCEL . '</a>'; ?></div>
        <div class="floatend rspacer"><?php echo '<a href="' . tep_href_link(FILENAME_ACCOUNT, '', 'SSL') . '" class="mbutton3">' . IMAGE_BUTTON_ACCOUNT . '</a>'; ?></div>
      </div>
<?php
  } else {
    if (sizeof($navigation->snapshot) > 0) {
      $back_link = tep_href_link($navigation->snapshot['page'], tep_array_to_string($navigation->snapshot['get'], array(tep_session_name())), $navigation->snapshot['mode'], true, false);
    } else {
      $back_link = tep_href_link(FILENAME_ADDRESS_BOOK, '', 'SSL');
    }
?>
      <div><h1><?php echo (isset($_GET['ab_id']))?HEADING_TITLE_MODIFY_ENTRY:HEADING_TITLE_ADD_ENTRY; ?></h1></div>
      <div class="bounder"><?php echo tep_draw_form('addressbook', tep_href_link($g_script, (isset($_GET['ab_id']) ? 'action=update&ab_id=' . $_GET['ab_id'] : 'action=insert'), 'SSL'), 'post'); ?><fieldset><legend><?php echo TEXT_INFO_FORM; ?></legend>
        <div class="bounder vpad bspacer altrow heavy">
          <div class="floater halfer lspacer"><span class="floater rspacer"><?php echo tep_draw_checkbox_field('primary', 1, '', 'id="primary"'); ?></span><label for="primary"><?php echo SET_AS_PRIMARY; ?></label></div>
          <div class="floatend rspacer"><span class="frequired vpad"><?php echo FORM_REQUIRED_INFORMATION; ?></span></div>
        </div>
        <div class="vpad"><label for="firstname" class="halfer floater"><?php echo ENTRY_FIRST_NAME; ?></label><?php echo tep_draw_input_field('firstname', '', 'id="firstname"', 'text', true, (isset($error_array['firstname'])?$error_array['firstname']:false)); ?><span class="frequired cpad lspacer"></span></div>
        <div class="vpad"><label for="lastname" class="halfer floater"><?php echo ENTRY_LAST_NAME; ?></label><?php echo tep_draw_input_field('lastname', '', 'id="lastname"', 'text', true, (isset($error_array['lastname'])?$error_array['lastname']:false)); ?><span class="frequired cpad lspacer"></span></div>
        <div class="vpad"><label for="company" class="halfer floater"><?php echo ENTRY_COMPANY; ?></label><?php echo tep_draw_input_field('company', '', 'id="company"', 'text', true, (isset($error_array['company'])?$error_array['company']:false)); ?></div>
        <div class="tpad"><label for="street_address" class="halfer floater"><?php echo ENTRY_STREET_ADDRESS; ?></label><?php echo tep_draw_input_field('street_address', '', 'id="street_address"', 'text', true, (isset($error_array['street_address'])?$error_array['street_address']:false)); ?><span class="frequired cpad lspacer"></span></div>
        <div class="bpad"><label for="suburb" class="halfer floater"><?php echo ENTRY_SUBURB; ?></label><?php echo tep_draw_input_field('suburb', '', 'id="suburb"', 'text', true, (isset($error_array['suburb'])?$error_array['suburb']:false)); ?></div>
        <div class="vpad"><label for="city" class="halfer floater"><?php echo ENTRY_CITY; ?></label><?php echo tep_draw_input_field('city', '', 'id="city"', 'text', true, (isset($error_array['city'])?$error_array['city']:false)); ?><span class="frequired cpad lspacer"></span></div>
        <div class="vpad"><label for="state" class="halfer floater"><?php echo ENTRY_STATE; ?></label><?php echo '<span id="switch_country">' . tep_draw_pull_down_menu('state', tep_get_country_zones($country), $state, 'id="state"') . '</span>'; ?><span class="frequired cpad lspacer"></span></div>
        <div class="vpad"><label for="country" class="halfer floater"><?php echo ENTRY_COUNTRY; ?></label><?php echo tep_get_country_list('country', $country, 'id="country" style="width:180px;"') . tep_draw_hidden_field('old_country', $country); ?><span style="margin-left: 8px;" id="hide_country"><?php echo tep_main_image_submit(DIR_WS_ICONS . 'icon_reload.png', IMAGE_BUTTON_CHANGE_COUNTRY, '', '', 'name="change_country"'); ?></span><span class="frequired cpad lspacer"></span></div>
        <div class="vpad"><label for="postcode" class="halfer floater"><?php echo ENTRY_POST_CODE; ?></label><?php echo tep_draw_input_field('postcode', '', 'id="postcode"', 'text', true, (isset($error_array['postcode'])?$error_array['postcode']:false)); ?><span class="frequired cpad lspacer"></span></div>
        <div class="buttonsRow vpad tspacer">
          <div class="floater lspacer">
<?php
    if( !isset($_GET['ab_id']) ) {
      echo '<a href="#" class="mbutton2 bsubmit">' . IMAGE_BUTTON_CONFIRM . '</a>';
    } else {
      echo '<a href="#" class="mbutton2 bsubmit">' . IMAGE_BUTTON_UPDATE . '</a>';
    }
?>
          </div>
          <div class="floatend rspacer"><?php echo '<a href="' . tep_href_link(FILENAME_ADDRESS_BOOK, '', 'SSL') . '" class="mbutton3">' . IMAGE_BUTTON_ADDRESS_BOOK . '</a>'; ?></div>
        </div>
      </fieldset></form></div>
<?php
  }
?>
<?php require('includes/objects/html_end.php'); ?>
