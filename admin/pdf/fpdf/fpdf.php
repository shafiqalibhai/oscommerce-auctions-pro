<?php
/*
//----------------------------------------------------------------------------
// Software: FPDF                                                               
// Based on Version: 1.53                                                      
// Date:     2004-12-31                                                         
// Authors:  Olivier PLATHEY 
//           Nicola Asuni (2005-08-28)
//           Mark Samios
//----------------------------------------------------------------------------
// This class is an extension and improvement of 
// the Public Domain FPDF v1.53 class by Olivier Plathey 
// (http://www.fpdf.org).
//----------------------------------------------------------------------------
// Main changes by Nicola Asuni:
//    UTF-8 Unicode support;
//    source code clean up;
//    code style and formatting;
//    All ISO page formats were included;
//    image scale factor;
//    includes methods to parse and printsome XHTML code, supporting the following elements: h1, h2, h3, h4, h5, h6, b, u, i, a, img, p, br, strong, em, font, blockquote, li, ul, ol, hr, td, th, tr, table, sup, sub, small;
//    defines standard Header() and Footer() methods.
//----------------------------------------------------------------------------
// Modifications by Asymmetrics.
// Last Update : 2006-05-08
// Copyright (c) 2006 Asymmetric Software. Innovation & Excellence.
// http://www.asymmetrics.com
// PDF MySQL Report Class
// - Ported for osCommerce
// - Fix for core fonts to take into account the descriptor
// - Added file names during font errors
// - Added Line Height function
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
if(!class_exists('FPDF')) {
  define("K_CELL_HEIGHT_RATIO", 1.25);
  define("K_SMALL_RATIO", 2/3);

  define('FPDF_VERSION','1.53 osC');

  class FPDF {
    //Private properties
    var $page;               //current page number
    var $n;                  //current object number
    var $offsets;            //array of object offsets
    var $buffer;             //buffer holding in-memory PDF
    var $pages;              //array containing pages
    var $state;              //current document state
    var $compress;           //compression flag
    var $DefOrientation;     //default orientation
    var $CurOrientation;     //current orientation
    var $OrientationChanges; //array indicating orientation changes
    var $k;                  //scale factor (number of points in user unit)
    var $fwPt,$fhPt;         //dimensions of page format in points
    var $fw,$fh;             //dimensions of page format in user unit
    var $wPt,$hPt;           //current dimensions of page in points
    var $w,$h;               //current dimensions of page in user unit
    var $lMargin;            //left margin
    var $tMargin;            //top margin
    var $rMargin;            //right margin
    var $bMargin;            //page break margin
    var $cMargin;            //cell margin
    var $x,$y;               //current position in user unit for cell positioning
    var $lasth;              //height of last cell printed
    var $LineWidth;          //line width in user unit
    var $CoreFonts;          //array of standard font names
    var $fonts;              //array of used fonts
    var $FontFiles;          //array of font files
    var $diffs;              //array of encoding differences
    var $images;             //array of used images
    var $PageLinks;          //array of links in pages
    var $links;              //array of internal links
    var $FontFamily;         //current font family
    var $FontStyle;          //current font style
    var $underline;          //underlining flag
    var $CurrentFont;        //current font info
    var $FontSizePt;         //current font size in points
    var $FontSize;           //current font size in user unit
    var $DrawColor;          //commands for drawing color
    var $FillColor;          //commands for filling color
    var $TextColor;          //commands for text color
    var $ColorFlag;          //indicates whether fill and text colors are different
    var $ws;                 //word spacing
    var $AutoPageBreak;      //automatic page breaking
    var $PageBreakTrigger;   //threshold used to trigger page breaks
    var $InFooter;           //flag set when processing footer
    var $ZoomMode;           //zoom display mode
    var $LayoutMode;         //layout display mode
    var $title;              //title
    var $subject;            //subject
    var $author;             //author
    var $keywords;           //keywords
    var $creator;            //creator
    var $AliasNbPages;       //alias for total number of pages
    var $ehw;                //Effective horizontal width
    var $isunicode;
    var $HREF;
    var $PDFVersion;         //PDF version number
  
//----------------------------------------------------------------------------
//
// Public Methods
//
//----------------------------------------------------------------------------

    function FPDF($orientation='P',$unit='mm',$format='A4') {
      //Some checks
      $this->_dochecks();
      //Initialization of properties
      $this->page=0;
      $this->n=2;
      $this->buffer='';
      $this->pages=array();
      $this->OrientationChanges=array();
      $this->state=0;
      $this->fonts=array();
      $this->FontFiles=array();
      $this->diffs=array();
      $this->images=array();
      $this->links=array();
      $this->InFooter=false;
      $this->lasth=0;
      $this->FontFamily='';
      $this->FontStyle='';
      $this->FontSizePt=12;
      $this->underline=false;
      $this->DrawColor='0 G';
      $this->FillColor='0 g';
      $this->TextColor='0 g';
      $this->ColorFlag=false;
      $this->ws=0;
      $this->isunicode = false;

      //Standard fonts
      $this->CoreFonts=array(
                             'courier'=>'Courier','courierB'=>'Courier-Bold','courierI'=>'Courier-Oblique','courierBI'=>'Courier-BoldOblique',
                             'verdana'=>'Verdana','verdanaB'=>'Verdana-Bold','verdanaI'=>'Verdana-Oblique','verdanaBI'=>'Verdana-BoldOblique',
                             'helvetica'=>'Helvetica','helveticaB'=>'Helvetica-Bold','helveticaI'=>'Helvetica-Oblique','helveticaBI'=>'Helvetica-BoldOblique',
                             'times'=>'Times-Roman','timesB'=>'Times-Bold','timesI'=>'Times-Italic','timesBI'=>'Times-BoldItalic',
                             'symbol'=>'Symbol','zapfdingbats'=>'ZapfDingbats'
                            );

      //Scale factor
      // 2003-06-11 - Nicola Asuni : changed if/else with switch statement
      switch (strtolower($unit)){
          case 'pt': {$this->k=1; break;}
          case 'mm': {$this->k=72/25.4; break;}
          case 'cm': {$this->k=72/2.54;; break;}
          case 'in': {$this->k=72;; break;}
          default : {$this->Error('Incorrect unit: '.$unit); break;}
      }

      //Page format
      if(is_string($format)) {
        // 2002-07-24 - Nicola Asuni (info@tecnick.com)
        // Added new page formats (45 standard ISO paper formats and 4 american common formats).
        // Paper cordinates are calculated in this way: (inches * 72) where (1 inch = 2.54 cm)
        switch (strtoupper($format)){
          case '4A0': {$format = array(4767.87,6740.79); break;}
          case '2A0': {$format = array(3370.39,4767.87); break;}
          case 'A0': {$format = array(2383.94,3370.39); break;}
          case 'A1': {$format = array(1683.78,2383.94); break;}
          case 'A2': {$format = array(1190.55,1683.78); break;}
          case 'A3': {$format = array(841.89,1190.55); break;}
          case 'A4': default: {$format = array(595.28,841.89); break;}
          case 'A5': {$format = array(419.53,595.28); break;}
          case 'A6': {$format = array(297.64,419.53); break;}
          case 'A7': {$format = array(209.76,297.64); break;}
          case 'A8': {$format = array(147.40,209.76); break;}
          case 'A9': {$format = array(104.88,147.40); break;}
          case 'A10': {$format = array(73.70,104.88); break;}
          case 'B0': {$format = array(2834.65,4008.19); break;}
          case 'B1': {$format = array(2004.09,2834.65); break;}
          case 'B2': {$format = array(1417.32,2004.09); break;}
          case 'B3': {$format = array(1000.63,1417.32); break;}
          case 'B4': {$format = array(708.66,1000.63); break;}
          case 'B5': {$format = array(498.90,708.66); break;}
          case 'B6': {$format = array(354.33,498.90); break;}
          case 'B7': {$format = array(249.45,354.33); break;}
          case 'B8': {$format = array(175.75,249.45); break;}
          case 'B9': {$format = array(124.72,175.75); break;}
          case 'B10': {$format = array(87.87,124.72); break;}
          case 'C0': {$format = array(2599.37,3676.54); break;}
          case 'C1': {$format = array(1836.85,2599.37); break;}
          case 'C2': {$format = array(1298.27,1836.85); break;}
          case 'C3': {$format = array(918.43,1298.27); break;}
          case 'C4': {$format = array(649.13,918.43); break;}
          case 'C5': {$format = array(459.21,649.13); break;}
          case 'C6': {$format = array(323.15,459.21); break;}
          case 'C7': {$format = array(229.61,323.15); break;}
          case 'C8': {$format = array(161.57,229.61); break;}
          case 'C9': {$format = array(113.39,161.57); break;}
          case 'C10': {$format = array(79.37,113.39); break;}
          case 'RA0': {$format = array(2437.80,3458.27); break;}
          case 'RA1': {$format = array(1729.13,2437.80); break;}
          case 'RA2': {$format = array(1218.90,1729.13); break;}
          case 'RA3': {$format = array(864.57,1218.90); break;}
          case 'RA4': {$format = array(609.45,864.57); break;}
          case 'SRA0': {$format = array(2551.18,3628.35); break;}
          case 'SRA1': {$format = array(1814.17,2551.18); break;}
          case 'SRA2': {$format = array(1275.59,1814.17); break;}
          case 'SRA3': {$format = array(907.09,1275.59); break;}
          case 'SRA4': {$format = array(637.80,907.09); break;}
          case 'LETTER': {$format = array(612.00,792.00); break;}
          case 'LEGAL': {$format = array(612.00,1008.00); break;}
          case 'EXECUTIVE': {$format = array(521.86,756.00); break;}
          case 'FOLIO': {$format = array(612.00,936.00); break;}
          // default: {$this->Error('Unknown page format: '.$format); break;}
          // END CHANGES Nicola Asuni
        }
        $this->fwPt=$format[0];
        $this->fhPt=$format[1];
      } else {
          $this->fwPt=$format[0]*$this->k;
          $this->fhPt=$format[1]*$this->k;
      }
      $this->fw=$this->fwPt/$this->k;
      $this->fh=$this->fhPt/$this->k;
      //Page orientation
      $orientation=strtolower($orientation);
      if($orientation=='p' || $orientation=='portrait') {
          $this->DefOrientation='P';
          $this->wPt=$this->fwPt;
          $this->hPt=$this->fhPt;
      } elseif($orientation=='l' || $orientation=='landscape') {
          $this->DefOrientation='L';
          $this->wPt=$this->fhPt;
          $this->hPt=$this->fwPt;
      } else {
        $this->Error('Incorrect orientation: '.$orientation);
      }
      $this->CurOrientation=$this->DefOrientation;
      $this->w=$this->wPt/$this->k;
      $this->h=$this->hPt/$this->k;
      //Page margins (1 cm)
      $margin=28.35/$this->k;
      $this->SetMargins($margin,$margin);
      //Interior cell margin (1 mm)
      $this->cMargin=$margin/10;
      //Line width (0.2 mm)
      $this->LineWidth=.567/$this->k;
      //Automatic page break
      $this->SetAutoPageBreak(true,2*$margin);
      //Full width display mode
      $this->SetDisplayMode('fullwidth');
      //Enable compression
      $this->SetCompression(true);
      //Set default PDF version number
      $this->PDFVersion='1.3';
    }

    /**
    * Set the image scale.
    * @param float $scale image scale.
    * @author Nicola Asuni
    * @since 1.5.2
    */
    function setImageScale($scale) {
      $this->imgscale=$scale;
    }

    /**
    * Returns the image scale.
    * @return float image scale.
    * @author Nicola Asuni
    * @since 1.5.2
    */
    function getImageScale() {
      return $this->imgscale;
    }

    /**
    * Returns the page width in units.
    * @return int page width.
    * @author Nicola Asuni
    * @since 1.5.2
    */
    function getPageWidth() {
      return $this->w;
    }

    /**
    * Returns the page height in units.
    * @return int page height.
    * @author Nicola Asuni
    * @since 1.5.2
    */
    function getPageHeight() {
        return $this->fh;
    }

    /**
    * Returns the page break margin.
    * @return int page break margin.
    * @author Nicola Asuni
    * @since 1.5.2
    */
    function getBreakMargin() {
        return $this->bMargin;
    }

    /**
    * Returns the scale factor (number of points in user unit).
    * @return int scale factor.
    * @author Nicola Asuni
    * @since 1.5.2
    */
    function getScaleFactor() {
        return $this->k;
    }


    function SetMargins($left,$top,$right=-1) {
      //Set left, top and right margins
      $this->lMargin=$left;
      $this->tMargin=$top;
      if($right==-1)
        $right=$left;
      $this->rMargin=$right;
      // Set effective horizontal width
      $this->ehw = $this->w - $this->lMargin - $this->rMargin;
    }

    function SetLeftMargin($margin) {
      //Set left margin
      $this->lMargin=$margin;
      if($this->page>0 && $this->x<$margin)
        $this->x=$margin;
    }

    function SetTopMargin($margin) {
        //Set top margin
        $this->tMargin=$margin;
    }
    
    function SetRightMargin($margin) {
        //Set right margin
        $this->rMargin=$margin;
    }
    
    function SetAutoPageBreak($auto,$margin=0) {
        //Set auto page break mode and triggering margin
        $this->AutoPageBreak=$auto;
        $this->bMargin=$margin;
        $this->PageBreakTrigger=$this->h-$margin;
    }

    function SetDisplayMode($zoom,$layout='continuous') {
      //Set display mode in viewer
      if($zoom=='fullpage' || $zoom=='fullwidth' || $zoom=='real' || $zoom=='default' || !is_string($zoom))
        $this->ZoomMode=$zoom;
      else
        $this->Error('Incorrect zoom display mode: '.$zoom);
      if($layout=='single' || $layout=='continuous' || $layout=='two' || $layout=='default')
        $this->LayoutMode=$layout;
      else
        $this->Error('Incorrect layout display mode: '.$layout);
    }
    
    function SetCompression($compress) {
      //Set page compression
      if(function_exists('gzcompress'))
        $this->compress=$compress;
      else
        $this->compress=false;
    }

    function SetTitle($title) {
      //Title of document
      $this->title=$title;
    }

    function SetSubject($subject) {
      //Subject of document
      $this->subject=$subject;
    }

    function SetAuthor($author) {
      //Author of document
      $this->author=$author;
    }

    function SetKeywords($keywords) {
      //Keywords of document
      $this->keywords=$keywords;
    }

    function SetCreator($creator) {
      //Creator of document
      $this->creator=$creator;
    }

    function AliasNbPages($alias='{nb}') {
      //Define an alias for total number of pages
      $this->AliasNbPages=$alias;
    }

    function Error($msg) {
      //Fatal error
      die('<B>FPDF error: </B>'.$msg);
    }

    function Open() {
      //Begin document
      $this->state=1;
    }

    function Close() {
      //Terminate document
      if($this->state==3)
          return;
      if($this->page==0)
          $this->AddPage();
      //Page footer
      $this->InFooter=true;
      $this->Footer();
      $this->InFooter=false;
      //Close page
      $this->_endpage();
      //Close document
      $this->_enddoc();
    }

    function AddPage($orientation='') {
      //Start a new page
      if($this->state==0)
        $this->Open();
      $family=$this->FontFamily;
      $style=$this->FontStyle.($this->underline ? 'U' : '');
      $size=$this->FontSizePt;
      $lw=$this->LineWidth;
      $dc=$this->DrawColor;
      $fc=$this->FillColor;
      $tc=$this->TextColor;
      $cf=$this->ColorFlag;
      if($this->page>0) {
        //Page footer
        $this->InFooter=true;
        $this->Footer();
        $this->InFooter=false;
        //Close page
        $this->_endpage();
      }
      //Start new page
      $this->_beginpage($orientation);
      //Set line cap style to square
      $this->_out('2 J');
      //Set line width
      $this->LineWidth=$lw;
      $this->_out(sprintf('%.2f w',$lw*$this->k));
      //Set font
      if($family)
        $this->SetFont($family,$style,$size);
      //Set colors
      $this->DrawColor=$dc;
      if($dc!='0 G')
        $this->_out($dc);
      $this->FillColor=$fc;
      if($fc!='0 g')
        $this->_out($fc);
      $this->TextColor=$tc;
      $this->ColorFlag=$cf;
      //Page header
      $this->Header();
      //Restore line width
      if($this->LineWidth!=$lw) {
        $this->LineWidth=$lw;
        $this->_out(sprintf('%.2f w',$lw*$this->k));
      }
      //Restore font
      if($family)
        $this->SetFont($family,$style,$size);
      //Restore colors
      if($this->DrawColor!=$dc) {
        $this->DrawColor=$dc;
        $this->_out($dc);
      }
      if($this->FillColor!=$fc) {
        $this->FillColor=$fc;
        $this->_out($fc);
      }
      $this->TextColor=$tc;
      $this->ColorFlag=$cf;
    }


function Header()
{
    //To be implemented in your own inherited class
}

function Footer()
{
    //To be implemented in your own inherited class
}

    function PageNo() {
      //Get current page number
      return $this->page;
    }

    function SetDrawColor($r,$g=-1,$b=-1) {
      //Set color for all stroking operations
      if(($r==0 && $g==0 && $b==0) || $g==-1)
        $this->DrawColor=sprintf('%.3f G',$r/255);
      else
        $this->DrawColor=sprintf('%.3f %.3f %.3f RG',$r/255,$g/255,$b/255);
      if($this->page>0)
        $this->_out($this->DrawColor);
    }

    function SetFillColor($r,$g=-1,$b=-1) {
      //Set color for all filling operations
      if(($r==0 && $g==0 && $b==0) || $g==-1)
        $this->FillColor=sprintf('%.3f g',$r/255);
      else
        $this->FillColor=sprintf('%.3f %.3f %.3f rg',$r/255,$g/255,$b/255);
      $this->ColorFlag=($this->FillColor!=$this->TextColor);
      if($this->page>0)
        $this->_out($this->FillColor);
    }

    function SetTextColor($r,$g=-1,$b=-1) {
      //Set color for text
      if(($r==0 && $g==0 && $b==0) || $g==-1)
        $this->TextColor=sprintf('%.3f g',$r/255);
      else
        $this->TextColor=sprintf('%.3f %.3f %.3f rg',$r/255,$g/255,$b/255);
      $this->ColorFlag=($this->FillColor!=$this->TextColor);
    }
/*
    function GetStringWidth($s, $estimate=false) {
      //Get width of a string in the current font
      $s=(string)$s;
      $l=strlen($s);

      if( $estimate )
        return $l*($this->FontSizePt);

      $cw=&$this->CurrentFont['cw'];
      $w=0;


      for($i=0;$i<$l;$i++)
        $w+=$cw[$s{$i}];

      return $w*$this->FontSize/1000;
    }
*/
     /**
     * Returns the length of a string in user unit. A font must be selected.<br>
     * Support UTF-8 Unicode [Nicola Asuni, 2005-01-02]
     * @param string $s The string whose length is to be computed
     * @return int
     * @since 1.2
     */
//-MS- Added Estimate code
    function GetStringWidth($s, $estimate=false) {

      //Get width of a string in the current font
      $s = (string)$s;
      $cw = &$this->CurrentFont['cw'];
      $w = 0;
      $l = strlen($s);

      if( $estimate )
        return $l*($this->FontSizePt);

      if($this->isunicode) {
        $unicode = $this->UTF8StringToArray($s);
        foreach($unicode as $char) {
          if (isset($cw[$char])) {
              $w+=$cw[$char];
          } elseif(isset($cw[ord($char)])) {
              $w+=$cw[ord($char)];
          } elseif(isset($cw[chr($char)])) {
              $w+=$cw[chr($char)];
          } elseif(isset($this->CurrentFont['desc']['MissingWidth'])) {
              $w += $this->CurrentFont['desc']['MissingWidth']; // set default size
          } else {
              $w += 500;
          }
        }
      } else {
        for($i=0; $i<$l; $i++) {
          if (isset($cw[$s{$i}])) {
            $w += $cw[$s{$i}];
          } else if (isset($cw[ord($s{$i})])) {
            $w += $cw[ord($s{$i})];
          }
        }
      }
      return ($w * $this->FontSize / 1000);
    }

    function GetLineHeight() {
      $up=$this->CurrentFont['up'];
      $ut=$this->CurrentFont['ut'];
      $height = $this->FontSizePt - (($up/1000*$this->FontSizePt)*$this->k);
      return $height;
    }

    function GetLineUnder() {
      $ut=$this->CurrentFont['ut'];
      $height = -($up/1000*$this->FontSizePt)*$this->k;
      return $height;
    }

    function SetLineWidth($width) {
      //Set line width
      $this->LineWidth=$width;
      if($this->page>0)
        $this->_out(sprintf('%.2f w',$width*$this->k));
    }

    function Line($x1,$y1,$x2,$y2) {
      //Draw a line
      $this->_out(sprintf('%.2f %.2f m %.2f %.2f l S',$x1*$this->k,($this->h-$y1)*$this->k,$x2*$this->k,($this->h-$y2)*$this->k));
    }

    function Rect($x,$y,$w,$h,$style='') {
      //Draw a rectangle
      if($style=='F')
        $op='f';
      elseif($style=='FD' || $style=='DF')
        $op='B';
      else
        $op='S';
      $this->_out(sprintf('%.2f %.2f %.2f %.2f re %s',$x*$this->k,($this->h-$y)*$this->k,$w*$this->k,-$h*$this->k,$op));
    }

    function AddFont($family,$style='',$file='') {
      //Add a TrueType or Type1 font
      $family=strtolower($family);
      if($family=='arial')
          $family='helvetica';
      $style=strtoupper($style);
      if($style=='IB')
          $style='BI';
      if(isset($this->fonts[$family.$style]))
          $this->Error('Font already added: '.$family.' '.$style);
      if($file=='')
          $file=str_replace(' ','',$family).strtolower($style).'.php';
      if(defined('FPDF_FONTPATH'))
          $file=FPDF_FONTPATH.$file;
      include($file);
      if(!isset($name))
          $this->Error('Could not include font definition file ' . $file);
      $i=count($this->fonts)+1;
      $this->fonts[$family.$style]=array('i'=>$i,'type'=>$type,'name'=>$name,'desc'=>$desc,'up'=>$up,'ut'=>$ut,'cw'=>$cw,'enc'=>$enc,'file'=>$file);
      if($diff) {
        //Search existing encodings
        $d=0;
        $nb=count($this->diffs);
        for($i=1;$i<=$nb;$i++) {
          if($this->diffs[$i]==$diff) {
            $d=$i;
            break;
          }
        }
        if($d==0) {
          $d=$nb+1;
          $this->diffs[$d]=$diff;
        }
        $this->fonts[$family.$style]['diff']=$d;
      }
      if($file) {
        if($type=='TrueType')
          $this->FontFiles[$file]=array('length1'=>$originalsize);
        else
          $this->FontFiles[$file]=array('length1'=>$size1,'length2'=>$size2);
      }
    }

    function SetFont($family,$style='',$size=0) {
      //Select a font; size given in points
      global $fpdf_charwidths;

      $family=strtolower($family);
      $external = false;
      if($family=='')
        $family=$this->FontFamily;
      if($family=='arial')
        $family='helvetica';
      elseif($family=='symbol' || $family=='zapfdingbats')
        $style='';
      elseif($family=='verdana')
        $external = true;
      $style=strtoupper($style);

      if(strpos($style,'U')!==false) {
        $this->underline=true;
        $style=str_replace('U','',$style);
      } else {
        $this->underline=false;
      }
      if($style=='IB')
        $style='BI';
      if($size==0)
        $size=$this->FontSizePt;
      //Test if font is already selected
      if($this->FontFamily==$family && $this->FontStyle==$style && $this->FontSizePt==$size)
        return;
      //Test if used for the first time
      $fontkey=$family.$style;
      if(!isset($this->fonts[$fontkey])) {
        //Check if one of the standard fonts
        if(isset($this->CoreFonts[$fontkey])) {
          if(!isset($fpdf_charwidths[$fontkey])) {
              //Load metric file
              $file=$family;
              //if($family=='times' || $family=='helvetica')
                  $file.=strtolower($style);
              include($this->_getfontpath().$file.'.php');
              if(!isset($fpdf_charwidths[$fontkey]))
                  $this->Error('Could not include font metric file ' . $file);
          }
          $i=count($this->fonts)+1;
          if( $external )
            $this->fonts[$fontkey]=array('i'=>$i,'type'=>$type,'name'=>$name, 'desc'=>$desc, 'up'=>$up,'ut'=>$ut,'cw'=>$fpdf_charwidths[$fontkey]);
          else
            $this->fonts[$fontkey]=array('i'=>$i,'type'=>'core','name'=>$this->CoreFonts[$fontkey],'up'=>-100,'ut'=>50,'cw'=>$fpdf_charwidths[$fontkey]);
        } else {
            $this->Error('Undefined font: '.$family.' '.$style);
        }
      }
      //Select it
      $this->FontFamily=$family;
      $this->FontStyle=$style;
      $this->FontSizePt=$size;
      $this->FontSize=$size/$this->k;
      $this->CurrentFont=&$this->fonts[$fontkey];
      if($this->page>0)
        $this->_out(sprintf('BT /F%d %.2f Tf ET',$this->CurrentFont['i'],$this->FontSizePt));
    }

    function SetFontSize($size) {
      //Set font size in points
      if($this->FontSizePt==$size)
        return;
      $this->FontSizePt=$size;
      $this->FontSize=$size/$this->k;
      if($this->page>0)
        $this->_out(sprintf('BT /F%d %.2f Tf ET',$this->CurrentFont['i'],$this->FontSizePt));
    }

    function AddLink() {
      //Create a new internal link
      $n=count($this->links)+1;
      $this->links[$n]=array(0,0);
      return $n;
    }

    function SetLink($link,$y=0,$page=-1) {
      //Set destination of internal link
      if($y==-1)
        $y=$this->y;
      if($page==-1)
        $page=$this->page;
      $this->links[$link]=array($page,$y);
    }

    function Link($x,$y,$w,$h,$link) {
      //Put a link on the page
      $this->PageLinks[$this->page][]=array($x*$this->k,$this->hPt-$y*$this->k,$w*$this->k,$h*$this->k,$link);
    }

    function Text($x,$y,$txt) {
      //Output a string
      $s=sprintf('BT %.2f %.2f Td (%s) Tj ET',$x*$this->k,($this->h-$y)*$this->k,$this->_escape($txt));
      if($this->underline && $txt!='')
        $s.=' '.$this->_dounderline($x,$y,$txt);
      if($this->ColorFlag)
        $s='q '.$this->TextColor.' '.$s.' Q';
      $this->_out($s);
    }

    function AcceptPageBreak() {
      //Accept automatic page break or not
      return $this->AutoPageBreak;
    }

    function Cell($w,$h=0,$txt='',$border=0,$ln=0,$align='',$fill=0,$link='') {
      //Output a cell
      $k=$this->k;
      if($this->y+$h>$this->PageBreakTrigger && !$this->InFooter && $this->AcceptPageBreak()) {
        //Automatic page break
        $x=$this->x;
        $ws=$this->ws;
        if($ws>0) {
          $this->ws=0;
          $this->_out('0 Tw');
        }
        $this->AddPage($this->CurOrientation);
        $this->x=$x;
//-MS- setup top margin
        $this->setY($this->tMargin);
//-MS- setup top margin EOM
        if($ws>0) {
          $this->ws=$ws;
          $this->_out(sprintf('%.3f Tw',$ws*$k));
        }
      }
      if($w==0)
        $w=$this->w-$this->rMargin-$this->x;
      $s='';
      if($fill==1 || $border==1) {
        if($fill==1)
          $op=($border==1) ? 'B' : 'f';
        else
          $op='S';
        $s=sprintf('%.2f %.2f %.2f %.2f re %s ',$this->x*$k,($this->h-$this->y)*$k,$w*$k,-$h*$k,$op);
      }
      if(is_string($border)) {
        $x=$this->x;
        $y=$this->y;
        if(strpos($border,'L')!==false)
          $s.=sprintf('%.2f %.2f m %.2f %.2f l S ',$x*$k,($this->h-$y)*$k,$x*$k,($this->h-($y+$h))*$k);
        if(strpos($border,'T')!==false)
          $s.=sprintf('%.2f %.2f m %.2f %.2f l S ',$x*$k,($this->h-$y)*$k,($x+$w)*$k,($this->h-$y)*$k);
        if(strpos($border,'R')!==false)
          $s.=sprintf('%.2f %.2f m %.2f %.2f l S ',($x+$w)*$k,($this->h-$y)*$k,($x+$w)*$k,($this->h-($y+$h))*$k);
        if(strpos($border,'B')!==false)
          $s.=sprintf('%.2f %.2f m %.2f %.2f l S ',$x*$k,($this->h-($y+$h))*$k,($x+$w)*$k,($this->h-($y+$h))*$k);
      }
      if($txt!=='') {
        if($align=='R')
          $dx=$w-$this->cMargin-$this->GetStringWidth($txt);
        elseif($align=='C')
          $dx=($w-$this->GetStringWidth($txt))/2;
        else
          $dx=$this->cMargin;
        if($this->ColorFlag)
          $s.='q '.$this->TextColor.' ';
        $txt2=str_replace(')','\\)',str_replace('(','\\(',str_replace('\\','\\\\',$txt)));
        $s.=sprintf('BT %.2f %.2f Td (%s) Tj ET',($this->x+$dx)*$k,($this->h-($this->y+.5*$h+.3*$this->FontSize))*$k,$txt2);
        if($this->underline)
          $s.=' '.$this->_dounderline($this->x+$dx,$this->y+.5*$h+.3*$this->FontSize,$txt);
        if($this->ColorFlag)
          $s.=' Q';
        if($link)
          $this->Link($this->x+$dx,$this->y+.5*$h-.5*$this->FontSize,$this->GetStringWidth($txt),$this->FontSize,$link);
      }
      if($s)
        $this->_out($s);
      $this->lasth=$h;
      if($ln>0) {
        //Go to next line
        $this->y+=$h;
        if($ln==1)
          $this->x=$this->lMargin;
      }
      else
        $this->x+=$w;
    }

    function MultiCell($w,$h,$txt,$border=0,$align='J',$fill=0, $th=0) {
      if( $th < $h ) $th = $h;
      //Output text with automatic or explicit line breaks
      $cw=&$this->CurrentFont['cw'];
      if($w==0)
        $w=$this->w-$this->rMargin-$this->x;
      $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
      $s=str_replace("\r",'',$txt);
      $nb=strlen($s);
      if($nb>0 && $s[$nb-1]=="\n")
        $nb--;
      $b=0;
      if($border) {
        if($border==1) {
          $border='LTRB';
          $b='LRT';
          $b2='LR';
        } else {
          $b2='';
          if(strpos($border,'L')!==false)
            $b2.='L';
          if(strpos($border,'R')!==false)
            $b2.='R';
          $b=(strpos($border,'T')!==false) ? $b2.'T' : $b2;
        }
      }
      $sep=-1;
      $i=0;
      $j=0;
      $l=0;
      $ns=0;
      $nl=1;
      while($i<$nb) {
        //Get next character
        $c=$s{$i};
        if($c=="\n") {
          //Explicit line break
          if($this->ws>0) {
            $this->ws=0;
            $this->_out('0 Tw');
          }
          $this->Cell($w,$h,substr($s,$j,$i-$j),$b,2,$align,$fill);
//-MS- Ajust total cell height
          if( $th > $h )
            $th -= $h;
//-MS- Ajust total cell height EOM
//$this->writeHTMLCell($w,$h,false,false,substr($s,$j,$i-$j),$b,2,$fill);
          $i++;
          $sep=-1;
          $j=$i;
          $l=0;
          $ns=0;
          $nl++;
          if($border && $nl==2)
            $b=$b2;
          continue;
        }
        if($c==' ') {
          $sep=$i;
          $ls=$l;
          $ns++;
        }
        $l+=$cw[$c];
        if($l>$wmax) {
          //Automatic line break
          if($sep==-1) {
            if($i==$j)
              $i++;
            if($this->ws>0) {
              $this->ws=0;
              $this->_out('0 Tw');
            }
            $this->Cell($w,$h,substr($s,$j,$i-$j),$b,2,$align,$fill);
          } else {
            if($align=='J') {
              $this->ws=($ns>1) ? ($wmax-$ls)/1000*$this->FontSize/($ns-1) : 0;
              $this->_out(sprintf('%.3f Tw',$this->ws*$this->k));
            }
            $this->Cell($w,$h,substr($s,$j,$sep-$j),$b,2,$align,$fill);
//$this->writeHTMLCell($w,$h,false, false, substr($s,$j,$sep-$j),$b,2,$fill);
            $i=$sep+1;
          }
          $sep=-1;
          $j=$i;
          $l=0;
          $ns=0;
          $nl++;
          if($border && $nl==2)
            $b=$b2;
//-MS- Ajust total cell height
          if( $th > $h )
            $th -= $h;
//-MS- Ajust total cell height EOM
        }
        else
          $i++;
      }
      //Last chunk
      if($this->ws>0) {
        $this->ws=0;
        $this->_out('0 Tw');
      }

//-MS- Check the vertical margin
      if( $th>$h ) {
        $this->Cell($w,$h,substr($s,$j,$i-$j),$b,2,$align,$fill);
        while($th>$h) {
          $th -= $h;
          if($th < 0)
            $th = 0;
          $this->_out('0 Tw');
          $this->Cell($w,$th,'',$b,2,$align,$fill);
          break;

          if( $th < $h ) {
            if($border && strpos($border,'B')!==false) {
              $b.='B';
            }
//            $this->Cell($w,$h,'',$b,2,$align,$fill);
          }
        }
//-MS- Check the vertical margin EOM
      } else {
        if($border && strpos($border,'B')!==false)
          $b.='B';
        $this->Cell($w,$h,substr($s,$j,$i-$j),$b,2,$align,$fill);
//$this->writeHTMLCell($w,$h,substr($s,$j,$i-$j),$b,2,$fill);
      }
      $this->x=$this->lMargin;
    }

    function Write($h,$txt,$link='') {
      //Output text in flowing mode
      $cw=&$this->CurrentFont['cw'];
      $w=$this->w-$this->rMargin-$this->x;
      $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
      $s=str_replace("\r",'',$txt);
      $nb=strlen($s);
      $sep=-1;
      $i=0;
      $j=0;
      $l=0;
      $nl=1;
      while($i<$nb) {
        //Get next character
        $c=$s{$i};
        if($c=="\n") {
          //Explicit line break
          $this->Cell($w,$h,substr($s,$j,$i-$j),0,2,'',0,$link);
          $i++;
          $sep=-1;
          $j=$i;
          $l=0;
          if($nl==1) {
              $this->x=$this->lMargin;
              $w=$this->w-$this->rMargin-$this->x;
              $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
          }
          $nl++;
          continue;
        }
        if($c==' ')
            $sep=$i;
        $l+=$cw[$c];
        if($l>$wmax) {
          //Automatic line break
          if($sep==-1) {
            if($this->x>$this->lMargin) {
              //Move to next line
              $this->x=$this->lMargin;
              $this->y+=$h;
              $w=$this->w-$this->rMargin-$this->x;
              $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
              $i++;
              $nl++;
              continue;
            }
            if($i==$j)
              $i++;
            $this->Cell($w,$h,substr($s,$j,$i-$j),0,2,'',0,$link);
          } else {
            $this->Cell($w,$h,substr($s,$j,$sep-$j),0,2,'',0,$link);
            $i=$sep+1;
          }
          $sep=-1;
          $j=$i;
          $l=0;
          if($nl==1) {
            $this->x=$this->lMargin;
            $w=$this->w-$this->rMargin-$this->x;
            $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
          }
          $nl++;
        }
        else
          $i++;
      }
      //Last chunk
      if($i!=$j)
        $this->Cell($l/1000*$this->FontSize,$h,substr($s,$j),0,0,'',0,$link);
    }

    function Image($file,$x,$y,$w=0,$h=0,$type='',$link='') {
      //Put an image on the page
      if(!isset($this->images[$file])) {
        //First use of image, get info
        if($type=='') {
            $pos=strrpos($file,'.');
            if(!$pos)
                $this->Error('Image file has no extension and no type was specified: '.$file);
            $type=substr($file,$pos+1);
        }
        $type=strtolower($type);
        $mqr=get_magic_quotes_runtime();
        set_magic_quotes_runtime(0);
        if($type=='jpg' || $type=='jpeg') {
            $info=$this->_parsejpg($file);
        } elseif($type=='png') {
            $info=$this->_parsepng($file);
        } else {
          //Allow for additional formats
          $mtd='_parse'.$type;
          if(!method_exists($this,$mtd))
            $this->Error('Unsupported image type: '.$type);
          $info=$this->$mtd($file);
        }
        set_magic_quotes_runtime($mqr);
        $info['i']=count($this->images)+1;
        $this->images[$file]=$info;
      }
      else
        $info=$this->images[$file];
      //Automatic width and height calculation if needed
      if($w==0 && $h==0) {
          //Put image at 72 dpi
          $w=$info['w']/$this->k;
          $h=$info['h']/$this->k;
      }
      if($w==0)
        $w=$h*$info['w']/$info['h'];
      if($h==0)
        $h=$w*$info['h']/$info['w'];
      $this->_out(sprintf('q %.2f 0 0 %.2f %.2f %.2f cm /I%d Do Q',$w*$this->k,$h*$this->k,$x*$this->k,($this->h-($y+$h))*$this->k,$info['i']));
      if($link)
        $this->Link($x,$y,$w,$h,$link);
    }

    function Ln($h='') {
      //Line feed; default value is last cell height
      $this->x=$this->lMargin;
      if(is_string($h))
        $this->y+=$this->lasth;
      else
        $this->y+=$h;
    }

    function GetX() {
      //Get x position
      return $this->x;
    }

    function SetX($x) {
      //Set x position
      if($x>=0)
        $this->x=$x;
      else
        $this->x=$this->w+$x;
    }

    function GetY() {
      //Get y position
      return $this->y;
    }

    function SetY($y) {
      //Set y position and reset x
      $this->x=$this->lMargin;
      if($y>=0)
        $this->y=$y;
      else
        $this->y=$this->h+$y;
    }

    function SetXY($x,$y) {
      //Set x and y positions
      $this->SetY($y);
      $this->SetX($x);
    }

    function Output($name='',$dest='') {
      //Output PDF to some destination
      global $HTTP_SERVER_VARS;

      //Finish document if necessary
      if($this->state<3)
        $this->Close();
      //Normalize parameters
      if(is_bool($dest))
        $dest=$dest ? 'D' : 'F';
      $dest=strtoupper($dest);
      if($dest=='') {
        //if($name=='') {
        //  $name='doc.pdf';
          $dest='I';
        //} else {
        //  $dest='F';
        //}
      }
      switch($dest) {
        case 'I':
          //Send to standard output
          if(isset($HTTP_SERVER_VARS['SERVER_NAME'])) {
            //We send to a browser
            Header('Content-Type: application/pdf');
            if(headers_sent())
              $this->Error('Some data has already been output to browser, can\'t send PDF file');
            Header('Content-Length: '.strlen($this->buffer));

            //Header('Content-disposition: attachment; filename='.$name);
            Header('Content-disposition: inline; filename="' . $name . '"');
            //Header("Pragma: public"); // <-- hack for SSL-Apache/IE5.x-IE6.x display problem
          }
          echo $this->buffer;
          break;
        case 'D':
          //Download file
          if(isset($HTTP_SERVER_VARS['HTTP_USER_AGENT']) and strpos($HTTP_SERVER_VARS['HTTP_USER_AGENT'],'MSIE'))
            Header('Content-Type: application/force-download');
          else
            Header('Content-Type: application/octet-stream');
            //Header('Content-Type: application/pdf');
          if(headers_sent())
            $this->Error('Some data has already been output to browser, can\'t send PDF file');
          Header('Content-Length: '.strlen($this->buffer));
          Header('Content-Disposition: attachment; filename="' . $name . '"');
          echo $this->buffer;
          break;
        case 'F':
          //Save to local file
          $f=fopen($name,'wb');
          if(!$f)
              $this->Error('Unable to create output file: '.$name);
          fwrite($f,$this->buffer,strlen($this->buffer));
          fclose($f);
          break;
        case 'S':
          //Return as a string
          return $this->buffer;
        default:
          $this->Error('Incorrect output destination: '.$dest);
      }
      return '';
    }

    // DDB - 040703 - added for watermark
    function Rotate($angle,$x=-1,$y=-1) {
      if($x==-1)
        $x=$this->x;
      if($y==-1)
        $y=$this->y;
      if($this->angle!=0)
        $this->_out('Q');
      $this->angle=$angle;
      if($angle!=0) {
        $angle*=M_PI/180;
        $c=cos($angle);
        $s=sin($angle);
        $cx=$x*$this->k;
        $cy=($this->h-$y)*$this->k;
        $this->_out(sprintf('q %.5f %.5f %.5f %.5f %.2f %.2f cm 1 0 0 1 %.2f %.2f cm',$c,$s,-$s,$c,$cx,$cy,-$cx,-$cy));
      }
    }

/*******************************************************************************
*                                                                              *
*                              Protected methods                               *
*                                                                              *
*******************************************************************************/
    function _dochecks() {
      //Check for locale-related bug
      if(1.1==1)
        $this->Error('Don\'t alter the locale before including class file');
      //Check for decimal separator
      if(sprintf('%.1f',1.0)!='1.0')
        setlocale(LC_NUMERIC,'C');
    }

    function _begindoc() {
      //Start document
      $this->state=1;
      $this->_out('%PDF-1.3');
    }

    function _getfontpath() {
      if(!defined('FPDF_FONTPATH') && is_dir(dirname(__FILE__).'/font'))
        define('FPDF_FONTPATH',dirname(__FILE__).'/font/');
      return defined('FPDF_FONTPATH') ? FPDF_FONTPATH : '';
    }

    function _putpages() {
      $nb=$this->page;
      if(!empty($this->AliasNbPages)) {
        //Replace number of pages
        for($n=1;$n<=$nb;$n++)
          $this->pages[$n]=str_replace($this->AliasNbPages,$nb,$this->pages[$n]);
      }
      if($this->DefOrientation=='P') {
        $wPt=$this->fwPt;
        $hPt=$this->fhPt;
      } else {
        $wPt=$this->fhPt;
        $hPt=$this->fwPt;
      }
      $filter=($this->compress) ? '/Filter /FlateDecode ' : '';
      for($n=1;$n<=$nb;$n++) {
        //Page
        $this->_newobj();
        $this->_out('<</Type /Page');
        $this->_out('/Parent 1 0 R');
        if(isset($this->OrientationChanges[$n]))
          $this->_out(sprintf('/MediaBox [0 0 %.2f %.2f]',$hPt,$wPt));
        $this->_out('/Resources 2 0 R');
        if(isset($this->PageLinks[$n])) {
          //Links
          $annots='/Annots [';
          foreach($this->PageLinks[$n] as $pl) {
            $rect=sprintf('%.2f %.2f %.2f %.2f',$pl[0],$pl[1],$pl[0]+$pl[2],$pl[1]-$pl[3]);
            $annots.='<</Type /Annot /Subtype /Link /Rect ['.$rect.'] /Border [0 0 0] ';
            if(is_string($pl[4])) {
              $annots.='/A <</S /URI /URI '.$this->_textstring($pl[4]).'>>>>';
            } else {
              $l=$this->links[$pl[4]];
              $h=isset($this->OrientationChanges[$l[0]]) ? $wPt : $hPt;
              $annots.=sprintf('/Dest [%d 0 R /XYZ 0 %.2f null]>>',1+2*$l[0],$h-$l[1]*$this->k);
            }
          }
          $this->_out($annots.']');
        }
        $this->_out('/Contents '.($this->n+1).' 0 R>>');
        $this->_out('endobj');
        //Page content
        $p=($this->compress) ? gzcompress($this->pages[$n]) : $this->pages[$n];
        $this->_newobj();
        $this->_out('<<'.$filter.'/Length '.strlen($p).'>>');
        $this->_putstream($p);
        $this->_out('endobj');
      }
      //Pages root
      $this->offsets[1]=strlen($this->buffer);
      $this->_out('1 0 obj');
      $this->_out('<</Type /Pages');
      $kids='/Kids [';
      for($i=0;$i<$nb;$i++)
          $kids.=(3+2*$i).' 0 R ';
      $this->_out($kids.']');
      $this->_out('/Count '.$nb);
      $this->_out(sprintf('/MediaBox [0 0 %.2f %.2f]',$wPt,$hPt));
      $this->_out('>>');
      $this->_out('endobj');
    }

    function _putfonts() {
      $nf=$this->n;
      foreach($this->diffs as $diff) {
        //Encodings
        $this->_newobj();
        $this->_out('<</Type /Encoding /BaseEncoding /WinAnsiEncoding /Differences ['.$diff.']>>');
        $this->_out('endobj');
      }
      $mqr=get_magic_quotes_runtime();
      set_magic_quotes_runtime(0);
      foreach($this->FontFiles as $file=>$info) {
        //Font file embedding
        $this->_newobj();
        $this->FontFiles[$file]['n']=$this->n;
        $font='';
        $f=fopen($this->_getfontpath().$file,'rb',1);
        if(!$f)
          $this->Error('Font file not found ' . $file);
        while(!feof($f))
          $font.=fread($f,8192);
        fclose($f);
        $compressed=(substr($file,-2)=='.z');
        if(!$compressed && isset($info['length2'])) {
          $header=(ord($font{0})==128);
          if($header) {
            //Strip first binary header
            $font=substr($font,6);
          }
          if($header && ord($font{$info['length1']})==128) {
            //Strip second binary header
            $font=substr($font,0,$info['length1']).substr($font,$info['length1']+6);
          }
        }
        $this->_out('<</Length '.strlen($font));
        if($compressed)
          $this->_out('/Filter /FlateDecode');
        $this->_out('/Length1 '.$info['length1']);
        if(isset($info['length2']))
          $this->_out('/Length2 '.$info['length2'].' /Length3 0');
        $this->_out('>>');
        $this->_putstream($font);
        $this->_out('endobj');
      }
      set_magic_quotes_runtime($mqr);
      foreach($this->fonts as $k=>$font) {
        //Font objects
        $this->fonts[$k]['n']=$this->n+1;
        $type=$font['type'];
        $name=$font['name'];
        if($type=='core') {
          //Standard font
          $this->_newobj();
          $this->_out('<</Type /Font');
          $this->_out('/BaseFont /'.$name);
          $this->_out('/Subtype /Type1');
          if($name!='Symbol' && $name!='ZapfDingbats')
            $this->_out('/Encoding /WinAnsiEncoding');
          $this->_out('>>');
          $this->_out('endobj');
        } elseif($type=='Type1' || $type=='TrueType') {
          //Additional Type1 or TrueType font
          $this->_newobj();
          $this->_out('<</Type /Font');
          $this->_out('/BaseFont /'.$name);
          $this->_out('/Subtype /'.$type);
          $this->_out('/FirstChar 32 /LastChar 255');
          $this->_out('/Widths '.($this->n+1).' 0 R');
          $this->_out('/FontDescriptor '.($this->n+2).' 0 R');
          if(isset($font['enc']) && $font['enc']) {
            if(isset($font['diff']))
              $this->_out('/Encoding '.($nf+$font['diff']).' 0 R');
            else
              $this->_out('/Encoding /WinAnsiEncoding');
          }
          $this->_out('>>');
          $this->_out('endobj');
          //Widths
          $this->_newobj();
          $cw=&$font['cw'];
          $s='[';
          for($i=32;$i<=255;$i++)
            $s.=$cw[chr($i)].' ';
          $this->_out($s.']');
          $this->_out('endobj');
          //Descriptor
          $this->_newobj();
          $s='<</Type /FontDescriptor /FontName /'.$name;
          foreach($font['desc'] as $k=>$v)
            $s.=' /'.$k.' '.$v;

          if(isset($font['file'])) {
            $file=$font['file'];
          }

          if( isset($file) )
            $s.=' /FontFile'.($type=='Type1' ? '' : '2').' '.$this->FontFiles[$file]['n'].' 0 R';

          $this->_out($s.'>>');
          $this->_out('endobj');

        } else {
          //Allow for additional types
          $mtd='_put'.strtolower($type);
          if(!method_exists($this,$mtd))
            $this->Error('Unsupported font type: '.$type);
          $this->$mtd($font);
        }
      }
    }

    function _putimages() {
      $filter=($this->compress) ? '/Filter /FlateDecode ' : '';
      reset($this->images);
      while(list($file,$info)=each($this->images)) {
        $this->_newobj();
        $this->images[$file]['n']=$this->n;
        $this->_out('<</Type /XObject');
        $this->_out('/Subtype /Image');
        $this->_out('/Width '.$info['w']);
        $this->_out('/Height '.$info['h']);
        if($info['cs']=='Indexed') {
          $this->_out('/ColorSpace [/Indexed /DeviceRGB '.(strlen($info['pal'])/3-1).' '.($this->n+1).' 0 R]');
        } else {
          $this->_out('/ColorSpace /'.$info['cs']);
          if($info['cs']=='DeviceCMYK')
            $this->_out('/Decode [1 0 1 0 1 0 1 0]');
        }
        $this->_out('/BitsPerComponent '.$info['bpc']);
        if(isset($info['f']))
          $this->_out('/Filter /'.$info['f']);
        if(isset($info['parms']))
          $this->_out($info['parms']);
        if(isset($info['trns']) && is_array($info['trns'])) {
          $trns='';
          for($i=0;$i<count($info['trns']);$i++)
            $trns.=$info['trns'][$i].' '.$info['trns'][$i].' ';
          $this->_out('/Mask ['.$trns.']');
        }
        $this->_out('/Length '.strlen($info['data']).'>>');
        $this->_putstream($info['data']);
        unset($this->images[$file]['data']);
        $this->_out('endobj');
        //Palette
        if($info['cs']=='Indexed') {
          $this->_newobj();
          $pal=($this->compress) ? gzcompress($info['pal']) : $info['pal'];
          $this->_out('<<'.$filter.'/Length '.strlen($pal).'>>');
          $this->_putstream($pal);
          $this->_out('endobj');
        }
      }
    }

    function _putxobjectdict() {
      foreach($this->images as $image)
        $this->_out('/I'.$image['i'].' '.$image['n'].' 0 R');
    }

    function _putresourcedict() {
      $this->_out('/ProcSet [/PDF /Text /ImageB /ImageC /ImageI]');
      $this->_out('/Font <<');
      foreach($this->fonts as $font)
        $this->_out('/F'.$font['i'].' '.$font['n'].' 0 R');
      $this->_out('>>');
      $this->_out('/XObject <<');
      $this->_putxobjectdict();
      $this->_out('>>');
    }

    function _putresources() {
      $this->_putfonts();
      $this->_putimages();
      //Resource dictionary
      $this->offsets[2]=strlen($this->buffer);
      $this->_out('2 0 obj');
      $this->_out('<<');
      $this->_putresourcedict();
      $this->_out('>>');
      $this->_out('endobj');
    }

    function _putinfo() {
      $this->_out('/Producer '.$this->_textstring('FPDF '.FPDF_VERSION));
      if(!empty($this->title))
        $this->_out('/Title '.$this->_textstring($this->title));
      if(!empty($this->subject))
        $this->_out('/Subject '.$this->_textstring($this->subject));
      if(!empty($this->author))
        $this->_out('/Author '.$this->_textstring($this->author));
      if(!empty($this->keywords))
        $this->_out('/Keywords '.$this->_textstring($this->keywords));
      if(!empty($this->creator))
        $this->_out('/Creator '.$this->_textstring($this->creator));
      $this->_out('/CreationDate '.$this->_textstring('D:'.date('YmdHis')));
    }
 
    function _putcatalog() {
      $this->_out('/Type /Catalog');
      $this->_out('/Pages 1 0 R');
      if($this->ZoomMode=='fullpage')
        $this->_out('/OpenAction [3 0 R /Fit]');
      elseif($this->ZoomMode=='fullwidth')
        $this->_out('/OpenAction [3 0 R /FitH null]');
      elseif($this->ZoomMode=='real')
        $this->_out('/OpenAction [3 0 R /XYZ null null 1]');
      elseif(!is_string($this->ZoomMode))
        $this->_out('/OpenAction [3 0 R /XYZ null null '.($this->ZoomMode/100).']');
      if($this->LayoutMode=='single')
        $this->_out('/PageLayout /SinglePage');
      elseif($this->LayoutMode=='continuous')
        $this->_out('/PageLayout /OneColumn');
      elseif($this->LayoutMode=='two')
        $this->_out('/PageLayout /TwoColumnLeft');
    }

    function _putheader() {
      $this->_out('%PDF-'.$this->PDFVersion);
    }

    function _puttrailer() {
      $this->_out('/Size '.($this->n+1));
      $this->_out('/Root '.$this->n.' 0 R');
      $this->_out('/Info '.($this->n-1).' 0 R');
    }

    function _enddoc() {
      $this->_putheader();
      $this->_putpages();
      $this->_putresources();
      //Info
      $this->_newobj();
      $this->_out('<<');
      $this->_putinfo();
      $this->_out('>>');
      $this->_out('endobj');
      //Catalog
      $this->_newobj();
      $this->_out('<<');
      $this->_putcatalog();
      $this->_out('>>');
      $this->_out('endobj');
      //Cross-ref
      $o=strlen($this->buffer);
      $this->_out('xref');
      $this->_out('0 '.($this->n+1));
      $this->_out('0000000000 65535 f ');
      for($i=1;$i<=$this->n;$i++)
        $this->_out(sprintf('%010d 00000 n ',$this->offsets[$i]));
      //Trailer
      $this->_out('trailer');
      $this->_out('<<');
      $this->_puttrailer();
      $this->_out('>>');
      $this->_out('startxref');
      $this->_out($o);
      $this->_out('%%EOF');
      $this->state=3;
    }

    function _beginpage($orientation) {
      $this->page++;
      $this->pages[$this->page]='';
      $this->state=2;
      $this->x=$this->lMargin;
      $this->y=$this->tMargin;
      $this->FontFamily='';
      //Page orientation
      if(!$orientation) {
        $orientation=$this->DefOrientation;
      } else {
        $orientation=strtoupper($orientation{0});
        if($orientation!=$this->DefOrientation)
          $this->OrientationChanges[$this->page]=true;
      }
      if($orientation!=$this->CurOrientation) {
        //Change orientation
        if($orientation=='P') {
            $this->wPt=$this->fwPt;
            $this->hPt=$this->fhPt;
            $this->w=$this->fw;
            $this->h=$this->fh;
        } else {
            $this->wPt=$this->fhPt;
            $this->hPt=$this->fwPt;
            $this->w=$this->fh;
            $this->h=$this->fw;
        }
        $this->PageBreakTrigger=$this->h-$this->bMargin;
        $this->CurOrientation=$orientation;
      }
    }

    function _endpage() {
      //End of page contents
      $this->state=1;
    }

    function _newobj() {
      //Begin a new object
      $this->n++;
      $this->offsets[$this->n]=strlen($this->buffer);
      $this->_out($this->n.' 0 obj');
    }

    function _dounderline($x,$y,$txt) {
      //Underline text
      $up=$this->CurrentFont['up'];
      $ut=$this->CurrentFont['ut'];
      $w=$this->GetStringWidth($txt)+$this->ws*substr_count($txt,' ');
      return sprintf('%.2f %.2f %.2f %.2f re f',$x*$this->k,($this->h-($y-$up/1000*$this->FontSize))*$this->k,$w*$this->k,-$ut/1000*$this->FontSizePt);
    }

    function _parsejpg($file) {
      //Extract info from a JPEG file
      $file = str_replace(' ', '%20', $file);
      $a=GetImageSize($file);
      if(!$a)
        $this->Error('Missing or incorrect image file: '.$file);
      if($a[2]!=2)
        $this->Error('Not a JPEG file: '.$file);
      if(!isset($a['channels']) || $a['channels']==3)
        $colspace='DeviceRGB';
      elseif($a['channels']==4)
        $colspace='DeviceCMYK';
      else
        $colspace='DeviceGray';
      $bpc=isset($a['bits']) ? $a['bits'] : 8;
      //Read whole file
      $f=fopen($file,'rb');
      $data='';
      while(!feof($f))
        $data.=fread($f,4096);
      fclose($f);
      return array('w'=>$a[0],'h'=>$a[1],'cs'=>$colspace,'bpc'=>$bpc,'f'=>'DCTDecode','data'=>$data);
    }

    function _parsepng($file) {
      //Extract info from a PNG file
      $f=fopen($file,'rb');
      if(!$f)
        $this->Error('Can\'t open image file: '.$file);
      //Check signature
      if(fread($f,8)!=chr(137).'PNG'.chr(13).chr(10).chr(26).chr(10))
        $this->Error('Not a PNG file: '.$file);
      //Read header chunk
      fread($f,4);
      if(fread($f,4)!='IHDR')
        $this->Error('Incorrect PNG file: '.$file);
      $w=$this->_freadint($f);
      $h=$this->_freadint($f);
      $bpc=ord(fread($f,1));
      if($bpc>8)
        $this->Error('16-bit depth not supported: '.$file);
      $ct=ord(fread($f,1));
      if($ct==0)
        $colspace='DeviceGray';
      elseif($ct==2)
        $colspace='DeviceRGB';
      elseif($ct==3)
        $colspace='Indexed';
      else
        $this->Error('Alpha channel not supported: '.$file);
      if(ord(fread($f,1))!=0)
        $this->Error('Unknown compression method: '.$file);
      if(ord(fread($f,1))!=0)
        $this->Error('Unknown filter method: '.$file);
      if(ord(fread($f,1))!=0)
        $this->Error('Interlacing not supported: '.$file);
      fread($f,4);
      $parms='/DecodeParms <</Predictor 15 /Colors '.($ct==2 ? 3 : 1).' /BitsPerComponent '.$bpc.' /Columns '.$w.'>>';
      //Scan chunks looking for palette, transparency and image data
      $pal='';
      $trns='';
      $data='';
      do {
        $n=$this->_freadint($f);
        $type=fread($f,4);
        if($type=='PLTE') {
          //Read palette
          $pal=fread($f,$n);
          fread($f,4);
        } elseif($type=='tRNS') {
          //Read transparency info
          $t=fread($f,$n);
          if($ct==0) {
            $trns=array(ord(substr($t,1,1)));
          } elseif($ct==2) {
            $trns=array(ord(substr($t,1,1)),ord(substr($t,3,1)),ord(substr($t,5,1)));
          } else {
            $pos=strpos($t,chr(0));
            if($pos!==false)
              $trns=array($pos);
          }
          fread($f,4);
        } elseif($type=='IDAT') {
          //Read image data block
          $data.=fread($f,$n);
          fread($f,4);
        }
        elseif($type=='IEND')
          break;
        else
          fread($f,$n+4);
      } while($n);

      if($colspace=='Indexed' && empty($pal))
        $this->Error('Missing palette in '.$file);
      fclose($f);
      return array('w'=>$w,'h'=>$h,'cs'=>$colspace,'bpc'=>$bpc,'f'=>'FlateDecode','parms'=>$parms,'pal'=>$pal,'trns'=>$trns,'data'=>$data);
    }

    // DDB - 040703 - added for GIF support
    function _parsegif($file) { 
      //Function by J�r�me Fenal
      require_once('gif.php'); //GIF class in pure PHP from Yamasoft (http://www.yamasoft.com/php-gif.zip)

      $h=0;
      $w=0;
      $gif=new CGIF();

      if (!$gif->loadFile($file, 0))
          $this->Error("GIF parser: unable to open file $file");

      if($gif->m_img->m_gih->m_bLocalClr) {
          $nColors = $gif->m_img->m_gih->m_nTableSize;
          $pal = $gif->m_img->m_gih->m_colorTable->toString();
          if($bgColor != -1) {
              $bgColor = $this->m_img->m_gih->m_colorTable->colorIndex($bgColor);
          }
          $colspace='Indexed';
      } elseif($gif->m_gfh->m_bGlobalClr) {
          $nColors = $gif->m_gfh->m_nTableSize;
          $pal = $gif->m_gfh->m_colorTable->toString();
          if($bgColor != -1) {
              $bgColor = $gif->m_gfh->m_colorTable->colorIndex($bgColor);
          }
          $colspace='Indexed';
      } else {
          $nColors = 0;
          $bgColor = -1;
          $colspace='DeviceGray';
          $pal='';
      }

      $trns='';
      if($gif->m_img->m_bTrans && ($nColors > 0)) {
          $trns=array($gif->m_img->m_nTrans);
      }

      $data=$gif->m_img->m_data;
      $w=$gif->m_gfh->m_nWidth;
      $h=$gif->m_gfh->m_nHeight;

      if($colspace=='Indexed' and empty($pal))
          $this->Error('Missing palette in '.$file);

      if ($this->compress) {
          $data=gzcompress($data);
          return array( 'w'=>$w, 'h'=>$h, 'cs'=>$colspace, 'bpc'=>8, 'f'=>'FlateDecode', 'pal'=>$pal, 'trns'=>$trns, 'data'=>$data);
      } else {
          return array( 'w'=>$w, 'h'=>$h, 'cs'=>$colspace, 'bpc'=>8, 'pal'=>$pal, 'trns'=>$trns, 'data'=>$data);
      } 
    }

    function _freadint($f) {
      //Read a 4-byte integer from file
      $a=unpack('Ni',fread($f,4));
      return $a['i'];
    }

    function _textstring($s) {
      //Format a text string
      return '('.$this->_escape($s).')';
    }

    function _escape($s) {
      //Add \ before \, ( and )
      return str_replace(')','\\)',str_replace('(','\\(',str_replace('\\','\\\\',$s)));
    }

    function _putstream($s) {
      $this->_out('stream');
      $this->_out($s);
      $this->_out('endstream');
    }

    function _out($s) {
      //Add a line to the document
      if($this->state==2)
        $this->pages[$this->page].=$s."\n";
      else
        $this->buffer.=$s."\n";
    }



      /**
      * Adds unicode fonts.<br>
      * Based on PDF Reference 1.3 (section 5)
      * @access protected
      * @author Nicola Asuni
      * @since 1.52.0.TC005 (2005-01-05)
      */
    function _puttruetypeunicode($font) {
      // Type0 Font
      // A composite font—a font composed of other fonts, organized hierarchically
      $this->_newobj();
      $this->_out('<</Type /Font');
      $this->_out('/Subtype /Type0');
      $this->_out('/BaseFont /'.$font['name'].'');
      $this->_out('/Encoding /Identity-H'); //The horizontal identity mapping for 2-byte CIDs; may be used with CIDFonts using any Registry, Ordering, and Supplement values.
      $this->_out('/DescendantFonts ['.($this->n + 1).' 0 R]');
      $this->_out('>>');
      $this->_out('endobj');
      
      // CIDFontType2
      // A CIDFont whose glyph descriptions are based on TrueType font technology
      $this->_newobj();
      $this->_out('<</Type /Font');
      $this->_out('/Subtype /CIDFontType2');
      $this->_out('/BaseFont /'.$font['name'].'');
      $this->_out('/CIDSystemInfo '.($this->n + 1).' 0 R'); 
      $this->_out('/FontDescriptor '.($this->n + 2).' 0 R');
      if (isset($font['desc']['MissingWidth'])){
        $this->_out('/DW '.$font['desc']['MissingWidth'].''); // The default width for glyphs in the CIDFont MissingWidth
      }
      $w = "";
      foreach ($font['cw'] as $cid => $width) {
        $w .= ''.$cid.' ['.$width.'] '; // define a specific width for each individual CID
      }
      $this->_out('/W ['.$w.']'); // A description of the widths for the glyphs in the CIDFont
      $this->_out('/CIDToGIDMap '.($this->n + 3).' 0 R');
      $this->_out('>>');
      $this->_out('endobj');
      
      // CIDSystemInfo dictionary
      // A dictionary containing entries that define the character collectionof the CIDFont.
      $this->_newobj();
      $this->_out('<</Registry (Adobe)'); // A string identifying an issuer of character collections
      $this->_out('/Ordering (UCS)'); // A string that uniquely names a character collection issued by a specific registry
      $this->_out('/Supplement 0'); // The supplement number of the character collection.
      $this->_out('>>');
      $this->_out('endobj');
      
      // Font descriptor
      // A font descriptor describing the CIDFont’s default metrics other than its glyph widths
      $this->_newobj();
      $this->_out('<</Type /FontDescriptor');
      $this->_out('/FontName /'.$font['name']);
      foreach ($font['desc'] as $key => $value) {
        $this->_out('/'.$key.' '.$value);
      }
      if ($font['file']) {
        // A stream containing a TrueType font program
        $this->_out('/FontFile2 '.$this->FontFiles[$font['file']]['n'].' 0 R');
      }
      $this->_out('>>');
      $this->_out('endobj');

      // Embed CIDToGIDMap
      // A specification of the mapping from CIDs to glyph indices
      $this->_newobj();
      $ctgfile = $this->_getfontpath().$font['ctg'];
      if(!file_exists($ctgfile)) {
        $this->Error('Font file not found: '.$ctgfile);
      }
      $size = filesize($ctgfile);
      $this->_out('<</Length '.$size.'');
      if(substr($ctgfile, -2) == '.z') { // check file extension
        /* Decompresses data encoded using the public-domain 
        zlib/deflate compression method, reproducing the 
        original text or binary data */
        $this->_out('/Filter /FlateDecode');
      }
      $this->_out('>>');
      $this->_putstream(file_get_contents($ctgfile));
      $this->_out('endobj');
    }

     /**
     * Converts UTF-8 strings to codepoints array.<br>
     * Invalid byte sequences will be replaced with 0xFFFD (replacement character)<br>
     * Based on: http://www.faqs.org/rfcs/rfc3629.html
     * <pre>
     *    Char. number range  |        UTF-8 octet sequence
     *       (hexadecimal)    |              (binary)
     *    --------------------+-----------------------------------------------
     *    0000 0000-0000 007F | 0xxxxxxx
     *    0000 0080-0000 07FF | 110xxxxx 10xxxxxx
     *    0000 0800-0000 FFFF | 1110xxxx 10xxxxxx 10xxxxxx
     *    0001 0000-0010 FFFF | 11110xxx 10xxxxxx 10xxxxxx 10xxxxxx
     *    ---------------------------------------------------------------------
     *
     *   ABFN notation:
     *   ---------------------------------------------------------------------
     *   UTF8-octets = *( UTF8-char )
     *   UTF8-char   = UTF8-1 / UTF8-2 / UTF8-3 / UTF8-4
     *   UTF8-1      = %x00-7F
     *   UTF8-2      = %xC2-DF UTF8-tail
     *
     *   UTF8-3      = %xE0 %xA0-BF UTF8-tail / %xE1-EC 2( UTF8-tail ) /
     *                 %xED %x80-9F UTF8-tail / %xEE-EF 2( UTF8-tail )
     *   UTF8-4      = %xF0 %x90-BF 2( UTF8-tail ) / %xF1-F3 3( UTF8-tail ) /
     *                 %xF4 %x80-8F 2( UTF8-tail )
     *   UTF8-tail   = %x80-BF
     *   ---------------------------------------------------------------------
     * </pre>
     * @param string $str string to process.
     * @return array containing codepoints (UTF-8 characters values)
     * @access protected
     * @author Nicola Asuni
     * @since 1.53.0.TC005 (2005-01-05)
     */
    function UTF8StringToArray($str) {
      if(!$this->isunicode) {
        return $str; // string is not in unicode
      }
      $unicode = array(); // array containing unicode values
      $bytes  = array(); // array containing single character byte sequences
      $numbytes  = 1; // number of octetc needed to represent the UTF-8 character
      
      $str .= ""; // force $str to be a string
      $length = strlen($str);
      
      for($i = 0; $i < $length; $i++) {
        $char = ord($str{$i}); // get one string character at time
        if(count($bytes) == 0) { // get starting octect
          if ($char <= 0x7F) {
            $unicode[] = $char; // use the character "as is" because is ASCII
          } elseif (($char >> 0x05) == 0x06) { // 2 bytes character (0x06 = 110 BIN)
            $bytes[] = ($char - 0xC0) << 0x06; 
            $numbytes = 2;
          } elseif (($char >> 0x04) == 0x0E) { // 3 bytes character (0x0E = 1110 BIN)
            $bytes[] = ($char - 0xE0) << 0x0C; 
            $numbytes = 3;
          } elseif (($char >> 0x03) == 0x1E) { // 4 bytes character (0x1E = 11110 BIN)
            $bytes[] = ($char - 0xF0) << 0x12; 
            $numbytes = 4;
          } else {
            // use replacement character for other invalid sequences
            $unicode[] = 0xFFFD;
            $bytes = array();
            $numbytes = 1;
          }
        } elseif (($char >> 0x06) == 0x02) { // bytes 2, 3 and 4 must start with 0x02 = 10 BIN
          $bytes[] = $char - 0x80;
          if (count($bytes) == $numbytes) {
            // compose UTF-8 bytes to a single unicode value
            $char = $bytes[0];
            for($j = 1; $j < $numbytes; $j++) {
              $char += ($bytes[$j] << (($numbytes - $j - 1) * 0x06));
            }
            if ((($char >= 0xD800) AND ($char <= 0xDFFF)) OR ($char >= 0x10FFFF)) {
              /* The definition of UTF-8 prohibits encoding character numbers between
              U+D800 and U+DFFF, which are reserved for use with the UTF-16
              encoding form (as surrogate pairs) and do not directly represent
              characters. */
              $unicode[] = 0xFFFD; // use replacement character
            }
            else {
              $unicode[] = $char; // add char to array
            }
            // reset data for next char
            $bytes = array(); 
            $numbytes = 1;
          }
        } else {
          // use replacement character for other invalid sequences
          $unicode[] = 0xFFFD;
          $bytes = array();
          $numbytes = 1;
        }
      }
      return $unicode;
    }
    
    /**
     * Converts UTF-8 strings to UTF16-BE.<br>
     * Based on: http://www.faqs.org/rfcs/rfc2781.html
     * <pre>
     *   Encoding UTF-16:
     * 
     *   Encoding of a single character from an ISO 10646 character value to
     *    UTF-16 proceeds as follows. Let U be the character number, no greater
     *    than 0x10FFFF.
     * 
     *    1) If U < 0x10000, encode U as a 16-bit unsigned integer and
     *       terminate.
     * 
     *    2) Let U' = U - 0x10000. Because U is less than or equal to 0x10FFFF,
     *       U' must be less than or equal to 0xFFFFF. That is, U' can be
     *       represented in 20 bits.
     * 
     *    3) Initialize two 16-bit unsigned integers, W1 and W2, to 0xD800 and
     *       0xDC00, respectively. These integers each have 10 bits free to
     *       encode the character value, for a total of 20 bits.
     * 
     *    4) Assign the 10 high-order bits of the 20-bit U' to the 10 low-order
     *       bits of W1 and the 10 low-order bits of U' to the 10 low-order
     *       bits of W2. Terminate.
     * 
     *    Graphically, steps 2 through 4 look like:
     *    U' = yyyyyyyyyyxxxxxxxxxx
     *    W1 = 110110yyyyyyyyyy
     *    W2 = 110111xxxxxxxxxx
     * </pre>
     * @param string $str string to process.
     * @param boolean $setbom if true set the Byte Order Mark (BOM = 0xFEFF)
     * @return string
     * @access protected
     * @author Nicola Asuni
     * @since 1.53.0.TC005 (2005-01-05)
     * @uses UTF8StringToArray
     */
    function UTF8ToUTF16BE($str, $setbom=true) {
      if(!$this->isunicode) {
        return $str; // string is not in unicode
      }
      $outstr = ""; // string to be returned
      $unicode = $this->UTF8StringToArray($str); // array containing UTF-8 unicode values
      $numitems = count($unicode);
      
      if ($setbom) {
          $outstr .= "\xFE\xFF"; // Byte Order Mark (BOM)
      }
      foreach($unicode as $char) {
        if($char == 0xFFFD) {
          $outstr .= "\xFF\xFD"; // replacement character
        } elseif ($char < 0x10000) {
          $outstr .= chr($char >> 0x08);
          $outstr .= chr($char & 0xFF);
        } else {
          $char -= 0x10000;
          $w1 = 0xD800 | ($char >> 0x10);
          $w2 = 0xDC00 | ($char & 0x3FF); 
          $outstr .= chr($w1 >> 0x08);
          $outstr .= chr($w1 & 0xFF);
          $outstr .= chr($w2 >> 0x08);
          $outstr .= chr($w2 & 0xFF);
        }
      }
      return $outstr;
    }
    // ====================================================

    /**
     * Set header font.
     * @param array $font font
     * @since 1.1
     */
    function setHeaderFont($font) {
      $this->header_font = $font;
    }
    
    /**
     * Set footer font.
     * @param array $font font
     * @since 1.1
     */
    function setFooterFont($font) {
      $this->footer_font = $font;
    }
    
    /**
     * Set language array.
     * @param array $language
     * @since 1.1
     */
    function setLanguageArray($language) {
      $this->l = $language;
    }

    /**
     * Returns the PDF data.
     */
    function getPDFData() {
      if($this->state < 3) {
        $this->Close();
      }
      return $this->buffer;
    }


    // --- HTML PARSER FUNCTIONS ---
    
    /**
     * Allows to preserve some HTML formatting.<br />
     * Supports: h1, h2, h3, h4, h5, h6, b, u, i, a, img, p, br, strong, em, font, blockquote, li, ul, ol, hr, td, th, tr, table, sup, sub, small
     * @param string $html text to display
     * @param boolean $ln if true add a new line after text (default = true)
     */
    function writeHTML($html, $ln=true) {
      $html=strip_tags($html,"<h1><h2><h3><h4><h5><h6><b><u><i><a><img><p><br><strong><em><font><blockquote><li><ul><ol><hr><td><th><tr><table><sup><sub><small>"); //remove all unsupported tags
      //replace carriage returns, newlines and tabs
      $repTable = array("\t" => " ", "\n" => " ", "\r" => " ", "\0" => " ", "\x0B" => " "); 
      $html = strtr($html, $repTable);
      $pattern = '/(<[^>]+>)/Uu';
      $a = preg_split($pattern, $html, -1, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY); //explodes the string
      
      if (empty($this->lasth)) {
        //set row height
        $this->lasth = $this->FontSize * K_CELL_HEIGHT_RATIO; 
      }
      
      foreach($a as $key=>$element) {
        if (!preg_match($pattern, $element)) {
          //Text
          if($this->HREF) {
            $this->addHtmlLink($this->HREF, $element);
          } elseif($this->tdbegin) {
            if((strlen(trim($element)) > 0) && ($element != "&nbsp;")) {
              $this->Cell($this->tdwidth, $this->tdheight, $this->unhtmlentities($element), $this->tableborder, '', $this->tdalign, $this->tdbgcolor);
            } elseif($element == "&nbsp;") {
              $this->Cell($this->tdwidth, $this->tdheight, '', $this->tableborder, '', $this->tdalign, $this->tdbgcolor);
            }
          }
          else {
            $this->Write($this->lasth, stripslashes($this->unhtmlentities($element)));
          }
        } else {
          $element = substr($element, 1, -1);
          //Tag
          if($element{0}=='/') {
            $this->closedHTMLTagHandler(strtolower(substr($element, 1)));
          } else {
            //Extract attributes
            $a2 = explode(' ',$element);
            $tag = strtolower(array_shift($a2));
            $attr = array();
            foreach($a2 as $v) {
              if (preg_match('/^([^=]*)=["\']?([^"\']*)["\']?$/', $v, $a3)) {
                $attr[strtolower($a3[1])] = $a3[2];
              }
            }
            $this->openHTMLTagHandler($tag, $attr);
          }
        }
      }
      if ($ln) {
        $this->Ln($this->lasth);
      }
    }
    
/*
// Prints a cell (rectangular area) with optional borders, background color and html text string. The upper-left corner of the cell corresponds to the current position. The text can be aligned or centered. After the call, the current position moves to the right or to the next line. It is possible to put a link on the text.<br />
// If automatic page breaking is enabled and the cell goes beyond the limit, a page break is done before outputting.
// @param float $w Cell width. If 0, the cell extends up to the right margin.
// @param float $h Cell minimum height. The cell extends automatically if needed.
// @param float $x upper-left corner X coordinate
// @param float $y upper-left corner Y coordinate
// @param string $html html text to print. Default value: empty string.
// @param mixed $border Indicates if borders must be drawn around the cell. The value can be either a number:<ul><li>0: no border (default)</li><li>1: frame</li></ul>or a string containing some or all of the following characters (in any order):<ul><li>L: left</li><li>T: top</li><li>R: right</li><li>B: bottom</li></ul>
// @param int $ln Indicates where the current position should go after the call. Possible values are:<ul><li>0: to the right</li><li>1: to the beginning of the next line</li><li>2: below</li></ul>
// Putting 1 is equivalent to putting 0 and calling Ln() just after. Default value: 0.
// @param int $fill Indicates if the cell background must be painted (1) or transparent (0). Default value: 0. NOTE: this parameter has not been implemented.
// @see Cell()
*/
    function writeHTMLCell($w, $h, $x=false, $y=false, $html='', $border=0, $ln=0, $fill=0) {
        
      $fill = 0; // disable fill function
      
      if (empty($this->lasth)) {
        //set row height
        $this->lasth = $this->FontSize * K_CELL_HEIGHT_RATIO; 
      }
      
      if($x === false) {
        $x = $this->GetX();
      }
      if($y === false ) {
        $y = $this->GetY();
      }
      
      // get current page number
      $pagenum = $this->page;
      
      $this->SetX($x);
      $this->SetY($y);
              
      if(empty($w)) {
        $w = $this->fw - $x - $this->rMargin;
      }
      
      // store original margin values
      $lMargin = $this->lMargin;
      $rMargin = $this->rMargin;
      
      // set new margin values
      $this->SetLeftMargin($x);
      $this->SetRightMargin($this->fw - $x - $w);
              
      // calculate remaining vertical space on page
      $restspace = $this->getPageHeight() - $this->GetY() - $this->getBreakMargin();
      
      $this->writeHTML($html); // write html text
      
      // check if a new page has been created
      if ($this->page > $pagenum) {
        $currentY =  $this->GetY();
        
        // design a cell around the text on first page
        $currentpage = $this->page;
        $this->page = $pagenum;
        $this->SetY($this->getPageHeight() - $restspace - $this->getBreakMargin());
        $h = $restspace - 1;
        $this->Cell($w, $h, "", $border, $ln, 'L', $fill);
        
        // design a cell around the text on last page
        $this->page = $currentpage;
        $h = $currentY - $this->tMargin;
        $this->SetY($this->tMargin); // put cursor at the beginning of text
        $this->Cell($w, $h, "", $border, $ln, 'L', $fill);
      } else {
        $h = max($h, ($this->GetY() - $y));
        $this->SetY($y); // put cursor at the beginning of text
        // design a cell around the text
        $this->Cell($w, $h, "", $border, $ln, 'L', $fill);
      }
      
      // restore original margin values
      $this->SetLeftMargin($lMargin);
      $this->SetRightMargin($rMargin);
      
      if ($ln) {
        $this->Ln(0);
      }
    }
    
/*
// Process opening tags.
// @param string $tag tag name (in uppercase)
// @param string $attr tag attribute (in uppercase)
// @access private
*/
    function openHTMLTagHandler($tag, $attr) {
      //Opening tag
      switch($tag) {
        case 'table': {
          if ((isset($attr['border'])) AND ($attr['border'] != '')) {
              $this->tableborder = $attr['border'];
          }
          else {
              $this->tableborder = 0;
          }
          break;
        }
        case 'tr': {
          break;
        }
        case 'td':
        case 'th': {
          if ((isset($attr['width'])) AND ($attr['width'] != '')) {
            $this->tdwidth = ($attr['width']/4);
          }
          else {
            $this->tdwidth = (($this->w - $this->lMargin - $this->rMargin) / $this->default_table_columns);
          }
          if ((isset($attr['height'])) AND ($attr['height'] != '')) {
            $this->tdheight=($attr['height'] / $this->k);
          }
          else {
            $this->tdheight = $this->lasth;
          }
          if ((isset($attr['align'])) AND ($attr['align'] != '')) {
            switch ($attr['align']) {
              case 'center': {
                $this->tdalign = "C";
                break;
              }
              case 'right': {
                $this->tdalign = "R";
                break;
              }
              default:
              case 'left': {
                $this->tdalign = "L";
                break;
              }
            }
          }
          if ((isset($attr['bgcolor'])) AND ($attr['bgcolor'] != '')) {
            $coul = $this->convertColorHexToDec($attr['bgcolor']);
            $this->SetFillColor($coul['R'], $coul['G'], $coul['B']);
            $this->tdbgcolor=true;
          }
          $this->tdbegin=true;
          break;
        }
        case 'hr': {
          $this->Ln();
          if ((isset($attr['width'])) AND ($attr['width'] != '')) {
              $hrWidth = $attr['width'];
          } else {
            $hrWidth = $this->w - $this->lMargin - $this->rMargin;
          }
          $x = $this->GetX();
          $y = $this->GetY();
          $this->SetLineWidth(0.2);
          $this->Line($x, $y, $x + $hrWidth, $y);
          $this->SetLineWidth(0.2);
          $this->Ln();
          break;
        }
        case 'strong': {
          $this->setStyle('b', true);
          break;
        }
        case 'em': {
          $this->setStyle('i', true);
          break;
        }
        case 'b':
        case 'i':
        case 'u': {
          $this->setStyle($tag, true);
          break;
        }
        case 'a': {
          $this->HREF = $attr['href'];
          break;
        }
        case 'img': {
          if(isset($attr['src'])) {
            // replace relative path with real server path
            $attr['src'] = str_replace(K_PATH_URL_CACHE, K_PATH_CACHE, $attr['src']);
            if(!isset($attr['width'])) {
              $attr['width'] = 0;
            }
            if(!isset($attr['height'])) {
              $attr['height'] = 0;
            }
              
            $this->Image($attr['src'], $this->GetX(),$this->GetY(), $this->pixelsToMillimeters($attr['width']), $this->pixelsToMillimeters($attr['height']));
            //$this->SetX($this->img_rb_x);
            $this->SetY($this->img_rb_y);              
          }
          break;
        }
        case 'ul': {
          $this->listordered = false;
          $this->listcount = 0;
          break;
        }
        case 'ol': {
          $this->listordered = true;
          $this->listcount = 0;
          break;
        }
        case 'li': {
          $this->Ln();
          if ($this->listordered) {
            $this->lispacer = "    ".(++$this->listcount).". ";
          }
          else {
            //unordered list simbol
            $this->lispacer = "    -  ";
          }
          $this->Write($this->lasth, $this->lispacer);
          break;
        }
        case 'tr':
        case 'blockquote':
        case 'br': {
          $this->Ln();
          if(strlen($this->lispacer) > 0) {
            $this->x += $this->GetStringWidth($this->lispacer);
          }
          break;
        }
        case 'p': {
          $this->Ln();
          $this->Ln();
          break;
        }
        case 'sup': {
          $currentFontSize = $this->FontSize;
          $this->tempfontsize = $this->FontSizePt;
          $this->SetFontSize($this->FontSizePt * K_SMALL_RATIO);
          $this->SetXY($this->GetX(), $this->GetY() - (($currentFontSize - $this->FontSize)*(K_SMALL_RATIO)));
          break;
        }
        case 'sub': {
          $currentFontSize = $this->FontSize;
          $this->tempfontsize = $this->FontSizePt;
          $this->SetFontSize($this->FontSizePt * K_SMALL_RATIO);
          $this->SetXY($this->GetX(), $this->GetY() + (($currentFontSize - $this->FontSize)*(K_SMALL_RATIO)));
          break;
        }
        case 'small': {
          $currentFontSize = $this->FontSize;
          $this->tempfontsize = $this->FontSizePt;
          $this->SetFontSize($this->FontSizePt * K_SMALL_RATIO);
          $this->SetXY($this->GetX(), $this->GetY() + (($currentFontSize - $this->FontSize)/3));
          break;
        }
        case 'font': {
          if (isset($attr['color']) AND $attr['color']!='') {
            $coul = $this->convertColorHexToDec($attr['color']);
            $this->SetTextColor($coul['R'],$coul['G'],$coul['B']);
            $this->issetcolor=true;
          }
          if (isset($attr['face']) and in_array(strtolower($attr['face']), $this->fontlist)) {
            $this->SetFont(strtolower($attr['FACE']));
            $this->issetfont=true;
          }
          if (isset($attr['size'])) {
            $headsize = intval($attr['size']);
          } else {
            $headsize = 0;
          }
          $currentFontSize = $this->FontSize;
          $this->tempfontsize = $this->FontSizePt;
          $this->SetFontSize($this->FontSizePt + $headsize);
          $this->lasth = $this->FontSize * K_CELL_HEIGHT_RATIO;
          break;
        }
        case 'h1': 
        case 'h2': 
        case 'h3': 
        case 'h4': 
        case 'h5': 
        case 'h6': {
          $headsize = (4 - substr($tag, 1)) * 2;
          $currentFontSize = $this->FontSize;
          $this->tempfontsize = $this->FontSizePt;
          $this->SetFontSize($this->FontSizePt + $headsize);
          $this->setStyle('b', true);
          $this->lasth = $this->FontSize * K_CELL_HEIGHT_RATIO;
          break;
        }
      }
    }
    
/*
// Process closing tags.
// @param string $tag tag name (in uppercase)
// @access private
*/
    function closedHTMLTagHandler($tag) {
      //Closing tag
      switch($tag) {
        case 'td':
        case 'th': {
          $this->tdbegin = false;
          $this->tdwidth = 0;
          $this->tdheight = 0;
          $this->tdalign = "L";
          $this->tdbgcolor = false;
          break;
        }
        case 'tr': {
          $this->Ln();
          break;
        }
        case 'table': {
          $this->tableborder=0;
          break;
        }
        case 'strong': {
          $tag = 'b';
          break;
        }
        case 'em': {
          $tag = 'i';
          break;
        }
        case 'b':
        case 'i':
        case 'u': {
          $this->setStyle($tag, false);
          break;
        }
        case 'a': {
          $this->HREF = '';
          break;
        }
        case 'sup': {
          $currentFontSize = $this->FontSize;
          $this->SetFontSize($this->tempfontsize);
          $this->tempfontsize = $this->FontSizePt;
          $this->SetXY($this->GetX(), $this->GetY() - (($currentFontSize - $this->FontSize)*(K_SMALL_RATIO)));
          break;
        }
        case 'sub': {
          $currentFontSize = $this->FontSize;
          $this->SetFontSize($this->tempfontsize);
          $this->tempfontsize = $this->FontSizePt;
          $this->SetXY($this->GetX(), $this->GetY() + (($currentFontSize - $this->FontSize)*(K_SMALL_RATIO)));
          break;
        }
        case 'small': {
          $currentFontSize = $this->FontSize;
          $this->SetFontSize($this->tempfontsize);
          $this->tempfontsize = $this->FontSizePt;
          $this->SetXY($this->GetX(), $this->GetY() - (($this->FontSize - $currentFontSize)/3));
          break;
        }
        case 'font': {
          if ($this->issetcolor == true) {
            $this->SetTextColor(0);
          }
          if ($this->issetfont) {
            $this->SetFont('arial');
            $this->issetfont = false;
          }
          $currentFontSize = $this->FontSize;
          $this->SetFontSize($this->tempfontsize);
          $this->tempfontsize = $this->FontSizePt;
          $this->lasth = $this->FontSize * K_CELL_HEIGHT_RATIO;
          break;
        }
        case 'ul': {
          $this->Ln();
          break;
        }
        case 'ol': {
          $this->Ln();
          break;
        }
        case 'li': {
          $this->lispacer = "";
          break;
        }
        case 'h1': 
        case 'h2': 
        case 'h3': 
        case 'h4': 
        case 'h5': 
        case 'h6': {
          $currentFontSize = $this->FontSize;
          $this->SetFontSize($this->tempfontsize);
          $this->tempfontsize = $this->FontSizePt;
          $this->setStyle('b', false);
          $this->Ln();
          $this->lasth = $this->FontSize * K_CELL_HEIGHT_RATIO;
          break;
        }
        default : {
          break;
        }
      }
    }


/**
 * Sets font style.
 * @param string $tag tag name (in lowercase)
 * @param boolean $enable
 * @access private
 */
    function setStyle($tag, $enable) {
      //Modify style and select corresponding font
      $this->$tag += ($enable ? 1 : -1);
      $style='';
      foreach(array('b', 'i', 'u') as $s) {
        if($this->$s > 0) {
          $style .= $s;
        }
      }
      $this->SetFont('', $style);
    }
    
/**
 * Output anchor link.
 * @param string $url link URL
 * @param string $name link name
 * @access public
 */
    function addHtmlLink($url, $name) {
      //Put a hyperlink
      $this->SetTextColor(0, 0, 255);
      $this->setStyle('u', true);
      $this->Write($this->lasth, $name, $url);
      $this->setStyle('u', false);
      $this->SetTextColor(0);
    }
    
    /**
     * Returns an associative array (keys: R,G,B) from 
     * a hex html code (e.g. #3FE5AA).
     * @param string $color hexadecimal html color [#rrggbb]
     * @return array
     * @access private
     */
    function convertColorHexToDec($color = "#000000"){
      $tbl_color = array();
      $tbl_color['R'] = hexdec(substr($color, 1, 2));
      $tbl_color['G'] = hexdec(substr($color, 3, 2));
      $tbl_color['B'] = hexdec(substr($color, 5, 2));
      return $tbl_color;
    }

/*
// Converts pixels to millimeters in 72 dpi.
// @param int $px pixels
// @return float millimeters
// @access private
*/
    function pixelsToMillimeters($px){
      return $px * 25.4 / 72;
    }
       
/*
// Reverse function for htmlentities.
// Convert entities in UTF-8.

// @param $text_to_convert Text to convert.
// @return string converted
*/
    function unhtmlentities($text_to_convert) {
      return html_entity_decode($text_to_convert, ENT_QUOTES, $this->encoding);
    }


//End of class
  }

  //Handle special IE contype request
  if(isset($HTTP_SERVER_VARS['HTTP_USER_AGENT']) and $HTTP_SERVER_VARS['HTTP_USER_AGENT']=='contype') {
    header('Content-Type: application/pdf');
    exit;
  }
}
?>
