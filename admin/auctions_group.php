<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2011 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
//----------------------------------------------------------------------------
// Admin: Auction Groups Script
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
  require('includes/application_top.php');

  $action = isset($_GET['action'])?tep_db_prepare_input($_GET['action']):'';
  $agID = isset($_GET['agID'])?(int)$_GET['agID']:0;

  switch ($action) {
    case 'delete_confirm':
      $agID = isset($_POST['auctions_group_id'])?(int)$_POST['auctions_group_id']:0;
      if( empty($agID) ) {
        $messageStack->add_session(ERROR_AUCTION_GROUP_INVALID);
        tep_redirect(tep_href_link($g_script));
      }

      $auctions_array = array();
      $auctions_array_raw = "select auctions_id from " . TABLE_AUCTIONS . " where auctions_group_id = '" . (int)$agID . "'";
      tep_query_to_array($auctions_array_raw, $auctions_array);
      for($i=0, $j=count($auctions_array); $i<$j; $i++) {
        $aID = $auctions_array[$i]['auctions_id'];
        tep_db_query("delete from " . TABLE_AUCTIONS_CUSTOMER . " where auctions_id = '" . (int)$aID . "'");
        tep_db_query("delete from " . TABLE_AUCTIONS_TIER . " where auctions_id = '" . (int)$aID . "'");
        tep_db_query("delete from " . TABLE_AUCTIONS_BID . " where auctions_id = '" . (int)$aID . "'");
        tep_db_query("delete from " . TABLE_AUCTIONS_HISTORY . " where auctions_id = '" . (int)$aID . "'");

        tep_db_query("delete from " . TABLE_META_AUCTIONS . " where auctions_id = '" . (int)$aID . "'");
        tep_db_query("delete from " . TABLE_SEO_TO_AUCTIONS . " where auctions_id = '" . (int)$aID . "'");
        tep_db_query("delete from " . TABLE_SEO_URL . " where seo_url_org like '%auctions_id=" . (int)$aID . "'");
      }
      tep_db_query("delete from " . TABLE_AUCTIONS . " where auctions_group_id = '" . (int)$agID . "'");
      tep_db_query("delete from " . TABLE_AUCTIONS_GROUP . " where auctions_group_id = '" . (int)$agID . "'");
      $messageStack->add_session(WARNING_AUCTION_GROUP_DELETED, 'warning');
      tep_redirect(tep_href_link($g_script, tep_get_all_get_params(array('action', 'agID')) ));
      break;
    case 'insert_confirm':
    case 'update_confirm':
      $agID = isset($_POST['auctions_group_id'])?(int)$_POST['auctions_group_id']:0;

      $auctions_group_id = $agID;
      $sql_data_array = array(
        'auctions_group_name' => tep_db_prepare_input($_POST['auctions_group_name']),
        'auctions_group_desc' => tep_db_prepare_input($_POST['auctions_group_desc']),
        'sort_order' => (int)$_POST['sort_order'],
      );

      if($action == 'insert_confirm') {
        tep_db_perform(TABLE_AUCTIONS_GROUP, $sql_data_array);
        $agID = tep_db_insert_id();
      } elseif ($action == 'update_confirm') {
        tep_db_perform(TABLE_AUCTIONS_GROUP, $sql_data_array, 'update', "auctions_group_id = '" . (int)$agID . "'");
      }
      tep_redirect(tep_href_link($g_script, 'agID=' . $agID));

      break;
    default:
      break;
  }
?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
</head>
<body>
<!-- header //-->
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<!-- header_eof //-->
<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
    <td width="<?php echo BOX_WIDTH; ?>" valign="top"><table border="0" width="<?php echo BOX_WIDTH; ?>" cellspacing="1" cellpadding="1" class="columnLeft">
<!-- left_navigation //-->
<?php require(DIR_WS_INCLUDES . 'column_left.php'); ?>
<!-- left_navigation_eof //-->
    </table></td>
<!-- body_text //-->
    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td class="pageHeading"><?php echo HEADING_TITLE; ?></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top"><table class="tabledata">
              <tr class="dataTableHeadingRow">
                <th><?php echo TABLE_HEADING_ID; ?></th>
                <th><?php echo TABLE_HEADING_TITLE; ?></th>
                <th class="calign"><?php echo TABLE_HEADING_ACTION; ?></th>
              </tr>
<?php
    $generic_count = 0;
    $rows = 0;
    $auctions_group_query = tep_db_query("select auctions_group_id, auctions_group_name, auctions_group_desc, sort_order from " . TABLE_AUCTIONS_GROUP . " order by sort_order, auctions_group_name");

    while ($generic_text = tep_db_fetch_array($auctions_group_query)) {
      if( ( empty($agID) && !isset($agInfo) ) || ( !empty($agID) && ($agID == $generic_text['auctions_group_id'])) && (substr($action, 0, 3) != 'new')) {
        $agInfo = new objectInfo($generic_text);
      }
      $generic_count++;
      $rows++;

      if (isset($agInfo) && is_object($agInfo) && ($generic_text['auctions_group_id'] == $agInfo->auctions_group_id) ) {
        echo '              <tr id="defaultSelected" class="dataTableRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . tep_href_link($g_script, 'agID=' . $generic_text['auctions_group_id'] . '&action=preview&read=only') . '\'">' . "\n";
      } else {
        echo '              <tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . tep_href_link($g_script, 'agID=' . $generic_text['auctions_group_id']) . '\'">' . "\n";
      }
?>
                <td><?php echo $generic_text['auctions_group_id']; ?></td>
                <td><?php echo $generic_text['auctions_group_name']; ?></td>
                <td class="calign"><?php if (isset($agInfo) && is_object($agInfo) && ($generic_text['auctions_group_id'] == $agInfo->auctions_group_id)) { echo tep_image(DIR_WS_IMAGES . 'icon_arrow_right.gif', ''); } else { echo '<a href="' . tep_href_link($g_script, 'agID=' . $generic_text['auctions_group_id']) . '">' . tep_image(DIR_WS_IMAGES . 'icon_info.gif', IMAGE_ICON_INFO) . '</a>'; } ?>&nbsp;</td>
              </tr>
<?php
    }
?>
              <tr>
                <td colspan="4"><table border="0" width="100%" cellspacing="0" cellpadding="2">
                  <tr>
                    <td class="smallText"><?php echo TEXT_INFO_ENTRIES . '&nbsp;' . $generic_count; ?></td>
                    <td align="right" class="smallText"><?php echo '<a href="' . tep_href_link($g_script, 'action=new') . '">' . tep_image_button('button_new.gif', IMAGE_NEW_GROUP) . '</a>'; ?>&nbsp;</td>
                  </tr>
                </table></td>
              </tr>
            </table></td>
<?php
    $heading = array();
    $contents = array();

    switch ($action) {
      case 'new':
        $heading[] = array('text' => '<b>' . TEXT_INFO_HEADING_NEW . '</b>');

        $contents = array('form' => tep_draw_form('new_group', $g_script, 'action=insert_confirm') . tep_draw_hidden_field('auctions_group_id', $agInfo->auctions_group_id));
        $contents[] = array('text' => TEXT_INFO_NEW_INTRO);
        $contents[] = array('text' => TEXT_INFO_GROUP_NAME . '<br />' . tep_draw_input_field('auctions_group_name'));
        $contents[] = array('text' => TEXT_INFO_GROUP_DESC . '<br />' . tep_draw_textarea_field('auctions_group_desc', 'hard', 30, 5, ''));
        $contents[] = array('text' => TEXT_INFO_GROUP_ORDER . '<br />' . tep_draw_input_field('sort_order'));

        $contents[] = array('align' => 'center', 'text' => '<br>' . tep_image_submit('button_insert.gif', IMAGE_INSERT) . ' <a href="' . tep_href_link($g_script, 'agID=' . $agInfo->auctions_group_id) . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>');
        break;

      case 'edit':
        $heading[] = array('text' => '<b>' . TEXT_INFO_HEADING_EDIT . '</b>');

        $contents = array('form' => tep_draw_form('edit_group', $g_script, 'action=update_confirm') . tep_draw_hidden_field('auctions_group_id', $agInfo->auctions_group_id));
        $contents[] = array('text' => TEXT_INFO_EDIT_INTRO);
        $contents[] = array('text' => TEXT_INFO_GROUP_NAME . '<br />' . tep_draw_input_field('auctions_group_name', $agInfo->auctions_group_name));
        $contents[] = array('text' => TEXT_INFO_GROUP_DESC . '<br />' . tep_draw_textarea_field('auctions_group_desc', 'hard', 30, 5, $agInfo->auctions_group_desc));
        $contents[] = array('text' => TEXT_INFO_GROUP_ORDER . '<br />' . tep_draw_input_field('sort_order', $agInfo->sort_order));
        $contents[] = array('align' => 'center', 'text' => '<br>' . tep_image_submit('button_update.gif', IMAGE_UPDATE) . ' <a href="' . tep_href_link($g_script, 'agID=' . $agInfo->auctions_group_id) . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>');
        break;

      case 'delete':
        $heading[] = array('text' => '<b>' . TEXT_INFO_HEADING_DELETE . '</b>');

        $contents = array('form' => tep_draw_form('delete_group', $g_script, 'action=delete_confirm') . tep_draw_hidden_field('auctions_group_id', $agInfo->auctions_group_id));
        $contents[] = array('text' => TEXT_INFO_DELETE_INTRO);
        $contents[] = array('text' => '<br><b>' . $agInfo->auctions_group_name . '</b>');
        $contents[] = array('align' => 'center', 'text' => '<br>' . tep_image_submit('button_delete.gif', IMAGE_DELETE) . ' <a href="' . tep_href_link($g_script, 'agID=' . $agInfo->auctions_group_id) . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>');
        break;

      default:
        if( isset($agInfo) && is_object($agInfo) ) { // generic_text info box contents
          $heading[] = array('text' => '<b>' . $agInfo->auctions_group_name . '</b>');
          $contents[] = array('align' => 'center', 'text' => '<a href="' . tep_href_link($g_script, 'agID=' . $agInfo->auctions_group_id . '&action=edit') . '">' . tep_image_button('button_edit.gif', IMAGE_EDIT) . '</a> <a href="' . tep_href_link($g_script, 'agID=' . $agInfo->auctions_group_id . '&action=delete') . '">' . tep_image_button('button_delete.gif', IMAGE_DELETE) . '</a>');
          $contents[] = array('text' => '<br /><b>' . $agInfo->auctions_group_name . '</b>');
          $contents[] = array('text' => '<br />' . $agInfo->auctions_group_desc);
        } else { // create generic_text dummy info
          $heading[] = array('text' => '<b>' . TEXT_INFO_NO_GROUPS . '</b>');
          $contents[] = array('text' => TEXT_INFO_ZERO_GROUPS);
        }
        break;

    }

    if ( (tep_not_null($heading)) && (tep_not_null($contents)) ) {
      echo '            <td width="25%" valign="top">' . "\n";

      $box = new box;
      echo $box->infoBox($heading, $contents);

      echo '            </td>' . "\n";
    }
?>
          </tr>
        </table></td>
      </tr>
    </table></td>
<!-- body_text_eof //-->
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
<br>
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
