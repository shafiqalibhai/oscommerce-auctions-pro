<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2008 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Admin: Product Filters Controller
//----------------------------------------------------------------------------
// I-Metrics Layer
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
  require('includes/application_top.php');

  $filters_layout_array = array(
                                array('id' => '0', 'text' => 'NONE'),
                                array('id' => '1', 'text' => 'MENU'),
                                array('id' => '2', 'text' => 'MENU+NAV'),
                                array('id' => '3', 'text' => 'NAV'),
                               );

  $action = (isset($_GET['action']) ? $_GET['action'] : 'list');
  $fID = (isset($_GET['fID']) ? $_GET['fID'] : '');
  if (isset($_POST['delete_x']) || isset($_POST['delete_y'])) {
    $action='delete';
  } elseif (isset($_POST['delete_sub_x']) || isset($_POST['delete_sub_y'])) {
    $action='delete_sub';
  }

  switch ($action) {
    case 'set_flag':
      $sql_data_array = array('status_id' => (int)$_GET['flag']);
      tep_db_perform(TABLE_PRODUCTS_FILTERS, $sql_data_array, 'update', 'products_filter_id=' . (int)$_GET['id']);
      tep_redirect(tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('action', 'flag', 'id')) ));
      break;
    case 'insert':
      if( tep_not_null($_POST['name']) && tep_not_null($_POST['db_name']) && tep_not_null($_POST['parameter']) ) {
        if($_POST['width'] < 1 || $_POST['width'] > 9) {
          $_POST['width'] = 1;
        }
        $sql_data_array = array(
                                'products_filter_name' => tep_db_prepare_input($_POST['name']),
                                'products_filter_db_name' => tep_db_prepare_input($_POST['db_name']),
                                'products_filter_parameter' => tep_db_prepare_input($_POST['parameter']),
                                'width_id' => (int)$_POST['width'],
                                'layout_id' => (int)$_POST['layout'],
                                'sort_id' => (int)$_POST['order']
                               );

        tep_db_query("alter table " . TABLE_PRODUCTS . " add " . tep_db_input(tep_db_prepare_input($_POST['db_name'])) . " int (" . (int)$_POST['width'] . ") default 0");
        tep_db_perform(TABLE_PRODUCTS_FILTERS, $sql_data_array, 'insert');
        $new_filter_id = tep_db_insert_id();

        $sql_data_array = array(
                                'products_select_id' => 0,
                                'products_filter_id' => (int)$new_filter_id,
                                'products_select_name' => tep_db_prepare_input(TEXT_INFO_NA),
                                'sort_id' => 0,
                                'status_id' => 0,
                               );
        tep_db_perform(TABLE_PRODUCTS_SELECT, $sql_data_array, 'insert');
      }
      tep_redirect(tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('action', 'fID')) . 'action=list&fID=' . $new_filter_id ));
      break;
    case 'update':
      if(isset($_POST['mark']) && is_array($_POST['mark']) ) {
        foreach ($_POST['mark'] as $key => $val) {
          if( !tep_not_null($_POST['name']) || !tep_not_null($_POST['db_name']) || !tep_not_null($_POST['parameter']) ) {
            continue;
          }

          if($_POST['width'][$key] < 1 || $_POST['width'][$key] > 9) {
            $_POST['width'][$key] = 1;
          }
          $check_query = tep_db_query("select products_filter_db_name, width_id from " . TABLE_PRODUCTS_FILTERS . " where products_filter_id = '" . (int)$key . "'");
          if( $check_array = tep_db_fetch_array($check_query) ) {
            $old_name = $check_array['products_filter_db_name'];
            $old_width = $check_array['width_id'];
            $sql_data_array = array(
                                    'products_filter_name' => tep_db_prepare_input($_POST['name'][$key]),
                                    'products_filter_db_name' => tep_db_prepare_input($_POST['db_name'][$key]),
                                    'products_filter_parameter' => tep_db_prepare_input($_POST['parameter'][$key]),
                                    'width_id' => (int)$_POST['width'][$key],
                                    'layout_id' => (int)$_POST['layout'][$key],
                                    'sort_id' => (int)$_POST['order'][$key]
                                   );
            tep_db_perform(TABLE_PRODUCTS_FILTERS, $sql_data_array, 'update', "products_filter_id= '" . (int)$key . "'");
            if( $old_name != $_POST['db_name'][$key] || $old_width != $_POST['width'][$key] ) {
              tep_db_query("alter table " . TABLE_PRODUCTS . " change " . tep_db_input(tep_db_prepare_input($old_name)) . " " . tep_db_input(tep_db_prepare_input($_POST['db_name'][$key])) . " int (" . (int)$_POST['width'][$key] . ") default 0");
            }
          }
        }
      }
      tep_redirect(tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('action')) . 'action=list' ));
      break;
    case 'delete':
      if( !isset($_POST['mark']) || !is_array($_POST['mark']) || !count($_POST['mark']) ) {
        tep_redirect(tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('action')) . 'action=list'));
      }
      break;
    case 'delete_confirm':
      if(isset($_POST['mark']) && is_array($_POST['mark']) ) {
        foreach ($_POST['mark'] as $key=>$val) {
          $check_query = tep_db_query("select products_filter_db_name from " . TABLE_PRODUCTS_FILTERS . " where products_filter_id=" . (int)$key);
          if( $check_array = tep_db_fetch_array($check_query) ) {
            tep_db_query("alter table " . TABLE_PRODUCTS . " drop " . tep_db_input(tep_db_prepare_input($check_array['products_filter_db_name'])) . "");
            tep_db_query("delete from " . TABLE_PRODUCTS_SELECT . " where products_filter_id=" . (int)$key);
            tep_db_query("delete from " . TABLE_PRODUCTS_FILTERS . " where products_filter_id=" . (int)$key);
          }
        }
        if( isset($_POST['mark'][$fID]) ) {
          tep_redirect(tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('action')) . 'action=list' ));
        }
      }
      tep_redirect(tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('action', 'fID')) . 'action=list&fID=' . $fID ));
      break;

    case 'set_sub_flag':
      $sql_data_array = array('status_id' => (int)$_GET['flag']);
      tep_db_perform(TABLE_PRODUCTS_SELECT, $sql_data_array, 'update', "products_filter_id = '" . (int)$fID . "' and products_select_id= '" . (int)$_GET['id'] . "'");
      tep_redirect(tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('action', 'flag', 'id')) . 'action=sub_list'));
      break;

    case 'insert_sub':
      if( tep_not_null($fID) && $fID > 0 ) {
        $check_query = tep_db_query("select products_select_id from " . TABLE_PRODUCTS_SELECT . " where products_filter_id = '" . (int)$fID . "' order by products_select_id desc limit 1");
        if( !tep_db_num_rows($check_query) ) {
          $sql_data_array = array(
                                  'products_select_id' => 0,
                                  'products_filter_id' => (int)$fID,
                                  'products_select_name' => tep_db_prepare_input(TEXT_INFO_NA),
                                  'sort_id' => 0,
                                  'status_id' => 0,
                                 );
          tep_db_perform(TABLE_PRODUCTS_SELECT, $sql_data_array, 'insert');
          $new_select_id = 1;
        } else {
          $check_array = tep_db_fetch_array($check_query);
          $new_select_id = $check_array['products_select_id']+1;
        }
        $sql_data_array = array(
                                'products_select_id' => (int)$new_select_id,
                                'products_select_name' => tep_db_prepare_input($_POST['name']),
                                'products_filter_id' => (int)$fID,
                                'sort_id' => (int)$_POST['order']
                               );

        tep_db_perform(TABLE_PRODUCTS_SELECT, $sql_data_array, 'insert');
      }
      tep_redirect(tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('action')) . 'action=sub_list' ));
      break;
    case 'update_sub':
      if(isset($_POST['mark']) && is_array($_POST['mark']) ) {
        foreach ($_POST['mark'] as $key => $val) {
          $sql_data_array = array(
                                  'products_select_name' => tep_db_prepare_input($_POST['name'][$key]),
                                  'sort_id' => (int)$_POST['order'][$key]
                                 );
          tep_db_perform(TABLE_PRODUCTS_SELECT, $sql_data_array, 'update', "products_filter_id = '" . (int)$fID . "' and products_select_id= '" . (int)$key . "'");
        }
      }
      tep_redirect(tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('action')) . 'action=sub_list' ));
      break;
    case 'delete_sub':
      if( !isset($_POST['mark']) || !is_array($_POST['mark']) || !count($_POST['mark']) ) {
        tep_redirect(tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('action')) . 'action=sub_list'));
      }
      break;
    case 'delete_sub_confirm':
      if(isset($_POST['mark']) && is_array($_POST['mark']) ) {
        foreach ($_POST['mark'] as $key=>$val) {
          $check_query = tep_db_query("select products_filter_db_name from " . TABLE_PRODUCTS_FILTERS . " where products_filter_id= '" . (int)$fID . "'");
          if( $check_array = tep_db_fetch_array($check_query) ) {
            tep_db_query("delete from " . TABLE_PRODUCTS_SELECT . " where products_select_id= '" . (int)$key . "' and products_filter_id= '" . (int)$fID . "'");
            tep_db_query("update " . TABLE_PRODUCTS . " set  " . tep_db_input(tep_db_prepare_input($check_array['products_filter_db_name'])) . " = '0' where " . tep_db_input(tep_db_prepare_input($check_array['products_filter_db_name'])) . " = '" . (int)$key . "'");
            tep_db_query("update " . TABLE_PRODUCTS_SELECT . " set products_select_id = products_select_id-1 where products_select_id > '" . (int)$key  . "' and products_filter_id= '" . (int)$fID . "' order by products_select_id");
            tep_db_query("update " . TABLE_PRODUCTS . " set  " . tep_db_input(tep_db_prepare_input($check_array['products_filter_db_name'])) . " = " . tep_db_input(tep_db_prepare_input($check_array['products_filter_db_name'])) . "-1 where " . tep_db_input(tep_db_prepare_input($check_array['products_filter_db_name'])) . " > '" . (int)$key . "'");
          }
        }
      }
      tep_redirect(tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('action')) . 'action=sub_list'));
      break;
    case 'sub_list':
      break;
    case 'list':
      break;
    default:
      $action = 'list';
      break;
  }
?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<script language="javascript" src="includes/general.js"></script>
<script language="javascript">
  var g_checkbox2 = 0;
  function copy_checkboxes(form, array_name) {
    for (var i = 0; i < form.elements.length; i++) {
      if( form.elements[i].type == "checkbox" ) {
        check_name = form.elements[i].name;
        if( array_name == check_name.substring(0, array_name.length) ) {
          form.elements[i].checked = g_checkbox2?"":"on";
        }
      }
    }
    g_checkbox2 ^= 1;
  }
</script>
</head>
<body>
<!-- header //-->
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<!-- header_eof //-->

<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
    <td width="<?php echo BOX_WIDTH; ?>" valign="top"><table border="0" width="<?php echo BOX_WIDTH; ?>" cellspacing="1" cellpadding="1" class="columnLeft">
<!-- left_navigation //-->
<?php require(DIR_WS_INCLUDES . 'column_left.php'); ?>
<!-- left_navigation_eof //-->
    </table></td>
<!-- body_text //-->
    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
<?php
  if($action == 'list') {
?>
        <td width="75%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
          <tr>
            <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
              <tr>
                <td class="pageHeading"><?php echo HEADING_FILTERS_UPDATE; ?></td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td class="smallText"><?php echo TEXT_INFO_UPDATE; ?></td>
          </tr>
          <tr>
            <td class="formArea"><?php echo tep_draw_form('filters_update', basename($PHP_SELF),'action=update', 'post'); ?><table border="0" width="100%" cellspacing="1" cellpadding="3">
              <tr class="dataTableHeaderForm">
                <td class="dataTableHeadingContent" width="5%"><?php echo '<a href="javascript:void(0)" onClick="copy_checkboxes(document.filters_update,\'mark\')" title="Page Select On/Off" class="menuBoxHeadingLink"><span class="dataTableHeadingContent">' . tep_image(DIR_WS_ICONS . 'tick.gif', 'Page Select On/Off') . '</span></a>'; ?></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_FILTERS; ?></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_DB_NAME; ?></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_PARAMETER; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_WIDTH; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_LAYOUT; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_ORDER; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_STATUS; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_ACTION; ?></td>
              </tr>
<?php
    $products_filter_query = tep_db_query("select products_filter_id, products_filter_name, products_filter_db_name, products_filter_parameter, width_id, layout_id, sort_id, status_id from " . TABLE_PRODUCTS_FILTERS . " order by sort_id");
    while ($product_filter = tep_db_fetch_array($products_filter_query)) {

      if ((!isset($_GET['fID']) || (isset($_GET['fID']) && ($_GET['fID'] == $product_filter['products_filter_id']))) && !isset($pInfo) ) {
        $product_filter['selects'] = array();
        $check_query = tep_db_query("select products_select_name from " . TABLE_PRODUCTS_SELECT . " where products_filter_id = '" . (int)$product_filter['products_filter_id'] . "'");
        if( tep_db_num_rows($check_query) ) {
          while($check_array = tep_db_fetch_array($check_query)) {
            $product_filter['selects'][] = $check_array['products_select_name'];
          }
        } else {
          $product_filter['selects'][] = 'No Options Set';
        }
        $pInfo = new objectInfo($product_filter);
      }

      if (isset($pInfo) && is_object($pInfo) && ($product_filter['products_filter_id'] == $pInfo->products_filter_id)) {
        echo '                  <tr id="defaultSelected" class="dataTableRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)">' . "\n";
      } else {
        echo '                  <tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)">' . "\n";
      }

?>
                <td width="20"><?php echo tep_draw_checkbox_field('mark['.$product_filter['products_filter_id'].']', 1) ?></td>
                <td class="dataTableContent"><?php echo tep_draw_input_field('name['.$product_filter['products_filter_id'].']', $product_filter['products_filter_name'], 'size=30', false, 'text', true);?></td>
                <td class="dataTableContent"><?php echo tep_draw_input_field('db_name['.$product_filter['products_filter_id'].']', $product_filter['products_filter_db_name'], 'size=30', false, 'text', true);?></td>
                <td class="dataTableContent"><?php echo tep_draw_input_field('parameter['.$product_filter['products_filter_id'].']', $product_filter['products_filter_parameter'], 'size=30', false, 'text', true);?></td>
                <td class="dataTableContent" align="center"><?php echo tep_draw_input_field('width[' . $product_filter['products_filter_id'] . ']', $product_filter['width_id'], 'size="1"', false, 'text', true);?></td>
                <td class="dataTableContent" align="center"><?php echo tep_draw_pull_down_menu('layout['.$product_filter['products_filter_id'].']', $filters_layout_array, $product_filter['layout_id']); ?></td>
                <td class="dataTableContent" align="center"><?php echo tep_draw_input_field('order['.$product_filter['products_filter_id'].']', $product_filter['sort_id'], 'size="5"', false, 'text', true);?></td>
                <td class="dataTableContent" align="center">
<?php
      if ($product_filter['status_id'] == '1') {
        echo tep_image(DIR_WS_IMAGES . 'icon_status_green.gif', IMAGE_ICON_STATUS_GREEN, 10, 10) . '&nbsp;&nbsp;<a href="' . tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('action', 'flag', 'id')) . 'action=set_flag&flag=0&id=' . $product_filter['products_filter_id'], 'NONSSL') . '">' . tep_image(DIR_WS_IMAGES . 'icon_status_red_light.gif', IMAGE_ICON_STATUS_RED_LIGHT, 10, 10) . '</a>';
      }
      else {
        echo '<a href="' . tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('action', 'flag', 'id')) . 'action=set_flag&flag=1&id=' . $product_filter['products_filter_id'], 'NONSSL') . '">' . tep_image(DIR_WS_IMAGES . 'icon_status_green_light.gif', IMAGE_ICON_STATUS_GREEN_LIGHT, 10, 10) . '</a>&nbsp;&nbsp;' . tep_image(DIR_WS_IMAGES . 'icon_status_red.gif', IMAGE_ICON_STATUS_RED, 10, 10);
      }
?>
                <td class="dataTableContent" align="right">
<?php 
      if (isset($pInfo) && is_object($pInfo) && ($product_filter['products_filter_id'] == $pInfo->products_filter_id)) { 
        echo '<a href="' . tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('action', 'fID')) . 'action=sub_list&fID=' . $pInfo->products_filter_id) . '">' . tep_image(DIR_WS_IMAGES . 'icon_arrow_right.gif', IMAGE_DETAILS) . '</a>'; 
      } else { 
        echo '<a href="' . tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('action', 'fID') ) . 'fID=' . $product_filter['products_filter_id']) . '">' . tep_image(DIR_WS_IMAGES . 'icon_info.gif', IMAGE_ICON_INFO) . '</a>'; 
      } 
?>
                </td>
              </tr>
<?php
    } 
?>
              <tr>
                <td colspan="9"><hr class="formAreaBorder" /></td>
              </tr>
              <tr>
                <td colspan="9"><?php echo tep_image_submit('button_update.gif',IMAGE_UPDATE, 'name="update"') . '&nbsp;&nbsp;' . tep_image_submit('button_delete.gif',IMAGE_DELETE,'name="delete"'); ?></td>
              </tr>
            </table></form></td>
          </tr>
          <tr>
            <td><?php echo tep_draw_separator('pixel_trans.gif', '100%', '10'); ?></td>
          </tr>
          <tr>
            <td width="100%"><table border="0" width="100%" cellspacing="0" cellpadding="0">
              <tr>
                <td class="pageHeading"><?php echo HEADING_TITLE; ?></td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td class="smallText"><?php echo TEXT_INFO_INSERT; ?></td>
          </tr>
          <tr>
            <td class="formArea"><?php echo tep_draw_form("insert_filter", basename($PHP_SELF), 'action=insert', 'post'); ?><table border="0" width="100%" cellspacing="1" cellpadding="3">
              <tr class="dataTableHeaderForm">
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_NEW_FILTER; ?></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_DB_NAME; ?></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_PARAMETER; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_WIDTH; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_LAYOUT; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_ORDER; ?></td>
              </tr>

              <tr>
                <td class="dataTableContent"><?php echo tep_draw_input_field('name', '', 'size="30"', false, 'text', true); ?></td>
                <td class="dataTableContent"><?php echo tep_draw_input_field('db_name', '', 'size="30"', false, 'text', true); ?></td>
                <td class="dataTableContent"><?php echo tep_draw_input_field('parameter', '', 'size="30"', false, 'text', true); ?></td>
                <td class="dataTableContent" align="center"><?php echo tep_draw_input_field('width', '', 'size="1"', false, 'text', true); ?></td>
                <td class="dataTableContent" align="center"><?php echo tep_draw_pull_down_menu('layout', $filters_layout_array); ?></td>
                <td class="dataTableContent" align="center"><?php echo tep_draw_input_field('order', '', 'size="5"', false, 'text', true); ?></td>
              </tr>
              <tr>
                <td colspan="6"><hr class="formAreaBorder" /></td>
              </tr>
              <tr colspan="6">
                <td class="dataTableHeadingContent"><?php echo tep_image_submit('button_insert.gif',IMAGE_INSERT); ?></td>
              </tr>
            </table></form></td>
          </tr>
          <tr>
            <td><?php echo tep_draw_separator('pixel_trans.gif', '100%', '10'); ?></td>
          </tr>
        </table></td>

<?php
    $heading = array();
    $contents = array();

    switch ($action) {
      default:
        if (isset($pInfo) && is_object($pInfo)) {
          $heading[] = array('text' => '<b>' . $pInfo->products_filter_name . '</b>');
          $contents[] = array('align' => 'center', 'text' => '<a href="' . tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('action', 'fID')) . 'action=sub_list&fID=' . $pInfo->products_filter_id) . '">' . tep_image_button('button_details.gif', IMAGE_DETAILS) . '</a>');
          $contents[] = array('text' => '<br>' . TEXT_INFO_NUMBER_FILTERS . ' <br />' . implode('<br />', $pInfo->selects) );
        }
        break;
    }
    if ( (tep_not_null($heading)) && (tep_not_null($contents)) ) {
      echo '            <td width="25%" valign="top">' . "\n";

      $box = new box;
      echo $box->infoBox($heading, $contents);

      echo '            </td>' . "\n";
    }
?>
      </tr>
    </table></td>
<?php
  } elseif($action == 'delete') {
?>
        <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
          <tr>
            <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
              <tr>
                <td class="pageHeading"><?php echo HEADING_FILTERS_DELETE; ?></td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td class="smallText"><?php echo TEXT_INFO_UPDATE; ?></td>
          </tr>
          <tr>
            <td class="formArea"><?php echo tep_draw_form('delete', basename($PHP_SELF), tep_get_all_get_params(array('action')) . 'action=delete_confirm', 'post'); ?><table border="0" width="100%" cellspacing="1" cellpadding="3">
              <tr class="dataTableHeaderForm">
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_NAME; ?></td>
              </tr>
<?php
    $rows = 0;
    foreach( $_POST['mark'] as $key => $value ) {
      $products_filter_query = tep_db_query("select products_filter_id, products_filter_name from " . TABLE_PRODUCTS_FILTERS . " where products_filter_id = '" . (int)$key . "'");
      if( $product_filter = tep_db_fetch_array($products_filter_query) ) {
        $row_class = ($rows%2)?'dataTableRow':'dataTableRowSelected';
        echo '                      <tr class="' . $row_class . '">';
?>
                <td class="dataTableContent"><?php echo $product_filter['products_filter_name'] . tep_draw_hidden_field('mark[' . $product_filter['products_filter_id'] . ']', $product_filter['products_filter_id']); ?></td>
              </tr>
<?php
      }
    }
?>
              <tr>
                <td>
<?php 
    echo '<a href="' . tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('action')) . 'action=list') . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a> ' . tep_image_submit('button_confirm.gif', IMAGE_CONFIRM);
?>
                </td>
              </tr>
            </table></form></td>
          </tr>
        </table></td>
<?php
  } elseif($action == 'sub_list') {
?>
        <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
          <tr>
            <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
              <tr>
                <td class="pageHeading">
<?php 
    $name_query = tep_db_query("select products_filter_name, products_filter_db_name, products_filter_parameter from " . TABLE_PRODUCTS_FILTERS . " where products_filter_id = '" . (int)$fID . "'");
    if($name_array = tep_db_fetch_array($name_query) ) {
      echo sprintf(HEADING_FILTERS_OPTIONS_UPDATE, $name_array['products_filter_name'], $name_array['products_filter_db_name']);
    } else {
      echo 'Error';
    }
?>
                </td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td class="smallText"><?php echo TEXT_INFO_UPDATE_SUB; ?></td>
          </tr>
          <tr>
            <td class="formArea"><?php echo tep_draw_form('select_update', basename($PHP_SELF), tep_get_all_get_params(array('action')) . 'action=update_sub', 'post'); ?><table border="0" width="100%" cellspacing="1" cellpadding="3">
              <tr class="dataTableHeaderForm">
                <td class="dataTableHeadingContent" width="5%"><?php echo '<a href="javascript:void(0)" onClick="copy_checkboxes(document.select_update,\'mark\')" title="Page Select On/Off" class="menuBoxHeadingLink"><span class="dataTableHeadingContent">' . tep_image(DIR_WS_ICONS . 'tick.gif', 'Page Select On/Off') . '</span></a>'; ?></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_NAME; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_ORDER; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_STATUS; ?></td>
              </tr>
<?php
    $rows = 0;
    $select_query = tep_db_query("select products_select_id, products_select_name, sort_id, status_id from " . TABLE_PRODUCTS_SELECT . " where products_filter_id = '" . (int)$fID . "' order by sort_id");
    while ($product_select = tep_db_fetch_array($select_query)) {
      $rows++;
      $row_class = ($rows%2)?'dataTableRow':'dataTableRowSelected';
      echo '                      <tr class="' . $row_class . '">';
      if($product_select['products_select_id']) {
?>
                <td class="dataTableContent" width="20"><?php echo tep_draw_checkbox_field('mark['.$product_select['products_select_id'].']', 1); ?></td>
                <td class="dataTableContent"><?php echo tep_draw_input_field('name['.$product_select['products_select_id'].']', $product_select['products_select_name'], 'size=30', false, 'text', true);?></td>
                <td class="dataTableContent" align="center"><?php echo tep_draw_input_field('order['.$product_select['products_select_id'].']', $product_select['sort_id'], 'size="5"', false, 'text', true);?></td>
                <td class="dataTableContent" align="center">
<?php
        if ($product_select['status_id'] == '1') {
          echo tep_image(DIR_WS_IMAGES . 'icon_status_green.gif', IMAGE_ICON_STATUS_GREEN, 10, 10) . '&nbsp;&nbsp;<a href="' . tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('action', 'flag', 'id')) . 'action=set_sub_flag&flag=0&id=' . $product_select['products_select_id'], 'NONSSL') . '">' . tep_image(DIR_WS_IMAGES . 'icon_status_red_light.gif', IMAGE_ICON_STATUS_RED_LIGHT, 10, 10) . '</a>';
        }
        else {
          echo '<a href="' . tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('action', 'flag', 'id')) . 'action=set_sub_flag&flag=1&id=' . $product_select['products_select_id'], 'NONSSL') . '">' . tep_image(DIR_WS_IMAGES . 'icon_status_green_light.gif', IMAGE_ICON_STATUS_GREEN_LIGHT, 10, 10) . '</a>&nbsp;&nbsp;' . tep_image(DIR_WS_IMAGES . 'icon_status_red.gif', IMAGE_ICON_STATUS_RED, 10, 10);
        }
?>
                </td>
<?php
      } else {
?>
                <td class="dataTableContent" width="20">N/A</td>
                <td class="dataTableContent"><?php echo $product_select['products_select_name']; ?></td>
                <td class="dataTableContent">&nbsp;</td>
                <td class="dataTableContent">&nbsp;</td>
<?php
      }
?>
              </tr>
<?php
    } 
?>
              <tr>
                <td colspan="8"><hr class="formAreaBorder" /></td>
              </tr>
              <tr>
                <td colspan="8"><?php echo '<a href="' . tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('action')) . 'action=list') . '">' . tep_image_button('button_back.gif', IMAGE_BACK) . '</a> ' . tep_image_submit('button_update.gif', IMAGE_UPDATE, 'name="update_sub"') . '&nbsp;' . tep_image_submit('button_delete.gif',IMAGE_DELETE,'name="delete_sub"'); ?></td>
              </tr>
            </table></form></td>
          </tr>
          <tr>
            <td><?php echo tep_draw_separator('pixel_trans.gif', '100%', '10'); ?></td>
          </tr>
          <tr>
            <td width="100%"><table border="0" width="100%" cellspacing="0" cellpadding="0">
              <tr>
                <td class="pageHeading">
<?php
    $name_query = tep_db_query("select products_filter_name, products_filter_db_name from " . TABLE_PRODUCTS_FILTERS . " where products_filter_id = '" . (int)$fID . "'");
    if($name_array = tep_db_fetch_array($name_query) ) {
      echo sprintf(HEADING_TITLE_SUB, $name_array['products_filter_name'], $name_array['products_filter_db_name']);
    } else {
      echo 'Error';
    }
?>
                </td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td class="smallText"><?php echo TEXT_INFO_INSERT_SUB; ?></td>
          </tr>
          <tr>
            <td class="formArea"><?php echo tep_draw_form("insert_filter", basename($PHP_SELF), tep_get_all_get_params(array('action')) . 'action=insert_sub', 'post'); ?><table border="0" width="100%" cellspacing="1" cellpadding="3">
              <tr class="dataTableHeaderForm">
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_NAME; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_ORDER; ?></td>
              </tr>

              <tr>
                <td class="dataTableContent"><?php echo tep_draw_input_field('name', '', 'size="30"', false, 'text', true); ?></td>
                <td class="dataTableContent" align="center"><?php echo tep_draw_input_field('order', '', 'size="5"', false, 'text', true); ?></td>
              </tr>
              <tr>
                <td colspan="6"><hr class="formAreaBorder" /></td>
              </tr>
              <tr colspan="6">
                <td class="dataTableHeadingContent"><?php echo tep_image_submit('button_insert.gif',IMAGE_INSERT); ?></td>
              </tr>
            </table></form></td>
          </tr>
          <tr>
            <td><?php echo tep_draw_separator('pixel_trans.gif', '100%', '10'); ?></td>
          </tr>
       </table></td>
      </tr>
    </table></td>
<?php
  } elseif($action == 'delete_sub') {
?>
   <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
     <tr>
       <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
         <tr>
           <td class="pageHeading"><?php echo HEADING_FILTERS_OPTIONS_DELETE; ?></td>
         </tr>
       </table></td>
     </tr>
     <tr>
       <td class="smallText"><?php echo TEXT_INFO_DELETE_SUB; ?></td>
     </tr>
     <tr>
       <td class="formArea"><?php echo tep_draw_form('delete_sub', basename($PHP_SELF), tep_get_all_get_params(array('action')) . 'action=delete_sub_confirm', 'post'); ?><table border="0" width="100%" cellspacing="1" cellpadding="3">
         <tr class="dataTableHeaderForm">
           <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_NAME; ?></td>
         </tr>
<?php
    $rows = 0;
    foreach( $_POST['mark'] as $key => $value ) {
      $products_select_query = tep_db_query("select products_select_id, products_select_name from " . TABLE_PRODUCTS_SELECT . " where products_filter_id = '" . (int)$fID . "' and products_select_id = '" . (int)$key . "'");
      if( $product_select = tep_db_fetch_array($products_select_query) ) {
        $rows++;
        $row_class = ($rows%2)?'dataTableRow':'dataTableRowSelected';
        echo '                      <tr class="' . $row_class . '">';
?>
            <td class="dataTableContent"><?php echo $product_select['products_select_name'] . tep_draw_hidden_field('mark[' . $product_select['products_select_id'] . ']', $product_select['products_select_id']); ?></td>
          </tr>
<?php
      }
    }
?>
              <tr>
                <td colspan="8"><hr class="formAreaBorder" /></td>
              </tr>
              <tr>
                <td>
<?php 
    echo '<a href="' . tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('action')) . 'action=sub_list') . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a> ' . tep_image_submit('button_confirm.gif', IMAGE_CONFIRM);
?>
                </td>
              </tr>
            </table></form></td>
          </tr>
        </table></td>
<?php
  }
?>
<!-- body_text_eof //-->
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
