<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2007 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Cache Reports for osC Admin
// - Added HTML Cache report
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// http://www.oscommerce.com
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
  require('includes/application_top.php');

  $action = (isset($_GET['action']) ? $_GET['action'] : '');

  switch($action) {
    case 'truncate_html':
      tep_db_query("truncate table " . TABLE_CACHE_HTML_REPORTS . "");
      tep_redirect(tep_href_link(FILENAME_CACHE_REPORTS, tep_get_all_get_params(array('action')) ));
      break;
    default:
      break;
  }

  $modes_array = array(
                        array('id' => '1', 'text' => 'Cache'),
                        array('id' => '2', 'text' => 'Flush'),
                        array('id' => '3', 'text' => 'Parametric')
                      );


?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<script language="javascript" src="includes/general.js"></script>
</head>
<body>
<!-- header //-->
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<!-- header_eof //-->

<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
    <td width="<?php echo BOX_WIDTH; ?>" valign="top"><table border="0" width="<?php echo BOX_WIDTH; ?>" cellspacing="1" cellpadding="1" class="columnLeft">
<!-- left_navigation //-->
<?php require(DIR_WS_INCLUDES . 'column_left.php'); ?>
<!-- left_navigation_eof //-->
    </table></td>
<!-- body_text //-->
    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td class="pageHeading"><?php echo HEADING_TITLE; ?></td>
          </tr>
        </table></td>
      </tr>
<?php
  if( $action == 'report_mysql' ) {
?>
<?php
  } else {
?>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
<?php
// Get Scripts info from the database
    $rows = 0;
    $cache_html_query_raw = "select cr.*, c.cache_html_type from " . TABLE_CACHE_HTML_REPORTS . " cr left join " . TABLE_CACHE_HTML . " c on (c.cache_html_key = cr.cache_html_key) order by cr.cache_html_script";
    $cache_html_split = new splitPageResults($_GET['page'], MAX_DISPLAY_HTML_CACHE_SCRIPTS, $cache_html_query_raw, $cache_html_query_numrows, 'cr.cache_html_key');
    if( $cache_html_query_numrows > 0 ) {
?>
              <tr>
                <td valign="top"><table border="0" width="100%" cellspacing="1" cellpadding="3">
                  <tr class="dataTableHeadingRow">
                    <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_FILENAME; ?></td>
                    <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_HITS; ?></td>
                    <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_MISSES; ?></td>
                    <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_EFFICIENCY; ?></td>
                    <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_SPIDER_HITS; ?></td>
                    <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_SPIDER_MISSES; ?></td>
                    <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_SPIDER_EFFICIENCY; ?></td>
                  </tr>
<?php
      $cache_html_query = tep_db_query($cache_html_query_raw);
      $bCheck = false;
      while ($cache_html = tep_db_fetch_array($cache_html_query)) {
        $rows++;
        if( $cache_html['cache_html_type'] == 3 ) {
          $row_class = 'dataTableRowHigh';
        } elseif($cache_html['cache_html_type'] == 2) {
          $row_class = 'dataTableRowImpact';
        } else {
          $row_class = ($rows%2)?'dataTableRow':'dataTableRowSelected';
        }
        echo '                      <tr class="' . $row_class . '">';
?>
                    <td class="dataTableContent"><?php echo $cache_html['cache_html_script']; ?></td>
                    <td class="dataTableContent" align="center"><?php echo $cache_html['cache_hits']; ?></td>
                    <td class="dataTableContent" align="center"><?php echo $cache_html['cache_misses']; ?></td>
                    <td class="dataTableContent" align="right">
<?php 
        $total_access = $cache_html['cache_misses']+$cache_html['cache_hits'];

        if( $total_access <= 0 )
          $total_access = 1;

        $efficiency = tep_round( ($cache_html['cache_hits']*100)/$total_access, 2);
        echo $efficiency . '%';
?>
                    </td>
                    <td class="dataTableContent" align="center"><?php echo $cache_html['cache_spider_hits']; ?></td>
                    <td class="dataTableContent" align="center"><?php echo $cache_html['cache_spider_misses']; ?></td>
                    <td class="dataTableContent" align="right">
<?php 
        $total_access = $cache_html['cache_spider_misses']+$cache_html['cache_spider_hits'];
        if( $total_access <= 0 )
          $total_access = 1;

        $efficiency = tep_round( ($cache_html['cache_spider_hits']*100)/$total_access, 2);
        echo $efficiency . '%';
?>
                    </td>
                  </tr>
<?php
      }
?>
                </table></td>
              </tr>
              <tr>
                <td><?php echo '<a href="' . tep_href_link(FILENAME_CACHE_REPORTS, tep_get_all_get_params(array('action')) . 'action=truncate_html' ) . '">' . tep_image_button('button_delete.gif', 'Truncate Cache HTML Reports') . '</a>'; ?></td>
              </tr>
              <tr>
                <td><table border="0" width="100%" cellspacing="0" cellpadding="2">
                  <tr>
                    <td class="smallText" valign="top"><?php echo $cache_html_split->display_count($cache_html_query_numrows, MAX_DISPLAY_HTML_CACHE_SCRIPTS, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_CACHE_SCRIPTS); ?></td>
                    <td class="smallText" align="right"><?php echo $cache_html_split->display_links($cache_html_query_numrows, MAX_DISPLAY_HTML_CACHE_SCRIPTS, MAX_DISPLAY_PAGE_LINKS, $_GET['page'], tep_get_all_get_params(array('action', 'page')), 'page'); ?></td>
                  </tr>
                </table></td>
              </tr>
<?php 
    }
?>
            </table></td>
          </tr>
        </table></td>
      </tr>
<?php
  }
?>
    </table></td>
<!-- body_text_eof //-->
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
<br>
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
