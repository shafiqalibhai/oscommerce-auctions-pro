<?php
/*
  $Id: helpdesk.php,v 1.6 2005/08/16 21:14:04 lane Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

  require('includes/application_top.php');
  $action = (isset($_GET['action']) ? $_GET['action'] : '');

  switch ($_GET['action']) {
    case 'mass_delete_confirm':
      foreach ($_POST['entries_id'] as $entry_id=>$val) {
        $ticket = tep_db_prepare_input($entry_id);
        tep_db_query("delete from " . TABLE_HELPDESK_TICKETS . " where ticket = '" . tep_db_input($ticket) . "'");
        tep_db_query("delete from " . TABLE_HELPDESK_ENTRIES . " where ticket = '" . tep_db_input($ticket) . "'");
      }
      $messageStack->add_session(SUCCESS_WHOLE_THREAD_REMOVED, 'success');
      tep_redirect(tep_href_link(FILENAME_HELPDESK));
      break;
    default:
      break;
  }

?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<script language="javascript" type="text/javascript"><!--
  var g_checkbox = 0;
  function check_boxes(form) {
    for (var i = 0; i < form.elements.length; i++) {
      if( form.elements[i].type == "checkbox" ) {
        form.elements[i].checked = g_checkbox?"":"on";
      }
    }
    g_checkbox ^= 1;
  }
//--></script>
<script language="javascript" src="includes/general.js"></script>
</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF" onload="SetFocus();">
<!-- header //-->
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<!-- header_eof //-->

<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
    <td width="<?php echo BOX_WIDTH; ?>" valign="top"><table border="0" width="<?php echo BOX_WIDTH; ?>" cellspacing="1" cellpadding="1" class="columnLeft">
<!-- left_navigation //-->
<?php require(DIR_WS_INCLUDES . 'column_left.php'); ?>
<!-- left_navigation_eof //-->
    </table></td>
<!-- body_text //-->
    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td class="pageHeading"><?php echo HEADING_TITLE; ?></td>
            <td class="pageHeading" align="right"><?php echo tep_draw_separator('pixel_trans.gif', '1', HEADING_IMAGE_HEIGHT); ?></td>
          </tr>
        </table</td>
      </tr>
<?php
 if (!isset($date_from)) {
	$date_from = date('Y-m-d');
 };
 if (!isset($date_to)) {
	$time_to = strtotime($date_from) - 60 * 60 * 24 * 7;
	$date_to = date('Y-m-d', $time_to);
 };
?>
	  <tr>
<?php echo tep_draw_form('dates', FILENAME_HELPDESK_MASS_DELETE, '', 'get'); ?>
		<td>
	From <?php echo tep_draw_input_field('date_from', $date_from); ?>
	To <?php echo tep_draw_input_field('date_to', $date_to); ?>
    <input type=submit value='Go'>
<?php echo (isset($sort))?tep_draw_hidden_field('sort', $sort):''; ?>
		</td>
</form>
	  </tr>
<?
 $params = tep_get_all_get_params(array('sort'));
?>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top"><?php echo tep_draw_form('md', FILENAME_HELPDESK_MASS_DELETE, 'action=mass_delete_confirm'); ?><table border="0" width="100%" cellspacing="0" cellpadding="2">
              <tr class="dataTableHeadingRow">
                <td class="dataTableHeadingContent"><?php echo '<a href="javascript:void(0)" onClick="check_boxes(document.md)" title="Page Select On/Off">' . TABLE_HEADING_SELECT . '</a>'; ?></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_TICKET; ?>
 <a href=<?php echo tep_href_link(FILENAME_HELPDESK_MASS_DELETE, $params . 'sort=ticket:a'); ?>>a</a> 
<a href=<?php echo tep_href_link(FILENAME_HELPDESK_MASS_DELETE, $params . 'sort=ticket:d'); ?>>d</a>
</td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_EMAIL; ?>
 <a href=<?php echo tep_href_link(FILENAME_HELPDESK_MASS_DELETE, $params . 'sort=email:a'); ?>>a</a> 
<a href=<?php echo tep_href_link(FILENAME_HELPDESK_MASS_DELETE, $params . 'sort=email:d'); ?>>d</a>
</td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_SUBJECT; ?>
 <a href=<?php echo tep_href_link(FILENAME_HELPDESK_MASS_DELETE, $params . 'sort=subject:a'); ?>>a</a> 
<a href=<?php echo tep_href_link(FILENAME_HELPDESK_MASS_DELETE, $params . 'sort=subject:d'); ?>>d</a>
</td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_SENDER; ?>
 <a href=<?php echo tep_href_link(FILENAME_HELPDESK_MASS_DELETE, $params . 'sort=sender:a'); ?>>a</a> 
<a href=<?php echo tep_href_link(FILENAME_HELPDESK_MASS_DELETE, $params . 'sort=sender:d'); ?>>d</a>
</td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_DATE; ?>
 <a href=<?php echo tep_href_link(FILENAME_HELPDESK_MASS_DELETE, $params . 'sort=date:a'); ?>>a</a> 
<a href=<?php echo tep_href_link(FILENAME_HELPDESK_MASS_DELETE, $params . 'sort=date:d'); ?>>d</a>
</td>
              </tr>
<?php
	$order_by = ' datestamp DESC';
	if (isset($_GET['sort'])) {
		$list = explode(':', $_GET['sort']);
		if (is_array($list)) {
		switch($list[0]) {
			case 'ticket':
				$order_by = ' ticket';
				break;
			case 'email':
				$order_by = ' email_address';
				break;
			case 'subject':
				$order_by = ' subject';
				break;
			case 'sender':
				$order_by = ' sender';
				break;
			case 'date':
				$order_by = ' datestamp';
				break;
		};
		if ($list[1] == 'a') {
			$order_by .= ' ASC';
		} else {
			$order_by .= ' DESC';
		}; 
		} else {
			$order_by = ' datestamp';
		};
	};
	if (!empty($order_by)) {
		$order_by = ' order by ' . $order_by;
	};
 	$date_from = date('Y-m-d', strtotime($date_from) + 60 * 60 * 24);
    $entries_query = tep_db_query("select ticket, email_address, subject, sender, datestamp from " . TABLE_HELPDESK_ENTRIES . " WHERE datestamp > '$date_to' and datestamp < '$date_from' " . $order_by);
    $rows = 0;
    while ($entries = tep_db_fetch_array($entries_query)) {
      $rows++;
      $row_class = ($rows%2)?'dataTableRow':'dataTableRowSelected';
      echo '                  <tr class="' . $row_class . '">';

/*
      if (((!$_GET['ticket']) || ($_GET['ticket'] == $entries['ticket'])) && (!$tInfo) && (substr($_GET['action'], 0, 3) != 'new')) {
        $tInfo = new objectInfo(tep_array_merge($entries, $ticket));
      }

      if ( (is_object($tInfo)) && ($entries['ticket'] == $tInfo->ticket) ) {
        echo '                  <tr class="dataTableRowSelected" onmouseover="this.style.cursor=\'hand\'" onclick="document.location.href=\'' . tep_href_link(FILENAME_HELPDESK, 'page=' . $_GET['page'] . '&ticket=' . $tInfo->ticket . '&action=view') . '\'">' . "\n";
      } else {
        echo '                  <tr class="dataTableRow" onmouseover="this.className=\'dataTableRowOver\';this.style.cursor=\'hand\'" onmouseout="this.className=\'dataTableRow\'" onclick="document.location.href=\'' . tep_href_link(FILENAME_HELPDESK, 'page=' . $_GET['page'] . '&ticket=' . $entries['ticket']) . '\'">' . "\n";
      }
*/
?>
                <td class="dataTableContent"><?php echo tep_draw_checkbox_field('entries_id[' . $entries['ticket'] . ']', ($bCheck?'on':''), $bCheck ); ?></td>
                <td class="dataTableContent"><?php echo $entries['ticket']; ?></td>
                <td class="dataTableContent"><?php echo $entries['email_address']; ?></td>
                <td class="dataTableContent"><?php echo $entries['subject']; ?></td>
                <td class="dataTableContent"><?php echo $entries['sender']; ?></td>
                <td class="dataTableContent"><?php echo $entries['datestamp']; ?></td>
              </tr>
<?php
    }
?>
              <tr>
                <td colspan="5"><table border="0" width="100%" cellspacing="0" cellpadding="2">
                  <tr>
                    <td><?php echo '<a href="' . tep_href_link(FILENAME_HELPDESK) . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>&nbsp;' . tep_image_submit('button_confirm.gif', IMAGE_CONFIRM); ?></td>
                  </tr>
                </table></td>
              </tr>
            </table></form></td>
          </tr>
        </table></td>
      </tr>
    </table></td>
<!-- body_text_eof //-->
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
<br>
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
