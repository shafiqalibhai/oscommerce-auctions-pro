<?php
/*  ----------------------------------------------
    Purpose: Retrieve emails from POP3 server for each helpdesk account (Department)

    v0.2 20-Aug-05 by Lane Roathe (www.ifd.com)
    - display meaningful error messagaes when imap_open fails
    - fix usage of name/email address
    - output is now in a logfile format suitable for CRON usage
    - fixed bug where emails were always deleted (never checked the setting, relied on read-only connecton)
    - cleanup messages
    - cleanup code
    - document code & usage
    - fixed use of depriciated tep_merge_array

    LOG FORMAT:
    [Sat Mar 10 15:16:08 2001 -0600] [INFO|ERROR|WARNING|MISC] message

//----------------------------------------------------------------------------
// Copyright (c) 2006-2007 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
//----------------------------------------------------------------------------
// Modifications by Asymmetrics
// - Added Attachments support
// - IMap fixes to receive emails for PHP 5
// - osCommerce formatting added
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
  require('includes/application_top.php');
  include(DIR_WS_INCLUDES . 'classes/mime_decode.php');

  include_once( DIR_WS_FUNCTIONS . 'helpdesk.php' );

  //-MS- Garbage Collector Added
  tep_db_query("delete from " . TABLE_HELPDESK_LOG . " where (unix_timestamp(now()) - unix_timestamp(access_date)) > 10713600");
  //-MS- Garbage Collector Added EOM

    // We need the date for each message, make it easy to maintain
  define('DATETIME', date("D M j G:i:s Y O"));              // Sat Mar 10 15:16:08 2001 -0600

  // Make the output work for both the logfile as well as for the "interactive" use via a web browser
  define('CRLF', "<br />\r\n");
?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<script language="javascript" src="includes/general.js"></script>
</head>
<body>
<!-- header //-->
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<!-- header_eof //-->

<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
    <td width="<?php echo BOX_WIDTH; ?>" valign="top"><table border="0" width="<?php echo BOX_WIDTH; ?>" cellspacing="1" cellpadding="1" class="columnLeft">
<!-- left_navigation //-->
<?php require(DIR_WS_INCLUDES . 'column_left.php'); ?>
<!-- left_navigation_eof //-->
    </table></td>
<!-- body_text //-->
    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td class="pageHeading"><?php echo HEADING_TITLE; ?></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><table border="0" width="100%" cellspacing="1" cellpadding="3">
<?php
  // display the run
  if( 'true' == DEFAULT_HELPDESK_DISPLAY_HEADER ) {
    echo '<tr class="dataTableRow">';
    echo '  <td class="dataTableContent">['. DATETIME . '] [MISC] +++ Helpdesk Mail retrieval system version 0.2' . '</td>';
    echo '</tr>';
    if( 'true' == DEFAULT_HELPDESK_DELETE_EMAILS ) {
      echo '<tr class="dataTableRow">';
      echo '  <td class="dataTableContent">' . '['. DATETIME . '] [MISC] +++ Emails will be deleted from the server +++' . '</td>';
      echo '</tr>';
    }
  }

  // loop through all of the helpdesk departments so we can query each one's server
  $account_query = tep_db_query("select * from " . TABLE_HELPDESK_DEPARTMENTS);

  $total_messages = 0;
  while( $account = tep_db_fetch_array($account_query) ) {
    $department = $account['title'];
    $username = $account['name'];
    $password = $account['password'];
    $mailbox = '';
    
    // No need to process the department if not setup to do so
    $emailaddress = $account['email_address'];
    if( empty($username) ) {
      echo '<tr class="dataTableRow">';
      echo '  <td class="dataTableContent">' . '['. DATETIME . '] [WARNING] No email account setup for [' . $department . ']' . '</td>';
      echo '</tr>';
      continue;
    }
    echo '<tr class="dataTableRow">';
    echo '  <td class="dataTableContent">' . '[' . DATETIME . '] [INFO] Processing emails for ['. $department . '] ('. $emailaddress . '/' . $username . ')' . '</td>';
    echo '</tr>';
    
    // See if we want to open the connection read-only or not.
    if( 'true' == DEFAULT_HELPDESK_DELETE_EMAILS || 'false' == DEFAULT_HELPDESK_READ_ONLY ) {
      $mode = CL_EXPUNGE;
    } else {
      $mode = OP_READONLY;
    }
    // Try to open the connection
    if ($conn = imap_open("{".DEFAULT_HELPDESK_MAILSERVER.DEFAULT_HELPDESK_PROTOCOL_SPECIFICATION."}".$mailbox, $username, $password, $mode)) {
      // setup the mime decoding for all of the messages
      $params = array();
      $params['decode_headers'] = true;
      $params['crlf']           = "\r\n";
      $params['include_bodies'] = true;
      $params['decode_bodies']  = true;
      
      // if we have any messages, start going through them
      if ($msgCount = imap_num_msg($conn)) {
        $total_messages += $msgCount;
        echo '<tr class="dataTableRow">';
        echo '  <td class="dataTableContent">' . '[' . DATETIME . '] [INFO] Found [' . $msgCount . '] new messages' . '</td>';
        echo '</tr>';
        // Process messages
        for($i = 1; $i <= $msgCount; $i++) {
//-MS- Added Attachments support
          //fetch structure of message
          $attachments_array = array();
          $parts_array = array();
          $mail_struct=imap_fetchstructure($conn,$i);
          //see if there are any parts
          if( isset($mail_struct->parts) && is_array($mail_struct->parts) ) {
            foreach ($mail_struct->parts as $partno=>$partarr) {
              echo '<tr class="dataTableRow">';
              echo '  <td class="dataTableContent">' . 'Processing Main Index: ' . $i . '<td>';
              echo '</tr>';
              //parse parts of email
              $attachment = help_desk_parsepart($partarr,$partno+1, $conn, $i, $parts_array);
              if( is_string($attachment) && !in_array($attachment, $attachments_array) ) {
                $attachments_array[] = help_desk_parsepart($partarr,$partno+1, $conn, $i, $parts_array);
              }
            }
          }
//-MS- Added Attachments support EOM
          // if desired, mark the message we are going to process for deletion
          if( 'true' == DEFAULT_HELPDESK_DELETE_EMAILS )
              imap_delete($conn, $i);     
          
          // Get the message header
          $header = imap_fetchheader($conn, $i, FT_PREFETCHTEXT);
          
          // and the body, marking as seen if desired (no effect if messages deleted of course!)
          if ( 'true' == DEFAULT_HELPDESK_MARKSEEN )
            $body = imap_body($conn, $i);
          else
            $body = imap_body($conn, $i, FT_PEEK);
          
          // get some readable text from the mime input
          $params['input'] = $header.$body;

          //-MS- Changed
          $tmp_obj = new Mail_mimeDecode($params['input'], "\r\n");
          $output = $tmp_obj->decode($params);
          //$output = Mail_mimeDecode::decode($params);
          //-MS- Changed EOM

          // Some mail servers and clients use special messages for holding mailbox data; ignore that message if it exists.
          // !! There has to be a more server independent method to check for this... right?
          if ($message->headers['subject'] != "DON'T DELETE THIS MESSAGE -- FOLDER INTERNAL DATA") {
            // Does the message have an attachment?
            if (strtolower($message->ctype_primary) == "multipart") {
              // yes, we need to make that part of the body
              $body = trim($message->parts[0]->body);
              $attachCount = count($message->parts) - 1;
              $attachSize  = 0;
              
              // combine the parts
              for($p = 1; $p < count($message->parts); $p++)
               $attachSize += strlen($message->parts[$p]->body);
              
              // append the attachment to the body
              $body .= "    [Message contains 1 attachment. (".translateSize($attachSize).")]";
            } else {
              // no attachments, just get the body
              $body = trim($message->body);
            }
            // see if we need to cut down on the size of the message
            if (DEFAULT_HELPDESK_BODY_SIZE_LIMIT && strlen($body) > DEFAULT_HELPDESK_BODY_SIZE_LIMIT) {
              $body = substr($body, 0, DEFAULT_HELPDESK_BODY_SIZE_LIMIT).'...';
            }
            // create email friendly text
            $body = nl2br($body);                    
          }

          // create the osc parts for an email
          $parts = array();
          osc_parse_mime_decode_output($output, $parts);
          
          // get rid of things that will confuse php          
          $field_body = trim($parts['text'][0]);
          
          // make sure we have a date
          if (empty($output->headers['date'])) {
            $field_date = date("Y-m-d H:i:s");
          } else {
            $field_date = date("Y-m-d H:i:s", strtotime($output->headers['date'], time()));
          }
          // if there is an IP address present, grab it & the host name as well if we can
          $received_headers = array();
          if( !is_array($output->headers['received']) ) {
            $received_headers[] = $output->headers['received'];
          } else {
            $received_headers = $output->headers['received'];
          }

          foreach( $received_headers as $header ) {
            if (preg_match('/([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+)/i', $header, $regs)) {
              $field_ip = $regs[1];
              $field_host = @gethostbyaddr($field_ip);
              break;
            } else {
              $field_ip = '';
              $field_host = '';
            }
          }

          // Make sure we try had to have a valid from email address
          if (empty($output->headers['from'])) {
              $field_from = '';
              $field_from_email_address = '';
          } else {
            if (preg_match('/"([^"]+)" <([^>]+)>/i', $output->headers['from'], $regs)) {
              $field_from = trim($regs[1]);
              $field_from_email_address = trim($regs[2]);
            } elseif (preg_match('/([^<]+)<([^>]+)>/i', $output->headers['from'], $regs)) {
              $field_from = trim($regs[1]);
              $field_from_email_address = trim($regs[2]);
            } elseif (substr($output->headers['from'], 0, 1) == '<') {
              $field_from = substr($output->headers['from'], 1, -1);
              $field_from_email_address = $field_from;
            } else {
              $field_from = $output->headers['from'];
              $field_from_email_address = $field_from;
            }
          }
          
          // Also try hard to fill in the to email address properly
          if (empty($output->headers['to'])) {
            $field_to = '';
            $field_to_email_address = '';
          } else {
            if (preg_match('/"([^"]+)" <([^>]+)>/i', $output->headers['to'], $regs)) {
              $field_to = trim($regs[1]);
              $field_to_email_address = trim($regs[2]);
            }elseif (preg_match('/([^<]+)<([^>]+)>/i', $output->headers['to'], $regs)) {
              $field_to = trim($regs[1]);
              $field_to_email_address = trim($regs[2]);
            } elseif (substr($output->headers['to'], 0, 1) == '<') {
              $field_to = substr($output->headers['to'], 1, -1);
              $field_to_email_address = $field_to;
            } else {
              $field_to = $output->headers['to'];
              $field_to_email_address = $field_to;
            }
          }
          
          // we're set, setup to store in DB
          $ticket = false;
          $parent_id = '0';
          $status_id = DEFAULT_HELPDESK_STATUS_ID;
          $priority_id = DEFAULT_HELPDESK_PRIORITY_ID;
          
          // Get the department ID. No need for a DB lookup, we already did that!
          $department_id = $account['department_id'];

          $field_message_id = trim($output->headers['message-id']);
          $field_subject = trim($output->headers['subject']);
          
          // check if the email already in the database
          $existing_query = tep_db_query("select message_id from " . TABLE_HELPDESK_ENTRIES . " where message_id='" . addslashes($field_message_id) . "'");
          if(!tep_db_num_rows($existing_query)) {
            // check for existing ticket number
            if (preg_match('/^.*\['.DEFAULT_HELPDESK_TICKET_PREFIX.'([A-Z0-9]{7})\].*$/i', $field_subject, $regs)) {
              $ticket = $regs[1];
              $ticket_info_query = tep_db_query("select he.parent_id, ht.department_id, ht.status_id, ht.priority_id from " . TABLE_HELPDESK_ENTRIES . " he left join " . TABLE_HELPDESK_TICKETS . " ht on (he.ticket = ht.ticket) where he.ticket = '" . $ticket . "' order by he.parent_id desc limit 1");
              // if we find one, setup as our parent ticket (limit 1 w/sort ensures this is correct)
              if (tep_db_num_rows($ticket_info_query)) {
                  $ticket_info = tep_db_fetch_array($ticket_info_query);
                  $parent_id = $ticket_info['parent_id'];
                  $status_id = $ticket_info['status_id'];
                  $priority_id = $ticket_info['priority_id'];
                  $department_id = $ticket_info['department_id'];
              }
            } else {       
              // Create a unique ticked ID and make sure it's available
              while (true) {
                // create & check for dups until unique
                $ticket = osc_create_random_string();
                $check_query = tep_db_query("select count(*) as count from " . TABLE_HELPDESK_TICKETS . " where ticket = '" . $ticket . "'");
                $check = tep_db_fetch_array($check_query);

                // if unique we can break the while loop
                if ($check['count'] < 1) {
                  break;
                }
              }
            }
            // how we insert the ticked depends on whether or not this is the first ticket
            // !! this is a completely wasteful DB query; use the data from above to make this decision!
            $check_query = tep_db_query("select count(*) as count from " . TABLE_HELPDESK_TICKETS . " where ticket = '" . $ticket . "'");
            $check = tep_db_fetch_array($check_query);
            
            // if this is the first ticket, we need to setup the "master" ticket item for tracking purposes          
            if ($check['count'] < 1) {
              $sql_data_array = array(
                                      'ticket' => tep_db_prepare_input($ticket),
                                      'department_id' => (int)$department_id,
                                      'priority_id' => DEFAULT_HELPDESK_PRIORITY_ID,
                                      'status_id' => DEFAULT_HELPDESK_STATUS_ID,
                                      'datestamp_last_entry' => 'now()',
                                     );
              tep_db_perform(TABLE_HELPDESK_TICKETS, $sql_data_array);

              echo '<tr class="dataTableRow">';
              echo '  <td class="dataTableContent">' . '[' . DATETIME . '] [_NEW_] [' . $ticket . '] - ' . $field_subject . '</td>';
              echo '</tr>';
            }

            // FINALLY -- we can now save this email as ticket entry!
            $sql_data_array = array(
                                    'ticket' => tep_db_prepare_input($ticket),
                                    'parent_id' => (int)$parent_id,
                                    'message_id' => tep_db_prepare_input($field_message_id),
                                    'ip_address' => tep_db_prepare_input($field_ip),
                                    'host' => tep_db_prepare_input($field_host),
                                    'datestamp_local' => 'now()',
                                    'datestamp' => tep_db_prepare_input($field_date),
                                    'receiver' => tep_db_prepare_input($field_to),
                                    'receiver_email_address' => tep_db_prepare_input($field_to_email_address),
                                    'sender' => tep_db_prepare_input($field_from),
                                    'email_address' => tep_db_prepare_input($field_from_email_address),
                                    'subject' => tep_db_prepare_input($field_subject),
                                    'body' => tep_db_prepare_input($field_body),
                                    'entry_read' => '0',
                                   );
            tep_db_perform(TABLE_HELPDESK_ENTRIES, $sql_data_array);
            $helpdesk_entry_id = tep_db_insert_id();

            for($x = 0, $y = count($attachments_array); $x<$y; $x++ ) {
              $sql_data_array = array(
                                      'helpdesk_entries_id' => (int)$helpdesk_entry_id,
                                      'ticket' => tep_db_prepare_input($ticket),
                                      'attachment' => tep_db_prepare_input($attachments_array[$x]),
                                     );
              tep_db_perform(TABLE_HELPDESK_ATTACHMENTS, $sql_data_array);
            }
/*
            tep_db_query("insert into " . TABLE_HELPDESK_ENTRIES . " (helpdesk_entries_id, ticket, parent_id, message_id, ip_address, host, datestamp_local, datestamp, receiver, receiver_email_address, sender, email_address, subject, body, entry_read) values ('', '" . $ticket . "', '" . $parent_id . "', '" . addslashes($field_message_id) . "', '" . addslashes($field_ip) . "', '" . addslashes($field_host) . "', now(), '" . addslashes($field_date) . "', '" . addslashes($field_to) . "', '" . addslashes($field_to_email_address) . "', '" . addslashes($field_from) . "', '" . addslashes($field_from_email_address) . "', '" . addslashes($field_subject) . "', '" . addslashes($field_body) . "', '0')");
*/
            echo '<tr class="dataTableRow">';
            echo '  <td class="dataTableContent">' . '[' . DATETIME . '] [UPDATE] [' . $ticket . '] - [' . $parent_id . '] - ' . $field_subject . '</td>' . "\n";
            echo '</tr>';
          }
          // !! NOT useful echo "Messages after delete: " . $conn->Nmsgs . "".CRLF;
        }
      } else {
        echo '<tr class="dataTableRow">';
        echo '  <td class="dataTableContent">' . '['. DATETIME . '] [INFO] Found [0] new messages -- No mail for this dept. on the server.' . '</td>';
        echo '</tr>';
      }
      imap_close($conn, $mode);
    } else {
      echo '<tr class="dataTableRow">';
      echo '  <td class="dataTableContent">' . '[' . DATETIME . '] [ERROR] Unable to connect: ' . imap_last_error() . '</td>';
      echo '</tr>';
    }
  }

  $name = 'Unknown';
  $names_query = tep_db_query("select concat(admin_firstname, ' ', admin_lastname) as final_name from " . TABLE_ADMIN . " where admin_id = '" . (int)$login_id . "'");
  if( $names_array = tep_db_fetch_array($names_query) ) {
    $name = $names_array['final_name'];
  }
  $sql_data_array = array(
                          'helpdesk_log_name' => tep_db_prepare_input($name),
                          'msg_count' => (int)$total_messages,
                          'access_date' => 'now()',
                         );
  tep_db_perform(TABLE_HELPDESK_LOG, $sql_data_array);
?>
    </table></td>
<!-- body_text_eof //-->
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
<br>
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
