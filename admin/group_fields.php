<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2008 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Admin: Group Fields Generator/Controller
//----------------------------------------------------------------------------
// I-Metrics Layer
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
  require('includes/application_top.php');
  $group_types_array = array(
                             array('id' => '1', 'text' => 'STATIC-TEXT'),
                             array('id' => '2', 'text' => 'RADIO'),
                             array('id' => '3', 'text' => 'RADIO-IMAGE'),
                             array('id' => '4', 'text' => 'DROP-DOWN'),
                             array('id' => '5', 'text' => 'INPUT-LINE'),
                             array('id' => '6', 'text' => 'TEXT-AREA'),
                             array('id' => '7', 'text' => 'CHECK-BOX'),
                            );

  $group_layout_array = array(
                             array('id' => '1', 'text' => 'LINEAR'),
                             array('id' => '2', 'text' => 'MATRIX'),
                            );

  $action = (isset($_GET['action']) ? $_GET['action'] : 'list');
  $fID = (isset($_GET['fID']) ? $_GET['fID'] : '');
  $oID = (isset($_GET['oID']) ? $_GET['oID'] : '');

  if (isset($_POST['delete_x']) || isset($_POST['delete_y'])) {
    $action='delete';
  } elseif (isset($_POST['delete_option_x']) || isset($_POST['delete_option_y'])) {
    $action='delete_option';
  } elseif (isset($_POST['delete_value_x']) || isset($_POST['delete_value_y'])) {
    $action='delete_value';
  }


  switch ($action) {
    case 'set_flag':
      $sql_data_array = array('status_id' => (int)$_GET['flag']);
      tep_db_perform(TABLE_GROUP_FIELDS, $sql_data_array, 'update', 'group_fields_id=' . (int)$_GET['id']);
      tep_redirect(tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('action', 'flag', 'id')) ));
      break;
    case 'insert':
      if( tep_not_null($_POST['name']) && $_POST['limit'] > 0) {
        $sql_data_array = array(
                                'group_fields_name' => tep_db_prepare_input($_POST['name']),
                                'group_fields_description' => tep_db_prepare_input($_POST['description']),
                                'layout_id' => (int)$_POST['layout'],
                                'limit_id' => (int)$_POST['limit'],
                                'sort_id' => (int)$_POST['order']
                               );

        tep_db_perform(TABLE_GROUP_FIELDS, $sql_data_array, 'insert');
        $new_group_field_id = tep_db_insert_id();
        tep_redirect(tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('action', 'fID')) . 'action=list&fID=' . $new_group_field_id ));
      }
      tep_redirect(tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('action', 'fID')) . 'action=list'));
      break;
    case 'update':
      if(isset($_POST['mark']) && is_array($_POST['mark']) ) {
        foreach ($_POST['mark'] as $key => $val) {
          if( !tep_not_null($_POST['name'][$key]) || $_POST['limit'][$key] < 1 ) {
            continue;
          }

          $check_query = tep_db_query("select group_fields_id from " . TABLE_GROUP_FIELDS . " where group_fields_id = '" . (int)$key . "'");
          if( $check_array = tep_db_fetch_array($check_query) ) {
            $sql_data_array = array(
                                    'group_fields_name' => tep_db_prepare_input($_POST['name'][$key]),
                                    'group_fields_description' => tep_db_prepare_input($_POST['description'][$key]),
                                    'layout_id' => (int)$_POST['layout'][$key],
                                    'limit_id' => (int)$_POST['limit'][$key],
                                    'sort_id' => (int)$_POST['order'][$key]
                                   );
            tep_db_perform(TABLE_GROUP_FIELDS, $sql_data_array, 'update', "group_fields_id= '" . (int)$key . "'");
          }
        }
      }
      tep_redirect(tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('action')) . 'action=list' ));
      break;
    case 'delete':
      if( !isset($_POST['mark']) || !is_array($_POST['mark']) || !count($_POST['mark']) ) {
        tep_redirect(tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('action')) . 'action=list'));
      }
      break;
    case 'delete_confirm':
      if(isset($_POST['mark']) && is_array($_POST['mark']) ) {
        foreach ($_POST['mark'] as $key=>$val) {
          $check_query = tep_db_query("select group_fields_id from " . TABLE_GROUP_FIELDS . " where group_fields_id=" . (int)$key);
          if( $check_array = tep_db_fetch_array($check_query) ) {
            tep_db_query("delete from " . TABLE_PRODUCTS_TO_GROUP_FIELDS . " where group_fields_id=" . (int)$key);
            tep_db_query("delete from " . TABLE_GROUP_VALUES . " where group_fields_id=" . (int)$key);
            tep_db_query("delete from " . TABLE_GROUP_OPTIONS . " where group_fields_id=" . (int)$key);
            tep_db_query("delete from " . TABLE_GROUP_FIELDS . " where group_fields_id=" . (int)$key);
          }
        }
        if( isset($_POST['mark'][$fID]) ) {
          tep_redirect(tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('action')) . 'action=list' ));
        }
      }
      tep_redirect(tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('action', 'fID')) . 'action=list&fID=' . $fID ));
      break;

    case 'duplicate':
      $group_fields_query = tep_db_query("select * from " . TABLE_GROUP_FIELDS . " where group_fields_id = '" . (int)$fID . "'");
      if( $group_fields_array = tep_db_fetch_array($group_fields_query) ) {
        unset($group_fields_array['group_fields_id']);
        tep_db_perform(TABLE_GROUP_FIELDS, $group_fields_array, 'insert');
        $new_group_field_id = tep_db_insert_id();

        $option_fields_query = tep_db_query("select * from " . TABLE_GROUP_OPTIONS . " where group_fields_id = '" . (int)$fID . "'");
        while($option_fields_array = tep_db_fetch_array($option_fields_query) ) {
          $org_option_field_id = $option_fields_array['group_options_id'];
          unset($option_fields_array['group_options_id']);
          $option_fields_array['group_fields_id'] = $new_group_field_id;
          tep_db_perform(TABLE_GROUP_OPTIONS, $option_fields_array, 'insert');
          $new_option_field_id = tep_db_insert_id();

          $value_fields_query = tep_db_query("select * from " . TABLE_GROUP_VALUES . " where group_options_id = '" . (int)$fID . "' and group_options_id = '" . (int)$org_option_field_id . "'");
          while($value_fields_array = tep_db_fetch_array($value_fields_query) ) {
            unset($value_fields_array['group_values_id']);
            $value_fields_array['group_fields_id'] = $new_group_field_id;
            $value_fields_array['group_options_id'] = $new_option_field_id;
            tep_db_perform(TABLE_GROUP_VALUES, $value_fields_array, 'insert');
          }
        }
        tep_redirect(tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('action', 'fID')) . 'action=list&fID=' . $new_group_field_id ));
      }
      tep_redirect(tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('action', 'fID')) . 'action=list'));
      break;

    case 'set_option_flag':
      $sql_data_array = array('status_id' => (int)$_GET['flag']);
      tep_db_perform(TABLE_GROUP_OPTIONS, $sql_data_array, 'update', "group_fields_id = '" . (int)$fID . "' and group_options_id= '" . (int)$_GET['id'] . "'");
      tep_redirect(tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('action', 'flag', 'id')) . 'action=options_list'));
      break;

    case 'insert_option':
      if( tep_not_null($_POST['name']) && $_POST['limit'] > 0) {
        $check_query = tep_db_query("select group_fields_id from " . TABLE_GROUP_FIELDS . " where group_fields_id = '" . (int)$fID . "'");
        if( $check_array = tep_db_fetch_array($check_query) ) {
          $sql_data_array = array(
                                  'group_fields_id' => (int)$fID,
                                  'group_types_id' => (int)$_POST['type'],
                                  'group_options_name' => tep_db_prepare_input($_POST['name']),
                                  'price_status' => (isset($_POST['price'])?1:0),
                                  'stock_status' => (isset($_POST['stock'])?1:0),
                                  'image_status' => (isset($_POST['image'])?1:0),
                                  'layout_id' => (int)$_POST['layout'],
                                  'limit_id' => (int)$_POST['limit'],
                                  'sort_id' => (int)$_POST['order']
                                 );
          tep_db_perform(TABLE_GROUP_OPTIONS, $sql_data_array, 'insert');
          $new_group_option_id = tep_db_insert_id();
          tep_redirect(tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('action', 'fID', 'oID')) . 'action=options_list&fID=' . $fID . '&oID=' . $new_group_option_id));
        }
      }
      tep_redirect(tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('action','fID','oID')) . 'action=options_list&fID=' . $fID));
      break;
    case 'update_option':
      if(isset($_POST['mark']) && is_array($_POST['mark']) ) {
        foreach ($_POST['mark'] as $key => $val) {
          if( !tep_not_null($_POST['name'][$key]) || $_POST['limit'][$key] < 1 ) {
            continue;
          }

          $sql_data_array = array(
                                  'group_options_name' => tep_db_prepare_input($_POST['name'][$key]),
                                  'group_types_id' => (int)$_POST['type'][$key],
                                  'price_status' => (isset($_POST['price'][$key])?1:0),
                                  'stock_status' => (isset($_POST['stock'][$key])?1:0),
                                  'image_status' => (isset($_POST['image'][$key])?1:0),
                                  'layout_id' => (int)$_POST['layout'][$key],
                                  'limit_id' => (int)$_POST['limit'][$key],
                                  'sort_id' => (int)$_POST['order'][$key]
                                 );
          tep_db_perform(TABLE_GROUP_OPTIONS, $sql_data_array, 'update', "group_fields_id = '" . (int)$fID . "' and group_options_id= '" . (int)$key . "'");
        }
      }
      tep_redirect(tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('action')) . 'action=options_list' ));
      break;
    case 'delete_option':
      if( !isset($_POST['mark']) || !is_array($_POST['mark']) || !count($_POST['mark']) ) {
        tep_redirect(tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('action')) . 'action=options_list'));
      }
      break;
    case 'delete_option_confirm':
      $check_query = tep_db_query("select group_fields_id from " . TABLE_GROUP_FIELDS . " where group_fields_id= '" . (int)$fID . "'");
      if( $check_array = tep_db_fetch_array($check_query) ) {
        if(isset($_POST['mark']) && is_array($_POST['mark']) ) {
          foreach ($_POST['mark'] as $key=>$val) {
            tep_db_query("delete from " . TABLE_GROUP_OPTIONS . " where group_options_id= '" . (int)$key . "' and group_fields_id= '" . (int)$fID . "'");
            tep_db_query("delete from " . TABLE_GROUP_VALUES . " where group_options_id= '" . (int)$key . "' and group_fields_id= '" . (int)$fID . "'");
          }
        }
      }
      tep_redirect(tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('action', 'oID')) . 'action=options_list'));
      break;

    case 'duplicate_option':
      $option_fields_query = tep_db_query("select * from " . TABLE_GROUP_OPTIONS . " where group_fields_id = '" . (int)$fID . "' and group_options_id = '" . (int)$oID . "'");
      if($option_fields_array = tep_db_fetch_array($option_fields_query) ) {
        unset($option_fields_array['group_options_id']);
        tep_db_perform(TABLE_GROUP_OPTIONS, $option_fields_array, 'insert');
        $new_option_field_id = tep_db_insert_id();

        $value_fields_query = tep_db_query("select * from " . TABLE_GROUP_VALUES . " where group_fields_id = '" . (int)$fID . "' and group_options_id = '" . (int)$oID . "'");
        while($value_fields_array = tep_db_fetch_array($value_fields_query) ) {
          unset($value_fields_array['group_values_id']);
          $value_fields_array['group_options_id'] = $new_option_field_id;
          tep_db_perform(TABLE_GROUP_VALUES, $value_fields_array, 'insert');
        }
        tep_redirect(tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('action', 'oID')) . 'action=options_list&oID=' . $new_option_field_id));
      }
      tep_redirect(tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('action', 'oID')) . 'action=options_list'));
      break;

    case 'set_value_flag':
      $sql_data_array = array('status_id' => (int)$_GET['flag']);
      tep_db_perform(TABLE_GROUP_VALUES, $sql_data_array, 'update', "group_fields_id = '" . (int)$fID . "' and group_options_id = '" . (int)$oID . "' and group_values_id= '" . (int)$_GET['id'] . "'");
      tep_redirect(tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('action', 'flag', 'id')) . 'action=values_list'));
      break;

    case 'insert_value':
      $check_query = tep_db_query("select group_options_id, image_status from " . TABLE_GROUP_OPTIONS . " where group_fields_id = '" . (int)$fID . "' and group_options_id = '" . (int)$oID . "'");
      if( $check_array = tep_db_fetch_array($check_query) ) {
        $sql_data_array = array(
                                'group_fields_id' => (int)$fID,
                                'group_options_id' => (int)$oID,
                                'group_values_name' => tep_db_prepare_input($_POST['name']),
                                'group_values_price' => (isset($_POST['price']))?tep_db_prepare_input($_POST['price']):'',
                                'group_values_stock' => (isset($_POST['stock']))?tep_db_prepare_input($_POST['stock']):'',
                                'sort_id' => (int)$_POST['order']
                               );
        tep_db_perform(TABLE_GROUP_VALUES, $sql_data_array, 'insert');
        $new_group_value_id = tep_db_insert_id();

        if( $check_array['image_status'] == '1' ) {
          $cImage = new upload('image', DIR_FS_CATALOG_IMAGES);
          if( $cImage->c_result ) {
            tep_db_query("update " . TABLE_GROUP_VALUES . " set group_values_image = '" . tep_db_input($cImage->filename) . "' where group_values_id = '" . (int)$new_group_value_id . "'");
          }
        }
      }
      tep_redirect(tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('action')) . 'action=values_list' ));
      break;
    case 'update_value':
      if(isset($_POST['mark']) && is_array($_POST['mark']) ) {
        foreach ($_POST['mark'] as $key => $val) {
          $sql_data_array = array(
                                  'group_values_name' => tep_db_prepare_input($_POST['name'][$key]),
                                  'group_values_price' => (isset($_POST['price'][$key]))?tep_db_prepare_input($_POST['price'][$key]):'',
                                  'group_values_stock' => (isset($_POST['stock'][$key]))?tep_db_prepare_input($_POST['stock'][$key]):'',
                                  'sort_id' => (int)$_POST['order'][$key]
                                 );
          tep_db_perform(TABLE_GROUP_VALUES, $sql_data_array, 'update', "group_fields_id = '" . (int)$fID . "' and group_options_id= '" . (int)$oID . "' and group_values_id = '" . (int)$key . "'");
          if( isset($_FILES['image_' . $key]) && tep_not_null($_FILES['image_' . $key]['name']) ) {
            $cImage = new upload('image_' . $key, DIR_FS_CATALOG_IMAGES);
            if( $cImage->c_result ) {
              tep_db_query("update " . TABLE_GROUP_VALUES . " set group_values_image = '" . tep_db_input($cImage->filename) . "' where group_fields_id = '" . (int)$fID . "' and group_options_id= '" . (int)$oID . "' and group_values_id = '" . (int)$key . "'");
            }
          }
        }
      }
      tep_redirect(tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('action')) . 'action=values_list' ));
      break;
    case 'delete_value':
      if( !isset($_POST['mark']) || !is_array($_POST['mark']) || !count($_POST['mark']) ) {
        tep_redirect(tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('action')) . 'action=values_list'));
      }
      break;
    case 'delete_value_confirm':
      if(isset($_POST['mark']) && is_array($_POST['mark']) ) {
        foreach ($_POST['mark'] as $key=>$val) {
          tep_db_query("delete from " . TABLE_GROUP_VALUES . " where group_values_id= '" . (int)$key . "' and group_fields_id= '" . (int)$fID . "' and group_options_id = '" . (int)$oID . "'");
        }
      }
      tep_redirect(tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('action')) . 'action=values_list'));
      break;
    case 'remove_values_image':
      if( isset($_GET['pm_id']) && tep_not_null($_GET['pm_id']) ) {
        $check_query = tep_db_query("select group_values_image from " . TABLE_GROUP_VALUES . " where group_values_id= '" . (int)$_GET['pm_id'] . "' and group_fields_id= '" . (int)$fID . "' and group_options_id = '" . (int)$oID . "'");
        if( $check_array = tep_db_fetch_array($check_query) ) {
          if(strlen($check_array['group_values_image']) > 4 && file_exists(DIR_FS_CATALOG_IMAGES . $check_array['group_values_image'])) {
            @unlink(DIR_FS_CATALOG_IMAGES . $check_array['group_values_image']);
            clearstatcache();
          }
          tep_db_query("update " . TABLE_GROUP_VALUES . " set group_values_image = '' where group_values_id= '" . (int)$_GET['pm_id'] . "' and group_fields_id= '" . (int)$fID . "' and group_options_id = '" . (int)$oID . "'");
        }
      }

      tep_redirect(tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('action', 'pm_id')) . 'action=values_list'));
      break;

    case 'values_list':
      break;
    case 'options_list':
      break;
    case 'list':
      break;
    default:
      $action = 'list';
      break;
  }

?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<script language="javascript" src="includes/general.js"></script>
<script language="javascript">
  var g_checkbox2 = 0;
  function copy_checkboxes(form, array_name) {
    for (var i = 0; i < form.elements.length; i++) {
      if( form.elements[i].type == "checkbox" ) {
        check_name = form.elements[i].name;
        if( array_name == check_name.substring(0, array_name.length) ) {
          form.elements[i].checked = g_checkbox2?"":"on";
        }
      }
    }
    g_checkbox2 ^= 1;
  }
</script>
</head>
<body>
<!-- header //-->
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<!-- header_eof //-->

<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
    <td width="<?php echo BOX_WIDTH; ?>" valign="top"><table border="0" width="<?php echo BOX_WIDTH; ?>" cellspacing="1" cellpadding="1" class="columnLeft">
<!-- left_navigation //-->
<?php require(DIR_WS_INCLUDES . 'column_left.php'); ?>
<!-- left_navigation_eof //-->
    </table></td>
<!-- body_text //-->
    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
<?php
  if($action == 'list') {
?>
        <td width="75%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
          <tr>
            <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
              <tr>
                <td class="pageHeading"><?php echo HEADING_GROUP_UPDATE; ?></td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td class="smallText"><?php echo TEXT_INFO_UPDATE; ?></td>
          </tr>
          <tr>
            <td class="formArea"><?php echo tep_draw_form('fields_update', basename($PHP_SELF),'action=update', 'post'); ?><table border="0" width="100%" cellspacing="1" cellpadding="3">
              <tr class="dataTableHeaderForm">
                <td class="dataTableHeadingContent" width="5%"><?php echo '<a href="javascript:void(0)" onClick="copy_checkboxes(document.fields_update,\'mark\')" title="Page Select On/Off" class="menuBoxHeadingLink"><span class="dataTableHeadingContent">' . tep_image(DIR_WS_ICONS . 'tick.gif', 'Page Select On/Off') . '</span></a>'; ?></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_NAME; ?></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_DESCRIPTION; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_LAYOUT; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_LIMITER; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_ORDER; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_STATUS; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_ACTION; ?></td>
              </tr>
<?php
    $group_fields_query = tep_db_query("select group_fields_id, group_fields_name, group_fields_description, layout_id, limit_id, sort_id, status_id from " . TABLE_GROUP_FIELDS . " order by sort_id");
    while ($group_field = tep_db_fetch_array($group_fields_query)) {

      if ((!isset($_GET['fID']) || (isset($_GET['fID']) && ($_GET['fID'] == $group_field['group_fields_id']))) && !isset($gfInfo) ) {
        $group_field['selects'] = array();
        $check_query = tep_db_query("select group_options_name from " . TABLE_GROUP_OPTIONS . " where group_fields_id = '" . (int)$group_field['group_fields_id'] . "' order by sort_id");
        if( tep_db_num_rows($check_query) ) {
          while($check_array = tep_db_fetch_array($check_query)) {
            $group_field['selects'][] = $check_array['group_options_name'];
          }
        } else {
          $group_field['selects'][] = 'No Options Set';
        }
        $gfInfo = new objectInfo($group_field);
      }

      if (isset($gfInfo) && is_object($gfInfo) && ($group_field['group_fields_id'] == $gfInfo->group_fields_id)) {
        echo '                  <tr id="defaultSelected" class="dataTableRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)">' . "\n";
      } else {
        echo '                  <tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)">' . "\n";
      }
?>
                <td width="20"><?php echo tep_draw_checkbox_field('mark['.$group_field['group_fields_id'].']', 1) ?></td>
                <td class="dataTableContent"><?php echo tep_draw_input_field('name['.$group_field['group_fields_id'].']', $group_field['group_fields_name'], 'size=30', false, 'text', true);?></td>
                <td class="dataTableContent"><?php echo tep_draw_textarea_field('description['.$group_field['group_fields_id'].']', 'soft', '60', '1', $group_field['group_fields_description']);?></td>
                <td class="dataTableContent" align="center"><?php echo tep_draw_pull_down_menu('layout['.$group_field['group_fields_id'].']', $group_layout_array, $group_field['layout_id']); ?></td>
                <td class="dataTableContent" align="center"><?php echo tep_draw_input_field('limit['.$group_field['group_fields_id'].']', $group_field['limit_id'], 'size="1"', false, 'text', true);?></td>
                <td class="dataTableContent" align="center"><?php echo tep_draw_input_field('order['.$group_field['group_fields_id'].']', $group_field['sort_id'], 'size="5"', false, 'text', true);?></td>
                <td class="dataTableContent" align="center">
<?php
      if ($group_field['status_id'] == '1') {
        echo tep_image(DIR_WS_IMAGES . 'icon_status_green.gif', IMAGE_ICON_STATUS_GREEN, 10, 10) . '&nbsp;&nbsp;<a href="' . tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('action', 'flag', 'id')) . 'action=set_flag&flag=0&id=' . $group_field['group_fields_id'], 'NONSSL') . '">' . tep_image(DIR_WS_IMAGES . 'icon_status_red_light.gif', IMAGE_ICON_STATUS_RED_LIGHT, 10, 10) . '</a>';
      }
      else {
        echo '<a href="' . tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('action', 'flag', 'id')) . 'action=set_flag&flag=1&id=' . $group_field['group_fields_id'], 'NONSSL') . '">' . tep_image(DIR_WS_IMAGES . 'icon_status_green_light.gif', IMAGE_ICON_STATUS_GREEN_LIGHT, 10, 10) . '</a>&nbsp;&nbsp;' . tep_image(DIR_WS_IMAGES . 'icon_status_red.gif', IMAGE_ICON_STATUS_RED, 10, 10);
      }
?>
                </td>
                <td class="dataTableContent" align="right">
<?php 
      if (isset($gfInfo) && is_object($gfInfo) && ($group_field['group_fields_id'] == $gfInfo->group_fields_id)) { 
        echo '<a href="' . tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('action', 'fID')) . 'action=options_list&fID=' . $gfInfo->group_fields_id) . '">' . tep_image(DIR_WS_IMAGES . 'icon_arrow_right.gif', IMAGE_DETAILS) . '</a>'; 
      } else { 
        echo '<a href="' . tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('action', 'fID') ) . 'fID=' . $group_field['group_fields_id']) . '">' . tep_image(DIR_WS_IMAGES . 'icon_info.gif', IMAGE_ICON_INFO) . '</a>'; 
      } 
?>
                </td>
              </tr>
<?php
    } 
?>
              <tr>
                <td colspan="8"><hr class="formAreaBorder" /></td>
              </tr>
              <tr>
                <td colspan="8"><?php echo tep_image_submit('button_update.gif',IMAGE_UPDATE, 'name="update"') . '&nbsp;&nbsp;' . tep_image_submit('button_delete.gif',IMAGE_DELETE,'name="delete"'); ?></td>
              </tr>
            </table></form></td>
          </tr>
          <tr>
            <td><?php echo tep_draw_separator('pixel_trans.gif', '100%', '10'); ?></td>
          </tr>
          <tr>
            <td width="100%"><table border="0" width="100%" cellspacing="0" cellpadding="0">
              <tr>
                <td class="pageHeading"><?php echo HEADING_TITLE; ?></td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td class="smallText"><?php echo TEXT_INFO_INSERT; ?></td>
          </tr>
          <tr>
            <td class="formArea"><?php echo tep_draw_form("insert_filter", basename($PHP_SELF), 'action=insert', 'post'); ?><table border="0" width="100%" cellspacing="1" cellpadding="3">
              <tr class="dataTableHeaderForm">
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_NAME; ?></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_DESCRIPTION; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_LAYOUT; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_LIMITER; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_ORDER; ?></td>
              </tr>
              <tr>
                <td class="dataTableContent"><?php echo tep_draw_input_field('name', '', 'size="30"', false, 'text', true); ?></td>
                <td class="dataTableContent"><?php echo tep_draw_textarea_field('description', 'soft', '60', '1');?></td>
                <td class="dataTableContent" align="center"><?php echo tep_draw_pull_down_menu('layout', $group_layout_array); ?></td>
                <td class="dataTableContent" align="center"><?php echo tep_draw_input_field('limit', '1', 'size="1"', false, 'text', true); ?></td>
                <td class="dataTableContent" align="center"><?php echo tep_draw_input_field('order', '1', 'size="5"', false, 'text', true); ?></td>
              </tr>
              <tr>
                <td colspan="6"><hr class="formAreaBorder" /></td>
              </tr>
              <tr colspan="6">
                <td class="dataTableHeadingContent"><?php echo tep_image_submit('button_insert.gif',IMAGE_INSERT); ?></td>
              </tr>
            </table></form></td>
          </tr>
          <tr>
            <td><?php echo tep_draw_separator('pixel_trans.gif', '100%', '10'); ?></td>
          </tr>
        </table></td>

<?php
    $heading = array();
    $contents = array();

    switch ($action) {
      default:
        if (isset($gfInfo) && is_object($gfInfo)) {
          $heading[] = array('text' => '<b>' . $gfInfo->group_fields_name . '</b>');
          $contents[] = array('align' => 'center', 'text' => '<a href="' . tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('action', 'fID')) . 'action=options_list&fID=' . $gfInfo->group_fields_id) . '">' . tep_image_button('button_details.gif', $gfInfo->group_fields_name . ' ' . IMAGE_DETAILS) . '</a> <a href="' . tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('action', 'fID')) . 'action=duplicate&fID=' . $gfInfo->group_fields_id) . '">' . tep_image_button('button_copy.gif', IMAGE_COPY . ' ' . $gfInfo->group_fields_name) . '</a>');
          $contents[] = array('text' => '<br>' . TEXT_INFO_NUMBER_OPTIONS . ' <br />' . implode('<br />', $gfInfo->selects) );
        }
        break;
    }
    if ( (tep_not_null($heading)) && (tep_not_null($contents)) ) {
      echo '            <td width="25%" valign="top">' . "\n";

      $box = new box;
      echo $box->infoBox($heading, $contents);

      echo '            </td>' . "\n";
    }
?>
      </tr>
    </table></td>
<?php
  } elseif($action == 'delete') {
?>
        <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
          <tr>
            <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
              <tr>
                <td class="pageHeading"><?php echo HEADING_GROUP_DELETE; ?></td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td class="smallText"><?php echo TEXT_INFO_UPDATE; ?></td>
          </tr>
          <tr>
            <td class="formArea"><?php echo tep_draw_form('delete', basename($PHP_SELF), tep_get_all_get_params(array('action')) . 'action=delete_confirm', 'post'); ?><table border="0" width="100%" cellspacing="1" cellpadding="3">
              <tr class="dataTableHeaderForm">
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_NAME; ?></td>
              </tr>
<?php
    $rows = 0;
    foreach( $_POST['mark'] as $key => $value ) {
      $group_fields_query = tep_db_query("select group_fields_id, group_fields_name from " . TABLE_GROUP_FIELDS . " where group_fields_id = '" . (int)$key . "'");
      if( $group_field = tep_db_fetch_array($group_fields_query) ) {
        $row_class = ($rows%2)?'dataTableRow':'dataTableRowSelected';
        echo '                      <tr class="' . $row_class . '">';
?>
                <td class="dataTableContent"><?php echo $group_field['group_fields_name'] . tep_draw_hidden_field('mark[' . $group_field['group_fields_id'] . ']', $group_field['group_fields_id']); ?></td>
              </tr>
<?php
      }
    }
?>
              <tr>
                <td>
<?php 
    echo '<a href="' . tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('action')) . 'action=list') . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a> ' . tep_image_submit('button_confirm.gif', IMAGE_CONFIRM);
?>
                </td>
              </tr>
            </table></form></td>
          </tr>
        </table></td>
<?php
// Options Listings
  } elseif($action == 'options_list') {
?>
        <td width="75%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
          <tr>
            <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
              <tr>
                <td class="pageHeading">
<?php 
    $name_query = tep_db_query("select group_fields_name from " . TABLE_GROUP_FIELDS . " where group_fields_id = '" . (int)$fID . "'");
    if($name_array = tep_db_fetch_array($name_query) ) {
      echo sprintf(HEADING_GROUP_OPTIONS_UPDATE, $name_array['group_fields_name']);
    } else {
      echo 'Error';
    }
?>
                </td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td class="smallText"><?php echo TEXT_INFO_UPDATE_OPTION; ?></td>
          </tr>
          <tr>
            <td class="formArea"><?php echo tep_draw_form('options_update', basename($PHP_SELF), tep_get_all_get_params(array('action')) . 'action=update_option', 'post'); ?><table border="0" width="100%" cellspacing="1" cellpadding="3">
              <tr class="dataTableHeaderForm">
                <td class="dataTableHeadingContent" width="5%"><?php echo '<a href="javascript:void(0)" onClick="copy_checkboxes(document.options_update,\'mark\')" title="Page Select On/Off" class="menuBoxHeadingLink"><span class="dataTableHeadingContent">' . tep_image(DIR_WS_ICONS . 'tick.gif', 'Page Select On/Off') . '</span></a>'; ?></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_NAME; ?></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_TYPE; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_PRICE; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_STOCK; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_IMAGE; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_LAYOUT; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_LIMITER; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_ORDER; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_STATUS; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_ACTION; ?></td>
              </tr>
<?php
    $rows = 0;
    $group_options_query = tep_db_query("select group_options_id, group_options_name, group_types_id, layout_id, limit_id, sort_id, price_status, stock_status, image_status, status_id from " . TABLE_GROUP_OPTIONS . " where group_fields_id = '" . (int)$fID . "' order by sort_id");
    while ($group_option = tep_db_fetch_array($group_options_query)) {

      if ((!isset($_GET['oID']) || (isset($_GET['oID']) && ($_GET['oID'] == $group_option['group_options_id']))) && !isset($ofInfo) ) {
        $option_field['selects'] = array();
        $check_query = tep_db_query("select group_values_name from " . TABLE_GROUP_VALUES . " where group_fields_id = '" . (int)$fID . "' and group_options_id = '" . (int)$group_option['group_options_id'] . "' order by sort_id");
        if( tep_db_num_rows($check_query) ) {
          while($check_array = tep_db_fetch_array($check_query)) {
            $group_option['selects'][] = $check_array['group_values_name'];
          }
        } else {
          $group_option['selects'][] = 'No Options Set';
        }
        $ofInfo = new objectInfo($group_option);
      }

      if (isset($ofInfo) && is_object($ofInfo) && ($group_option['group_options_id'] == $ofInfo->group_options_id)) {
        echo '                  <tr id="defaultSelected" class="dataTableRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)">' . "\n";
      } else {
        echo '                  <tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)">' . "\n";
      }
?>
                <td class="dataTableContent" width="20"><?php echo tep_draw_checkbox_field('mark['.$group_option['group_options_id'].']', 1); ?></td>
                <td class="dataTableContent"><?php echo tep_draw_input_field('name['.$group_option['group_options_id'].']', $group_option['group_options_name'], 'size=30', false, 'text', true);?></td>
                <td class="dataTableContent"><?php echo tep_draw_pull_down_menu('type['.$group_option['group_options_id'].']', $group_types_array, $group_option['group_types_id']); ?></td>
                <td class="dataTableContent" align="center"><?php echo tep_draw_checkbox_field('price['.$group_option['group_options_id'].']', 'on', $group_option['price_status']?true:false); ?></td>
                <td class="dataTableContent" align="center"><?php echo tep_draw_checkbox_field('stock['.$group_option['group_options_id'].']', 'on', $group_option['stock_status']?true:false); ?></td>
                <td class="dataTableContent" align="center"><?php echo tep_draw_checkbox_field('image['.$group_option['group_options_id'].']', 'on', $group_option['image_status']?true:false); ?></td>
                <td class="dataTableContent" align="center"><?php echo tep_draw_pull_down_menu('layout['.$group_option['group_options_id'].']', $group_layout_array, $group_option['layout_id']); ?></td>
                <td class="dataTableContent" align="center"><?php echo tep_draw_input_field('limit['.$group_option['group_options_id'].']', $group_option['limit_id'], 'size="1"', false, 'text', true);?></td>
                <td class="dataTableContent" align="center"><?php echo tep_draw_input_field('order['.$group_option['group_options_id'].']', $group_option['sort_id'], 'size="5"', false, 'text', true);?></td>
                <td class="dataTableContent" align="center">
<?php
      if ($group_option['status_id'] == '1') {
        echo tep_image(DIR_WS_IMAGES . 'icon_status_green.gif', IMAGE_ICON_STATUS_GREEN, 10, 10) . '&nbsp;&nbsp;<a href="' . tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('action', 'flag', 'id')) . 'action=set_option_flag&flag=0&id=' . $group_option['group_options_id'], 'NONSSL') . '">' . tep_image(DIR_WS_IMAGES . 'icon_status_red_light.gif', IMAGE_ICON_STATUS_RED_LIGHT, 10, 10) . '</a>';
      }
      else {
        echo '<a href="' . tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('action', 'flag', 'id')) . 'action=set_option_flag&flag=1&id=' . $group_option['group_options_id'], 'NONSSL') . '">' . tep_image(DIR_WS_IMAGES . 'icon_status_green_light.gif', IMAGE_ICON_STATUS_GREEN_LIGHT, 10, 10) . '</a>&nbsp;&nbsp;' . tep_image(DIR_WS_IMAGES . 'icon_status_red.gif', IMAGE_ICON_STATUS_RED, 10, 10);
      }
?>
                </td>
                <td class="dataTableContent" align="right">
<?php 
      if (isset($ofInfo) && is_object($ofInfo) && ($group_option['group_options_id'] == $ofInfo->group_options_id)) { 
        echo '<a href="' . tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('action', 'oID')) . 'action=values_list&oID=' . $ofInfo->group_options_id) . '">' . tep_image(DIR_WS_IMAGES . 'icon_arrow_right.gif', IMAGE_DETAILS) . '</a>';
      } else { 
        echo '<a href="' . tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('action', 'oID') ) . 'action=options_list&oID=' . $group_option['group_options_id']) . '">' . tep_image(DIR_WS_IMAGES . 'icon_info.gif', IMAGE_ICON_INFO) . '</a>'; 
      } 
?>
                </td>
              </tr>
<?php
    } 
?>
              <tr>
                <td colspan="12"><hr class="formAreaBorder" /></td>
              </tr>
              <tr>
                <td colspan="12"><?php echo '<a href="' . tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('action', 'oID')) . 'action=list') . '">' . tep_image_button('button_back.gif', IMAGE_BACK) . '</a> ' . tep_image_submit('button_update.gif', IMAGE_UPDATE, 'name="update_option"') . '&nbsp;' . tep_image_submit('button_delete.gif',IMAGE_DELETE,'name="delete_option"'); ?></td>
              </tr>
            </table></form></td>
          </tr>
          <tr>
            <td><?php echo tep_draw_separator('pixel_trans.gif', '100%', '10'); ?></td>
          </tr>
          <tr>
            <td width="100%"><table border="0" width="100%" cellspacing="0" cellpadding="0">
              <tr>
                <td class="pageHeading">
<?php
    $name_query = tep_db_query("select group_fields_name from " . TABLE_GROUP_FIELDS . " where group_fields_id = '" . (int)$fID . "'");
    if($name_array = tep_db_fetch_array($name_query) ) {
      echo sprintf(HEADING_TITLE_OPTION, $name_array['group_fields_name']);
    } else {
      echo 'Error';
    }
?>
                </td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td class="smallText"><?php echo TEXT_INFO_INSERT_OPTION; ?></td>
          </tr>
          <tr>
            <td class="formArea"><?php echo tep_draw_form("insert_option", basename($PHP_SELF), tep_get_all_get_params(array('action')) . 'action=insert_option', 'post'); ?><table border="0" width="100%" cellspacing="1" cellpadding="3">
              <tr class="dataTableHeaderForm">
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_NAME; ?></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_TYPE; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_PRICE; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_STOCK; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_IMAGE; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_LAYOUT; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_LIMITER; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_ORDER; ?></td>
              </tr>

              <tr>
                <td class="dataTableContent"><?php echo tep_draw_input_field('name', '', 'size="30"', false, 'text', true); ?></td>
                <td class="dataTableContent"><?php echo tep_draw_pull_down_menu('type', $group_types_array); ?></td>
                <td class="dataTableContent" align="center"><?php echo tep_draw_checkbox_field('price'); ?></td>
                <td class="dataTableContent" align="center"><?php echo tep_draw_checkbox_field('stock'); ?></td>
                <td class="dataTableContent" align="center"><?php echo tep_draw_checkbox_field('image'); ?></td>
                <td class="dataTableContent" align="center"><?php echo tep_draw_pull_down_menu('layout', $group_layout_array); ?></td>
                <td class="dataTableContent" align="center"><?php echo tep_draw_input_field('limit', '1', 'size="1"', false, 'text', true); ?></td>
                <td class="dataTableContent" align="center"><?php echo tep_draw_input_field('order', '1', 'size="5"', false, 'text', true); ?></td>
              </tr>
              <tr>
                <td colspan="10"><hr class="formAreaBorder" /></td>
              </tr>
              <tr colspan="10">
                <td class="dataTableHeadingContent"><?php echo '<a href="' . tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('action', 'oID')) . 'action=list') . '">' . tep_image_button('button_back.gif', IMAGE_BACK) . '</a> ' . tep_image_submit('button_insert.gif',IMAGE_INSERT); ?></td>
              </tr>
            </table></form></td>
          </tr>
          <tr>
            <td><?php echo tep_draw_separator('pixel_trans.gif', '100%', '10'); ?></td>
          </tr>
       </table></td>
<?php
    $heading = array();
    $contents = array();

    switch ($action) {
      default:
        if (isset($ofInfo) && is_object($ofInfo)) {
          $heading[] = array('text' => '<b>' . $ofInfo->group_options_name . '</b>');
          $contents[] = array('align' => 'center', 'text' => '<a href="' . tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('action', 'oID')) . 'action=list') . '">' . tep_image_button('button_back.gif', IMAGE_BACK) . '</a> <a href="' . tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('action', 'oID')) . 'action=values_list&oID=' . $ofInfo->group_options_id) . '">' . tep_image_button('button_details.gif', $ofInfo->group_options_name . ' ' . IMAGE_DETAILS) . '</a> <a href="' . tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('action', 'oID')) . 'action=duplicate_option&oID=' . $ofInfo->group_options_id) . '">' . tep_image_button('button_copy.gif', IMAGE_COPY . ' ' . $ofInfo->group_options_name) . '</a>');
          $contents[] = array('text' => '<br>' . TEXT_INFO_NUMBER_VALUES . ' <br />' . implode('<br />', $ofInfo->selects) );
        }
        break;
    }
    if ( (tep_not_null($heading)) && (tep_not_null($contents)) ) {
      echo '            <td width="25%" valign="top">' . "\n";

      $box = new box;
      echo $box->infoBox($heading, $contents);

      echo '            </td>' . "\n";
    }
?>

      </tr>
    </table></td>
<?php
  } elseif($action == 'delete_option') {
?>
   <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
     <tr>
       <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
         <tr>
           <td class="pageHeading"><?php echo HEADING_GROUP_OPTIONS_DELETE; ?></td>
         </tr>
       </table></td>
     </tr>
     <tr>
       <td class="smallText"><?php echo TEXT_INFO_DELETE_OPTION; ?></td>
     </tr>
     <tr>
       <td class="formArea"><?php echo tep_draw_form('delete_option', basename($PHP_SELF), tep_get_all_get_params(array('action')) . 'action=delete_option_confirm', 'post'); ?><table border="0" width="100%" cellspacing="1" cellpadding="3">
         <tr class="dataTableHeaderForm">
           <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_NAME; ?></td>
         </tr>
<?php
    $rows = 0;
    foreach( $_POST['mark'] as $key => $value ) {
      $group_options_query = tep_db_query("select group_options_id, group_options_name from " . TABLE_GROUP_OPTIONS . " where group_fields_id = '" . (int)$fID . "' and group_options_id = '" . (int)$key . "'");
      if( $group_option = tep_db_fetch_array($group_options_query) ) {
        $rows++;
        $row_class = ($rows%2)?'dataTableRow':'dataTableRowSelected';
        echo '                      <tr class="' . $row_class . '">';
?>
            <td class="dataTableContent"><?php echo $group_option['group_options_name'] . tep_draw_hidden_field('mark[' . $group_option['group_options_id'] . ']', $group_option['group_options_id']); ?></td>
          </tr>
<?php
      }
    }
?>
              <tr>
                <td colspan="8"><hr class="formAreaBorder" /></td>
              </tr>
              <tr>
                <td>
<?php 
    echo '<a href="' . tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('action')) . 'action=options_list') . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a> ' . tep_image_submit('button_confirm.gif', IMAGE_CONFIRM);
?>
                </td>
              </tr>
            </table></form></td>
          </tr>
        </table></td>
<?php
  } elseif($action == 'values_list') {
?>
        <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
          <tr>
            <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
              <tr>
                <td class="pageHeading">
<?php 
    $name_query = tep_db_query("select gf.group_fields_name, go.group_options_name, go.price_status, go.stock_status, go.image_status from " . TABLE_GROUP_OPTIONS . " go left join " . TABLE_GROUP_FIELDS . " gf on (gf.group_fields_id=go.group_fields_id) where gf.group_fields_id = '" . (int)$fID . "' and go.group_options_id = '" . (int)$oID . "'");
    if($name_array = tep_db_fetch_array($name_query) ) {
      echo sprintf(HEADING_GROUP_VALUES_UPDATE, $name_array['group_fields_name'], $name_array['group_options_name']);
    } else {
      echo('Error - Invalid Arguments Passed');
    }
?>
                </td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td class="smallText"><?php echo TEXT_INFO_UPDATE_VALUE; ?></td>
          </tr>
          <tr>
            <td class="formArea"><?php echo tep_draw_form('values_update', basename($PHP_SELF), tep_get_all_get_params(array('action')) . 'action=update_value', 'post', 'enctype="multipart/form-data"'); ?><table border="0" width="100%" cellspacing="1" cellpadding="3">
              <tr class="dataTableHeaderForm">
                <td class="dataTableHeadingContent" width="5%"><?php echo '<a href="javascript:void(0)" onClick="copy_checkboxes(document.values_update,\'mark\')" title="Page Select On/Off" class="menuBoxHeadingLink"><span class="dataTableHeadingContent">' . tep_image(DIR_WS_ICONS . 'tick.gif', 'Page Select On/Off') . '</span></a>'; ?></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_NAME; ?></td>
<?php
    if( $name_array['image_status'] == '1' ) {
      echo '<td class="dataTableHeadingContent">' . TABLE_HEADING_IMAGE . '</td>' . "\n";
    }
    if( $name_array['price_status'] == '1' ) {
      echo '<td class="dataTableHeadingContent">' . TABLE_HEADING_PRICE . '</td>' . "\n";
    }
    if( $name_array['stock_status'] == '1' ) {
      echo '<td class="dataTableHeadingContent">' . TABLE_HEADING_STOCK . '</td>' . "\n";
    }
?>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_ORDER; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_STATUS; ?></td>
              </tr>
<?php
    $rows = 0;
    $group_values_query = tep_db_query("select group_values_id, group_values_name, group_values_image, group_values_price, group_values_stock, sort_id, status_id from " . TABLE_GROUP_VALUES . " where group_fields_id = '" . (int)$fID . "' and group_options_id = '" . (int)$oID . "' order by sort_id");
    while ($group_value = tep_db_fetch_array($group_values_query)) {
      $rows++;
      $row_class = ($rows%2)?'dataTableRow':'dataTableRowSelected';
      echo '                      <tr class="' . $row_class . '">';
?>
                <td class="dataTableContent" width="20"><?php echo tep_draw_checkbox_field('mark['.$group_value['group_values_id'].']', $group_value['group_values_id']); ?></td>
                <td class="dataTableContent"><?php echo tep_draw_input_field('name['.$group_value['group_values_id'].']', $group_value['group_values_name'], 'size="30"', false, 'text', true);?></td>
<?php
    if( $name_array['image_status'] == '1' ) {
      $previous_extra = TEXT_INFO_IMAGE_NOT_SET;
      $delete_link = '';
      if( tep_not_null($group_value['group_values_image']) ) {
        $previous_extra = '<b><font color="#BF0000">' . $group_value['group_values_image'] . '</font></b>';
        $delete_link = '<a href="' . tep_href_link(basename($PHP_SELF), 'fID=' . $fID . '&oID=' . $oID . '&action=remove_values_image&pm_id=' . $group_value['group_values_id']) . '" title="Click to remove the image">' . tep_image(DIR_WS_ICONS . 'delete.gif', IMAGE_DELETE . ' ' . $group_value['group_values_image']) . '</a>';
      }
      $old_image = '<br />' . $previous_extra;
      echo '<td class="dataTableContent">' . tep_draw_file_field('image_'.$group_value['group_values_id']) . '&nbsp;' . $delete_link . $old_image . '</td>' . "\n";
    }
    if( $name_array['price_status'] == '1' ) {
      echo '<td class="dataTableContent">' . tep_draw_input_field('price['.$group_value['group_values_id'].']', $group_value['group_values_price'], 'size="8"', false, 'text', true) . '</td>' . "\n";
    }
    if( $name_array['stock_status'] == '1' ) {
      echo '<td class="dataTableContent">' . tep_draw_input_field('stock['.$group_value['group_values_id'].']', $group_value['group_values_stock'], 'size="3"', false, 'text', true) . '</td>' . "\n";
    }
?>
                <td class="dataTableContent" align="center"><?php echo tep_draw_input_field('order['.$group_value['group_values_id'].']', $group_value['sort_id'], 'size="5"', false, 'text', true);?></td>
                <td class="dataTableContent" align="center">
<?php
        if ($group_value['status_id'] == '1') {
          echo tep_image(DIR_WS_IMAGES . 'icon_status_green.gif', IMAGE_ICON_STATUS_GREEN, 10, 10) . '&nbsp;&nbsp;<a href="' . tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('action', 'flag', 'id')) . 'action=set_value_flag&flag=0&id=' . $group_value['group_values_id'], 'NONSSL') . '">' . tep_image(DIR_WS_IMAGES . 'icon_status_red_light.gif', IMAGE_ICON_STATUS_RED_LIGHT, 10, 10) . '</a>';
        }
        else {
          echo '<a href="' . tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('action', 'flag', 'id')) . 'action=set_value_flag&flag=1&id=' . $group_value['group_values_id'], 'NONSSL') . '">' . tep_image(DIR_WS_IMAGES . 'icon_status_green_light.gif', IMAGE_ICON_STATUS_GREEN_LIGHT, 10, 10) . '</a>&nbsp;&nbsp;' . tep_image(DIR_WS_IMAGES . 'icon_status_red.gif', IMAGE_ICON_STATUS_RED, 10, 10);
        }
?>
                </td>
              </tr>
<?php
    } 
?>
              <tr>
                <td colspan="8"><hr class="formAreaBorder" /></td>
              </tr>
              <tr>
                <td colspan="8"><?php echo '<a href="' . tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('action')) . 'action=options_list') . '">' . tep_image_button('button_back.gif', IMAGE_BACK) . '</a> ' . tep_image_submit('button_update.gif', IMAGE_UPDATE, 'name="update_value"') . '&nbsp;' . tep_image_submit('button_delete.gif',IMAGE_DELETE,'name="delete_value"'); ?></td>
              </tr>
            </table></form></td>
          </tr>
          <tr>
            <td><?php echo tep_draw_separator('pixel_trans.gif', '100%', '10'); ?></td>
          </tr>
          <tr>
            <td width="100%"><table border="0" width="100%" cellspacing="0" cellpadding="0">
              <tr>
                <td class="pageHeading">
<?php
    $name_query = tep_db_query("select gf.group_fields_name, go.group_options_name, go.price_status, go.stock_status, go.image_status from " . TABLE_GROUP_OPTIONS . " go left join " . TABLE_GROUP_FIELDS . " gf on (gf.group_fields_id=go.group_fields_id) where gf.group_fields_id = '" . (int)$fID . "' and go.group_options_id = '" . (int)$oID . "'");
    if($name_array = tep_db_fetch_array($name_query) ) {
      echo sprintf(HEADING_TITLE_VALUE, $name_array['group_fields_name'], $name_array['group_options_name']);
    } else {
      echo 'Error';
    }
?>
                </td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td class="smallText"><?php echo TEXT_INFO_INSERT_VALUE; ?></td>
          </tr>
          <tr>
            <td class="formArea"><?php echo tep_draw_form("insert_value", basename($PHP_SELF), tep_get_all_get_params(array('action')) . 'action=insert_value', 'post'); ?><table border="0" width="100%" cellspacing="1" cellpadding="3">
              <tr class="dataTableHeaderForm">
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_NAME; ?></td>
<?php
    if( $name_array['image_status'] == '1' ) {
      echo '<td class="dataTableHeadingContent">' . TABLE_HEADING_IMAGE . '</td>' . "\n";
    }
    if( $name_array['price_status'] == '1' ) {
      echo '<td class="dataTableHeadingContent">' . TABLE_HEADING_PRICE . '</td>' . "\n";
    }
    if( $name_array['stock_status'] == '1' ) {
      echo '<td class="dataTableHeadingContent">' . TABLE_HEADING_STOCK . '</td>' . "\n";
    }
?>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_ORDER; ?></td>
              </tr>
              <tr>
                <td class="dataTableContent"><?php echo tep_draw_input_field('name', '', 'size="30"', false, 'text', true); ?></td>
<?php
    if( $name_array['image_status'] == '1' ) {
      echo '<td class="dataTableContent">' . tep_draw_file_field('image') . '</td>' . "\n";
    }
    if( $name_array['price_status'] == '1' ) {
      echo '<td class="dataTableContent">' . tep_draw_input_field('price', '', 'size="8"', false, 'text', true) . '</td>' . "\n";
    }
    if( $name_array['stock_status'] == '1' ) {
      echo '<td class="dataTableContent">' . tep_draw_input_field('stock', '', 'size="3"', false, 'text', true) . '</td>' . "\n";
    }
?>
                <td class="dataTableContent" align="center"><?php echo tep_draw_input_field('order', '', 'size="5"', false, 'text', true); ?></td>
              </tr>
              <tr>
                <td colspan="6"><hr class="formAreaBorder" /></td>
              </tr>
              <tr colspan="6">
                <td class="dataTableHeadingContent"><?php echo '<a href="' . tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('action')) . 'action=options_list') . '">' . tep_image_button('button_back.gif', IMAGE_BACK) . '</a> ' . tep_image_submit('button_insert.gif',IMAGE_INSERT); ?></td>
              </tr>
            </table></form></td>
          </tr>
          <tr>
            <td><?php echo tep_draw_separator('pixel_trans.gif', '100%', '10'); ?></td>
          </tr>
       </table></td>
      </tr>
    </table></td>
<?php
  } elseif($action == 'delete_value') {
?>
   <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
     <tr>
       <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
         <tr>
           <td class="pageHeading"><?php echo HEADING_GROUP_OPTIONS_DELETE; ?></td>
         </tr>
       </table></td>
     </tr>
     <tr>
       <td class="smallText"><?php echo TEXT_INFO_DELETE_VALUE; ?></td>
     </tr>
     <tr>
       <td class="formArea"><?php echo tep_draw_form('delete_option', basename($PHP_SELF), tep_get_all_get_params(array('action')) . 'action=delete_option_confirm', 'post'); ?><table border="0" width="100%" cellspacing="1" cellpadding="3">
         <tr class="dataTableHeaderForm">
           <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_NAME; ?></td>
         </tr>
<?php
    $rows = 0;
    foreach( $_POST['mark'] as $key => $value ) {
      $group_values_query = tep_db_query("select group_values_id, group_values_name from " . TABLE_GROUP_VALUES . " where group_fields_id = '" . (int)$fID . "' and group_options_id = '" . (int)$oID . "' and group_values_id = '" . (int)$key . "'");
      if( $group_value = tep_db_fetch_array($group_values_query) ) {
        $rows++;
        $row_class = ($rows%2)?'dataTableRow':'dataTableRowSelected';
        echo '                      <tr class="' . $row_class . '">';
?>
            <td class="dataTableContent"><?php echo $group_value['group_values_name'] . tep_draw_hidden_field('mark[' . $group_value['group_values_id'] . ']', $group_value['group_values_id']); ?></td>
          </tr>
<?php
      }
    }
?>
              <tr>
                <td colspan="8"><hr class="formAreaBorder" /></td>
              </tr>
              <tr>
                <td>
<?php 
    echo '<a href="' . tep_href_link(basename($PHP_SELF), tep_get_all_get_params(array('action')) . 'action=values_list') . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a> ' . tep_image_submit('button_confirm.gif', IMAGE_CONFIRM);
?>
                </td>
              </tr>
            </table></form></td>
          </tr>
        </table></td>
<?php
  }
?>
<!-- body_text_eof //-->
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
