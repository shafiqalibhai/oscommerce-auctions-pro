<?php
/*
  $Id: orders.php,v 1.112 2003/06/29 22:50:52 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

  require('includes/application_top.php');

  //-MS- Garbage Collector Added
  $clear_query = tep_db_query("select orders_id from " . TABLE_ORDERS . " where (unix_timestamp(now()) - unix_timestamp(date_purchased)) > " . GARBAGE_ORDERS_TIMEOUT);
  $clear_array = array();
  while($clear = tep_db_fetch_array($clear_query) ) {
    $clear_array[] = $clear;
    if( count($clear_array) > 1000 )
      break;
  }
  tep_db_free_result($clear_query);
  foreach($clear_array as $key => $value) {
    tep_remove_order($value['orders_id']);
  }
  unset($clear_array);
  //-MS- Garbage Collector Added EOM


  require(DIR_WS_CLASSES . 'currencies.php');
  $currencies = new currencies();
  include(DIR_WS_CLASSES . 'order.php');

  $orders_statuses = array();
  $orders_status_array = array();
  $orders_status_query = tep_db_query("select orders_status_id, orders_status_name from " . TABLE_ORDERS_STATUS . " where language_id = '" . (int)$languages_id . "'");
  while ($orders_status = tep_db_fetch_array($orders_status_query)) {
    $orders_statuses[] = array('id' => $orders_status['orders_status_id'],
                               'text' => $orders_status['orders_status_name']);
    $orders_status_array[$orders_status['orders_status_id']] = $orders_status['orders_status_name'];
  }

  $action = (isset($_GET['action']) ? $_GET['action'] : '');

  if (tep_not_null($action)) {
    switch ($action) {
      case 'update_processing':
        $query = "select * from " . TABLE_ORDERS_STATUS . " where orders_status_name = 'Processing' and language_id = '" . (int)$languages_id . "'";
        $res = tep_db_query($query);
        $sos = tep_db_fetch_array($res);
        $query = "select * from " . TABLE_ORDERS_STATUS . " where orders_status_name = 'Awaiting Tracking' and language_id = '" . (int)$languages_id . "'";
        $res = tep_db_query($query);
        $dos = tep_db_fetch_array($res);
        $query = "select * from " . TABLE_ORDERS . " where orders_status = '" . (int)$sos['orders_status_id'] . "'";
        $res = tep_db_query($query);
        $customer_notified = '0';
        $comments = '';
        while ($r = tep_db_fetch_array($res)) {
          $oID = $r['orders_id'];
          $orders_id_list[] = $oID;
          $status = $dos['orders_status_id'];
          tep_db_query("update " . TABLE_ORDERS . " set orders_status = '" . (int)$status . "', last_modified = now() where orders_id = '" . (int)$oID . "'");
          tep_db_query("insert into " . TABLE_ORDERS_STATUS_HISTORY . " (orders_id, orders_status_id, date_added, customer_notified, comments) values ('" . (int)$oID . "', '" . (int)$status . "', now(), '" . tep_db_input($customer_notified) . "', '" . tep_db_input($comments)  . "')");
        }
        unset($oID);
        unset($status);
        $messageStack->add_session(SUCCESS_ORDER_UPDATED.' Total count = '.sizeof($orders_id_list).'. IDs : '.implode(', ', $orders_id_list).'', 'success');
        tep_redirect(tep_href_link(FILENAME_ORDERS));
       break;

      case 'update_order':
        $oID = tep_db_prepare_input($_GET['oID']);
        $status = tep_db_prepare_input($_POST['status']);
        $comments = tep_db_prepare_input($_POST['comments']);

        $order_updated = false;
        $check_status_query = tep_db_query("select customers_name, customers_email_address, orders_status, date_purchased from " . TABLE_ORDERS . " where orders_id = '" . (int)$oID . "'");
        $check_status = tep_db_fetch_array($check_status_query);

        if ( ($check_status['orders_status'] != $status) || tep_not_null($comments)) {
          tep_db_query("update " . TABLE_ORDERS . " set orders_status = '" . tep_db_input($status) . "', last_modified = now() where orders_id = '" . (int)$oID . "'");

          $customer_notified = '0';
          if (isset($_POST['notify']) && ($_POST['notify'] == 'on')) {
            $notify_comments = '';
            if (isset($_POST['notify_comments']) && ($_POST['notify_comments'] == 'on')) {
              $notify_comments = sprintf(EMAIL_TEXT_COMMENTS_UPDATE, $comments) . "\n\n";
            }

//-MS- Email Templates added
            //Let's build a message object using the email class
            $mail_query = tep_db_query("select * from " . TABLE_EMAIL_TEMPLATES . " where grp='order_change'");
            if( $email_array = tep_db_fetch_array($mail_query) ) {

              $order = new order($oID);

              for( $order_totals_string='<table width="100%">', $i = 0, $n = sizeof($order->totals); $i < $n; $i++) {
                $order_totals_string .= '<tr><td><font size="2" face="verdana,geneva" color="#660000">' . $order->totals[$i]['title'] . '</font></td><td align="right"><font size="2" face="verdana,geneva" color="#660000">' . $order->totals[$i]['text'] . '</font></td>';
              }
              $order_totals_string .= '</table>';

              $customers_name = $check_status['customers_name'];
              $right_col = tep_email_column(DEFAULT_EMAIL_ABSTRACT_ZONE_ID);
              $temp_array = array(
                                  'CUSTOMER_NAME' => $customers_name,
                                  'CUSTOMER_EMAIL' => $check_status['customers_email_address'],
                                  'ORDER_STATUS' => $orders_status_array[$status],
                                  'ORDER_COMMENTS' => $comments,
                                  'ORDER_ID' => $oID,
                                  'ORDER_SHIPTO' => tep_address_format($order->delivery['format_id'], $order->delivery, 1, '', "\n"),
                                  'ORDER_BILLTO' => tep_address_format($order->billing['format_id'], $order->billing, 1, '', "\n"),
                                  'ORDER_TOTALS' => $order_totals_string,
                                  'ORDER_PAYMENT' => $order->info['payment_method'],
                                  'STORE_NAME' => STORE_OWNER,
                                  'ORDER_DATE' => tep_date_long($check_status['date_purchased']),
                                  'ORDER_URL' => '<a href="' . tep_catalog_href_link(FILENAME_CATALOG_ACCOUNT_HISTORY_INFO, 'order_id=' . $oID, 'SSL') . '">' . 'ORDER # ' . $oID . '</a>',
                                  'RIGHT_COL' => $right_col
                                 );
              $email_text = tep_email_templates_replace_entities($email_array['contents'], $temp_array);
              $email_array['subject'] = tep_email_templates_replace_entities($email_array['subject'], $temp_array);
              tep_mail($customers_name, $check_status['customers_email_address'], $email_array['subject'], $email_text, STORE_OWNER, STORE_OWNER_EMAIL_ADDRESS);
              unset($order, $email_text);
            } else {
//-MS- Email Templates added EOM
             $email = STORE_NAME . "\n" . EMAIL_SEPARATOR . "\n" . EMAIL_TEXT_ORDER_NUMBER . ' ' . $oID . "\n" . EMAIL_TEXT_INVOICE_URL . ' ' . tep_catalog_href_link(FILENAME_CATALOG_ACCOUNT_HISTORY_INFO, 'order_id=' . $oID, 'SSL') . "\n" . EMAIL_TEXT_DATE_ORDERED . ' ' . tep_date_long($check_status['date_purchased']) . "\n\n" . $notify_comments . sprintf(EMAIL_TEXT_STATUS_UPDATE, $orders_status_array[$status]);
             tep_mail($check_status['customers_name'], $check_status['customers_email_address'], EMAIL_TEXT_SUBJECT, $email, STORE_OWNER, STORE_OWNER_EMAIL_ADDRESS);
            }
            $customer_notified = '1';
          }

          tep_db_query("insert into " . TABLE_ORDERS_STATUS_HISTORY . " (orders_id, orders_status_id, date_added, customer_notified, comments) values ('" . (int)$oID . "', '" . tep_db_input($status) . "', now(), '" . tep_db_input($customer_notified) . "', '" . tep_db_input($comments)  . "')");

          $order_updated = true;
        }

        if ($order_updated == true) {
          $messageStack->add_session(SUCCESS_ORDER_UPDATED, 'success');
        } else {
          $messageStack->add_session(WARNING_ORDER_NOT_UPDATED, 'warning');
        }

        tep_redirect(tep_href_link(FILENAME_ORDERS, tep_get_all_get_params(array('action')) . 'action=edit'));
        break;
      case 'deleteconfirm':
        $oID = tep_db_prepare_input($_GET['oID']);
        tep_remove_order($oID, isset($_POST['restock'])?$_POST['restock']:false );
        tep_redirect(tep_href_link(FILENAME_ORDERS, tep_get_all_get_params(array('oID', 'action'))));
        break;
      case 'delete_multiconfirm':
        if( !isset($HTTP_POST_VARS['pc_id']) || !is_array($HTTP_POST_VARS['pc_id']) ) {
          $messageStack->add_session(ERROR_ORDER_NOT_SELECTED, 'error');
          tep_redirect(tep_href_link(FILENAME_ORDERS, tep_get_all_get_params(array('oID', 'action'))));
        }
        for($i=0, $j=count($HTTP_POST_VARS['pc_id']); $i<$j; $i++ ) {
          $oID = (int)$HTTP_POST_VARS['pc_id'][$i];
          tep_remove_order($oID, true);
        }
        tep_redirect(tep_href_link(FILENAME_ORDERS, tep_get_all_get_params(array('oID', 'action'))));
        break;
      case 'delete_multi':
        if( !isset($HTTP_POST_VARS['pc_id']) || !is_array($HTTP_POST_VARS['pc_id']) ) {
          $messageStack->add_session(ERROR_ORDER_NOT_SELECTED, 'error');
          tep_redirect(tep_href_link(FILENAME_ORDERS, tep_get_all_get_params(array('oID', 'action'))));
        }
        break;
    }
  }

  if (($action == 'edit') && isset($_GET['oID'])) {
    $oID = tep_db_prepare_input($_GET['oID']);

    $orders_query = tep_db_query("select orders_id from " . TABLE_ORDERS . " where orders_id = '" . (int)$oID . "'");
    $order_exists = true;
    if (!tep_db_num_rows($orders_query)) {
      $order_exists = false;
      $messageStack->add(sprintf(ERROR_ORDER_DOES_NOT_EXIST, $oID), 'error');
    }
  }
?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<script language="javascript" src="includes/general.js"></script>
</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF">
<!-- header //-->
<?php
  require(DIR_WS_INCLUDES . 'header.php');
?>
<!-- header_eof //-->

<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
    <td width="<?php echo BOX_WIDTH; ?>" valign="top"><table border="0" width="<?php echo BOX_WIDTH; ?>" cellspacing="1" cellpadding="1" class="columnLeft">
<!-- left_navigation //-->
<?php require(DIR_WS_INCLUDES . 'column_left.php'); ?>
<!-- left_navigation_eof //-->
    </table></td>
<!-- body_text //-->
    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
<?php
  if (($action == 'edit') && ($order_exists == true)) {
    $order = new order($oID);
?>
      <tr>
        <td width="100%"><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td class="pageHeading"><?php echo HEADING_TITLE . ' (#' . $oID . ')'; ?></td>
            <td class="pageHeading" align="right"><?php echo tep_draw_separator('pixel_trans.gif', 1, HEADING_IMAGE_HEIGHT); ?></td>
            <td class="pageHeading" align="right">
<?php 
//-MS- Next/Previous orders added
    $next = $previous = 0;
    $orders_query = tep_db_query("select o.orders_id from " . TABLE_ORDERS . " o where o.orders_id < '" . (int)$oID . "' order by o.orders_id desc limit 1");
    if( $orders_array = tep_db_fetch_array($orders_query) ) {
      $previous = $orders_array['orders_id'];
      echo '<a href="' . tep_href_link(FILENAME_ORDERS, 'oID=' . $previous . '&action=edit') . '">' . tep_image_button('button_previous.gif', 'Previous Order') . '</a>&nbsp;';
    }
    $orders_query = tep_db_query("select o.orders_id from " . TABLE_ORDERS . " o where o.orders_id > '" . (int)$oID . "' limit 1");
    if( $orders_array = tep_db_fetch_array($orders_query) ) {
      $next = $orders_array['orders_id'];
      echo '<a href="' . tep_href_link(FILENAME_ORDERS, 'oID=' . $next . '&action=edit') . '">' . tep_image_button('button_next.gif', 'Next Order') . '</a>&nbsp;';
    }
//-MS- Next/Previous orders added EOM
    echo '<a href="' . tep_href_link(FILENAME_ORDERS, 'cID=' . $order->info['customers_id']) . '">' . tep_image_button('button_orders.gif', IMAGE_ORDERS) . '</a>&nbsp;' . 
         '<a href="' . tep_href_link(FILENAME_ORDERS, tep_get_all_get_params(array('action'))) . '">' . tep_image_button('button_back.gif', IMAGE_BACK) . '</a>';
?>
            </td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><table width="100%" border="0" cellspacing="0" cellpadding="2">
          <tr>
            <td colspan="3"><?php echo tep_draw_separator(); ?></td>
          </tr>
          <tr>
            <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="2">
              <tr>
                <td class="main" valign="top"><b><?php echo ENTRY_CUSTOMER; ?></b></td>
                <td class="main"><?php echo tep_address_format($order->customer['format_id'], $order->customer, 1, '', '<br>'); ?></td>
              </tr>
              <tr>
                <td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '1', '5'); ?></td>
              </tr>
              <tr>
                <td class="main"><b><?php echo ENTRY_TELEPHONE_NUMBER; ?></b></td>
                <td class="main"><?php echo $order->customer['telephone']; ?></td>
              </tr>
              <tr>
                <td class="main"><b><?php echo ENTRY_EMAIL_ADDRESS; ?></b></td>
                <td class="main"><?php echo '<a href="mailto:' . $order->customer['email_address'] . '"><u>' . $order->customer['email_address'] . '</u></a>'; ?></td>
              </tr>
            </table></td>
            <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="2">
              <tr>
                <td class="main" valign="top"><b><?php echo ENTRY_SHIPPING_ADDRESS; ?></b></td>
                <td class="main"><?php echo tep_address_format($order->delivery['format_id'], $order->delivery, 1, '', '<br>'); ?></td>
              </tr>
            </table></td>
            <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="2">
              <tr>
                <td class="main" valign="top"><b><?php echo ENTRY_BILLING_ADDRESS; ?></b></td>
                <td class="main"><?php echo tep_address_format($order->billing['format_id'], $order->billing, 1, '', '<br>'); ?></td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
      </tr>
      <tr>
        <td><table border="0" cellspacing="0" cellpadding="2">
          <tr>
            <td class="main"><b><?php echo ENTRY_PAYMENT_METHOD; ?></b></td>
            <td class="main"><?php echo $order->info['payment_method']; ?></td>
          </tr>
<?php
    if (tep_not_null($order->info['cc_type']) || tep_not_null($order->info['cc_owner']) || tep_not_null($order->info['cc_number'])) {
?>
          <tr>
            <td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
          </tr>
          <tr>
            <td class="main"><?php echo ENTRY_CREDIT_CARD_TYPE; ?></td>
            <td class="main"><?php echo $order->info['cc_type']; ?></td>
          </tr>
          <tr>
            <td class="main"><?php echo ENTRY_CREDIT_CARD_OWNER; ?></td>
            <td class="main"><?php echo $order->info['cc_owner']; ?></td>
          </tr>
          <tr>
            <td class="main"><?php echo ENTRY_CREDIT_CARD_NUMBER; ?></td>
            <td class="main"><?php echo $order->info['cc_number']; ?></td>
          </tr>
          <tr>
            <td class="main"><?php echo ENTRY_CREDIT_CARD_EXPIRES; ?></td>
            <td class="main"><?php echo $order->info['cc_expires']; ?></td>
          </tr>
<?php
    }
?>
        </table></td>
      </tr>
      <tr>
        <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
      </tr>
      <tr>
        <td><table border="0" width="100%" cellspacing="1" cellpadding="3">
          <tr class="dataTableHeadingRow">
            <td class="dataTableHeadingContent" colspan="2"><?php echo TABLE_HEADING_PRODUCTS; ?></td>
<?php
/*
            <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_PRODUCTS_MODEL; ?></td>
            <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_PRODUCTS_TRACKING; ?></td>
*/
?>
            <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_TAX; ?></td>
            <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_PRICE_EXCLUDING_TAX; ?></td>
            <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_PRICE_INCLUDING_TAX; ?></td>
            <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_TOTAL_EXCLUDING_TAX; ?></td>
            <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_TOTAL_INCLUDING_TAX; ?></td>
          </tr>
<?php
    for ($i=0, $n=sizeof($order->products); $i<$n; $i++) {
      echo '          <tr class="dataTableRow">' . "\n" .
           '            <td class="dataTableContent" valign="top" align="right">' . $order->products[$i]['qty'] . '&nbsp;x</td>' . "\n" .
           '            <td class="dataTableContent" valign="top">' . $order->products[$i]['name'];

      if (isset($order->products[$i]['attributes']) && (sizeof($order->products[$i]['attributes']) > 0)) {
        for ($j = 0, $k = sizeof($order->products[$i]['attributes']); $j < $k; $j++) {
          echo '<br><nobr><small>&nbsp;<i> - ' . $order->products[$i]['attributes'][$j]['option'] . ': ' . $order->products[$i]['attributes'][$j]['value'];
          if ($order->products[$i]['attributes'][$j]['price'] != '0') echo ' (' . $order->products[$i]['attributes'][$j]['prefix'] . $currencies->format($order->products[$i]['attributes'][$j]['price'] * $order->products[$i]['qty'], true, $order->info['currency'], $order->info['currency_value']) . ')';
          echo '</i></small></nobr>';
        }
      }

//-MS- Group Fields Added
      if ( (isset($order->products[$i]['groups'])) && (sizeof($order->products[$i]['groups']) > 0) ) {
        echo '<br />';
        for ($j=0, $n2=sizeof($order->products[$i]['groups']); $j<$n2; $j++) {
          echo '<i>';
          if( !empty($order->products[$i]['groups'][$j]['name']) ) {
            $order->products[$i]['groups'][$j]['name'] . ': ';
          }
          echo $order->products[$i]['groups'][$j]['value'] . '</i>&nbsp;';
        }
      }
//-MS- Group Fields Added EOM

      echo '            </td>' . "\n" .
           //'            <td class="dataTableContent" valign="top">' . $order->products[$i]['model'] . '</td>' . "\n" .
           //'            <td class="dataTableContent" valign="top">' . $order->products[$i]['tracking'] . '</td>' . "\n" .
           '            <td class="dataTableContent" align="right" valign="top">' . tep_display_tax_value($order->products[$i]['tax']) . '%</td>' . "\n" .
           '            <td class="dataTableContent" align="right" valign="top"><b>' . $currencies->format($order->products[$i]['final_price'], true, $order->info['currency'], $order->info['currency_value']) . '</b></td>' . "\n" .
           '            <td class="dataTableContent" align="right" valign="top"><b>' . $currencies->format(tep_add_tax($order->products[$i]['final_price'], $order->products[$i]['tax']), true, $order->info['currency'], $order->info['currency_value']) . '</b></td>' . "\n" .
           '            <td class="dataTableContent" align="right" valign="top"><b>' . $currencies->format($order->products[$i]['final_price'] * $order->products[$i]['qty'], true, $order->info['currency'], $order->info['currency_value']) . '</b></td>' . "\n" .
           '            <td class="dataTableContent" align="right" valign="top"><b>' . $currencies->format(tep_add_tax($order->products[$i]['final_price'], $order->products[$i]['tax']) * $order->products[$i]['qty'], true, $order->info['currency'], $order->info['currency_value']) . '</b></td>' . "\n";
      echo '          </tr>' . "\n";
    }
?>
          <tr>
            <td align="right" colspan="9"><table border="0" cellspacing="0" cellpadding="2">
<?php
    for ($i = 0, $n = sizeof($order->totals); $i < $n; $i++) {
      echo '              <tr>' . "\n" .
           '                <td align="right" class="smallText">' . $order->totals[$i]['title'] . '</td>' . "\n" .
           '                <td align="right" class="smallText">' . $order->totals[$i]['text'] . '</td>' . "\n" .
           '              </tr>' . "\n";
    }
?>
            </table></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
      </tr>
      <tr>
        <td class="main"><table border="1" cellspacing="0" cellpadding="5">
          <tr>
            <td class="smallText" align="center"><b><?php echo TABLE_HEADING_DATE_ADDED; ?></b></td>
            <td class="smallText" align="center"><b><?php echo TABLE_HEADING_CUSTOMER_NOTIFIED; ?></b></td>
            <td class="smallText" align="center"><b><?php echo TABLE_HEADING_STATUS; ?></b></td>
            <td class="smallText" align="center"><b><?php echo TABLE_HEADING_COMMENTS; ?></b></td>
          </tr>
<?php
    $orders_history_query = tep_db_query("select orders_status_id, date_added, customer_notified, comments from " . TABLE_ORDERS_STATUS_HISTORY . " where orders_id = '" . tep_db_input($oID) . "' order by date_added");
    if (tep_db_num_rows($orders_history_query)) {
      while ($orders_history = tep_db_fetch_array($orders_history_query)) {
        echo '          <tr>' . "\n" .
             '            <td class="smallText" align="center">' . tep_datetime_short($orders_history['date_added']) . '</td>' . "\n" .
             '            <td class="smallText" align="center">';
        if ($orders_history['customer_notified'] == '1') {
          echo tep_image(DIR_WS_ICONS . 'tick.gif', ICON_TICK) . "</td>\n";
        } else {
          echo tep_image(DIR_WS_ICONS . 'cross.gif', ICON_CROSS) . "</td>\n";
        }
        echo '            <td class="smallText">' . $orders_status_array[$orders_history['orders_status_id']] . '</td>' . "\n" .
             '            <td class="smallText">' . nl2br(tep_db_output($orders_history['comments'])) . '&nbsp;</td>' . "\n" .
             '          </tr>' . "\n";
      }
    } else {
      echo '          <tr>' . "\n" .
           '            <td class="smallText" colspan="5">' . TEXT_NO_ORDER_HISTORY . '</td>' . "\n" .
           '          </tr>' . "\n";
    }
?>
        </table></td>
      </tr>
      <tr>
        <td class="main"><br><b><?php echo TABLE_HEADING_COMMENTS; ?></b></td>
      </tr>
      <tr>
        <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '5'); ?></td>
      </tr>
      <tr>
        <td><?php echo tep_draw_form('status', FILENAME_ORDERS, tep_get_all_get_params(array('action')) . 'action=update_order'); ?><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td class="main"><?php echo tep_draw_textarea_field('comments', 'soft', '60', '5'); ?></td>
          </tr>
          <tr>
            <td><?php include(DIR_WS_MODULES . 'comment_bar.php'); ?></td>
          </tr>
          <tr>
            <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
          </tr>
          <tr>
            <td><table border="0" cellspacing="0" cellpadding="2">
              <tr>
                <td><table border="0" cellspacing="0" cellpadding="2">
                  <tr>
                    <td class="main"><b><?php echo ENTRY_STATUS; ?></b> <?php echo tep_draw_pull_down_menu('status', $orders_statuses, $order->info['orders_status'], 'id="orders_status"'); ?></td>
                  </tr>
                  <tr>
                    <td class="main"><b><?php echo ENTRY_NOTIFY_CUSTOMER; ?></b> <?php echo tep_draw_checkbox_field('notify', '', true); ?></td>
                    <td class="main"><b><?php echo ENTRY_NOTIFY_COMMENTS; ?></b> <?php echo tep_draw_checkbox_field('notify_comments', '', true); ?></td>
                  </tr>
                </table></td>
                <td valign="top"><?php echo tep_image_submit('button_update.gif', IMAGE_UPDATE); ?></td>
              </tr>
            </table></td>
          </tr>
        </table></form></td>
      </tr>
      <tr>
        <td colspan="2" align="right"><?php echo '<a href="' . tep_href_link(FILENAME_ORDERS_INVOICE, 'oID=' . $_GET['oID']) . '" TARGET="_blank">' . tep_image_button('button_invoice.gif', IMAGE_ORDERS_INVOICE) . '</a> <a href="' . tep_href_link(FILENAME_ORDERS_PACKINGSLIP, 'oID=' . $_GET['oID']) . '" TARGET="_blank">' . tep_image_button('button_packingslip.gif', IMAGE_ORDERS_PACKINGSLIP) . '</a> <a href="' . tep_href_link(FILENAME_ORDERS, tep_get_all_get_params(array('action'))) . '">' . tep_image_button('button_back.gif', IMAGE_BACK) . '</a>'; ?></td>
      </tr>
<?php
  } elseif($action == 'delete_multi') {
?>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td class="pageHeading"><?php echo HEADING_MASS_DELETE; ?></td>
          </tr>
          <tr>
            <td class="main"><?php echo TEXT_INFO_MASS_DELETE; ?></td>
          </tr>
          <tr>
            <td><hr /></td>
          </tr>
          <tr>
            <td valign="top"><?php echo tep_draw_form('rl', FILENAME_ORDERS, tep_get_all_get_params(array('action')) . 'action=delete_multiconfirm', 'post'); ?><table border="0" width="100%" cellspacing="0" cellpadding="2">
              <tr>
                <td colspan="5"><?php echo '<a href="' . tep_href_link(FILENAME_ORDERS, tep_get_all_get_params(array('oID', 'action'))) . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>&nbsp;' . tep_image_submit('button_confirm.gif', IMAGE_CONFIRM); ?></td>
              </tr>
              <tr class="dataTableHeadingRow">
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_CUSTOMERS; ?></td>
                <td class="dataTableHeadingContent"><?php echo 'CID'; ?></td>
                <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_ORDER_TOTAL; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_DATE_PURCHASED; ?></td>
                <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_STATUS; ?></td>
              </tr>
<?php
    $orders_keys = array_keys($HTTP_POST_VARS['pc_id']);
    $orders_query = tep_db_query("select o.orders_id, o.customers_id, o.customers_name, o.payment_method, o.date_purchased, o.last_modified, o.currency, o.currency_value, s.orders_status_name, ot.text as order_total from " . TABLE_ORDERS . " o left join " . TABLE_ORDERS_TOTAL . " ot on (o.orders_id = ot.orders_id), " . TABLE_ORDERS_STATUS . " s where o.orders_id in ('" . implode("', '", $orders_keys) . "') and o.orders_status = s.orders_status_id and s.language_id = '" . (int)$languages_id . "' and ot.class = 'ot_total' order by o.orders_id DESC");
    $rows = 0;
    while ($orders = tep_db_fetch_array($orders_query)) {
      $rows++;
      $row_class = ($rows%2)?'dataTableRow':'dataTableRowSelected';
      echo '              <tr class="' . $row_class . '">' . "\n";
?>
                <td class="dataTableContent"><?php echo $orders['customers_name']; ?></td>
                <td class="dataTableContent"><?php echo $orders['customers_id'] . tep_draw_hidden_field('pc_id[]', $orders['orders_id']); ?></td>
                <td class="dataTableContent" align="right"><?php echo strip_tags($orders['order_total']); ?></td>
                <td class="dataTableContent" align="center"><?php echo tep_datetime_short($orders['date_purchased']); ?></td>
                <td class="dataTableContent" align="right"><?php echo $orders['orders_status_name']; ?></td>

<?php
      echo '              </tr>' . "\n";
    }
?>
              <tr>
                <td colspan="5"><?php echo '<a href="' . tep_href_link(FILENAME_ORDERS, tep_get_all_get_params(array('oID', 'action'))) . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>&nbsp;' . tep_image_submit('button_confirm.gif', IMAGE_CONFIRM); ?></td>
              </tr>
            </table></form></td>
          </tr>
        </table></td>
      </tr>
<?php
  } else {
?>
      <tr>
        <td width="100%"><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td class="pageHeading"><?php echo HEADING_TITLE; ?></td>
            <td class="pageHeading" align="right"><?php echo tep_draw_separator('pixel_trans.gif', 1, HEADING_IMAGE_HEIGHT); ?></td>
            <td width="250" class="formArea"><?php echo tep_draw_form('orders_range', FILENAME_ORDERS, '', 'get'); ?><table border="0" width="100%" cellspacing="0" cellpadding="2">
              <tr>
                <td class="smallText" align="right"><?php echo 'From ID:'; ?></td>
                <td class="smallText" align="right"><?php echo tep_draw_input_field('oid_start', '', 'size="12"'); ?></td>
                <td />
              </tr>
              <tr>
                <td class="smallText" align="right"><?php echo 'To ID:'; ?></td>
                <td class="smallText" align="right"><?php echo tep_draw_input_field('oid_end', '', 'size="12"'); ?></td>
                <td class="smallText" align="right"><?php echo tep_image_submit('button_search.gif', IMAGE_SEARCH); ?></td>
              </tr>
            </table></form></td>
            <td align="right"><table border="0" width="100%" cellspacing="0" cellpadding="0">
              <tr>
                <td class="smallText" align="right"><?php echo tep_draw_form('orders', FILENAME_ORDERS, '', 'get'); ?><?php echo HEADING_TITLE_SEARCH . ' ' . tep_draw_input_field('oID', '', 'size="12"') . tep_draw_hidden_field('action', 'edit'); ?></form></td>
              </tr>
              <tr>
                <td class="smallText" align="right"><?php echo tep_draw_form('status', FILENAME_ORDERS, '', 'get'); ?><?php echo HEADING_TITLE_STATUS . ' ' . tep_draw_pull_down_menu('status', array_merge(array(array('id' => '', 'text' => TEXT_ALL_ORDERS)), $orders_statuses), (isset($_GET['status']))?$_GET['status']:'', 'onChange="this.form.submit();"'); ?></form></td>
              </tr>            
            </table></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="0">
              <tr>
                <td valign="top"><?php echo tep_draw_form('rl', FILENAME_ORDERS, 'action=delete_multi&' . tep_get_all_get_params(array('action')), 'post'); ?><table border="0" width="100%" cellspacing="1" cellpadding="3">
                  <tr>
                    <td colspan="8"><?php echo tep_image_submit('button_mass_delete.gif', IMAGE_DELETE); ?></td>
                  </tr>
                  <tr class="dataTableHeadingRow">
                    <td class="dataTableHeadingContent"><?php echo '<a href="javascript:void(0)" onClick="copy_checkboxes(document.rl, \'pc_id\')" title="Page Select On/Off" class="menuBoxHeadingLink"><span class="dataTableHeadingContent">' . tep_image(DIR_WS_ICONS . 'tick.gif', 'Page Select On/Off') . '</span></a>'; ?></td>
                    <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_CUSTOMERS; ?></td>
                    <td class="dataTableHeadingContent" align="center"><?php echo 'CID'; ?></td>
                    <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_EDIT_ORDERS; ?></td>
                    <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_ORDER_TOTAL; ?></td>
                    <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_DATE_PURCHASED; ?></td>
                    <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_STATUS; ?></td>
                    <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_ACTION; ?>&nbsp;</td>
                  </tr>
<?php
    if (isset($_GET['cID'])) {
      $cID = tep_db_prepare_input($_GET['cID']);
      $orders_query_raw = "select orders_id, customers_name, customers_id, payment_method, date_purchased, last_modified, currency, currency_value, orders_status from " . TABLE_ORDERS . " where customers_id = '" . (int)$cID . "' order by orders_id DESC";
    } elseif (isset($_GET['status']) && tep_not_null($_GET['status']) ) {
      $status = tep_db_prepare_input($_GET['status']);
      $orders_query_raw = "select orders_id, customers_id, customers_name, payment_method, date_purchased, last_modified, currency, currency_value, orders_status from " . TABLE_ORDERS . " where orders_status = '" . (int)$status . "' order by orders_id DESC";

    } elseif (isset($_GET['oid_start']) && isset($_GET['oid_end']) ) {
      $oid_start = $_GET['oid_start'];
      $oid_end = $_GET['oid_end'];
      if( $oid_start > $oid_end ) {
        $oid_tmp = $oid_end;
        $oid_end = $oid_start;
        $oid_start = $oid_tmp;
      }
      $orders_query_raw = "select orders_id, customers_id, customers_name, payment_method, date_purchased, last_modified, currency, currency_value, orders_status from " . TABLE_ORDERS . " where orders_id >= '" . (int)$oid_start . "' and orders_id <= '" . (int)$oid_end . "' order by orders_id DESC";
    } elseif (isset($_GET['address']) ) {
      $address = $_GET['address'];
      $orders_query_raw = "select orders_id, customers_id, customers_name, payment_method, date_purchased, last_modified, currency, currency_value, orders_status from " . TABLE_ORDERS . " where (delivery_street_address like '%" . tep_db_prepare_input(tep_db_input($address)) . "%' or billing_street_address like '%" . tep_db_prepare_input(tep_db_input($address)) . "%') order by o.orders_id DESC";
    } else {
      $orders_query_raw = "select orders_id, customers_id, customers_name, payment_method, date_purchased, last_modified, currency, currency_value, orders_status from " . TABLE_ORDERS . " order by orders_id DESC";
    }

    $orders_split = new splitPageResults($_GET['page'], MAX_DISPLAY_ADMIN_ORDERS, $orders_query_raw, $orders_query_numrows);
    $orders_query = tep_db_query($orders_query_raw);
    $bCheck = false;
    while( $orders = tep_db_fetch_array($orders_query) ) {

      $extra_query = tep_db_query("select orders_status_name from " . TABLE_ORDERS_STATUS . " where orders_status_id = '" . (int)$orders['orders_status'] . "' and language_id = '" . (int)$languages_id . "'");
      if( tep_db_num_rows($extra_query)) {
        $extra_array = tep_db_fetch_array($extra_query);
        $orders = array_merge($orders, $extra_array);
      } else {
        $orders['orders_status_name'] = TEXT_INFO_NA;
      }

      $extra_query = tep_db_query("select text as order_total from " . TABLE_ORDERS_TOTAL . " where orders_id = '" . (int)$orders['orders_id'] . "' and class = 'ot_total'");
      if( tep_db_num_rows($extra_query)) {
        $extra_array = tep_db_fetch_array($extra_query);
        $orders = array_merge($orders, $extra_array);
      } else {
        $orders['order_total'] = TEXT_INFO_NA;
      }

      if ((!isset($_GET['oID']) || (isset($_GET['oID']) && ($_GET['oID'] == $orders['orders_id']))) && !isset($oInfo)) {
        $oInfo = new objectInfo($orders);
      }

      if (isset($oInfo) && is_object($oInfo) && ($orders['orders_id'] == $oInfo->orders_id)) {
        echo '                  <tr id="defaultSelected" class="dataTableRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)">' . "\n";
      } else {
        echo '                  <tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)">' . "\n";
      }
?>
                    <td class="dataTableContent"><?php echo tep_draw_checkbox_field('pc_id[' . $orders['orders_id'] . ']', ($bCheck?'on':''), $bCheck ); ?></td> 
                    <td class="dataTableContent">
<?php 
      echo '<a href="' . tep_href_link(FILENAME_ORDERS, tep_get_all_get_params(array('oID', 'action')) . 'oID=' . $orders['orders_id'] . '&action=edit') . '">' . tep_image(DIR_WS_ICONS . 'preview.gif', ICON_PREVIEW . ' order #' . $orders['orders_id']) . '</a>';
      $check_query = tep_db_query("select customers_id from " . TABLE_CUSTOMERS . " where customers_id = '" . (int)$orders['customers_id'] . "'");
      if( tep_db_num_rows($check_query) ) {
        echo '&nbsp;<a href="' . tep_href_link(FILENAME_CUSTOMERS, 'cID=' . $orders['customers_id'] . '&action=edit') . '">' . $orders['customers_name'] . '</a>'; 
      } else {
        echo '&nbsp;' . $orders['customers_name'];
      }
?>
                    </td>
                    <td class="dataTableContent" align="center"><?php echo $orders['customers_id']; ?></td>
                    <td class="dataTableContent" align="center"><?php echo '<a href="' . tep_href_link(FILENAME_EDIT_ORDERS, tep_get_all_get_params(array('oID', 'action')) . 'oID='. $orders['orders_id']) . '">' . tep_image(DIR_WS_ICONS . 'edit.gif', 'Extended Edit for Order #' . $orders['orders_id']) . '</a>'  ?></td>
                    <td class="dataTableContent" align="right"><?php echo strip_tags($orders['order_total']); ?></td>
                    <td class="dataTableContent" align="center"><?php echo tep_datetime_short($orders['date_purchased']); ?></td>
                    <td class="dataTableContent" align="right"><?php echo $orders['orders_status_name']; ?></td>
                    <td class="dataTableContent" align="right"><?php if (isset($oInfo) && is_object($oInfo) && ($orders['orders_id'] == $oInfo->orders_id)) { echo tep_image(DIR_WS_IMAGES . 'icon_arrow_right.gif', ''); } else { echo '<a href="' . tep_href_link(FILENAME_ORDERS, tep_get_all_get_params(array('oID')) . 'oID=' . $orders['orders_id']) . '">' . tep_image(DIR_WS_IMAGES . 'icon_info.gif', IMAGE_ICON_INFO) . '</a>'; } ?>&nbsp;</td>
                  </tr>
<?php
    }
?>
                  <tr>
                    <td colspan="8"><?php echo tep_image_submit('button_mass_delete.gif', IMAGE_DELETE); ?></td>
                  </tr>
                  <tr>
                    <td colspan="8"><hr /></td>
                  </tr>
                </table></form></td>
              </tr>
<?php
    if(isset($_GET['status']) && $_GET['status'] == 2) {
?>
			  <tr>
                <td colspan="6" valign="top"><?php echo tep_draw_form('change', FILENAME_ORDERS, tep_get_all_get_params(array('action')) . 'action=update_processing', 'post'); ?><table border="0" width="100%" cellspacing="0" cellpadding="0">
                  <tr>
				    <td align="right"><?php echo tep_image_submit('button_processing_to_awaiting.gif', 'Change order status from Processing to Awaiting Tracking'); ?></td>
                  </tr>
                </table></form></td>
			  </tr>
<?php
    }
?>

              <tr>
                <td colspan="5"><table border="0" width="100%" cellspacing="0" cellpadding="2">
                  <tr>
                    <td class="smallText" valign="top"><?php echo $orders_split->display_count($orders_query_numrows, MAX_DISPLAY_ADMIN_ORDERS, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_ORDERS); ?></td>
                    <td class="smallText" align="right"><?php echo $orders_split->display_links($orders_query_numrows, MAX_DISPLAY_ADMIN_ORDERS, MAX_DISPLAY_PAGE_LINKS, $_GET['page'], tep_get_all_get_params(array('page', 'oID', 'action'))); ?></td>
                  </tr>
                </table></td>
              </tr>
            </table></td>
<?php
  $heading = array();
  $contents = array();

  switch ($action) {
    case 'delete':
      $heading[] = array('text' => '<b>' . TEXT_INFO_HEADING_DELETE_ORDER . '</b>');

      $contents = array('form' => tep_draw_form('orders', FILENAME_ORDERS, tep_get_all_get_params(array('oID', 'action')) . 'oID=' . $oInfo->orders_id . '&action=deleteconfirm'));
      $contents[] = array('text' => TEXT_INFO_DELETE_INTRO . '<br><br><b>' . TEXT_INFO_PLACED_BY . $oInfo->customers_name . '</b>');
      $contents[] = array('text' => '<br>' . tep_draw_checkbox_field('restock') . ' ' . TEXT_INFO_RESTOCK_PRODUCT_QUANTITY);
      $contents[] = array('align' => 'center', 'text' => '<br>' . tep_image_submit('button_delete.gif', IMAGE_DELETE) . ' <a href="' . tep_href_link(FILENAME_ORDERS, tep_get_all_get_params(array('oID', 'action')) . 'oID=' . $oInfo->orders_id) . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>');
      break;
    default:
      if (isset($oInfo) && is_object($oInfo)) {
        $heading[] = array('text' => '<b>[' . $oInfo->orders_id . ']&nbsp;&nbsp;' . tep_datetime_short($oInfo->date_purchased) . '</b>');

        $contents[] = array('align' => 'center', 'text' => '<a href="' . tep_href_link(FILENAME_ORDERS, tep_get_all_get_params(array('oID', 'action')) . 'oID=' . $oInfo->orders_id . '&action=edit') . '">' . tep_image_button('button_edit.gif', IMAGE_EDIT) . '</a> <a href="' . tep_href_link(FILENAME_ORDERS, tep_get_all_get_params(array('oID', 'action')) . 'oID=' . $oInfo->orders_id . '&action=delete') . '">' . tep_image_button('button_delete.gif', IMAGE_DELETE) . '</a>');
        $contents[] = array('align' => 'center', 'text' => '<a href="' . tep_href_link(FILENAME_ORDERS_INVOICE, 'oID=' . $oInfo->orders_id) . '" TARGET="_blank">' . tep_image_button('button_invoice.gif', IMAGE_ORDERS_INVOICE) . '</a> <a href="' . tep_href_link(FILENAME_ORDERS_PACKINGSLIP, 'oID=' . $oInfo->orders_id) . '" TARGET="_blank">' . tep_image_button('button_packingslip.gif', IMAGE_ORDERS_PACKINGSLIP) . '</a>');
        $contents[] = array('text' => '<br>' . TEXT_DATE_ORDER_CREATED . ' ' . tep_date_short($oInfo->date_purchased));
        if (tep_not_null($oInfo->last_modified)) $contents[] = array('text' => TEXT_DATE_ORDER_LAST_MODIFIED . ' ' . tep_date_short($oInfo->last_modified));
        $contents[] = array('text' => '<br>' . TEXT_INFO_PAYMENT_METHOD . ' '  . $oInfo->payment_method);
        $shipping_method_query = tep_db_query("select title from " . TABLE_ORDERS_TOTAL . " where class = 'ot_shipping' and orders_id= '" . $oInfo->orders_id . "'");
        if( $shipping_method = tep_db_fetch_array($shipping_method_query) ) {
          $contents[] = array('text' => '<br>' . TEXT_INFO_SHIPPING_METHOD . ' '  . $shipping_method['title']);
        }

      }
      break;
  }

  if ( (tep_not_null($heading)) && (tep_not_null($contents)) ) {
    echo '            <td width="25%" valign="top">' . "\n";

    $box = new box;
    echo $box->infoBox($heading, $contents);

    echo '            </td>' . "\n";
  }
?>
          </tr>
        </table></td>
      </tr>
<?php
  }
?>
    </table></td>
<!-- body_text_eof //-->
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
<br>
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
