<?php
/*
  $Id: footer.php,v 1.12 2003/02/17 16:54:12 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/
?>
<br>
<table border="0" width="100%" cellspacing="0" cellpadding="2">
  <tr>
    <td align="center" class="smallText">Abbzy Baby E-Commerce Solutions :)</td>
  </tr>
  <tr>
    <td align="center" class="smallText">
<?php
/*
  The following copyright announcement is in compliance
  to section 2c of the GNU General Public License, and
  thus can not be removed, or can only be modified
  appropriately.

  For more information please read the osCommerce
  Copyright Policy at:

  http://www.oscommerce.com/about/copyright

  Please leave this comment intact together with the
  following copyright announcement.
*/
?>
E-Commerce Engine Copyright &copy; 2003 osCommerce<br>
osCommerce provides no warranty and is redistributable under the <a href="http://www.fsf.org/licenses/gpl.txt" target="_blank"><b>GNU General Public License</b></a>
    </td>
  </tr>
  <tr>
    <td align="center" class="smallText"><?php echo 'Copyright &copy; ' . date('Y') . '&nbsp;<a href="' . tep_catalog_href_link() . '" target="_blank"><b>' . STORE_NAME . '</b></a> - All rights reserved.'; ?></td>
  </tr>
</table>
