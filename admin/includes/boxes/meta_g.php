<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2008 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// META-G box for the Admin end
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
?>
          <tr>
            <td>
<?php
  $heading = array();
  $contents = array();

  $heading[] = array('text'  => BOX_HEADING_META_ZONES,
                     'link'  => tep_href_link(FILENAME_META_ZONES_CONFIG, 'selected_box=meta_config'));

  if ($selected_box == 'meta_config') {
    $contents[] = array('text' => tep_admin_files_boxes(FILENAME_META_ZONES_CONFIG, BOX_META_CONFIG) .
                                  tep_admin_files_boxes(FILENAME_META_TYPES, BOX_META_TYPES) .
                                  tep_admin_files_boxes(FILENAME_META_ZONES, BOX_META_ZONES) .
                                  tep_admin_files_boxes(FILENAME_META_FEEDS, BOX_META_FEEDS) .
                                  tep_admin_files_boxes(FILENAME_META_LEXICO, BOX_META_LEXICO) .
                                  tep_admin_files_boxes(FILENAME_META_EXCLUDE, BOX_META_EXCLUDE)
                       );
/*
'<a href="' . tep_href_link(FILENAME_META_ZONES_CONFIG, '', 'NONSSL') . '" class="menuBoxContentLink">' . BOX_META_CONFIG . '</a><br>' .
                                   '<a href="' . tep_href_link(FILENAME_META_TYPES, '', 'NONSSL') . '" class="menuBoxContentLink">' . BOX_META_TYPES . '</a><br>' .
                                   '<a href="' . tep_href_link(FILENAME_META_ZONES, '', 'NONSSL') . '" class="menuBoxContentLink">' . BOX_META_ZONES . '</a><br>' . 
                                   '<a href="' . tep_href_link(FILENAME_META_FEEDS, '', 'NONSSL') . '" class="menuBoxContentLink">' . BOX_META_FEEDS . '</a><br>' . 
                                   '<a href="' . tep_href_link(FILENAME_META_LEXICO, '', 'NONSSL') . '" class="menuBoxContentLink">' . BOX_META_LEXICO . '</a><br>' . 
                                   '<a href="' . tep_href_link(FILENAME_META_EXCLUDE, '', 'NONSSL') . '" class="menuBoxContentLink">' . BOX_META_EXCLUDE . '</a>'
                       );
*/
  }

  $box = new box;
  echo $box->menuBox($heading, $contents);
?>
            </td>
          </tr>
