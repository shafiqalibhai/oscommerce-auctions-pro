<?php
/*
  $Id: helpdesk.php,v 1.5 2005/08/16 20:56:39 lane Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/
?>
          <tr>
            <td>
<?php
  $heading = array();
  $contents = array();

  $heading[] = array('text'  => BOX_HEADING_HELPDESK,
                     'link'  => tep_href_link(FILENAME_HELPDESK, 'selected_box=helpdesk'));

  if ($selected_box == 'helpdesk') {
    $contents[] = array('text' => tep_admin_files_boxes(FILENAME_HELPDESK, BOX_HELPDESK_ENTRIES) .
                                  tep_admin_files_boxes(FILENAME_HELPDESK_DEPARTMENTS, BOX_HELPDESK_DEPARTMENTS) .
                                  tep_admin_files_boxes(FILENAME_HELPDESK_TEMPLATES, BOX_HELPDESK_TEMPLATES) .
                                  tep_admin_files_boxes(FILENAME_HELPDESK_STATUS, BOX_HELPDESK_STATUSES) .
                                  tep_admin_files_boxes(FILENAME_HELPDESK_PRIORITIES, BOX_HELPDESK_PRIORITIES) .
	                              tep_admin_files_boxes(FILENAME_HELPDESK_LOG, BOX_HELPDESK_LOG) .
                                  tep_admin_files_boxes(FILENAME_HELPDESK_POP3, BOX_HELPDESK_POP3)
                       );
/*
 '<a href="' . tep_href_link(FILENAME_HELPDESK) . '" class="menuBoxContentLink">' . BOX_HELPDESK_ENTRIES . '</a><br>' .
                                   //'<a href="' . tep_href_link(FILENAME_GREP_TEXT) . '" class="menuBoxContentLink">' . BOX_HELPDESK_REPOSITORY . '</a><br>' .
                                   '<a href="' . tep_href_link(FILENAME_HELPDESK_DEPARTMENTS) . '" class="menuBoxContentLink">' . BOX_HELPDESK_DEPARTMENTS . '</a><br>' .
                                   '<a href="' . tep_href_link(FILENAME_HELPDESK_TEMPLATES) . '" class="menuBoxContentLink">' . BOX_HELPDESK_TEMPLATES . '</a><br>' .
                                   '<a href="' . tep_href_link(FILENAME_HELPDESK_STATUS) . '" class="menuBoxContentLink">' . BOX_HELPDESK_STATUSES . '</a><br>' .
                                   '<a href="' . tep_href_link(FILENAME_HELPDESK_PRIORITIES) . '" class="menuBoxContentLink">' . BOX_HELPDESK_PRIORITIES . '</a><br>' .
                                   '<a href="' . tep_href_link(FILENAME_HELPDESK_POP3) . '" class="menuBoxContentLink" target="_BLANK">' . BOX_HELPDESK_POP3 . '</a>'
						 );
*/
  }

  $box = new box;
  echo $box->menuBox($heading, $contents);
?>
            </td>
          </tr>
