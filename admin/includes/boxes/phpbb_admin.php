<?php
/*
  $Id: phpbb_admin.php,v 1.2.2.1 2003/04/18 21:13:51 wilt Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 - 2003 osCommerce

  phpBB Integration

  Released under the GNU General Public License
*/


  $cl_box_groups[] = array(
    'heading' => BOX_FORUM,
    'apps' => array(
      array(
        'code' => BOX_FORUM,
        'title' => BOX_FORUM,
        'link' => tep_href_link(DIR_WS_FORUMS.'mcp.php','','SSL') // Can't make it DIR_WS_FORUMS/adm/index.php because we don't have the session id 'sid'
      )
    )
  );

?>
