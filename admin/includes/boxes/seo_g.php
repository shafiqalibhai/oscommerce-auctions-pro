<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2007 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// SEO_G box
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// http://www.oscommerce.com
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
?>
          <tr>
            <td>
<?php
  $heading = array();
  $contents = array();

  $heading[] = array('text'  => BOX_HEADING_SEO_ZONES,
                     'link'  => tep_href_link(FILENAME_SEO_ZONES_CONFIG, 'selected_box=seo_config'));

  if ($selected_box == 'seo_config') {
    $contents[] = array('text' => tep_admin_files_boxes(FILENAME_SEO_ZONES_CONFIG, BOX_SEO_CONFIG) .
                                  tep_admin_files_boxes(FILENAME_SEO_TYPES, BOX_SEO_TYPES) .
                                  tep_admin_files_boxes(FILENAME_SEO_ZONES, BOX_SEO_ZONES) .
                                  tep_admin_files_boxes(FILENAME_SEO_EXCLUDE, BOX_SEO_EXCLUDE) .
                                  tep_admin_files_boxes(FILENAME_SEO_REPORTS, BOX_SEO_REPORTS) .
                                  tep_admin_files_boxes(FILENAME_SEO_REDIRECTS, BOX_SEO_REDIRECTS)
                       );
/*
'<a href="' . tep_href_link(FILENAME_SEO_ZONES_CONFIG, '', 'NONSSL') . '" class="menuBoxContentLink">' . BOX_SEO_CONFIG . '</a><br>' .
                                   '<a href="' . tep_href_link(FILENAME_SEO_TYPES, '', 'NONSSL') . '" class="menuBoxContentLink">' . BOX_SEO_TYPES . '</a><br>' .
                                   '<a href="' . tep_href_link(FILENAME_SEO_ZONES, '', 'NONSSL') . '" class="menuBoxContentLink">' . BOX_SEO_ZONES . '</a><br>' . 
                                   '<a href="' . tep_href_link(FILENAME_SEO_EXCLUDE, '', 'NONSSL') . '" class="menuBoxContentLink">' . BOX_SEO_EXCLUDE . '</a><br>' . 
                                   '<a href="' . tep_href_link(FILENAME_SEO_REPORTS, '', 'NONSSL') . '" class="menuBoxContentLink">' . BOX_SEO_REPORTS . '</a><br>' . 
                                   '<a href="' . tep_href_link(FILENAME_SEO_REDIRECTS, '', 'NONSSL') . '" class="menuBoxContentLink">' . BOX_SEO_REDIRECTS . '</a>'
                       );
*/
  }

  $box = new box;
  echo $box->menuBox($heading, $contents);
?>
            </td>
          </tr>