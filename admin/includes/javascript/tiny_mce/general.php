<script language="javascript" type="text/javascript">
// Configuration file for tinyMCE editor: general
tinyMCE.init({
  mode : "exact",
  elements: "<?php echo $mce_str ?>",
  language: "en",
  theme : "advanced",
  skin : "o2k7",
  plugins : "safari,spellchecker,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,media,searchreplace,print,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,inlinepopups",
    theme_advanced_buttons1 : "bold,italic,underline,|,styleselect,formatselect.|,removeformat,|bullist,numlist,|,outdent,indent,|,link,unlink,anchor,image,|,forecolor,backcolor,|,cleanup,code",
    theme_advanced_buttons2 : "",
    theme_advanced_buttons3 : "",
	theme_advanced_buttons1_add_before : "",
	theme_advanced_buttons1_add : "",
	theme_advanced_buttons2_add_before: "",
	theme_advanced_buttons2_add : "",
	theme_advanced_buttons3_add_before : "",
	theme_advanced_buttons3_add : "",

  fullscreen_settings : {
  theme_advanced_path_location : "bottom"},
  //document_base_url : "<?php echo HTTP_SERVER.DIR_WS_CATALOG ?>",
  document_base_url : "http://base.example.com",
  content_css : "<?php echo HTTP_SERVER.DIR_WS_CATALOG ?>stylesheet.css",
  //external_image_list_url : "tinymce_images.js.php",
  		width : "100%",
		height : "460",
		//theme_medium_disable : "image",
  theme_advanced_toolbar_location : "bottom",
  theme_advanced_toolbar_align : "center",
  theme_advanced_path_location : "bottom",
  extended_valid_elements : "a[name|href|target|title|onclick],img[class|src|border=0|alt|title|hspace|vspace|width|height|align|onmouseover|onmouseout|name],hr[class|width|size|noshade],font[face|size|color|style],span[class|align|style]"
});
</script>
