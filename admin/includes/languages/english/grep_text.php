<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2007 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Generic Entries module for osCommerce Admin
// Adds entries for the catalog front end
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// http://www.oscommerce.com
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
define('HEADING_TITLE', 'Repository Text Entries');
define('HEADING_TITLE_SEARCH', 'Search:');
define('HEADING_TITLE_GOTO', 'Go To:');

define('TABLE_HEADING_ID', 'ID');
define('TABLE_HEADING_ACTION', 'Action');
define('TABLE_HEADING_STATUS', 'Status');
define('TABLE_HEADING_GREP', 'Repository Text');
define('TABLE_HEADING_TITLE', 'Title');
define('TABLE_HEADING_ADMIN', 'Admin');
define('TABLE_HEADING_DATE', 'Date');

define('TEXT_NEW_GREP', 'New Entry');
define('TEXT_GREP', 'Repository Text:');

define('TEXT_NO_GREP', 'Please insert a new entry.');

define('TEXT_EDIT_INTRO', 'Please make any necessary changes');

define('TEXT_INFO_COPY_TO_INTRO', 'Please choose a new name to copy this entry to');

define('TEXT_INFO_HEADING_DELETE_GREP', 'Delete Entry');
define('TEXT_INFO_HEADING_COPY_TO', 'Copy To');

define('TEXT_DELETE_GREP_INTRO', 'Are you sure you want to permanently delete this entry?');

define('TEXT_GREP_STATUS', 'Status:');
define('TEXT_GREP_NAME', 'Title:');
define('TEXT_GREP_DESCRIPTION', 'Description:');

define('TEXT_COPY_AS_DUPLICATE', 'Duplicate entry');
define('TEXT_GREP_AVAILABLE', 'Enabled');
define('TEXT_GREP_NOT_AVAILABLE', 'Disabled');
define('EMPTY_GREP', 'No Entries');

define('IMAGE_NEW_GREP_TEXT', 'Create a new generic text entry');
?>
