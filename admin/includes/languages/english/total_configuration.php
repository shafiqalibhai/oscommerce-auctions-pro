<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2007 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Total Configuration module for osCommerce Admin
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// http://www.oscommerce.com
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
define('HEADING_TITLE', 'Configuration');
define('HEADING_ALL', 'Total Configuration');
define('HEADING_SELECT', 'Select:');
define('HEADING_ACTION', 'Action');

define('TABLE_HEADING_CONFIGURATION_ID', 'ID');
define('TABLE_HEADING_CONFIGURATION_KEY', 'Key');
define('TABLE_HEADING_CONFIGURATION_TITLE', 'Title');
define('TABLE_HEADING_CONFIGURATION_VALUE', 'Value');
define('TABLE_HEADING_EDIT', 'Edit');
define('TABLE_HEADING_ACTION', 'Action');

define('HEADING_CONFIRM', 'Confirmation - Backup your Database before proceeding');

define('TABLE_HEADING_OPTIMIZE', 'Configuration Table Options');

define('TEXT_INFO_EDIT_INTRO', 'Please make any necessary changes');
define('TEXT_INFO_DATE_ADDED', 'Date Added:');
define('TEXT_INFO_LAST_MODIFIED', 'Last Modified:');

define('TEXT_INFO_OPTIMIZE_SORT', 'Sort by ID');
define('TEXT_INFO_OPTIMIZE_DUPLICATES', 'Remove Duplicates');

define('TEXT_INFO_OPERATION', '<b>Sort by ID:</b> Restructures the configuration table to use the IDs sequentially. <br><b>Remove Duplicates:</b> Removes duplicated keys from the configuration table.');

define('TEXT_INFO_CONFIRM_DUPLICATES', 'The following duplicates will be removed from the configuration table in the database');
define('TEXT_INFO_CONFIRM_CONFIG', 'The Configuration table will be sorted by configuration_id');
?>