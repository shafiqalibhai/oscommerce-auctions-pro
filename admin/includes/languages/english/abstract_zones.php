<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2007 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Abstract Zones component for osCommerce Admin
// Controls relationships among products, categories and attributes etc.
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// http://www.oscommerce.com
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/

define('HEADING_TITLE', 'Abstract Zones Framework');
define('HEADING_SUB_TITLE', 'Multi Abstract/Zones Options');
define('TABLE_HEADING_ABSTRACT_TYPE', 'Type');
define('TABLE_HEADING_ABSTRACT_ZONES', 'Abstract Zones');
define('TABLE_HEADING_ACTION', 'Action');

define('TEXT_INFO_HEADING_NEW_ZONE', 'New Zone');
define('TEXT_INFO_NEW_ZONE_INTRO', 'Please enter the new zone information');

define('TEXT_INFO_HEADING_EDIT_ZONE', 'Edit Zone');
define('TEXT_INFO_EDIT_ZONE_INTRO', 'Please make any necessary changes');

define('TEXT_INFO_HEADING_DELETE_ZONE', 'Delete Zone');
define('TEXT_INFO_DELETE_ZONE_INTRO', 'Are you sure you want to delete this zone?');

define('TEXT_INFO_HEADING_NEW_SUB_ZONE', 'New Sub Zone');
define('TEXT_INFO_NEW_SUB_ZONE_INTRO', 'Please enter the new sub zone information');

define('TEXT_INFO_HEADING_EDIT_SUB_ZONE', 'Edit Sub Zone');
define('TEXT_INFO_EDIT_SUB_ZONE_INTRO', 'Please make any necessary changes');

define('TEXT_INFO_HEADING_DELETE_SUB_ZONE', 'Delete Sub Zone');
define('TEXT_INFO_DELETE_SUB_ZONE_INTRO', 'Are you sure you want to delete this sub zone?');

define('TEXT_INFO_DATE_ADDED', 'Date Added:');
define('TEXT_INFO_LAST_MODIFIED', 'Last Modified:');
define('TEXT_INFO_ZONE_TYPE', 'Abstract Zone Type:');
define('TEXT_INFO_ZONE_NAME', 'Abstract Zone Name:');
define('TEXT_INFO_ZONE_DESC', 'Description:');
define('TEXT_INFO_ZONE_CLASS', 'Associated Class Script:');
define('TEXT_INFO_ZONE_TABLE', 'Associated DBase Tables:');

define('TEXT_INFO_NUMBER_ENTRIES', 'Number of Entries:');
define('TEXT_INFO_CATEGORY', 'Category:');
define('TEXT_INFO_PRODUCT', 'Product:');
define('TEXT_INFO_NO_ENTRIES', 'There are no entries defined for this zone.<br>Use the form below to insert new entries.');
define('TYPE_BELOW', 'All Zones');
define('PLEASE_SELECT', 'All Zones');

define('TABLE_HEADING_SELECT', 'Select');
define('TABLE_HEADING_MODE', 'Mode');
define('TABLE_HEADING_TYPE', 'Type');
define('TABLE_HEADING_PRICE', 'Price');
define('TABLE_HEADING_WEIGHT', 'Weight');
define('TABLE_HEADING_SORT', 'Sort Order');
define('TABLE_HEADING_STATUS', 'Status');
define('TABLE_HEADING_PRODUCTS_NAME', 'Product');
define('TABLE_HEADING_CATEGORIES_NAME', 'Category');
define('TEXT_SELECT_MULTICATEGORIES', 'Select the categories to insert from the following list.<br>Mode Notes: \'<b>Expand to Products</b>\' will expand and insert individual products from a category. \'<b>All Products</b>\' Will insert a single entry covering all products of a specific category');
define('TEXT_SELECT_MULTIZONES', 'Select the products from the categories form below to insert into this shipping zone. <br>Note: Duplicate products are filtered.');
define('TEXT_DELETE_MULTIZONE_CONFIRM', 'The following zones will be deleted from the <b>%s</b> zone');
define('TEXT_DELETE_MULTIZONE', 'Delete Selected Zones');
define('TEXT_UPDATE_MULTIZONE', 'Update Selected Zones');
define('TEXT_SWITCH_CATEGORIES', 'Switch to Categories Mode');
define('TEXT_SWITCH_PRODUCTS', 'Switch to Products Mode in the selected Category');
define('ERROR_CANNOT_ADD_MORE', 'All Products/Categories are covered. Cannot add more to this zone');
define('TEXT_INSERT_ALL', 'Insert All');


define('TEXT_ALL_PRODUCTS', 'All Products');
define('TEXT_ALL_CATEGORIES', 'All Categories');
define('TABLE_HEADING_CATEGORIES', 'Category');
define('TABLE_HEADING_PRODUCTS', 'Product');
define('TABLE_HEADING_ENTRIES', 'Entry');

define('TABLE_HEADING_ATTRIBUTES_ZONES', 'Attributes Zones');
define('TABLE_HEADING_ATTRIBUTES_OPTIONS', 'Attribute Options');
define('TABLE_HEADING_ATTRIBUTES_VALUES', 'Attribute Values');
define('TABLE_HEADING_ATTRIBUTES_PREFIX', 'Prefix');
define('TABLE_HEADING_ATTRIBUTES_PRICE', 'Price');
define('TABLE_HEADING_ATTRIBUTES_CONTROLS', 'Attribute Controls');
define('TEXT_INFO_ZONE_APPLY', 'Apply the selected zone to the catalog front-end');
define('TEXT_SELECT_MULTIOPTIONS', 'Check the attribute values and attribute option groups that apply to this attributes zone. Click <b>insert</b> to submit the form once finished or <b>cancel</b> to abort the changes and return back to the main zone level.');

define('TEXT_ALL_VALUES', 'All Values');
define('TEXT_DISPLAY_NUMBER_OF_ATTRIBUTES_ZONES', 'Displaying <b>%d</b> to <b>%d</b> (of <b>%d</b> attribute zones)');
define('TEXT_DISPLAY_NUMBER_OF_ATTRIBUTES_CATEGORIES_PRODUCTS', 'Displaying <b>%d</b> to <b>%d</b> (of <b>%d</b> categores/products)');
define('TEXT_DISPLAY_NUMBER_OF_ATTRIBUTES_OPTIONS_VALUES', 'Displaying <b>%d</b> to <b>%d</b> (of <b>%d</b> attributes options/values)');

define('TEXT_DISPLAY_NUMBER_OF_ABSTRACT_ZONES', 'Displaying <b>%d</b> to <b>%d</b> (of <b>%d</b> abstract zones)');
define('TEXT_DISPLAY_NUMBER_OF_CATEGORIES_PRODUCTS', 'Displaying <b>%d</b> to <b>%d</b> (of <b>%d</b> categores/products)');

define('TEXT_SELECT_MULTIENTRIES', 'Select the entries to insert from the following list. Entries can then be controlled from the main sub-zone and be related only with this abstract zone.');
define('TABLE_HEADING_SEQUENCE_ORDER', 'Sequence');
define('TABLE_HEADING_ASSIGN_PRODUCTS', 'Assign');

define('TEXT_APPLY_MULTIZONE_CONFIRM', 'This operation <b>will update the catalog products</b>. Attributes of all products as described by the categories/product entries below (From Zone: <b>%s</b>) will be affected.');
define('TEXT_SWITCH_ATTRIBUTES', 'Switch to Attributes Mode for options/values');
define('TEXT_DELETE_ALL_ATTRIBUTES_CONFIRM', 'This operation removes all relationships between products/attributes assigned. Attribute Options and Values are not affected.');

define('TABLE_HEADING_PRODUCTS_FIELDS_CONTROLS', 'Products Fields Zones');
define('TEXT_INFO_ZONE_APPLY_PRICE', 'Apply the product prices of the selected zone to the catalog front-end');
define('TEXT_INFO_ZONE_APPLY_WEIGHT', 'Apply the product weights of the selected zone to the catalog front-end');
define('TEXT_INFO_ZONE_APPLY_STATUS', 'Apply the product status of the selected zone to the catalog front-end');

define('TEXT_INFO_ADDITIONAL_OPTIONS', 'Additional Options for this zone');
define('WARNING_NOTHING_SELECTED', 'No entries selected. Use the checkboxes to select entries first');

define('TABLE_HEADING_EXTRA_PERCENTAGE', 'Price Adjustment');
define('TABLE_HEADING_VENDORS_CONTROLS', 'Vendor Actions');
define('TEXT_INFO_VENDORS_LIST', 'Vendors List');
?>
