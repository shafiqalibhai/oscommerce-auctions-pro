<?php
/*
 $Id: comment_bar.php,v 4.0 2005/02/16 Skeedo.com Enterprises Exp $
 Admin Comments Toolbar 4.0 by Skeedo.com Enterprises info@skeedo.com
  - Update to multilangual: Ben Zukrel   Date:2005/02/16
  
  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

//The Button Text
define('TEXT_BUTTON_01', 'Canada Post');
define('TEXT_BUTTON_02', 'USPS');
define('TEXT_BUTTON_03', 'OoS Refund');
define('TEXT_BUTTON_04', 'Updated');
define('TEXT_BUTTON_05', 'No Match Void');
define('TEXT_BUTTON_06', 'No Match Refund');
define('TEXT_BUTTON_07', 'Package Signing');
define('TEXT_BUTTON_08', 'Processing');
define('TEXT_BUTTON_09', 'Attempted');
define('TEXT_BUTTON_10', 'Separately');
define('TEXT_BUTTON_11', 'EMS');
define('TEXT_BUTTON_12', 'Returned');
define('TEXT_BUTTON_13', 'Button Text');
define('TEXT_BUTTON_14', 'Button Text');

define('TEXT_BUTTON_RESET', 'Reset');

define('TEXT_PROMPT', 'Enter scheduled restock date:');//used in script language="javascript prompt window
define('TEXT_TRACKNO', 'Enter tracking number:');//used in script language="javascript prompt window

define('TEXT_NULL1', 'Enter null1 parameter:');//used in script language="javascript prompt window
define('TEXT_NULL2', 'Enter null2 parameter:');//used in script language="javascript prompt window
define('TEXT_COMMENT_PRE_10','Your ');

//The Button comment
define('TEXT_COMMENT_01','Your order has been shipped via Canada Post. Thank you for shopping at OmarsKicks.com. You can track the status of your order online at www.CanadaPost.ca using the tracking number below:\n\nTracking Number:\n');
define('TEXT_COMMENT_02','Your order has been shipped via USPS. Thank you for shopping at OmarsKicks.com. You can track the status of your order online at www.USPS.com using the tracking number below:\n\nTracking Number:\n');
define('TEXT_COMMENT_03','Unfortunately the shoes you ordered are out of stock. Your transaction has been refunded, thank you for shopping at OmarsKicks.com.\n\n');
define('TEXT_COMMENT_04','Your shipping address has been updated to reflect your Paypal.com confirmed address. Contact us within 48 hours if you prefer we cancel this order.\n');
define('TEXT_COMMENT_05','The shipping address you provided does not match the address the issuing bank of your credit card has on file. Your payment has been voided and your order will not be processed.\n');
define('TEXT_COMMENT_06','The shipping address you provided does not match the address the issuing bank of your credit card has on file. Your payment has been refunded and your order will not be processed.');
define('TEXT_COMMENT_07','All packages must be signed for. If you are not home at the time of delivery, a notice will be left and you will be required to pick up the package at your nearest post office.\nAdditionally, once your package is shipped you will also be given a tracking number.\nPlease use this tracking number to anticipate approximate delivery date.');
define('TEXT_COMMENT_08','');
define('TEXT_COMMENT_09','Your tracking number shows that USPS attempted to deliver your order, however no one was home to receive it. Your parcel is being held at your local post office. Please pick up your parcel at the post office as soon as possible. If you do not pick up your parcel, it will be returned to the warehouse');
define('TEXT_COMMENT_10',' has been shipped separately and your tracking number is:\n\n');
define('TEXT_COMMENT_11','Your order has been shipped via EMS. Thank you for shopping at OmarsKicks.com. You can track the status of your order online at www.ems.com.cn/english-main.jsp using the tracking number below:\n\n');
define('TEXT_COMMENT_12','We have received your returned merchandise. Your transaction has been refunded, thank you for shopping at OmarsKicks.com.');

define('TEXT_COMMENT_13','Comment text ');
define('TEXT_COMMENT_14','Comment text ');
?>
