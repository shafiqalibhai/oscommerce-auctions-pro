<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2011 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Auctions Strings for osCommerce Admin
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// http://www.oscommerce.com
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
define('HEADING_TITLE', 'Auctions');
define('HEADING_TITLE_NEW', 'New Auction');
define('HEADING_TITLE_EDIT', 'Edit Auction');
define('HEADING_TITLE_SELECT_PRODUCTS', 'Select Products to Copy as Auctions');

define('TABLE_HEADING_ID', 'ID');
define('TABLE_HEADING_IMAGE', 'Image');
define('TABLE_HEADING_PRODUCT', 'Product');
define('TABLE_HEADING_ACTION', 'Action');
define('TABLE_HEADING_STATUS', 'Status');
define('TABLE_HEADING_TYPE', 'Type');
define('TABLE_HEADING_TITLE', 'Title');
define('TABLE_HEADING_GROUP', 'Group');
define('TABLE_HEADING_INSTANCES', 'Already in Auctions/Tiers');
define('TABLE_HEADING_DATE', 'Date');
define('TABLE_HEADING_SELECT', 'Select');
define('TABLE_HEADING_TIERS', 'Auction Tiers');

define('TEXT_INFO_COPY_TO_INTRO', 'Please choose a new name to copy this entry to');

define('TEXT_INFO_HEADING_DELETE', 'Delete Group');
define('TEXT_INFO_HEADING_EDIT', 'Edit Group');
define('TEXT_INFO_HEADING_NEW', 'New Group');

define('TEXT_INFO_DELETE_INTRO', 'Are you sure you want to permanently delete this entry?');
define('TEXT_INFO_EDIT_INTRO', 'Please make any necessary changes');
define('TEXT_INFO_NEW_INTRO', 'Please fill in the form below to create a new group');

define('TEXT_INFO_GROUP_NAME', 'Group Name:');
define('TEXT_INFO_GROUP_DESC', 'Group Description:');

define('TEXT_INFO_ZERO_AUCTIONS', 'No Auction');
define('TEXT_INFO_NO_AUCTIONS', 'Please insert a new auction or select an existing one.');

define('TEXT_INFO_IMAGE_NEW', 'Create a new group');
define('TEXT_INFO_ENTRIES', 'Entries');

define('TEXT_INFO_GROUP_FILTER', 'Filter by Group:');
define('TEXT_INFO_GROUP_ALL', 'Show All');

define('TEXT_INFO_AUCTION_STATE', 'Auction State:');
define('TEXT_INFO_AUCTION_NAME', 'Auction Name:');
define('TEXT_INFO_AUCTION_IMAGE', 'Auction Image:');
define('TEXT_INFO_AUCTION_SORT_ORDER', 'Sort Order to Display:');
define('TEXT_INFO_PRODUCT_ID', 'Product ID:');
define('TEXT_INFO_PRODUCT_NAME', 'Using Product:');
define('TEXT_INFO_GROUP_SELECT', 'Assign to Group:');
define('TEXT_INFO_AUCTION_OVERBID', 'Allow Auction Overbid:');
define('TEXT_INFO_AUCTION_BID_STEP', 'Bid Step:');
define('TEXT_INFO_AUCTION_MAX_BIDS', 'Maximum Bids:');
define('TEXT_INFO_AUCTION_COUNTDOWN_BIDS', 'Countdown Bids:');
define('TEXT_INFO_AUCTION_COUNTDOWN_TIMER', 'Countdown Timer:');
define('TEXT_INFO_AUCTION_AUTO_TIMER', 'Auto Expiry Timer (Days):');
define('TEXT_INFO_AUCTION_START_PRICE', 'Starting Price:');
define('TEXT_INFO_AUCTION_SHIPPING', 'Shipping Cost:');
define('TEXT_INFO_AUCTION_START_PAUSE', 'Enable Pause at:');
define('TEXT_INFO_AUCTION_END_PAUSE', 'Disable Pause at:');
define('TEXT_INFO_AUCTION_DATE_START', 'Start Auction at:');
define('TEXT_INFO_AUCTION_DATE_EXPIRE', 'Expire Auction at:');
define('TEXT_INFO_AUCTION_DESCRIPTION', 'Short Description:');
define('TEXT_INFO_AUCTION_TYPE', 'Type of Auction:');
define('TEXT_INFO_AUCTION_CAP_PRICE', 'Cap Price:');
define('TEXT_INFO_AUCTION_BIDS_ADDED', 'Show Bids Added:');
define('TEXT_INFO_AUCTION_BIDS_LEFT', 'Show Bids Left:');

define('TEXT_INFO_AUCTION_BIDS_TIMERS', 'Countdown Bids and Timers Pairs');
define('TEXT_INFO_AUCTION_BIDS_TIMERS_ADD', 'Add another Bid and Timer Pair');

define('TEXT_INFO_AUCTION_BIDS_ENTERED', 'Bids Entered:');
define('TEXT_INFO_AUCTION_BIDS_VALUE', 'Bids Value:');

define('TEXT_INFO_AUCTION_PREFIX', 'Auction');
define('TEXT_INFO_LOADED_IMAGE', 'Loaded Image:');

define('SUCCESS_AUCTION_PRODUCTS_ENTERED', 'Selected Products were entered as auction items');
define('SUCCESS_AUCTION_CREATED', 'New Auction Created');
define('SUCCESS_AUCTION_UPDATED', 'Auction Updated');
define('WARNING_AUCTION_DELETED', 'Specified auction was removed');
define('ERROR_AUCTION_PRODUCTS_TICK', 'Use the checkboxes on the left and select at least one product');
define('ERROR_AUCTION_PRODUCTS_INVALID', 'Invalid Product Specified');
define('ERROR_AUCTION_INVALID', 'Invalid Auction Specified');
?>
