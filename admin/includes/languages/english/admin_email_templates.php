<?php
/*
********************************************************************
*
* OSCommerce Contribution
*
* Name:    Email Templates
* 
* Author:  idabagusmade@gmail.com
*
* Purpose: Allow to create email templates and use them mainly for emailing customers or orders
*
* Date:    January 8, 2005
*
********************************************************************
//----------------------------------------------------------------------------
// Copyright (c) 2006-2007 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Email Templates language file for osC Admin
// - Moved language strings into a separate file
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// http://www.oscommerce.com
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
define('HEADING_TITLE', 'Email Templates');
define('TITLE_ADD_EMAIL_TEMPLATE', 'Add Email Template');
define('TITLE_LIST_EMAIL_TEMPLATE', 'List Email Template');
define('TABLE_HEADING_UPDATE', 'Update Email Template');
define('TABLE_HEADING_ADD', 'Add Email Template');
define('TABLE_HEADING_GROUP', 'Group');
define('TABLE_HEADING_TITLE', 'Title');
define('TABLE_HEADING_EMAIL_SUBJECT', 'Email Subject');
define('TABLE_HEADING_EMAIL_CONTENTS', 'Email Contents');
define('TABLE_HEADING_LAST_UPDATE', 'Last Update');
define('TABLE_HEADING_HELP', '
                            <p><u>Reserved Words:</u></p>
                            <font color="blue">[CUSTOMER_NAME]</font>:&nbsp;<i>Will be replaced by customer full name</i><br><br>
                            <font color="blue">[CUSTOMER_EMAIL]</font>:&nbsp;<i>Will be replaced by customer email address</i><br><br>
                            <font color="blue">[CUSTOMER_DOB]</font>:&nbsp;<i>Will be replaced by customer date of birth</i><br><br>
                            <font color="blue">[CUSTOMER_PHONE]</font>:&nbsp;<i>Will be replaced by customer telephone number</i><br><br>
                            <font color="blue">[CUSTOMER_FAX]</font>:&nbsp;<i>Will be replaced by customer fax number</i><br><br>
                        ');

define('TEXT_EMAIL_TEMPLATE_SETUP_PAGE', 'Setup Page');
define('TEXT_EMAIL_TEMPLATE_NEW_GROUP', ' or update to new group ');
define('TEXT_EMAIL_TEMPLATE_NO_GROUP', ' No group. Create one! ');
define('TEXT_EMAIL_TEMPLATE', 'Email Templates: ');
