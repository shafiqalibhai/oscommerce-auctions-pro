<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2007 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// META-G Exclusion List for Admin
//----------------------------------------------------------------------------
// Released under the GNU General Public License v3.0
//----------------------------------------------------------------------------
*/
define('HEADING_TITLE', 'META-G Words Exclusion List');
define('HEADING_TITLE2', 'Insert Word to Exclude');

define('TABLE_HEADING_PHRASE', 'Word');
define('TABLE_HEADING_SELECT', 'Select');
define('TABLE_HEADING_SORT', 'Sort Order');
define('TABLE_HEADING_STATUS', 'Status');
define('TEXT_INSERT', 'Insert word into the exclusion list');
define('TEXT_UPDATE', 'Update selected entries');
define('TEXT_DELETE', 'Delete selected entries');

define('TEXT_DISPLAY_NUMBER_OF_SCRIPTS', 'Displaying <b>%d</b> to <b>%d</b> (of <b>%d</b> phrases)');

define('TEXT_INFO_DELETE', 'Are you sure you want to remove these script entries? <br>Note: No files are removed only the exclusion database table for the SEO-G is updated');
define('TEXT_INFO_MAIN', 'The following words are used by META-G to detect the appropriate phrases used for the keywords Meta-Tag. Phrases/Words those intersect with the exclusion list are not used for keywords generation.<br>Select the words to update or delete from the following list.');
define('TEXT_INFO_MAIN2', 'This operation inserts words to exclude in the database.');
define('TEXT_GENERATE', 'Generate phrases/words for the selected class');
?>
