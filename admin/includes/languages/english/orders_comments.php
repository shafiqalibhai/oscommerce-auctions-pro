<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2007 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Admin: Orders Comments
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
define('HEADING_TITLE', 'Orders Comments Entries');
define('HEADING_TITLE_SEARCH', 'Search:');
define('HEADING_TITLE_GOTO', 'Go To:');

define('TABLE_HEADING_ID', 'ID');
define('TABLE_HEADING_ACTION', 'Action');
define('TABLE_HEADING_STATUS', 'Status');
define('TABLE_HEADING_COMMENTS', 'Generic Text');
define('TABLE_HEADING_TITLE', 'Title');
define('TABLE_HEADING_ADMIN', 'Admin');
define('TABLE_HEADING_DATE', 'Date');
define('TABLE_HEADING_ORDER_STATUS', 'Order Status');

define('TEXT_NEW_COMMENTS', 'New Entry');
define('TEXT_COMMENTS', 'Generic Text:');
define('TEXT_COMMENTS_ORDER_STATUS', 'Orders Status:');

define('TEXT_NO_COMMENTS', 'Please insert a new entry.');

define('TEXT_EDIT_INTRO', 'Please make any necessary changes');

define('TEXT_INFO_COPY_TO_INTRO', 'Please choose a new name to copy this entry to');

define('TEXT_INFO_HEADING_DELETE_COMMENTS', 'Delete Entry');
define('TEXT_INFO_HEADING_COPY_TO', 'Copy To');

define('TEXT_DELETE_COMMENTS_INTRO', 'Are you sure you want to permanently delete this entry?');

define('TEXT_COMMENTS_STATUS', 'Status:');
define('TEXT_COMMENTS_NAME', 'Title:');
define('TEXT_COMMENTS_DESCRIPTION', 'Description:');

define('TEXT_COPY_AS_DUPLICATE', 'Duplicate entry');
define('TEXT_COMMENTS_AVAILABLE', 'Enabled');
define('TEXT_COMMENTS_NOT_AVAILABLE', 'Disabled');
define('EMPTY_COMMENTS', 'No Entries');

define('IMAGE_NEW_COMMENTS_TEXT', 'Create a new generic text entry');
?>
