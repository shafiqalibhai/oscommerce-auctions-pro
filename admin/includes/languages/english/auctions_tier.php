<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2011 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Auctions Tiers Strings for osCommerce Admin
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// http://www.oscommerce.com
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
define('HEADING_TITLE', 'Auctions Tiers');
define('HEADING_TITLE_NEW', 'New Tier');

define('HEADING_TITLE_EDIT', 'Edit Auction');
define('HEADING_TITLE_SELECT_PRODUCTS', 'Select Products to Copy as Auctions');

define('TABLE_HEADING_ID', 'ID');
define('TABLE_HEADING_IMAGE', 'Image');
define('TABLE_HEADING_ACTION', 'Action');
define('TABLE_HEADING_TYPE', 'Type');
define('TABLE_HEADING_STATUS', 'Status');
define('TABLE_HEADING_TIERS', 'Tiers');
define('TABLE_HEADING_TITLE', 'In Auction');
define('TABLE_HEADING_PRODUCT_TITLE', 'Product Prize');
define('TABLE_HEADING_PRODUCT_BIDS', 'Product Bids');
define('TABLE_HEADING_PRODUCT_IN_AUCTIONS', 'Product in Auctions');
define('TABLE_HEADING_GROUP', 'Group');
define('TABLE_HEADING_DATE', 'Date');
define('TABLE_HEADING_SELECT', 'Select');

define('TEXT_INFO_COPY_TO_INTRO', 'Please choose a new name to copy this entry to');

define('TEXT_INFO_HEADING_DELETE', 'Delete Group');
define('TEXT_INFO_HEADING_EDIT', 'Edit Group');
define('TEXT_INFO_HEADING_NEW', 'New Group');

define('TEXT_INFO_DELETE_INTRO', 'Are you sure you want to permanently delete this entry?');
define('TEXT_INFO_EDIT_INTRO', 'Please make any necessary changes');
define('TEXT_INFO_NEW_INTRO', 'Please fill in the form below to create a new group');

define('TEXT_INFO_GROUP_NAME', 'Group Name:');
define('TEXT_INFO_GROUP_DESC', 'Group Description:');

define('TEXT_INFO_ZERO_AUCTIONS', 'No Auction');
define('TEXT_INFO_NO_AUCTIONS', 'Please insert a new auction or select an existing one.');

define('TEXT_INFO_DETAILS', 'Edit Tier Details');

define('TEXT_INFO_AUCTION_ID', 'Auction ID:');
define('TEXT_INFO_AUCTION_STATE', 'Auction State:');
define('TEXT_INFO_AUCTION_NAME', 'Auction Name:');
define('TEXT_INFO_AUCTION_IMAGE', 'Auction Image:');
define('TEXT_INFO_AUCTION_SORT_ORDER', 'Sort Order to Display:');
define('TEXT_INFO_PRODUCT_ID', 'Product ID:');
define('TEXT_INFO_PRODUCT_NAME', 'Using Product:');
define('TEXT_INFO_GROUP_SELECT', 'Assign to Group:');
define('TEXT_INFO_AUCTION_OVERBID', 'Allow Auction Overbid:');
define('TEXT_INFO_AUCTION_BID_STEP', 'Bid Step:');
define('TEXT_INFO_AUCTION_MAX_BIDS', 'Maximum Bids:');
define('TEXT_INFO_AUCTION_COUNTDOWN_BIDS', 'Countdown Bids:');
define('TEXT_INFO_AUCTION_START_PRICE', 'Starting Price:');
define('TEXT_INFO_AUCTION_SHIPPING', 'Shipping Cost:');
define('TEXT_INFO_AUCTION_START_PAUSE', 'Enable Pause at:');
define('TEXT_INFO_AUCTION_END_PAUSE', 'Disable Pause at:');
define('TEXT_INFO_AUCTION_DATE_EXPIRE', 'Expire Auction at:');
define('TEXT_INFO_AUCTION_DESCRIPTION', 'Short Description:');
define('TEXT_INFO_AUCTION_PRIZE', 'Tier Prize:');

define('TEXT_INFO_AUCTION_PREFIX', 'Auction');
define('TEXT_INFO_LOADED_IMAGE', 'Loaded Image:');

define('SUCCESS_AUCTION_CREATED', 'New Auction Tier Created');
define('SUCCESS_AUCTION_UPDATED', 'Auction Tier Updated');
define('WARNING_AUCTION_TIER_DELETED', 'Specified auction tier was removed');
define('ERROR_AUCTION_PRODUCTS_TICK', 'Use the checkboxes on the left and select at least one product');
define('ERROR_AUCTION_PRODUCTS_INVALID', 'Invalid Product Specified');
define('ERROR_AUCTION_TIER_INVALID', 'Invalid Auction Tier Specified');
?>
