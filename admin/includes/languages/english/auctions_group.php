<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2007 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Generic Entries module for osCommerce Admin
// Adds entries for the catalog front end
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// http://www.oscommerce.com
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
define('HEADING_TITLE', 'Auctions Groups');

define('TABLE_HEADING_ID', 'ID');
define('TABLE_HEADING_ACTION', 'Action');
define('TABLE_HEADING_STATUS', 'Status');
define('TABLE_HEADING_TITLE', 'Title');
define('TABLE_HEADING_DATE', 'Date');

define('TEXT_INFO_COPY_TO_INTRO', 'Please choose a new name to copy this entry to');

define('TEXT_INFO_HEADING_DELETE', 'Delete Group');
define('TEXT_INFO_HEADING_EDIT', 'Edit Group');
define('TEXT_INFO_HEADING_NEW', 'New Group');

define('TEXT_INFO_DELETE_INTRO', 'Are you sure you want to permanently delete this entry?');
define('TEXT_INFO_EDIT_INTRO', 'Please make any necessary changes');
define('TEXT_INFO_NEW_INTRO', 'Please fill in the form below to create a new group');

define('TEXT_INFO_GROUP_NAME', 'Group Name:');
define('TEXT_INFO_GROUP_DESC', 'Group Description:');
define('TEXT_INFO_GROUP_ORDER', 'Sort Order:');

define('TEXT_INFO_ZERO_GROUPS', 'No Groups');
define('TEXT_INFO_NO_GROUPS', 'Please insert a new group.');

define('TEXT_INFO_IMAGE_NEW', 'Create a new group');
define('TEXT_INFO_ENTRIES', 'Entries');
?>
