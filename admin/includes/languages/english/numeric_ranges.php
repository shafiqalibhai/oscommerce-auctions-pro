<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2007 Asymmetric Software. Innovation & Excellence.
// Author: Mark Samios
// http://www.asymmetrics.com
// ---------------------------------------------------------------------------
// Admin: Numeric Ranges Strings file
// ----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/

define('HEADING_TITLE', 'Numeric Ranges');

define('TABLE_HEADING_ID', 'ID');
define('TABLE_HEADING_ACTION', 'Action');
define('TABLE_HEADING_STATUS', 'Status');
define('TABLE_HEADING_DESC', 'Description');
define('TABLE_HEADING_MIN', 'Min');
define('TABLE_HEADING_MAX', 'Max');
define('TABLE_HEADING_ORDER', 'Order');
define('TABLE_HEADING_SELECT', 'Select');

define('IMAGE_UPDATE_FIELDS', 'Update Fields');
define('IMAGE_REMOVE_FIELDS', 'Remove Fields');
define('IMAGE_ADD_FIELD', 'Add Field');

define('TEXT_INFO', 'The Numeric Ranges module can be used for filtering purposes. This script only defines the ranges.<br>
Relationships among products, attributes, orders, etc., should be defined on the catalog end, therefore utilizing the ranges listed here.<br>
There are 2 sections within the list below. Use the top section to create a new range. Use the lower section to modify the properties or delete a range.<br>
<br>Numeric Ranges Definitions:
<ul>
<li><b>Min:</b>&nbsp;Controls the minimum value of a range.</li>
<li><b>Max:</b>&nbsp;Controls the maximum value of a range.</li>
<li><b>Description:</b>&nbsp;Short description upto 64 characters long that maybe used to represent a range.</li>
<li><b>Order:</b>&nbsp;The order the ranges will be processed. The lower the order the higher the priority of a range.</li>
<li><b>Status:</b>&nbsp;Range visibility. Active/Inactive. When Active a range should not be processed on the catalog end.</li>
</ul>
');
?>
