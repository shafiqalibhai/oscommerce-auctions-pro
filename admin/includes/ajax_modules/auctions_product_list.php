<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2011 Asymmetric Software. Innovation & Excellence.
// Author: Mark Samios
// http://www.asymmetrics.com
//----------------------------------------------------------------------------
// Admin: Auctions Product List AJAX Module
//----------------------------------------------------------------------------
// I-Metrics CMS
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// http://www.oscommerce.com
// Copyright (c) 2003 osCommerce
------------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
?>
        <div class="wider"><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top"><table class="tabledata">
              <tr class="dataTableHeadingRow">
                <th><?php echo TABLE_HEADING_ID; ?></th>
                <th class="calign"><?php echo TABLE_HEADING_IMAGE; ?></th>
                <th><?php echo TABLE_HEADING_PRODUCT_TITLE; ?></th>
                <th class="calign"><?php echo TABLE_HEADING_PRODUCT_BIDS; ?></th>
                <th class="calign"><?php echo TABLE_HEADING_PRODUCT_IN_AUCTIONS; ?></th>
              </tr>
<?php
  $products_query_raw = "select products_id, products_name from " . TABLE_PRODUCTS_DESCRIPTION . " where language_id = '" . (int)$languages_id . "' order by products_name";
  $products_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS, $products_query_raw, $products_query_numrows);
  $products_array = array();
  tep_query_to_array($products_query_raw, $products_array, 'products_id');

  $rows = 0;
  foreach( $products_array as $key => $product ) {
    $rows++;
    $rowclass = ($rows%2)?'dataTableRow':'dataTableRowAlt';
    $data_query = tep_db_query("select products_bids, products_image from " . TABLE_PRODUCTS . " where products_id = '" . $key . "'");
    $data_array = tep_db_fetch_array($data_query);

    $check_query = tep_db_query("select count(*) as total from " . TABLE_AUCTIONS . " where status_id < 2 and products_id = '" . $key . "'");
    $check_array = tep_db_fetch_array($check_query);
?>
              <tr class="<?php echo $rowclass . ' products_tier_row'; ?>" attr="<?php echo $key; ?>">
                <td><?php echo $key; ?></td>
                <td class="calign"><?php echo tep_image(DIR_WS_CATALOG_IMAGES . $data_array['products_image'], $product['products_name'], SMALL_IMAGE_WIDTH, SMALL_IMAGE_HEIGHT); ?></td>
                <td><?php echo $product['products_name']; ?></td>
                <td class="calign"><?php echo $data_array['products_bids']; ?></td>
                <td class="calign"><?php echo $check_array['total']; ?></td>
              </tr>
<?php
  }
?>
            </table></td>
          </tr>
          <tr>
            <td><table class="tabledata">
              <tr>
                <td class="smallText"><?php echo $products_split->display_count($products_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_ENTRIES); ?></td>
                <td class="smallText ralign"><?php echo $products_split->display_links($products_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, MAX_DISPLAY_PAGE_LINKS, $_GET['page'], 'module=auctions_product_list&strings=auctions_tier.php'); ?></td>
              </tr>
            </table></td>
          </tr>
        </table></div>
