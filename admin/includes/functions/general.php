<?php
/*
  $Id: general.php,v 1.160 2003/07/12 08:32:47 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

//-MS- Functions Ported from catalog end
////
// Return a product's special price (returns nothing if there is no offer)
// TABLES: products
  function tep_get_products_special_price($product_id) {
    $product_query = tep_db_query("select specials_new_products_price from " . TABLE_SPECIALS . " where products_id = '" . (int)$product_id . "' and status");
    $product = tep_db_fetch_array($product_query);

    return $product['specials_new_products_price'];
  }
//-MS- Functions Ported from catalog end EOM

  function tep_get_to_post() {
    foreach($_GET as $key => $value) {
      $_POST[$key] = $_GET[$key];
    }
    reset($_GET);
  }

// Strips special characters
  function tep_special_chars_to_html($special_string) {

    $chars = array(
           151 => '&#8212;',
           153 => '&#8482;',
           139 => '&#8249;',
           155 => '&#8250;',
           174 => '&#174;',
           146 => '&#8217;',
           147 => '&#8220;',
           148 => '&#8221;',
           150 => '&#8211;',
           39 => '&#8216;',
           169 => '&#169;');

    $special_string = str_replace(array_map('chr', array_keys($chars)), $chars, $special_string);
    return $special_string;
  }

// Strips special characters
  function tep_html_to_special_chars($special_string) {

    $chars = array(
           8482 => '',
           153 => '',
           174 => '',
           169 => '');

    $special_string = str_replace(array_map('chr', array_keys($chars)), $chars, $special_string);
    return $special_string;
  }

  function tep_query_to_array( $string_query, &$result_array, $index=false) {
    if( !is_array($result_array) )
      $result_array = array();

    $query = tep_db_query($string_query);
    if( !$index ) {
      while( $result_array[] = tep_db_fetch_array($query) );
      array_pop($result_array);
    } else {
      while( $tmp_array = tep_db_fetch_array($query) ) {
        $result_array[$tmp_array[$index]] = $tmp_array;
      }
    }
    tep_db_free_result($query);
  }

  function tep_result_to_array( $query, &$result_array, $free=true) {
    if( !is_array($result_array) )
      $result_array = array();
    while( $result_array[] = tep_db_fetch_array($query) );
    array_pop($result_array);
    if($free)
      tep_db_free_result($query);
  }

  function tep_php_string($string) {
    $converted = stripslashes($string);
    $converted = tep_special_chars_to_html($converted);
    return $converted;
  }
  
  function tep_html_string($string) {
    $converted = htmlspecialchars($string);
    $converted = tep_special_chars_to_html($converted);
    return $converted;
  }

//-MS- safe string added
  function tep_create_safe_string($string, $separator=' ') {
    $string = preg_replace('/\s\s+/', ' ', trim($string));
    $string = preg_replace("/[^0-9a-z]+/i", $separator, $string);
    $string = trim($string, $separator);
    $string = str_replace($separator . $separator . $separator, $separator, $string);
    $string = str_replace($separator . $separator, $separator, $string);
    return $string;
  }
//-MS- safe string added EOM

////-MS- products collection added
  function tep_get_collection($category_id=0, $filter_array=array(), $where_flag=true, $select_flag=true, $products_array=array() ) {
    global $languages_id;

    $in_string = '1';
    if( $category_id > 0 ) {
      tep_get_subcategories($categories_array, $category_id);
      $categories_array[] = (int)$category_id;
      $in_string = 'p2c.categories_id in (' . implode(',',$categories_array) . ')';
    }
    $select_string = '';

    if( is_array($filter_array) ) {
      foreach($filter_array as $db_col => $index) {
        if($where_flag) {
          $in_string .= " and p." . tep_db_input(tep_db_prepare_input($db_col)) . "='" . (int)$index  . "'";
        }
        if($select_flag) {
          $select_string .= "p." . tep_db_input(tep_db_prepare_input($db_col)) . ", ";
        }
      }
    }

    $products_string = '';
    if( count($products_array) ) {
      $products_string = ' and p2c.products_id in (' . tep_db_input(tep_db_prepare_input(implode(',',$products_array))) . ')';
    }

    //$collection_query_raw = "select distinct p.products_id, p.products_image, p.products_price, p.products_model, p.products_status, p.products_tax_class_id, pd.products_name from " . TABLE_PRODUCTS . " p left join " . TABLE_PRODUCTS_DESCRIPTION . " pd on (p.products_id=pd.products_id)  left join " . TABLE_PRODUCTS_TO_CATEGORIES . " p2c on (p2c.products_id=p.products_id) where " . $in_string . " and p.products_display='1' and pd.language_id = '" . (int)$languages_id . "' order by p.products_status desc, p.products_ordered desc";
    $collection_query_raw = "select p.products_id," . $select_string . "p.products_image, p.products_price, p.products_model, p.products_status, p.products_tax_class_id from " . TABLE_PRODUCTS . " p left join " . TABLE_PRODUCTS_TO_CATEGORIES . " p2c on (p2c.products_id=p.products_id) where " . $in_string . $products_string . " and p.products_display='1' group by p.products_id order by p.products_status desc, p.products_ordered desc, p.products_id desc";
    return $collection_query_raw;
  }
////-MS- products collection added EOM


////
// Stop from parsing any further PHP code
  function tep_exit() {
   tep_session_close();
   exit();
  }

////
// Redirect to another page or site
  function tep_redirect($url) {
    global $logger;

    if ( (strstr($url, "\n") != false) || (strstr($url, "\r") != false) ) {
      tep_redirect(tep_href_link(FILENAME_DEFAULT, '', 'NONSSL', false));
    }

    if (STORE_PAGE_PARSE_TIME == 'true') {
      if (!is_object($logger)) $logger = new logger;
      $logger->timer_stop();
    }

    header('Location: ' . $url);
    tep_exit();
  }

////
// Parse the data used in the html tags to ensure the tags will not break
  function tep_parse_input_field_data($data, $parse) {
    return strtr(trim($data), $parse);
  }

  function tep_output_string($string, $translate = false, $protected = false) {
    if ($protected == true) {
      return htmlspecialchars($string);
    } else {
      if ($translate == false) {
        return tep_parse_input_field_data($string, array('"' => '&quot;'));
      } else {
        return tep_parse_input_field_data($string, $translate);
      }
    }
  }

  function tep_output_string_protected($string) {
    return tep_output_string($string, false, true);
  }

  function tep_sanitize_string($string) {
    $patterns = array ('/ +/','/[<>]/');
    $replace = array (' ', '_');
    return preg_replace($patterns, $replace, trim($string));
  }

  function tep_customers_name($customers_id) {
    $customers = tep_db_query("select customers_firstname, customers_lastname from " . TABLE_CUSTOMERS . " where customers_id = '" . (int)$customers_id . "'");
    $customers_values = tep_db_fetch_array($customers);

    return $customers_values['customers_firstname'] . ' ' . $customers_values['customers_lastname'];
  }

  function tep_get_path($current_category_id = '') {
    global $cPath_array;

    if ($current_category_id == '') {
      $cPath_new = implode('_', $cPath_array);
    } else {
      if (sizeof($cPath_array) == 0) {
        $cPath_new = $current_category_id;
      } else {
        $cPath_new = '';
        $last_category_query = tep_db_query("select parent_id from " . TABLE_CATEGORIES . " where categories_id = '" . (int)$cPath_array[(sizeof($cPath_array)-1)] . "'");
        $last_category = tep_db_fetch_array($last_category_query);

        $current_category_query = tep_db_query("select parent_id from " . TABLE_CATEGORIES . " where categories_id = '" . (int)$current_category_id . "'");
        $current_category = tep_db_fetch_array($current_category_query);

        if ($last_category['parent_id'] == $current_category['parent_id']) {
          for ($i = 0, $n = sizeof($cPath_array) - 1; $i < $n; $i++) {
            $cPath_new .= '_' . $cPath_array[$i];
          }
        } else {
          for ($i = 0, $n = sizeof($cPath_array); $i < $n; $i++) {
            $cPath_new .= '_' . $cPath_array[$i];
          }
        }

        $cPath_new .= '_' . $current_category_id;

        if (substr($cPath_new, 0, 1) == '_') {
          $cPath_new = substr($cPath_new, 1);
        }
      }
    }

    return 'cPath=' . $cPath_new;
  }

  function tep_get_all_get_params($exclude_array = '') {

    if ($exclude_array == '') $exclude_array = array();

    $get_url = '';

    reset($_GET);
    while (list($key, $value) = each($_GET)) {
      if (($key != tep_session_name()) && ($key != 'error') && (!in_array($key, $exclude_array))) $get_url .= $key . '=' . $value . '&';
    }

    return $get_url;
  }

  function tep_date_long($raw_date) {
    if ( ($raw_date == '0000-00-00 00:00:00') || ($raw_date == '') ) return false;

    $year = (int)substr($raw_date, 0, 4);
    $month = (int)substr($raw_date, 5, 2);
    $day = (int)substr($raw_date, 8, 2);
    $hour = (int)substr($raw_date, 11, 2);
    $minute = (int)substr($raw_date, 14, 2);
    $second = (int)substr($raw_date, 17, 2);

    return strftime(DATE_FORMAT_LONG, mktime($hour, $minute, $second, $month, $day, $year));
  }

////
// Output a raw date string in the selected locale date format
// $raw_date needs to be in this format: YYYY-MM-DD HH:MM:SS
// NOTE: Includes a workaround for dates before 01/01/1970 that fail on windows servers
  function tep_date_short($raw_date) {
    if ( ($raw_date == '0000-00-00 00:00:00') || ($raw_date == '') ) return false;

    $year = substr($raw_date, 0, 4);
    $month = (int)substr($raw_date, 5, 2);
    $day = (int)substr($raw_date, 8, 2);
    $hour = (int)substr($raw_date, 11, 2);
    $minute = (int)substr($raw_date, 14, 2);
    $second = (int)substr($raw_date, 17, 2);

    if (@date('Y', mktime($hour, $minute, $second, $month, $day, $year)) == $year) {
      return date(DATE_FORMAT, mktime($hour, $minute, $second, $month, $day, $year));
    } else {
      return preg_replace('/2037$/', $year, date(DATE_FORMAT, mktime($hour, $minute, $second, $month, $day, 2037)));
    }

  }

  function tep_datetime_short($raw_datetime) {
    if ( ($raw_datetime == '0000-00-00 00:00:00') || ($raw_datetime == '') ) return false;

    $year = (int)substr($raw_datetime, 0, 4);
    $month = (int)substr($raw_datetime, 5, 2);
    $day = (int)substr($raw_datetime, 8, 2);
    $hour = (int)substr($raw_datetime, 11, 2);
    $minute = (int)substr($raw_datetime, 14, 2);
    $second = (int)substr($raw_datetime, 17, 2);

    return strftime(DATE_TIME_FORMAT, mktime($hour, $minute, $second, $month, $day, $year));
  }

  function tep_get_category_tree($parent_id = '0', $spacing = '', $exclude = '', $category_tree_array = '', $include_itself = false) {
    global $languages_id;

    if (!is_array($category_tree_array)) $category_tree_array = array();
    if ( (sizeof($category_tree_array) < 1) && ($exclude != '0') ) $category_tree_array[] = array('id' => '0', 'text' => TEXT_TOP);

    if ($include_itself) {
      $category_query = tep_db_query("select cd.categories_name from " . TABLE_CATEGORIES_DESCRIPTION . " cd where cd.language_id = '" . (int)$languages_id . "' and cd.categories_id = '" . (int)$parent_id . "'");
      $category = tep_db_fetch_array($category_query);
      $category_tree_array[] = array('id' => $parent_id, 'text' => $category['categories_name']);
    }

    $categories_query = tep_db_query("select c.categories_id, cd.categories_name, c.parent_id from " . TABLE_CATEGORIES . " c, " . TABLE_CATEGORIES_DESCRIPTION . " cd where c.categories_id = cd.categories_id and cd.language_id = '" . (int)$languages_id . "' and c.parent_id = '" . (int)$parent_id . "' order by c.sort_order, cd.categories_name");
    while ($categories = tep_db_fetch_array($categories_query)) {
      if ($exclude != $categories['categories_id']) $category_tree_array[] = array('id' => $categories['categories_id'], 'text' => $spacing . $categories['categories_name']);
      $category_tree_array = tep_get_category_tree($categories['categories_id'], $spacing . '&nbsp;&nbsp;&nbsp;', $exclude, $category_tree_array);
    }

    return $category_tree_array;
  }

  function tep_draw_products_pull_down($name, $parameters = '', $exclude = '') {
    global $currencies, $languages_id;

    if ($exclude == '') {
      $exclude = array();
    }

    $select_string = '<select name="' . $name . '"';

    if ($parameters) {
      $select_string .= ' ' . $parameters;
    }

    $select_string .= '>';

    $products_query = tep_db_query("select p.products_id, pd.products_name, p.products_price from " . TABLE_PRODUCTS . " p, " . TABLE_PRODUCTS_DESCRIPTION . " pd where p.products_id = pd.products_id and pd.language_id = '" . (int)$languages_id . "' order by products_name");
    while ($products = tep_db_fetch_array($products_query)) {
      if (!in_array($products['products_id'], $exclude)) {
        $select_string .= '<option value="' . $products['products_id'] . '">' . $products['products_name'] . ' (' . $currencies->format($products['products_price']) . ')</option>';
      }
    }

    $select_string .= '</select>';

    return $select_string;
  }

  function tep_options_name($options_id) {
    global $languages_id;

    $options = tep_db_query("select products_options_name from " . TABLE_PRODUCTS_OPTIONS . " where products_options_id = '" . (int)$options_id . "' and language_id = '" . (int)$languages_id . "'");
    $options_values = tep_db_fetch_array($options);

    return $options_values['products_options_name'];
  }

  function tep_values_name($values_id) {
    global $languages_id;

    $values = tep_db_query("select products_options_values_name from " . TABLE_PRODUCTS_OPTIONS_VALUES . " where products_options_values_id = '" . (int)$values_id . "' and language_id = '" . (int)$languages_id . "'");
    $values_values = tep_db_fetch_array($values);

    return $values_values['products_options_values_name'];
  }

  function tep_info_image($image, $alt, $width = '', $height = '') {
    if (tep_not_null($image) && (file_exists(DIR_FS_CATALOG_IMAGES . $image)) ) {
      $image = tep_image(DIR_WS_CATALOG_IMAGES . $image, $alt, $width, $height);
    } else {
      $image = TEXT_IMAGE_NONEXISTENT;
    }

    return $image;
  }

  function tep_break_string($string, $len, $break_char = '-') {
    $l = 0;
    $output = '';
    for ($i=0, $n=strlen($string); $i<$n; $i++) {
      $char = substr($string, $i, 1);
      if ($char != ' ') {
        $l++;
      } else {
        $l = 0;
      }
      if ($l > $len) {
        $l = 1;
        $output .= $break_char;
      }
      $output .= $char;
    }

    return $output;
  }

  function tep_get_country_name($country_id) {
    $country_query = tep_db_query("select countries_name from " . TABLE_COUNTRIES . " where countries_id = '" . (int)$country_id . "'");

    if (!tep_db_num_rows($country_query)) {
      return $country_id;
    } else {
      $country = tep_db_fetch_array($country_query);
      return $country['countries_name'];
    }
  }

  function tep_get_zone_name($country_id, $zone_id, $default_zone) {
    $zone_query = tep_db_query("select zone_name from " . TABLE_ZONES . " where zone_country_id = '" . (int)$country_id . "' and zone_id = '" . (int)$zone_id . "'");
    if (tep_db_num_rows($zone_query)) {
      $zone = tep_db_fetch_array($zone_query);
      return $zone['zone_name'];
    } else {
      return $default_zone;
    }
  }

  function tep_not_null($value) {
    if (is_array($value)) {
      if (sizeof($value) > 0) {
        return true;
      } else {
        return false;
      }
    } else {
      if ( (is_string($value) || is_int($value)) && ($value != '') && ($value != 'NULL') && (strlen(trim($value)) > 0)) {
        return true;
      } else {
        return false;
      }
    }
  }

  function tep_browser_detect($component) {
    if( isset($_SERVER['HTTP_USER_AGENT']) ) {
      return stristr($_SERVER['HTTP_USER_AGENT'], $component);
    } else {
      return false;
    } 
  }

  function tep_tax_classes_pull_down($parameters, $selected = '') {
    $select_string = '<select ' . $parameters . '>';
    $classes_query = tep_db_query("select tax_class_id, tax_class_title from " . TABLE_TAX_CLASS . " order by tax_class_title");
    while ($classes = tep_db_fetch_array($classes_query)) {
      $select_string .= '<option value="' . $classes['tax_class_id'] . '"';
      if ($selected == $classes['tax_class_id']) $select_string .= ' SELECTED';
      $select_string .= '>' . $classes['tax_class_title'] . '</option>';
    }
    $select_string .= '</select>';

    return $select_string;
  }

  function tep_geo_zones_pull_down($parameters, $selected = '') {
    $select_string = '<select ' . $parameters . '>';
    $zones_query = tep_db_query("select geo_zone_id, geo_zone_name from " . TABLE_GEO_ZONES . " order by geo_zone_name");
    while ($zones = tep_db_fetch_array($zones_query)) {
      $select_string .= '<option value="' . $zones['geo_zone_id'] . '"';
      if ($selected == $zones['geo_zone_id']) $select_string .= ' SELECTED';
      $select_string .= '>' . $zones['geo_zone_name'] . '</option>';
    }
    $select_string .= '</select>';

    return $select_string;
  }

  function tep_get_geo_zone_name($geo_zone_id) {
    $zones_query = tep_db_query("select geo_zone_name from " . TABLE_GEO_ZONES . " where geo_zone_id = '" . (int)$geo_zone_id . "'");

    if (!tep_db_num_rows($zones_query)) {
      $geo_zone_name = $geo_zone_id;
    } else {
      $zones = tep_db_fetch_array($zones_query);
      $geo_zone_name = $zones['geo_zone_name'];
    }

    return $geo_zone_name;
  }


  function tep_address_format($address_format_id, $address, $html, $boln, $eoln) {
    $address_format_query = tep_db_query("select address_format as format from " . TABLE_ADDRESS_FORMAT . " where address_format_id = '" . (int)$address_format_id . "'");
    $address_format = tep_db_fetch_array($address_format_query);

    $company = tep_output_string_protected($address['company']);
    if (isset($address['firstname']) && tep_not_null($address['firstname'])) {
      $firstname = tep_output_string_protected($address['firstname']);
      $lastname = tep_output_string_protected($address['lastname']);
    } elseif (isset($address['name']) && tep_not_null($address['name'])) {
      $firstname = tep_output_string_protected($address['name']);
      $lastname = '';
    } else {
      $firstname = '';
      $lastname = '';
    }
    $street = tep_output_string_protected($address['street_address']);
    $suburb = tep_output_string_protected($address['suburb']);
    $city = tep_output_string_protected($address['city']);
    $state = tep_output_string_protected($address['state']);
    if (isset($address['country_id']) && tep_not_null($address['country_id'])) {
      $country = tep_get_country_name($address['country_id']);

      if (isset($address['zone_id']) && tep_not_null($address['zone_id'])) {
        $state = tep_get_zone_code($address['country_id'], $address['zone_id'], $state);
      }
    } elseif (isset($address['country']) && tep_not_null($address['country'])) {
      $country = tep_output_string_protected($address['country']);
    } else {
      $country = '';
    }
    $postcode = tep_output_string_protected($address['postcode']);
    $zip = $postcode;

    if ($html) {
// HTML Mode
      $HR = '<hr>';
      $hr = '<hr>';
      if ( ($boln == '') && ($eoln == "\n") ) { // Values not specified, use rational defaults
        $CR = '<br>';
        $cr = '<br>';
        $eoln = $cr;
      } else { // Use values supplied
        $CR = $eoln . $boln;
        $cr = $CR;
      }
    } else {
// Text Mode
      $CR = $eoln;
      $cr = $CR;
      $HR = '----------------------------------------';
      $hr = '----------------------------------------';
    }

    $statecomma = '';
    $streets = $street;
    if ($suburb != '') $streets = $street . $cr . $suburb;
    if ($country == '') $country = tep_output_string_protected($address['country']);
    if ($state != '') $statecomma = $state . ', ';

    $fmt = $address_format['format'];
    eval("\$address = \"$fmt\";");

    if ( (ACCOUNT_COMPANY == 'true') && (tep_not_null($company)) ) {
      $address = $company . $cr . $address;
    }

    return $address;
  }


/*
  function tep_address_format($address_format_id, $address, $html, $boln, $eoln, $ajax, $order_id) {
    $address_format_query = tep_db_query("select address_format as format from " . TABLE_ADDRESS_FORMAT . " where address_format_id = '" . (int)$address_format_id . "'");
    $address_format = tep_db_fetch_array($address_format_query);

    $company = (($address['company'] != '') ? (($ajax != '') ? '<a href="javascript: updateOrderField(\'' . $order_id . '\', \'orders\', $ajax . \'company\', \'' . addslashes(tep_output_string_protected($address['company'])) . '\');" class="ajaxLink">' : '') . tep_output_string_protected($address['company']) . (($ajax != '') ? '</a>' : '') : '');
    if (isset($address['firstname']) && tep_not_null($address['firstname'])) {
      $firstname = tep_output_string_protected($address['firstname']);
      $lastname = tep_output_string_protected($address['lastname']);
    } elseif (isset($address['name']) && tep_not_null($address['name'])) {
      $firstname = (($ajax != '') ? '<a href="javascript: updateOrderField(\'' . $order_id . '\', \'orders\', \'' . $ajax . 'name\', \'' . addslashes(tep_output_string_protected($address['name'])) . '\');" class="ajaxLink">' : '') . tep_output_string_protected($address['name']) . (($ajax != '') ? '</a>' : '');
      $lastname = '';
    } else {
      $firstname = '';
      $lastname = '';
    }
    $street = (($ajax != '') ? '<a href="javascript: updateOrderField(\'' . $order_id . '\', \'orders\', \'' . $ajax . 'street_address\', \'' . addslashes(tep_output_string_protected($address['street_address'])) . '\');" class="ajaxLink">' : '') . tep_output_string_protected($address['street_address']) . (($ajax != '') ? '</a>' : '');
    (($address['suburb'] != '') ? $suburb = (($ajax != '') ? '<a href="javascript: updateOrderField(\'' . $order_id . '\', \'orders\', \'' . $ajax . 'suburb\', \'' . addslashes(tep_output_string_protected($address['suburb'])) . '\');" class="ajaxLink">' : '') . tep_output_string_protected($address['suburb']) . (($ajax != '') ? '</a>' : '') : '');
    $city = (($ajax != '') ? '<a href="javascript: updateOrderField(\'' . $order_id . '\', \'orders\', \'' . $ajax . 'city\', \'' . addslashes(tep_output_string_protected($address['city'])) . '\');" class="ajaxLink">' : '') . tep_output_string_protected($address['city']) . (($ajax != '') ? '</a>' : '');
    $state = (($ajax != '') ? '<a href="javascript: updateOrderField(\'' . $order_id . '\', \'orders\', \'' . $ajax . 'state\', \'' . addslashes(tep_output_string_protected($address['state'])) . '\');" class="ajaxLink">' : '') . tep_output_string_protected($address['state']) . (($ajax != '') ? '</a>' : '');
    if (isset($address['country_id']) && tep_not_null($address['country_id'])) {
      $country = tep_get_country_name($address['country_id']);

      if (isset($address['zone_id']) && tep_not_null($address['zone_id'])) {
        $state = tep_get_zone_code($address['country_id'], $address['zone_id'], $state);
      }
    } elseif (isset($address['country']) && tep_not_null($address['country'])) {
      $country = (($ajax != '') ? '<a href="javascript: updateOrderField(\'' . $order_id . '\', \'orders\', \'' . $ajax . 'country\', \'' . addslashes(tep_output_string_protected($address['country'])) . '\');" class="ajaxLink">' : '') . tep_output_string_protected($address['country']) . (($ajax != '') ? '</a>' : '');
    } else {
      $country = '';
    }
    $postcode = (($ajax != '') ? '<a href="javascript: updateOrderField(\'' . $order_id . '\', \'orders\', \'' . $ajax . 'postcode\', \'' . addslashes(tep_output_string_protected($address['postcode'])) . '\');" class="ajaxLink">' : '') . tep_output_string_protected($address['postcode']) . (($ajax != '') ? '</a>' : '');

    $zip = $postcode;

    if ($html) {
// HTML Mode
      $HR = '<hr>';
      $hr = '<hr>';
      if ( ($boln == '') && ($eoln == "\n") ) { // Values not specified, use rational defaults
        $CR = '<br>';
        $cr = '<br>';
        $eoln = $cr;
      } else { // Use values supplied
        $CR = $eoln . $boln;
        $cr = $CR;
      }
    } else {
// Text Mode
      $CR = $eoln;
      $cr = $CR;
      $HR = '----------------------------------------';
      $hr = '----------------------------------------';
    }

    $statecomma = '';
    $streets = $street;
    if ($suburb != '') $streets = $street . $cr . $suburb;
    if ($country == '') $country = tep_output_string_protected($address['country']);
    if ($state != '') $statecomma = $state . ', ';

    $fmt = $address_format['format'];
    eval("\$address = \"$fmt\";");

    if ( (ACCOUNT_COMPANY == 'true') && (tep_not_null($company)) ) {
      $address = $company . $cr . $address;
    }

    return $address;
  }
*/

  ////////////////////////////////////////////////////////////////////////////////////////////////
  //
  // Function    : tep_get_zone_code
  //
  // Arguments   : country           country code string
  //               zone              state/province zone_id
  //               def_state         default string if zone==0
  //
  // Return      : state_prov_code   state/province code
  //
  // Description : Function to retrieve the state/province code (as in FL for Florida etc)
  //
  ////////////////////////////////////////////////////////////////////////////////////////////////
  function tep_get_zone_code($country, $zone, $def_state) {

    $state_prov_query = tep_db_query("select zone_code from " . TABLE_ZONES . " where zone_country_id = '" . (int)$country . "' and zone_id = '" . (int)$zone . "'");

    if (!tep_db_num_rows($state_prov_query)) {
      $state_prov_code = $def_state;
    }
    else {
      $state_prov_values = tep_db_fetch_array($state_prov_query);
      $state_prov_code = $state_prov_values['zone_code'];
    }
    
    return $state_prov_code;
  }

  function tep_get_uprid($prid, $params) {
    $uprid = $prid;
    if ( (is_array($params)) && (!strstr($prid, '{')) ) {
      while (list($option, $value) = each($params)) {
        $uprid = $uprid . '{' . $option . '}' . $value;
      }
    }

    return $uprid;
  }

  function tep_get_prid($uprid) {
    $pieces = explode('{', $uprid);

    return $pieces[0];
  }

  function tep_get_languages() {
    $languages_query = tep_db_query("select languages_id, name, code, image, directory from " . TABLE_LANGUAGES . " order by sort_order");
    while ($languages = tep_db_fetch_array($languages_query)) {
      $languages_array[] = array('id' => $languages['languages_id'],
                                 'name' => $languages['name'],
                                 'code' => $languages['code'],
                                 'image' => $languages['image'],
                                 'directory' => $languages['directory']);
    }

    return $languages_array;
  }


////
// Recursively go through the categories and retreive all parent categories IDs
// TABLES: categories
  function tep_get_parent_categories(&$categories, $categories_id) {
    $parent_categories_query = tep_db_query("select parent_id from " . TABLE_CATEGORIES . " where categories_id = '" . (int)$categories_id . "'");
    while ($parent_categories = tep_db_fetch_array($parent_categories_query)) {
      if ($parent_categories['parent_id'] == 0) return true;
      $categories[sizeof($categories)] = $parent_categories['parent_id'];
      if ($parent_categories['parent_id'] != $categories_id) {
        tep_get_parent_categories($categories, $parent_categories['parent_id']);
      }
    }
  }

////
// Construct a category path to the product
// TABLES: products_to_categories
  function tep_get_product_path($products_id) {
    $cPath = '';

    $category_query = tep_db_query("select p2c.categories_id from " . TABLE_PRODUCTS . " p, " . TABLE_PRODUCTS_TO_CATEGORIES . " p2c where p.products_id = '" . (int)$products_id . "' and p.products_status = '1' and p.products_id = p2c.products_id limit 1");
    if (tep_db_num_rows($category_query)) {
      $category = tep_db_fetch_array($category_query);

      $categories = array();
      tep_get_parent_categories($categories, $category['categories_id']);

      $categories = array_reverse($categories);

      $cPath = implode('_', $categories);

      if (tep_not_null($cPath)) $cPath .= '_';
      $cPath .= $category['categories_id'];
    }

    return $cPath;
  }

  function tep_get_category_path($categories_id, $parent_flag=false) {
    $categories = array();
    $categories[] = $categories_id;
    tep_get_parent_categories($categories, $categories_id);
    $categories = array_reverse($categories);
    if( $parent_flag && count($categories) ) {
      unset($categories[count($categories)-1]);
    }
    $path = implode('_', $categories);
    if (substr($path, 0, 1) == '_') {
      $path = substr($path, 1);
    }
    return $path;
  }

  function tep_get_category_name($category_id, $language_id) {
    $category_query = tep_db_query("select categories_name from " . TABLE_CATEGORIES_DESCRIPTION . " where categories_id = '" . (int)$category_id . "' and language_id = '" . (int)$language_id . "'");
    $category = tep_db_fetch_array($category_query);

    return $category['categories_name'];
  }

  function tep_get_category_description($category_id, $language_id) {
    $category_query = tep_db_query("select categories_description from " . TABLE_CATEGORIES_DESCRIPTION . " where categories_id = '" . (int)$category_id . "' and language_id = '" . (int)$language_id . "'");
    $category = tep_db_fetch_array($category_query);

    return $category['categories_description'];
  }

  function tep_get_orders_status_name($orders_status_id, $language_id = '') {
    global $languages_id;

    if (!$language_id) $language_id = $languages_id;
    $orders_status_query = tep_db_query("select orders_status_name from " . TABLE_ORDERS_STATUS . " where orders_status_id = '" . (int)$orders_status_id . "' and language_id = '" . (int)$language_id . "'");
    $orders_status = tep_db_fetch_array($orders_status_query);

    return $orders_status['orders_status_name'];
  }

  function tep_get_orders_status() {
    global $languages_id;

    $orders_status_array = array();
    $orders_status_query = tep_db_query("select orders_status_id, orders_status_name from " . TABLE_ORDERS_STATUS . " where language_id = '" . (int)$languages_id . "' order by orders_status_id");
    while ($orders_status = tep_db_fetch_array($orders_status_query)) {
      $orders_status_array[] = array('id' => $orders_status['orders_status_id'],
                                     'text' => $orders_status['orders_status_name']);
    }

    return $orders_status_array;
  }

  function tep_get_products_name($product_id, $language_id = 0) {
    global $languages_id;

    if ($language_id == 0) $language_id = $languages_id;

    $product_query = tep_db_query("select products_name from " . TABLE_PRODUCTS_DESCRIPTION . " where products_id = '" . (int)$product_id . "' and language_id = '" . (int)$language_id . "'");
    $product = tep_db_fetch_array($product_query);
    return $product['products_name'];
  }

  function tep_get_products_description($product_id, $language_id) {
    $product_query = tep_db_query("select products_description from " . TABLE_PRODUCTS_DESCRIPTION . " where products_id = '" . (int)$product_id . "' and language_id = '" . (int)$language_id . "'");
    $product = tep_db_fetch_array($product_query);

    return $product['products_description'];
  }

  function tep_get_products_url($product_id, $language_id) {
    $product_query = tep_db_query("select products_url from " . TABLE_PRODUCTS_DESCRIPTION . " where products_id = '" . (int)$product_id . "' and language_id = '" . (int)$language_id . "'");
    $product = tep_db_fetch_array($product_query);

    return $product['products_url'];
  }

////
// Return the manufacturers URL in the needed language
// TABLES: manufacturers_info
  function tep_get_manufacturer_url($manufacturer_id, $language_id) {
    $manufacturer_query = tep_db_query("select manufacturers_url from " . TABLE_MANUFACTURERS_INFO . " where manufacturers_id = '" . (int)$manufacturer_id . "' and languages_id = '" . (int)$language_id . "'");
    $manufacturer = tep_db_fetch_array($manufacturer_query);

    return $manufacturer['manufacturers_url'];
  }

////
// Wrapper for class_exists() function
// This function is not available in all PHP versions so we test it before using it.
  function tep_class_exists($class_name) {
    if (function_exists('class_exists')) {
      return class_exists($class_name);
    } else {
      return true;
    }
  }

////
// Count how many products exist in a category
// TABLES: products, products_to_categories, categories
  function tep_products_in_category_count($categories_id, $include_deactivated = false) {
    $products_count = 0;

    if ($include_deactivated) {
      $products_query = tep_db_query("select count(*) as total from " . TABLE_PRODUCTS . " p, " . TABLE_PRODUCTS_TO_CATEGORIES . " p2c where p.products_id = p2c.products_id and p2c.categories_id = '" . (int)$categories_id . "'");
    } else {
      $products_query = tep_db_query("select count(*) as total from " . TABLE_PRODUCTS . " p, " . TABLE_PRODUCTS_TO_CATEGORIES . " p2c where p.products_id = p2c.products_id and p.products_status = '1' and p2c.categories_id = '" . (int)$categories_id . "'");
    }

    $products = tep_db_fetch_array($products_query);

    $products_count += $products['total'];

    $childs_query = tep_db_query("select categories_id from " . TABLE_CATEGORIES . " where parent_id = '" . (int)$categories_id . "'");
    if (tep_db_num_rows($childs_query)) {
      while ($childs = tep_db_fetch_array($childs_query)) {
        $products_count += tep_products_in_category_count($childs['categories_id'], $include_deactivated);
      }
    }

    return $products_count;
  }

////
// Count how many subcategories exist in a category
// TABLES: categories
  function tep_childs_in_category_count($categories_id) {
    $categories_count = 0;

    $categories_query = tep_db_query("select categories_id from " . TABLE_CATEGORIES . " where parent_id = '" . (int)$categories_id . "'");
    while ($categories = tep_db_fetch_array($categories_query)) {
      $categories_count++;
      $categories_count += tep_childs_in_category_count($categories['categories_id']);
    }

    return $categories_count;
  }

////
// Returns an array with countries
// TABLES: countries
  function tep_get_countries($default = '') {
    $countries_array = array();
    if ($default) {
      $countries_array[] = array('id' => '',
                                 'text' => $default);
    }
    $countries_query = tep_db_query("select countries_id, countries_name from " . TABLE_COUNTRIES . " order by countries_name");
    while ($countries = tep_db_fetch_array($countries_query)) {
      $countries_array[] = array('id' => $countries['countries_id'],
                                 'text' => $countries['countries_name']);
    }

    return $countries_array;
  }

////
// return an array with country zones
  function tep_get_country_zones($country_id) {
    $zones_array = array();
    $zones_query = tep_db_query("select zone_id, zone_name from " . TABLE_ZONES . " where zone_country_id = '" . (int)$country_id . "' order by zone_name");
    while ($zones = tep_db_fetch_array($zones_query)) {
      $zones_array[] = array('id' => $zones['zone_id'],
                             'text' => $zones['zone_name']);
    }

    return $zones_array;
  }

  function tep_prepare_country_zones_pull_down($country_id = '') {
// preset the width of the drop-down for Netscape
    $pre = '';
    if ( (!tep_browser_detect('MSIE')) && (tep_browser_detect('Mozilla/4')) ) {
      for ($i=0; $i<45; $i++) $pre .= '&nbsp;';
    }

    $zones = tep_get_country_zones($country_id);

    if (sizeof($zones) > 0) {
      $zones_select = array(array('id' => '', 'text' => PLEASE_SELECT));
      $zones = array_merge($zones_select, $zones);
    } else {
      $zones = array(array('id' => '', 'text' => TYPE_BELOW));
// create dummy options for Netscape to preset the height of the drop-down
      if ( (!tep_browser_detect('MSIE')) && (tep_browser_detect('Mozilla/4')) ) {
        for ($i=0; $i<9; $i++) {
          $zones[] = array('id' => '', 'text' => $pre);
        }
      }
    }

    return $zones;
  }

////
// Get list of address_format_id's
  function tep_get_address_formats() {
    $address_format_query = tep_db_query("select address_format_id from " . TABLE_ADDRESS_FORMAT . " order by address_format_id");
    $address_format_array = array();
    while ($address_format_values = tep_db_fetch_array($address_format_query)) {
      $address_format_array[] = array('id' => $address_format_values['address_format_id'],
                                      'text' => $address_format_values['address_format_id']);
    }
    return $address_format_array;
  }

////
// Alias function for Store configuration values in the Administration Tool
  function tep_cfg_pull_down_country_list($country_id) {
    return tep_draw_pull_down_menu('configuration_value', tep_get_countries(), $country_id);
  }

  function tep_cfg_pull_down_zone_list($zone_id) {
    return tep_draw_pull_down_menu('configuration_value', tep_get_country_zones(STORE_COUNTRY), $zone_id);
  }

  function tep_cfg_pull_down_tax_classes($tax_class_id, $key = '') {
    $name = (($key) ? 'configuration[' . $key . ']' : 'configuration_value');

    $tax_class_array = array(array('id' => '0', 'text' => TEXT_NONE));
    $tax_class_query = tep_db_query("select tax_class_id, tax_class_title from " . TABLE_TAX_CLASS . " order by tax_class_title");
    while ($tax_class = tep_db_fetch_array($tax_class_query)) {
      $tax_class_array[] = array('id' => $tax_class['tax_class_id'],
                                 'text' => $tax_class['tax_class_title']);
    }

    return tep_draw_pull_down_menu($name, $tax_class_array, $tax_class_id);
  }

////
// Function to read in text area in admin
 function tep_cfg_textarea($text) {
    return tep_draw_textarea_field('configuration_value', false, 35, 5, $text);
  }

  function tep_cfg_get_zone_name($zone_id) {
    $zone_query = tep_db_query("select zone_name from " . TABLE_ZONES . " where zone_id = '" . (int)$zone_id . "'");

    if (!tep_db_num_rows($zone_query)) {
      return $zone_id;
    } else {
      $zone = tep_db_fetch_array($zone_query);
      return $zone['zone_name'];
    }
  }

////
// Sets the status of a banner
  function tep_set_banner_status($banners_id, $status) {
    if ($status == '1') {
      return tep_db_query("update " . TABLE_BANNERS . " set status = '1', expires_impressions = NULL, expires_date = NULL, date_status_change = NULL where banners_id = '" . $banners_id . "'");
    } elseif ($status == '0') {
      return tep_db_query("update " . TABLE_BANNERS . " set status = '0', date_status_change = now() where banners_id = '" . $banners_id . "'");
    } else {
      return -1;
    }
  }

////
// Sets the status of a product
  function tep_set_product_status($products_id, $status) {
    if ($status == '1') {
      return tep_db_query("update " . TABLE_PRODUCTS . " set products_status = '1', products_last_modified = now() where products_id = '" . (int)$products_id . "'");
    } elseif ($status == '0') {
      return tep_db_query("update " . TABLE_PRODUCTS . " set products_status = '0', products_last_modified = now() where products_id = '" . (int)$products_id . "'");
    } else {
      return -1;
    }
  }

////
// Sets the status of a product on special
  function tep_set_specials_status($specials_id, $status) {
    if ($status == '1') {
      return tep_db_query("update " . TABLE_SPECIALS . " set status = '1', expires_date = NULL, date_status_change = NULL where specials_id = '" . (int)$specials_id . "'");
    } elseif ($status == '0') {
      return tep_db_query("update " . TABLE_SPECIALS . " set status = '0', date_status_change = now() where specials_id = '" . (int)$specials_id . "'");
    } else {
      return -1;
    }
  }

//-MS- Set products display
////
// Sets the status of a product
  function tep_set_product_display($products_id, $status) {
    if ($status == '1') {
      return tep_db_query("update " . TABLE_PRODUCTS . " set products_display = '1', products_last_modified = now() where products_id = '" . (int)$products_id . "'");
    } elseif ($status == '0') {
      return tep_db_query("update " . TABLE_PRODUCTS . " set products_display = '0', products_last_modified = now() where products_id = '" . (int)$products_id . "'");
    } else {
      return -1;
    }
  }
//-MS- Set products display EOM

////
// Sets timeout for the current script.
// Cant be used in safe mode.
  function tep_set_time_limit($limit) {
    if (!get_cfg_var('safe_mode')) {
      set_time_limit($limit);
    }
  }

////
// Alias function for Store configuration values in the Administration Tool
  function tep_cfg_select_option($select_array, $key_value, $key = '') {
    $string = '';

    for ($i=0, $n=sizeof($select_array); $i<$n; $i++) {
      $name = ((tep_not_null($key)) ? 'configuration[' . $key . ']' : 'configuration_value');

      $string .= '<br><input type="radio" name="' . $name . '" value="' . $select_array[$i] . '"';

      if ($key_value == $select_array[$i]) $string .= ' CHECKED';

      $string .= '> ' . $select_array[$i];
    }

    return $string;
  }

////
// Alias function for module configuration keys
  function tep_mod_select_option($select_array, $key_name, $key_value) {
    reset($select_array);
    while (list($key, $value) = each($select_array)) {
      if (is_int($key)) $key = $value;
      $string .= '<br><input type="radio" name="configuration[' . $key_name . ']" value="' . $key . '"';
      if ($key_value == $key) $string .= ' CHECKED';
      $string .= '> ' . $value;
    }

    return $string;
  }

////
// Retreive server information
  function tep_get_system_information() {

    $db_query = tep_db_query("select now() as datetime");
    $db = tep_db_fetch_array($db_query);
    //$system=$host='';
    list($system, $host, $kernel) = preg_split('/[\s,]+/', exec('uname -a'), 5);

    return array('date' => tep_datetime_short(date('Y-m-d H:i:s')),
                 'system' => $system,
                 'kernel' => $kernel,
                 'host' => $host,
                 'ip' => gethostbyname($host),
                 'uptime' => @exec('uptime'),
                 'http_server' => $_SERVER['SERVER_SOFTWARE'],
                 'php' => PHP_VERSION,
                 'zend' => (function_exists('zend_version') ? zend_version() : ''),
                 'db_server' => DB_SERVER,
                 'db_ip' => gethostbyname(DB_SERVER),
                 'db_version' => 'MySQL ' . (function_exists('mysql_get_server_info') ? mysql_get_server_info() : ''),
                 'db_date' => tep_datetime_short($db['datetime']));
  }

  function tep_generate_category_path($id, $from = 'category', $categories_array = '', $index = 0) {
    global $languages_id;

    if (!is_array($categories_array)) $categories_array = array();

    if ($from == 'product') {
      $categories_query = tep_db_query("select categories_id from " . TABLE_PRODUCTS_TO_CATEGORIES . " where products_id = '" . (int)$id . "'");
      while ($categories = tep_db_fetch_array($categories_query)) {
        if ($categories['categories_id'] == '0') {
          $categories_array[$index][] = array('id' => '0', 'text' => TEXT_TOP);
        } else {
          $category_query = tep_db_query("select cd.categories_name, c.parent_id from " . TABLE_CATEGORIES . " c, " . TABLE_CATEGORIES_DESCRIPTION . " cd where c.categories_id = '" . (int)$categories['categories_id'] . "' and c.categories_id = cd.categories_id and cd.language_id = '" . (int)$languages_id . "'");
          $category = tep_db_fetch_array($category_query);
          $categories_array[$index][] = array('id' => $categories['categories_id'], 'text' => $category['categories_name']);
          if ( (tep_not_null($category['parent_id'])) && ($category['parent_id'] != '0') ) $categories_array = tep_generate_category_path($category['parent_id'], 'category', $categories_array, $index);
          $categories_array[$index] = array_reverse($categories_array[$index]);
        }
        $index++;
      }
    } elseif ($from == 'category') {
      $category_query = tep_db_query("select cd.categories_name, c.parent_id from " . TABLE_CATEGORIES . " c, " . TABLE_CATEGORIES_DESCRIPTION . " cd where c.categories_id = '" . (int)$id . "' and c.categories_id = cd.categories_id and cd.language_id = '" . (int)$languages_id . "'");
      $category = tep_db_fetch_array($category_query);
      $categories_array[$index][] = array('id' => $id, 'text' => $category['categories_name']);
      if ( (tep_not_null($category['parent_id'])) && ($category['parent_id'] != '0') ) $categories_array = tep_generate_category_path($category['parent_id'], 'category', $categories_array, $index);
    }

    return $categories_array;
  }

  function tep_output_generated_category_path($id, $from = 'category') {
    $calculated_category_path_string = '';
    $calculated_category_path = tep_generate_category_path($id, $from);
    for ($i=0, $n=sizeof($calculated_category_path); $i<$n; $i++) {
      for ($j=0, $k=sizeof($calculated_category_path[$i]); $j<$k; $j++) {
        $calculated_category_path_string .= $calculated_category_path[$i][$j]['text'] . '&nbsp;&gt;&nbsp;';
      }
      $calculated_category_path_string = substr($calculated_category_path_string, 0, -16) . '<br>';
    }
    $calculated_category_path_string = substr($calculated_category_path_string, 0, -4);

    if (strlen($calculated_category_path_string) < 1) $calculated_category_path_string = TEXT_TOP;

    return $calculated_category_path_string;
  }

  function tep_get_generated_category_path_ids($id, $from = 'category') {
    $calculated_category_path_string = '';
    $calculated_category_path = tep_generate_category_path($id, $from);
    for ($i=0, $n=sizeof($calculated_category_path); $i<$n; $i++) {
      for ($j=0, $k=sizeof($calculated_category_path[$i]); $j<$k; $j++) {
        $calculated_category_path_string .= $calculated_category_path[$i][$j]['id'] . '_';
      }
      $calculated_category_path_string = substr($calculated_category_path_string, 0, -1) . '<br>';
    }
    $calculated_category_path_string = substr($calculated_category_path_string, 0, -4);

    if (strlen($calculated_category_path_string) < 1) $calculated_category_path_string = TEXT_TOP;

    return $calculated_category_path_string;
  }

  function tep_remove_category($category_id) {
    $category_image_query = tep_db_query("select categories_image, logo_image from " . TABLE_CATEGORIES . " where categories_id = '" . (int)$category_id . "'");
    $category_image = tep_db_fetch_array($category_image_query);

    $duplicate_image_query = tep_db_query("select count(*) as total from " . TABLE_CATEGORIES . " where categories_image = '" . tep_db_input($category_image['categories_image']) . "'");
    $duplicate_image = tep_db_fetch_array($duplicate_image_query);

    if ($duplicate_image['total'] < 2) {
      if (file_exists(DIR_FS_CATALOG_IMAGES . $category_image['categories_image'])) {
        @unlink(DIR_FS_CATALOG_IMAGES . $category_image['categories_image']);
      }
    }

    $duplicate_image_query = tep_db_query("select count(*) as total from " . TABLE_CATEGORIES . " where logo_image = '" . tep_db_input($category_image['logo_image']) . "'");
    $duplicate_image = tep_db_fetch_array($duplicate_image_query);

    if ($duplicate_image['total'] < 2) {
      if (file_exists(DIR_FS_CATALOG_IMAGES . $category_image['categories_image'])) {
        @unlink(DIR_FS_CATALOG_IMAGES . $category_image['logo_image']);
      }
    }

    tep_db_query("delete from " . TABLE_CATEGORIES . " where categories_id = '" . (int)$category_id . "'");
    tep_db_query("delete from " . TABLE_CATEGORIES_DESCRIPTION . " where categories_id = '" . (int)$category_id . "'");
    tep_db_query("delete from " . TABLE_PRODUCTS_TO_CATEGORIES . " where categories_id = '" . (int)$category_id . "'");
//-MS- Abstract Zones Added
    tep_db_query("delete from " . TABLE_PRODUCTS_ZONES_TO_CATEGORIES . " where categories_id = '" . (int)$category_id . "'");
    //tep_db_query("delete from " . TABLE_ACCESSORIES_ZONES_TO_CATEGORIES . " where categories_id = '" . (int)$category_id . "'");
    //tep_db_query("delete from " . TABLE_CONTROL_ZONES_TO_CATEGORIES . " where categories_id = '" . (int)$category_id . "'");
    //tep_db_query("delete from " . TABLE_VENDORS_PRICE_ZONES_TO_CATEGORIES . " where categories_id = '" . (int)$category_id . "'");
    //tep_db_query("delete from " . TABLE_PRODUCTS_FIELDS_ZONES_TO_CATEGORIES . " where categories_id = '" . (int)$category_id . "'");
    //tep_db_query("delete from " . TABLE_ATTRIBUTES_ZONES_TO_CATEGORIES . " where categories_id = '" . (int)$category_id . "'");
    //tep_db_query("delete from " . TABLE_WEAK_ZONES_TO_CATEGORIES . " where categories_id = '" . (int)$category_id . "'");
    //tep_db_query("delete from " . TABLE_BANNER_ZONES_TO_CATEGORIES . " where categories_id = '" . (int)$category_id . "'");
//-MS- Abstract Zones Added EOM

//-MS- SEO-G Added
    tep_db_query("delete from " . TABLE_SEO_TO_CATEGORIES . " where categories_id = '" . (int)$category_id . "'");
    tep_db_query("delete from " . TABLE_META_CATEGORIES . " where categories_id = '" . (int)$category_id . "'");
//-MS- SEO-G Added EOM
  }

  function tep_remove_product($product_id) {
    $product_image_query = tep_db_query("select products_image from " . TABLE_PRODUCTS . " where products_id = '" . (int)$product_id . "'");
    $product_image = tep_db_fetch_array($product_image_query);

    $duplicate_image_query = tep_db_query("select count(*) as total from " . TABLE_PRODUCTS . " where products_image = '" . tep_db_input($product_image['products_image']) . "'");
    $duplicate_image = tep_db_fetch_array($duplicate_image_query);

    if ($duplicate_image['total'] < 2) {
      if (file_exists(DIR_FS_CATALOG_IMAGES . $product_image['products_image'])) {
        @unlink(DIR_FS_CATALOG_IMAGES . $product_image['products_image']);
      }
    }

    tep_db_query("delete from " . TABLE_SPECIALS . " where products_id = '" . (int)$product_id . "'");
    tep_db_query("delete from " . TABLE_PRODUCTS . " where products_id = '" . (int)$product_id . "'");
    tep_db_query("delete from " . TABLE_PRODUCTS_TO_CATEGORIES . " where products_id = '" . (int)$product_id . "'");
    tep_db_query("delete from " . TABLE_PRODUCTS_DESCRIPTION . " where products_id = '" . (int)$product_id . "'");
    tep_db_query("delete from " . TABLE_PRODUCTS_ATTRIBUTES . " where products_id = '" . (int)$product_id . "'");
    tep_db_query("delete from " . TABLE_CUSTOMERS_BASKET . " where products_id = '" . (int)$product_id . "' or products_id like '" . (int)$product_id . "{%'");
    tep_db_query("delete from " . TABLE_CUSTOMERS_BASKET_ATTRIBUTES . " where products_id = '" . (int)$product_id . "' or products_id like '" . (int)$product_id . "{%'");

    tep_db_query("delete from " . TABLE_AUCTIONS . " where products_id = '" . (int)$product_id . "'");
    tep_db_query("delete from " . TABLE_AUCTIONS_TIER . " where products_id = '" . (int)$product_id . "'");

    $product_reviews_query = tep_db_query("select reviews_id from " . TABLE_REVIEWS . " where products_id = '" . (int)$product_id . "'");
    while ($product_reviews = tep_db_fetch_array($product_reviews_query)) {
      tep_db_query("delete from " . TABLE_REVIEWS_DESCRIPTION . " where reviews_id = '" . (int)$product_reviews['reviews_id'] . "'");
    }
    tep_db_query("delete from " . TABLE_REVIEWS . " where products_id = '" . (int)$product_id . "'");

//-MS Fields Added
    tep_db_query("delete from " . TABLE_PRODUCTS_TO_GROUP_FIELDS . " where products_id = '" . (int)$product_id . "'");
//-MS Fields Added EOM

//-MS- Abstract Zones Added
    tep_db_query("delete from " . TABLE_GTEXT_TO_PRODUCTS . " where products_id = '" . (int)$product_id . "'");
    tep_db_query("delete from " . TABLE_PRODUCTS_ZONES_TO_CATEGORIES . " where products_id = '" . (int)$product_id . "'");
//-MS- Abstract Zones Added EOM

//-MS- Extra Images Added
    tep_db_query("delete from " . TABLE_PRODUCTS_EXTRA_IMAGES . " where products_id = '" . (int)$product_id . "'");
//-MS- Extra Images Added EOM

//-MS- SEO-G Added
    tep_db_query("delete from " . TABLE_SEO_TO_PRODUCTS . " where products_id = '" . (int)$product_id . "'");
    tep_db_query("delete from " . TABLE_META_PRODUCTS . " where products_id = '" . (int)$product_id . "'");
//-MS- SEO-G Added EOM
  }

  function tep_remove_order($order_id, $restock = false) {
    $order_query = tep_db_query("select orders_products_id, products_id, products_quantity from " . TABLE_ORDERS_PRODUCTS . " where orders_id = '" . (int)$order_id . "'");
    while ($order = tep_db_fetch_array($order_query)) {
      if ($restock == 'on') {
        tep_db_query("update " . TABLE_PRODUCTS . " set products_quantity = products_quantity + " . $order['products_quantity'] . ", products_ordered = products_ordered - " . $order['products_quantity'] . " where products_id = '" . (int)$order['products_id'] . "'");
      }
    }
    tep_db_query("delete from " . TABLE_ORDERS . " where orders_id = '" . (int)$order_id . "'");
    tep_db_query("delete from " . TABLE_ORDERS_PRODUCTS . " where orders_id = '" . (int)$order_id . "'");
    tep_db_query("delete from " . TABLE_ORDERS_PRODUCTS_ATTRIBUTES . " where orders_id = '" . (int)$order_id . "'");
    tep_db_query("delete from " . TABLE_ORDERS_PRODUCTS_GROUP_FIELDS . " where orders_id = '" . (int)$order_id . "'");
    tep_db_query("delete from " . TABLE_ORDERS_STATUS_HISTORY . " where orders_id = '" . (int)$order_id . "'");
    tep_db_query("delete from " . TABLE_ORDERS_TOTAL . " where orders_id = '" . (int)$order_id . "'");
  }

  function tep_reset_cache_block($cache_block) {
    return;
  }

  function tep_get_file_permissions($mode) {
// determine type
    if ( ($mode & 0xC000) == 0xC000) { // unix domain socket
      $type = 's';
    } elseif ( ($mode & 0x4000) == 0x4000) { // directory
      $type = 'd';
    } elseif ( ($mode & 0xA000) == 0xA000) { // symbolic link
      $type = 'l';
    } elseif ( ($mode & 0x8000) == 0x8000) { // regular file
      $type = '-';
    } elseif ( ($mode & 0x6000) == 0x6000) { //bBlock special file
      $type = 'b';
    } elseif ( ($mode & 0x2000) == 0x2000) { // character special file
      $type = 'c';
    } elseif ( ($mode & 0x1000) == 0x1000) { // named pipe
      $type = 'p';
    } else { // unknown
      $type = '?';
    }

// determine permissions
    $owner['read']    = ($mode & 00400) ? 'r' : '-';
    $owner['write']   = ($mode & 00200) ? 'w' : '-';
    $owner['execute'] = ($mode & 00100) ? 'x' : '-';
    $group['read']    = ($mode & 00040) ? 'r' : '-';
    $group['write']   = ($mode & 00020) ? 'w' : '-';
    $group['execute'] = ($mode & 00010) ? 'x' : '-';
    $world['read']    = ($mode & 00004) ? 'r' : '-';
    $world['write']   = ($mode & 00002) ? 'w' : '-';
    $world['execute'] = ($mode & 00001) ? 'x' : '-';

// adjust for SUID, SGID and sticky bit
    if ($mode & 0x800 ) $owner['execute'] = ($owner['execute'] == 'x') ? 's' : 'S';
    if ($mode & 0x400 ) $group['execute'] = ($group['execute'] == 'x') ? 's' : 'S';
    if ($mode & 0x200 ) $world['execute'] = ($world['execute'] == 'x') ? 't' : 'T';

    return $type .
           $owner['read'] . $owner['write'] . $owner['execute'] .
           $group['read'] . $group['write'] . $group['execute'] .
           $world['read'] . $world['write'] . $world['execute'];
  }

  function tep_remove($source) {
    global $messageStack, $tep_remove_error;

    if (isset($tep_remove_error)) $tep_remove_error = false;

    if (is_dir($source)) {
      $dir = dir($source);
      while ($file = $dir->read()) {
        if ( ($file != '.') && ($file != '..') ) {
          if (is_writeable($source . '/' . $file)) {
            tep_remove($source . '/' . $file);
          } else {
            $messageStack->add(sprintf(ERROR_FILE_NOT_REMOVEABLE, $source . '/' . $file), 'error');
            $tep_remove_error = true;
          }
        }
      }
      $dir->close();

      if (is_writeable($source)) {
        rmdir($source);
      } else {
        $messageStack->add(sprintf(ERROR_DIRECTORY_NOT_REMOVEABLE, $source), 'error');
        $tep_remove_error = true;
      }
    } else {
      if (is_writeable($source)) {
        unlink($source);
      } else {
        $messageStack->add(sprintf(ERROR_FILE_NOT_REMOVEABLE, $source), 'error');
        $tep_remove_error = true;
      }
    }
  }

////
// Output the tax percentage with optional padded decimals
  function tep_display_tax_value($value, $padding = TAX_DECIMAL_PLACES) {
    if (strpos($value, '.')) {
      $loop = true;
      while ($loop) {
        if (substr($value, -1) == '0') {
          $value = substr($value, 0, -1);
        } else {
          $loop = false;
          if (substr($value, -1) == '.') {
            $value = substr($value, 0, -1);
          }
        }
      }
    }

    if ($padding > 0) {
      if ($decimal_pos = strpos($value, '.')) {
        $decimals = strlen(substr($value, ($decimal_pos+1)));
        for ($i=$decimals; $i<$padding; $i++) {
          $value .= '0';
        }
      } else {
        $value .= '.';
        for ($i=0; $i<$padding; $i++) {
          $value .= '0';
        }
      }
    }

    return $value;
  }

  function tep_mail($to_name, $to_email_address, $email_subject, $email_text, $from_email_name, $from_email_address) {
    if (SEND_EMAILS != 'true') return false;
    // Instantiate a new mail object
    $message = new email(array('X-Mailer: osCommerce'));

    // Build the text version
    $text = strip_tags($email_text);
    if (EMAIL_USE_HTML == 'true') {
      $message->add_html($email_text, $text);
    } else {
      $message->add_text($text);
    }

    // Send message
    $message->build_message();
    $message->send($to_name, $to_email_address, $from_email_name, $from_email_address, $email_subject);
  }

  function tep_get_tax_class_title($tax_class_id) {
    if ($tax_class_id == '0') {
      return TEXT_NONE;
    } else {
      $classes_query = tep_db_query("select tax_class_title from " . TABLE_TAX_CLASS . " where tax_class_id = '" . (int)$tax_class_id . "'");
      $classes = tep_db_fetch_array($classes_query);

      return $classes['tax_class_title'];
    }
  }

  function tep_banner_image_extension() {
    if (function_exists('imagetypes')) {
      if (imagetypes() & IMG_PNG) {
        return 'png';
      } elseif (imagetypes() & IMG_JPG) {
        return 'jpg';
      } elseif (imagetypes() & IMG_GIF) {
        return 'gif';
      }
    } elseif (function_exists('imagecreatefrompng') && function_exists('imagepng')) {
      return 'png';
    } elseif (function_exists('imagecreatefromjpeg') && function_exists('imagejpeg')) {
      return 'jpg';
    } elseif (function_exists('imagecreatefromgif') && function_exists('imagegif')) {
      return 'gif';
    }

    return false;
  }

////
// Wrapper function for round()
  function tep_round($n, $d = 0) {
    $n = $n - 0;
    if ($d === NULL) $d = 2;

    $f = pow(10, $d);
    $n += pow(10, - ($d + 1));
    $n = round($n * $f) / $f;
    $n += pow(10, - ($d + 1));
    $n += '';

    if ( $d == 0 )
      return substr($n, 0, strpos($n, '.'));
    else
      return substr($n, 0, strpos($n, '.') + $d + 1);
  }

////
// Add tax to a products price
  function tep_add_tax($price, $tax) {
    global $currencies;

    if (DISPLAY_PRICE_WITH_TAX == 'true') {
      return tep_round($price, $currencies->currencies[DEFAULT_CURRENCY]['decimal_places']) + tep_calculate_tax($price, $tax);
    } else {
      return tep_round($price, $currencies->currencies[DEFAULT_CURRENCY]['decimal_places']);
    }
  }

// Calculates Tax rounding the result
  function tep_calculate_tax($price, $tax) {
    global $currencies;

    return tep_round($price * $tax / 100, $currencies->currencies[DEFAULT_CURRENCY]['decimal_places']);
  }

////
// Returns the tax rate for a zone / class
// TABLES: tax_rates, zones_to_geo_zones
  function tep_get_tax_rate($class_id, $country_id = -1, $zone_id = -1) {
    global $customer_zone_id, $customer_country_id;

    if ( ($country_id == -1) && ($zone_id == -1) ) {
      if (!tep_session_is_registered('customer_id')) {
        $country_id = STORE_COUNTRY;
        $zone_id = STORE_ZONE;
      } else {
        $country_id = $customer_country_id;
        $zone_id = $customer_zone_id;
      }
    }

    $tax_query = tep_db_query("select SUM(tax_rate) as tax_rate from " . TABLE_TAX_RATES . " tr left join " . TABLE_ZONES_TO_GEO_ZONES . " za ON tr.tax_zone_id = za.geo_zone_id left join " . TABLE_GEO_ZONES . " tz ON tz.geo_zone_id = tr.tax_zone_id WHERE (za.zone_country_id IS NULL OR za.zone_country_id = '0' OR za.zone_country_id = '" . (int)$country_id . "') AND (za.zone_id IS NULL OR za.zone_id = '0' OR za.zone_id = '" . (int)$zone_id . "') AND tr.tax_class_id = '" . (int)$class_id . "' GROUP BY tr.tax_priority");
    if (tep_db_num_rows($tax_query)) {
      $tax_multiplier = 0;
      while ($tax = tep_db_fetch_array($tax_query)) {
        $tax_multiplier += $tax['tax_rate'];
      }
      return $tax_multiplier;
    } else {
      return 0;
    }
  }

////
// Returns the tax rate for a tax class
// TABLES: tax_rates
  function tep_get_tax_rate_value($class_id) {
    $tax_query = tep_db_query("select SUM(tax_rate) as tax_rate from " . TABLE_TAX_RATES . " where tax_class_id = '" . (int)$class_id . "' group by tax_priority");
    if (tep_db_num_rows($tax_query)) {
      $tax_multiplier = 0;
      while ($tax = tep_db_fetch_array($tax_query)) {
        $tax_multiplier += $tax['tax_rate'];
      }
      return $tax_multiplier;
    } else {
      return 0;
    }
  }

  function tep_call_function($function, $parameter, $object = '') {
    if ($object == '') {
      return call_user_func($function, $parameter);
    } elseif (PHP_VERSION < 4) {
      return call_user_method($function, $object, $parameter);
    } else {
      return call_user_func(array($object, $function), $parameter);
    }
  }

  function tep_get_zone_class_title($zone_class_id) {
    if ($zone_class_id == '0') {
      return TEXT_NONE;
    } else {
      $classes_query = tep_db_query("select geo_zone_name from " . TABLE_GEO_ZONES . " where geo_zone_id = '" . (int)$zone_class_id . "'");
      $classes = tep_db_fetch_array($classes_query);

      return $classes['geo_zone_name'];
    }
  }

  function tep_cfg_pull_down_zone_classes($zone_class_id, $key = '') {
    $name = (($key) ? 'configuration[' . $key . ']' : 'configuration_value');

    $zone_class_array = array(array('id' => '0', 'text' => TEXT_NONE));
    $zone_class_query = tep_db_query("select geo_zone_id, geo_zone_name from " . TABLE_GEO_ZONES . " order by geo_zone_name");
    while ($zone_class = tep_db_fetch_array($zone_class_query)) {
      $zone_class_array[] = array('id' => $zone_class['geo_zone_id'],
                                  'text' => $zone_class['geo_zone_name']);
    }

    return tep_draw_pull_down_menu($name, $zone_class_array, $zone_class_id);
  }

  function tep_cfg_pull_down_order_statuses($order_status_id, $key = '') {
    global $languages_id;

    $name = (($key) ? 'configuration[' . $key . ']' : 'configuration_value');

    $statuses_array = array(array('id' => '0', 'text' => TEXT_DEFAULT));
    $statuses_query = tep_db_query("select orders_status_id, orders_status_name from " . TABLE_ORDERS_STATUS . " where language_id = '" . (int)$languages_id . "' order by orders_status_name");
    while ($statuses = tep_db_fetch_array($statuses_query)) {
      $statuses_array[] = array('id' => $statuses['orders_status_id'],
                                'text' => $statuses['orders_status_name']);
    }

    return tep_draw_pull_down_menu($name, $statuses_array, $order_status_id);
  }

  function tep_get_order_status_name($order_status_id, $language_id = '') {
    global $languages_id;

    if ($order_status_id < 1) return TEXT_DEFAULT;

    if (!is_numeric($language_id)) $language_id = $languages_id;

    $status_query = tep_db_query("select orders_status_name from " . TABLE_ORDERS_STATUS . " where orders_status_id = '" . (int)$order_status_id . "' and language_id = '" . (int)$language_id . "'");
    $status = tep_db_fetch_array($status_query);

    return $status['orders_status_name'];
  }

////
// Return a random value
  function tep_rand($min = null, $max = null) {
    static $seeded;

    if (!$seeded) {
      mt_srand((double)microtime()*1000000);
      $seeded = true;
    }

    if (isset($min) && isset($max)) {
      if ($min >= $max) {
        return $min;
      } else {
        return mt_rand($min, $max);
      }
    } else {
      return mt_rand();
    }
  }

// nl2br() prior PHP 4.2.0 did not convert linefeeds on all OSs (it only converted \n)
  function tep_convert_linefeeds($from, $to, $string) {
    if ((PHP_VERSION < "4.0.5") && is_array($from)) {
      return preg_replace('/(' . implode('|', $from) . ')/', $to, $string);
    } else {
      return str_replace($from, $to, $string);
    }
  }

  function tep_string_to_int($string) {
    return (int)$string;
  }

////
// Parse and secure the cPath parameter values
  function tep_parse_category_path($cPath) {
// make sure the category IDs are integers
    $cPath_array = array_map('tep_string_to_int', explode('_', $cPath));

// make sure no duplicate category IDs exist which could lock the server in a loop
    $tmp_array = array();
    $n = sizeof($cPath_array);
    for ($i=0; $i<$n; $i++) {
      if (!in_array($cPath_array[$i], $tmp_array)) {
        $tmp_array[] = $cPath_array[$i];
      }
    }

    return $tmp_array;
  }

//-MS- Generic Text Added
  function tep_set_generic_text_status($gtext_id, $status) {
    if ($status == '1') {
      return tep_db_query("update " . TABLE_GTEXT . " set status = '1' where gtext_id = '" . $gtext_id . "'");
    } elseif ($status == '0') {
      return tep_db_query("update " . TABLE_GTEXT . " set status = '0' where gtext_id = '" . $gtext_id . "'");
    } else {
      return -1;
    }
  }
//-MS- Generic Text Added EOM

//-MS- Abstract Zones support functions added
  function tep_cfg_pull_down_gtext_entries($gtext_id, $key = '') {

    $name = (($key) ? 'configuration[' . $key . ']' : 'configuration_value');

    $gtext_array = array();
    $gtext_query = tep_db_query("select gtext_id, gtext_title from " . TABLE_GTEXT . "");
    while ($gtext = tep_db_fetch_array($gtext_query)) {
      $gtext_array[] = array('id' => $gtext['gtext_id'],
                             'text' => $gtext['gtext_title']);
    }

    return tep_draw_pull_down_menu($name, $gtext_array, $gtext_id);
  }

  function tep_get_gtext_title($gtext_id) {
    $gtext_query = tep_db_query("select gtext_title from " . TABLE_GTEXT . " where gtext_id = '" . (int)$gtext_id . "'");
    $gtext = tep_db_fetch_array($gtext_query);

    return $gtext['gtext_title'];
  }

  function tep_cfg_pull_down_product_zones($abstract_zone_id, $key = '') {

    $name = (($key) ? 'configuration[' . $key . ']' : 'configuration_value');

    $abstract_array = array();
    $abstract_query = tep_db_query("select az.abstract_zone_id, az.abstract_zone_name from " . TABLE_ABSTRACT_ZONES . " az left join " . TABLE_ABSTRACT_TYPES . " abt on (abt.abstract_types_id=az.abstract_types_id) where abstract_types_class='products_zones'");
    while ($abstract = tep_db_fetch_array($abstract_query)) {
      $abstract_array[] = array('id' => $abstract['abstract_zone_id'],
                                'text' => $abstract['abstract_zone_name']);
    }

    return tep_draw_pull_down_menu($name, $abstract_array, $abstract_zone_id);
  }

  function tep_cfg_pull_down_control_zones($abstract_zone_id, $key = '') {

    $name = (($key) ? 'configuration[' . $key . ']' : 'configuration_value');

    $abstract_array = array();
    $abstract_query = tep_db_query("select az.abstract_zone_id, az.abstract_zone_name from " . TABLE_ABSTRACT_ZONES . " az left join " . TABLE_ABSTRACT_TYPES . " abt on (abt.abstract_types_id=az.abstract_types_id) where abstract_types_class='products_control_zones'");
    while ($abstract = tep_db_fetch_array($abstract_query)) {
      $abstract_array[] = array('id' => $abstract['abstract_zone_id'],
                                'text' => $abstract['abstract_zone_name']);
    }

    return tep_draw_pull_down_menu($name, $abstract_array, $abstract_zone_id);
  }

  function tep_cfg_pull_down_categories_zones($abstract_zone_id, $key = '') {

    $name = (($key) ? 'configuration[' . $key . ']' : 'configuration_value');

    $abstract_array = array();
    $abstract_query = tep_db_query("select az.abstract_zone_id, az.abstract_zone_name from " . TABLE_ABSTRACT_ZONES . " az left join " . TABLE_ABSTRACT_TYPES . " abt on (abt.abstract_types_id=az.abstract_types_id) where abstract_types_class='categories_zones'");
    while ($abstract = tep_db_fetch_array($abstract_query)) {
      $abstract_array[] = array('id' => $abstract['abstract_zone_id'],
                                'text' => $abstract['abstract_zone_name']);
    }

    return tep_draw_pull_down_menu($name, $abstract_array, $abstract_zone_id);
  }

  function tep_cfg_pull_down_text_zones($abstract_zone_id, $key = '') {

    $name = (($key) ? 'configuration[' . $key . ']' : 'configuration_value');

    $abstract_array = array();
    $abstract_query = tep_db_query("select az.abstract_zone_id, az.abstract_zone_name from " . TABLE_ABSTRACT_ZONES . " az left join " . TABLE_ABSTRACT_TYPES . " abt on (abt.abstract_types_id=az.abstract_types_id) where abstract_types_class='generic_zones'");
    while ($abstract = tep_db_fetch_array($abstract_query)) {
      $abstract_array[] = array('id' => $abstract['abstract_zone_id'],
                                'text' => $abstract['abstract_zone_name']);
    }

    return tep_draw_pull_down_menu($name, $abstract_array, $abstract_zone_id);
  }

  function tep_get_abstract_zone_name($abstract_zone_id) {
    $abstract_query = tep_db_query("select abstract_zone_name from " . TABLE_ABSTRACT_ZONES . " where abstract_zone_id = '" . (int)$abstract_zone_id . "'");
    $abstract = tep_db_fetch_array($abstract_query);

    return $abstract['abstract_zone_name'];
  }
//-MS- Abstract Zones support functions added EOM

//-MS- Added active countries
////
// Sets the status of a country
  function tep_set_country_status($countries_id, $status) {
    if ($status == '1') {
      return tep_db_query("update " . TABLE_COUNTRIES . " set status_id = '1' where countries_id = '" . (int)$countries_id . "'");
    } elseif ($status == '0') {
      return tep_db_query("update " . TABLE_COUNTRIES . " set status_id = '0' where countries_id = '" . (int)$countries_id . "'");
    } else {
      return -1;
    }
  }

////
// Sets the status of a state
  function tep_set_zone_status($zone_id, $status) {
    if ($status == '1') {
      return tep_db_query("update " . TABLE_ZONES . " set status_id = '1' where zone_id = '" . (int)$zone_id . "'");
    } elseif ($status == '0') {
      return tep_db_query("update " . TABLE_ZONES . " set status_id = '0' where zone_id = '" . (int)$zone_id . "'");
    } else {
      return -1;
    }
  }

////
// Sets the pay status of a country
  function tep_set_country_pay_status($countries_id, $status) {
    if ($status == '1') {
      return tep_db_query("update " . TABLE_COUNTRIES . " set pay_status_id = '1' where countries_id = '" . (int)$countries_id . "'");
    } elseif ($status == '0') {
      return tep_db_query("update " . TABLE_COUNTRIES . " set pay_status_id = '0' where countries_id = '" . (int)$countries_id . "'");
    } else {
      return -1;
    }
  }

////
// Sets the status of a country
  function tep_set_country_ship_status($countries_id, $status) {
    if ($status == '1') {
      return tep_db_query("update " . TABLE_COUNTRIES . " set ship_status_id = '1' where countries_id = '" . (int)$countries_id . "'");
    } elseif ($status == '0') {
      return tep_db_query("update " . TABLE_COUNTRIES . " set ship_status_id = '0' where countries_id = '" . (int)$countries_id . "'");
    } else {
      return -1;
    }
  }

////
// Sets the pay status of a country
  function tep_set_zone_pay_status($zones_id, $status) {
    if ($status == '1') {
      return tep_db_query("update " . TABLE_ZONES . " set pay_status_id = '1' where zone_id = '" . (int)$zones_id . "'");
    } elseif ($status == '0') {
      return tep_db_query("update " . TABLE_ZONES . " set pay_status_id = '0' where zone_id = '" . (int)$zones_id . "'");
    } else {
      return -1;
    }
  }

////
// Sets the status of a country
  function tep_set_zone_ship_status($zones_id, $status) {
    if ($status == '1') {
      return tep_db_query("update " . TABLE_ZONES . " set ship_status_id = '1' where zone_id = '" . (int)$zones_id . "'");
    } elseif ($status == '0') {
      return tep_db_query("update " . TABLE_ZONES . " set ship_status_id = '0' where zone_id = '" . (int)$zones_id . "'");
    } else {
      return -1;
    }
  }

////
// Returns an array of active countries
  function tep_get_active_countries($countries_array = '') {

    if (!is_array($countries_array)) 
      $countries_array = array();
    //PHP 4.x Bug, not init as local var automatically with a global present?
    $countries_query = '';
    $countries_query = tep_db_query("select countries_id, countries_name from " . TABLE_COUNTRIES . " where status_id = '1' order by countries_name");
    while( $countries = tep_db_fetch_array($countries_query) ) {
      $countries_array[] = array('id' => $countries['countries_id'],
                                 'text' => $countries['countries_name']);
    }
    return $countries_array;
  }
//-MS- Added active countries EOM

//-MS- Email Templates Added
  function tep_email_templates_replace_entities($email_contents, $contents_array) {
    if( !is_array($contents_array) || !tep_not_null($email_contents) )
      return false;

    $translation = $email_contents;
    foreach($contents_array as $key => $value) {
      $translation = str_replace('[' . $key . ']', $value, $translation);
    }
    return $translation;
  }

  function tep_get_group($grp = '') {
    $result = tep_db_query("select distinct grp as 'grp' from " . TABLE_EMAIL_TEMPLATES . " order by 1 asc");
    if(tep_db_num_rows($result)){
      $i = 0;
      while ($line = tep_db_fetch_array($result)) {
        $arr[$i]['id'] = $arr[$i]['text'] = stripslashes($line['grp']);
        $i++;
      }
      $out  = tep_draw_pull_down_menu('grp', $arr, $grp);
      $out .= TEXT_EMAIL_TEMPLATE_NEW_GROUP . tep_draw_input_field('grp_new', '', 'size="25"');
    } else {
      $out  = tep_draw_input_field('grp_new', '', 'size="25"') . TEXT_EMAIL_TEMPLATE_NO_GROUP;    
    }
    return $out;
  }

  function tep_email_column($zone) {
    global $currencies, $g_front_server;
    require_once(DIR_WS_CLASSES . 'abstract_front.php');
    require_once(DIR_WS_CLASSES . 'product_front.php');
    $result = '';
    $cBox = new product_front();
    $data = $cBox->get_zone_data($zone);
    if( is_array($data) && count($data) ) {
      $products_array = $cBox->get_random_products($zone);
      if( count($products_array) ) {
        $total_items = array();
        $listing_sql = tep_get_collection(0, '', false, false, $products_array);
        tep_query_to_array($listing_sql, $total_items);
        $result = 
          '<table border="0" width="100%" cellspacing="0" cellpadding="0">' . "\n" .
          '  <tr>' . "\n" . 
          '    <td valign="top"><font size="3" face="verdana,geneva" color="000077"><strong>' . strtoupper($data['abstract_zone_name']) . '</strong></font></td>' . "\n" . 
          '  </tr>' . "\n" . 
          '  <tr>' . "\n" . 
          '    <td width="100%" height="8">&nbsp;</td>' . "\n" . 
          '  </tr>' . "\n" . 
          '  <tr>' . "\n" . 
          '    <td><font size="2" face="verdana,geneva">' . $data['abstract_zone_desc'] . '</font></td>' . "\n" . 
          '  </tr>' . "\n" . 
          '  <tr>' . "\n" . 
          '    <td width="100%" height="8">&nbsp;</td>' . "\n" . 
          '  </tr>' . "\n"; 

        for($i=0, $j=count($total_items); $i<$j; $i++) {
          $total_items[$i]['products_name'] = tep_get_products_name($total_items[$i]['products_id']);
          $total_items[$i]['products_name'] = tep_create_safe_string($total_items[$i]['products_name']);

          if( !isset($total_items[$i]['specials_new_products_price']) ) {
            $total_items[$i]['specials_new_products_price'] = tep_get_products_special_price($total_items[$i]['products_id']);
          }
          if( tep_not_null($total_items[$i]['specials_new_products_price'])) {
            $products_price = '<s>' . $currencies->display_price($total_items[$i]['products_price'], tep_get_tax_rate($total_items[$i]['products_tax_class_id'])) . '</s><br />';
            $products_price .= $currencies->display_price($total_items[$i]['specials_new_products_price'], tep_get_tax_rate($total_items[$i]['products_tax_class_id']));
          } else {
            $products_price = $currencies->display_price($total_items[$i]['products_price'], tep_get_tax_rate($total_items[$i]['products_tax_class_id']));
          }
          $tmp_image = DIR_WS_CATALOG_IMAGES . $total_items[$i]['products_image'];
          if( !tep_not_null($total_items[$i]['products_image']) || !file_exists($tmp_image) || filesize($tmp_image) <= 0 ) {
            $total_items[$i]['products_image'] = $tmp_image;
          }
          $result .= 
          '  <tr>' . "\n" . 
          '    <td align="center"><a href="' . tep_catalog_href_link('product_info.php', 'products_id=' . $total_items[$i]['products_id']) . '">' . tep_image($g_front_server . $total_items[$i]['products_image'], $total_items[$i]['products_name'], SMALL_IMAGE_WIDTH, SMALL_IMAGE_HEIGHT) . '</a></td>' . "\n" . 
          '  </tr>' . "\n" . 
          '  <tr>' . "\n" . 
          '    <td align="center"><a href="' . tep_catalog_href_link('product_info.php', 'products_id=' . $total_items[$i]['products_id']) . '" title="' . $total_items[$i]['products_name'] . '"><font size="2" face="verdana,geneva" color="000077"><strong>' . $total_items[$i]['products_name'] . '</strong></font></a></td>' . "\n" . 
          '  </tr>' . "\n" . 
          '  <tr>' . "\n" . 
          '    <td align="center"><font size="2" face="verdana,geneva" color="CC0000"><strong>' . $products_price . '</strong></font></td>' . "\n" . 
          '  </tr>' . "\n" . 
          '  <tr>' . "\n" . 
          '    <td width="100%" height="8">&nbsp;</td>' . "\n" . 
          '  </tr>' . "\n"; 
        }
        $result .= 
          '</table>' . "\n";
      }
    }
    return $result;
  }

//-MS- Email Templates Added EOM

//-MS- Testimonials Added
// Sets the status of a testimonial
  function tep_set_testimonials_status($testimonials_id, $status) {
    if ($status == '1') {
      return tep_db_query("update " . TABLE_TESTIMONIALS . " set status = '1' where testimonials_id = '" . (int)$testimonials_id . "'");
    } elseif ($status == '0') {
      return tep_db_query("update " . TABLE_TESTIMONIALS . " set status = '0' where testimonials_id = '" . (int)$testimonials_id . "'");
    } else {
      return -1;
    }
  }
//-MS- Testimonials Added EOM

//-MS- Added Customers status
////
// Sets the status of customers
  function tep_set_customers_status($customers_id, $status) {
    if ($status == '1') {
      return tep_db_query("update " . TABLE_CUSTOMERS . " set customers_status = '1' where customers_id = '" . tep_db_input($customers_id) . "'");
    } elseif ($status == '0') {
      return tep_db_query("update " . TABLE_CUSTOMERS . " set customers_status = '0' where customers_id = '" . tep_db_input($customers_id) . "'");
    } else {
      return -1;
    }
  }
//-MS- Added Customers status EOM

//-MS- Added Reviews Approved
////
// Sets the status of a reviews
  function tep_set_reviews_status($reviews_id, $status) {
    if ($status == '1') {
      return tep_db_query("update " . TABLE_REVIEWS . " set approved = '1' where reviews_id = '" . tep_db_input($reviews_id) . "'");
    } elseif ($status == '0') {
      return tep_db_query("update " . TABLE_REVIEWS . " set approved = '0' where reviews_id = '" . tep_db_input($reviews_id) . "'");
    } else {
      return -1;
    }
  }
//-MS- Added Reviews Approved EOM

  function tep_mysql_to_time_stamp($mysqlDate) {
    if (strlen($mysqlDate) > 10) {
      list($year, $month, $day_time) = explode('-', $mysqlDate);
      list($day, $time) = explode(" ", $day_time);
      list($hour, $minute, $second) = explode(":", $time);
      $ts = mktime($hour, $minute, $second, $month, $day, $year);
    } else {
      list($year, $month, $day) = explode('-', $mysqlDate);
      $ts = mktime(0, 0, 0, $month, $day, $year);
    }
    if( !(int)$month || !(int)$day || !(int)$year ) $ts = 0;
    return $ts;
  }

//-MS- Admin begin
////
//Check login and file access
  function tep_admin_check_login() {
    global $PHP_SELF, $login_groups_id;
    if (!tep_session_is_registered('login_id')) {
      tep_redirect(tep_href_link(FILENAME_LOGIN, '', 'SSL'));
    } else {
      $filename = basename( $PHP_SELF );
      if ($filename != FILENAME_DEFAULT && $filename != FILENAME_FORBIDEN && $filename != FILENAME_LOGOFF && $filename != FILENAME_ADMIN_ACCOUNT && $filename != FILENAME_POPUP_IMAGE && $filename != 'packingslip.php' && $filename != 'invoice.php') {
        $db_file_query = tep_db_query("select admin_files_name from " . TABLE_ADMIN_FILES . " where FIND_IN_SET( '" . $login_groups_id . "', admin_groups_id) and admin_files_name = '" . $filename . "'");
        if (!tep_db_num_rows($db_file_query)) {
          tep_redirect(tep_href_link(FILENAME_FORBIDEN));
        }
      }
    }  
  }

////
//Return 'true' or 'false' value to display boxes and files in index.php and column_left.php
  function tep_admin_check_boxes($filename, $boxes='') {
    global $login_groups_id;
    
    $is_boxes = 1;
    if ($boxes == 'sub_boxes') {
      $is_boxes = 0;
    }
    $dbquery = tep_db_query("select admin_files_id from " . TABLE_ADMIN_FILES . " where FIND_IN_SET( '" . $login_groups_id . "', admin_groups_id) and admin_files_is_boxes = '" . $is_boxes . "' and admin_files_name = '" . $filename . "'");
    
    $return_value = false;
    if (tep_db_num_rows($dbquery)) {
      $return_value = true;
    }
    return $return_value;
  }

  ////
  //Return files stored in box that can be accessed by user
  function tep_admin_files_boxes($filename, $sub_box_name) {
    global $login_groups_id;
    $sub_boxes = '';
    
    $dbquery = tep_db_query("select admin_files_name from " . TABLE_ADMIN_FILES . " where FIND_IN_SET( '" . $login_groups_id . "', admin_groups_id) and admin_files_is_boxes = '0' and admin_files_name = '" . $filename . "'");
    if (tep_db_num_rows($dbquery)) {
      $sub_boxes = '<a href="' . tep_href_link($filename) . '" class="menuBoxContentLink">' . $sub_box_name . '</a><br>';
    }
    return $sub_boxes;
  }

  ////
  //Get selected file for index.php
  function tep_selected_file($filename) {
    global $login_groups_id;
    $randomize = FILENAME_ADMIN_ACCOUNT;
    
    $dbquery = tep_db_query("select admin_files_id as boxes_id from " . TABLE_ADMIN_FILES . " where FIND_IN_SET( '" . $login_groups_id . "', admin_groups_id) and admin_files_is_boxes = '1' and admin_files_name = '" . $filename . "'");
    if (tep_db_num_rows($dbquery)) {
      $boxes_id = tep_db_fetch_array($dbquery);
      $randomize_query = tep_db_query("select admin_files_name from " . TABLE_ADMIN_FILES . " where FIND_IN_SET( '" . $login_groups_id . "', admin_groups_id) and admin_files_is_boxes = '0' and admin_files_to_boxes = '" . $boxes_id['boxes_id'] . "'");
      if (tep_db_num_rows($randomize_query)) {
        $file_selected = tep_db_fetch_array($randomize_query);
        $randomize = $file_selected['admin_files_name'];
      }
    }
    return $randomize;
  }
//-MS- Admin end

  function tep_params_to_string($array, $sep='=', $glue='&') {
    $result = '';
    if( !is_array($array) || !count($array) ) return $result;

    $result_array = array();
    foreach($array as $key => $value) {
      if( empty($key) ) continue;
      $result_array[] = $key . $sep . $value;
    }
    $result = implode($glue, $result_array);
    return $result;
  }

//-MS- Array Invert Added
// Function array_invert came from:
// http://www.php.net/manual/en/function.array-flip.php
// By: benles at bldigital dot com
  function tep_array_invert($arr) {
    $res = array();
    foreach(array_keys($arr) as $key) {
      if (!array_key_exists($arr[$key], $res)) {
        $res[$arr[$key]] = array();
      }
      array_push($res[$arr[$key]], $key);
    }
    return $res;
  }
//-MS- Array Invert Added EOM

  function tep_array_invert_element($input_array, $element, $filter=false) {
    $result_array = array();
    foreach($input_array as $key => $value) {
      if( is_array($value) && isset($value[$element]) ) {
        $index = $value[$element];
        if( !isset($result_array[$index]) ) {
          $result_array[$index] = array();
        }
        if( $filter && isset($value[$filter] ) ) {
          $result_array[$index][] = $value[$filter];
        } else {
          $result_array[$index][] = $value;
        }
      }
    }
    return $result_array;
  } 

  function tep_array_invert_flat($input_array, $element, $filter) {
    $result_array = array();
    foreach($input_array as $key => $value) {
      if( is_array($value) && isset($value[$element]) && isset($value[$filter]) ) {
        $index = $value[$element];
        $result_array[$index] = $value[$filter];
      }
    }
    return $result_array;
  }

  function tep_encode_serialize($data_array) {
    if( !is_array($data_array) ) $data_array = array();
    $result = base64_encode(serialize($data_array));
    return $result;
  }

?>
