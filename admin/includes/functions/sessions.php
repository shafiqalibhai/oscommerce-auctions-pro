<?php
/*
  $Id: sessions.php,v 1.9 2003/06/23 01:20:05 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

//----------------------------------------------------------------------------
// Modifications by Asymmetrics
// Copyright (c) 2006-2008 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Admin: Session fixes, for session read, session recreate,
// session destroy, session integrity, cookie check and globals reference. 
// Added configurable session life setting.
// Implemented separate table for admin sessions
// Added session callback handling switch for Manual or PHP driven.
//----------------------------------------------------------------------------
// I-Metrics Layer
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
  function _sess_open($save_path, $session_name) {
    return true;
  }

  function _sess_close() {
    return true;
  }

  function _sess_read($key) {
    $qid = tep_db_query("select value from " . TABLE_SESSIONS_ADMIN . " where sesskey = '" . tep_db_input($key) . "' and expiry > '" . time() . "'");

    $value = tep_db_fetch_array($qid);
    if ($value['value']) {
      if( SESSION_PHP_HANDLING == 'false') {
        $result = unserialize($value['value']);
        return $result;
      } else {
        return $value['value'];
      }
    }
    return ("");
  }

  function _sess_write($key, $val) {
    global $SESS_LIFE;
    $expiry = time() + $SESS_LIFE;
    $value = $val;

    $qid = tep_db_query("select count(*) as total from " . TABLE_SESSIONS_ADMIN . " where sesskey = '" . tep_db_input($key) . "'");
    $total = tep_db_fetch_array($qid);

    if ($total['total'] > 0) {
      return tep_db_query("update " . TABLE_SESSIONS_ADMIN . " set expiry = '" . tep_db_input($expiry) . "', value = '" . tep_db_input($value) . "' where sesskey = '" . tep_db_input($key) . "'");
    } else {
      return tep_db_query("insert into " . TABLE_SESSIONS_ADMIN . " values ('" . tep_db_input($key) . "', '" . tep_db_input($expiry) . "', '" . tep_db_input($value) . "')");
    }
  }

  function _sess_destroy($key) {
    return tep_db_query("delete from " . TABLE_SESSIONS_ADMIN . " where sesskey = '" . tep_db_input($key) . "'");
  }

  function _sess_gc($maxlifetime) {
    tep_db_query("delete from " . TABLE_SESSIONS_ADMIN . " where expiry < '" . time() . "'");

    return true;
  }



  function tep_session_start() {
    global $session_started;

    $success = session_start();

    if( $success ) {
      if( SESSION_PHP_HANDLING == 'false') {
        $name = tep_session_id();
        $_SESSION = _sess_read($name);
        if( !is_array($_SESSION) ) $_SESSION = array();
      }

      foreach($_SESSION as $key => $value ) {
        $GLOBALS[$key] = $value;
        $_SESSION[$key] = &$GLOBALS[$key];
      }
      $session_started = true;
    } else {
      $session_started = false;
    }
    return $success;
  }

  function tep_session_register($variable) {
    global $session_started;
    if( isset($session_started) && $session_started == true ) {
      if(!tep_session_is_registered($variable) ) {
        if( !isset($GLOBALS[$variable]) ) {
          $GLOBALS[$variable] = '';
        }
        $_SESSION[$variable] = &$GLOBALS[$variable];
      }
      return true;
    } else {
      return false;
    } 
  }

  function tep_session_is_registered($variable) {
    global $session_started;
    if( !isset($session_started) || $session_started == false ) {
      return false;
    }
    return isset($_SESSION[$variable]);
  }

  function tep_session_unregister($variable) {
    global $session_started;

    if( !isset($session_started) || $session_started == false ) {
      return false;
    }

    if( isset($_SESSION[$variable]) ) {
      unset($_SESSION[$variable]);
      return true;
    }
    return false;
  }

  function tep_session_id($sessid = '') {
    if ($sessid != '') {
      return session_id($sessid);
    } else {
      return session_id();
    }
  }

  function tep_session_name($name = '') {
    if ($name != '') {
      return session_name($name);
    } else {
      return session_name();
    }
  }

  function tep_session_close() {
    global $session_started;

    if( !isset($session_started) || $session_started == false ) {
      return;
    }

    if( isset($_SESSION) && is_array($_SESSION) ) {
      if( SESSION_PHP_HANDLING == 'false') {
        $serial = serialize($_SESSION);
        _sess_write(tep_session_id(), $serial);
      } else {
        session_write_close();
      }
    }
  }

  function tep_session_destroy($send_headers=true) {
    global $session_started;

    if( !isset($session_started) || $session_started == false ) {
      return false;
    }

    // Unset all of the session variables.
    $_SESSION = array();

    // If its desired to kill the session, also delete the session cookie.
    // Note: This will destroy the session, and not just the session data!
    if( $send_headers && isset($_COOKIE[session_name()])) {
      $session_data = session_get_cookie_params();
      setcookie(tep_session_name(), '', 0, $session_data['path'], $session_data['domain']);
      unset($_COOKIE[tep_session_name()]);
    }

    // Finally, destroy the session.
    $result = session_destroy();
    $session_started = false;
    return $result;
  }

  function tep_session_save_path($path = '') {
    if ($path != '') {
      return session_save_path($path);
    } else {
      return session_save_path();
    }
  }

?>
