<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2007 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Generic Text Zones class for osCommerce Admin
// This is a Bridge for Abstract Zones Class
// Controls relationships among text page, sequence of display, products
// Featuring:
// - Multi-Products instant selection
// - Multi-Text Entries instant selection
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/

  class generic_zones extends abstract_zones {
    var $m_sID, $m_ssID, $m_spage, $m_apage;
// class constructor
    function generic_zones() {
      $this->m_spage = isset($_GET['spage'])?$_GET['spage']:'';
      $this->m_ssID = isset($_GET['ssID'])?$_GET['ssID']:'';
      $this->m_sID = isset($_GET['sID'])?$_GET['sID']:'';
      $this->m_apage = isset($_GET['apage'])?$_GET['apage']:'';
      parent::abstract_zones();
      if( !tep_not_null($this->m_sID) ) {
        $zones_query = tep_db_query("select gtext_id from " . TABLE_GTEXT_TO_DISPLAY . " where abstract_zone_id = '" . (int)$this->m_zID . "'");
        if( $zones_array = tep_db_fetch_array($zones_query) ) {
          $this->m_sID = $zones_array['gtext_id'];
        }
      }
    }

    function get_products_zone_array($zone_id) {
      $products_array = array();
      $generic_zone_query = tep_db_query("select gz2p.categories_id, gz2p.products_id from " . TABLE_ABSTRACT_ZONES . " az left join " . TABLE_GTEXT_TO_PRODUCTS . " gz2p on (gz2p.abstract_zone_id=az.abstract_zone_id) where az.abstract_zone_id = '" . (int)$this->m_zID . "'");

      if( !tep_db_num_rows($generic_zone_query) ) {
        return $products_array;
      }
      while( $generic_zone = tep_db_fetch_array($generic_zone_query) ) {
        if( !$generic_zone['categories_id'] && !$generic_zone['products_id'] ) {
          return $products_array;
        }
        if( !$generic_zone['products_id'] ) {
          $products_query = tep_db_query("select products_id from " . TABLE_PRODUCTS_TO_CATEGORIES . " where categories_id = '" . (int)$generic_zone['categories_id'] . "'");
          while( $product = tep_db_fetch_array($products_query) ) {
            $products_array[$product['products_id']] = $generic_zone;
          }
        } else {
          $products_array[$generic_zone['products_id']] = $generic_zone;
        }
      }
      return $products_array;
    }


    function get_products_string($gtext_id, $format='<br>', $hidden=false) {
      global $languages_id;
      $products_string = '';
      $products_array = array();
      $products_query = "select gz2p.products_id, pd.products_name from " . TABLE_ABSTRACT_ZONES . " az left join " . TABLE_GTEXT_TO_PRODUCTS . " gz2p on (gz2p.abstract_zone_id=az.abstract_zone_id) left join " . TABLE_PRODUCTS_DESCRIPTION . " pd on (pd.products_id=gz2p.products_id) where az.abstract_zone_id = '" . (int)$this->m_zID . "' and gz2p.gtext_id= '" . (int)$gtext_id . "' and pd.language_id = '" . (int)$languages_id . "'";
      tep_query_to_array($products_query, $products_array);
      for( $i=0, $j=count($products_array); $i<$j; $i++ ) {
        $products_string .= $products_array[$i]['products_name'];
        if( $hidden )
          $products_string .= tep_draw_hidden_field('p_id[' . $products_array[$i]['products_id'] . ']', $products_array[$i]['products_id']);
        $products_string .= $format;
      }
      if( $j ) {
        $products_string = substr($products_string, 0, -4);
      } else {
        $products_string = 'None';
      }
      return $products_string;
    }


    function process_action() {
      switch( $this->m_action ) {
        case 'update_multizone':
          return $this->update_multizone();
        case 'multi_products':
          return $this->multi_products();
        case 'multi_generic_entries':
          return $this->multi_generic_entries();
        case 'deleteconfirm_multizone':
          return $this->deleteconfirm_multizone();
        case 'deleteconfirm_zone':
          return $this->deleteconfirm_zone();
        case 'remove_products':
          return $this->remove_products();
        default:
          return parent::process_action(); 
          break;
      }
    }

    function update_multizone() {
      global $messageStack;
      if( !isset($_POST['gt_id']) || !is_array($_POST['gt_id']) ) {
        $messageStack->add_session('You must select at least one entry to update', 'error');
        tep_redirect(tep_href_link(FILENAME_ABSTRACT_ZONES, tep_get_all_get_params(array('action')) . 'action=list'));
      }
      foreach ($_POST['gt_id'] as $gtext_id=>$val) {
        $sql_data_array = array(
                                'sequence_order' => tep_db_input(tep_db_prepare_input($_POST['seq'][$gtext_id]))
                               );

        tep_db_perform(TABLE_GTEXT_TO_DISPLAY, $sql_data_array, 'update', "abstract_zone_id = '" . (int)$this->m_zID . "' and gtext_id = '" . (int)$gtext_id . "'");
      }
      tep_redirect(tep_href_link(FILENAME_ABSTRACT_ZONES, tep_get_all_get_params(array('action')) . 'action=list'));
    }

    function multi_products() {
      if( isset($_GET['products_list']) ) {
        $ssID = tep_db_prepare_input($_GET['products_list']);
        $gtext_id = tep_db_prepare_input($this->m_sID);
        tep_redirect(tep_href_link(FILENAME_ABSTRACT_ZONES, tep_get_all_get_params(array('action', 'sID', 'products_list')) . 'action=multi_products&sID=' . $gtext_id . '&ssID=' . $ssID));
      }
    }


    function multi_generic_entries() {
      $multi_form = (isset($_POST['multi_form']) ? $_POST['multi_form'] : '');
      switch( $multi_form ) {
        case 'multi_generic_entries':
          $gtext_id = tep_db_prepare_input($_POST['gtext_id']);
          if( !is_array($_POST['gtext_id'])) break;
          foreach($_POST['gtext_id'] as $gtext_id=>$val) {
            $check_query = tep_db_query("select gtext_id from " . TABLE_GTEXT_TO_DISPLAY . " where gtext_id = '" . (int)$gtext_id . "' and abstract_zone_id = '" . (int)$this->m_zID . "'");
            if( tep_db_num_rows($check_query) )
                continue;

            tep_db_query("insert into " . TABLE_GTEXT_TO_DISPLAY . " (abstract_zone_id, gtext_id) values ('" . (int)tep_db_input($this->m_zID) . "', '" . (int)tep_db_input($gtext_id) . "')");
          }
          tep_redirect(tep_href_link(FILENAME_ABSTRACT_ZONES, tep_get_all_get_params(array('action', 'sID')) . 'action=list&sID=' . $gtext_id));
          break;

        case 'multi_products':
          $gtext_id = tep_db_prepare_input($_POST['products_list']);
          if( !is_array($_POST['products_id'])) break;
          foreach($_POST['products_id'] as $products_id=>$val) {
            $check_query = tep_db_query("select products_id from " . TABLE_GTEXT_TO_PRODUCTS . " where products_id = '" . (int)$products_id . "' and gtext_id = '" . (int)$gtext_id . "' and abstract_zone_id = '" . (int)$this->m_zID . "'");
            if( tep_db_num_rows($check_query) )
                continue;

            tep_db_query("insert into " . TABLE_GTEXT_TO_PRODUCTS . " (abstract_zone_id, gtext_id, products_id) values ('" . (int)tep_db_input($this->m_zID) . "', '" . (int)tep_db_input($gtext_id) . "', '" . (int)tep_db_input($products_id) . "')");
          }
          tep_redirect(tep_href_link(FILENAME_ABSTRACT_ZONES, tep_get_all_get_params(array('action', 'sID')) . 'action=list&sID=' . $gtext_id));
          break;
        default:
          break;
      }
    }

    function deleteconfirm_multizone() {
      for($i=0, $j=count($_POST['gtext_id']); $i<$j; $i++ ) {
        $gtext_id = $_POST['gtext_id'][$i];
        tep_db_query("delete from " . TABLE_GTEXT_TO_DISPLAY . " where abstract_zone_id = '" . (int)$this->m_zID . "' and gtext_id = '" . (int)$gtext_id . "'");
      }
      tep_redirect(tep_href_link(FILENAME_ABSTRACT_ZONES, tep_get_all_get_params(array('action')) . 'action=list'));
    }

    function deleteconfirm_zone() {
      tep_db_query("delete from " . TABLE_GTEXT_TO_PRODUCTS . " where abstract_zone_id = '" . (int)$this->m_zID . "'");
      tep_db_query("delete from " . TABLE_GTEXT_TO_DISPLAY . " where abstract_zone_id = '" . (int)$this->m_zID . "'");
      parent::deleteconfirm_zone();
      tep_redirect(tep_href_link(FILENAME_ABSTRACT_ZONES));
    }

    function remove_products() {
      tep_db_query("delete from " . TABLE_GTEXT_TO_PRODUCTS . " where abstract_zone_id = '" . (int)$this->m_zID . "' and gtext_id= '" . (int)$this->m_sID . "'");

      tep_redirect(tep_href_link(FILENAME_ABSTRACT_ZONES, tep_get_all_get_params(array('action')) . 'action=list'));
    }

    function display_html() {
      switch( $this->m_action ) {
        case 'list':
          $result = $this->display_list();
          break;
        case 'multi_generic_entries':
          $result = $this->display_multi_generic_entries();
          break;
        case 'multi_products':
          $result = $this->display_multi_products();
          break;
        case 'delete_multizone':
          $result = $this->display_delete_multizone();
          break;
        case 'apply_multizone':
          $result = $this->display_apply_multizone();
          break;
        default:
          $result = $this->display_default();
          $result .= $this->display_bottom();
          break;
      }
      return $result;
    }

// Categories/Products List
    function display_list() {
      global $languages_id;
      $categories_array = $this->get_category_tree_paths();
      $html_string = '';
      $rows = 0;
      $zones_query_raw = "select gz2d.abstract_zone_id, gz2d.gtext_id, gz2d.sequence_order, gt.gtext_title from " . TABLE_GTEXT . " gt left join " . TABLE_GTEXT_TO_DISPLAY . " gz2d on (gz2d.gtext_id = gt.gtext_id) where gz2d.abstract_zone_id = '" . (int)$this->m_zID . "' order by gz2d.sequence_order, gt.gtext_title";
      $zones_split = new splitPageResults($this->m_spage, ABSTRACT_PAGE_SPLIT, $zones_query_raw, $zones_query_numrows, 'gz2d.gtext_id');
      if( $zones_query_numrows > 0 ) {
        $html_string .= 
        '      <tr>' . "\n" . 
        '        <td valign="top">' . tep_draw_form('rl', FILENAME_ABSTRACT_ZONES, 'action=delete_multizone&zID=' . $this->m_zID . '&zpage=' . $this->m_zpage . '&spage=' . $this->m_spage, 'post') . '<table border="0" width="100%" cellspacing="1" cellpadding="3">' . "\n" . 
        '          <tr class="dataTableHeadingRow">' . "\n" . 
        '            <td class="dataTableHeadingContent"><a href="javascript:void(0)" onClick="check_boxes(document.rl)" title="Page Select On/Off" class="menuBoxHeadingLink"><span class="dataTableHeadingContent">' . tep_image(DIR_WS_ICONS . 'tick.gif', 'Page Select On/Off') . '</span></a></td>' . "\n" . 

        '            <td class="dataTableHeadingContent">' . TABLE_HEADING_ENTRIES . '</td>' . "\n" . 
        '            <td class="dataTableHeadingContent">' . TABLE_HEADING_SEQUENCE_ORDER . '</td>' . "\n" . 
        '            <td class="dataTableHeadingContent">' . TABLE_HEADING_PRODUCTS . '</td>' . "\n" . 
        '            <td class="dataTableHeadingContent" align="right">' . TABLE_HEADING_ASSIGN_PRODUCTS . '</td>' . "\n" . 
        '          </tr>' . "\n";
        $zones_query = tep_db_query($zones_query_raw);
        $bCheck = false;
        while ($zones = tep_db_fetch_array($zones_query)) {
          $products_string = $this->get_products_string($zones['gtext_id']);
          $rows++;
          if( (!tep_not_null($this->m_sID) || (tep_not_null($this->m_sID) && $this->m_sID == $zones['gtext_id'])) && !isset($sInfo) ) {
            $sInfo = new objectInfo($zones);
            $row_class = 'dataTableRowHigh';
          } else {
            $row_class = ($rows%2)?'dataTableRow':'dataTableRowSelected';
          }
          $html_string .= 
          '          <tr class="' . $row_class . '">' . "\n" . 
          '            <td class="dataTableContent">' . tep_draw_checkbox_field('gt_id[' . $zones['gtext_id'] . ']', ($bCheck?'on':''), $bCheck ) . '</td>' . "\n" . 
          '            <td class="dataTableContent">' . $zones['gtext_title'] . '</td>' . "\n" . 
          '            <td class="dataTableContent">' . tep_draw_input_field('seq[' . $zones['gtext_id'] . ']', $zones['sequence_order'], 'size="3" maxlength="3"') . '</td>' . "\n" . 
          '            <td class="dataTableContent">' . $products_string . '</td>' . "\n";
          if( isset($sInfo) && is_object($sInfo) && $sInfo->gtext_id == $zones['gtext_id'] ) {
            $html_string .= 
            '            <td class="dataTableContent" align="right">' . 
            '<a href="' . tep_href_link(FILENAME_ABSTRACT_ZONES, tep_get_all_get_params(array('action', 'sID')) . 'action=remove_products&sID=' . $zones['gtext_id']) . '">' . tep_image(DIR_WS_ICONS . 'cross.gif', 'Remove assigned products from this entry') . '</a>&nbsp;' . tep_image(DIR_WS_IMAGES . 'icon_arrow_right.gif', 'Selected Entry') .
            '            </td>' . "\n"; 
          } else {
            $html_string .= 
            '            <td class="dataTableContent" align="right">' .
            '<a href="' . tep_href_link(FILENAME_ABSTRACT_ZONES, tep_get_all_get_params(array('action', 'sID')) . 'action=remove_products&sID=' . $zones['gtext_id']) . '">' . tep_image(DIR_WS_ICONS . 'cross.gif', 'Remove assigned products from this entry') . '</a>&nbsp;<a href="' . tep_href_link(FILENAME_ABSTRACT_ZONES, tep_get_all_get_params(array('sID')) . 'sID=' . $zones['gtext_id']) . '">' . tep_image(DIR_WS_IMAGES . 'icon_info.gif', 'Click here to select this entry for additional options.') . '</a>' . 
            '</td>' . "\n";
          }
          $html_string .= 
          '          </tr>'  . "\n";
        }
        if(empty($this->m_saction)) {
          $html_string .= 
          '          <tr>' . "\n" . 
          '            <td colspan="5"><table border="0" width="100%" cellspacing="0" cellpadding="2">' . "\n" . 
          '              <tr>' . "\n" . 
          '                <td><a href="' . tep_href_link(FILENAME_ABSTRACT_ZONES, 'zpage=' . $this->m_zpage . '&zID=' . $this->m_zID) . '">' . tep_image_button('button_back.gif', IMAGE_BACK) . '</a> ' . tep_image_submit('button_update.gif', TEXT_UPDATE_MULTIZONE, 'onclick="this.form.action=' . '\'' . tep_href_link(FILENAME_ABSTRACT_ZONES, tep_get_all_get_params(array('action')) . 'action=update_multizone') . '\'' . '"') . ' ' . tep_image_submit('button_delete.gif', TEXT_DELETE_MULTIZONE, 'onclick="this.form.action=' . '\'' . tep_href_link(FILENAME_ABSTRACT_ZONES, tep_get_all_get_params(array('action')) . 'action=delete_multizone') . '\'' . '"') . '</td>' . "\n" . 
          '              </tr>' . "\n" . 
          '            </table></td>' . "\n" . 
          '          </tr>' . "\n";
        }
        $html_string .= 
        '          </table></form></td>' . "\n" . 
        '        </tr>' . "\n" . 
        '        <tr>' . "\n" . 
        '          <td><table border="0" width="100%" cellspacing="0" cellpadding="2">' . "\n" . 
        '            <tr>' . "\n" . 
        '              <td class="smallText" valign="top">' . $zones_split->display_count($zones_query_numrows, ABSTRACT_PAGE_SPLIT, $this->m_spage, TEXT_DISPLAY_NUMBER_OF_ENTRIES) . '</td>' . "\n" . 
        '              <td class="smallText" align="right">' . $zones_split->display_links($zones_query_numrows, ABSTRACT_PAGE_SPLIT, MAX_DISPLAY_PAGE_LINKS, $this->m_spage, tep_get_all_get_params(array('action', 'spage')) . 'action=list', 'spage') . '</td>' . "\n" . 
        '            </tr>' . "\n" . 
        '          </table></td>' . "\n" . 
        '        </tr>' . "\n";
      } else {
        $html_string .= 
        '        <tr>' . "\n" . 
        '          <td class="smallText">' . TEXT_INFO_NO_ENTRIES . '</td>' . "\n" . 
        '        </tr>' . "\n";
      }
      if (empty($this->m_saction)) {
        $html_string .= 
        '        <tr>' . "\n" . 
        '          <td>' . tep_draw_separator('pixel_trans.gif', '100%', '10') . '</td>' . "\n" . 
        '        </tr>' . "\n" . 
        '        <tr>' . "\n" . 
        '          <td><table border="0" width="100%" cellspacing="0" cellpadding="0">' . "\n" . 
        '            <tr>' . "\n" . 
        '              <td><table border="0" width="100%" cellspacing="0" cellpadding="2">' . "\n" . 
        '              <td nowrap><a href="' . tep_href_link(FILENAME_ABSTRACT_ZONES, tep_get_all_get_params(array('action')) . 'action=multi_generic_entries') . '">' . tep_image_button('button_entries.gif', 'Switch to Generic Entries Mode') . '</a></td>' . "\n" . 
        '              <td nowrap width="90%">&nbsp;</td>' . "\n" . 
        '              </table></td>' . "\n" . 
        '            </tr>' . "\n";
        if( isset($sInfo) ) {
          $html_string .= 
          '            <tr>' . "\n" . 
          '              <td><hr /></td>' . "\n" . 
          '            </tr>' . "\n" . 
          '            <tr>' . "\n" . 
          '              <td class="pageHeading">Assign Products to&nbsp;&raquo;&nbsp;' . $sInfo->gtext_title . '</td>' . "\n" . 
          '            </tr>' . "\n" . 
          '            <tr>' . "\n" . 
          '              <td>' . tep_draw_separator('pixel_trans.gif', '100%', '4') . '</td>' . "\n" . 
          '            </tr>' . "\n" . 
          '            <tr>' . "\n" . 
          '              <td>' . tep_draw_form('mz', FILENAME_ABSTRACT_ZONES, '', 'get') . '<table border="0" width="100%" cellspacing="0" cellpadding="2">' . "\n" . 
          '                <tr>' . "\n" . 
          '                  <td nowrap class="smallText"><b>Select Products from:</b></td>' . "\n" . 
          '                  <td nowrap>' . tep_draw_pull_down_menu('products_list', $categories_array, (isset($this->m_ssID)?$this->m_ssID:'')) . '</td>' . "\n" . 
          '                  <td nowrap>' . tep_draw_hidden_field('action', 'multi_products') . tep_draw_hidden_field('zID', $this->m_zID) . tep_draw_hidden_field('zpage', $this->m_zpage) . tep_draw_hidden_field('sID', $this->m_sID) . tep_draw_hidden_field('spage', $this->m_spage) . tep_draw_hidden_field('apage', $this->m_apage) . tep_image_submit('button_submit.gif', TEXT_SWITCH_PRODUCTS) . '</td>' . "\n" . 
          '                  <td nowrap width="90%">&nbsp;</td>' . "\n" . 
          '                </tr>' . "\n" . 
          '              </table></form></td>' . "\n" . 
          '            </tr>' . "\n";
        }
        $html_string .= 
        '          </table></td>' . "\n" . 
        '        </tr>' . "\n"; 
      }
      return $html_string;
    }


    function display_multi_generic_entries() {
      global $languages_id;
      $html_string = '';
      $html_string .=
      '        <tr>' . "\n" . 
      '          <td class="smallText">' . TEXT_SELECT_MULTIENTRIES . '</td>' . "\n" . 
      '        </tr>' . "\n" . 
      '        <tr>' . "\n" . 
      '          <td>' . tep_draw_separator('pixel_trans.gif', '100%', '10') . '</td>' . "\n" . 
      '        </tr>' . "\n" . 
      '        <tr>' . "\n" . 
      '          <td valign="top">' . tep_draw_form('mc', FILENAME_ABSTRACT_ZONES, tep_get_all_get_params(array('action')) . 'action=multi_generic_entries', 'post') . '<table border="0" width="100%" cellspacing="1" cellpadding="3">' . "\n" . 
      '            <tr class="dataTableHeadingRow">' . "\n" . 
      '              <td class="dataTableHeadingContent"><a href="javascript:void(0)" onClick="check_boxes(document.mc)" title="Page Select On/Off" class="menuBoxHeadingLink"><span class="dataTableHeadingContent">' . tep_image(DIR_WS_ICONS . 'tick.gif', 'Page Select On/Off') . '</span></a></td>' . "\n" . 
      '              <td class="dataTableHeadingContent">' . TABLE_HEADING_ENTRIES . '</td>' . "\n" . 
      '            </tr>' . "\n"; 
      $rows = 0;
      $generic_query_raw = "select gt.gtext_id, gt.gtext_title from " . TABLE_GTEXT . " gt order by gt.gtext_title";
      $generic_split = new splitPageResults($_GET['mcpage'], ABSTRACT_PAGE_SPLIT, $generic_query_raw, $generic_query_numrows);
      $generic_query = tep_db_query($generic_query_raw);
      $bCheck = false;
      while ($gentries = tep_db_fetch_array($generic_query)) {
        $rows++;
        $row_class = ($rows%2)?'dataTableRow':'dataTableRowSelected';
        $html_string .=
        '            <tr class="' . $row_class . '">' . "\n" . 
        '              <td class="dataTableContent">' . tep_draw_checkbox_field('gtext_id[' . $gentries['gtext_id'] . ']', ($bCheck?'on':''), $bCheck ) . '</td>' . "\n" . 
        '              <td class="dataTableContent">' . $gentries['gtext_title'] . '</td>' . "\n" . 
        '            </tr>' . "\n";
      }
      $html_string .=
      '              <tr>' . "\n" . 
      '                <td colspan="3"><table border="0" width="100%" cellspacing="0" cellpadding="2">' . "\n" . 
      '                  <tr>' . "\n" . 
      '                    <td><a href="' . tep_href_link(FILENAME_ABSTRACT_ZONES, tep_get_all_get_params(array('action', 'mcpage')) . 'action=list') . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>' . tep_draw_hidden_field('multi_form', 'multi_generic_entries') . '&nbsp;' . tep_image_submit('button_insert.gif', IMAGE_INSERT) . '</td>' . "\n" . 
      '                  </tr>' . "\n" . 
      '                </table></td>' . "\n" . 
      '              </tr>' . "\n" . 
      '            </table></form></td>' . "\n" . 
      '          </tr>' . "\n" . 
      '          <tr>' . "\n" . 
      '            <td><table border="0" width="100%" cellspacing="0" cellpadding="2">' . "\n" . 
      '              <tr>' . "\n" . 
      '                <td class="smallText" valign="top">' . $generic_split->display_count($generic_query_numrows, ABSTRACT_PAGE_SPLIT, $_GET['mcpage'], TEXT_DISPLAY_NUMBER_OF_ENTRIES) . '</td>' . "\n" . 
      '                <td class="smallText" align="right">' . $generic_split->display_links($generic_query_numrows, ABSTRACT_PAGE_SPLIT, MAX_DISPLAY_PAGE_LINKS, $_GET['mcpage'], tep_get_all_get_params(array('action', 'mcpage')) . 'action=multi_generic_entries', 'mcpage') . '</td>' . "\n" . 
      '              </tr>' . "\n" . 
      '            </table></td>' . "\n" . 
      '          </tr>' . "\n";
      return $html_string;
    }


    function display_multi_products() {
      global $languages_id;
      $html_string = '';
      $html_string .=
      '        <tr>' . "\n" . 
      '          <td class="smallText">' . TEXT_SELECT_MULTIZONES . '</td>' . "\n" . 
      '        </tr>' . "\n" . 
      '        <tr>' . "\n" . 
      '          <td>' .  tep_draw_separator('pixel_trans.gif', '100%', '10') . '</td>' . "\n" . 
      '        </tr>' . "\n" . 
      '        <tr>' . "\n" . 
      '          <td valign="top">' .  tep_draw_form('mp', FILENAME_ABSTRACT_ZONES, tep_get_all_get_params(array('action')) . 'action=multi_generic_entries', 'post') . '<table border="0" width="100%" cellspacing="1" cellpadding="3">' . "\n" . 
      '            <tr class="dataTableHeadingRow">' . "\n" . 
      '              <td class="dataTableHeadingContent"><a href="javascript:void(0)" onClick="check_boxes(document.mp)" title="Page Select On/Off" class="menuBoxHeadingLink"><span class="dataTableHeadingContent">' . tep_image(DIR_WS_ICONS . 'tick.gif', 'Page Select On/Off') . '</span></a></td>' . "\n" . 
      '              <td class="dataTableHeadingContent">' . TABLE_HEADING_CATEGORIES . '</td>' . "\n" . 
      '              <td class="dataTableHeadingContent">' . TABLE_HEADING_PRODUCTS . '</td>' . "\n" . 
      '            </tr>' . "\n";

      $rows = 0;
      $zones_query_raw = "select distinct p2c.products_id, p2c.categories_id, cd.categories_name, pd.products_name from " . TABLE_PRODUCTS_TO_CATEGORIES . " p2c left join " . TABLE_CATEGORIES_DESCRIPTION . " cd on (p2c.categories_id=cd.categories_id) left join " . TABLE_PRODUCTS_DESCRIPTION . " pd on (pd.products_id=p2c.products_id) where cd.language_id= '" . (int)$languages_id . "' and pd.language_id= '" . (int)$languages_id . "' and p2c.categories_id = '" . (int)$this->m_ssID . "' order by p2c.categories_id, cd.categories_name, pd.products_name";
      $zones_split = new splitPageResults($_GET['mppage'], ABSTRACT_PAGE_SPLIT, $zones_query_raw, $zones_query_numrows, 'p2c.products_id');
      $zones_query = tep_db_query($zones_query_raw);
      $bCheck = false;
      while ($zones = tep_db_fetch_array($zones_query)) {
        $product_categories_string = $this->get_categories_string($zones['categories_id']);
        $rows++;
        $row_class = ($rows%2)?'dataTableRow':'dataTableRowSelected';
        $html_string .=
        '            <tr class="' . $row_class . '">' . "\n" . 
        '              <td class="dataTableContent">' . tep_draw_checkbox_field('products_id[' . $zones['products_id'] . ']', ($bCheck?'on':''), $bCheck ) . '</td>' . "\n" . 
        '              <td class="dataTableContent">' . $product_categories_string . '</td>' . "\n" . 
        '              <td class="dataTableContent">' . $zones['products_name'] . '</td>' . "\n" . 
        '            </tr>' . "\n";
      }
      $html_string .=
      '              <tr>' . "\n" . 
      '                <td colspan="3"><table border="0" width="100%" cellspacing="0" cellpadding="2">' . "\n" . 
      '                  <tr>' . "\n" . 
      '                    <td><a href="' . tep_href_link(FILENAME_ABSTRACT_ZONES, tep_get_all_get_params(array('action', 'mppage')) . 'action=list') . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>' . tep_draw_hidden_field('multi_form', 'multi_products') . '&nbsp;' . tep_image_submit('button_insert.gif', IMAGE_INSERT) . tep_draw_hidden_field('multi_form', 'multi_products') . tep_draw_hidden_field('products_list', $this->m_sID) . '</td>' . "\n" . 
      '                  </tr>' . "\n" . 
      '                </table></td>' . "\n" . 
      '              </tr>' . "\n" . 
      '            </table></form></td>' . "\n" . 
      '          </tr>' . "\n" . 
      '          <tr>' . "\n" . 
      '            <td><table border="0" width="100%" cellspacing="0" cellpadding="2">' . "\n" . 
      '              <tr>' . "\n" . 
      '                <td class="smallText" valign="top">' . $zones_split->display_count($zones_query_numrows, ABSTRACT_PAGE_SPLIT, $_GET['mppage'], TEXT_DISPLAY_NUMBER_OF_CATEGORIES_PRODUCTS) . '</td>' . "\n" . 
      '                <td class="smallText" align="right">' . $zones_split->display_links($zones_query_numrows, ABSTRACT_PAGE_SPLIT, MAX_DISPLAY_PAGE_LINKS, $_GET['mppage'], tep_get_all_get_params(array('action', 'mppage')) . 'action=multi_products', 'mppage') . '</td>' . "\n" . 
      '              </tr>' . "\n" . 
      '            </table></td>' . "\n" . 
      '          </tr>' . "\n";
      return $html_string;
    }


    function display_delete_multizone() {
      global $languages_id;
      $rows = 0;
      $html_string = '';
      $zones_query = tep_db_query("select abstract_zone_name from " . TABLE_ABSTRACT_ZONES . " where abstract_zone_id = '" . (int)$this->m_zID . "'");
      $zones_array = tep_db_fetch_array($zones_query);
      $html_string .= 
      '        <tr>' . "\n" . 
      '          <td class="smallText">' . sprintf(TEXT_DELETE_MULTIZONE_CONFIRM, $zones_array['abstract_zone_name']) . '</td>' . "\n" . 
      '        </tr>' . "\n" . 
      '        <tr>' . "\n" . 
      '          <td>' . tep_draw_separator('pixel_trans.gif', '100%', '10') . '</td>' . "\n" . 
      '        </tr>' . "\n" . 
      '        <tr>' . "\n" . 
      '          <td valign="top">' . tep_draw_form('rl_confirm', FILENAME_ABSTRACT_ZONES, tep_get_all_get_params(array('action')) . 'action=deleteconfirm_multizone', 'post') . '<table border="0" width="100%" cellspacing="1" cellpadding="3">' . "\n" . 
      '            <tr class="dataTableHeadingRow">' . "\n" . 
      '              <td class="dataTableHeadingContent">' . TABLE_HEADING_ENTRIES . '</td>' . "\n" . 
      '            </tr>' . "\n";

      foreach ($_POST['gt_id'] as $gtext_id=>$val) {
        $delete_query = tep_db_query("select gz2d.abstract_zone_id, gz2d.gtext_id, gt.gtext_title from " . TABLE_GTEXT . " gt left join " . TABLE_GTEXT_TO_DISPLAY . " gz2d on (gz2d.gtext_id = gt.gtext_id) where gz2d.gtext_id = '" . (int)$gtext_id . "' and gz2d.abstract_zone_id = '" . (int)$this->m_zID . "' order by gt.gtext_title");
        if( $delete_array = tep_db_fetch_array($delete_query) ) {
          $rows++;
          $row_class = ($rows%2)?'dataTableRow':'dataTableRowSelected';
          $html_string .= 
          '          <tr class="' . $row_class . '">' . "\n" . 
          '            <td class="dataTableContent">' . tep_draw_hidden_field('gtext_id[]', $delete_array['gtext_id']) . $delete_array['gtext_title'] . '</td>' . "\n" . 
          '          </tr>' . "\n";
        }
      }
      if( count($_POST['gt_id']) ) {
        $html_string .= 
        '            <tr>' . "\n" . 
        '              <td colspan="4"><a href="' . tep_href_link(FILENAME_ABSTRACT_ZONES, tep_get_all_get_params(array('action')) . 'action=list') . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a> ' . tep_image_submit('button_confirm.gif', IMAGE_CONFIRM) . '</td>' . "\n" . 
        '            </tr>' . "\n";
      }
      $html_string .= 
      '            </table></form></td>' . "\n" . 
      '          </tr>' . "\n";
      return $html_string;
    }
  }
?>