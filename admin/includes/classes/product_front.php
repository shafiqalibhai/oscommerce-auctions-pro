<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2008 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
//----------------------------------------------------------------------------
// I-Metrics Layer
//----------------------------------------------------------------------------
// Catalog: Products Zones class
// This is a Bridge for the Abstract Zones front-end
// Support class for products via abstract zones
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
  class product_front extends abstract_front {

// class constructor
    function product_front() {
      parent::abstract_front();
    }

    function get_group_products($zone) {
      global $languages_id;

      $result_array = array();
      $zone_id = $this->get_zone($zone);
      if( !$zone_id) {
        return $result_array;
      }

      $zone_query = tep_db_query("select products_id, categories_id from " . TABLE_PRODUCTS_ZONES_TO_CATEGORIES . " where abstract_zone_id = '" . (int)$zone_id . "' and status_id='1' order by sort_id");
      while( $zone = tep_db_fetch_array($zone_query) ) {
        if( !$zone['products_id'] ) {
          $this->set_key_products($result_array, $zone['categories_id']);         
        } else {
          $result_array[$zone['products_id']] = $zone['products_id'];
        }
      }
      return $result_array;
    }

    function get_random_products($zone, $limit=9) {
      global $languages_id;

      $result_array = array();
      $zone_id = $this->get_zone($zone);
      if( !$zone_id) {
        return $result_array;
      }

      $count = 0;
      $zone_query = tep_db_query("select products_id, categories_id from " . TABLE_PRODUCTS_ZONES_TO_CATEGORIES . " where abstract_zone_id = '" . (int)$zone_id . "' and status_id='1' order by sort_id");
      while( $zone = tep_db_fetch_array($zone_query) ) {
        if( !$zone['products_id'] ) {
          $count += $this->set_key_products($result_array, $zone['categories_id']);
        } else {
          $result_array[$zone['products_id']] = $zone['products_id'];
          $count++;
        }
        if( $count > ($limit-1) ) break;
      }
      return $result_array;
    }
  }
?>