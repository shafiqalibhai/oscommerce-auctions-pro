<?php
/*
//----------------------------------------------------------------------------
//-------------- SEO-G by Asymmetrics (Renegade Edition) ---------------------
//----------------------------------------------------------------------------
// Copyright (c) 2006-2007 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Admin: SEO-G Filters class
// This is a Bridge for SEO-G
// Processes product filters table generates filters seo urls.
//----------------------------------------------------------------------------
// I-Metrics Layer
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/

  class seo_filters extends seo_zones {
    var $error_array;

// class constructor
    function seo_filters() {
      $this->m_ssID = isset($_GET['ssID'])?$_GET['ssID']:'';
      $this->m_mcpage = isset($_GET['mcpage'])?$_GET['mcpage']:'';
      $this->m_mppage = isset($_GET['mppage'])?$_GET['mppage']:'';
      parent::seo_zones();
    }

    function generate_name($filter_id, $select_id, $separator=SEO_DEFAULT_WORDS_SEPARATOR) {
      $string = '';
      //$name_query = tep_db_query("select concat(pf.products_filter_name, ' ', ps.products_select_name) as seo_name from " . TABLE_PRODUCTS_FILTERS . " pf left join " . TABLE_PRODUCTS_SELECT . " ps on (pf.products_filter_id = ps.products_filter_id) where ps.products_filter_id = '" . (int)$filter_id . "' and ps.products_select_id = '" . (int)$select_id . "'");
      $name_query = tep_db_query("select ps.products_select_name as seo_name from " . TABLE_PRODUCTS_FILTERS . " pf left join " . TABLE_PRODUCTS_SELECT . " ps on (pf.products_filter_id = ps.products_filter_id) where ps.products_filter_id = '" . (int)$filter_id . "' and ps.products_select_id = '" . (int)$select_id . "'");

      if( !tep_db_num_rows($name_query) )
        return $string;

      $names_array = tep_db_fetch_array($name_query);
      $string =  $this->create_safe_string($names_array['seo_name'], $separator);
      return $this->adapt_lexico($string, $separator);
    }

    function process_action() {
      switch( $this->m_action ) {
        case 'validate':
          return $this->validate();
        case 'validate_confirm':
          return $this->validate_confirm();
        case 'update_multizone':
          $result = parent::validate_array_selection('pc_id'); 
          return $this->update_multizone();
        case 'multi_filter':
          return $this->multi_filter();
        case 'deleteconfirm_multizone':
          $result = parent::validate_array_selection('pc_id'); 
          return $this->deleteconfirm_multizone();
        case 'delete_multizone':
          $result = parent::validate_array_selection('pc_id'); 
        default:
          return parent::process_action(); 
          break;
      }
    }

    function validate() {
      $this->error_array = array();
      // First pass check for missing ranges from seo table
      $check_query = tep_db_query("select concat(ps.products_filter_id,'_',ps.products_select_id) as pc_id, ps.products_select_name as name, '0' as missing_id from " . TABLE_PRODUCTS_SELECT . " ps left join " . TABLE_SEO_TO_FILTERS . " s2f on (s2f.products_filter_id = ps.products_filter_id and s2f.products_select_id = ps.products_select_id) where s2f.products_filter_id is null and s2f.products_select_id is null and ps.products_select_id > 0 order by ps.products_filter_id desc limit " . SEO_PAGE_SPLIT);
      while( $check_array = tep_db_fetch_array($check_query) ) {
        $this->error_array[] = $check_array;
      }
      // Second pass check for redundant entries in the seo table
      $check_query = tep_db_query("select concat(s2f.products_filter_id,'_',s2f.products_select_id) as pc_id, s2f.seo_name as name, '-1' as missing_id from " . TABLE_SEO_TO_FILTERS . " s2f left join " . TABLE_PRODUCTS_SELECT . " ps on (s2f.products_filter_id = ps.products_filter_id and s2f.products_select_id = ps.products_select_id) where ps.products_filter_id is null and ps.products_select_id is null order by s2f.products_filter_id desc limit " . SEO_PAGE_SPLIT);
      while( $check_array = tep_db_fetch_array($check_query) ) {
        $this->error_array[] = $check_array;
      }
      return $this->error_array;
    }

    function validate_confirm() {
      if( !is_array($_POST['pc_id'])) {
        tep_redirect(tep_href_link(FILENAME_SEO_ZONES, tep_get_all_get_params(array('action')) . 'action=validate'));
      }

      foreach($_POST['pc_id'] as $key => $val) {
        $pfs_array = explode('_', $key);
        if( !is_array($pfs_array) || count($pfs_array) != 2 ) {
          continue;
        }

        if( $_POST['missing'][$key] == -1 ) {
          tep_db_query("delete from " . TABLE_SEO_TO_FILTERS . " where products_filter_id = '" . (int)$pfs_array[0] . "' and products_select_id = '" . (int)$pfs_array[1] . "'");
        } elseif( $_POST['missing'][$key] == 0 ) {
          $seo_name = $this->generate_name($pfs_array[0], $pfs_array[1]);

          $sql_data_array = array(
                                  'products_filter_id' => (int)$pfs_array[0],
                                  'products_select_id' => (int)$pfs_array[1],
                                  'seo_name' => tep_db_prepare_input($seo_name),
                                  );
          tep_db_perform(TABLE_SEO_TO_FILTERS, $sql_data_array, 'insert');
        }
      }
      tep_redirect(tep_href_link(FILENAME_SEO_ZONES, tep_get_all_get_params(array('action')) . 'action=validate'));
    }

    function update_multizone() {
      if( !is_array($_POST['pc_id'])) {
        tep_redirect(tep_href_link(FILENAME_SEO_ZONES, tep_get_all_get_params(array('action')) . 'action=list'));
      }
      foreach ($_POST['pc_id'] as $key => $val) {
        $pfs_array = explode('_', $key);
        if( !is_array($pfs_array) || count($pfs_array) != 2 ) {
          continue;
        }

        $seo_name = $this->create_safe_string($_POST['name'][$key]);
        if( SEO_PROXIMITY_CLEANUP == 'true' ) {
          $check_query = tep_db_query("select seo_name from " . TABLE_SEO_TO_FILTERS . " where products_filter_id = '" . (int)$pfs_array[0] . "' and products_select_id = '" . (int)$pfs_array[1] . "'");
          if( $check_array = tep_db_fetch_array($check_query) ) {
            $check_name = $check_array['seo_name'];
            tep_db_query("delete from " . TABLE_SEO_URL . " where seo_url_get like '%" . tep_db_input(tep_db_prepare_input($check_name)) . "%'");
          }
        }
        $sql_data_array = array(
                                'seo_name' => tep_db_prepare_input($seo_name)
                               );

        tep_db_perform(TABLE_SEO_TO_FILTERS, $sql_data_array, 'update', "products_filter_id = '" . (int)$pfs_array[0] . "' and products_select_id = '" . (int)$pfs_array[1] . "'");
      }
      tep_redirect(tep_href_link(FILENAME_SEO_ZONES, tep_get_all_get_params(array('action')) . 'action=list'));
    }


    function multi_filter() {
      $multi_form = (isset($_POST['multi_form']) ? $_POST['multi_form'] : '');
      switch( $multi_form ) {
        case 'multi_filter':
          if( !is_array($_POST['pc_id'])) break;
          $tmp_array = array();
          foreach ($_POST['pc_id'] as $key => $val) {
            $pfs_array = explode('_', $key);
            if( !is_array($pfs_array) || count($pfs_array) != 2 ) {
              continue;
            }
            $check_query = tep_db_query("select seo_name from " . TABLE_SEO_TO_FILTERS . " where products_filter_id = '" . (int)$pfs_array[0] . "' and products_select_id = '" . (int)$pfs_array[1] . "'");
            if( tep_db_num_rows($check_query) > 0 ) continue;
            $seo_name = $this->generate_name($pfs_array[0], $pfs_array[1]);

            if( !isset($tmp_array[$seo_name]) ) {
              $tmp_array[$seo_name] = 1;
            } else {
              $tmp_array[$seo_name]++;
              $seo_name .= $tmp_array[$seo_name];
            }

            $sql_data_array = array(
                                    'products_filter_id' => (int)$pfs_array[0],
                                    'products_select_id' => (int)$pfs_array[1],
                                    'seo_name' => tep_db_prepare_input($seo_name),
                                    );
            tep_db_perform(TABLE_SEO_TO_FILTERS, $sql_data_array);
          }
          tep_redirect(tep_href_link(FILENAME_SEO_ZONES, tep_get_all_get_params(array('action')) . 'action=list'));
          break;
        default:
          break;
      }
    }

    function deleteconfirm_multizone() {
      for($i=0, $j=count($_POST['pc_id']); $i<$j; $i++ ) {
        $pfs_array = explode('_', $_POST['pc_id'][$i]);
        if( !is_array($pfs_array) || count($pfs_array) != 2 ) {
          continue;
        }
        if( SEO_PROXIMITY_CLEANUP == 'true' ) {
          $check_query = tep_db_query("select seo_name from " . TABLE_SEO_TO_FILTERS . " where products_filter_id = '" . (int)$pfs_array[0] . "' and products_select_id = '" . (int)$pfs_array[1] . "'");
          if( $check_array = tep_db_fetch_array($check_query) ) {
            $check_name = $check_array['seo_name'];
            tep_db_query("delete from " . TABLE_SEO_URL . " where seo_url_get like '%" . tep_db_input(tep_db_prepare_input($check_name)) . "%'");
          }
        }
        tep_db_query("delete from " . TABLE_SEO_TO_FILTERS . " where products_filter_id = '" . (int)$pfs_array[0] . "' and products_select_id = '" . (int)$pfs_array[1] . "'");
      }
      tep_redirect(tep_href_link(FILENAME_SEO_ZONES, tep_get_all_get_params(array('action')) . 'action=list'));
    }

    function display_html() {
      switch( $this->m_action ) {
        case 'validate':
          $result = $this->display_validation();
          break;
        case 'list':
          $result = $this->display_list();
          break;
        case 'multi_filter':
          $result = $this->display_multi_filter();
          break;
        case 'delete_multizone':
          $result = $this->display_delete_multizone();
          break;
        default:
          $result = $this->display_default();
          $result .= $this->display_bottom();
          break;
      }
      return $result;
    }

    function display_validation() {
      $html_string = '';
      $html_string .= 
      '      <tr>' . "\n" . 
      '        <td><hr /></td>' . "\n" . 
      '      </tr>' . "\n";
      if( count($this->error_array) ) {
        $html_string .= 
        '      <tr>' . "\n" . 
        '        <td><table border="0" width="100%" cellspacing="0" cellpadding="2">' . "\n" . 
        '          <tr>' . "\n" . 
        '            <td class="dataTableRowHighBorder" width="16">&nbsp;</td>' . "\n" . 
        '            <td class="smallText"><b>&nbsp;-&nbsp;Filter Combo present in the Filters/Selects tables but not present in the SEO-G table</b></td>' . "\n" . 
        '          </tr>' . "\n" . 
        '          <tr>' . "\n" . 
        '            <td colspan="2">' . tep_draw_separator('pixel_trans.gif', '100%', '1') . '</td>' . "\n" . 
        '          </tr>' . "\n" . 
        '          <tr>' . "\n" . 
        '            <td class="dataTableRowImpactBorder" width="16">&nbsp;</td>' . "\n" . 
        '            <td class="smallText"><b>&nbsp;-&nbsp;Filter Combo present in the SEO-G table but it is not present in the Filters/Selects tables</b></td>' . "\n" . 
        '          </tr>' . "\n" . 
        '        </table></td>' . "\n" . 
        '      </tr>' . "\n" .
        '      <tr>' . "\n" . 
        '        <td>' . tep_draw_separator('pixel_trans.gif', '100%', '10') . '</td>' . "\n" . 
        '      </tr>' . "\n";
        $html_string .= 
        '      <tr>' . "\n" . 
        '        <td valign="top">' . tep_draw_form('rl', FILENAME_SEO_ZONES, 'action=validate_confirm&zID=' . $this->m_zID . '&zpage=' . $this->m_zpage, 'post') . '<table border="0" width="100%" cellspacing="0" cellpadding="2">' . "\n" . 
        '          <tr class="dataTableHeadingRow">' . "\n" . 
        '            <td class="dataTableHeadingContent" width="40"><a href="javascript:void(0)" onClick="copy_checkboxes(document.rl, \'pc_id\')" title="Page Select On/Off" class="menuBoxHeadingLink"><span class="dataTableHeadingContent">' . TABLE_HEADING_SELECT . '</span></a></td>' . "\n" . 
        '            <td class="dataTableHeadingContent">' . TABLE_HEADING_ID . '</td>' . "\n" . 
        '            <td class="dataTableHeadingContent">' . TABLE_HEADING_NAME . '</td>' . "\n" . 
        '            <td class="dataTableHeadingContent">' . TABLE_HEADING_COMMENT . '</td>' . "\n" . 
        '          </tr>' . "\n";
        for($i=0, $j=count($this->error_array); $i<$j; $i++ ) {
          $row_class = ($this->error_array[$i]['missing_id'])?'dataTableRowImpact':'dataTableRowHigh';
          $html_string .= 
          '          <tr class="' . $row_class . '">' . "\n" . 
          '            <td class="dataTableContent">' . tep_draw_checkbox_field('pc_id[' . $this->error_array[$i]['pc_id'] . ']', 'on', false ) . tep_draw_hidden_field('missing[' . $this->error_array[$i]['pc_id'] . ']', $this->error_array[$i]['missing_id']) . '</td>' . "\n" . 
          '            <td class="dataTableContent">' . $this->error_array[$i]['pc_id'] . '</td>' . "\n" . 
          '            <td class="dataTableContent">' . $this->error_array[$i]['name'] . '</td>' . "\n" . 
          '            <td class="dataTableContent">' . (($this->error_array[$i]['missing_id'])?'Missing from Ranges Table':'Missing from SEO-G') . '</td>' . "\n" . 
          '          </tr>'  . "\n";
        }
        $html_string .= 
        '          <tr>' . "\n" . 
        '            <td colspan="4"><table border="0" width="100%" cellspacing="0" cellpadding="2">' . "\n" . 
        '              <tr>' . "\n" . 
        '                <td><a href="' . tep_href_link(FILENAME_SEO_ZONES, 'zpage=' . $this->m_zpage . '&zID=' . $this->m_zID) . '">' . tep_image_button('button_back.gif', IMAGE_BACK) . '</a> ' . tep_image_submit('button_fix_errors.gif', 'Fix Listed Errors') . '</td>' . "\n" . 
        '              </tr>' . "\n" . 
        '            </table></td>' . "\n" . 
        '          </tr>' . "\n" .
        '        </table></form></td>' . "\n" . 
        '      </tr>' . "\n";
      } else {
        $html_string .= 
        '      <tr>' . "\n" . 
        '        <td class="smallText">' . 'No Errors Found' . '</td>' . "\n" . 
        '      </tr>' . "\n" . 
        '      <tr>' . "\n" . 
        '        <td colspan="2">' . tep_draw_separator('pixel_trans.gif', '100%', '4') . '</td>' . "\n" . 
        '      </tr>' . "\n" . 
        '      <tr>' . "\n" . 
        '        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">' . "\n" . 
        '          <tr>' . "\n" . 
        '            <td><a href="' . tep_href_link(FILENAME_SEO_ZONES, 'zpage=' . $this->m_zpage . '&zID=' . $this->m_zID) . '">' . tep_image_button('button_back.gif', IMAGE_BACK) . '</a></td>' . "\n" . 
        '          </tr>' . "\n" . 
        '        </table></td>' . "\n" . 
        '      </tr>' . "\n";
      }
      return $html_string;
    }

// Default List
    function display_list() {
      $html_string = '';
      $rows = 0;
      $zones_query_raw = "select products_filter_id, products_select_id, seo_name from " . TABLE_SEO_TO_FILTERS . " order by seo_name";

      $zones_split = new splitPageResults($this->m_spage, SEO_PAGE_SPLIT, $zones_query_raw, $zones_query_numrows, 'products_select_id');
      if( $zones_query_numrows > 0 ) {
        $html_string .= 
        '      <tr>' . "\n" . 
        '        <td valign="top">' . tep_draw_form('rl', FILENAME_SEO_ZONES, 'action=delete_multizone&zID=' . $this->m_zID . '&zpage=' . $this->m_zpage . '&spage=' . $this->m_spage, 'post') . '<table border="0" width="100%" cellspacing="0" cellpadding="2">' . "\n" . 
        '          <tr class="dataTableHeadingRow">' . "\n" . 
        '            <td class="dataTableHeadingContent"><a href="javascript:void(0)" onClick="copy_checkboxes(document.rl, \'pc_id\')" title="Page Select On/Off" class="menuBoxHeadingLink"><span class="dataTableHeadingContent">' . TABLE_HEADING_SELECT . '</span></a></td>' . "\n" . 
        '            <td class="dataTableHeadingContent">' . TABLE_HEADING_RANGE . '</td>' . "\n" . 
        '            <td class="dataTableHeadingContent" align="center">' . TABLE_HEADING_NAME . '</td>' . "\n" . 
        '          </tr>' . "\n";
        $zones_query = tep_db_query($zones_query_raw);
        $bCheck = false;
        while ($zones = tep_db_fetch_array($zones_query)) {
          $filters_query = tep_db_query("select concat(pf.products_filter_name, ' ', ps.products_select_name) as final_name from " . TABLE_PRODUCTS_FILTERS . " pf left join " . TABLE_PRODUCTS_SELECT . " ps on (pf.products_filter_id=ps.products_filter_id) where pf.products_filter_id = '" . (int)$zones['products_filter_id'] . "' and products_select_id = '" . (int)$zones['products_select_id'] . "'");
          if( $filters_array = tep_db_fetch_array($filters_query) ) {
            $final_name = '<a href="' . tep_href_link(FILENAME_PRODUCTS_FILTERS, 'fID=' . $zones['products_filter_id'] . '&action=sub_list') . '"><b>' . $filters_array['final_name'] . '</b></a>';
          } else {
            $final_name = 'N/A';
          }


          $rows++;
          $row_class = ($rows%2)?'dataTableRow':'dataTableRowSelected';
          $html_string .= 
          '          <tr class="' . $row_class . '">' . "\n" . 
          '            <td class="dataTableContent">' . tep_draw_checkbox_field('pc_id[' . $zones['products_filter_id'] . '_' . $zones['products_select_id'] . ']', ($bCheck?'on':''), $bCheck ) . '</td>' . "\n" . 
          '            <td class="dataTableContent">' . $final_name . '</td>' . "\n" . 
          '            <td class="dataTableContent" align="center">' . tep_draw_input_field('name[' . $zones['products_filter_id'] . '_' . $zones['products_select_id'] . ']', $zones['seo_name'], 'style="width: 300px"') . '</td>' . "\n" . 
          '          </tr>'  . "\n";
        }
        if(empty($this->saction)) {
          $html_string .= 
          '          <tr>' . "\n" . 
          '            <td colspan="5"><table border="0" width="100%" cellspacing="0" cellpadding="2">' . "\n" . 
          '              <tr>' . "\n" . 
          '                <td><a href="' . tep_href_link(FILENAME_SEO_ZONES, 'zpage=' . $this->m_zpage . '&zID=' . $this->m_zID) . '">' . tep_image_button('button_back.gif', IMAGE_BACK) . '</a> ' . tep_image_submit('button_update.gif', TEXT_UPDATE_MULTIZONE, 'onclick="this.form.action=' . '\'' . tep_href_link(FILENAME_SEO_ZONES, tep_get_all_get_params(array('action')) . 'action=update_multizone') . '\'' . '"') . ' ' . tep_image_submit('button_delete.gif', TEXT_DELETE_MULTIZONE, 'onclick="this.form.action=' . '\'' . tep_href_link(FILENAME_SEO_ZONES, tep_get_all_get_params(array('action')) . 'action=delete_multizone') . '\'' . '"') . '</td>' . "\n" . 
          '              </tr>' . "\n" . 
          '            </table></td>' . "\n" . 
          '          </tr>' . "\n";
        }
        $html_string .= 
        '          </table></form></td>' . "\n" . 
        '        </tr>' . "\n" . 
        '        <tr>' . "\n" . 
        '          <td><table border="0" width="100%" cellspacing="0" cellpadding="2">' . "\n" . 
        '            <tr>' . "\n" . 
        '              <td class="smallText" valign="top">' . $zones_split->display_count($zones_query_numrows, SEO_PAGE_SPLIT, $this->m_spage, TEXT_DISPLAY_NUMBER_OF_ENTRIES) . '</td>' . "\n" . 
        '              <td class="smallText" align="right">' . $zones_split->display_links($zones_query_numrows, SEO_PAGE_SPLIT, MAX_DISPLAY_PAGE_LINKS, $this->m_spage, tep_get_all_get_params(array('action', 'spage')) . 'action=list', 'spage') . '</td>' . "\n" . 
        '            </tr>' . "\n" . 
        '          </table></td>' . "\n" . 
        '        </tr>' . "\n";
      } else {
        $html_string .= 
        '        <tr>' . "\n" . 
        '          <td class="smallText">' . TEXT_INFO_NO_ENTRIES . '</td>' . "\n" . 
        '        </tr>' . "\n";
      }
      if (empty($this->saction)) {
        $html_string .= 
        '        <tr>' . "\n" . 
        '          <td>' . tep_draw_separator('pixel_trans.gif', '100%', '10') . '</td>' . "\n" . 
        '        </tr>' . "\n" . 
        '        <tr>' . "\n" . 
        '          <td><table border="0" width="100%" cellspacing="0" cellpadding="0">' . "\n" . 
        '            <tr>' . "\n" . 
        '              <td nowrap><a href="' . tep_href_link(FILENAME_SEO_ZONES, tep_get_all_get_params(array('action')) . 'action=multi_filter') . '">' . tep_image_button('button_filters.gif', TEXT_SWITCH_FILTERS) . '</a></td>' . "\n" . 
        '            </tr>' . "\n" . 
        '          </table></td>' . "\n" . 
        '        </tr>' . "\n"; 
      }
      return $html_string;
    }


    function display_multi_filter() {
      $html_string = '';
      $html_string .=
      '        <tr>' . "\n" . 
      '          <td class="smallText">' . TEXT_SELECT_MULTIENTRIES . '</td>' . "\n" . 
      '        </tr>' . "\n" . 
      '        <tr>' . "\n" . 
      '          <td>' . tep_draw_separator('pixel_trans.gif', '100%', '10') . '</td>' . "\n" . 
      '        </tr>' . "\n" . 
      '        <tr>' . "\n" . 
      '          <td valign="top">' . tep_draw_form('mc', FILENAME_SEO_ZONES, tep_get_all_get_params(array('action')) . 'action=multi_filter', 'post') . '<table border="0" width="100%" cellspacing="0" cellpadding="2">' . "\n" . 
      '            <tr class="dataTableHeadingRow">' . "\n" . 
      '              <td class="dataTableHeadingContent"><a href="javascript:void(0)" onClick="copy_checkboxes(document.mc, \'pc_id\')" title="Page Select On/Off" class="menuBoxHeadingLink"><span class="dataTableHeadingContent">' . TABLE_HEADING_SELECT . '</span></a></td>' . "\n" . 
      '              <td class="dataTableHeadingContent">' . TABLE_HEADING_RANGE . '</td>' . "\n" . 
      '            </tr>' . "\n"; 
      $rows = 0;
      $filter_query_raw = "select pf.products_filter_id, ps.products_select_id, pf.products_filter_name, ps.products_select_name from " . TABLE_PRODUCTS_SELECT . " ps left join " . TABLE_PRODUCTS_FILTERS . " pf on (pf.products_filter_id=ps.products_filter_id) where products_select_id <> 0 order by pf.products_filter_name, ps.products_select_name";
      $filter_split = new splitPageResults($this->m_mcpage, SEO_PAGE_SPLIT, $filter_query_raw, $filter_query_numrows);
      $filter_query = tep_db_query($filter_query_raw);
      $bCheck = false;
      while ($filter = tep_db_fetch_array($filter_query)) {
        $check_query = tep_db_query("select count(*) as total from " . TABLE_SEO_TO_FILTERS . " where products_filter_id = '" . (int)$filter['products_filter_id'] . "' and products_select_id = '" . (int)$filter['products_select_id'] . "'");
        $check_array = tep_db_fetch_array($check_query);

        $bCheck = ($check_array['total'] == '1')?true:false;
        $rows++;
        $row_class = ($rows%2)?'dataTableRow':'dataTableRowSelected';
        if($bCheck)
          $row_class = 'dataTableRowGreen';

        $html_string .=
        '            <tr class="' . $row_class . '">' . "\n" . 
        '              <td class="dataTableContent">' . ($bCheck?'Included':tep_draw_checkbox_field('pc_id[' . $filter['products_filter_id'] . '_' . $filter['products_select_id'] . ']')) . '</td>' . "\n" . 
        '              <td class="dataTableContent">' . $filter['products_filter_name'] . '&nbsp;&raquo;&nbsp;' . $filter['products_select_name'] . '</td>' . "\n" . 
        '            </tr>' . "\n";
      }
      $html_string .=
      '              <tr>' . "\n" . 
      '                <td colspan="3"><table border="0" width="100%" cellspacing="0" cellpadding="2">' . "\n" . 
      '                  <tr>' . "\n" . 
      '                    <td><a href="' . tep_href_link(FILENAME_SEO_ZONES, tep_get_all_get_params(array('action', 'mcpage')) . 'action=list') . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>' . tep_draw_hidden_field('multi_form', 'multi_filter') . '&nbsp;' . tep_image_submit('button_insert.gif', IMAGE_INSERT) . '</td>' . "\n" . 
      '                  </tr>' . "\n" . 
      '                </table></td>' . "\n" . 
      '              </tr>' . "\n" . 
      '            </table></form></td>' . "\n" . 
      '          </tr>' . "\n" . 
      '          <tr>' . "\n" . 
      '            <td><table border="0" width="100%" cellspacing="0" cellpadding="2">' . "\n" . 
      '              <tr>' . "\n" . 
      '                <td class="smallText" valign="top">' . $filter_split->display_count($filter_query_numrows, SEO_PAGE_SPLIT, $this->m_mcpage, TEXT_DISPLAY_NUMBER_OF_ENTRIES) . '</td>' . "\n" . 
      '                <td class="smallText" align="right">' . $filter_split->display_links($filter_query_numrows, SEO_PAGE_SPLIT, MAX_DISPLAY_PAGE_LINKS, $this->m_mcpage, tep_get_all_get_params(array('action', 'mcpage')) . 'action=multi_filter', 'mcpage') . '</td>' . "\n" . 
      '              </tr>' . "\n" . 
      '            </table></td>' . "\n" . 
      '          </tr>' . "\n";
      return $html_string;
    }

    function display_delete_multizone() {
      $html_string = '';
      $filter_query = tep_db_query("select seo_types_name from " . TABLE_SEO_TYPES . " where seo_types_id = '" . (int)$this->m_zID . "'");
      $filter_array = tep_db_fetch_array($filter_query);
      $html_string .= 
      '        <tr>' . "\n" . 
      '          <td class="smallText">' . sprintf(TEXT_DELETE_MULTIZONE_CONFIRM, $filter_array['seo_types_name']) . '</td>' . "\n" . 
      '        </tr>' . "\n" . 
      '        <tr>' . "\n" . 
      '          <td>' . tep_draw_separator('pixel_trans.gif', '100%', '10') . '</td>' . "\n" . 
      '        </tr>' . "\n" . 
      '        <tr>' . "\n" . 
      '          <td valign="top">' . tep_draw_form('rl_confirm', FILENAME_SEO_ZONES, tep_get_all_get_params(array('action')) . 'action=deleteconfirm_multizone', 'post') . '<table border="0" width="100%" cellspacing="0" cellpadding="2">' . "\n" . 
      '            <tr class="dataTableHeadingRow">' . "\n" . 
      '              <td class="dataTableHeadingContent">' . TABLE_HEADING_RANGE . '</td>' . "\n" . 
      '            </tr>' . "\n";
      $rows = 0;
      foreach ($_POST['pc_id'] as $key => $val) {
        $pfs_array = explode('_', $key);
        if( !is_array($pfs_array) || count($pfs_array) != 2 ) {
          continue;
        }
        $delete_query = tep_db_query("select concat(pf.products_filter_name, ' ', ps.products_select_name) as final_name from " . TABLE_SEO_TO_FILTERS . " s2f left join " . TABLE_PRODUCTS_SELECT . " ps on (ps.products_filter_id=s2f.products_filter_id and ps.products_select_id=s2f.products_select_id), " . TABLE_PRODUCTS_FILTERS . " pf where ps.products_select_id <> 0 and pf.products_filter_id = ps.products_filter_id and s2f.products_filter_id = '" . (int)$pfs_array[0] . "' and s2f.products_select_id = '" . (int)$pfs_array[1] . "' order by pf.products_filter_name, ps.products_select_name");

        if( tep_db_num_rows($delete_query) ) {
          $delete_array = tep_db_fetch_array($delete_query);
        } else {
          $delete_array = array('final_name' => 'N/A - ' . $key);
        }
        $rows++;
        $row_class = ($rows%2)?'dataTableRow':'dataTableRowSelected';
        $html_string .= 
        '          <tr class="' . $row_class . '">' . "\n" . 
        '            <td class="dataTableContent">' . tep_draw_hidden_field('pc_id[]', $key) . $delete_array['final_name'] . '</td>' . "\n" . 
        '          </tr>' . "\n";
      }
      if( count($_POST['pc_id']) ) {
        $html_string .= 
        '            <tr>' . "\n" . 
        '              <td colspan="4"><a href="' . tep_href_link(FILENAME_SEO_ZONES, tep_get_all_get_params(array('action')) . 'action=list') . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a> ' . tep_image_submit('button_confirm.gif', IMAGE_CONFIRM) . '</td>' . "\n" . 
        '            </tr>' . "\n";
      }
      $html_string .= 
      '            </table></form></td>' . "\n" . 
      '          </tr>' . "\n";
      return $html_string;
    }
  }
?>