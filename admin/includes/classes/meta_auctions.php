<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2011 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// META-G Text Entries class for Admin
// This is a Bridge for META-G
// Processes text pages generates meta-tag segments.
// Featuring:
// - Auction Entries Listings with Meta-Tags
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
  class meta_auctions extends meta_zones {
    var $error_array;

// class constructor
    function meta_auctions() {
      $this->m_ssID = isset($_GET['ssID'])?$_GET['ssID']:'';
      $this->m_mcpage = isset($_GET['mcpage'])?$_GET['mcpage']:'';
      $this->m_mppage = isset($_GET['mppage'])?$_GET['mppage']:'';
      parent::meta_zones();
    }

    function generate_name($auctions_id) {
      $name = '';
      $name_query = tep_db_query("select auctions_name from " . TABLE_AUCTIONS . " where auctions_id = '" . (int)$auctions_id . "'");
      if( $names_array = tep_db_fetch_array($name_query) ) {
        $name = $names_array['auctions_name'];
        $name =  $this->create_safe_string($name);
      }
      return $name;
    }

    function generate_lexico($index=0) {
      $auction_query = tep_db_query("select auctions_name from " . TABLE_AUCTIONS . "");
      while( $auction_array = tep_db_fetch_array($auction_query) ) {

        $phrase = $this->create_safe_string($auction_array['auctions_name']);
        $md5_key = md5($phrase);
        $check_query = tep_db_query("select count(*) as total from " . TABLE_META_LEXICO . " where meta_lexico_key = '" . tep_db_input(tep_db_prepare_input($md5_key)) . "'");
        $check_array = tep_db_fetch_array($check_query);
        if( !$check_array['total'] ) {
          $sql_data_array = array(
                                  'meta_lexico_key' => tep_db_prepare_input($md5_key),
                                  'meta_lexico_text' => tep_db_prepare_input($phrase)
                                 );
          tep_db_perform(TABLE_META_LEXICO, $sql_data_array);
        }
      }
    }

    function process_action() {
      switch( $this->m_action ) {
        case 'validate':
          return $this->validate();
        case 'validate_confirm':
          return $this->validate_confirm();
        case 'update_multizone':
          $result = parent::validate_array_selection('pc_id'); 
          return $this->update_multizone();
        case 'insert_multientries':
          $result = parent::validate_array_selection('pc_id', 'multi_entries'); 
          return $this->insert_multientries();
        case 'deleteconfirm_multizone':
          $result = parent::validate_array_selection('pc_id'); 
          return $this->deleteconfirm_multizone();
        case 'delete_multizone':
          $result = parent::validate_array_selection('pc_id'); 
          break;
        default:
          return parent::process_action(); 
          break;
      }
    }

    function validate() {
      global $languages_id;
      $this->error_array = array();
      // First pass check for missing products from seo table
      $check_query = tep_db_query("select pd.auctions_id, pd.auctions_name as name, if(s2p.auctions_id, s2p.auctions_id, 0) as missing_id from " . TABLE_AUCTIONS . " pd left join " . TABLE_META_AUCTIONS . " s2p on ((s2p.auctions_id = if(s2p.auctions_id,pd.auctions_id,0))) where pd.language_id = '" . (int)$languages_id . "' order by pd.auctions_id desc");
      while( $check_array = tep_db_fetch_array($check_query) ) {
        if( !$check_array['missing_id'] ) {
          $this->error_array[] = $check_array;
        }
        if( count($this->error_array) >= META_PAGE_SPLIT )
          break;
      }
      // Second pass check for redundant entries in the seo table
      $check_query = tep_db_query("select s2p.auctions_id, if(pd.auctions_id, pd.auctions_name, s2p.meta_name) as name, if(pd.auctions_id, pd.auctions_id, -1) as missing_id from " . TABLE_META_AUCTIONS . " s2p left join " . TABLE_AUCTIONS . " pd on ((s2p.auctions_id = if(pd.auctions_id,pd.auctions_id,0)) and (pd.language_id = if(pd.auctions_id, '" . (int)$languages_id . "', 0))) order by s2p.auctions_id desc");
      while( $check_array = tep_db_fetch_array($check_query) ) {
        if( $check_array['missing_id'] == -1 ) {
          $this->error_array[] = $check_array;
        }
        if( count($this->error_array) >= META_PAGE_SPLIT )
          break;
      }
      return $this->error_array;
    }

    function validate_confirm() {
      foreach($_POST['pc_id'] as $auctions_id => $val) {
        if( $_POST['missing'][$auctions_id] == -1 ) {
          tep_db_query("delete from " . TABLE_META_AUCTIONS . " where meta_types_id = '" . (int)$this->m_zID . "' and auctions_id = '" . (int)$auctions_id . "'");
        } elseif( $_POST['missing'][$auctions_id] == 0 ) {
          $meta_name = $this->generate_name($auctions_id);
          $sql_data_array = array(
                                  'meta_types_id' => (int)$this->m_zID,
                                  'auctions_id' => (int)$auctions_id,
                                  'meta_name' => tep_db_prepare_input($meta_name),
                                  );
          tep_db_perform(TABLE_META_AUCTIONS, $sql_data_array, 'insert');
        }
      }
      tep_redirect(tep_href_link(FILENAME_META_ZONES, tep_get_all_get_params(array('action')) . 'action=validate'));
    }

    function update_multizone() {
      global $languages_id;

      foreach($_POST['pc_id'] as $auctions_id => $val) {

        $meta_title = $_POST['title'][$auctions_id];
        $meta_keywords = $_POST['keywords'][$auctions_id];
        $meta_text = $_POST['text'][$auctions_id];

        $sql_data_array = array(
                                'meta_title' => tep_db_prepare_input($meta_title),
                                'meta_keywords' => tep_db_prepare_input($meta_keywords),
                                'meta_text' => tep_db_prepare_input($meta_text)
                               );

        tep_db_perform(TABLE_META_AUCTIONS, $sql_data_array, 'update', "language_id = '" . (int)$languages_id . "' and auctions_id = '" . (int)$auctions_id . "'");
      }
      tep_redirect(tep_href_link(FILENAME_META_ZONES, tep_get_all_get_params(array('action')) . 'action=list'));
    }


    function insert_multientries() {
      global $languages_id;

      $multi_form = (isset($_POST['multi_form']) ? $_POST['multi_form'] : '');

      switch( $multi_form ) {
        case 'multi_entries':
          $tmp_array = array();
          foreach ($_POST['pc_id'] as $auctions_id=>$val) {
            $multi_query = tep_db_query("select auctions_id, auctions_name, auctions_description from " . TABLE_AUCTIONS . " where auctions_id = '" . (int)$auctions_id . "'");
            if( $multi = tep_db_fetch_array($multi_query) ) {
              $check_query = tep_db_query("select auctions_id from " . TABLE_META_AUCTIONS . " where auctions_id = '" . (int)$multi['auctions_id'] . "'");
              if( tep_db_num_rows($check_query) )
                continue;

              $meta_name = $this->create_safe_string($multi['auctions_name']);

              $meta_keywords = $this->create_keywords_lexico($multi['auctions_description']);
              $meta_text = $this->create_safe_description($multi['auctions_description']);

              $sql_data_array = array(
                                      'auctions_id' => (int)$auctions_id,
                                      'meta_title' => tep_db_prepare_input($meta_name),
                                      'meta_keywords' => tep_db_prepare_input($meta_keywords),
                                      'meta_text' => tep_db_prepare_input($meta_text)
                                     );
              tep_db_perform(TABLE_META_AUCTIONS, $sql_data_array, 'insert');
            }
          }
          tep_db_query("optimize table " . TABLE_META_AUCTIONS . "");
          tep_redirect(tep_href_link(FILENAME_META_ZONES, tep_get_all_get_params(array('action')) . 'action=list'));
          break;
        default:
          break;
      }
    }

    function deleteconfirm_multizone() {
      global $languages_id;

      for($i=0, $j=count($_POST['pc_id']); $i<$j; $i++ ) {
        $auctions_id = $_POST['pc_id'][$i];
        tep_db_query("delete from " . TABLE_META_AUCTIONS . " where auctions_id = '" . (int)$auctions_id . "'");
      }
      tep_redirect(tep_href_link(FILENAME_META_ZONES, tep_get_all_get_params(array('action')) . 'action=list'));
    }


    function display_html() {
      switch( $this->m_action ) {
        case 'validate':
          $result = $this->display_validation();
          break;
        case 'list':
          $result = $this->display_list();
          break;
        case 'multi_entries':
          $result = $this->display_multi_entries();
          break;
        case 'delete_multizone':
          $result = $this->display_delete_multizone();
          break;
        default:
          $result = $this->display_default();
          $result .= $this->display_bottom();
          break;
      }
      return $result;
    }

    function display_validation() {
      $html_string = '';
      $html_string .= 
      '      <tr>' . "\n" . 
      '        <td><hr /></td>' . "\n" . 
      '      </tr>' . "\n";
      if( count($this->error_array) ) {
        $html_string .= 
        '      <tr>' . "\n" . 
        '        <td><table border="0" width="100%" cellspacing="0" cellpadding="2">' . "\n" . 
        '          <tr>' . "\n" . 
        '            <td class="dataTableRowHighBorder" width="16">&nbsp;</td>' . "\n" . 
        '            <td class="smallText"><b>&nbsp;-&nbsp;Product present in the products table but not present in the SEO-G table</b></td>' . "\n" . 
        '          </tr>' . "\n" . 
        '          <tr>' . "\n" . 
        '            <td colspan="2">' . tep_draw_separator('pixel_trans.gif', '100%', '1') . '</td>' . "\n" . 
        '          </tr>' . "\n" . 
        '          <tr>' . "\n" . 
        '            <td class="dataTableRowImpactBorder" width="16">&nbsp;</td>' . "\n" . 
        '            <td class="smallText"><b>&nbsp;-&nbsp;Product present in the SEO-G table but it is not present in the products table</b></td>' . "\n" . 
        '          </tr>' . "\n" . 
        '        </table></td>' . "\n" . 
        '      </tr>' . "\n" .
        '      <tr>' . "\n" . 
        '        <td>' . tep_draw_separator('pixel_trans.gif', '100%', '10') . '</td>' . "\n" . 
        '      </tr>' . "\n";
        $html_string .= 
        '      <tr>' . "\n" . 
        '        <td valign="top">' . tep_draw_form('rl', FILENAME_META_ZONES, 'action=validate_confirm&zID=' . $this->m_zID . '&zpage=' . $this->m_zpage, 'post') . '<table border="0" width="100%" cellspacing="1" cellpadding="3">' . "\n" . 
        '          <tr class="dataTableHeadingRow">' . "\n" . 
        '            <td class="dataTableHeadingContent" width="40"><a href="javascript:void(0)" onClick="copy_checkboxes(document.rl, \'pc_id\')" title="Page Select On/Off" class="menuBoxHeadingLink"><span class="dataTableHeadingContent">' . TABLE_HEADING_SELECT . '</span></a></td>' . "\n" . 
        '            <td class="dataTableHeadingContent">' . TABLE_HEADING_ID . '</td>' . "\n" . 
        '            <td class="dataTableHeadingContent">' . TABLE_HEADING_NAME . '</td>' . "\n" . 
        '            <td class="dataTableHeadingContent">' . TABLE_HEADING_COMMENT . '</td>' . "\n" . 
        '          </tr>' . "\n";
        for($i=0, $j=count($this->error_array); $i<$j; $i++ ) {
          $row_class = ($this->error_array[$i]['missing_id'])?'dataTableRowImpact':'dataTableRowHigh';
          $html_string .= 
          '          <tr class="' . $row_class . '">' . "\n" . 
          '            <td class="dataTableContent">' . tep_draw_checkbox_field('pc_id[' . $this->error_array[$i]['auctions_id'] . ']', 'on', false ) . tep_draw_hidden_field('missing[' . $this->error_array[$i]['auctions_id'] . ']', $this->error_array[$i]['missing_id']) . '</td>' . "\n" . 
          '            <td class="dataTableContent">' . $this->error_array[$i]['auctions_id'] . '</td>' . "\n" . 
          '            <td class="dataTableContent">' . $this->error_array[$i]['name'] . '</td>' . "\n" . 
          '            <td class="dataTableContent">' . (($this->error_array[$i]['missing_id'])?'Missing from Products':'Missing from SEO-G') . '</td>' . "\n" . 
          '          </tr>'  . "\n";
        }
        $html_string .= 
        '          <tr>' . "\n" . 
        '            <td colspan="4"><table border="0" width="100%" cellspacing="0" cellpadding="2">' . "\n" . 
        '              <tr>' . "\n" . 
        '                <td><a href="' . tep_href_link(FILENAME_META_ZONES, 'zpage=' . $this->m_zpage . '&zID=' . $this->m_zID) . '">' . tep_image_button('button_back.gif', IMAGE_BACK) . '</a> ' . tep_image_submit('button_fix_errors.gif', 'Fix Listed Errors') . '</td>' . "\n" . 
        '              </tr>' . "\n" . 
        '            </table></td>' . "\n" . 
        '          </tr>' . "\n" .
        '        </table></form></td>' . "\n" . 
        '      </tr>' . "\n";
      } else {
        $html_string .= 
        '      <tr>' . "\n" . 
        '        <td class="smallText">' . 'No Errors Found' . '</td>' . "\n" . 
        '      </tr>' . "\n" . 
        '      <tr>' . "\n" . 
        '        <td colspan="2">' . tep_draw_separator('pixel_trans.gif', '100%', '4') . '</td>' . "\n" . 
        '      </tr>' . "\n" . 
        '      <tr>' . "\n" . 
        '        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">' . "\n" . 
        '          <tr>' . "\n" . 
        '            <td><a href="' . tep_href_link(FILENAME_META_ZONES, 'zpage=' . $this->m_zpage . '&zID=' . $this->m_zID) . '">' . tep_image_button('button_back.gif', IMAGE_BACK) . '</a></td>' . "\n" . 
        '          </tr>' . "\n" . 
        '        </table></td>' . "\n" . 
        '      </tr>' . "\n";
      }
      return $html_string;
    }

// Categories/Products List
    function display_list() {
      global $languages_id;
      //$auction_array = $this->get_category_tree_paths();
      $html_string = '';
      $rows = 0;
      //$zones_query_raw = "select s2p.auctions_id, s2p.meta_types_id, if(pd.auctions_id, pd.auctions_name, 'N/A') as final_name, s2p.meta_name from " . TABLE_META_AUCTIONS . " s2p left join " . TABLE_AUCTIONS . " pd on ((s2p.auctions_id = if(pd.auctions_id,pd.auctions_id,0)) and (pd.language_id = if(pd.auctions_id, '" . (int)$languages_id . "', 0))) where s2p.meta_types_id = '" . (int)$this->m_zID . "' order by pd.auctions_name";
      $zones_query_raw = "select * from " . TABLE_META_AUCTIONS . " order by auctions_id desc";
      $zones_split = new splitPageResults($this->m_spage, META_PAGE_SPLIT, $zones_query_raw, $zones_query_numrows);

      if( $zones_query_numrows > 0 ) {
        $html_string .= 
        '      <tr>' . "\n" . 
        '        <td valign="top">' . tep_draw_form('rl', FILENAME_META_ZONES, 'action=delete_multizone&zID=' . $this->m_zID . '&zpage=' . $this->m_zpage . '&spage=' . $this->m_spage, 'post') . '<table border="0" width="100%" cellspacing="1" cellpadding="3">' . "\n";

        if(empty($this->saction)) {
          $html_string .= 
          '          <tr>' . "\n" . 
          '            <td colspan="5"><table border="0" width="100%" cellspacing="0" cellpadding="2">' . "\n" . 
          '              <tr>' . "\n" . 
          '                <td><a href="' . tep_href_link(FILENAME_META_ZONES, 'zpage=' . $this->m_zpage . '&zID=' . $this->m_zID) . '">' . tep_image_button('button_back.gif', IMAGE_BACK) . '</a> ' . tep_image_submit('button_update.gif', TEXT_UPDATE_MULTIZONE, 'onclick="this.form.action=' . '\'' . tep_href_link(FILENAME_META_ZONES, tep_get_all_get_params(array('action')) . 'action=update_multizone') . '\'' . '"') . ' ' . tep_image_submit('button_delete.gif', TEXT_DELETE_MULTIZONE, 'onclick="this.form.action=' . '\'' . tep_href_link(FILENAME_META_ZONES, tep_get_all_get_params(array('action')) . 'action=delete_multizone') . '\'' . '"') . '</td>' . "\n" . 
          '              </tr>' . "\n" . 
          '            </table></td>' . "\n" . 
          '          </tr>' . "\n";
        }
        $html_string .= 
        '          <tr class="dataTableHeadingRow">' . "\n" . 
        '            <td class="dataTableHeadingContent"><a href="javascript:void(0)" onClick="copy_checkboxes(document.rl, \'pc_id\')" title="Page Select On/Off" class="menuBoxHeadingLink"><span class="dataTableHeadingContent">' . TABLE_HEADING_SELECT . '</span></a></td>' . "\n" . 
        '            <td class="dataTableHeadingContent">' . TABLE_HEADING_TITLE . '</td>' . "\n" . 
        '            <td class="dataTableHeadingContent">' . TABLE_HEADING_KEYWORDS . '</td>' . "\n" . 
        '            <td class="dataTableHeadingContent">' . TABLE_HEADING_DESCRIPTION . '</td>' . "\n" . 
        '          </tr>' . "\n";
        $zones_query = tep_db_query($zones_query_raw);
        $bCheck = false;
        while ($zones = tep_db_fetch_array($zones_query)) {
          $text_query = tep_db_query("select auctions_name from " . TABLE_AUCTIONS . " where auctions_id = '" . (int)$zones['auctions_id'] . "'");
          if( $text_array = tep_db_fetch_array($text_query) ) {
            $final_name = '<a href="' . tep_href_link(FILENAME_AUCTIONS, 'aID=' . $zones['auctions_id'] . '&action=edit') . '"><b>' . $text_array['auctions_name'] . '</a></b>';
          } else {
            $final_name = '<b>N/A - Error (Delete Entry)</b>';
          }
          $rows++;
          $row_class = ($rows%2)?'dataTableRow':'dataTableRowSelected';
          $html_string .= 
          '          <tr class="' . $row_class . '">' . "\n" . 
          '            <td class="dataTableContent">' . tep_draw_checkbox_field('pc_id[' . $zones['auctions_id'] . ']', ($bCheck?'on':''), $bCheck ) . '</td>' . "\n" . 
          '            <td class="dataTableContent">Text Page:&nbsp;<b>' . $final_name . '</b><br />' . tep_draw_input_field('title[' . $zones['auctions_id'] . ']', $zones['meta_title'], 'style="width: 300px"') . '</td>' . "\n" . 
          '            <td class="dataTableContent">' . tep_draw_textarea_field('keywords[' . $zones['auctions_id'] . ']', 'soft', '40','2', $zones['meta_keywords']) . '</td>' . "\n" . 
          '            <td class="dataTableContent">' . tep_draw_textarea_field('text[' . $zones['auctions_id'] . ']', 'soft', '40','2', $zones['meta_text']) . '</td>' . "\n" . 
          '          </tr>'  . "\n";
        }
        if(empty($this->saction)) {
          $html_string .= 
          '          <tr>' . "\n" . 
          '            <td colspan="5"><table border="0" width="100%" cellspacing="0" cellpadding="2">' . "\n" . 
          '              <tr>' . "\n" . 
          '                <td><a href="' . tep_href_link(FILENAME_META_ZONES, 'zpage=' . $this->m_zpage . '&zID=' . $this->m_zID) . '">' . tep_image_button('button_back.gif', IMAGE_BACK) . '</a> ' . tep_image_submit('button_update.gif', TEXT_UPDATE_MULTIZONE, 'onclick="this.form.action=' . '\'' . tep_href_link(FILENAME_META_ZONES, tep_get_all_get_params(array('action')) . 'action=update_multizone') . '\'' . '"') . ' ' . tep_image_submit('button_delete.gif', TEXT_DELETE_MULTIZONE, 'onclick="this.form.action=' . '\'' . tep_href_link(FILENAME_META_ZONES, tep_get_all_get_params(array('action')) . 'action=delete_multizone') . '\'' . '"') . '</td>' . "\n" . 
          '              </tr>' . "\n" . 
          '            </table></td>' . "\n" . 
          '          </tr>' . "\n";
        }
        $html_string .= 
        '          </table></form></td>' . "\n" . 
        '        </tr>' . "\n" . 
        '        <tr>' . "\n" . 
        '          <td><table border="0" width="100%" cellspacing="0" cellpadding="2">' . "\n" . 
        '            <tr>' . "\n" . 
        '              <td class="smallText" valign="top">' . $zones_split->display_count($zones_query_numrows, META_PAGE_SPLIT, $this->m_spage, TEXT_DISPLAY_NUMBER_OF_ENTRIES) . '</td>' . "\n" . 
        '              <td class="smallText" align="right">' . $zones_split->display_links($zones_query_numrows, META_PAGE_SPLIT, MAX_DISPLAY_PAGE_LINKS, $this->m_spage, tep_get_all_get_params(array('action', 'spage')) . 'action=list', 'spage') . '</td>' . "\n" . 
        '            </tr>' . "\n" . 
        '          </table></td>' . "\n" . 
        '        </tr>' . "\n";
      } else {
        $html_string .= 
        '        <tr>' . "\n" . 
        '          <td class="smallText">' . TEXT_INFO_NO_ENTRIES . '</td>' . "\n" . 
        '        </tr>' . "\n";
      }
      if (empty($this->saction)) {
        $html_string .= 
        '        <tr>' . "\n" . 
        '          <td>' . tep_draw_separator('pixel_trans.gif', '100%', '10') . '</td>' . "\n" . 
        '        </tr>' . "\n" . 
        '        <tr>' . "\n" . 
        '          <td><table border="0" width="100%" cellspacing="0" cellpadding="0">' . "\n" . 
        '            <tr>' . "\n" . 
        '              <td><table border="0" width="100%" cellspacing="0" cellpadding="2">' . "\n" . 
        '                <tr>' . "\n" . 
        '                  <td nowrap><a href="' . tep_href_link(FILENAME_META_ZONES, 'zpage=' . $this->m_zpage . '&zID=' . $this->m_zID) . '">' . tep_image_button('button_back.gif', IMAGE_BACK) . '</a> <a href="' . tep_href_link(FILENAME_META_ZONES, tep_get_all_get_params(array('action')) . 'action=multi_entries') . '">' . tep_image_button('button_entries.gif', TEXT_SWITCH_ENTRIES) . '</a></td>' . "\n" . 
        '                  <td nowrap width="90%">&nbsp;</td>' . "\n" . 
        '                </tr>' . "\n" . 
        '              </table></td>' . "\n" . 
        '            </tr>' . "\n" . 
        '            <tr>' . "\n" . 
        '              <td><hr /></td>' . "\n" . 
        '            </tr>' . "\n" . 
        '          </table></td>' . "\n" . 
        '        </tr>' . "\n"; 
      }
      return $html_string;
    }


    function display_multi_entries() {
      $html_string = '';
      $html_string .=
      '        <tr>' . "\n" . 
      '          <td class="smallText">' . TEXT_SELECT_MULTIENTRIES . '</td>' . "\n" . 
      '        </tr>' . "\n" . 
      '        <tr>' . "\n" . 
      '          <td>' . tep_draw_separator('pixel_trans.gif', '100%', '10') . '</td>' . "\n" . 
      '        </tr>' . "\n" . 
      '        <tr>' . "\n" . 
      '          <td valign="top">' . tep_draw_form('mc', FILENAME_META_ZONES, tep_get_all_get_params(array('action')) . 'action=insert_multientries', 'post') . '<table border="0" width="100%" cellspacing="1" cellpadding="3">' . "\n" . 
      '            <tr>' . "\n" . 
      '              <td colspan="3"><table border="0" width="100%" cellspacing="0" cellpadding="2">' . "\n" . 
      '                <tr>' . "\n" . 
      '                  <td><a href="' . tep_href_link(FILENAME_META_ZONES, tep_get_all_get_params(array('action', 'mcpage')) . 'action=list') . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>' . tep_draw_hidden_field('multi_form', 'multi_entries') . '&nbsp;' . tep_image_submit('button_insert.gif', IMAGE_INSERT) . '</td>' . "\n" . 
      '                </tr>' . "\n" . 
      '              </table></td>' . "\n" . 
      '            </tr>' . "\n" . 
      '            <tr class="dataTableHeadingRow">' . "\n" . 
      '              <td class="dataTableHeadingContent"><a href="javascript:void(0)" onClick="copy_checkboxes(document.mc, \'pc_id\')" title="Page Select On/Off" class="menuBoxHeadingLink"><span class="dataTableHeadingContent">' . TABLE_HEADING_SELECT . '</span></a></td>' . "\n" . 
      '              <td class="dataTableHeadingContent">' . TABLE_HEADING_ENTRIES . '</td>' . "\n" . 
      '            </tr>' . "\n"; 
      $rows = 0;
      //$auction_query_raw = "select auctions_id, auctions_name from " . TABLE_AUCTIONS . " order by auctions_name";
      $auction_query_raw = "select m.auctions_id, m.auctions_name, if(mm.auctions_id, '1', '0') as checkbox from " . TABLE_AUCTIONS . " m left join " . TABLE_META_AUCTIONS . " mm on ((mm.auctions_id = if(mm.auctions_id, m.auctions_id,0))) order by m.auctions_id, m.auctions_name";
      $auction_split = new splitPageResults($this->m_mcpage, META_PAGE_SPLIT, $auction_query_raw, $auction_query_numrows, 'm.auctions_id');
      $auction_query = tep_db_query($auction_query_raw);
      $bCheck = false;
      while ($auction = tep_db_fetch_array($auction_query)) {
        $bCheck = ($auction['checkbox'] == '1')?true:false;
        $rows++;
        $row_class = ($rows%2)?'dataTableRow':'dataTableRowSelected';
        if($bCheck)
          $row_class = 'dataTableRowGreen';

        $html_string .=
        '            <tr class="' . $row_class . '">' . "\n" . 
        '              <td class="dataTableContent">' . ($bCheck?'Included':tep_draw_checkbox_field('pc_id[' . $auction['auctions_id'] . ']')) . '</td>' . "\n" . 
        '              <td class="dataTableContent"><a href="' . tep_href_link(FILENAME_AUCTIONS, 'aID=' . $auction['auctions_id'] . '&action=edit') . '"><b>' . $auction['auctions_name'] . '</b></a></td>' . "\n" . 
        '            </tr>' . "\n";
      }
      $html_string .=
      '            <tr>' . "\n" . 
      '              <td colspan="3"><table border="0" width="100%" cellspacing="0" cellpadding="2">' . "\n" . 
      '                <tr>' . "\n" . 
      '                  <td><a href="' . tep_href_link(FILENAME_META_ZONES, tep_get_all_get_params(array('action', 'mcpage')) . 'action=list') . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>' . tep_draw_hidden_field('multi_form', 'multi_entries') . '&nbsp;' . tep_image_submit('button_insert.gif', IMAGE_INSERT) . '</td>' . "\n" . 
      '                </tr>' . "\n" . 
      '              </table></td>' . "\n" . 
      '            </tr>' . "\n" . 
      '          </table></form></td>' . "\n" . 
      '        </tr>' . "\n" . 
      '        <tr>' . "\n" . 
      '          <td><table border="0" width="100%" cellspacing="0" cellpadding="2">' . "\n" . 
      '            <tr>' . "\n" . 
      '              <td class="smallText" valign="top">' . $auction_split->display_count($auction_query_numrows, META_PAGE_SPLIT, $this->m_mcpage, TEXT_DISPLAY_NUMBER_OF_ENTRIES) . '</td>' . "\n" . 
      '              <td class="smallText" align="right">' . $auction_split->display_links($auction_query_numrows, META_PAGE_SPLIT, MAX_DISPLAY_PAGE_LINKS, $this->m_mcpage, tep_get_all_get_params(array('action', 'mcpage')) . 'action=multi_entries', 'mcpage') . '</td>' . "\n" . 
      '            </tr>' . "\n" . 
      '          </table></td>' . "\n" . 
      '        </tr>' . "\n";
      return $html_string;
    }

    function display_delete_multizone() {
      global $languages_id;

      if( !isset($_POST['pc_id']) || !is_array($_POST['pc_id']) ) {
        return '';
      }

      $html_string = '';
      $attr_query = tep_db_query("select meta_types_name from " . TABLE_META_TYPES . " where meta_types_id = '" . (int)$this->m_zID . "'");
      $attr_array = tep_db_fetch_array($attr_query);
      $html_string .= 
      '        <tr>' . "\n" . 
      '          <td class="smallText">' . sprintf(TEXT_DELETE_MULTIZONE_CONFIRM, $attr_array['meta_types_name']) . '</td>' . "\n" . 
      '        </tr>' . "\n" . 
      '        <tr>' . "\n" . 
      '          <td>' . tep_draw_separator('pixel_trans.gif', '100%', '10') . '</td>' . "\n" . 
      '        </tr>' . "\n" . 
      '        <tr>' . "\n" . 
      '          <td valign="top">' . tep_draw_form('rl_confirm', FILENAME_META_ZONES, tep_get_all_get_params(array('action')) . 'action=deleteconfirm_multizone', 'post') . '<table border="0" width="100%" cellspacing="1" cellpadding="3">' . "\n" . 
      '            <tr class="dataTableHeadingRow">' . "\n" . 
      '              <td class="dataTableHeadingContent">' . TABLE_HEADING_PRODUCTS . '</td>' . "\n" . 
      '            </tr>' . "\n";
      $rows = 0;

      foreach($_POST['pc_id'] as $key => $value) {
        $auctions_id = $key;
        $delete_query = tep_db_query("select auctions_id, meta_title from " . TABLE_META_AUCTIONS . " where auctions_id = '" . (int)$key . "' order by meta_title, auctions_id");
        if( $delete_array = tep_db_fetch_array($delete_query) ) {
          $rows++;
          $row_class = ($rows%2)?'dataTableRow':'dataTableRowSelected';
          $html_string .= 
          '          <tr class="' . $row_class . '">' . "\n" . 
          '            <td class="dataTableContent">' . tep_draw_hidden_field('pc_id[]', $delete_array['auctions_id']) . $delete_array['meta_title'] . '</td>' . "\n" . 
          '          </tr>' . "\n";
        }
      }
      if( count($_POST['pc_id']) ) {
        $html_string .= 
        '            <tr>' . "\n" . 
        '              <td colspan="3"><a href="' . tep_href_link(FILENAME_META_ZONES, tep_get_all_get_params(array('action')) . 'action=list') . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a> ' . tep_image_submit('button_confirm.gif', IMAGE_CONFIRM) . '</td>' . "\n" . 
        '            </tr>' . "\n";
      }
      $html_string .= 
      '          </table></form></td>' . "\n" . 
      '        </tr>' . "\n";
      return $html_string;
    }
  }
?>