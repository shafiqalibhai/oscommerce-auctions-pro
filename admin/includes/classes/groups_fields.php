<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2008 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Catalog: Groups Front class
// Manages the Group fields for products
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
  class groups_fields {
    var $group_array;
// class constructor
    function groups_fields() {
      $this->group_array = array();
    }

    function get_product_groups($product_id) {
      if( isset($this->group_array[$product_id]) ) {
        return $this->group_array[$product_id];
      }
      $group_array = array();
      $group_query_raw = "select p2gf.group_fields_id, gf.group_fields_name, gf.group_fields_description, gf.limit_id from " . TABLE_PRODUCTS_TO_GROUP_FIELDS . " p2gf left join " . TABLE_GROUP_FIELDS . " gf on (gf.group_fields_id=p2gf.group_fields_id) where p2gf.products_id = '" . (int)$product_id . "' order by gf.sort_id";
      tep_query_to_array($group_query_raw, $group_array);
      if( count($group_array) ) {
        $this->group_array[$product_id] = $group_array;
      }
      return $group_array;
    }


    function get_price_from_group($ids_array) {
      $total = 0;
      if( !is_array($ids_array) || !count($ids_array) ) {
        return $total;
      }
      $tmp_array = array_keys($ids_array);
      $values_query = tep_db_query("select group_values_price from " . TABLE_GROUP_VALUES . " where group_values_id in ('" . implode("','", $tmp_array) . "') and status_id = '1'");
      while($values_array = tep_db_fetch_array($values_query)) {
        $total += $values_array['group_values_price'];
      }
      return $total;
    }

    function get_group_prices($ids_array) {
      if( !is_array($ids_array) || !count($ids_array) ) {
        return $total;
      }
      $prices_array = array();
      $tmp_array = array_keys($ids_array);
      $values_query = tep_db_query("select group_values_id, group_values_price from " . TABLE_GROUP_VALUES . " where group_values_id in ('" . implode("','", $tmp_array) . "') and status_id = '1'");
      while($values_array = tep_db_fetch_array($values_query)) {
        $prices_array[$values_array['group_values_id']] = $values_array['group_values_price'];
      }
      return $prices_array;
    }

    function get_names_from_group($ids_array) {
      $names_array = array();
      if( !is_array($ids_array) || !count($ids_array) ) {
        return $names_array;
      }
      $tmp_array = array_keys($ids_array);
      $values_query = tep_db_query("select gv.group_values_id, gv.group_values_name, go.group_options_name from " . TABLE_GROUP_VALUES . " gv left join " . TABLE_GROUP_OPTIONS . " go on (go.group_options_id=gv.group_options_id) where gv.group_values_id in ('" . implode("','", $tmp_array) . "') order by gv.group_fields_id, go.sort_id, gv.sort_id");
      while($values_array = tep_db_fetch_array($values_query)) {
        $names_array[$values_array['group_values_id']] = $values_array['group_options_name'] . ': ' . $values_array['group_values_name'];
      }
      return $names_array;
    }

  }
?>
