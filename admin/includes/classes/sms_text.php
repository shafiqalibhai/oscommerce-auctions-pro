<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2012 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Catalog: SMS Text Marketer class
// http://www.textmarketer.co.uk/developers/restful-api.htm
//----------------------------------------------------------------------------
// I-Metrics Layer
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
  class sms_text {
    // Compatibility Constructor
    function sms_text() {

      $this->modes_array = array(
         'live' => 'https://www.textmarketer.biz/services/rest/',
         'test' => 'http://sandbox.textmarketer.biz/services/rest/',
      );
      $this->url = $this->modes_array[SMS_TEXT_MODE];
    }

    function show_error($xml_array) {
      if( isset($xml_array['errors']) ) {
        echo 'Errors Detected<br />';
      } else {
        echo 'Invalid Response<br />';
      }
      echo '<pre>';
      var_dump($xml_array);
      echo '</pre>';

    tep_exit();
    }

    function command($service, $params_array = array(), $method='GET') {
      $data_array = array(
        'username' => SMS_TEXT_USERNAME,
        'password' => SMS_TEXT_PASSWORD,
        'apiClient' => 'tm-php-1.1',
      );

      if( !empty($params_array) ) {
        $data_array = array_merge($data_array, $params_array);
      }
      $url = $this->url . $service;

      $ch = curl_init();
      if( $method == 'GET' ) {
        $url .= '?' . tep_params_to_string($data_array);
      } else {
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_array);
      }
      curl_setopt($ch, CURLOPT_URL, $url);

      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

      curl_setopt($ch, CURLOPT_TIMEOUT, 10);

      $result = curl_exec($ch);
      curl_close($ch);

      $cxml = new xml_parse();
      $xml_array = $cxml->parse($result);
      return $xml_array['response'];
    }

    function get_available_credit() {
      $result = false;

      $xml_array = $this->command('credits');
      if( isset($xml_array['credits']) ) {
        $result = $xml_array['credits'];
      } else {
        $this->show_error($xml_array);
      }
      return $result;
    }

    function send_message($msg, $target) {
      $result = false;

      $params_array = array(
        'message' => utf8_encode($msg),
        'mobile_number' => $target,
        'originator' => SMS_TEXT_STORE_NUMBER,
      );

      $xml_array = $this->command('sms', $params_array, 'POST');
      if( !isset($xml_array['status']) || strtolower($xml_array['status']) != 'sent' ) {
        $xml_array['errors'] = 'Not Sent';
      }
      return $xml_array;
    }

  }
?>
