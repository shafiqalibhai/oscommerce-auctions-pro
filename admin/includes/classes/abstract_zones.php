<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2007 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Abstract Zones root class for osCommerce Admin
// Controls relationships among products, categories, customers etc.
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/

  class abstract_zones {
   var $m_zID, $m_zpage, $m_saction, $m_action, $m_zInfo, $m_sID, $m_spage;
// class constructor
    function abstract_zones() {
      $this->m_zID = isset($_GET['zID'])?$_GET['zID']:'';
      $this->m_zpage = isset($_GET['zpage'])?$_GET['zpage']:'';
      $this->m_saction = isset($_GET['saction'])?$_GET['saction']:'';
      $this->m_action = isset($_GET['action'])?$_GET['action']:'';
      $this->m_sID = isset($_GET['sID'])?$_GET['sID']:'';
      $this->m_spage = isset($_GET['spage'])?$_GET['spage']:'';
    }

    function validate_array_selection($entity, $action='list') {
      global $messageStack;
      if( !isset($_POST[$entity]) || !is_array($_POST[$entity]) || !count($_POST[$entity]) ) {
        $messageStack->add_session(WARNING_NOTHING_SELECTED, 'warning');
        tep_redirect(tep_href_link(FILENAME_ABSTRACT_ZONES, tep_get_all_get_params(array('action')) . 'action=' . $action));
      }
    }

    function get_categories_string($categories_id) {
      $product_categories = tep_generate_category_path($categories_id, 'category', '', 0, false);
      $product_categories_string = '';
      for ($i = 0, $n = sizeof($product_categories); $i < $n; $i++) {
        $category_path = '';
        for ($j = 0, $k = sizeof($product_categories[$i]); $j < $k; $j++) {
          $category_path .= $product_categories[$i][$j]['text'] . '&nbsp;&raquo;&nbsp;';
        }
        $product_categories_string .= $category_path;
      }

      if( strlen($product_categories_string) > 19 )
        $product_categories_string = substr($product_categories_string, 0, -19);
      return $product_categories_string;
    }

    function get_category_tree_paths() {
      global $languages_id;
      $categories_query = tep_db_query("select distinct p2c.categories_id as id, cd.categories_name as text from " . TABLE_PRODUCTS_TO_CATEGORIES . " p2c left join " . TABLE_CATEGORIES_DESCRIPTION . " cd on (cd.categories_id=p2c.categories_id) where cd.language_id = '" . (int)$languages_id . "' order by p2c.categories_id, cd.categories_name");
      $categories_array = array();

      while( $categories = tep_db_fetch_array($categories_query) ) {
        $product_categories = tep_generate_category_path($categories['id'], 'category', '', 0, false);
        $product_categories_string = '';
        for ($i = 0, $n = sizeof($product_categories); $i < $n; $i++) {
          $category_path = '';
          for ($j = 0, $k = sizeof($product_categories[$i]); $j < $k; $j++) {
            $category_path .= $product_categories[$i][$j]['text'] . '&nbsp;&raquo;&nbsp;';
          }
          $product_categories_string .= $category_path;
        }
        $product_categories_string = substr($product_categories_string, 0, -19);
        $categories_array[$product_categories_string] = array(
                                   'id' => $categories['id'],
                                   'text' => $product_categories_string
                                 );
      }
      ksort($categories_array, SORT_STRING);
      $categories_array = array_values($categories_array);
      return $categories_array;
    }

    function process_action() {
      switch( $this->m_action ) {
        case 'insert_zone':
          return $this->insert_zone();
        case 'save_zone':
          return $this->save_zone();
        case 'deleteconfirm_zone':
          return $this->deleteconfirm_zone();
        default:
          break;
      }
    }

    function deleteconfirm_zone() {
      tep_db_query("delete from " . TABLE_SEO_TO_ABSTRACT . " where abstract_zone_id = '" . (int)$this->m_zID . "'");
      tep_db_query("delete from " . TABLE_ABSTRACT_ZONES . " where abstract_zone_id = '" . (int)$this->m_zID . "'");
    }

    function process_saction() {
      switch( $this->m_saction ) {
        default:
          break;
      }
    }

    function insert_zone() {
      $abstract_zone_name = tep_db_prepare_input($_POST['abstract_zone_name']);
      $abstract_zone_desc = tep_db_prepare_input($_POST['abstract_zone_desc']);
      $abstract_types_id = tep_db_prepare_input($_POST['abstract_types_id']);

      tep_db_query("insert into " . TABLE_ABSTRACT_ZONES . " (abstract_zone_name, abstract_zone_desc, abstract_types_id, date_added) values ('" . tep_db_input($abstract_zone_name) . "', '" . tep_db_input($abstract_zone_desc) . "', '" . (int)tep_db_input($abstract_types_id) . "', now())");
      $new_zone_id = tep_db_insert_id();
      tep_redirect(tep_href_link(FILENAME_ABSTRACT_ZONES, tep_get_all_get_params(array('action', 'zID')) . 'zID=' . $new_zone_id));
    }

    function save_zone() {
      $abstract_zone_name = tep_db_prepare_input($_POST['abstract_zone_name']);
      $abstract_zone_desc = tep_db_prepare_input($_POST['abstract_zone_desc']);

      tep_db_query("update " . TABLE_ABSTRACT_ZONES . " set abstract_zone_name = '" . tep_db_input($abstract_zone_name) . "', abstract_zone_desc = '" . tep_db_input($abstract_zone_desc) . "', last_modified = now() where abstract_zone_id = '" . (int)$this->m_zID . "'");

      tep_redirect(tep_href_link(FILENAME_ABSTRACT_ZONES, 'zpage=' . $this->m_zpage . '&zID=' . $this->m_zID));
    }

    function save_sub() {
      tep_redirect(tep_href_link(FILENAME_ABSTRACT_ZONES));
    }

    function redirect_default() {
      tep_redirect(tep_href_link(FILENAME_ABSTRACT_ZONES));
    }


    function get_zone_name($abstract_zone_id, $default_zone='Unknown Zone') {
      $zone_query = tep_db_query("select abstract_zone_name from " . TABLE_ABSTRACT_ZONES . " where abstract_zone_id = '" . (int)$abstract_zone_id . "'");
      if (tep_db_num_rows($zone_query)) {
        $zone = tep_db_fetch_array($zone_query);
        return $zone['abstract_zone_name'];
      } else {
        return $default_zone;
      }
    }

    function get_zone_type($abstract_zone_id, $default_zone='Unknown Type') {
      $zone_query = tep_db_query("select at.abstract_types_id, at.abstract_types_name from " . TABLE_ABSTRACT_TYPES . " at left join " . TABLE_ABSTRACT_ZONES . " az on (az.abstract_types_id=at.abstract_types_id) where az.abstract_zone_id = '" . (int)$abstract_zone_id . "'");
      if (tep_db_num_rows($zone_query)) {
        $zone = tep_db_fetch_array($zone_query);
        return $zone['abstract_types_name'];
      } else {
        return $default_zone;
      }
    }  

    function display_html() {
      $html_string = '';
      if (!$this->m_action) {
        $html_string = $this->display_default();
        if( !tep_not_null($this->m_zID) ) {

          $html_string .= 
          '             <tr>' . "\n" . 
          '               <td align="right" colspan="2"><a href="' . tep_href_link(FILENAME_ABSTRACT_ZONES, 'zpage=' . $this->m_zpage . '&action=new_zone') . '">' . tep_image_button('button_insert.gif', IMAGE_INSERT) . '</a></td>' . "\n" . 
          '            </tr>' . "\n";

/*
          $html_string .= 
          '             <tr>' . "\n" . 
          '               <td align="right" colspan="2"><a href="' . tep_href_link(FILENAME_ABSTRACT_ZONES, 'zpage=' . $this->m_zpage . '&zID=' . $this->m_zInfo->abstract_zone_id . '&action=new_zone') . '">' . tep_image_button('button_insert.gif', IMAGE_INSERT) . '</a></td>' . "\n" . 
          '            </tr>' . "\n";
*/
        }
      }
      return $html_string;
    }

    function display_default() {
      $html_string = '';
      $html_string .= 
      '        <tr>' . "\n" . 
      '          <td valign="top"><table border="0" width="100%" cellspacing="1" cellpadding="3">' . "\n" . 
      '            <tr class="dataTableHeadingRow">' . "\n" . 
      '              <td class="dataTableHeadingContent">' . TABLE_HEADING_ABSTRACT_ZONES . '</td>' . "\n" . 
      '              <td class="dataTableHeadingContent">' . TABLE_HEADING_ABSTRACT_TYPE . '</td>' . "\n" . 
      '              <td class="dataTableHeadingContent" align="right">' . TABLE_HEADING_ACTION . '&nbsp;</td>' . "\n" . 
      '            </tr>' . "\n";
      $zones_query_raw = "select az.abstract_zone_id, az.abstract_zone_name, az.abstract_zone_desc, az.last_modified, az.date_added, at.abstract_types_id, at.abstract_types_name, at.abstract_types_class, at.abstract_types_table from " . TABLE_ABSTRACT_ZONES . " az left join " . TABLE_ABSTRACT_TYPES . " at on (az.abstract_types_id=at.abstract_types_id) order by at.abstract_types_id, az.abstract_zone_name";
      $zones_split = new splitPageResults($this->m_zpage, ABSTRACT_PAGE_SPLIT, $zones_query_raw, $zones_query_numrows);
      $zones_query = tep_db_query($zones_query_raw);
      while ($zones = tep_db_fetch_array($zones_query)) {

        if( (!tep_not_null($this->m_zID) || (tep_not_null($this->m_zID) && ($this->m_zID == $zones['abstract_zone_id']))) && !isset($this->m_zInfo) && (substr($this->m_action, 0, 3) != 'new')) {
          $this->m_zInfo = new objectInfo($zones);
          $this->m_zID = $this->m_zInfo->abstract_zone_id;
        }
        if (isset($this->m_zInfo) && is_object($this->m_zInfo) && ($zones['abstract_zone_id'] == $this->m_zInfo->abstract_zone_id)) {
          $html_string .= 
          '          <tr id="defaultSelected" class="dataTableRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . tep_href_link(FILENAME_ABSTRACT_ZONES, 'zpage=' . $this->m_zpage . '&spage=1' . '&zID=' . $this->m_zInfo->abstract_zone_id . '&action=list') . '\'">' . "\n";
        } else {
          $html_string .= 
          '          <tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . tep_href_link(FILENAME_ABSTRACT_ZONES, 'zpage=' . $this->m_zpage . '&zID=' . $zones['abstract_zone_id']) . '\'">' . "\n";
        }
        $html_string .= 
        '              <td class="dataTableContent"><a href="' . tep_href_link(FILENAME_ABSTRACT_ZONES, 'zpage=' . $this->m_zpage . '&zID=' . $zones['abstract_zone_id'] . '&action=list') . '">' . tep_image(DIR_WS_ICONS . 'folder.gif', ICON_FOLDER) . '</a>&nbsp;' . $zones['abstract_zone_name'] . '</td>' . "\n" . 
        '              <td class="dataTableContent">' . $zones['abstract_types_name'] . '</td>' . "\n" . 
        '              <td class="dataTableContent" align="right">';
        if (isset($this->m_zInfo) && is_object($this->m_zInfo) && ($zones['abstract_zone_id'] == $this->m_zInfo->abstract_zone_id) && tep_not_null($this->m_zID) ) { 
          $html_string .= tep_image(DIR_WS_IMAGES . 'icon_arrow_right.gif'); 
        } else { 
          $html_string .= '<a href="' . tep_href_link(FILENAME_ABSTRACT_ZONES, 'zpage=' . $this->m_zpage . '&zID=' . $zones['abstract_zone_id']) . '">' . tep_image(DIR_WS_IMAGES . 'icon_info.gif', IMAGE_ICON_INFO) . '</a>'; 
        } 
        $html_string .= '&nbsp;</td>' . "\n" . 
        '            </tr>' . "\n";
      }
      $html_string .= 
      '              <tr>' . "\n" . 
      '                <td colspan="3"><table border="0" width="100%" cellspacing="0" cellpadding="2">' . "\n" . 
      '                  <tr>' . "\n" . 
      '                    <td class="smallText">' . $zones_split->display_count($zones_query_numrows, ABSTRACT_PAGE_SPLIT, $this->m_zpage, TEXT_DISPLAY_NUMBER_OF_ABSTRACT_ZONES) . '</td>' . "\n" . 
      '                    <td class="smallText" align="right">' . $zones_split->display_links($zones_query_numrows, ABSTRACT_PAGE_SPLIT, MAX_DISPLAY_PAGE_LINKS, $this->m_zpage, '', 'zpage') . '</td>' . "\n" . 
      '                  </tr>' . "\n" . 
      '                </table></td>' . "\n" . 
      '              </tr>' . "\n" . 
      '            </table></td>' . "\n" . 
      '          </tr>' . "\n";
      return $html_string;
    }

    function display_right_box() {
      $html_string = '';
      switch( $this->m_action ) {
        case 'list':
          break;
        case 'new_zone':
          $heading[] = array('text' => '<b>' . TEXT_INFO_HEADING_NEW_ZONE . '</b>');

          $azone_array = array();
          $azone_query = "select at.abstract_types_id as id, at.abstract_types_name as text from " . TABLE_ABSTRACT_TYPES . " at where at.abstract_types_status = '1' order by at.sort_order";
          tep_query_to_array($azone_query, $azone_array);
          $contents = array('form' => tep_draw_form('zones', FILENAME_ABSTRACT_ZONES, 'zpage=' . $this->m_zpage . '&zID=' . $this->m_zID . '&action=insert_zone'));
          $contents[] = array('text' => TEXT_INFO_NEW_ZONE_INTRO);
          $contents[] = array('text' => '<br>' . TEXT_INFO_ZONE_NAME . '<br>' . tep_draw_input_field('abstract_zone_name'));
          $contents[] = array('text' => '<br>' . TEXT_INFO_ZONE_TYPE . '<br>' . tep_draw_pull_down_menu('abstract_types_id', $azone_array,''));
          $contents[] = array('text' => '<br>' . TEXT_INFO_ZONE_DESC . '<br>' . tep_draw_textarea_field('abstract_zone_desc', true, 35, 5));
          $contents[] = array('align' => 'center', 'text' => '<br>' . tep_image_submit('button_insert.gif', IMAGE_INSERT) . ' <a href="' . tep_href_link(FILENAME_ABSTRACT_ZONES, 'zpage=' . $this->m_zpage . '&zID=' . $this->m_zID) . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>');
          break;
        case 'edit_zone':
          $heading[] = array('text' => '<b>' . TEXT_INFO_HEADING_EDIT_ZONE . '</b>');

          $contents = array('form' => tep_draw_form('zones', FILENAME_ABSTRACT_ZONES, 'zpage=' . $this->m_zpage . '&zID=' . $this->m_zInfo->abstract_zone_id . '&action=save_zone'));
          $contents[] = array('text' => TEXT_INFO_EDIT_ZONE_INTRO);
          $contents[] = array('text' => '<br>' . TEXT_INFO_ZONE_NAME . '<br>' . tep_draw_input_field('abstract_zone_name', $this->m_zInfo->abstract_zone_name));
          $contents[] = array('text' => '<br>' . TEXT_INFO_ZONE_DESC . '<br>' . tep_draw_textarea_field('abstract_zone_desc', true, 35, 5, $this->m_zInfo->abstract_zone_desc));
          $contents[] = array('align' => 'center', 'text' => '<br>' . tep_image_submit('button_update.gif', IMAGE_UPDATE) . ' <a href="' . tep_href_link(FILENAME_ABSTRACT_ZONES, 'zpage=' . $this->m_zpage . '&zID=' . $this->m_zInfo->abstract_zone_id) . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>');
          break;
        case 'delete_zone':
          $heading[] = array('text' => '<b>' . TEXT_INFO_HEADING_DELETE_ZONE . '</b>');

          $contents = array('form' => tep_draw_form('zones', FILENAME_ABSTRACT_ZONES, 'zpage=' . $this->m_zpage . '&zID=' . $this->m_zInfo->abstract_zone_id . '&action=deleteconfirm_zone'));
          $contents[] = array('text' => TEXT_INFO_DELETE_ZONE_INTRO);
          $contents[] = array('text' => '<br><b>' . $this->m_zInfo->abstract_zone_name . '</b>');
          $contents[] = array('align' => 'center', 'text' => '<br>' . tep_image_submit('button_delete.gif', IMAGE_DELETE) . ' <a href="' . tep_href_link(FILENAME_ABSTRACT_ZONES, 'zpage=' . $this->m_zpage . '&zID=' . $this->m_zInfo->abstract_zone_id) . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>');
          break;

        default:
          if (isset($this->m_zInfo) && is_object($this->m_zInfo) && tep_not_null($this->m_zID) ) {
            $heading[] = array('text' => '<b>' . $this->m_zInfo->abstract_zone_name . '</b>');

            $contents[] = array('align' => 'center', 'text' => '<a href="' . tep_href_link(FILENAME_ABSTRACT_ZONES, 'zpage=' . $this->m_zpage . '&zID=' . $this->m_zInfo->abstract_zone_id . '&action=edit_zone') . '">' . tep_image_button('button_edit.gif', IMAGE_EDIT) . '</a> <a href="' . tep_href_link(FILENAME_ABSTRACT_ZONES, 'zpage=' . $this->m_zpage . '&zID=' . $this->m_zInfo->abstract_zone_id . '&action=delete_zone') . '">' . tep_image_button('button_delete.gif', IMAGE_DELETE) . '</a>&nbsp;<a href="' . tep_href_link(FILENAME_ABSTRACT_ZONES, 'zpage=' . $this->m_zpage . '&zID=' . $this->m_zInfo->abstract_zone_id . '&action=new_zone') . '">' . tep_image_button('button_new.gif', IMAGE_INSERT) . '</a>&nbsp;<a href="' . tep_href_link(FILENAME_ABSTRACT_ZONES, 'zpage=' . $this->m_zpage . '&spage=1' . '&zID=' . $this->m_zInfo->abstract_zone_id . '&action=list') . '">' . tep_image_button('button_details.gif', IMAGE_DETAILS) . '</a>');
            $contents[] = array('text' => '<br>' . TEXT_INFO_ZONE_TYPE . '<br><b>' . $this->m_zInfo->abstract_types_name . '</b>');
            $contents[] = array('text' => '<br>' . TEXT_INFO_ZONE_CLASS . '<br><b>' . $this->m_zInfo->abstract_types_class . '.php</b>');
            $contents[] = array('text' => '<br>' . TEXT_INFO_ZONE_TABLE . '<br><b>' . $this->m_zInfo->abstract_types_table . '</b>');
            $contents[] = array('text' => '<br>' . TEXT_INFO_ZONE_DESC . '<br>' . $this->m_zInfo->abstract_zone_desc);
            $contents[] = array('text' => '<br>' . TEXT_INFO_DATE_ADDED . '<br>' . tep_date_short($this->m_zInfo->date_added));
            if (tep_not_null($this->m_zInfo->last_modified)) 
              $contents[] = array('text' => TEXT_INFO_LAST_MODIFIED . '<br>' . tep_date_short($this->m_zInfo->last_modified));
          }
          break;
      }

      if( isset($heading) && isset($contents) && tep_not_null($heading) && tep_not_null($contents) ) {
        $html_string .= '            <td width="25%" valign="top">' . "\n";
        $box = new box;
        $html_string .= $box->infoBox($heading, $contents);
        $html_string .= '            </td>' . "\n";
      }
      return $html_string;
    }

    function display_bottom() {
       return '';
    }
  }
?>
