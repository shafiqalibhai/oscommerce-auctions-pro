<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006 Asymmetric Software. Innovation & Excellence.
// Author: Mark Samios
// http://www.asymmetrics.com
// META-G Feeds for the Admin end
//----------------------------------------------------------------------------
// I-Metrics Layer
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
  require('includes/application_top.php');

  $max_limit = 1000;
  $action = (isset($_GET['action']) ? $_GET['action'] : '');

  if (!tep_session_is_registered('last_id') || !$last_id) {
    tep_session_register('last_id');
    $last_id = 0;
  }

  switch ($action) {
    case 'generate':
      include(DIR_WS_CLASSES . 'xml_core.php');
      include(DIR_WS_CLASSES . 'bulk_feed.php');

      $xml_filename = META_BASE_FILENAME;
      $basefeed_url = HTTP_CATALOG_SERVER . DIR_WS_CATALOG . $xml_filename;
      $file_location = DIR_FS_CATALOG . $xml_filename;

      if( !$last_id ) {
        $feed =  new bulk_feed();
        $feed->build_channel('META-G Generator in ' . STORE_NAME, 'Products Feed from ' . STORE_NAME);
        //$xml_string = $feed->get_xml_string();
        //$final_string = $xml_string;

        $handle = fopen($file_location, 'w+');
        if( $handle ) {
          //fwrite($handle, $final_string);
          fclose($handle);
          chmod($file_location, 0644);
          $messageStack->add('Feed Output Started at <b>' . $file_location . '</b>', 'success');
        } else {
          $messageStack->add_session('Could not create/write file: <b>' . $file_location . '</b>', 'error');
          tep_redirect(tep_href_link(FILENAME_META_FEEDS, tep_get_all_get_params(array('action')) ));
        }

      } else {
        $feed =  new bulk_feed('rss20', false);
      }
      $products_array = array();
      $last_id = $feed->build_products($last_id, $max_limit, $products_array, $max_limit);

      if( !$last_id ) {
        $feed->close_channel();
        $feed->close_feed();
        tep_session_unregister('last_id');
      }

      $xml_string = $feed->get_xml_string();
      $final_string = $xml_string;

      $handle = fopen($file_location, 'a+');
      if( $handle ) {
        fwrite($handle, $final_string);
        fclose($handle);
        chmod($file_location, 0644);
        //$messageStack->add_session('File <b>' . $file_location . '</b> successfully created', 'success');
      } else {
        $messageStack->add_session('Could not write file: <b>' . $file_location . '</b>', 'error');
        tep_redirect(tep_href_link(FILENAME_META_FEEDS, tep_get_all_get_params(array('action')) ));
      }

      if( !$last_id ) {
        $messageStack->add_session('File <b>' . $file_location . '</b> successfully created', 'success');
        tep_redirect(tep_href_link(FILENAME_META_FEEDS, tep_get_all_get_params(array('action')) ));
      }

/*
      if( !$last_id ) {
        if( isset($_POST['store_feed']) ) {
          tep_redirect(tep_href_link(FILENAME_META_FEEDS, tep_get_all_get_params(array('action')) ));
        } else {
          header("Expires: 0");
          header("Cache-Control: no-cache, must-revalidate");
          header("Pragma: no-cache");
          header('Content-Disposition: attachment; filename="' . $xml_filename . '"');
          header('Content-Length: '. strlen($final_string) );
          header("Content-Type: application/octet-stream");
          echo $final_string;
          exit();
        }
      }
*/
      break;
    case 'cancel':
      tep_session_unregister('last_id');
      tep_redirect(tep_href_link(FILENAME_META_FEEDS));
      break;
    default:
      break;
  }

?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<?php
  if( $last_id > 0 )
    echo '  <meta http-equiv="refresh" content="10">' . "\n";
?>
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
</head>
<body>
<!-- header //-->
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<!-- header_eof //-->

<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
    <td width="<?php echo BOX_WIDTH; ?>" valign="top"><table border="0" width="<?php echo BOX_WIDTH; ?>" cellspacing="1" cellpadding="1" class="columnLeft">
<!-- left_navigation //-->
<?php require(DIR_WS_INCLUDES . 'column_left.php'); ?>
<!-- left_navigation_eof //-->
    </table></td>
<!-- body_text //-->
    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
        <td width="100%"><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td class="pageHeading"><?php echo HEADING_TITLE; ?></td>
          </tr>
        </table></td>
      </tr>
<?php
  if( $action == 'generate' ) {
?>
      <tr>
        <td><?php echo '<a href="' . tep_href_link(FILENAME_META_FEEDS, 'action=cancel') . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>'; ?></td>
      </tr>
      <tr>
        <td><table border="0" cellspacing="0" cellpadding="2">
          <tr>
            <td class="smallText">
<?php
    $matches = count($products_array);
    echo '<b>' . $matches . ' Entries Matched from ' . $max_limit . '</b> - Efficiency (' . tep_round($matches/$max_limit, 2)*100 . '%)'; 
?>
            </td>
          </tr>
          <tr>
            <td><hr /></td>
          </tr>
        </table></td>
      </tr>
<?php
    foreach( $products_array as $key => $value ) {
?>
      <tr>
        <td><table border="0" cellspacing="0" cellpadding="2">
          <tr>
            <td class="smallText"><?php echo 'Entry Added &raquo; <b>' . $value['seo_url_get'] . '</b>'; ?></td>
          </tr>
        </table></td>
      </tr>
<?php
    }
  } else {
?>
      <tr>
        <td class="pageHeading"><hr /></td>
      </tr>
      <tr>
        <td class="smallText"><?php echo TEXT_CREATE_PRODUCTS; ?></td>
      </tr>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="2">
          <tr>
            <td><?php echo '<a href="' . tep_href_link(FILENAME_META_FEEDS, 'action=generate') . '">' . tep_image_button('button_generate.gif', TEXT_GENERATE) . '</a>'; ?></td>
            <td width="95%">&nbsp;</td>
          </tr>
        </table></td>
      </tr>
<?php
  }  

?>
    </table></td>
<!-- body_text_eof //-->
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
