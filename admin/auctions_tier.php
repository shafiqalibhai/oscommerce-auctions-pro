<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2011 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
//----------------------------------------------------------------------------
// Admin: Auctions Main Script
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
  require('includes/application_top.php');
  require(DIR_WS_CLASSES . 'currencies.php');
  $currencies = new currencies();

  $auction_status_array = array(
    array('id' => 0, 'text' => 'Coming Auction'),
    array('id' => 1, 'text' => 'Live Auction'),
    array('id' => 2, 'text' => 'Ended Auction'),
  );

  $auction_types_array = array(
    array('id' => 0, 'text' => 'Penny Auction'),
    array('id' => 1, 'text' => 'Lowest Unique Bid'),
    array('id' => 2, 'text' => 'Normal Auction'),
  );

  $action = isset($_GET['action'])?tep_db_prepare_input($_GET['action']):'';
  $sort_id = isset($_GET['sort_id'])?tep_db_prepare_input($_GET['sort_id']):1;

  if (isset($_POST['update_auction_x']) || isset($_POST['update_auction_y'])) $action='update_confirm';

  $aID = isset($_GET['aID'])?(int)$_GET['aID']:0;

  switch ($action) {
    case 'delete_confirm':
      $aID = isset($_POST['auctions_id'])?(int)$_POST['auctions_id']:0;
      $sort_id = isset($_POST['sort_id'])?(int)$_POST['sort_id']:0;

      if( empty($aID) ) {
        $messageStack->add_session(ERROR_AUCTION_TIER_INVALID);
        tep_redirect(tep_href_link($g_script));
      }

      $check_query = tep_db_query("select auctions_image from " . TABLE_AUCTIONS_TIER . " where auctions_id = '" . (int)$aID . "' and sort_id='" . (int)$sort_id . "'");
      if( !tep_db_num_rows($check_query) ) {
        $messageStack->add_session(ERROR_AUCTION_TIER_INVALID);
        tep_redirect(tep_href_link($g_script));
      }

      $check_array = tep_db_fetch_array($check_query);
      if( !empty($check_array['auctions_image']) && is_file(DIR_FS_CATALOG_IMAGES . $check_array['auctions_image']) ) {
        @unlink(DIR_FS_CATALOG_IMAGES . $check_array['auctions_image']);
        clearstatcache();
      }

      tep_db_query("delete from " . TABLE_AUCTIONS_TIER . " where auctions_id = '" . (int)$aID . "' and sort_id = '" . (int)$sort_id . "'");

      $messageStack->add_session(WARNING_AUCTION_TIER_DELETED, 'warning');
      tep_redirect(tep_href_link($g_script, tep_get_all_get_params(array('action', 'aID', 'sort_id')) ));
      break;

    case 'insert_confirm':
      $products_id = isset($_POST['products_id'])?(int)$_POST['products_id']:0;
      $check_query = tep_db_query("select count(*) as total from " . TABLE_PRODUCTS . " where products_id = '" . (int)$products_id . "'");
      $check_array = tep_db_fetch_array($check_query);
      if( !$check_array['total'] ) {
        $messageStack->add_session(ERROR_AUCTION_PRODUCTS_INVALID);
        tep_redirect(tep_href_link($g_script, tep_get_all_get_params(array('action', 'aID')) ));
      }

      $image_query = tep_db_query("select products_image from " . TABLE_PRODUCTS . " where products_id  = '" . (int)$products_id . "'");
      if( tep_db_num_rows($image_query) ) {
        $image_array = tep_db_fetch_array($image_query);     
      } else {
        $image_array['products_image'] = 'image_not_available.jpg';
      }

    case 'update_confirm':
      $auctions_id = isset($_POST['auctions_id'])?(int)$_POST['auctions_id']:0;
      $check_query = tep_db_query("select count(*) as total from " . TABLE_AUCTIONS . " where status_id < 2 and auctions_id = '" . (int)$auctions_id . "'");
      $check_array = tep_db_fetch_array($check_query);
      if( !$check_array['total'] ) {
        $messageStack->add_session(ERROR_AUCTION_INVALID);
        tep_redirect(tep_href_link($g_script, tep_get_all_get_params(array('action', 'aID')) ));
      }

      $sort_id = max((int)$_POST['sort_id'], 1);

      $check_query = tep_db_query("select count(*) as total from " . TABLE_AUCTIONS_TIER . " where auctions_id = '" . (int)$auctions_id . "' and sort_id = '" . (int)$sort_id . "'");
      $check_array = tep_db_fetch_array($check_query);

      if( $check_array['total'] ) {
        $check_query = tep_db_query("select sort_id from " . TABLE_AUCTIONS_TIER . " where auctions_id = '" . (int)$auctions_id . "' order by sort_id desc limit 1");
        $check_array = tep_db_fetch_array($check_query);
        $sort_id = $check_array['sort_id']+1;
      }


      $auctions_name = tep_db_prepare_input($_POST['auctions_name']);
      if( empty($auctions_name) ) {
        $check_query = tep_db_query("select auctions_name from " . TABLE_AUCTIONS . " where auctions_id = '" . $auctions_id . "'");
        $check_array = tep_db_fetch_array($check_query);
        $auctions_name = $check_array['auctions_name'] . '-at' . $sort_id;
      }

      $auction_array = array(
        'auctions_id' => (int)$auctions_id,
        'products_id' => (int)$products_id,
        'auctions_name' => $auctions_name,
        'auctions_description' => tep_db_prepare_input($_POST['auctions_description']),
        'auctions_price' => tep_db_prepare_input($_POST['auctions_price']),
        'shipping_cost' => tep_db_prepare_input($_POST['shipping_cost']),
        'sort_id' => (int)$sort_id,
      );

      if( isset($_FILES['auctions_image']) && !empty($_FILES['auctions_image']['name']) ) {
        $cImage = new upload('auctions_image', DIR_FS_CATALOG_IMAGES);
        if( $cImage->c_result ) {
          $auction_array['auctions_image'] = $cImage->filename;
        }
      }

      if($action == 'insert_confirm') {
        tep_db_perform(TABLE_AUCTIONS_TIER, $auction_array);
        $aID = $auctions_id;
        if( !isset($auction_array['auctions_image']) && is_file(DIR_FS_CATALOG_IMAGES . $image_array['products_image']) ) {

          $tmp_array = explode('.', $image_array['products_image']);
          $dst_image =  $tmp_array[0] . '-t' . $aID . '-' . $sort_id . '.' . $tmp_array[count($tmp_array)-1];

          $src = DIR_FS_CATALOG_IMAGES . $image_array['products_image'];
          $dst = DIR_FS_CATALOG_IMAGES . $dst_image;
          copy($src, $dst);
          tep_db_query("update " . TABLE_AUCTIONS_TIER . " set auctions_image = '" . tep_db_input($dst_image) . "' where auctions_id = '" . (int)$aID . "' and sort_id = '" . (int)$sort_id . "'");
        }

        $messageStack->add_session(SUCCESS_AUCTION_CREATED, 'success');
      } elseif ($action == 'update_confirm') {
        tep_db_perform(TABLE_AUCTIONS_TIER, $auction_array, 'update', "auctions_id = '" . (int)$auctions_id . "' and sort_id='" . (int)$_POST['sort_id'] . "'");
        $messageStack->add_session(SUCCESS_AUCTION_UPDATED, 'success');
      }
      tep_redirect(tep_href_link($g_script, tep_get_all_get_params(array('action', 'aID')) . 'aID=' . $auctions_id . '&sort_id=' . $sort_id));
      break;

    case 'new':
    case 'edit':
      $check_query = tep_db_query("select * from " . TABLE_AUCTIONS_TIER . " where auctions_id = '" . (int)$aID . "' and sort_id='" . (int)$sort_id . "'");
      if( !tep_db_num_rows($check_query) ) {
        $action = 'new';
        $auction_array = array(
          'auctions_id' => (int)$aID,
          'products_id' => 0,
          'auctions_name' => '',
          'auctions_description' => '',
          'auctions_price' => '0.00',
          'shipping_cost' => '0.00',
          'sort_id' => 1,
        );
      } else {
        $auction_array = tep_db_fetch_array($check_query);
      }
      break;
    default:
      break;
  }

?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<link rel="stylesheet" type="text/css" href="includes/javascript/jquery/themes/smoothness/jquery-ui.css" />
<link rel="stylesheet" type="text/css" href="includes/javascript/jquery/timepick/jquery-ui-timepicker.css" />
<script type="text/javascript" src="includes/javascript/jquery/jquery.js"></script>
<script language="javascript" type="text/javascript" src="includes/javascript/jquery/ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="includes/javascript/fancybox/jquery.fancybox.js"></script>
<script type="text/javascript" src="includes/javascript/fancybox/jquery.mousewheel.pack.js"></script>
<link rel="stylesheet" type="text/css" href="includes/javascript/fancybox/jquery.fancybox.css" media="screen" />
<script type="text/javascript" src="includes/javascript/auctions_tier.js"></script>
<?php
  if( $action == 'edit' || $action == 'new' ) {
    $mce_str = 'auctions_description'; // YOURCODEHERE // Comma separated list of textarea names
    echo '<script language="javascript" type="text/javascript" src="includes/javascript/tiny_mce/tiny_mce.js"></script>';
    include "includes/javascript/tiny_mce/general2.php";
  }
?>
</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF">
<!-- header //-->
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<!-- header_eof //-->
<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
    <td width="<?php echo BOX_WIDTH; ?>" valign="top"><table border="0" width="<?php echo BOX_WIDTH; ?>" cellspacing="1" cellpadding="1" class="columnLeft">
<!-- left_navigation //-->
<?php require(DIR_WS_INCLUDES . 'column_left.php'); ?>
<!-- left_navigation_eof //-->
    </table></td>
<!-- body_text //-->
    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
<?php
  if( $action == 'edit' || $action == 'new') {
    $title = ($action=='new')?HEADING_TITLE_NEW:HEADING_TITLE_EDIT;

    $buttons = array(
      '<a href="' . tep_href_link($g_script, tep_get_all_get_params(array('action'))) .'">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>',
    );

    if( $action == 'edit' ) {
      $buttons[] = tep_image_submit('button_update.gif',IMAGE_UPDATE, 'name="update_auction"');
    } else {
      $buttons[] = tep_image_submit('button_insert.gif',IMAGE_INSERT);
    }
?>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td class="pageHeading"><?php echo $title; ?></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><?php echo tep_draw_form("edit_tier", $g_script, tep_get_all_get_params(array('action', 'aID')) . 'action=insert_confirm&aID=' . $aID, 'post', 'enctype="multipart/form-data"'); ?><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><table width="100%" border="0" cellspacing="1" cellpadding="3">
              <tr>
                <td class="main"><?php echo TEXT_INFO_AUCTION_ID; ?></td>
                <td><table border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td class="rpad"><?php echo tep_draw_input_field('auctions_id', $auction_array['auctions_id'], 'id="insert_auction_tier" size="4"'); ?></td>
                    <td><?php echo '<a href="#" id="button_auction_tier">' . tep_image_button('button_insert.gif', IMAGE_INSERT) . '<a>'; ?></td>
                  </tr>
                </table></td>
              </tr>
              <tr>
                <td class="main"><?php echo TEXT_INFO_PRODUCT_ID; ?></td>
                <td><table border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td class="rpad"><?php echo tep_draw_input_field('products_id', $auction_array['products_id'], 'id="insert_product_tier" size="4"'); ?></td>
                    <td class="rpad"><?php echo '<a href="#" id="button_product_tier">' . tep_image_button('button_insert.gif', IMAGE_INSERT) . '<a>'; ?></td>
<?php
    $product_name = tep_get_products_name($auction_array['products_id']);
    if( !empty($product_name) ) {
?>
                    <td class="main"><?php echo TEXT_INFO_PRODUCT_NAME . '&nbsp;<b>' . $product_name . '</b>'; ?></td>
<?php
    }
?>
                  </tr>
                </table></td>
              </tr>
              <tr>
                <td class="main"><?php echo TEXT_INFO_AUCTION_NAME; ?></td>
                <td><?php echo tep_draw_input_field('auctions_name', $auction_array['auctions_name']); ?></td>
              </tr>
              <tr>
                <td class="main"><?php echo TEXT_INFO_AUCTION_IMAGE; ?></td>
                <td><table border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td class="rpad"><?php echo tep_draw_file_field('auctions_image'); ?></td>
<?php
    if( !empty($auction_array['auctions_image']) ) {
?>
                    <td class="rpad"><?php echo $auction_array['auctions_image']; ?></td>
<?php
    }
?>
                  </tr>
                </table></td>
              </tr>
              <tr>
                <td><?php echo TEXT_INFO_AUCTION_START_PRICE; ?></td>
                <td><?php echo tep_draw_input_field('auctions_price', $auction_array['auctions_price']); ?></td>
              </tr>
              <tr>
                <td><?php echo TEXT_INFO_AUCTION_SHIPPING; ?></td>
                <td><?php echo tep_draw_input_field('shipping_cost', $auction_array['shipping_cost']); ?></td>
              </tr>
              <tr>
                <td colspan="2"><table border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td><?php echo TEXT_INFO_AUCTION_DESCRIPTION; ?></td>
                  </tr>
                  <tr>
                    <td><?php echo tep_draw_textarea_field('auctions_description', 'hard', 41, 5, $auction_array['auctions_description']); ?></td>
                  </tr>
                </table></td>
              </tr>
              <tr>
                <td><?php echo TEXT_INFO_AUCTION_SORT_ORDER; ?></td>
                <td><?php echo tep_draw_input_field('sort_id', $auction_array['sort_id']); ?></td>
              </tr>
            </table></td>
          </tr>

          <tr>
            <td class="formAreaRow"><?php echo implode('', $buttons); ?></td>
          </tr>
        </table></form></td>
      </tr>
<?php
  } else {
    $buttons = array(
      '<a href="' . tep_href_link(FILENAME_AUCTIONS_TIER, tep_get_all_get_params(array('action', 'aID', 'sort_id')) . 'action=new') . '">' . tep_image_button('button_insert.gif', IMAGE_INSERT) . '</a>',
    );
?>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td class="pageHeading"><?php echo HEADING_TITLE; ?></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top"><table class="tabledata">
              <tr class="dataTableHeadingRow">
                <th class="calign"><?php echo TABLE_HEADING_ID; ?></th>
                <th class="calign"><?php echo TABLE_HEADING_IMAGE; ?></th>
                <th><?php echo TABLE_HEADING_TITLE; ?></th>
                <th><?php echo TABLE_HEADING_PRODUCT_TITLE; ?></th>
                <th class="calign"><?php echo TABLE_HEADING_ACTION; ?></th>
              </tr>
<?php
    $generic_count = 0;
    $rows = 0;
    $auctions_query_raw = "select auctions_id, products_id, auctions_name, auctions_image, sort_id from " . TABLE_AUCTIONS_TIER . " order by auctions_id desc, sort_id asc";

    $auctions_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS, $auctions_query_raw, $auctions_query_numrows);
    $auctions_query = tep_db_query($auctions_query_raw);

    while( $auctions_array = tep_db_fetch_array($auctions_query) ) {

      if( ( empty($aID) && !isset($aInfo) ) || ( !empty($aID) && ($aID == $auctions_array['auctions_id'] && $sort_id == $auctions_array['sort_id'])) && (substr($action, 0, 3) != 'new')) {
        $aInfo = new objectInfo($auctions_array);
      }
      $generic_count++;
      $rows++;

      if (isset($aInfo) && is_object($aInfo) && ($auctions_array['auctions_id'] == $aInfo->auctions_id) && $auctions_array['sort_id'] == $aInfo->sort_id ) {
        $class = 'dataTableRowSelected';
        $aInfo->products_name = tep_get_products_name($auctions_array['products_id']);
      } else {
        $class = 'dataTableRow';
      }

      echo '              <tr class="' . $class . '">' . "\n";
?>
                <td class="calign"><?php echo $auctions_array['auctions_id'] . '-' . $auctions_array['sort_id']; ?></td>
                <td class="calign"><?php echo tep_image(DIR_WS_CATALOG_IMAGES . $auctions_array['auctions_image'], $auctions_array['auctions_name'], SMALL_IMAGE_WIDTH, SMALL_IMAGE_HEIGHT); ?></td>
                <td><?php echo $auctions_array['auctions_name']; ?></td>
                <td><?php echo tep_get_products_name($auctions_array['products_id']); ?></td>
                <td class="calign"><?php if (isset($aInfo) && is_object($aInfo) && ($auctions_array['auctions_id'] == $aInfo->auctions_id && $auctions_array['sort_id'] == $aInfo->sort_id)) { echo tep_image(DIR_WS_IMAGES . 'icon_arrow_right.gif', ''); } else { echo '<a href="' . tep_href_link($g_script, 'aID=' . $auctions_array['auctions_id'] . '&sort_id=' . $auctions_array['sort_id']) . '">' . tep_image(DIR_WS_IMAGES . 'icon_info.gif', IMAGE_ICON_INFO) . '</a>'; } ?>&nbsp;</td>
              </tr>
<?php
    }
?>
              <tr>
                <td colspan="7"><table border="0" width="100%" cellpadding="0"cellspacing="2">
                  <tr>
                    <td class="smallText"><?php echo $auctions_split->display_count($auctions_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_ENTRIES); ?></td>
                    <td class="smallText ralign"><?php echo $auctions_split->display_links($auctions_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, MAX_DISPLAY_PAGE_LINKS, $_GET['page']); ?></td>
                  </tr>
                </table></td>
              </tr>
              <tr>
                <td class="formAreaRow ralign" colspan="7"><?php echo implode('', $buttons); ?></td>
              </tr>
            </table></td>
<?php
    $heading = array();
    $contents = array();

    switch ($action) {
/*
      case 'new':
        $heading[] = array('text' => '<b>' . TEXT_INFO_HEADING_AUCTION_NEW . '</b>');

        $contents = array('form' => tep_draw_form('new_group', $g_script, 'action=insert_confirm') . tep_draw_hidden_field('auctions_id', $aInfo->auctions_id));
        $contents[] = array('text' => TEXT_INFO_AUCTION_NEW_INTRO);
        $contents[] = array('text' => TEXT_INFO_AUCTION_NAME . '<br />' . tep_draw_input_field('auctions_name'));
        $contents[] = array('text' => TEXT_INFO_AUCTION_DESC . '<br />' . tep_draw_textarea_field('auctions_description', 'hard', 30, 5, ''));

        $contents[] = array('align' => 'center', 'text' => '<br>' . tep_image_submit('button_insert.gif', IMAGE_INSERT) . ' <a href="' . tep_href_link($g_script, 'aID=' . $aInfo->auctions_id) . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>');
        break;

      case 'edit':
        $heading[] = array('text' => '<b>' . TEXT_INFO_HEADING_EDIT . '</b>');

        $contents = array('form' => tep_draw_form('edit_group', $g_script, 'action=update_confirm') . tep_draw_hidden_field('auctions_id', $aInfo->auctions_id));
        $contents[] = array('text' => TEXT_INFO_EDIT_INTRO);
        $contents[] = array('text' => TEXT_INFO_GROUP_NAME . '<br />' . tep_draw_input_field('auctions_name', $aInfo->auctions_name));
        $contents[] = array('text' => TEXT_INFO_GROUP_DESC . '<br />' . tep_draw_textarea_field('auctions_description', 'hard', 30, 5, $aInfo->auctions_description));
        $contents[] = array('align' => 'center', 'text' => '<br>' . tep_image_submit('button_update.gif', IMAGE_UPDATE) . ' <a href="' . tep_href_link($g_script, 'aID=' . $aInfo->auctions_id) . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>');
        break;
*/
      case 'delete':
        $heading[] = array('text' => '<b>' . TEXT_INFO_HEADING_DELETE . '</b>');

        $contents = array('form' => tep_draw_form('delete_auction', $g_script, 'action=delete_confirm') . tep_draw_hidden_field('auctions_id', $aInfo->auctions_id) . tep_draw_hidden_field('sort_id', $aInfo->sort_id));
        $contents[] = array('text' => TEXT_INFO_DELETE_INTRO);
        $contents[] = array('text' => '<br><b>' . $aInfo->auctions_name . '</b>');
        $contents[] = array('align' => 'center', 'text' => '<br>' . tep_image_submit('button_delete.gif', IMAGE_DELETE) . ' <a href="' . tep_href_link($g_script, 'aID=' . $aInfo->auctions_id) . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>');
        break;

      default:
        if( isset($aInfo) && is_object($aInfo) ) { // generic_text info box contents
          $extra_query = tep_db_query("select bid_step, max_bids, countdown_bids, date_expire, start_pause, end_pause, start_price, shipping_cost from "  . TABLE_AUCTIONS . " where auctions_id = '" . $aInfo->auctions_id . "'");
          $extra_array = tep_db_fetch_array($extra_query);
          $heading[] = array('text' => '<b>' . $aInfo->auctions_name . '</b>');
          $contents[] = array(
            'align' => 'center', 
            'text' => '<a href="' . tep_href_link($g_script, 'aID=' . $aInfo->auctions_id . '&sort_id=' . $aInfo->sort_id . '&action=edit') . '">' . tep_image_button('button_details.gif', TEXT_INFO_DETAILS) . '</a> <a href="' . tep_href_link($g_script, 'aID=' . $aInfo->auctions_id . '&sort_id=' . $aInfo->sort_id . '&action=delete') . '">' . tep_image_button('button_delete.gif', IMAGE_DELETE) . '</a>');
          $contents[] = array('text' => '<br />');
          $contents[] = array('text' => TEXT_INFO_AUCTION_NAME . '<br /><b>' . $aInfo->auctions_name . '</b>');
          $contents[] = array('text' => TEXT_INFO_AUCTION_PRIZE . '<br /><b>' . $aInfo->products_name . '</b>');
          $product_name = tep_get_products_name($aInfo->products_id);
          if( !empty($products_name) ) {
            $contents[] = array('text' => TEXT_INFO_PRODUCT_NAME . '<br /><b>' . $product_name . '</b>');
          }
          //$contents[] = array('text' => $aInfo->auctions_description);
          //$contents[] = array('text' => $aInfo->auctions_description);
          //$contents[] = array('text' => $aInfo->auctions_description);

        } else { // create generic_text dummy info
          $heading[] = array('text' => '<b>' . TEXT_INFO_NO_AUCTIONS . '</b>');
          $contents[] = array('text' => TEXT_INFO_ZERO_AUCTIONS);
        }
        break;

    }

    if ( (tep_not_null($heading)) && (tep_not_null($contents)) ) {
      echo '            <td width="25%" valign="top">' . "\n";

      $box = new box;
      echo $box->infoBox($heading, $contents);

      echo '            </td>' . "\n";
    }
?>
          </tr>
        </table></td>
      </tr>
<?php
  }

?>
    </table></td>
<!-- body_text_eof //-->
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
<div><script language="javascript" type="text/javascript">
  var jqWrap = auctions_tier;
  jqWrap.auctionsURL = "<?php echo tep_href_link(FILENAME_AJAX_CONTROL, 'module=auctions_list&strings=' . $g_script); ?>"
  jqWrap.productsURL = "<?php echo tep_href_link(FILENAME_AJAX_CONTROL, 'module=auctions_product_list&strings=' . $g_script); ?>"
  jqWrap.launch();
</script></div>
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
