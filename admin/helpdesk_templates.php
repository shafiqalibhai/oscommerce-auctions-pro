<?php
/*
  $Id: helpdesk_templates.php,v 1.6 2005/08/16 21:14:04 lane Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

  require('includes/application_top.php');

  $action = (isset($_GET['action']) ? $_GET['action'] : '');
  $template_id = (isset($_GET['template_id']) ? (int)$_GET['template_id'] : '');

  switch($action) {
    case 'insert':
    case 'save':
      $title = tep_db_prepare_input($_POST['title']);
      $body = tep_db_prepare_input($_POST['body']);

      $sql_data_array = array('title' => $title,
                              'template' => $body);

      if( $action == 'insert' ) {
        tep_db_perform(TABLE_HELPDESK_TEMPLATES, $sql_data_array);
        $template_id = tep_db_insert_id();
      } elseif( $action == 'save' ) {
        tep_db_perform(TABLE_HELPDESK_TEMPLATES, $sql_data_array, 'update', "template_id = '" . (int)$template_id . "'");
      }

      tep_redirect(tep_href_link(FILENAME_HELPDESK_TEMPLATES, 'page=' . $_GET['page'] . '&template_id=' . $template_id));
      break;
    case 'deleteconfirm':
      tep_db_query("delete from " . TABLE_HELPDESK_TEMPLATES . " where template_id = '" . (int)$template_id . "'");
      tep_redirect(tep_href_link(FILENAME_HELPDESK_TEMPLATES, 'page=' . $_GET['page']));
      break;
    default:
      break;
  }
?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<script language="javascript" src="includes/general.js"></script>
</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF" onload="SetFocus();">
<!-- header //-->
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<!-- header_eof //-->

<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
    <td width="<?php echo BOX_WIDTH; ?>" valign="top"><table border="0" width="<?php echo BOX_WIDTH; ?>" cellspacing="1" cellpadding="1" class="columnLeft">
<!-- left_navigation //-->
<?php require(DIR_WS_INCLUDES . 'column_left.php'); ?>
<!-- left_navigation_eof //-->
    </table></td>
<!-- body_text //-->
    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td class="pageHeading"><?php echo HEADING_TITLE; ?></td>
            <td class="pageHeading" align="right"><?php echo tep_draw_separator('pixel_trans.gif', HEADING_IMAGE_WIDTH, HEADING_IMAGE_HEIGHT); ?></td>
          </tr>
        </table></td>
      </tr>
<?php
  if( $action== 'new' || $action == 'edit' ) {
    switch ($action) {
      case 'new':
        $form_action = 'insert';
        $tInfo = new objectInfo(array('title'=>'', 'template'=> ''));
        break;
      case 'edit':
        $form_action = 'save';
        $template_query = tep_db_query("select template_id, title, template from " . TABLE_HELPDESK_TEMPLATES . " where template_id = '" . tep_db_input($template_id) . "'");
        $template = tep_db_fetch_array($template_query);

        $tInfo = new objectInfo($template, false);
        break;
    }
?>
      <?php echo tep_draw_form('template', FILENAME_HELPDESK_TEMPLATES, 'page=' . $_GET['page'] . '&template_id=' . $template_id . '&action=' . $form_action); ?>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="2" class="columnLeft">
          <tr>
            <td class="smallText"><?php echo (($form_action == 'insert') ? TEXT_INSERT_TEMPLATE : TEXT_UPDATE_TEMPLATE); ?></td>
            <td align="right"><?php echo tep_image_submit('button_confirm.gif', IMAGE_CONFIRM) . '&nbsp;<a href="' . tep_href_link(FILENAME_HELPDESK_TEMPLATES, 'page=' . $_GET['page'] . '&template_id=' . $template_id) . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>'; ?></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
      </tr>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="2" class="columnLeft">
          <tr>
            <td class="smallText"><?php echo TEXT_TEMPLATE_TITLE; ?></td>
            <td class="smallText"><?php echo tep_draw_input_field('title', $tInfo->title); ?></td>
          </tr>
          <tr>
            <td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
          </tr>
          <tr>
            <td class="smallText" valign="top"><?php echo TEXT_TEMPLATE_BODY; ?></td>
            <td class="smallText"><?php echo tep_draw_textarea_field('body', 'virtual', '60', '10', $tInfo->template, 'style="width: 100%"'); ?></td>
          </tr>
        </table></td>
      </tr>
      </form>
<?php
  } else {
?>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top"><table border="0" width="100%" cellspacing="1" cellpadding="3">
              <tr class="dataTableHeadingRow">
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_TEMPLATES; ?></td>
                <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_ACTION; ?>&nbsp;</td>
              </tr>
<?php
    $templates_query_raw = "select template_id, title from " . TABLE_HELPDESK_TEMPLATES . " order by title";
    $templates_split = new splitPageResults($_GET['page'], MAX_DISPLAY_SEARCH_RESULTS, $templates_query_raw, $templates_query_numrows);
    $templates_query = tep_db_query($templates_query_raw);
    while ($templates = tep_db_fetch_array($templates_query)) {
      if( ((empty($template_id) || $template_id == $templates['template_id'])) && ( !isset($tInfo) && substr($action, 0, 3) != 'new') ) {
        $tInfo = new objectInfo($templates, false);
      }

      if( isset($tInfo) && is_object($tInfo) && ($templates['template_id'] == $tInfo->template_id) ) {
        echo '                  <tr class="dataTableRowSelected" onmouseover="this.style.cursor=\'hand\'" onclick="document.location.href=\'' . tep_href_link(FILENAME_HELPDESK_TEMPLATES, 'page=' . $_GET['page'] . '&template_id=' . $tInfo->template_id . '&action=edit') . '\'">' . "\n";
      } else {
        echo '                  <tr class="dataTableRow" onmouseover="this.className=\'dataTableRowOver\';this.style.cursor=\'hand\'" onmouseout="this.className=\'dataTableRow\'" onclick="document.location.href=\'' . tep_href_link(FILENAME_HELPDESK_TEMPLATES, 'page=' . $_GET['page'] . '&template_id=' . $templates['template_id']) . '\'">' . "\n";
      }
?>
                <td class="dataTableContent"><?php echo $templates['title']; ?></td>
                <td class="dataTableContent" align="right"><?php if ( isset($tInfo) && is_object($tInfo) && ($templates['template_id'] == $tInfo->template_id) ) { echo tep_image(DIR_WS_IMAGES . 'icon_arrow_right.gif', ''); } else { echo '<a href="' . tep_href_link(FILENAME_HELPDESK_TEMPLATES, 'page=' . $_GET['page'] . '&template_id=' . $templates['template_id']) . '">' . tep_image(DIR_WS_IMAGES . 'icon_info.gif', IMAGE_ICON_INFO) . '</a>'; } ?>&nbsp;</td>
              </tr>
<?php
    }
?>
              <tr>
                <td colspan="2"><table border="0" width="100%" cellspacing="0" cellpadding="2">
                  <tr>
                    <td class="smallText" valign="top"><?php echo $templates_split->display_count($templates_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_ENTRIES); ?></td>
                    <td class="smallText" align="right"><?php echo $templates_split->display_links($templates_query_numrows, MAX_DISPLAY_SEARCH_RESULTS, MAX_DISPLAY_PAGE_LINKS, $_GET['page']); ?></td>
                  </tr>
<?php
    if( empty($action) ) {
?>
                  <tr>
                    <td colspan="2" align="right"><?php echo '<a href="' . tep_href_link(FILENAME_HELPDESK_TEMPLATES, 'page=' . $_GET['page'] . '&action=new') . '">' . tep_image_button('button_insert.gif', IMAGE_INSERT) . '</a>'; ?></td>
                  </tr>

<?php
    }
?>
                </table></td>
              </tr>
            </table></td>
<?php
    $heading = array();
    $contents = array();
    switch ($action) {
      case 'delete':
        $heading[] = array('text' => '<b>' . TEXT_INFO_HEADING_DELETE_TEMPLATE . '</b>');

        $contents = array('form' => tep_draw_form('template', FILENAME_HELPDESK_TEMPLATES, 'page=' . $_GET['page'] . '&template_id=' . $tInfo->template_id  . '&action=deleteconfirm'));
        $contents[] = array('text' => TEXT_INFO_DELETE_INTRO);
        $contents[] = array('text' => '<br><b>' . $tInfo->title . '</b>');
        $contents[] = array('align' => 'center', 'text' => '<br>' . tep_image_submit('button_delete.gif', IMAGE_DELETE) . ' <a href="' . tep_href_link(FILENAME_HELPDESK_TEMPLATES, 'page=' . $_GET['page'] . '&template_id=' . $tInfo->template_id) . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>');
        break;
      default:
        if (is_object($tInfo)) {
          $heading[] = array('text' => '<b>' . $tInfo->title . '</b>');

          $contents[] = array('align' => 'center', 'text' => '<a href="' . tep_href_link(FILENAME_HELPDESK_TEMPLATES, 'page=' . $_GET['page'] . '&template_id=' . $tInfo->template_id . '&action=edit') . '">' . tep_image_button('button_edit.gif', IMAGE_EDIT) . '</a> <a href="' . tep_href_link(FILENAME_HELPDESK_TEMPLATES, 'page=' . $_GET['page'] . '&template_id=' . $tInfo->template_id . '&action=delete') . '">' . tep_image_button('button_delete.gif', IMAGE_DELETE) . '</a>');
          $contents[] = array('text' => '<br>' . TEXT_INFO_TEMPLATE . '<br>' . $tInfo->title);
        }
        break;
    }

    if ( (tep_not_null($heading)) && (tep_not_null($contents)) ) {
      echo '            <td width="25%" valign="top">' . "\n";

      $box = new box;
      echo $box->infoBox($heading, $contents);

      echo '            </td>' . "\n";
    }
?>
          </tr>
        </table></td>
      </tr>
<?php
  }
?>
    </table></td>
<!-- body_text_eof //-->
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
<br>
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>