<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2007 Asymmetric Software. Innovation & Excellence.
// Author: Mark Samios
// http://www.asymmetrics.com
// ---------------------------------------------------------------------------
// Admin: Numeric Ranges Main file
// ----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
  require('includes/application_top.php');
  $action = (isset($_GET['action']) ? $_GET['action'] : '');
  if (isset($_POST['remove_x']) || isset($_POST['remove_y'])) $action='remove';

  switch ($action) {
    case 'setflag':
      $sql_data_array = array('status_id' => (int)$_GET['flag']);
      tep_db_perform(TABLE_NUMERIC_RANGES, $sql_data_array, 'update', 'numeric_ranges_id=' . $_GET['id']);
      tep_redirect(tep_href_link(FILENAME_NUMERIC_RANGES));   
      break;
    case 'add':
      $sql_data_array = array(
                              'numeric_ranges_min' => (int)$_POST['field']['min'],
                              'numeric_ranges_max' => (int)$_POST['field']['max'],
                              'numeric_ranges_desc' => tep_db_prepare_input($_POST['field']['desc']),
                              'sort_id' => (int)$_POST['field']['sort_id']
                             );
      tep_db_perform(TABLE_NUMERIC_RANGES, $sql_data_array, 'insert');
      tep_redirect(tep_href_link(FILENAME_NUMERIC_RANGES));
      break;
    case 'update':
      foreach ($_POST['field'] as $key=>$val) {
        $sql_data_array = array(
                                'numeric_ranges_min' => (int)$val['min'],
                                'numeric_ranges_max' => (int)$val['max'],
                                'numeric_ranges_desc' => tep_db_prepare_input($val['desc']),
                                'sort_id' => (int)$val['sort_id'],
                               );
        tep_db_perform(TABLE_NUMERIC_RANGES, $sql_data_array, 'update', 'numeric_ranges_id= ' . $key);
      }
      tep_redirect(tep_href_link(FILENAME_NUMERIC_RANGES));
      break;
    case 'remove':
      if ($_POST['mark']) {
        foreach ($_POST['mark'] as $key=>$val) {
          tep_db_query("DELETE FROM " . TABLE_NUMERIC_RANGES . " WHERE numeric_ranges_id=" . (int)$key);
          tep_db_query("DELETE FROM " . TABLE_SEO_TO_RANGES . " WHERE numeric_ranges_id=" . (int)$key);
        }
        tep_redirect(tep_href_link(FILENAME_NUMERIC_RANGES));
      }
      break;
    default:
      break;
  }
?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<script language="javascript" src="includes/general.js"></script>
<script language="javascript">
  var g_checkbox2 = 0;
  function copy_checkboxes(form, array_name) {
    for (var i = 0; i < form.elements.length; i++) {
      if( form.elements[i].type == "checkbox" ) {
        check_name = form.elements[i].name;
        if( array_name == check_name.substring(0, array_name.length) ) {
          form.elements[i].checked = g_checkbox2?"":"on";
        }
      }
    }
    g_checkbox2 ^= 1;
  }
</script>
</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF" onload="SetFocus();">
<!-- header //-->
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<!-- header_eof //-->

<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
    <td width="<?php echo BOX_WIDTH; ?>" valign="top"><table border="0" width="<?php echo BOX_WIDTH; ?>" cellspacing="1" cellpadding="1" class="columnLeft">
<!-- left_navigation //-->
<?php require(DIR_WS_INCLUDES . 'column_left.php'); ?>
<!-- left_navigation_eof //-->
    </table></td>
<!-- body_text //-->
    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
        <td width="100%"><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td class="pageHeading"><?php echo HEADING_TITLE; ?></td>
          </tr>
        </table></td>
      </tr>

      <tr>
        <td width="100%"><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td class="smallText"><?php echo TEXT_INFO; ?></td>
          </tr>
        </table></td>
      </tr>

      <tr>
        <td class="formArea"><?php echo tep_draw_form("add_range", FILENAME_NUMERIC_RANGES, 'action=add', 'post'); ?><table border="0" width="100%" cellspacing="1" cellpadding="3">
          <tr class="dataTableHeadingRow">
            <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_MIN; ?></td>
            <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_MAX; ?></td>
            <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_DESC; ?></td>
            <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_ORDER; ?></td>
          </tr>

          <tr>
            <td class="dataTableContent"><?php echo tep_draw_input_field('field[min]', '', 'size=6', false, 'text', true); ?></td>
            <td class="dataTableContent"><?php echo tep_draw_input_field('field[max]', '', 'size=6', false, 'text', true); ?></td>
            <td class="dataTableContent"><?php echo tep_draw_input_field('field[desc]', '', 'size=60', false, 'text', true); ?></td>
            <td class="dataTableContent" align="center"><?php echo tep_draw_input_field('field[sort_id]', '', 'size=5', false, 'text', true); ?></td>
          </tr>
          <tr>
            <td colspan="4"><hr class="formAreaBorder" /></td>
          </tr>
          <tr colspan="4">
            <td class="dataTableHeadingContent"><?php echo tep_image_submit('button_insert.gif',IMAGE_ADD_FIELD); ?></td>
          </tr>
        </table></form></td>
      </tr>
      <tr>
        <td><?php echo tep_draw_separator('pixel_trans.gif', '100%', '10'); ?></td>
      </tr>
      <tr>
        <td class="formArea"><?php echo tep_draw_form("list_ranges", FILENAME_NUMERIC_RANGES,'action=update','post') ?><table border="0" width="100%" cellspacing="1" cellpadding="3">
          <tr class="dataTableHeadingRow">
            <td class="dataTableHeadingContent" width="5%"><?php echo '<a href="javascript:void(0)" onClick="copy_checkboxes(document.list_ranges,\'mark\')" title="Page Select On/Off" class="menuBoxHeadingLink"><span class="dataTableHeadingContent">' . tep_image(DIR_WS_ICONS . 'tick.gif', 'Page Select On/Off') . '</span></a>'; ?></td>
            <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_MIN; ?></td>
            <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_MAX; ?></td>
            <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_DESC; ?></td>
            <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_ORDER; ?></td>
            <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_STATUS; ?></td>
          </tr>
<?php
  $numeric_ranges_query = tep_db_query("SELECT numeric_ranges_id, numeric_ranges_desc, numeric_ranges_min, numeric_ranges_max, sort_id, status_id from " . TABLE_NUMERIC_RANGES . " order by sort_id");
  while ($numeric_ranges_array = tep_db_fetch_array($numeric_ranges_query)) {
?>
          <tr>
            <td width="20"><?php echo tep_draw_checkbox_field('mark['.$numeric_ranges_array['numeric_ranges_id'].']', 1) ?></td>
            <td class="dataTableContent"><?php echo tep_draw_input_field('field['.$numeric_ranges_array['numeric_ranges_id'].'][min]', $numeric_ranges_array['numeric_ranges_min'], 'size=6', false, 'text', true);?></td>
            <td class="dataTableContent"><?php echo tep_draw_input_field('field['.$numeric_ranges_array['numeric_ranges_id'].'][max]', $numeric_ranges_array['numeric_ranges_max'], 'size=6', false, 'text', true);?></td>
            <td class="dataTableContent"><?php echo tep_draw_input_field('field['.$numeric_ranges_array['numeric_ranges_id'].'][desc]', $numeric_ranges_array['numeric_ranges_desc'], 'size=60', false, 'text', true);?></td>
            <td class="dataTableContent" align="center"><?php echo tep_draw_input_field('field['.$numeric_ranges_array['numeric_ranges_id'].'][sort_id]', $numeric_ranges_array['sort_id'], 'size=5', false, 'text', true);?></td>
            <td class="dataTableContent" align="center">
<?php
    if ($numeric_ranges_array['status_id'] == '1') {
      echo tep_image(DIR_WS_IMAGES . 'icon_status_green.gif', IMAGE_ICON_STATUS_GREEN, 10, 10) . '&nbsp;&nbsp;<a href="' . tep_href_link(FILENAME_NUMERIC_RANGES, 'action=setflag&flag=0&id=' . $numeric_ranges_array['numeric_ranges_id'], 'NONSSL') . '">' . tep_image(DIR_WS_IMAGES . 'icon_status_red_light.gif', IMAGE_ICON_STATUS_RED_LIGHT, 10, 10) . '</a>';
    }
    else {
      echo '<a href="' . tep_href_link(FILENAME_NUMERIC_RANGES, 'action=setflag&flag=1&id=' . $numeric_ranges_array['numeric_ranges_id'], 'NONSSL') . '">' . tep_image(DIR_WS_IMAGES . 'icon_status_green_light.gif', IMAGE_ICON_STATUS_GREEN_LIGHT, 10, 10) . '</a>&nbsp;&nbsp;' . tep_image(DIR_WS_IMAGES . 'icon_status_red.gif', IMAGE_ICON_STATUS_RED, 10, 10);
    }
?>
            </td>
          </tr>
<?php
  } 
?>
          <tr>
            <td colspan="6"><hr class="formAreaBorder" /></td>
          </tr>
          <tr>
            <td colspan="6"><?php echo tep_image_submit('button_update.gif',IMAGE_UPDATE_FIELDS, 'name="update"') . '&nbsp;&nbsp;' . tep_image_submit('button_delete.gif',IMAGE_REMOVE_FIELDS,'name="remove"'); ?></td>
          </tr>
        </table></form></td>
      </tr>
    </table></td>
<!-- body_text_eof //-->
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
