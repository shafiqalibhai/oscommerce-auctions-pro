<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2007 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Abstract Zones component for osCommerce Admin
// Controls relationships among different types, invokes selected class
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/

  require('includes/application_top.php');
  require(DIR_WS_CLASSES . FILENAME_ABSTRACT_ZONES);
// initialize the abstract zone class for different type support
  if( isset($_GET['zID']) && tep_not_null($_GET['zID']) ) {
    $azone_query = tep_db_query("select at.abstract_types_class from " . TABLE_ABSTRACT_ZONES . " az left join " . TABLE_ABSTRACT_TYPES . " at on (az.abstract_types_id=at.abstract_types_id) where az.abstract_zone_id = '" . (int)$_GET['zID'] . "'");
    if( $azone = tep_db_fetch_array($azone_query) ) {
      $azone_script = $azone['abstract_types_class'];
    }
  }

  if( isset($azone_script) && tep_not_null($azone_script) ) {
    require(DIR_WS_CLASSES . $azone_script . '.php');
  } else {
    $azone_script = 'abstract_zones';
  }

  $cAbstract = new $azone_script();
  $cAbstract->process_saction();
  $cAbstract->process_action();

?>
<?php require('includes/objects/html_start_sub1.php'); ?>
<script language="javascript" src="includes/general.js"></script>
<?php
  $set_focus = true;
  require('includes/objects/html_start_sub2.php'); 
?>
<?php 
  $heading_row = true;
  require('includes/objects/html_body_header.php'); 
?>
      <tr>
        <td colspan="2" width="100%"><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td class="pageHeading">
<?php 
  if (isset($_GET['zID']) && tep_not_null($_GET['zID']) ) {
    echo $cAbstract->get_zone_type($_GET['zID']) . '&nbsp;&raquo;&nbsp;' . $cAbstract->get_zone_name($_GET['zID']);
  } else {
    echo HEADING_TITLE;
  }
?>
            </td>
          </tr>
          <tr>
            <td><?php echo tep_draw_separator('pixel_trans.gif', '100%', '10'); ?></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td valign="top" width="75%"><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="0">
<?php
  echo $cAbstract->display_html();
?>
            </table></td>
          </tr>
        </table></td>
<?php
  echo $cAbstract->display_right_box();
?>
      </tr>
<?php require('includes/objects/html_end.php'); ?>
