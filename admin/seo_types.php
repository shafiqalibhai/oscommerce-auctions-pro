<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2008 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// SEO-G Class Types for the SEO-G Zones component for osCommerce Admin
//----------------------------------------------------------------------------
// E-Commerce Engine Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
  require('includes/application_top.php');

  $action = (isset($_GET['action']) ? $_GET['action'] : '');
  if (isset($_POST['remove_x']) || isset($_POST['remove_y'])) $action='remove';

  switch ($action) {
    case 'setflag':
      $sql_data_array = array('seo_types_status' => tep_db_prepare_input($_GET['flag']));
      tep_db_perform(TABLE_SEO_TYPES, $sql_data_array, 'update', 'seo_types_id=' . $_GET['id']);
      tep_redirect(tep_href_link(FILENAME_SEO_TYPES));
      break;
    case 'add':
      $sql_data_array = array(
                              'seo_types_name' => tep_db_prepare_input($_POST['name']),
                              'seo_types_handler' => tep_db_prepare_input($_POST['handler']),
                              'seo_types_subfix' => tep_db_prepare_input($_POST['subfix']),
                              'seo_types_class' => tep_db_prepare_input($_POST['class']),
                              'seo_types_prefix' => tep_db_prepare_input($_POST['prefix']),
                              'sort_order' => (int)($_POST['sort']),
                              'seo_types_linkage' => (int)($_POST['link'])
                             );

      tep_db_perform(TABLE_SEO_TYPES, $sql_data_array, 'insert');
      tep_redirect(tep_href_link(FILENAME_SEO_TYPES));
      break;
    case 'update':
      if( isset($_POST['mark']) ) {
        foreach ($_POST['mark'] as $key=>$val) {
          $sql_data_array = array(
                                  'seo_types_name' => tep_db_prepare_input($_POST['name'][$key]),
                                  'seo_types_handler' => tep_db_prepare_input($_POST['handler'][$key]),
                                  'seo_types_subfix' => tep_db_prepare_input($_POST['subfix'][$key]),
                                  'seo_types_class' => tep_db_prepare_input($_POST['class'][$key]),
                                  'seo_types_prefix' => tep_db_prepare_input($_POST['prefix'][$key]),
                                  'sort_order' => (int)($_POST['sort'][$key]),
                                  'seo_types_linkage' => (int)($_POST['link'][$key])
                                 );
          tep_db_perform(TABLE_SEO_TYPES, $sql_data_array, 'update', 'seo_types_id= ' . $key);
        }
      }
      tep_redirect(tep_href_link(FILENAME_SEO_TYPES));
      break;
    case 'remove':
      if( is_array($_POST['mark']) ) {
        foreach ($_POST['mark'] as $key=>$val) {
          tep_db_query("delete from " . TABLE_SEO_TYPES . " where seo_types_id='" . tep_db_input($key) . "'");
        }
        tep_redirect(tep_href_link(FILENAME_SEO_TYPES));
      }
      break;
    default:
      break;
  }
?>

<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<script language="javascript" src="includes/general.js"></script>
<script language="javascript">
  var g_checkbox2 = 0;
  function copy_checkboxes(form, array_name) {
    for (var i = 0; i < form.elements.length; i++) {
      if( form.elements[i].type == "checkbox" ) {
        check_name = form.elements[i].name;
        if( array_name == check_name.substring(0, array_name.length) ) {
          form.elements[i].checked = g_checkbox2?"":"on";
        }
      }
    }
    g_checkbox2 ^= 1;
  }
</script>
</head>
<body onload="SetFocus();">
<!-- header //-->
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<!-- header_eof //-->

<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
    <td width="<?php echo BOX_WIDTH; ?>" valign="top"><table border="0" width="<?php echo BOX_WIDTH; ?>" cellspacing="1" cellpadding="1" class="columnLeft">
<!-- left_navigation //-->
<?php require(DIR_WS_INCLUDES . 'column_left.php'); ?>
<!-- left_navigation_eof //-->
    </table></td>
<!-- body_text //-->
    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td class="pageHeading"><?php echo HEADING_SEO_TYPES_ADD; ?></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td class="smallText"><?php echo TEXT_INFO_INSERT; ?></td>
      </tr>
      <tr>
        <td class="formArea"><?php echo tep_draw_form("add_field", FILENAME_SEO_TYPES, 'action=add', 'post'); ?><table border="0" width="100%" cellspacing="1" cellpadding="3">
          <tr class="dataTableHeaderForm">
            <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_SEO_NAME; ?></td>
            <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_SEO_HANDLER; ?></td>
            <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_SEO_SUBFIX; ?></td>
            <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_SEO_CLASS; ?></td>
            <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_SEO_PREFIX; ?></td>
            <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_SORT_ORDER; ?></td>
            <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_SEO_LINKAGE; ?></td>
          </tr>
          <tr>
            <td class="dataTableContent"><?php echo tep_draw_input_field('name', '', '', false, 'text', true); ?></td>
            <td class="dataTableContent"><?php echo tep_draw_input_field('handler', '', '', false, 'text', true); ?></td>
            <td class="dataTableContent"><?php echo tep_draw_input_field('subfix', '', '', false, 'text', true); ?></td>
            <td class="dataTableContent"><?php echo tep_draw_input_field('class', '', '', false, 'text', true) . '.php'; ?></td>
            <td class="dataTableContent"><?php echo tep_draw_input_field('prefix', '', '', false, 'text', true); ?></td>
            <td class="dataTableContent"><?php echo tep_draw_input_field('sort', '', 'size=3', false, 'text', true); ?></td>
            <td class="dataTableContent"><?php echo tep_draw_input_field('link', '', 'size=3', false, 'text', true); ?></td>
          </tr>
          <tr>
            <td colspan="7"><hr class="formAreaBorder" /></td>
          </tr>
          <tr>
            <td colspan="7" class="dataTableHeadingContent"><?php echo tep_image_submit('button_add_field.gif', IMAGE_ADD_FIELD); ?></td>
          </tr>
        </table></form>
      </tr>
      <tr>
        <td><?php echo tep_draw_separator('pixel_trans.gif', '100%', '10'); ?></td>
      </tr>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td class="pageHeading"><?php echo HEADING_SEO_TYPES_UPDATE; ?></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td class="smallText"><?php echo TEXT_INFO_UPDATE; ?></td>
      </tr>
      <tr>
        <td class="formarea"><?php echo tep_draw_form('seo_types', FILENAME_SEO_TYPES,'action=update', 'post'); ?><table border="0" width="100%" cellspacing="1" cellpadding="3">
          <tr class="dataTableHeaderForm">
            <td class="dataTableHeadingContent" width="5%"><?php echo '<a href="javascript:void(0)" onClick="copy_checkboxes(document.seo_types,\'mark\')" title="Page Select On/Off" class="menuBoxHeadingLink"><span class="dataTableHeadingContent">' . tep_image(DIR_WS_ICONS . 'tick.gif', 'Page Select On/Off') . '</span></a>'; ?></td>
            <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_SEO_NAME; ?></td>
            <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_SEO_HANDLER; ?></td>
            <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_SEO_SUBFIX; ?></td>
            <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_SEO_CLASS; ?></td>
            <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_SEO_PREFIX; ?></td>
            <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_SORT_ORDER; ?></td>
            <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_SEO_LINKAGE; ?></td>
            <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_STATUS; ?></td>
          </tr>
<?php
  $seo_types_query = tep_db_query("select at.* from " . TABLE_SEO_TYPES . " at order by at.sort_order");
  while ($seo_types = tep_db_fetch_array($seo_types_query)) {
?>
          <tr>
            <td width="20"><?php echo tep_draw_checkbox_field('mark['.$seo_types['seo_types_id'].']', 1) ?></td>
            <td class="dataTableContent"><?php echo tep_draw_input_field('name[' . $seo_types['seo_types_id'] . ']', $seo_types['seo_types_name'], '', false, 'text', true); ?></td>
            <td class="dataTableContent"><?php echo tep_draw_input_field('handler[' . $seo_types['seo_types_id'] . ']', $seo_types['seo_types_handler'], '', false, 'text', true); ?></td>
            <td class="dataTableContent"><?php echo tep_draw_input_field('subfix[' . $seo_types['seo_types_id'] . ']', $seo_types['seo_types_subfix'], '', false, 'text', true); ?></td>
            <td class="dataTableContent"><?php echo tep_draw_input_field('class[' . $seo_types['seo_types_id'] . ']', $seo_types['seo_types_class'], '', false, 'text', true) . '.php'; ?></td>
            <td class="dataTableContent"><?php echo tep_draw_input_field('prefix[' . $seo_types['seo_types_id'] . ']', $seo_types['seo_types_prefix'], '', false, 'text', true); ?></td>
            <td class="dataTableContent"><?php echo tep_draw_input_field('sort[' . $seo_types['seo_types_id'] . ']', $seo_types['sort_order'], 'size=3', false, 'text', true); ?></td>
            <td class="dataTableContent"><?php echo tep_draw_input_field('link['.$seo_types['seo_types_id'] . ']', $seo_types['seo_types_linkage'], 'size=3', false, 'text', true); ?></td>
            <td class="dataTableContent" align="center">
<?php
    if ($seo_types['seo_types_status'] == '1') {
      echo tep_image(DIR_WS_IMAGES . 'icon_status_green.gif', IMAGE_ICON_STATUS_GREEN, 10, 10) . '&nbsp;&nbsp;<a href="' . tep_href_link(FILENAME_SEO_TYPES, 'action=setflag&flag=0&id=' . $seo_types['seo_types_id'], 'NONSSL') . '">' . tep_image(DIR_WS_IMAGES . 'icon_status_red_light.gif', IMAGE_ICON_STATUS_RED_LIGHT, 10, 10) . '</a>';
    } else {
      echo '<a href="' . tep_href_link(FILENAME_SEO_TYPES, 'action=setflag&flag=1&id=' . $seo_types['seo_types_id'], 'NONSSL') . '">' . tep_image(DIR_WS_IMAGES . 'icon_status_green_light.gif', IMAGE_ICON_STATUS_GREEN_LIGHT, 10, 10) . '</a>&nbsp;&nbsp;' . tep_image(DIR_WS_IMAGES . 'icon_status_red.gif', IMAGE_ICON_STATUS_RED, 10, 10);
    }
?>
            </td>
          </tr>
<?php
  } 
?>
          <tr>
            <td colspan="9"><hr class="formAreaBorder" /></td>
          </tr>
          <tr>
            <td colspan="9"><?php echo tep_image_submit('button_update_fields.gif',IMAGE_UPDATE_FIELDS, 'name="update"') . '&nbsp;' . tep_image_submit('button_remove_fields.gif',IMAGE_REMOVE_FIELDS,'name="remove"') ?></td>
          </tr>
        </table></form></td>
      </tr>
    </table></td>
<!-- body_text_eof //-->
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
<br />
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
