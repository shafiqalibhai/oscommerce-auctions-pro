<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2007 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Total Configuration module for osCommerce Admin
// Based on admin\configuration.php v1.43
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// http://www.oscommerce.com
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/

  require('includes/application_top.php');
  require(DIR_WS_CLASSES . 'currencies.php');
  $currencies = new currencies();

  $action = (isset($_GET['action']) ? $_GET['action'] : '');
  $gID = (isset($_GET['gID']) ? $_GET['gID'] : '');

  switch($action) {
    case 'save':
      $configuration_value = tep_db_prepare_input($_POST['configuration_value']);
      $cID = tep_db_prepare_input($_GET['cID']);

      tep_db_query("update " . TABLE_CONFIGURATION . " set configuration_value = '" . tep_db_input($configuration_value) . "', last_modified = now() where configuration_id = '" . (int)$cID . "'");

      tep_redirect(tep_href_link(FILENAME_TOTAL_CONFIGURATION, 'gID=' . $_GET['gID'] . '&cID=' . $cID));
      break;
    case 'modify_confirm':
      if( $_POST['sort_duplicates'] == 'on' ) {
        $duplicates_query = tep_db_query("select configuration_key, configuration_id, count(*) as total from " . TABLE_CONFIGURATION . " group by configuration_key having count(*) > 1");
        $duplicates_array = array();
        while($duplicates = tep_db_fetch_array($duplicates_query) ) {
          $duplicates_array[$duplicates['configuration_key']] = $duplicates;
        }

        foreach($duplicates_array as $key => $value) {
          tep_db_query("delete from " . TABLE_CONFIGURATION . " where configuration_id = '" . (int)$value['configuration_id'] ."'");
        }
      }
      if($_POST['sort_config'] == 'on') {
        tep_db_query("alter table " . TABLE_CONFIGURATION . " drop configuration_id");
        tep_db_query("alter table " . TABLE_CONFIGURATION . " add configuration_id INT( 11 ) not null auto_increment first, add primary key (configuration_id)");
      }
      tep_redirect(tep_href_link(FILENAME_TOTAL_CONFIGURATION));
      break;
     case 'modify':
      if( $_POST['sort_duplicates'] == '0' && $_POST['sort_config'] == '0' ) {
        $action = '';
        break;
      }
      break;
    default:
      break;
  }

  $cfg_group_query = tep_db_query("select distinct configuration_group_id as id, CONCAT('Group','-',configuration_group_id) as text from " . TABLE_CONFIGURATION . " order by configuration_group_id");
  $group_array = array(
                       array('id' => '0', 'text' => 'Show All')
                      );

  while($group_array[]=tep_db_fetch_array($cfg_group_query));
  array_pop($group_array);
  if( $gID != 0 ) {
    $cfg_group_query = tep_db_query("select configuration_group_title from " . TABLE_CONFIGURATION_GROUP . " where configuration_group_id = '" . (int)$gID . "'");
    if( $cfg_group = tep_db_fetch_array($cfg_group_query) ) {
      $group_name = $cfg_group['configuration_group_title'];
    } else {
      $group_name = 'Unnamed Group-ID=' . $gID;
    }
  }
?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<script language="javascript" src="includes/general.js"></script>
</head>
<body onload="SetFocus();">
<!-- header //-->
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<!-- header_eof //-->

<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
    <td width="<?php echo BOX_WIDTH; ?>" valign="top"><table border="0" width="<?php echo BOX_WIDTH; ?>" cellspacing="1" cellpadding="1" class="columnLeft">
<!-- left_navigation //-->
<?php require(DIR_WS_INCLUDES . 'column_left.php'); ?>
<!-- left_navigation_eof //-->
    </table></td>
<!-- body_text //-->
    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
<?php
  // Modify/Confirm screen
  if($action == 'modify' && $gID == 0) {
?>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td class="pageHeading"><?php echo HEADING_CONFIRM; ?></td>
          </tr>
          <tr>
            <td><hr /></td>
          </tr>
          <tr>
            <td><?php echo tep_draw_form('global_form', FILENAME_TOTAL_CONFIGURATION, tep_get_all_get_params(array('action, gID')) . 'action=modify_confirm', 'post') ?><table border="0" width="100%" cellspacing="0" cellpadding="0">
<?php
    if($_POST['sort_duplicates'] == 'on') {
?>
              <tr>
                <td class="smallText"><?php echo TEXT_INFO_CONFIRM_DUPLICATES; ?></td>
              </tr>
              <tr>
                <td><?php echo tep_draw_separator('pixel_trans.gif', '100%', '8'); ?></td>
              </tr>
              <tr>
                <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
                  <tr class="dataTableHeadingRow">
                    <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_CONFIGURATION_ID; ?></td>
                    <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_CONFIGURATION_KEY; ?></td>
                  </tr>
<?php
      $duplicates_query = tep_db_query("select configuration_key, configuration_id, count(*) as total from " . TABLE_CONFIGURATION . " group by configuration_key having count(*) > 1");
      $duplicates_array = array();

      while($duplicates = tep_db_fetch_array($duplicates_query) ) {
        $duplicates_array[$duplicates['configuration_key']] = $duplicates;
      }
      foreach($duplicates_array as $key => $value) {
?>
                  <tr class="dataTableRow">
                    <td class="dataTableContent"><?php echo $value['configuration_id']; ?></td>
                    <td class="dataTableContent"><?php echo $value['configuration_key'];; ?></td>
                  </tr>
<?php
      }
?>
                </table></td>
              </tr>
              <tr>
                <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '8'); ?></td>
              </tr>
<?php 
    }
    if($_POST['sort_config'] == 'on') {
?>
              <tr>
                <td class="smallText"><b><?php echo TEXT_INFO_CONFIRM_CONFIG; ?></b></td>
              </tr>
<?php
    }
    foreach( $_POST as $key => $value ) {
      echo tep_draw_hidden_field($key, $value);
    }
?>
              <tr>
                <td><?php echo tep_draw_separator('pixel_trans.gif', '100%', '6'); ?></td>
              </tr>
              <tr>
                <td class="main"><?php echo tep_image_submit('button_confirm.gif', 'Confirm MySql Configuration Table changes') . ' <a href="' . tep_href_link(FILENAME_TOTAL_CONFIGURATION) .'">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>'; ?></td>
              </tr>
            </table></form></td>
          </tr>
        </table></td>
      </tr>
<?php
  // Show them all
  } elseif($gID == 0) {
?>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td class="pageHeading"><?php echo HEADING_ALL; ?></td>
            <td class="main" align="right">
<?php
    echo tep_draw_form('group', FILENAME_TOTAL_CONFIGURATION, '', 'get');
    echo HEADING_SELECT . '&nbsp;' . tep_draw_pull_down_menu('gID', $group_array, $gID, 'onChange="this.form.submit();"');
    echo '</form>';

/*
alter table " . TABLE_CONFIGURATION . " drop configuration_id;
alter table " . TABLE_CONFIGURATION . " add configuration_id INT( 11 ) not null auto_increment first, add primary key (configuration_id);
alter table test drop test_id
ALTER TABLE `test` DROP PRIMARY KEY, ADD PRIMARY KEY ( `test_id` ) 
alter table test add test_id INT( 11 ) not null auto_increment first, add primary key (test_id)
select configuration_id, configuration_key, count(*) as total from " . TABLE_CONFIGURATION . " group by configuration_key having count(*) > 1;

  CREATE TABLE tmp AS SELECT DISTINCT ON ( x, y ) * FROM t;
  DROP TABLE t;
  ALTER TABLE tmp RENAME TO t;


  create table tmp as select distinct ON ( test_text ) * FROM test;
  DROP TABLE test;
  ALTER TABLE tmp RENAME TO test;

delete from test where (test_text) not in (SELECT DISTINCT ON (test_text) test_text FROM test_text ORDER BY test_text DESC)

*/
?>
            </td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><?php echo tep_draw_form('global_form', FILENAME_TOTAL_CONFIGURATION, 'action=modify', 'post') ?><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td class="smallText"><b><?php echo TABLE_HEADING_OPTIMIZE; ?></b></td>
          </tr>
          <tr>
            <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '6'); ?></td>
          </tr>
          <tr>
            <td class="formarea"><table border="0" width="100%" cellspacing="0" cellpadding="0">
              <tr>
                <td class="smallText">&nbsp;<?php echo 'No Changes&nbsp;' . tep_draw_radio_field('sort_config', '0', true); ?></td>
                <td class="smallText"><?php echo 'Enable&nbsp;' . tep_draw_radio_field('sort_config', 'on', false); ?></td>
                <td class="smallText"><b><?php echo TEXT_INFO_OPTIMIZE_SORT; ?></b></td>
              </tr>
              <tr>
                <td class="smallText">&nbsp;<?php echo 'No Changes&nbsp;' . tep_draw_radio_field('sort_duplicates', '0', true); ?></td>
                <td class="smallText"><?php echo 'Enable&nbsp;' . tep_draw_radio_field('sort_duplicates', 'on', false); ?></td>
                <td class="smallText"><b><?php echo TEXT_INFO_OPTIMIZE_DUPLICATES; ?></b></td>
              </tr>
            </table></td> 
          </tr>
          <tr>
            <td><?php echo tep_draw_separator('pixel_trans.gif', '100%', '6'); ?></td>
          </tr>
          <tr>
            <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
              <tr>
                <td class="smallText"><?php echo tep_image_submit('button_submit.gif', 'Process Global Options'); ?></td>
                <td><?php echo tep_draw_separator('pixel_trans.gif', '12', '1'); ?></td>
                <td class="smallText"><?php echo TEXT_INFO_OPERATION ?></td>
              </tr>
            </table></td>
          </tr>
        </table></form></td>
      </tr>
      <tr>
        <td><hr /></td>
      </tr>

<?php
    unset($group_array[0]);

    foreach($group_array as $key => $value) {
      $cfg_group_query = tep_db_query("select configuration_group_title from " . TABLE_CONFIGURATION_GROUP . " where configuration_group_id = '" . (int)$value['id'] . "'");
      if( tep_db_num_rows($cfg_group_query) ) {
        $cfg_group = tep_db_fetch_array($cfg_group_query);
        $group_name = $cfg_group['configuration_group_title'];
      } else {
        $group_name = 'Unnamed';
      }
?>
      <tr>
        <td><?php echo tep_draw_separator('pixel_trans.gif', '100%', '4'); ?></td>
      </tr>
      <tr>
        <td class="pageHeading"><?php echo $value['id'] . '.&nbsp;' . $group_name; ?></td>
      </tr>
      <tr>
        <td><?php echo tep_draw_separator('pixel_trans.gif', '100%', '4'); ?></td>
      </tr>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top"><table border="0" width="100%" cellspacing="1" cellpadding="3">
              <tr class="dataTableHeadingRow">
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_CONFIGURATION_ID; ?></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_CONFIGURATION_KEY; ?></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_CONFIGURATION_TITLE; ?></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_CONFIGURATION_VALUE; ?></td>
                <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_ACTION; ?>&nbsp;</td>
              </tr>
<?php
      $configuration_query = tep_db_query("select c.configuration_id, c.configuration_group_id, c.configuration_key, c.configuration_title, c.configuration_value, c.use_function from " . TABLE_CONFIGURATION . " c where c.configuration_group_id = '" . (int)$value['id'] . "' order by c.configuration_key");
      while ($configuration = tep_db_fetch_array($configuration_query)) {
        if (tep_not_null($configuration['use_function'])) {
          $use_function = $configuration['use_function'];
          if (preg_match('/->/', $use_function)) {
            $class_method = explode('->', $use_function);
            if (!is_object(${$class_method[0]})) {
              include(DIR_WS_CLASSES . $class_method[0] . '.php');
              ${$class_method[0]} = new $class_method[0]();
            }
            $cfgValue = tep_call_function($class_method[1], $configuration['configuration_value'], ${$class_method[0]});
          } else {
            $cfgValue = tep_call_function($use_function, $configuration['configuration_value']);
          }
        } else {
          $cfgValue = $configuration['configuration_value'];
        }

        if ((!isset($_GET['cID']) || (isset($_GET['cID']) && ($_GET['cID'] == $configuration['configuration_id']))) && !isset($cInfo) && (substr($action, 0, 3) != 'new')) {
          $cfg_extra_query = tep_db_query("select configuration_key, configuration_description, date_added, last_modified, use_function, set_function from " . TABLE_CONFIGURATION . " where configuration_id = '" . (int)$configuration['configuration_id'] . "'");
          $cfg_extra = tep_db_fetch_array($cfg_extra_query);

          $cInfo_array = array_merge($configuration, $cfg_extra);
          $cInfo = new objectInfo($cInfo_array);
        }

        if ( (isset($cInfo) && is_object($cInfo)) && ($configuration['configuration_id'] == $cInfo->configuration_id) ) {
          echo '                  <tr id="defaultSelected" class="dataTableRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . tep_href_link(FILENAME_TOTAL_CONFIGURATION, 'gID=' . $configuration['configuration_group_id'] . '&cID=' . $cInfo->configuration_id . '&action=edit') . '\'">' . "\n";
        } else {
          echo '                  <tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . tep_href_link(FILENAME_TOTAL_CONFIGURATION, 'gID=' . $configuration['configuration_group_id'] . '&cID=' . $configuration['configuration_id']) . '\'">' . "\n";
        }
?>
                <td class="dataTableContent"><?php echo $configuration['configuration_id']; ?></td>
                <td class="dataTableContent"><?php echo $configuration['configuration_key']; ?></td>
                <td class="dataTableContent"><?php echo $configuration['configuration_title']; ?></td>
                <td class="dataTableContent"><?php echo htmlspecialchars($cfgValue); ?></td>
                <td class="dataTableContent" align="right"><?php if ( (isset($cInfo) && is_object($cInfo)) && ($configuration['configuration_id'] == $cInfo->configuration_id) ) { echo tep_image(DIR_WS_IMAGES . 'icon_arrow_right.gif', ''); } else { echo '<a href="' . tep_href_link(FILENAME_TOTAL_CONFIGURATION, 'gID=' . $configuration['configuration_group_id'] . '&cID=' . $configuration['configuration_id'] . '&action=edit') . '">' . tep_image(DIR_WS_ICONS . 'edit.gif', IMAGE_ICON_INFO) . '</a>'; } ?>&nbsp;</td>
              </tr>
<?php
      }
?>
            </table></td>
          </tr>
          <tr>
            <td><hr /></td>
          </tr>
        </table></td>
      </tr>
<?php
    }
  // Show selected group
  } else {
?>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td class="pageHeading"><?php echo HEADING_TITLE . '&nbsp;&raquo;&nbsp;' . $group_name; ?></td>
            <td class="main" align="right">
<?php
    echo tep_draw_form('group', FILENAME_TOTAL_CONFIGURATION, '', 'get');
    echo HEADING_SELECT . '&nbsp;' . tep_draw_pull_down_menu('gID', $group_array, $gID, 'onChange="this.form.submit();"');
    echo '</form>';
?>
            </td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top"><table border="0" width="100%" cellspacing="1" cellpadding="3">
              <tr class="dataTableHeadingRow">
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_CONFIGURATION_ID; ?></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_CONFIGURATION_KEY; ?></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_CONFIGURATION_TITLE; ?></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_CONFIGURATION_VALUE; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_EDIT; ?>&nbsp;</td>
                <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_ACTION; ?>&nbsp;</td>
              </tr>
<?php
  $configuration_query = tep_db_query("select c.configuration_id, c.configuration_key, c.configuration_title, c.configuration_value, c.use_function from " . TABLE_CONFIGURATION . " c where c.configuration_group_id = '" . (int)$gID . "' order by c.configuration_key");
  while ($configuration = tep_db_fetch_array($configuration_query)) {
    if (tep_not_null($configuration['use_function'])) {
      $use_function = $configuration['use_function'];
      if (preg_match('/->/', $use_function)) {
        $class_method = explode('->', $use_function);
        if (!is_object(${$class_method[0]})) {
          include(DIR_WS_CLASSES . $class_method[0] . '.php');
          ${$class_method[0]} = new $class_method[0]();
        }
        $cfgValue = tep_call_function($class_method[1], $configuration['configuration_value'], ${$class_method[0]});
      } else {
        $cfgValue = tep_call_function($use_function, $configuration['configuration_value']);
      }
    } else {
      $cfgValue = $configuration['configuration_value'];
    }

    if ((!isset($_GET['cID']) || (isset($_GET['cID']) && ($_GET['cID'] == $configuration['configuration_id']))) && !isset($cInfo) && (substr($action, 0, 3) != 'new')) {
      $cfg_extra_query = tep_db_query("select configuration_key, configuration_description, date_added, last_modified, use_function, set_function from " . TABLE_CONFIGURATION . " where configuration_id = '" . (int)$configuration['configuration_id'] . "'");
      $cfg_extra = tep_db_fetch_array($cfg_extra_query);

      $cInfo_array = array_merge($configuration, $cfg_extra);
      $cInfo = new objectInfo($cInfo_array);
    }

    if ( (isset($cInfo) && is_object($cInfo)) && ($configuration['configuration_id'] == $cInfo->configuration_id) ) {
      echo '                  <tr id="defaultSelected" class="dataTableRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . tep_href_link(FILENAME_TOTAL_CONFIGURATION, 'gID=' . $gID . '&cID=' . $cInfo->configuration_id . '&action=edit') . '\'">' . "\n";
    } else {
      echo '                  <tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . tep_href_link(FILENAME_TOTAL_CONFIGURATION, 'gID=' . $gID . '&cID=' . $configuration['configuration_id']) . '\'">' . "\n";
    }
?>
                <td class="dataTableContent"><?php echo $configuration['configuration_id']; ?></td>
                <td class="dataTableContent"><?php echo $configuration['configuration_key']; ?></td>
                <td class="dataTableContent"><?php echo $configuration['configuration_title']; ?></td>
                <td class="dataTableContent"><?php echo htmlspecialchars($cfgValue); ?></td>
                <td class="dataTableContent" align="center"><?php if ( (isset($cInfo) && is_object($cInfo)) && ($configuration['configuration_id'] == $cInfo->configuration_id) ) { echo '&nbsp;'; } else { echo '<a href="' . tep_href_link(FILENAME_TOTAL_CONFIGURATION, 'gID=' . $gID . '&cID=' . $configuration['configuration_id'] . '&action=edit') . '">' . tep_image(DIR_WS_ICONS . 'edit.gif', IMAGE_ICON_INFO) . '</a>'; } ?></td>
                <td class="dataTableContent" align="right"><?php if ( (isset($cInfo) && is_object($cInfo)) && ($configuration['configuration_id'] == $cInfo->configuration_id) ) { echo tep_image(DIR_WS_IMAGES . 'icon_arrow_right.gif', ''); } else { echo '<a href="' . tep_href_link(FILENAME_TOTAL_CONFIGURATION, 'gID=' . $gID . '&cID=' . $configuration['configuration_id']) . '">' . tep_image(DIR_WS_IMAGES . 'icon_info.gif', IMAGE_ICON_INFO) . '</a>'; } ?>&nbsp;</td>
              </tr>
<?php
    }
?>
            </table></td>
<?php
    $heading = array();
    $contents = array();

    switch ($action) {
      case 'edit':
        $heading[] = array('text' => '<b>' . $cInfo->configuration_title . '</b>');

        if ($cInfo->set_function) {
          eval('$value_field = ' . $cInfo->set_function . '"' . htmlspecialchars($cInfo->configuration_value) . '");');
        } else {
          $value_field = tep_draw_input_field('configuration_value', $cInfo->configuration_value);
        }

        $contents = array('form' => tep_draw_form('configuration', FILENAME_TOTAL_CONFIGURATION, 'gID=' . $gID . '&cID=' . $cInfo->configuration_id . '&action=save'));
        $contents[] = array('text' => TEXT_INFO_EDIT_INTRO);
        $contents[] = array('text' => '<br><b>' . $cInfo->configuration_title . '</b><br>' . $cInfo->configuration_description . '<br>' . $value_field);
        $contents[] = array('align' => 'center', 'text' => '<br>' . tep_image_submit('button_update.gif', IMAGE_UPDATE) . '&nbsp;<a href="' . tep_href_link(FILENAME_TOTAL_CONFIGURATION, 'gID=' . $gID . '&cID=' . $cInfo->configuration_id) . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>');
        break;
      default:
        if (isset($cInfo) && is_object($cInfo)) {
          $heading[] = array('text' => '<b>' . $cInfo->configuration_title . '</b>');

          $contents[] = array('align' => 'center', 'text' => '<a href="' . tep_href_link(FILENAME_TOTAL_CONFIGURATION, 'gID=' . $gID . '&cID=' . $cInfo->configuration_id . '&action=edit') . '">' . tep_image_button('button_edit.gif', IMAGE_EDIT) . '</a>');
          $contents[] = array('text' => '<br>' . $cInfo->configuration_description);
          $contents[] = array('text' => '<br>' . TEXT_INFO_DATE_ADDED . ' ' . tep_date_short($cInfo->date_added));
          if (tep_not_null($cInfo->last_modified)) $contents[] = array('text' => TEXT_INFO_LAST_MODIFIED . ' ' . tep_date_short($cInfo->last_modified));
        }
        break;
    }

    if ( (tep_not_null($heading)) && (tep_not_null($contents)) ) {
      echo '            <td width="25%" valign="top">' . "\n";

      $box = new box;
      echo $box->infoBox($heading, $contents);

      echo '            </td>' . "\n";
    }
?>
          </tr>
        </table></td>
      </tr>
<?php
  }
?>
    </table></td>
<!-- body_text_eof //-->
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
<br>
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
