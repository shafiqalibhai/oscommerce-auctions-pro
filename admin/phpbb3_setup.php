<?php
require('includes/application_top.php');
set_time_limit (0);
ini_set('display_errors', 1);

$action = (isset($_REQUEST['action']) ? $_REQUEST['action'] : '');
$next_step = 'Start';
require(DIR_WS_INCLUDES . 'template_top.php');


?><h1>phpBB3 <-> osCommerce Bridge Setup</h1><?php
echo tep_draw_form('oscphp_setup', 'phpbb3_setup.php', '', 'POST', '');

if (tep_not_null($action)) {
	switch ($action) {
		case 'Continue to Step 1': case 'Return to Step 1':
			// Insert configuration values
			?>
			<h3>Inserting configuration values into database</h3>
			<ul>
			<?php
			
			// STORE_URL
			$existing_query = tep_db_query("SELECT count(*) AS total FROM " . TABLE_CONFIGURATION . " WHERE configuration_key = 'STORE_URL'");
			$existing_value = tep_db_fetch_array($existing_query);
			if ($existing_value['total'] > 0) {
				?><li>'STORE_URL' already exists in configuration table</li><?php
			} else {
				tep_db_query(
					"INSERT INTO " . TABLE_CONFIGURATION . " 
					(configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function)
					VALUES 
					(NULL, 'Store URL', 'STORE_URL', '', '" . tep_db_input ("The web address of my store (without 'http://' and without the trailing slash).") . "', 1, 2, NULL, NOW(), NULL, NULL)");
				?><li>'STORE_URL' added to configuration table</li><?php
			 }
			// STORE_SLOGAN
			$existing_query = tep_db_query("SELECT count(*) AS total FROM " . TABLE_CONFIGURATION . " WHERE configuration_key = 'STORE_SLOGAN'");
			$existing_value = tep_db_fetch_array($existing_query);
			if ($existing_value['total'] > 0) {
				?><li>'STORE_SLOGAN' already exists in configuration table</li><?php
			} else {
				tep_db_query(
					"INSERT INTO " . TABLE_CONFIGURATION . " 
					(configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function)
					VALUES 
					(NULL, 'Slogan', 'STORE_SLOGAN', '', '" . tep_db_input ("The slogan of your store or business.") . "', 1, 2, NULL, NOW(), NULL, NULL)");
				?><li>'STORE_SLOGAN' added to configuration table</li><?php
			}
			$next_step = 'Continue to Step 2';

			?></ul><?php
		break;
		
		
		case 'Continue to Step 2': case 'Return to Step 2':
			// Try to see if we know where phpBB3 directory is. Check defines and database tables
			$errors = array();
			//Admin
			if (!defined('DIR_WS_FORUMS')) { $errors[] = 'DIR_WS_FORUMS is not defined in catalog/admin/includes/configure.php'; }
			if (!defined('DIR_FS_FORUMS_LOCATION')) { $errors[] = 'DIR_FS_FORUMS_LOCATION is not defined in catalog/admin/includes/configure.php'; }
			if (!defined('TABLE_PHPBB_USERS')) { $errors[] = 'TABLE_PHPBB_USERS is not defined in catalog/admin/includes/database_tables.php'; }
			if (!file_exists(DIR_WS_FORUMS.'includes/functions.php')) { $errors[] = 'DIR_WS_FORUMS is incorrectly defined in catalog/admin/includes/configure.php'; }
			if (!file_exists(DIR_FS_FORUMS_LOCATION.'includes/functions.php')) { $errors[] = 'DIR_FS_FORUMS_LOCATION is incorrectly defined in catalog/admin/includes/configure.php'; }
			
			if (count($errors) == 0) {
				?><h3>Your phpBB3forum directory looks fine!</h3>
				No further action needed.<br><?php
				$next_step = 'Continue to Step 3';
			} else {
				?><h3>Please use the installation instructions to update the following files</h3>
				Some values in these files are needed before continuing the setup script.
				<ol>
					<li>catalog/admin/includes/configure.php</li>
					<li>catalog/admin/includes/database_tables.php</li>
					<li>catalog/includes/configure.php</li>
					<li>catalog/includes/database_tables.php</li>
				</ol><?php
				echo implode($errors, '<br>');
				$next_step = 'Return to Step 2';
			}
		break;
		
		case 'Continue to Step 3': case 'Return to Step 3':
			?>
			<h3>Checking admin username/password between osComemrce and phpBB3</h3>
			<ul>
			<?php
			// Default to osCommerce admin
			$existing_query = tep_db_query("SELECT user_name FROM  " . TABLE_ADMINISTRATORS . " ORDER BY id ASC LIMIT 1");
			$existing_value = tep_db_fetch_array($existing_query); // Just use the first one
			?>
			<table>
			<tr> <td><label for "admin_name">phpBB Admin name</label></td> <td><input type="text" name="admin_name" value="<?=$existing_value['user_name']?>"></td> </tr>
			<tr> <td><label for "admin_email">phpBB Admin email</label></td> <td><input type="text" name="admin_email"></td> </tr>
			<tr> <td><label for "admin_password">phpBB Admin password</label></td> <td><input type="password" name="admin_password"></td> </tr>
			</table>
			<?php
			$next_step = 'Continue to Step 4';
			?></ul><?php
			
		break;
		
		
		case 'Continue to Step 4': case 'Return to Step 4':
			$admin_username = $_REQUEST['admin_name'];
			$admin_email    = $_REQUEST['admin_email'];
			$admin_password = $_REQUEST['admin_password'];
			?>
			<h3>Inserting admin configuration values into configuration table</h3>
			<table>
			<tr> <td><label for "admin_name">phpBB Admin name</label></td> <td><?php=$admin_username?></td> </tr>
			<tr> <td><label for "admin_email">phpBB Admin email</label></td> <td><?php=$admin_email?></td> </tr>
			<tr> <td><label for "admin_password">phpBB Admin password</label></td> <td>******</td> </tr>
			</table>
			<?php
			// First, we need to check that the phpBB admin password is correct.
			// In the past, MD5() was use in phpBB2.
			// Now it's more complicated, so we include ppBB3's own hashing functions
			
			define('IN_PHPBB', true);
			include_once(DIR_WS_FORUMS.'includes/functions.php');
			// Check that the phpBB admin exists, should be user_id=2 (default)
			$phpbb_admin_query = tep_db_query("SELECT user_id, user_type, username, username_clean, user_password, user_email FROM " . TABLE_PHPBB_USERS . " WHERE user_id = '2'");
			$phpbb_admin_array = tep_db_fetch_array($phpbb_admin_query);
			$errors = array();
			if ($admin_username != $phpbb_admin_array['username']) {
				$errors[] = "Admin with username '".$admin_username."' not found in phpBB table ".TABLE_PHPBB_USERS;
			}
			if ($admin_email != $phpbb_admin_array['user_email']) {
				$errors[] = "Admin with email '".$user_email."' not found in phpBB table ".TABLE_PHPBB_USERS;
			}
			if (!phpbb_check_hash(htmlentities($admin_password), $phpbb_admin_array['user_password'])) { // phpBB sanitizes inputs with htmlentities(and probably more) for passwords like '&123' into '&amp;123'. It was not easy learning this.
				$errors[] = "Admin with submitted password not found in phpBB table ".TABLE_PHPBB_USERS;
			}
			// Now get the admin from osCommerce
			$osC_admin_query = tep_db_query("SELECT user_name, user_password FROM " . TABLE_ADMINISTRATORS . " WHERE user_name = '" . tep_db_input($admin_username) . "'");
			$osC_admin_array = tep_db_fetch_array($osC_admin_query); 
			if ($admin_username != $osC_admin_array['user_name']) {
				$errors[] = "Admin with username '".$admin_username."' not found in osCommerce table ".TABLE_ADMINISTRATORS;
			}
			require('includes/functions/password_funcs.php');
			if (!tep_validate_password($admin_password, $osC_admin_array['user_password'])) {
				$errors[] = "Admin with submitted password not found in osCommerce table ".TABLE_ADMINISTRATORS;
			}
			
			// Output errors, if any
			if (count($errors)) {
				echo "There were error(s) with your request: <br>".implode("<br>", $errors)."<br>";
				$next_step = 'Return to Step 3';
				break;
			}
			?><ul><?php
			// Add configuration group
			$existing_query = tep_db_query("SELECT count(*) AS total, configuration_group_id FROM " . TABLE_CONFIGURATION_GROUP . " WHERE configuration_group_title = 'Forums' AND configuration_group_description='phpBB configuration'");
			$existing_value = tep_db_fetch_array($existing_query);
			if ($existing_value['total'] > 0) {
				?><li>'Forums' already exists in configuration group table</li><?php
			} else {
				tep_db_query(
					"INSERT INTO " . TABLE_CONFIGURATION_GROUP . " 
					(configuration_group_id, configuration_group_title, configuration_group_description, sort_order, visible)
					VALUES 
					(NULL, 'Forums', 'phpBB configuration', 99, 1)");
				$existing_query = tep_db_query("SELECT count(*) AS total, configuration_group_id FROM " . TABLE_CONFIGURATION_GROUP . " WHERE configuration_group_title = 'Forums' AND configuration_group_description='phpBB configuration'");
				$existing_value = tep_db_fetch_array($existing_query);
				?><li>'Forums' added to configuration group table</li><?php
			}
			$forum_group_id = $existing_value['configuration_group_id'];
			// Add admin configuration values
            
            // We really don't need the three config values below
            /*
			// FORUM_USERNAME
			$existing_query = tep_db_query("SELECT count(*) AS total FROM " . TABLE_CONFIGURATION . " WHERE configuration_key = 'FORUM_USERNAME'");
			$existing_value = tep_db_fetch_array($existing_query);
			if ($existing_value['total'] > 0) {
				?><li>'FORUM_USERNAME' already exists in configuration table</li><?
			} else {
				tep_db_query(
					"INSERT INTO " . TABLE_CONFIGURATION . " 
					(configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function)
					VALUES 
					(NULL, 'Forum Administrator Display Name', 'FORUM_USERNAME', '" . tep_db_input($admin_username) . "', 'The displayed username of the forum administrator.', ".$forum_group_id.", 1, NULL, NOW(), NULL, NULL)");
				?><li>'FORUM_USERNAME' added to configuration table</li><?
			}
			// FORUM_USER_EMAIL
			$existing_query = tep_db_query("SELECT count(*) AS total FROM " . TABLE_CONFIGURATION . " WHERE configuration_key = 'FORUM_USER_EMAIL'");
			$existing_value = tep_db_fetch_array($existing_query);
			if ($existing_value['total'] > 0) {
				?><li>'FORUM_USER_EMAIL' already exists in configuration table</li><?
			} else {
				tep_db_query(
					"INSERT INTO " . TABLE_CONFIGURATION . " 
					(configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function)
					VALUES 
					(NULL, 'Forum Administrator E-Mail', 'FORUM_USER_EMAIL', '" . tep_db_input($admin_email) . "', 'The forum administrator e-mail address for correspondence.', ".$forum_group_id.", 2, NULL, NOW(), NULL, NULL)");
				?><li>'FORUM_USER_EMAIL' added to configuration table</li><?
			}
			// FORUM_PASSWORD
			$existing_query = tep_db_query("SELECT count(*) AS total FROM " . TABLE_CONFIGURATION . " WHERE configuration_key = 'FORUM_PASSWORD'");
			$existing_value = tep_db_fetch_array($existing_query);
			if ($existing_value['total'] > 0) {
				?><li>'FORUM_PASSWORD' already exists in configuration table</li><?
			} else {
				tep_db_query(
					"INSERT INTO " . TABLE_CONFIGURATION . " 
					(configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function)
					VALUES 
					(NULL, 'Forum Administrator Password', 'FORUM_PASSWORD', MD5('" . tep_db_input($admin_password) . "'), 'The forum administrator password.', ".$forum_group_id.", 3, NULL, NOW(), NULL, NULL)");
				?><li>'FORUM_PASSWORD' added to configuration table</li><?
			}
            */
            
			// FORUM_INITIAL_PASSWORD_REWRITE
			$existing_query = tep_db_query("SELECT count(*) AS total FROM " . TABLE_CONFIGURATION . " WHERE configuration_key = 'FORUM_INITIAL_PASSWORD_REWRITE'");
			$existing_value = tep_db_fetch_array($existing_query);
			if ($existing_value['total'] > 0) {
				?><li>'FORUM_INITIAL_PASSWORD_REWRITE' already exists in configuration table</li><?php
			} else {
				tep_db_query(
					"INSERT INTO " . TABLE_CONFIGURATION . " 
					(configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function)
					VALUES 
					(NULL, 'Forum User Password Rewrite', 'FORUM_INITIAL_PASSWORD_REWRITE', 1, 'Wether passwords should be updated to phpBB3 format.', ".$forum_group_id.", 3, NULL, NOW(), NULL, NULL)");
				?><li>'FORUM_INITIAL_PASSWORD_REWRITE' added to configuration table</li><?php
			}
			// FORUM_FORCE_USERNAME
			$existing_query = tep_db_query("SELECT count(*) AS total FROM " . TABLE_CONFIGURATION . " WHERE configuration_key = 'FORUM_FORCE_USERNAME'");
			$existing_value = tep_db_fetch_array($existing_query);
			if ($existing_value['total'] > 0) {
				?><li>'FORUM_FORCE_USERNAME' already exists in configuration table</li><?php
			} else {
				tep_db_query(
					"INSERT INTO " . TABLE_CONFIGURATION . " 
					(configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function)
					VALUES 
					(NULL, 'Require Forum Username', 'FORUM_FORCE_USERNAME', '0', 'Require forum username after first store login.', ".$forum_group_id.", 3, NULL, NOW(), NULL, NULL)");
				?><li>'FORUM_FORCE_USERNAME' added to configuration table</li><?php
			}
			$next_step = 'Continue to Step 5';
			?></ul>
			<em>Note: The next step will create forum accounts for all your osCommerce  customers. It might take a while.</em>
			<?php
		break;
		
		
		case 'Continue to Step 5': case 'Return to Step 5':
			?>
			<h3>Finding users in osCommerce without an account in phpBB3</h3>
			<ul>
			<?php
			define('IN_PHPBB', true);
			$current_dir = getcwd();
			chdir(DIR_WS_FORUMS);
			$phpbb_root_path = DIR_FS_FORUMS_LOCATION;
			$phpEx = 'php';
			include_once('includes/utf/utf_tools.php');
			chdir($current_dir);
			// First, we need to create a new entry under phpBB user_groups to indicate which customers did not have the change to select a username
			$group_name = 'osCommerce';
			$group_desc = 'osCommerce users who have not selected their usernames.';
			// Check that group does not exist already (In case we're running this script multiple times).
			$new_phpBB_group_query = tep_db_query("SELECT group_id FROM ".TABLE_PHPBB_GROUPS." WHERE group_type = '2' AND group_name= '".tep_db_input($group_name)."' AND group_desc = '".tep_db_input($group_desc)."'");
			$new_phpBB_group = tep_db_fetch_array($new_phpBB_group_query);
			$new_group_id = $new_phpBB_group['group_id'];
			if (empty($new_group_id)) {
				tep_db_query("INSERT INTO `".TABLE_PHPBB_GROUPS."` (`group_type`,`group_name`, `group_desc`, `group_desc_options`, `group_legend`) VALUES (2, '".tep_db_input($group_name)."', '".tep_db_input($group_desc)."', 0, 0);");
				// We need the group_id of the added entry.
				$new_phpBB_group_query = tep_db_query("SELECT group_id FROM ".TABLE_PHPBB_GROUPS." WHERE group_type = '2' AND group_name= '".tep_db_input($group_name)."' AND group_desc = '".tep_db_input($group_desc)."'");
				$new_phpBB_group = tep_db_fetch_array($new_phpBB_group_query);
				$new_group_id = $new_phpBB_group['group_id'];
			}

			// Get the big list of users and store it in an array
			$customers_query = tep_db_query("
			SELECT
				c.customers_id, c.customers_firstname, c.customers_lastname, c.customers_password, c.customers_dob, c.customers_email_address, c.customers_default_address_id,
				a.entry_zone_id, a.entry_country_id,
				u.user_id, u.username, u.username_clean, u.user_password, u.user_email, u.user_email_hash, u.user_new
			FROM " . TABLE_CUSTOMERS . " c 
			LEFT JOIN " . TABLE_ADDRESS_BOOK . " a on c.customers_default_address_id = a.address_book_id AND a.customers_id = c.customers_id
			LEFT JOIN " . TABLE_PHPBB_USERS . " u on c.customers_email_address = u.user_email");
			while ($customer_info = tep_db_fetch_array($customers_query)) {
				// Check if phpBB user_id is NULL
				if (empty($customer_info['user_id'])) {
					// Find if username exists already, append a number if it does.
					// Username = first name + last initial (+ number).
					$new_username = strtolower(trim($customer_info['customers_firstname'])) . strtolower(substr(trim($customer_info['customers_lastname']),0,1));
					$new_username = preg_replace('/\s+/', '', $new_username); // remove whitespace
					$new_username = utf8_clean_string($new_username);
					$existing_query = tep_db_query("SELECT username FROM ".TABLE_PHPBB_USERS." WHERE username LIKE '".$new_username."%'");
					$existing_usernames = array();
					while ($existing_info = tep_db_fetch_array($existing_query)) {
						$existing_usernames[] = $existing_info['username'];
					}
					$count = ''; // allow for no number postfix if first of its kind
					$name_ok = false;
					while (!$name_ok) {
						$current_username = $new_username.$count;
						if (!in_array($current_username, $existing_usernames)) {
							$name_ok = true;
						} else {
							$count = (int) $count + 1;
						}
					}
					// Insert into TABLE_PHPBB_USERS with only basic information
					define('IN_PHPBB', true);
					include_once(DIR_WS_FORUMS.'includes/functions.php');
					// Luckily osCommerce and phpBB3 both used similar salted hash functions for password.
					// Otherwise it would be imposible to retrieve the original password to run it thorugh another hash.
					// I don't think the hash prefix '$P$' does anything different than '$H$', but lets use '$H$' in the TABLE_PHPBB_USERS table for consistency
					$phpBB_pass = $customer_info['customers_password'];
					$prefix = substr($phpBB_pass,0,3);
					if ($prefix == '$P$') {
						$phpBB_pass = '$H$'.substr($phpBB_pass,3);
					}
					// Check date of birth
					$phpBB_group_id = 2; // People over 13
					if (strtotime ($customer_info['customers_dob']) > strtotime('-13 years')) {
						$phpBB_group_id = 3;
					}
					$sql_data_array = array(
						'username' => $current_username,
						'username_clean' => $current_username,
						'user_password' => $phpBB_pass,
						'user_regdate' => time(),
						'user_new' => 1,
						'user_lang' => 'en', // change as needed
						'user_dateformat' => 'D M d, Y g:i a',
						'user_style' => 1, // prosilver, default
						'group_id' => $phpBB_group_id,
						'user_email' => $customer_info['customers_email_address'],
						'user_from' => tep_get_zone_name((int)($customer_info['entry_country_id']),(int)($customer_info['entry_zone_id']),0)
						);                         
					tep_db_perform(TABLE_PHPBB_USERS, $sql_data_array);
					// We need the user_id of the added entry.
					$new_phpBB_user_query = tep_db_query("SELECT user_id, username FROM ".TABLE_PHPBB_USERS." WHERE username = '".$current_username."'");
					$new_phpBB_user = tep_db_fetch_array($new_phpBB_user_query);
					$new_user_id = $new_phpBB_user['user_id'];
					// Insert our new user into the default group.
					$phpBB_group_id = $phpBB_group_id; // If you want your new signups to go to a different group, change $phpBB_group_id = YOUR_GROUP_ID
					tep_db_query("
						INSERT INTO " . TABLE_PHPBB_USER_GROUPS . " 
						(group_id, user_id, user_pending) 
						VALUES 
							('".$phpBB_group_id."', '".$new_user_id."', 0),
							('".$new_group_id."', '".$new_user_id."', 0)
						");
					echo "<li>New phpBB user: ".$customer_info['customers_email_address']."</li>";
				} else {
					echo "<li>User: ".$customer_info['customers_email_address']." is already a phpBB user.</li>";
				}
				 flush();
			} // end while
			?>
			</ul>
			<h3>Select default country for phpBB users (next step)</h3>
			<?php echo tep_draw_pull_down_menu('entry_country_id', tep_get_countries()); ?><br>
			There's no way to know the location of existing phpBB users that do not have an osCommerce account. We have to select a country, or else the database structure for our customers will be broken.<br>
			<?php
			
			$next_step = 'Continue to Step 6';
		break;
		
		
		case 'Continue to Step 6': case 'Return to Step 6':
			?>
			<h3>Finding users in phpBB without an account in osCommerce</h3>
			<ul>
			<?php
			$customers_query = tep_db_query("
			SELECT
				c.customers_id, c.customers_firstname, c.customers_lastname, c.customers_password, c.customers_dob, c.customers_email_address, c.customers_default_address_id,
				a.entry_zone_id, a.entry_country_id,
				u.user_id, u.username, u.username_clean, u.user_password, u.user_email, u.user_email_hash, u.user_new
			FROM " . TABLE_PHPBB_USERS . " u 
			LEFT JOIN " . TABLE_CUSTOMERS . " c on c.customers_email_address = u.user_email
			LEFT JOIN " . TABLE_ADDRESS_BOOK . " a on c.customers_default_address_id = a.address_book_id AND a.customers_id = c.customers_id
			WHERE u.user_type != 2
				AND u.user_id != 2"); // Ignore web crawlers and admin
			while ($customer_info = tep_db_fetch_array($customers_query)) {
				if (empty($customer_info['customers_id'])) {
					// Here we add info to customers, customers_info, and address
					$osC_pass = $customer_info['user_password'];
					$prefix = substr($osC_pass,0,3);
					if ($prefix == '$H$') {
						$osC_pass = '$P$'.substr($osC_pass,3);
					}

					// Assume username is firstname + last initial
					$first_name = substr($customer_info['username'], 0, -1);
					$last_name  = substr($customer_info['username'], -1);
					$sql_data_array = array(
						'customers_firstname' => $first_name,
						'customers_lastname'  => $last_name,
						'customers_email_address'  => $customer_info['user_email'],
						'customers_password'  => $osC_pass,
						'customers_newsletter' => 1); // Could also be '0'
					tep_db_perform(TABLE_CUSTOMERS, $sql_data_array);
					$customer_id = tep_db_insert_id();
					// Now add to TABLE_CUSTOMERS_INFO
					$sql_data_array = array(
						'customers_info_id' => $customer_id,
						'customers_info_number_of_logons'  => 0,
						'customers_info_date_account_created'  => date('Y-m-d H:i:s'));
					tep_db_perform(TABLE_CUSTOMERS_INFO, $sql_data_array);
					// Now add empty entry to TABLE_ADDRESS_BOOK
					$sql_data_array = array(
						'customers_id' => $customer_id,
						'entry_firstname'  => $first_name,
						'entry_lastname'  => $last_name,
						'entry_country_id' =>tep_db_input($_REQUEST['entry_country_id']));
					tep_db_perform(TABLE_ADDRESS_BOOK, $sql_data_array);
					$address_id = tep_db_insert_id();
					tep_db_query("UPDATE " . TABLE_CUSTOMERS . " SET customers_default_address_id = '".$address_id."' WHERE customers_id ='" . $customer_id . "'");
					echo "<li>New osCommerce user: ".$customer_info['user_email']."</li>";
					
				} else {
					echo "<li>User: ".$customer_info['user_email']." is already a osCommerce user.</li>";
				}
				 flush();
			}

			?>
			</ul>
			<?php
			$next_step = 'Done!';
		break;
		
		
		case 'Done!':
			?>
			<h3>Your phpBB3 <-> osCommerce bridge is ready</h3>
			Features:
			<ul>
				<li>Signing in to the store (now through email) signs you in to the forums</li>
			</ul>
			If you have a busy forum/store, I recommend you runthis script one more time, in case someone created an accout while the script was executing.<br>
			If no one did, no changes will occur.<br>
			<?php
			$next_step = 'Return to Step 1';
		break;

	}
} else {
	// Welcome screen
	?>
	<h3>The following script will perform these actions</h3>
	<ol>
		<li>Create database configuration entries</li>
		<li>Create automatic phpBB3 usernames for all existing osCommerce accounts with no name colllision (first name + last name initial + number)</li>
		<li>Create osCommerce accounts for all phpBB3 accounts</li>
	</ol>
	<?php
	$next_step = 'Continue to Step 1';
}

echo '<input type="submit" name="action" value="'.$next_step.'">';


?>

</form>
<?php
require(DIR_WS_INCLUDES . 'template_bottom.php');
require(DIR_WS_INCLUDES . 'application_bottom.php');
?>
