<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2007 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// HTML Cache for osC Admin
// Inserts scripts to be cached or invalidated
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// http://www.oscommerce.com
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
  require('includes/application_top.php');

  $action = (isset($_GET['action']) ? $_GET['action'] : '');


  $dir = dir(DIR_FS_CATALOG);
  $scripts_array = array();
  while ($script = $dir->read()) {
    if( strlen($script) < 5 || substr($script, -4, 4) != '.php')
      continue;

    $scripts_array[strtolower($script)] = array(
                             'id' => $script, 
                             'text' => $script
                            );
  }
  $dir->close();

  ksort($scripts_array, SORT_STRING);
  $scripts_array = array_values($scripts_array);

  $modes_array = array(
                        array('id' => '1', 'text' => 'Cache'),
                        array('id' => '2', 'text' => 'Flush'),
                        array('id' => '3', 'text' => 'Parametric')
                      );


  switch($action) {
    case 'insert':
      $script_key = md5($_POST['scripts_list']);
      $check_query = tep_db_query("select count(*) as total from " . TABLE_CACHE_HTML . " where cache_html_key = '" . tep_db_input(tep_db_prepare_input($script_key)) . "'");
      $check_array = tep_db_fetch_array($check_query);
      if( !$check_array['total'] ) {
        $sql_data_array = array(
                                'cache_html_key' => tep_db_prepare_input(md5($_POST['scripts_list'])),
                                'cache_html_script' => tep_db_prepare_input($_POST['scripts_list']),
                                'cache_html_duration' => DEFAULT_HTML_CACHE_TIMEOUT,
                                'cache_html_type' => '1'
                               );
        tep_db_perform(TABLE_CACHE_HTML, $sql_data_array);
      }
      tep_redirect(tep_href_link(FILENAME_CACHE_HTML, tep_get_all_get_params(array('action')) ));
      break;

    case 'update_multi':
      if( !isset($_POST['tag_id']) || !is_array($_POST['tag_id']) ) {
        tep_redirect(tep_href_link(FILENAME_CACHE_HTML, tep_get_all_get_params(array('action')) ));
      }
      foreach ($_POST['tag_id'] as $key=>$value) {
        $sql_data_array = array(
                                'cache_html_params' => tep_db_prepare_input($_POST['params'][$key]),
                                'cache_html_duration' => tep_db_prepare_input($_POST['duration'][$key]),
                                'cache_html_type' => tep_db_prepare_input($_POST['mode'][$key])
                               );

        tep_db_perform(TABLE_CACHE_HTML, $sql_data_array, 'update', "cache_html_key = '" . tep_db_input(tep_db_prepare_input($key)) . "'");
      }
      tep_redirect(tep_href_link(FILENAME_CACHE_HTML, tep_get_all_get_params(array('action')) ));
      break;

    case 'delete_confirm_multi':
      if( !isset($_POST['tag_id']) || !is_array($_POST['tag_id']) ) {
        tep_redirect(tep_href_link(FILENAME_CACHE_HTML, tep_get_all_get_params(array('action')) ));
      }

      foreach ($_POST['tag_id'] as $key=>$value) {
        tep_db_query("delete from " . TABLE_CACHE_HTML . " where cache_html_key = '" . tep_db_input(tep_db_prepare_input($value)) . "'");
        tep_db_query("delete from " . TABLE_CACHE_HTML_REPORTS . " where cache_html_key = '" . tep_db_input(tep_db_prepare_input($value)) . "'");
      }
      tep_redirect(tep_href_link(FILENAME_CACHE_HTML, tep_get_all_get_params(array('action')) ));
      break;
    default:
      break;
  }

?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<script language="javascript" src="includes/general.js"></script>
<script language="javascript">
  var g_checkbox2 = 0;
  function copy_checkboxes(form, array_name) {
    for (var i = 0; i < form.elements.length; i++) {
      if( form.elements[i].type == "checkbox" ) {
        check_name = form.elements[i].name;
        if( array_name == check_name.substring(0, array_name.length) ) {
          form.elements[i].checked = g_checkbox2?"":"on";
        }
      }
    }
    g_checkbox2 ^= 1;
  }
</script>
</head>
<body>
<!-- header //-->
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<!-- header_eof //-->

<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
    <td width="<?php echo BOX_WIDTH; ?>" valign="top"><table border="0" width="<?php echo BOX_WIDTH; ?>" cellspacing="1" cellpadding="1" class="columnLeft">
<!-- left_navigation //-->
<?php require(DIR_WS_INCLUDES . 'column_left.php'); ?>
<!-- left_navigation_eof //-->
    </table></td>
<!-- body_text //-->
    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td class="pageHeading"><?php echo HEADING_TITLE; ?></td>
          </tr>
        </table></td>
      </tr>
<?php
  if( $action == 'delete_multi' ) {
?>
      <tr>
        <td class="smallText"><?php echo TEXT_INFO_DELETE; ?></td>
      </tr>

      <tr>
        <td valign="top"><?php echo tep_draw_form('rl_confirm', FILENAME_CACHE_HTML, tep_get_all_get_params(array('action')) . 'action=delete_confirm_multi', 'post'); ?><table border="0" width="100%" cellspacing="1" cellpadding="3">
          <tr class="dataTableHeadingRow">
            <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_FILENAME; ?></td>
          </tr>
<?php
    $rows = 0;
    foreach ($_POST['tag_id'] as $key=>$value) {
      $delete_query = tep_db_query("select cache_html_script from " . TABLE_CACHE_HTML . " where cache_html_key = '" . tep_db_input(tep_db_prepare_input($key)) . "'");
      if( $delete_array = tep_db_fetch_array($delete_query) ) {
        $rows++;
        $row_class = ($rows%2)?'dataTableRow':'dataTableRowSelected';
        echo '                  <tr class="' . $row_class . '">';    
?>
            <td class="dataTableContent"><?php echo tep_draw_hidden_field('tag_id[]', $key) . $delete_array['cache_html_script']; ?></td>
          </tr>

<?php
      }
    }
?>

          <tr>
            <td>
<?php 
    if( count($_POST['tag_id']) ) {
      echo '<a href="' . tep_href_link(FILENAME_CACHE_HTML, tep_get_all_get_params(array('action')) ) . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a> ' . tep_image_submit('button_confirm.gif', IMAGE_CONFIRM);
    }
?>
            </td>
          </tr>
        </table></form></td>
      </tr>
<?php
  } else {
?>
      <tr>
        <td class="smallText"><?php echo TEXT_INFO_MAIN; ?></td>
      </tr>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
<?php
// Catalog File List Stored in the database
    $rows = 0;
    $cache_html_query_raw = "select * from " . TABLE_CACHE_HTML . " order by cache_html_script";
    $cache_html_split = new splitPageResults($_GET['page'], MAX_DISPLAY_HTML_CACHE_SCRIPTS, $cache_html_query_raw, $cache_html_query_numrows, 'cache_html_key');
    if( $cache_html_query_numrows > 0 ) {
?>
              <tr>
                <td valign="top"><?php echo tep_draw_form('rl', FILENAME_CACHE_HTML, tep_get_all_get_params(array('action')) . 'action=delete_multi', 'post'); ?><table border="0" width="100%" cellspacing="1" cellpadding="3">
                  <tr class="dataTableHeadingRow">
                    <td class="dataTableHeadingContent"><?php echo '<a href="javascript:void(0)" onClick="copy_checkboxes(document.rl,\'tag_id\')" title="Page Select On/Off" class="menuBoxHeadingLink"><span class="dataTableHeadingContent">' . TABLE_HEADING_SELECT . '</span></a>'; ?></td>
                    <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_FILENAME; ?></td>
                    <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_TYPE; ?></td>
                    <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_DURATION; ?></td>
                    <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_PARAMETERS; ?></td>
                  </tr>
<?php
      $cache_html_query = tep_db_query($cache_html_query_raw);
      $bCheck = false;
      while ($cache_html = tep_db_fetch_array($cache_html_query)) {
        $rows++;
        if( $cache_html['cache_html_type'] == 3 ) {
          $row_class = 'dataTableRowHigh';
        } elseif($cache_html['cache_html_type'] == 2) {
          $row_class = 'dataTableRowImpact';
        } else {
          $row_class = ($rows%2)?'dataTableRow':'dataTableRowSelected';
        }
        echo '                      <tr class="' . $row_class . '">';
?>
                    <td class="dataTableContent"><?php echo tep_draw_checkbox_field('tag_id[' . $cache_html['cache_html_key'] . ']', ($bCheck?'on':''), $bCheck ); ?></td>
                    <td class="dataTableContent"><?php echo $cache_html['cache_html_script']; ?></td>
                    <td class="dataTableContent" align="center"><?php echo tep_draw_pull_down_menu('mode[' . $cache_html['cache_html_key'] . ']', $modes_array, $cache_html['cache_html_type']); ?></td>
                    <td class="dataTableContent" align="center"><?php echo tep_draw_input_field('duration[' . $cache_html['cache_html_key'] . ']', $cache_html['cache_html_duration'], 'style="width: 100px"') . '&nbsp;(Secs)'; ?></td>
                    <td class="dataTableContent" align="center"><?php echo tep_draw_input_field('params[' . $cache_html['cache_html_key'] . ']', $cache_html['cache_html_params'], 'style="width: 140px"'); ?></td>
                  </tr>
<?php
      }
?>
                  <tr>
                    <td colspan="3"><table border="0" width="100%" cellspacing="0" cellpadding="2">
                      <tr>
                        <td>
<?php 
        echo tep_image_submit('button_update.gif', TEXT_UPDATE, 'onclick="this.form.action=' . '\'' . tep_href_link(FILENAME_CACHE_HTML, tep_get_all_get_params(array('action')) . 'action=update_multi') . '\'' . '"') . ' ' . tep_image_submit('button_delete.gif', TEXT_DELETE, 'onclick="this.form.action=' . '\'' . tep_href_link(FILENAME_CACHE_HTML, tep_get_all_get_params(array('action')) . 'action=delete_multi') . '\'' . '"');
?>
                        </td>
                      </tr>
                    </table></td>
                  </tr>
                </table></form></td>
              </tr>

              <tr>
                <td><table border="0" width="100%" cellspacing="0" cellpadding="2">
                  <tr>
                    <td class="smallText" valign="top"><?php echo $cache_html_split->display_count($cache_html_query_numrows, MAX_DISPLAY_HTML_CACHE_SCRIPTS, $_GET['page'], TEXT_DISPLAY_NUMBER_OF_CACHE_SCRIPTS); ?></td>
                    <td class="smallText" align="right"><?php echo $cache_html_split->display_links($cache_html_query_numrows, MAX_DISPLAY_HTML_CACHE_SCRIPTS, MAX_DISPLAY_PAGE_LINKS, $_GET['page'], tep_get_all_get_params(array('action', 'page')), 'page'); ?></td>
                  </tr>
                </table></td>
              </tr>
<?php 
    }
?>
              <tr>
                <td><?php echo tep_draw_separator('pixel_trans.gif', '100%', '10'); ?></td>
              </tr>
              <tr>
                <td><hr /></td>
              </tr>
              <tr>
                <td class="pageHeading"><?php echo HEADING_TITLE2; ?></td>
              </tr>
              <tr>
                <td class="smallText"><?php echo TEXT_INFO_MAIN2; ?></td>
              </tr>
              <tr>
                <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
                  <tr>
                    <td class="formArea"><?php echo tep_draw_form('mz', FILENAME_CACHE_HTML, tep_get_all_get_params(array('action')) . 'action=insert', 'post'); ?><table border="0" width="100%" cellspacing="0" cellpadding="4">
<?php
      echo '<td nowrap class="smallText"><b>Select Script:</b></td>' . "\n";
      echo '<td nowrap>' . tep_draw_pull_down_menu('scripts_list', $scripts_array) . '</td>' . "\n";
      echo '<td nowrap>' . tep_image_submit('button_insert.gif', TEXT_INSERT) . '</td>' . "\n";
      echo '<td nowrap width="90%">&nbsp;</td>' . "\n";
?>
                    </table></form></td>
                  </tr>
                </table></td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
<?php
  }
?>
    </table></td>
<!-- body_text_eof //-->
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
<br>
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
