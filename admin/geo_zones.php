<?php
/*
  $Id: geo_zones.php,v 1.29 2003/06/29 22:50:52 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

-------------------------------------------
// Modifications by Mark Samios.
// Last Update : 2006-06-05
// Copyright (c) 2006 Asymmetric Software. Innovation & Excellence.
// http://www.asymmetrics.com
// Multi-Zones/Multi-Select Support for active countries
// - Added Form to Delete multiple zones
// - Added Form to Expand Zones from countries
// - Added Form to Insert multiple zones into a tax zone
// - Removed Single Zone Delete functionality.
// - Zone selection is now based on active countries
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
define('LOCAL_SPLIT_RESULTS', 100);
  require('includes/application_top.php');

  $saction = (isset($_GET['saction']) ? $_GET['saction'] : '');

  if (tep_not_null($saction)) {
    switch ($saction) {
      case 'insert_sub':
        $zID = tep_db_prepare_input($_GET['zID']);
        $zone_country_id = tep_db_prepare_input($_POST['zone_country_id']);
        $zone_id = tep_db_prepare_input($_POST['zone_id']);

        tep_db_query("insert into " . TABLE_ZONES_TO_GEO_ZONES . " (zone_country_id, zone_id, geo_zone_id, date_added) values ('" . (int)$zone_country_id . "', '" . (int)$zone_id . "', '" . (int)$zID . "', now())");
        $new_subzone_id = tep_db_insert_id();

        tep_redirect(tep_href_link(FILENAME_GEO_ZONES, 'zpage=' . $_GET['zpage'] . '&zID=' . $_GET['zID'] . '&action=list&spage=' . $_GET['spage'] . '&sID=' . $new_subzone_id));
        break;
      case 'save_sub':
        $sID = tep_db_prepare_input($_GET['sID']);
        $zID = tep_db_prepare_input($_GET['zID']);
        $zone_country_id = tep_db_prepare_input($_POST['zone_country_id']);
        $zone_id = tep_db_prepare_input($_POST['zone_id']);

        tep_db_query("update " . TABLE_ZONES_TO_GEO_ZONES . " set geo_zone_id = '" . (int)$zID . "', zone_country_id = '" . (int)$zone_country_id . "', zone_id = " . (tep_not_null($zone_id) ? "'" . (int)$zone_id . "'" : 'null') . ", last_modified = now() where association_id = '" . (int)$sID . "'");

        tep_redirect(tep_href_link(FILENAME_GEO_ZONES, 'zpage=' . $_GET['zpage'] . '&zID=' . $_GET['zID'] . '&action=list&spage=' . $_GET['spage'] . '&sID=' . $_GET['sID']));
        break;
/*
      case 'deleteconfirm_sub':
        $sID = tep_db_prepare_input($_GET['sID']);

        tep_db_query("delete from " . TABLE_ZONES_TO_GEO_ZONES . " where association_id = '" . (int)$sID . "'");

        tep_redirect(tep_href_link(FILENAME_GEO_ZONES, 'zpage=' . $_GET['zpage'] . '&zID=' . $_GET['zID'] . '&action=list&spage=' . $_GET['spage']));
        break;
*/
    }
  }

  $action = (isset($_GET['action']) ? $_GET['action'] : '');
  if (tep_not_null($action)) {
    switch ($action) {
      case 'delete_multizone':
        if( !is_array($_POST['tax_zone_id']) )
          tep_redirect(tep_href_link(FILENAME_GEO_ZONES, tep_get_all_get_params(array('action')) . '&action=list'));
        break;
      case 'insert_zone':
        $geo_zone_name = tep_db_prepare_input($_POST['geo_zone_name']);
        $geo_zone_description = tep_db_prepare_input($_POST['geo_zone_description']);

        tep_db_query("insert into " . TABLE_GEO_ZONES . " (geo_zone_name, geo_zone_description, date_added) values ('" . tep_db_input($geo_zone_name) . "', '" . tep_db_input($geo_zone_description) . "', now())");
        $new_zone_id = tep_db_insert_id();

        tep_redirect(tep_href_link(FILENAME_GEO_ZONES, 'zpage=' . $_GET['zpage'] . '&zID=' . $new_zone_id));
        break;
      case 'save_zone':
        $zID = tep_db_prepare_input($_GET['zID']);
        $geo_zone_name = tep_db_prepare_input($_POST['geo_zone_name']);
        $geo_zone_description = tep_db_prepare_input($_POST['geo_zone_description']);

        tep_db_query("update " . TABLE_GEO_ZONES . " set geo_zone_name = '" . tep_db_input($geo_zone_name) . "', geo_zone_description = '" . tep_db_input($geo_zone_description) . "', last_modified = now() where geo_zone_id = '" . (int)$zID . "'");

        tep_redirect(tep_href_link(FILENAME_GEO_ZONES, 'zpage=' . $_GET['zpage'] . '&zID=' . $_GET['zID']));
        break;
      case 'deleteconfirm_zone':
        $zID = tep_db_prepare_input($_GET['zID']);
        tep_db_query("delete from " . TABLE_GEO_ZONES . " where geo_zone_id = '" . (int)$zID . "'");
        tep_db_query("delete from " . TABLE_ZONES_TO_GEO_ZONES . " where geo_zone_id = '" . (int)$zID . "'");

        tep_redirect(tep_href_link(FILENAME_GEO_ZONES, 'zpage=' . $_GET['zpage']));
        break;

      case 'multi_countries':
        $multi_form = (isset($_POST['multi_form']) ? $_POST['multi_form'] : '');
        switch( $multi_form ) {
          case 'multi_countries':
            $zID = tep_db_prepare_input($_GET['zID']);
            foreach ($_POST['countries_id'] as $country_id=>$val) {
              if( $_POST['mode'][$country_id] == 'all' ) {
                $check_query = tep_db_query("select zone_country_id from " . TABLE_ZONES_TO_GEO_ZONES . " where zone_country_id = '" . (int)$country_id . "' and geo_zone_id = '" . (int)tep_db_input($zID) . "'");
                if( tep_db_num_rows($check_query) )
                  continue;

                tep_db_query("insert into " . TABLE_ZONES_TO_GEO_ZONES . " (zone_country_id, zone_id, geo_zone_id, date_added) values ('" . (int)$country_id . "', null, '" . (int)tep_db_input($zID) . "', now())");
              } elseif($_POST['mode'][$country_id] == 'expand') {
                $multi_query = tep_db_query("select zone_id from " . TABLE_ZONES . " where zone_status='1' and zone_country_id = '" . (int)$country_id . "'");
                while( $multi = tep_db_fetch_array($multi_query) ) {
                  $check_query = tep_db_query("select zone_id from " . TABLE_ZONES_TO_GEO_ZONES . " where zone_country_id = '" . (int)$country_id . "' and zone_id = '" . (int)$multi['zone_id'] . "' and geo_zone_id = '" . (int)$zID . "'");
                  if( tep_db_num_rows($check_query) )
                    continue;

                  tep_db_query("insert into " . TABLE_ZONES_TO_GEO_ZONES . " (zone_country_id, zone_id, geo_zone_id, date_added) values ('" . (int)$country_id . "', '" . (int)$multi['zone_id'] . "', '" . (int)$zID . "', now())");
                }
              }
            }
            tep_redirect(tep_href_link(FILENAME_GEO_ZONES, 'zpage=' . $_GET['zpage'] . '&zID=' . $_GET['zID'] . '&action=list&spage=' . $_GET['spage']));
            break;
          case 'multi_zones':
            $zID = tep_db_prepare_input($_GET['zID']);
            $country_id = tep_db_prepare_input($_POST['countries_list']);

            foreach ($_POST['zones_id'] as $zone_id=>$val) {
              $check_query = tep_db_query("select zone_id from " . TABLE_ZONES_TO_GEO_ZONES . " where zone_id = '" . (int)$zone_id . "' and zone_country_id = '" . (int)$country_id . "' and geo_zone_id = '" . (int)tep_db_input($zID) . "'");
              if( tep_db_num_rows($check_query) )
                  continue;

              tep_db_query("insert into " . TABLE_ZONES_TO_GEO_ZONES . " (zone_country_id, zone_id, geo_zone_id, date_added) values ('" . (int)$country_id . "', '" . (int)$zone_id . "', '" . (int)$zID . "', now())");

            }
            tep_redirect(tep_href_link(FILENAME_GEO_ZONES, 'zpage=' . $_GET['zpage'] . '&zID=' . $_GET['zID'] . '&action=list&spage=' . $_GET['spage']));
            break;
          default:
            break;
        }
        break;
      case 'deleteconfirm_multizone':
        $zID = tep_db_prepare_input($_GET['zID']);
        foreach ($_POST['tax_zone_id'] as $tax_zone_id=>$val) {
          tep_db_query("delete from " . TABLE_ZONES_TO_GEO_ZONES . " where geo_zone_id = '" . (int)$zID . "' and association_id = '" . (int)$tax_zone_id . "'");
        }
        tep_redirect(tep_href_link(FILENAME_GEO_ZONES, 'zpage=' . $_GET['zpage'] . '&zID=' . $_GET['zID'] . '&action=list&spage=' . $_GET['spage']));
        break;

    }
  }

  $countries_array = tep_get_active_countries();
  $modes_array = array(
                        array('id' => 'expand', 'text' => 'Expand To Zones'),
                        array('id' => 'all', 'text' => 'All Zones')
                      );

?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<script language="javascript" src="includes/general.js"></script>
<script language="javascript"><!--
  var g_checkbox = 0;
  function check_boxes(form) {
    for (var i = 0; i < form.elements.length; i++) {
      if( form.elements[i].type == "checkbox" ) {
        form.elements[i].checked = g_checkbox?"":"on";
      }
    }
    g_checkbox ^= 1;
  }
//--></script>

<?php
  if (isset($_GET['zID']) && (($saction == 'edit') || ($saction == 'new'))) {
?>
<script language="javascript"><!--
function resetZoneSelected(theForm) {
  if (theForm.state.value != '') {
    theForm.zone_id.selectedIndex = '0';
    if (theForm.zone_id.options.length > 0) {
      theForm.state.value = '<?php echo JS_STATE_SELECT; ?>';
    }
  }
}

function update_zone(theForm) {
  var NumState = theForm.zone_id.options.length;
  var SelectedCountry = "";

  while(NumState > 0) {
    NumState--;
    theForm.zone_id.options[NumState] = null;
  }         

  SelectedCountry = theForm.zone_country_id.options[theForm.zone_country_id.selectedIndex].value;

<?php echo tep_js_zone_list('SelectedCountry', 'theForm', 'zone_id'); ?>

}
//--></script>
<?php
  }
?>
</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF" onload="SetFocus();">
<!-- header //-->
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<!-- header_eof //-->

<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
    <td width="<?php echo BOX_WIDTH; ?>" valign="top"><table border="0" width="<?php echo BOX_WIDTH; ?>" cellspacing="1" cellpadding="1" class="columnLeft">
<!-- left_navigation //-->
<?php require(DIR_WS_INCLUDES . 'column_left.php'); ?>
<!-- left_navigation_eof //-->
    </table></td>
<!-- body_text //-->
    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
        <td colspan="2" width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td><?php echo tep_draw_separator('pixel_trans.gif', '100%', '10'); ?></td>
          </tr>
          <tr>
            <td class="pageHeading">
<?php 
  echo HEADING_TITLE; 
  if (isset($_GET['zID'])) 
    echo '&nbsp;&raquo;&nbsp;' . tep_get_geo_zone_name($_GET['zID']);
?>
            </td>
          </tr>
          <tr>
            <td><?php echo tep_draw_separator('pixel_trans.gif', '100%', '10'); ?></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td valign="top" width="75%"><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="0">
<?php
  if ($action == 'list') {
?>
              <tr>
                <td valign="top"><?php echo tep_draw_form('rl', FILENAME_GEO_ZONES, 'action=delete_multizone&zID=' . $_GET['zID'] . '&zpage=' . $_GET['zpage'] . '&spage=' . $_GET['spage'], 'post'); ?><table border="0" width="100%" cellspacing="1" cellpadding="3">
                  <tr class="dataTableHeadingRow">
                    <td class="dataTableHeadingContent"><?php echo '<a href="javascript:void(0)" onClick="check_boxes(document.rl)" title="Page Select On/Off" class="menuBoxHeadingLink"><span class="dataTableHeadingContent">' . TABLE_HEADING_SELECT . '</span></a>'; ?></td>
                    <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_COUNTRY; ?></td>
                    <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_COUNTRY_ZONE; ?></td>
                    <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_ACTION; ?>&nbsp;</td>
                  </tr>
<?php
    $rows = 0;
    $zones_query_raw = "select a.association_id, a.zone_country_id, c.countries_name, a.zone_id, a.geo_zone_id, a.last_modified, a.date_added, z.zone_name from " . TABLE_ZONES_TO_GEO_ZONES . " a left join " . TABLE_COUNTRIES . " c on a.zone_country_id = c.countries_id left join " . TABLE_ZONES . " z on a.zone_id = z.zone_id where a.geo_zone_id = " . (int)$_GET['zID'] . " order by association_id";
    $zones_split = new splitPageResults($_GET['spage'], LOCAL_SPLIT_RESULTS, $zones_query_raw, $zones_query_numrows);
    $zones_query = tep_db_query($zones_query_raw);
    $bCheck = false;
    while ($zones = tep_db_fetch_array($zones_query)) {
      $rows++;
      if ((!isset($_GET['sID']) || (isset($_GET['sID']) && ($_GET['sID'] == $zones['association_id']))) && !isset($sInfo) && (substr($action, 0, 3) != 'new')) {
        $sInfo = new objectInfo($zones);
      }
      $row_class = ($rows%2)?'dataTableRow':'dataTableRowSelected';
      echo '                      <tr class="' . $row_class . '">';
?>
                    <td class="dataTableContent"><?php echo tep_draw_checkbox_field('tax_zone_id[' . $zones['association_id'] . ']', ($bCheck?'on':''), $bCheck ); ?></td>
                    <td class="dataTableContent"><?php echo (($zones['countries_name']) ? $zones['countries_name'] : TEXT_ALL_COUNTRIES); ?></td>
                    <td class="dataTableContent"><?php echo (($zones['zone_id']) ? $zones['zone_name'] : PLEASE_SELECT); ?></td>
                    <td class="dataTableContent" align="right"><?php if (isset($sInfo) && is_object($sInfo) && ($zones['association_id'] == $sInfo->association_id)) { echo tep_image(DIR_WS_IMAGES . 'icon_arrow_right.gif', ''); } else { echo '<a href="' . tep_href_link(FILENAME_GEO_ZONES, 'zpage=' . $_GET['zpage'] . '&zID=' . $_GET['zID'] . '&action=list&spage=' . $_GET['spage'] . '&sID=' . $zones['association_id']) . '">' . tep_image(DIR_WS_IMAGES . 'icon_info.gif', IMAGE_ICON_INFO) . '</a>'; } ?>&nbsp;</td>
                  </tr>
<?php
    }
?>
                  <tr>
                    <td colspan="4">
<?php 
    if(empty($saction)) {
      //echo '<a href="' . tep_href_link(FILENAME_GEO_ZONES, 'zpage=' . $_GET['zpage'] . '&zID=' . $_GET['zID']) . '">' . tep_image_button('button_back.gif', IMAGE_BACK) . '</a> <a href="' . tep_href_link(FILENAME_GEO_ZONES, 'zpage=' . $_GET['zpage'] . '&zID=' . $_GET['zID'] . '&action=list&spage=' . $_GET['spage'] . '&' . (isset($sInfo) ? 'sID=' . $sInfo->association_id . '&' : '') . 'saction=new') . '">' . tep_image_button('button_insert.gif', IMAGE_INSERT) . '</a>';
      echo '<a href="' . tep_href_link(FILENAME_GEO_ZONES, 'zpage=' . $_GET['zpage'] . '&zID=' . $_GET['zID']) . '">' . tep_image_button('button_back.gif', IMAGE_BACK) . '</a> ' . tep_image_submit('button_delete.gif', TEXT_DELETE_MULTIZONE);
    }
?>
                    </td>
                  </tr>
                </table></form></td>
              </tr>

              <tr>
                <td><table border="0" width="100%" cellspacing="0" cellpadding="2">
                  <tr>
                    <td class="smallText" valign="top"><?php echo $zones_split->display_count($zones_query_numrows, LOCAL_SPLIT_RESULTS, $_GET['spage'], TEXT_DISPLAY_NUMBER_OF_COUNTRIES); ?></td>
                    <td class="smallText" align="right"><?php echo $zones_split->display_links($zones_query_numrows, LOCAL_SPLIT_RESULTS, MAX_DISPLAY_PAGE_LINKS, $_GET['spage'], 'zpage=' . $_GET['zpage'] . '&zID=' . $_GET['zID'] . '&action=list', 'spage'); ?></td>
                  </tr>
                </table></td>
              </tr>
<?php 
    if (empty($saction)) {
?>
              <tr>
                <td colspan="3"><?php echo tep_draw_separator('pixel_trans.gif', '100%', '10'); ?></td>
              </tr>

              <tr>
                <td colspan="3" class="pageHeading"><hr /></td>
              </tr>

              <tr>
                <td colspan="3" class="pageHeading">Multi Countries/Zones Options</td>

              </tr>
              <tr>
                <td colspan="3"><?php echo tep_draw_separator('pixel_trans.gif', '100%', '10'); ?></td>
              </tr>
              <tr>
                <td colspan="3"><?php echo tep_draw_form('mz', FILENAME_GEO_ZONES, '', 'get'); ?><table border="0" width="100%" cellspacing="0" cellpadding="2">
                  <tr>
                    <td>
<?php
      echo '<td nowrap><a href="' . tep_href_link(FILENAME_GEO_ZONES, tep_get_all_get_params(array('action')) . 'action=multi_countries') . '">' . tep_image_button('button_countries.gif', TEXT_SWITCH_ACTIVE_COUNTRIES) . '</a></td>' . "\n";
      echo '<td nowrap width="90%">&nbsp</td>' . "\n";
      echo '<td nowrap class="smallText"><b>Zones from:</b></td>' . "\n";
      echo '<td nowrap>' . tep_draw_pull_down_menu('countries_list', $countries_array) . '</td>' . "\n";
      echo '<td nowrap>' . tep_draw_hidden_field('action', 'multi_zones') . tep_draw_hidden_field('zID', $_GET['zID']) . tep_draw_hidden_field('zpage', $_GET['zpage']) . tep_draw_hidden_field('spage', $_GET['spage']) . tep_image_submit('button_submit.gif', TEXT_SWITCH_ACTIVE_ZONES) . '</td>' . "\n";
?>
                    </td>
                  <tr>
                </table></form></td>
              </tr>
<?php
    }
?>

<?php
  } elseif($action == 'multi_countries') {
?>
              <tr>
                <td class="smallText"><?php echo TEXT_SELECT_MULTICOUNTRIES; ?></td>
              </tr>
              <tr>
                <td><?php echo tep_draw_separator('pixel_trans.gif', '100%', '10'); ?></td>
              </tr>

              <tr>
                <td valign="top"><?php echo tep_draw_form('mc', FILENAME_GEO_ZONES, 'action=multi_countries&zID=' . $_GET['zID'] . '&zpage=' . $_GET['zpage'] . '&spage=' . $_GET['spage'], 'post'); ?><table border="0" width="100%" cellspacing="1" cellpadding="3">
                  <tr class="dataTableHeadingRow">
                    <td class="dataTableHeadingContent"><?php echo '<a href="javascript:void(0)" onClick="check_boxes(document.mc)" title="Page Select On/Off" class="menuBoxHeadingLink"><span class="dataTableHeadingContent">' . TABLE_HEADING_SELECT . '</span></a>'; ?></td>
                    <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_COUNTRY; ?></td>
                    <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_MODE; ?></td>
                  </tr>
<?php
    $rows = 0;
    $countries_query_raw = "select c.countries_id, c.countries_name from " . TABLE_COUNTRIES . " c where pay_status_id='1'";
    $countries_split = new splitPageResults($_GET['mcpage'], LOCAL_SPLIT_RESULTS, $countries_query_raw, $countries_query_numrows);
    $countries_query = tep_db_query($countries_query_raw);
    $bCheck = false;
    while ($countries = tep_db_fetch_array($countries_query)) {
      $rows++;
      $row_class = ($rows%2)?'dataTableRow':'dataTableRowSelected';
      echo '                  <tr class="' . $row_class . '">';
?>
                    <td class="dataTableContent"><?php echo tep_draw_checkbox_field('countries_id[' . $countries['countries_id'] . ']', ($bCheck?'on':''), $bCheck ); ?></td>
                    <td class="dataTableContent"><?php echo $countries['countries_name']; ?></td>
                    <td class="dataTableContent" align="center"><?php echo tep_draw_pull_down_menu('mode[' . $countries['countries_id'] . ']', $modes_array); ?></td>
                  </tr>
<?php
    }
?>
                  <tr>
                    <td colspan="3"><table border="0" width="100%" cellspacing="0" cellpadding="2">
                      <tr>
                        <td><?php echo '<a href="' . tep_href_link(FILENAME_GEO_ZONES, 'zpage=' . $_GET['zpage'] . '&zID=' . $_GET['zID'] . '&action=list&spage=' . $_GET['spage'] . '&sID=' . $_GET['sID']) . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>' . tep_draw_hidden_field('multi_form', 'multi_countries') . '&nbsp;' . tep_image_submit('button_insert.gif', IMAGE_INSERT); ?></td>
                      </tr>
                    </table></td>
                  </tr>
                </table></form></td>
              </tr>
              <tr>
                <td><table border="0" width="100%" cellspacing="0" cellpadding="2">
                  <tr>
                    <td class="smallText" valign="top"><?php echo $countries_split->display_count($countries_query_numrows, LOCAL_SPLIT_RESULTS, $_GET['mcpage'], TEXT_DISPLAY_NUMBER_OF_COUNTRIES); ?></td>
<?php
/*
                    <td class="smallText" align="right"><?php echo $countries_split->display_links($countries_query_numrows, LOCAL_SPLIT_RESULTS, MAX_DISPLAY_PAGE_LINKS, $_GET['mcpage'], 'mcpage=' . $_GET['mcpage'] . '&mcID=' . $_GET['mcID'] . '&action=multi_countries&zpage=' . $_GET['zpage'] . '&spage=' . $_GET['spage'], 'mcpage'); ?></td>
*/
?>
                    <td class="smallText" align="right"><?php echo $countries_split->display_links($countries_query_numrows, LOCAL_SPLIT_RESULTS, MAX_DISPLAY_PAGE_LINKS, $_GET['mcpage'], 'zID=' . $_GET['zID'] . '&action=multi_countries&zpage=' . $_GET['zpage'] . '&spage=' . $_GET['spage'], 'mcpage'); ?></td>
                  </tr>
                </table></td>
              </tr>

<?php
  } elseif($action == 'multi_zones') {
?>
              <tr>
                <td class="smallText"><?php echo TEXT_SELECT_MULTIZONES; ?></td>
              </tr>
              <tr>
                <td><?php echo tep_draw_separator('pixel_trans.gif', '100%', '10'); ?></td>
              </tr>
              <tr>
                <td valign="top"><?php echo tep_draw_form('mz', FILENAME_GEO_ZONES, 'action=multi_countries&zID=' . $_GET['zID'] . '&zpage=' . $_GET['zpage'] . '&spage=' . $_GET['spage'], 'post'); ?><table border="0" width="100%" cellspacing="1" cellpadding="3">
                  <tr class="dataTableHeadingRow">
                    <td class="dataTableHeadingContent"><?php echo '<a href="javascript:void(0)" onClick="check_boxes(document.mz)" title="Page Select On/Off" class="menuBoxHeadingLink"><span class="dataTableHeadingContent">' . TABLE_HEADING_SELECT . '</span></a>'; ?></td>
                    <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_ZONE_CODE; ?></td>
                    <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_ZONE_NAME; ?></td>
                  </tr>

<?php
    $rows = 0;
    $zones_query_raw = "select z.zone_id, z.zone_code, z.zone_name from " . TABLE_ZONES . " z where status_id='1' and zone_country_id = '" . (int)$_GET['countries_list'] . "'";
    $zones_split = new splitPageResults($_GET['mzpage'], LOCAL_SPLIT_RESULTS, $zones_query_raw, $zones_query_numrows);
    $zones_query = tep_db_query($zones_query_raw);
    $bCheck = false;
    while ($zones = tep_db_fetch_array($zones_query)) {
      $rows++;
      $row_class = ($rows%2)?'dataTableRow':'dataTableRowSelected';
      echo '                  <tr class="' . $row_class . '">';
?>
                    <td class="dataTableContent"><?php echo tep_draw_checkbox_field('zones_id[' . $zones['zone_id'] . ']', ($bCheck?'on':''), $bCheck ); ?></td>
                    <td class="dataTableContent"><?php echo $zones['zone_code']; ?></td>
                    <td class="dataTableContent"><?php echo $zones['zone_name']; ?></td>
                  </tr>
<?php
    }
?>
                  <tr>
                    <td colspan="3"><table border="0" width="100%" cellspacing="0" cellpadding="2">
                      <tr>
                        <td><?php echo '<a href="' . tep_href_link(FILENAME_GEO_ZONES, 'zpage=' . $_GET['zpage'] . '&zID=' . $_GET['zID'] . '&action=list&spage=' . $_GET['spage'] . '&sID=' . $_GET['sID']) . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>' . tep_draw_hidden_field('multi_form', 'multi_zones') . '&nbsp;' . tep_image_submit('button_insert.gif', IMAGE_INSERT) . tep_draw_hidden_field('multi_form', 'multi_zones') . tep_draw_hidden_field('countries_list', $_GET['countries_list']); ?></td>
                      </tr>
                    </table></td>
                  </tr>
                </table></form></td>
              </tr>

              <tr>
                <td><table border="0" width="100%" cellspacing="0" cellpadding="2">
                  <tr>
                    <td class="smallText" valign="top"><?php echo $zones_split->display_count($zones_query_numrows, LOCAL_SPLIT_RESULTS, $_GET['mzpage'], TEXT_DISPLAY_NUMBER_OF_ZONES); ?></td>
                    <td class="smallText" align="right"><?php echo $zones_split->display_links($zones_query_numrows, LOCAL_SPLIT_RESULTS, MAX_DISPLAY_PAGE_LINKS, $_GET['mzpage'], '&action=multi_zones&zID=' . $_GET['zID'] . '&zpage=' . $_GET['zpage'] . '&spage=' . $_GET['spage'] . '&countries_list=' . $_GET['countries_list'], 'mzpage'); ?></td>
                  </tr>
                </table></td>
              </tr>

<?php
  } elseif($action == 'delete_multizone') {
    $geo_query = tep_db_query("select geo_zone_name, geo_zone_description from " . TABLE_GEO_ZONES . " where geo_zone_id = '" . (int)$_GET['zID'] . "'");
    $geo_array = tep_db_fetch_array($geo_query);
?>
              <tr>
                <td class="smallText"><?php echo sprintf(TEXT_DELETE_MULTIZONE_CONFIRM, $geo_array['geo_zone_name'], $geo_array['geo_zone_description']); ?></td>
              </tr>
              <tr>
                <td><?php echo tep_draw_separator('pixel_trans.gif', '100%', '10'); ?></td>
              </tr>
              <tr>
                <td valign="top"><?php echo tep_draw_form('rl_confirm', FILENAME_GEO_ZONES, 'action=deleteconfirm_multizone&zID=' . $_GET['zID'] . '&zpage=' . $_GET['zpage'] . '&spage=' . $_GET['spage'], 'post'); ?><table border="0" width="100%" cellspacing="1" cellpadding="3">
                  <tr class="dataTableHeadingRow">
                    <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_COUNTRY; ?></td>
                    <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_ZONE_NAME; ?></td>
                  </tr>
<?php
    foreach ($_POST['tax_zone_id'] as $tax_zone_id=>$val) {
      $delete_query = tep_db_query("select z2gz.association_id, if(z2gz.zone_id, z.zone_name, 'All Zones') as final_zone_name, c.countries_name from " . TABLE_ZONES_TO_GEO_ZONES . " z2gz left join " . TABLE_COUNTRIES . " c on (c.countries_id=z2gz.zone_country_id) left join " . TABLE_ZONES . " z on (z2gz.zone_id=z.zone_id) where z2gz.geo_zone_id = '" . (int)$_GET['zID'] . "' and z2gz.association_id= '" . (int)$tax_zone_id . "'");
      if( $delete_array = tep_db_fetch_array($delete_query) ) {
        $rows++;
        $row_class = ($rows%2)?'dataTableRow':'dataTableRowSelected';
        echo '                  <tr class="' . $row_class . '">';
      
?>
                    <td class="dataTableContent"><?php echo tep_draw_hidden_field('tax_zone_id[' . $tax_zone_id .']') . $delete_array['countries_name']; ?></td>
                    <td class="dataTableContent"><?php echo $delete_array['final_zone_name']; ?></td>
                  </tr>

<?php
      }
    }
?>

                  <tr>
                    <td colspan="4">
<?php 
      echo '<a href="' . tep_href_link(FILENAME_GEO_ZONES, 'zpage=' . $_GET['zpage'] . '&spage=' . $_GET['spage'] . '&zID=' . $_GET['zID']) . '&action=list">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a> ' . tep_image_submit('button_confirm.gif', IMAGE_CONFIRM);
?>
                    </td>
                  </tr>
                </table></form></td>
              </tr>

<?php
  } elseif($action == 'delete_catzone') {
    $geo_query = tep_db_query("select geo_zone_name, geo_zone_description from " . TABLE_GEO_ZONES . " where geo_zone_id = '" . (int)$_GET['zID'] . "'");
    $geo_array = tep_db_fetch_array($geo_query);
?>
              <tr>
                <td class="smallText"><?php echo sprintf(TEXT_DELETE_CATZONE_CONFIRM, $geo_array['geo_zone_name'], $geo_array['geo_zone_description']); ?></td>
              </tr>
              <tr>
                <td><?php echo tep_draw_separator('pixel_trans.gif', '100%', '10'); ?></td>
              </tr>
              <tr>
                <td valign="top"><?php echo tep_draw_form('rl_confirm', FILENAME_GEO_ZONES, 'action=deleteconfirm_zone&zID=' . $_GET['zID'] . '&zpage=' . $_GET['zpage'] . '&spage=' . $_GET['spage'], 'post'); ?><table border="0" width="100%" cellspacing="1" cellpadding="3">
                  <tr class="dataTableHeadingRow">
                    <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_COUNTRY; ?></td>
                    <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_ZONE_NAME; ?></td>
                  </tr>
<?php
    $rows = 0;
    $zones_query = tep_db_query("select z.zone_name, z2gz.association_id, if(z2gz.zone_id, z.zone_name, 'All Zones') as final_zone_name, c.countries_name from " . TABLE_ZONES_TO_GEO_ZONES . " z2gz left join " . TABLE_COUNTRIES . " c on (c.countries_id=z2gz.zone_country_id) left join " . TABLE_ZONES . " z on (z2gz.zone_id=z.zone_id) where z2gz.geo_zone_id = '" . (int)$_GET['zID'] . "'");
    while( $delete_zones_array = tep_db_fetch_array($zones_query) ) {
      $rows++;
      $row_class = ($rows%2)?'dataTableRow':'dataTableRowSelected';
      echo '                  <tr class="' . $row_class . '">';
?>
                    <td class="dataTableContent"><?php echo $delete_zones_array['countries_name']; ?></td>
                    <td class="dataTableContent"><?php echo $delete_zones_array['final_zone_name']; ?></td>
                  </tr>
<?php
    }
?>

                  <tr>
                    <td colspan="4">
<?php 
      echo '<a href="' . tep_href_link(FILENAME_GEO_ZONES, 'zpage=' . $_GET['zpage'] . '&spage=' . $_GET['spage'] . '&zID=' . $_GET['zID']) . '&action=list">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a> ' . tep_image_submit('button_confirm.gif', IMAGE_CONFIRM);
?>
                    </td>
                  </tr>
                </table></form></td>
              </tr>


<?php
  } else {
?>
              <tr>
                <td valign="top"><table border="0" width="100%" cellspacing="1" cellpadding="3">
                  <tr class="dataTableHeadingRow">
                    <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_TAX_ZONES; ?></td>
                    <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_ACTION; ?>&nbsp;</td>
                  </tr>
<?php
    $zones_query_raw = "select geo_zone_id, geo_zone_name, geo_zone_description, last_modified, date_added from " . TABLE_GEO_ZONES . " order by geo_zone_name";
    $zones_split = new splitPageResults($_GET['zpage'], LOCAL_SPLIT_RESULTS, $zones_query_raw, $zones_query_numrows);
    $zones_query = tep_db_query($zones_query_raw);
    while ($zones = tep_db_fetch_array($zones_query)) {
      if ((!isset($_GET['zID']) || (isset($_GET['zID']) && ($_GET['zID'] == $zones['geo_zone_id']))) && !isset($zInfo) && (substr($action, 0, 3) != 'new')) {
        $num_zones_query = tep_db_query("select count(*) as num_zones from " . TABLE_ZONES_TO_GEO_ZONES . " where geo_zone_id = '" . (int)$zones['geo_zone_id'] . "' group by geo_zone_id");
        $num_zones = tep_db_fetch_array($num_zones_query);

        if ($num_zones['num_zones'] > 0) {
          $zones['num_zones'] = $num_zones['num_zones'];
        } else {
          $zones['num_zones'] = 0;
        }

        $zInfo = new objectInfo($zones);
      }
      if (isset($zInfo) && is_object($zInfo) && ($zones['geo_zone_id'] == $zInfo->geo_zone_id)) {
        echo '                  <tr id="defaultSelected" class="dataTableRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . tep_href_link(FILENAME_GEO_ZONES, 'zpage=' . $_GET['zpage'] . '&spage=1' . '&zID=' . $zInfo->geo_zone_id . '&action=list') . '\'">' . "\n";
      } else {
        echo '                  <tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . tep_href_link(FILENAME_GEO_ZONES, 'zpage=' . $_GET['zpage'] . '&zID=' . $zones['geo_zone_id']) . '\'">' . "\n";
      }
?>
                    <td class="dataTableContent"><?php echo '<a href="' . tep_href_link(FILENAME_GEO_ZONES, 'zpage=' . $_GET['zpage'] . '&zID=' . $zones['geo_zone_id'] . '&action=list') . '">' . tep_image(DIR_WS_ICONS . 'folder.gif', ICON_FOLDER) . '</a>&nbsp;' . $zones['geo_zone_name']; ?></td>
                    <td class="dataTableContent" align="right"><?php if (isset($zInfo) && is_object($zInfo) && ($zones['geo_zone_id'] == $zInfo->geo_zone_id)) { echo tep_image(DIR_WS_IMAGES . 'icon_arrow_right.gif'); } else { echo '<a href="' . tep_href_link(FILENAME_GEO_ZONES, 'zpage=' . $_GET['zpage'] . '&zID=' . $zones['geo_zone_id']) . '">' . tep_image(DIR_WS_IMAGES . 'icon_info.gif', IMAGE_ICON_INFO) . '</a>'; } ?>&nbsp;</td>
                  </tr>
<?php
    }
?>
                  <tr>
                    <td colspan="2"><table border="0" width="100%" cellspacing="0" cellpadding="2">
                      <tr>
                        <td class="smallText"><?php echo $zones_split->display_count($zones_query_numrows, LOCAL_SPLIT_RESULTS, $_GET['zpage'], TEXT_DISPLAY_NUMBER_OF_TAX_ZONES); ?></td>
                        <td class="smallText" align="right"><?php echo $zones_split->display_links($zones_query_numrows, LOCAL_SPLIT_RESULTS, MAX_DISPLAY_PAGE_LINKS, $_GET['zpage'], '', 'zpage'); ?></td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td align="right" colspan="2"><?php if (!$action) echo '<a href="' . tep_href_link(FILENAME_GEO_ZONES, 'zpage=' . $_GET['zpage'] . '&zID=' . $zInfo->geo_zone_id . '&action=new_zone') . '">' . tep_image_button('button_insert.gif', IMAGE_INSERT) . '</a>'; ?></td>
                  </tr>
                </table></td>
              </tr>
            </table></td>
          </tr>
<?php
  }
?>
        </table></td>
<?php

  $heading = array();
  $contents = array();

  if ($action == 'list') {
    switch ($saction) {
      case 'new':
        $heading[] = array('text' => '<b>' . TEXT_INFO_HEADING_NEW_SUB_ZONE . '</b>');

        $contents = array('form' => tep_draw_form('zones', FILENAME_GEO_ZONES, 'zpage=' . $_GET['zpage'] . '&zID=' . $_GET['zID'] . '&action=list&spage=' . $_GET['spage'] . '&' . (isset($_GET['sID']) ? 'sID=' . $_GET['sID'] . '&' : '') . 'saction=insert_sub'));
        $contents[] = array('text' => TEXT_INFO_NEW_SUB_ZONE_INTRO);
        $contents[] = array('text' => '<br>' . TEXT_INFO_COUNTRY . '<br>' . tep_draw_pull_down_menu('zone_country_id', tep_get_countries(TEXT_ALL_COUNTRIES), '', 'onChange="update_zone(this.form);"'));
        $contents[] = array('text' => '<br>' . TEXT_INFO_COUNTRY_ZONE . '<br>' . tep_draw_pull_down_menu('zone_id', tep_prepare_country_zones_pull_down()));
        $contents[] = array('align' => 'center', 'text' => '<br>' . tep_image_submit('button_insert.gif', IMAGE_INSERT) . ' <a href="' . tep_href_link(FILENAME_GEO_ZONES, 'zpage=' . $_GET['zpage'] . '&zID=' . $_GET['zID'] . '&action=list&spage=' . $_GET['spage'] . '&' . (isset($_GET['sID']) ? 'sID=' . $_GET['sID'] : '')) . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>');
        break;
      case 'edit':
        $heading[] = array('text' => '<b>' . TEXT_INFO_HEADING_EDIT_SUB_ZONE . '</b>');

        $contents = array('form' => tep_draw_form('zones', FILENAME_GEO_ZONES, 'zpage=' . $_GET['zpage'] . '&zID=' . $_GET['zID'] . '&action=list&spage=' . $_GET['spage'] . '&sID=' . $sInfo->association_id . '&saction=save_sub'));
        $contents[] = array('text' => TEXT_INFO_EDIT_SUB_ZONE_INTRO);
        $contents[] = array('text' => '<br>' . TEXT_INFO_COUNTRY . '<br>' . tep_draw_pull_down_menu('zone_country_id', tep_get_countries(TEXT_ALL_COUNTRIES), $sInfo->zone_country_id, 'onChange="update_zone(this.form);"'));
        $contents[] = array('text' => '<br>' . TEXT_INFO_COUNTRY_ZONE . '<br>' . tep_draw_pull_down_menu('zone_id', tep_prepare_country_zones_pull_down($sInfo->zone_country_id), $sInfo->zone_id));
        $contents[] = array('align' => 'center', 'text' => '<br>' . tep_image_submit('button_update.gif', IMAGE_UPDATE) . ' <a href="' . tep_href_link(FILENAME_GEO_ZONES, 'zpage=' . $_GET['zpage'] . '&zID=' . $_GET['zID'] . '&action=list&spage=' . $_GET['spage'] . '&sID=' . $sInfo->association_id) . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>');
        break;
/*
      case 'delete':
        $heading[] = array('text' => '<b>' . TEXT_INFO_HEADING_DELETE_SUB_ZONE . '</b>');

        $contents = array('form' => tep_draw_form('zones', FILENAME_GEO_ZONES, 'zpage=' . $_GET['zpage'] . '&zID=' . $_GET['zID'] . '&action=list&spage=' . $_GET['spage'] . '&sID=' . $sInfo->association_id . '&saction=deleteconfirm_sub'));
        $contents[] = array('text' => TEXT_INFO_DELETE_SUB_ZONE_INTRO);
        $contents[] = array('text' => '<br><b>' . $sInfo->countries_name . '</b>');
        $contents[] = array('align' => 'center', 'text' => '<br>' . tep_image_submit('button_delete.gif', IMAGE_DELETE) . ' <a href="' . tep_href_link(FILENAME_GEO_ZONES, 'zpage=' . $_GET['zpage'] . '&zID=' . $_GET['zID'] . '&action=list&spage=' . $_GET['spage'] . '&sID=' . $sInfo->association_id) . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>');
        break;
*/
      default:
        if (isset($sInfo) && is_object($sInfo)) {
          $heading[] = array('text' => '<b>' . $sInfo->countries_name . '</b>');

          //$contents[] = array('align' => 'center', 'text' => '<a href="' . tep_href_link(FILENAME_GEO_ZONES, 'zpage=' . $_GET['zpage'] . '&zID=' . $_GET['zID'] . '&action=list&spage=' . $_GET['spage'] . '&sID=' . $sInfo->association_id . '&saction=edit') . '">' . tep_image_button('button_edit.gif', IMAGE_EDIT) . '</a> <a href="' . tep_href_link(FILENAME_GEO_ZONES, 'zpage=' . $_GET['zpage'] . '&zID=' . $_GET['zID'] . '&action=list&spage=' . $_GET['spage'] . '&sID=' . $sInfo->association_id . '&saction=delete') . '">' . tep_image_button('button_delete.gif', IMAGE_DELETE) . '</a>');
          $contents[] = array('align' => 'center', 'text' => '<a href="' . tep_href_link(FILENAME_GEO_ZONES, 'zpage=' . $_GET['zpage'] . '&zID=' . $_GET['zID'] . '&action=list&spage=' . $_GET['spage'] . '&sID=' . $sInfo->association_id . '&saction=edit') . '">' . tep_image_button('button_edit.gif', IMAGE_EDIT) . '</a>');
          $contents[] = array('text' => '<br>' . TEXT_INFO_DATE_ADDED . ' ' . tep_date_short($sInfo->date_added));
          if (tep_not_null($sInfo->last_modified)) $contents[] = array('text' => TEXT_INFO_LAST_MODIFIED . ' ' . tep_date_short($sInfo->last_modified));
        }
        break;
    }
  } else {
    switch ($action) {
      case 'new_zone':
        $heading[] = array('text' => '<b>' . TEXT_INFO_HEADING_NEW_ZONE . '</b>');

        $contents = array('form' => tep_draw_form('zones', FILENAME_GEO_ZONES, 'zpage=' . $_GET['zpage'] . '&zID=' . $_GET['zID'] . '&action=insert_zone'));
        $contents[] = array('text' => TEXT_INFO_NEW_ZONE_INTRO);
        $contents[] = array('text' => '<br>' . TEXT_INFO_ZONE_NAME . '<br>' . tep_draw_input_field('geo_zone_name'));
        $contents[] = array('text' => '<br>' . TEXT_INFO_ZONE_DESCRIPTION . '<br>' . tep_draw_input_field('geo_zone_description'));
        $contents[] = array('align' => 'center', 'text' => '<br>' . tep_image_submit('button_insert.gif', IMAGE_INSERT) . ' <a href="' . tep_href_link(FILENAME_GEO_ZONES, 'zpage=' . $_GET['zpage'] . '&zID=' . $_GET['zID']) . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>');
        break;
      case 'edit_zone':
        $heading[] = array('text' => '<b>' . TEXT_INFO_HEADING_EDIT_ZONE . '</b>');

        $contents = array('form' => tep_draw_form('zones', FILENAME_GEO_ZONES, 'zpage=' . $_GET['zpage'] . '&zID=' . $zInfo->geo_zone_id . '&action=save_zone'));
        $contents[] = array('text' => TEXT_INFO_EDIT_ZONE_INTRO);
        $contents[] = array('text' => '<br>' . TEXT_INFO_ZONE_NAME . '<br>' . tep_draw_input_field('geo_zone_name', $zInfo->geo_zone_name));
        $contents[] = array('text' => '<br>' . TEXT_INFO_ZONE_DESCRIPTION . '<br>' . tep_draw_input_field('geo_zone_description', $zInfo->geo_zone_description));
        $contents[] = array('align' => 'center', 'text' => '<br>' . tep_image_submit('button_update.gif', IMAGE_UPDATE) . ' <a href="' . tep_href_link(FILENAME_GEO_ZONES, 'zpage=' . $_GET['zpage'] . '&zID=' . $zInfo->geo_zone_id) . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>');
        break;
/*
      case 'delete_zone':
        $heading[] = array('text' => '<b>' . TEXT_INFO_HEADING_DELETE_ZONE . '</b>');

        $contents = array('form' => tep_draw_form('zones', FILENAME_GEO_ZONES, 'zpage=' . $_GET['zpage'] . '&zID=' . $zInfo->geo_zone_id . '&action=deletecat_zone'));
        $contents[] = array('text' => TEXT_INFO_DELETE_ZONE_INTRO);
        $contents[] = array('text' => '<br><b>' . $zInfo->geo_zone_name . '</b>');
        $contents[] = array('align' => 'center', 'text' => '<br>' . tep_image_submit('button_delete.gif', IMAGE_DELETE) . ' <a href="' . tep_href_link(FILENAME_GEO_ZONES, 'zpage=' . $_GET['zpage'] . '&zID=' . $zInfo->geo_zone_id) . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>');
        break;
*/
      default:
        if (isset($zInfo) && is_object($zInfo)) {
          $heading[] = array('text' => '<b>' . $zInfo->geo_zone_name . '</b>');

          $contents[] = array('align' => 'center', 'text' => '<a href="' . tep_href_link(FILENAME_GEO_ZONES, 'zpage=' . $_GET['zpage'] . '&zID=' . $zInfo->geo_zone_id . '&action=edit_zone') . '">' . tep_image_button('button_edit.gif', IMAGE_EDIT) . '</a> <a href="' . tep_href_link(FILENAME_GEO_ZONES, 'zpage=' . $_GET['zpage'] . '&zID=' . $zInfo->geo_zone_id . '&action=delete_catzone') . '">' . tep_image_button('button_delete.gif', IMAGE_DELETE) . '</a>' . ' <a href="' . tep_href_link(FILENAME_GEO_ZONES, 'zpage=' . $_GET['zpage'] . '&spage=1' . '&zID=' . $zInfo->geo_zone_id . '&action=list') . '">' . tep_image_button('button_details.gif', IMAGE_DETAILS) . '</a>');
          $contents[] = array('text' => '<br>' . TEXT_INFO_NUMBER_ZONES . ' ' . $zInfo->num_zones);
          $contents[] = array('text' => '<br>' . TEXT_INFO_DATE_ADDED . ' ' . tep_date_short($zInfo->date_added));
          if (tep_not_null($zInfo->last_modified)) $contents[] = array('text' => TEXT_INFO_LAST_MODIFIED . ' ' . tep_date_short($zInfo->last_modified));
          $contents[] = array('text' => '<br>' . TEXT_INFO_ZONE_DESCRIPTION . '<br>' . $zInfo->geo_zone_description);
        }
        break;
    }
  }

  if ( (tep_not_null($heading)) && (tep_not_null($contents)) ) {
    echo '            <td width="25%" valign="top">' . "\n";

    $box = new box;
    echo $box->infoBox($heading, $contents);

    echo '            </td>' . "\n";
  }
?>
          </tr>
        </table></td>
      </tr>
    </table></td>
<!-- body_text_eof //-->
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
<br>
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
