<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2007 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
//----------------------------------------------------------------------------
// Admin: Generic Text script
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
  require('includes/application_top.php');

  $action = (isset($_GET['action']) ? $_GET['action'] : '');

  switch ($action) {
    case 'setflag':
      if ( ($_GET['flag'] == '0') || ($_GET['flag'] == '1') ) {
        if (isset($_GET['gtID'])) {
          tep_set_generic_text_status($_GET['gtID'], $_GET['flag']);
        }
      }
      tep_redirect(tep_href_link(FILENAME_GENERIC_TEXT, 'gtID=' . $_GET['gtID']));
      break;

    case 'delete_generic_text_confirm':
      if( isset($_POST['gtext_id']) && tep_not_null($_POST['gtext_id']) ) {
        $gtext_id = $_POST['gtext_id'];
        tep_db_query("delete from " . TABLE_GTEXT . " where gtext_id = '" . (int)$gtext_id . "'");
        tep_db_query("delete from " . TABLE_GTEXT_TO_PRODUCTS . " where gtext_id = '" . (int)$gtext_id . "'");
        tep_db_query("delete from " . TABLE_GTEXT_TO_DISPLAY . " where gtext_id = '" . (int)$gtext_id . "'");
        tep_db_query("delete from " . TABLE_SEO_TO_GTEXT . " where gtext_id = '" . (int)$gtext_id . "'");
        tep_db_query("delete from " . TABLE_META_GTEXT . " where gtext_id = '" . (int)$gtext_id . "'");
      }

      tep_redirect(tep_href_link(FILENAME_GENERIC_TEXT));
      break;
    case 'insert_generic_text':
    case 'update_generic_text':
      if (isset($_POST['edit_x']) || isset($_POST['edit_y'])) {
        $action = 'new_generic_text';
      } else {
        $sql_data_array = array('gtext_title' => tep_db_prepare_input($_POST['gtext_title']),
                                'gtext_description' => tep_db_prepare_input($_POST['gtext_description']),
                                'status' => tep_db_prepare_input($_POST['status'])
                               );

        if (isset($_GET['gtID'])) {
          $gtext_id = tep_db_prepare_input($_GET['gtID']);
        } elseif($action == 'insert_generic_text') {
          tep_db_perform(TABLE_GTEXT, $sql_data_array);
          $gtext_id = tep_db_insert_id();
        } else {
          tep_redirect(tep_href_link(FILENAME_GENERIC_TEXT));
        }

//-MS- SEO-G Added
        $seo_clear = false;
        if(tep_not_null($_POST['seo_name']) ) {
          $seo_name = $_POST['seo_name'];
        } else {
          $seo_name = $_POST['gtext_title'];
          $seo_clear = true;
        }
        require_once(DIR_WS_CLASSES . 'seo_zones.php');
        $cSEO = new seo_zones();
        $seo_name = $cSEO->create_safe_string($seo_name);
        $seog_array = array('gtext_id' => (int)$gtext_id, 'seo_name' => tep_db_prepare_input($seo_name) );
//-MS- SEO-G Added EOM


//-MS- META-G Added
        $metag_title_array = $_POST['meta_title'];
        $metag_keywords_array = $_POST['meta_keywords'];
        $metag_text_array = $_POST['meta_text'];
        $metag_array = array(
                             'meta_title' => (tep_not_null($metag_title_array) ? tep_db_prepare_input($metag_title_array) :  tep_db_prepare_input($_POST['gtext_title'])),
                             'meta_keywords' => (tep_not_null($metag_keywords_array) ? tep_db_prepare_input($metag_keywords_array) :  tep_db_prepare_input($_POST['gtext_title'])),
                             'meta_text' => (tep_not_null($metag_text_array) ? tep_db_prepare_input($metag_text_array) :  tep_db_prepare_input($_POST['gtext_title'])),
                            );
//-MS- META-G Added EOM

        if($action == 'insert_generic_text') {
//-MS- SEO-G Added
          if( !$seo_clear ) {
            tep_db_perform(TABLE_SEO_TO_GTEXT, $seog_array);
//-MS- SEO-G Added EOM

//-MS- META-G Added
            $insert_sql_data = array('gtext_id' => (int)$gtext_id,
                                     'language_id' => (int)$languages_id);

            $metag_array = array_merge($metag_array, $insert_sql_data);
            tep_db_perform(TABLE_META_GTEXT, $metag_array);
          } else {
            tep_db_query("delete from " . TABLE_SEO_TO_GTEXT . " where gtext_id = '" . (int)$gtext_id . "'");
            tep_db_query("delete from " . TABLE_META_GTEXT . " where gtext_id = '" . (int)$gtext_id . "'");
          }
//-MS- META-G Added EOM
        } elseif ($action == 'update_generic_text') {

          tep_db_perform(TABLE_GTEXT, $sql_data_array, 'update', "gtext_id = '" . (int)$gtext_id . "'");

          if( !$seo_clear ) {
            $check_query = tep_db_query("select gtext_id from " . TABLE_SEO_TO_GTEXT . " where gtext_id = '" . (int)$gtext_id . "'");
            if( !tep_db_num_rows($check_query) ) {
              tep_db_perform(TABLE_SEO_TO_GTEXT, $seog_array);
            } else {
              unset($seog_array['gtext_id']);
              tep_db_perform(TABLE_SEO_TO_GTEXT, $seog_array, 'update', "gtext_id = '" . (int)$gtext_id . "'");
            }
//-MS- SEO-G Added
            $check_query = tep_db_query("select gtext_id from " . TABLE_SEO_TO_GTEXT . " where gtext_id = '" . (int)$gtext_id . "'");
            if( !tep_db_num_rows($check_query) ) {
              tep_db_perform(TABLE_SEO_TO_GTEXT, $seog_array);
            } else {
              unset($seog_array['gtext_id']);
              tep_db_perform(TABLE_SEO_TO_GTEXT, $seog_array, 'update', "gtext_id = '" . (int)$gtext_id . "'");
            }
//-MS- SEO-G Added EOM

//-MS- META-G Added
            $check_query = tep_db_query("select gtext_id from " . TABLE_META_GTEXT . " where gtext_id = '" . (int)$gtext_id . "' and language_id = '" . (int)$languages_id . "'");
            if( !tep_db_num_rows($check_query) ) {
              $metag_array['gtext_id'] = (int)$gtext_id;
              $metag_array['language_id'] = (int)$languages_id;
              tep_db_perform(TABLE_META_GTEXT, $metag_array);
            } else {
              tep_db_perform(TABLE_META_GTEXT, $metag_array, 'update', "gtext_id = '" . (int)$gtext_id . "' and language_id = '" . (int)$languages_id . "'");
            }
          } else {
            tep_db_query("delete from " . TABLE_SEO_TO_GTEXT . " where gtext_id = '" . (int)$gtext_id . "'");
            tep_db_query("delete from " . TABLE_META_GTEXT . " where gtext_id = '" . (int)$gtext_id . "'");
          }
//-MS- META-G Added EOM
        }
        tep_redirect(tep_href_link(FILENAME_GENERIC_TEXT, 'gtID=' . $gtext_id));
      }
      break;
    case 'copy_to_confirm':
      if( isset($_POST['gtext_id']) && tep_not_null($_POST['gtext_id']) ) {
        $gtext_id = tep_db_prepare_input($_POST['gtext_id']);
        if ($_POST['copy_as'] == 'duplicate') {
          $generic_text_query = tep_db_query("select gtext_title, gtext_description from " . TABLE_GTEXT . " where gtext_id = '" . (int)$gtext_id . "'");
          $generic_text = tep_db_fetch_array($generic_text_query);

          tep_db_query("insert into " . TABLE_GTEXT . " (gtext_title, gtext_description, status) values ('" . tep_db_input($generic_text['gtext_title']) . "', '" . tep_db_input($generic_text['gtext_description']) . "', '0')");
          $gtext_id = tep_db_insert_id();
        }
      }

      tep_redirect(tep_href_link(FILENAME_GENERIC_TEXT, 'gtID=' . $gtext_id));
      break;

    case 'generic_text_preview':
      break;
    case 'sload':
      //if( isset($_GET['gtID']) && tep_not_null($_GET['gtID']) ) {
        $generic_text_query = tep_db_query("select gtext_description from " . TABLE_GTEXT . " where gtext_id = '" . (int)$_GET['gtID'] . "'");
        if( tep_db_num_rows($generic_text_query) ) {
          $generic_text = tep_db_fetch_array($generic_text_query);
          echo $generic_text['gtext_description'];
        } else {
          echo 'error';
        }
        tep_exit();
      //}
      break;
    default:
      break;
  }
?>
<?php require('includes/objects/html_start_sub1.php'); ?>
<script language="javascript" src="includes/general.js"></script>
<script type="text/javascript" src="includes/javascript/jquery/jquery.js"></script>
<script type="text/javascript" src="includes/javascript/jquery.ipweditor-1.2.js"></script>
<?php
  if( !isset($_GET['action']) || (isset($_GET['action']) && $_GET['action'] == 'new_generic_text') ) {
    $mce_str = 'gtext_description'; // YOURCODEHERE // Comma separated list of textarea names
    // You can add more textareas to convert in the $mce_str, be careful that they are all separated by a comma.
    echo '<script language="javascript" type="text/javascript" src="includes/javascript/tiny_mce/tiny_mce.js"></script>';
    include "includes/javascript/tiny_mce/general2.php";
  }
?>
<script type="text/javascript">
  $().ready(function() {
    var ed = new tinymce.Editor('content', {
      some_setting : 1
    });
  });
</script>
<?php
  $set_focus = true;
  require('includes/objects/html_start_sub2.php'); 
?>
<?php 
  $heading_row = true;
  require('includes/objects/html_body_header.php'); 
?>
<?php
  if ($action == 'new_generic_text') {
    $parameters = array(
                        'gtext_id' => '',
                        'gtext_title' => '',
                        'gtext_description' => '',
                        'status' => '',
                        'seo_name' => '',
                       );

    $gtInfo = new objectInfo($parameters);

    if (isset($_GET['gtID']) && empty($_POST)) {
      $generic_text_query = tep_db_query("select gtext_id, gtext_title, gtext_description, status from " . TABLE_GTEXT . " where gtext_id = '" . (int)$_GET['gtID'] . "'");
      $generic_text = tep_db_fetch_array($generic_text_query);

//-MS- SEO-G Added
      $seog_query = tep_db_query("select seo_name from " . TABLE_SEO_TO_GTEXT . " where gtext_id = '" . (int)$_GET['gtID'] . "'");
      if( $seog_array = tep_db_fetch_array($seog_query) ) {
      } else {
        $seog_array = array('seo_name' => '');
      }
      $generic_text = array_merge($generic_text, $seog_array);
//-MS- SEO-G Added EOM

//-MS- META-G Added
      $metag_query = tep_db_query("select meta_title, meta_keywords, meta_text from " . TABLE_META_GTEXT . " where gtext_id = '" . (int)$_GET['gtID'] . "' and language_id = '" . (int)$languages_id . "'");
      if( $metag_array = tep_db_fetch_array($metag_query) ) {
      } else {
        $metag_array = array('meta_title' => '', 'meta_keywords' => '', 'meta_text' => '');
      }
      $generic_text = array_merge($generic_text, $metag_array);
//-MS- META-G Added EOM

      $gtInfo->objectInfo($generic_text);
    } elseif (tep_not_null($_POST)) {
      $gtInfo->objectInfo($_POST);
      $gtext_title = $_POST['gtext_title'];
      $gtext_description = $_POST['gtext_description'];

//-MS- META-G Added
      $metag_title_array = $_POST['meta_title'];
      $metag_keywords_array = $_POST['meta_keywords'];
      $metag_text_array = $_POST['meta_text'];
//-MS- META-G Added EOM
    }

    if (!isset($gtInfo->status)) $gtInfo->status = '1';
    switch ($gtInfo->status) {
      case '0': $in_status = false; $out_status = true; break;
      case '1':
      default: $in_status = true; $out_status = false;
    }
?>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td class="pageHeading"><?php echo TEXT_NEW_GENERIC; ?></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><hr /></td>
      </tr>

      <tr>
        <td><?php echo tep_draw_form('new_generic_text', FILENAME_GENERIC_TEXT, (isset($_GET['gtID']) ? 'gtID=' . $_GET['gtID'] : '') . '&action=generic_text_preview', 'post', 'enctype="multipart/form-data"'); ?><table border="0" width="100%" cellspacing="0" cellpadding="2">
          <tr>
            <td><table border="0" cellspacing="0" cellpadding="2">
              <tr>
                <td class="main"><b><?php echo TEXT_GENERIC_STATUS; ?></b></td>
              </tr>
              <tr>
                <td class="main"><?php echo tep_draw_radio_field('status', '0', $out_status) . '&nbsp;' . TEXT_GENERIC_NOT_AVAILABLE . '&nbsp;' . tep_draw_radio_field('status', '1', $in_status) . '&nbsp;' . TEXT_GENERIC_AVAILABLE; ?></td>
              </tr>
              <tr>
                <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
              </tr>
              <tr>
                <td class="main"><b><?php echo TEXT_GENERIC_NAME; ?></b></td>
              </tr>
              <tr>
                <td class="main"><?php echo tep_draw_input_field('gtext_title', (isset($gtext_title)?$gtext_title:$gtInfo->gtext_title), 'size="70"'); ?></td>
              <tr>
                <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '4'); ?></td>
              </tr>
<?php
//-MS- SEO-G Added
?>
              <tr bgcolor="#ffffeb">
                <td><table border="0" cellspacing="0" cellpadding="2">
                  <tr>
                    <td class="smallText"><b><?php echo TEXT_SEO_NAME; ?></b></td>
                    <td class="smallText"><?php echo tep_draw_separator('pixel_trans.gif', '24', '15') . '&nbsp;' . tep_draw_input_field('seo_name', $gtInfo->seo_name); ?></td>
                  </tr>
                </table></td>
              </tr>
<?php
//-MS- SEO-G Added EOM
?>
              <tr>
                <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
              </tr>

<?php
//-MS- META-G Added
      if( !isset($metag_title_array) ) {
        $metag_query = tep_db_query("select meta_title, meta_keywords, meta_text from " . TABLE_META_GTEXT . " where gtext_id = '" . (int)$gtInfo->gtext_id .  "' and language_id = '" . (int)$languages_id . "'");
        if( $metag_array = tep_db_fetch_array($metag_query) ) {
          $metag_title = stripslashes($metag_array['meta_title']);
          $metag_keywords = stripslashes($metag_array['meta_keywords']);
          $metag_text = stripslashes($metag_array['meta_text']);
        } else {
          $metag_title = '';
          $metag_keywords = '';
          $metag_text = '';
        }
      } else {
        $metag_title = stripslashes($metag_title_array);
        $metag_keywords = stripslashes($metag_keywords_array);
        $metag_text = stripslashes($metag_keywords_array);
      }
      echo '<tr bgcolor="#ffffeb"><td class="smallText"><b>' . TEXT_METAG . '</b></td></tr>' . "\n";
?>
              <tr>
                <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '2'); ?></td>
              </tr>
              <tr bgcolor="#ffffeb">
                <td><table border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '4'); ?></td>
                  </tr>
                  <tr>
                    <td><table border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td class="smallText"><?php echo TEXT_META_TITLE . '&nbsp;'; ?></td>
                        <td class="smallText"><?php echo tep_draw_input_field('meta_title', $metag_title, 'size="62"'); ?></td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '4'); ?></td>
                  </tr>
                  <tr>
                    <td class="smallText"><?php echo TEXT_META_KEYWORDS; ?></td>
                  </tr>
                  <tr>
                    <td class="smallText"><?php echo tep_draw_textarea_field('meta_keywords', 'soft', '70', '2', $metag_keywords); ?></td>
                  </tr>
                  <tr>
                    <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '4'); ?></td>
                  </tr>
                  <tr>
                    <td class="smallText"><?php echo TEXT_META_TEXT; ?></td>
                  </tr>
                  <tr>
                    <td class="smallText"><?php echo tep_draw_textarea_field('meta_text', 'soft', '70', '2', $metag_text); ?></td>
                  </tr>
                </table></td>
              </tr>
              <tr>
                <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '4'); ?></td>
              </tr>
<?php
//-MS- META-G Added EOM
?>
              <tr>
                <td class="main"><b><?php echo TEXT_GENERIC_DESCRIPTION; ?></b></td>
              </tr>
              <tr>
                <td class="main"><?php echo tep_draw_textarea_field('gtext_description', 'soft', '70', '15', (isset($gtext_description)?$gtext_description:$gtInfo->gtext_description) ); ?></td>
              </tr>
              <tr>
                <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
          </tr>
          <tr>
            <td class="main"><?php echo tep_image_submit('button_preview.gif', IMAGE_PREVIEW) . '&nbsp;&nbsp;<a href="' . tep_href_link(FILENAME_GENERIC_TEXT, (isset($_GET['gtID']) ? 'gtID=' . $_GET['gtID'] : '')) . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>'; ?></td>
          </tr>
        </table></form></td>
      </tr>
<?php
  } elseif ($action == 'generic_text_preview') {

    if (tep_not_null($_POST)) {
      $gtInfo = new objectInfo($_POST);
      $gtext_title = $_POST['gtext_title'];
      $gtext_description = $_POST['gtext_description'];
    } else {
      $generic_text_query = tep_db_query("select gtext_id, gtext_title, gtext_description, status from " . TABLE_GTEXT . " where gtext_id = '" . (int)$_GET['gtID'] . "'");
      $generic_text = tep_db_fetch_array($generic_text_query);
      $gtInfo = new objectInfo($generic_text);
    }

    $form_action = (isset($_GET['gtID'])) ? 'update_generic_text' : 'insert_generic_text';
?>
      <tr>
        <td><?php echo tep_draw_form($form_action, FILENAME_GENERIC_TEXT, (isset($_GET['gtID']) ? 'gtID=' . $_GET['gtID'] : '') . '&action=' . $form_action, 'post', 'enctype="multipart/form-data"'); ?><table border="0" width="100%" cellspacing="0" cellpadding="2">
          <tr>
            <td class="pageHeading"><?php echo $gtInfo->gtext_title; ?></td>
          </tr>
          <tr>
            <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
          </tr>
<?php
    if ($gtInfo->gtext_description) {
?>
          <tr>
            <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
          </tr>
          <tr>
            <td class="main"><?php echo $gtInfo->gtext_description; ?></td>
          </tr>
          <tr>
            <td><?php echo tep_draw_separator('pixel_trans.gif', '1', '10'); ?></td>
          </tr>
<?php
    }
?>

<?php
    if (isset($_GET['read']) && ($_GET['read'] == 'only')) {
      if (isset($_GET['origin'])) {
        $pos_params = strpos($_GET['origin'], '?', 0);
        if ($pos_params != false) {
          $back_url = substr($_GET['origin'], 0, $pos_params);
          $back_url_params = substr($_GET['origin'], $pos_params + 1);
        } else {
          $back_url = $_GET['origin'];
          $back_url_params = '';
        }
      } else {
        $back_url = FILENAME_GENERIC_TEXT;
        $back_url_params = 'gtID=' . $gtInfo->gtext_id;
      }
?>
          <tr>
            <td align="right"><?php echo '<a href="' . tep_href_link($back_url, $back_url_params, 'NONSSL') . '">' . tep_image_button('button_back.gif', IMAGE_BACK) . '</a>'; ?></td>
          </tr>
<?php
    } else {
?>
          <tr>
            <td align="right" class="smallText">
<?php
// Re-Post all POSTed variables
      reset($_POST);
      while (list($key, $value) = each($_POST)) {
        if (!is_array($_POST[$key])) {
          echo tep_draw_hidden_field($key, htmlspecialchars(stripslashes($value)));
        }
      }
      echo tep_draw_hidden_field('gtext_title', htmlspecialchars(stripslashes($gtext_title)));
      echo tep_draw_hidden_field('gtext_description', htmlspecialchars(stripslashes($gtext_description)));
      echo tep_image_submit('button_back.gif', IMAGE_BACK, 'name="edit"') . '&nbsp;&nbsp;';

      if (isset($_GET['gtID'])) {
        echo tep_image_submit('button_update.gif', IMAGE_UPDATE);
      } else {
        echo tep_image_submit('button_insert.gif', IMAGE_INSERT);
      }
      echo '&nbsp;&nbsp;<a href="' . tep_href_link(FILENAME_GENERIC_TEXT, (isset($_GET['gtID']) ? 'gtID=' . $_GET['gtID'] : '')) . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>';
?>
            </td>
          </tr>
        </table></form></td>
      </tr>
<?php
    }
  } else {
?>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td class="pageHeading"><?php echo HEADING_TITLE; ?></td>
            <td class="pageHeading" align="right"><?php echo tep_draw_separator('pixel_trans.gif', 1, HEADING_IMAGE_HEIGHT); ?></td>
            <td align="right"><table border="0" width="100%" cellspacing="0" cellpadding="0">
              <tr>
                <td class="smallText" align="right">
<?php
    echo tep_draw_form('search', FILENAME_GENERIC_TEXT, '', 'get');
    echo HEADING_TITLE_SEARCH . ' ' . tep_draw_input_field('search');
    echo tep_draw_hidden_field(tep_session_name(),tep_session_id());
    echo '</form>';
?>
                </td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top"><table border="0" width="100%" cellspacing="1" cellpadding="3">
              <tr class="dataTableHeadingRow">
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_ID; ?></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_TITLE; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_EDIT; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_STATUS; ?></td>
                <td class="dataTableHeadingContent" align="right"><?php echo TABLE_HEADING_ACTION; ?>&nbsp;</td>
              </tr>
<?php
    $generic_count = 0;
    $rows = 0;
    if (isset($_GET['search'])) {
      $generic_text_query = tep_db_query("select gtext_id, gtext_title, gtext_description, status from " . TABLE_GTEXT . " where (gtext_title like '%" . tep_db_input($search) . "%' or gtext_description like '%" . tep_db_input($search) . "%') order by gtext_id desc");
    } else {
      $generic_text_query = tep_db_query("select gtext_id, gtext_title, gtext_description, status from " . TABLE_GTEXT . " order by gtext_id desc");
    }

    while ($generic_text = tep_db_fetch_array($generic_text_query)) {
      if( (!isset($_GET['gtID']) && !isset($gtInfo) ) || (isset($_GET['gtID']) && ($_GET['gtID'] == $generic_text['gtext_id'])) && (substr($action, 0, 3) != 'new')) {
        $gtInfo = new objectInfo($generic_text);
      }
      $generic_count++;
      $rows++;
/*
      if (isset($gtInfo) && is_object($gtInfo) && ($generic_text['gtext_id'] == $gtInfo->gtext_id) ) {
        echo '              <tr id="defaultSelected" class="dataTableRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . tep_href_link(FILENAME_GENERIC_TEXT, 'gtID=' . $generic_text['gtext_id'] . '&action=generic_text_preview&read=only') . '\'">' . "\n";
      } else {
        echo '              <tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . tep_href_link(FILENAME_GENERIC_TEXT, 'gtID=' . $generic_text['gtext_id']) . '\'">' . "\n";
      }
*/
      if (isset($gtInfo) && is_object($gtInfo) && ($generic_text['gtext_id'] == $gtInfo->gtext_id) ) {
        echo '              <tr id="defaultSelected" class="dataTableRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)">' . "\n";
      } else {
        echo '              <tr class="dataTableRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)">' . "\n";
      }

?>
                <td class="dataTableContent"><?php echo '<a href="' . tep_href_link(FILENAME_GENERIC_TEXT, 'gtID=' . $generic_text['gtext_id'] . '&action=generic_text_preview&read=only') . '">' . tep_image(DIR_WS_ICONS . 'preview.gif', ICON_PREVIEW) . '</a>&nbsp;' . $generic_text['gtext_id']; ?></td>
                <td class="dataTableContent"><?php echo $generic_text['gtext_title']; ?></td>
                <td class="dataTableContent" align="center">
<?php
//-MS- Direct Edit Added
      echo '<a href="' . tep_href_link(basename($PHP_SELF), 'gtID=' . $generic_text['gtext_id'] . '&action=new_generic_text') . '">' . tep_image(DIR_WS_ICONS . 'edit.gif', TEXT_EDIT . ' ' . $generic_text['gtext_title']) . '</a>';
//-MS- Direct Edit Added EOM
?>
                </td>
                <td class="dataTableContent" align="center">
<?php
      if ($generic_text['status'] == '1') {
        echo tep_image(DIR_WS_IMAGES . 'icon_status_green.gif', IMAGE_ICON_STATUS_GREEN, 10, 10) . '&nbsp;&nbsp;<a href="' . tep_href_link(FILENAME_GENERIC_TEXT, 'action=setflag&flag=0&gtID=' . $generic_text['gtext_id']) . '">' . tep_image(DIR_WS_IMAGES . 'icon_status_red_light.gif', IMAGE_ICON_STATUS_RED_LIGHT, 10, 10) . '</a>';
      } else {
        echo '<a href="' . tep_href_link(FILENAME_GENERIC_TEXT, 'action=setflag&flag=1&gtID=' . $generic_text['gtext_id']) . '">' . tep_image(DIR_WS_IMAGES . 'icon_status_green_light.gif', IMAGE_ICON_STATUS_GREEN_LIGHT, 10, 10) . '</a>&nbsp;&nbsp;' . tep_image(DIR_WS_IMAGES . 'icon_status_red.gif', IMAGE_ICON_STATUS_RED, 10, 10);
      }
?>
                </td>
                <td class="dataTableContent" align="right"><?php if (isset($gtInfo) && is_object($gtInfo) && ($generic_text['gtext_id'] == $gtInfo->gtext_id)) { echo tep_image(DIR_WS_IMAGES . 'icon_arrow_right.gif', ''); } else { echo '<a href="' . tep_href_link(FILENAME_GENERIC_TEXT, 'gtID=' . $generic_text['gtext_id']) . '">' . tep_image(DIR_WS_IMAGES . 'icon_info.gif', IMAGE_ICON_INFO) . '</a>'; } ?>&nbsp;</td>
              </tr>
<?php
    }
?>
              <tr>
                <td colspan="5"><table border="0" width="100%" cellspacing="0" cellpadding="2">
                  <tr>
                    <td class="smallText"><?php echo TEXT_GENERIC . '&nbsp;' . $generic_count; ?></td>
                    <td align="right" class="smallText"><?php if (!isset($_GET['search'])) echo '<a href="' . tep_href_link(FILENAME_GENERIC_TEXT, 'action=new_generic_text') . '">' . tep_image_button('button_new.gif', IMAGE_NEW_GENERIC_TEXT) . '</a>'; ?>&nbsp;</td>
                  </tr>
                </table></td>
              </tr>
            </table></td>
<?php
    $heading = array();
    $contents = array();
    switch ($action) {
      case 'delete_generic_text':
        $heading[] = array('text' => '<b>' . TEXT_INFO_HEADING_DELETE_GENERIC . '</b>');

        $contents = array('form' => tep_draw_form('generic_text', FILENAME_GENERIC_TEXT, 'action=delete_generic_text_confirm') . tep_draw_hidden_field('gtext_id', $gtInfo->gtext_id));
        $contents[] = array('text' => TEXT_DELETE_GENERIC_INTRO);
        $contents[] = array('text' => '<br><b>' . $gtInfo->gtext_title . '</b>');
        $contents[] = array('align' => 'center', 'text' => '<br>' . tep_image_submit('button_delete.gif', IMAGE_DELETE) . ' <a href="' . tep_href_link(FILENAME_GENERIC_TEXT, 'gtID=' . $gtInfo->gtext_id) . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>');
        break;
      case 'copy_to':
        $heading[] = array('text' => '<b>' . TEXT_INFO_HEADING_COPY_TO . '</b>');
        $contents = array('form' => tep_draw_form('copy_to', FILENAME_GENERIC_TEXT, 'action=copy_to_confirm') . tep_draw_hidden_field('gtext_id', $gtInfo->gtext_id));
        $contents[] = array('text' => TEXT_INFO_COPY_TO_INTRO);
        $contents[] = array('text' => tep_draw_hidden_field('copy_as', 'duplicate'));
        $contents[] = array('align' => 'center', 'text' => '<br>' . tep_image_submit('button_copy.gif', IMAGE_COPY) . ' <a href="' . tep_href_link(FILENAME_GENERIC_TEXT, 'gtID=' . $gtInfo->gtext_id) . '">' . tep_image_button('button_cancel.gif', IMAGE_CANCEL) . '</a>');
        break;

      default:
        if ($rows > 0) {
          if (isset($gtInfo) && is_object($gtInfo)) { // generic_text info box contents
            $heading[] = array('text' => '<b>' . $gtInfo->gtext_title . '</b>');
            $contents[] = array('align' => 'center', 'text' => '<a href="' . tep_href_link(FILENAME_GENERIC_TEXT, 'gtID=' . $gtInfo->gtext_id . '&action=new_generic_text') . '">' . tep_image_button('button_edit.gif', IMAGE_EDIT) . '</a> <a href="' . tep_href_link(FILENAME_GENERIC_TEXT, 'gtID=' . $gtInfo->gtext_id . '&action=delete_generic_text') . '">' . tep_image_button('button_delete.gif', IMAGE_DELETE) . '</a> <a href="' . tep_href_link(FILENAME_GENERIC_TEXT, 'gtID=' . $gtInfo->gtext_id . '&action=copy_to') . '">' . tep_image_button('button_copy_to.gif', IMAGE_COPY_TO) . '</a>');
            $contents[] = array('text' => '<br>' . $gtInfo->gtext_title);
          }
        } else { // create generic_text dummy info
          $heading[] = array('text' => '<b>' . EMPTY_GENERIC . '</b>');
          $contents[] = array('text' => TEXT_NO_GENERIC);
        }
        break;
    }

    if ( (tep_not_null($heading)) && (tep_not_null($contents)) ) {
      echo '            <td width="25%" valign="top">' . "\n";

      $box = new box;
      echo $box->infoBox($heading, $contents);

      echo '            </td>' . "\n";
    }
?>
          </tr>
        </table></td>
      </tr>
<?php
  }

?>
    </table></td>
<!-- body_text_eof //-->
  </tr>
</table>
<!-- body_eof //-->
<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
<br>
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
