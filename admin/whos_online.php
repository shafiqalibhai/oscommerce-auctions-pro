<?php
/*
  $Id: whos_online.php,v 1.32 2003/06/29 22:50:52 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

  require('includes/application_top.php');
  $xx_mins_ago = (time() - MAX_WHOS_ONLINE_TIME);

  require(DIR_WS_CLASSES . 'currencies.php');
  $currencies = new currencies();

  $action = (isset($_GET['action']) ? $_GET['action'] : '');
  $info = (isset($_GET['info']) ? $_GET['info'] : '');
  switch($action) {
    case 'delete_all':
      tep_db_query("truncate table " . TABLE_WHOS_ONLINE . "");
      tep_redirect(tep_href_link(FILENAME_WHOS_ONLINE, tep_get_all_get_params(array('action')) ));
      break;
    default:
      break;
  }
// remove entries that have expired
  tep_db_query("delete from " . TABLE_WHOS_ONLINE . " where time_last_click < '" . $xx_mins_ago . "'");
?>
<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<script language="javascript" src="includes/general.js"></script>
</head>
<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" bgcolor="#FFFFFF">
<!-- header //-->
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<!-- header_eof //-->

<!-- body //-->
<table border="0" width="100%" cellspacing="2" cellpadding="2">
  <tr>
    <td width="<?php echo BOX_WIDTH; ?>" valign="top"><table border="0" width="<?php echo BOX_WIDTH; ?>" cellspacing="1" cellpadding="1" class="columnLeft">
<!-- left_navigation //-->
<?php require(DIR_WS_INCLUDES . 'column_left.php'); ?>
<!-- left_navigation_eof //-->
    </table></td>
<!-- body_text //-->
    <td width="100%" valign="top"><table border="0" width="100%" cellspacing="0" cellpadding="2">
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td class="pageHeading"><?php echo HEADING_TITLE; ?></td>
            <td align="right"><table border="0" width="100%" cellspacing="0" cellpadding="2">
              <tr>
                <td class="dataTableRowHighBorder" width="16">&nbsp;</td>
                <td class="smallText"><b>&nbsp;-&nbsp;Signifies Spider presense</b></td>
              </tr> 
              <tr>
                <td colspan="2"><?php echo tep_draw_separator('pixel_trans.gif', '100%', '1'); ?></td>
              </tr>
              <tr>
                <td class="dataTableRowGreenBorder" width="16">&nbsp;</td>
                <td class="smallText"><b>&nbsp;-&nbsp;Signifies Customer presense</b></td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top"><table border="0" width="100%" cellspacing="1" cellpadding="3">
              <tr class="dataTableHeadingRow">
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_CUSTOMER_ID; ?></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_FULL_NAME; ?></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_IP_ADDRESS; ?></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_HOST; ?></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_COOKIE; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_ENTRY_TIME; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_LAST_CLICK; ?></td>
                <td class="dataTableHeadingContent" align="center"><?php echo TABLE_HEADING_ONLINE; ?></td>
                <td class="dataTableHeadingContent"><?php echo TABLE_HEADING_LAST_PAGE_URL; ?>&nbsp;</td>
              </tr>
<?php
  $whos_online_query = tep_db_query("select customer_id, full_name, ip_address, time_entry, time_last_click, last_page_url, session_id, cookie_sent from " . TABLE_WHOS_ONLINE . " order by customer_id desc, cookie_sent desc, time_last_click desc");
  while ($whos_online = tep_db_fetch_array($whos_online_query)) {
    $time_online = (time() - $whos_online['time_last_click']);
/*
    if( (!isset($_GET['info']) || (isset($_GET['info']) && tep_not_null($_GET['info']) && ($_GET['info'] == $whos_online['session_id']))) && !isset($info)) {
      if( isset($_GET['info']) ) {
        $info = $_GET['info'];
      }
    }
*/
    $class= 'dataTableRow';
    if( $whos_online['customer_id'] > 0 ) {
      $class='dataTableRowGreen';
    } elseif($whos_online['full_name'] != 'Guest') {
      $class= 'dataTableRowHigh';
    }

    if( isset($info) && tep_not_null($info) && $whos_online['session_id'] == $info ) {
      echo '              <tr id="defaultSelected" class="dataTableRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)">' . "\n";
    } else {
      echo '              <tr class="' . $class . '" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="document.location.href=\'' . tep_href_link(FILENAME_WHOS_ONLINE, tep_get_all_get_params(array('info', 'action')) . 'info=' . $whos_online['session_id'], 'NONSSL') . '\'">' . "\n";
    }
?>
                <td class="dataTableContent" align="center"><?php echo $whos_online['customer_id']; ?></td>
                <td class="dataTableContent">
<?php
    if( $whos_online['customer_id'] > 0 ) {
      echo '<a href="' . tep_href_link(FILENAME_CUSTOMERS, 'cID=' . $whos_online['customer_id'] . '&action=edit') . '"><b>' . $whos_online['full_name'] . '</b></a>';
    } else {
      echo $whos_online['full_name'];
    }
?>
                </td>
                <td class="dataTableContent"><?php echo $whos_online['ip_address']; ?></td>
                <td class="dataTableContent">
<?php
    if( $action == 'host' ) {
      $result = gethostbyaddr($whos_online['ip_address']);
      if( $result == $whos_online['ip_address'] )
        echo 'Cannot Resolve';
      else
        echo $result;
    } else {
      echo 'N/A';
    }
?>
                </td>
                <td class="dataTableContent"><?php echo ($whos_online['cookie_sent'])?'Enabled':'Disabled'; ?></td>
                <td class="dataTableContent" align="center"><?php echo date('H:i:s', $whos_online['time_entry']); ?></td>
                <td class="dataTableContent" align="center"><?php echo date('H:i:s', $whos_online['time_last_click']); ?></td>
                <td class="dataTableContent" align="center"><?php echo gmdate('H:i:s', $time_online); ?></td>
                <td class="dataTableContent"><?php if (preg_match('/^(.*)' . tep_session_name() . '=[a-f,0-9]+[&]*(.*)/i', $whos_online['last_page_url'], $array)) { echo $array[1] . $array[2]; } else { echo $whos_online['last_page_url']; } ?>&nbsp;</td>
              </tr>
<?php
  }
?>
              <tr>
                <td colspan="9"><table border="0" width="100%" cellspacing="0" cellpadding="2">
                  <tr>
                    <td class="smallText"><?php echo sprintf(TEXT_NUMBER_OF_CUSTOMERS, tep_db_num_rows($whos_online_query)); ?></td>
                    <td class="smallText" align="right"><?php echo '<a href="' . tep_href_link(FILENAME_WHOS_ONLINE, tep_get_all_get_params(array('action')) . 'action=host' ) . '">' . tep_image_button('button_hosts.gif', 'Resolve Host Information - May take some time') . '&nbsp;<a href="' . tep_href_link(FILENAME_WHOS_ONLINE, tep_get_all_get_params(array('action')) . 'action=delete_all' ) . '">' . tep_image_button('button_delete_all.gif', 'Flush Whos Online Information') . '</a>'; ?></td>
                  </tr>
                </table></td>
              </tr>
            </table></td>
<?php
  $heading = array();
  $contents = array();
  $session_data = '';
  if (isset($info)) {
    $heading[] = array('text' => '<b>' . TABLE_HEADING_SHOPPING_CART . '</b>');

    if (STORE_SESSIONS == 'mysql') {
      $session_query = tep_db_query("select value from " . TABLE_SESSIONS . " WHERE sesskey = '" . tep_db_input(tep_db_prepare_input($info)) . "'");
      if( $session_array = tep_db_fetch_array($session_query) ) {
        $session_data = trim($session_array['value']);
      }
    } else {
      if ( (file_exists(tep_session_save_path() . '/sess_' . $info)) && (filesize(tep_session_save_path() . '/sess_' . $info) > 0) ) {
        $session_array = file(tep_session_save_path() . '/sess_' . $info);
        $session_data = trim(implode('', $session_array));
      }
    }

    if( $length = strlen($session_data) ) {
      $start_id = strpos($session_data, 'customer_id|s');
      $start_cart = strpos($session_data, 'cart|O');
      $start_currency = strpos($session_data, 'currency|s');
      $start_country = strpos($session_data, 'customer_country_id|s');
      $start_zone = strpos($session_data, 'customer_zone_id|s');

      for ($i=$start_cart; $i<$length; $i++) {
        if ($session_data[$i] == '{') {
          if (isset($tag)) {
            $tag++;
          } else {
            $tag = 1;
          }
        } elseif ($session_data[$i] == '}') {
          $tag--;
        } elseif ( (isset($tag)) && ($tag < 1) ) {
          break;
        }
      }

      $session_data_id = substr($session_data, $start_id, (strpos($session_data, ';', $start_id) - $start_id + 1));
      $session_data_cart = substr($session_data, $start_cart, $i);
      $session_data_currency = substr($session_data, $start_currency, (strpos($session_data, ';', $start_currency) - $start_currency + 1));
      $session_data_country = substr($session_data, $start_country, (strpos($session_data, ';', $start_country) - $start_country + 1));
      $session_data_zone = substr($session_data, $start_zone, (strpos($session_data, ';', $start_zone) - $start_zone + 1));

      session_decode($session_data_id);
      session_decode($session_data_currency);
      session_decode($session_data_country);
      session_decode($session_data_zone);
      session_decode($session_data_cart);

      if(isset($cart) && is_object($cart)) {
        $products = $cart->get_products();
        for ($i = 0, $n = sizeof($products); $i < $n; $i++) {
          $contents[] = array('text' => $products[$i]['quantity'] . ' x ' . $products[$i]['name']);
        }

        if (sizeof($products) > 0) {
          $contents[] = array('text' => tep_draw_separator('pixel_black.gif', '100%', '1'));
          $contents[] = array('align' => 'right', 'text'  => TEXT_SHOPPING_CART_SUBTOTAL . ' ' . $currencies->format($cart->show_total(), true, $currency));
        } else {
          $contents[] = array('text' => '&nbsp;');
        }
      }
    }
  }

  if ( (tep_not_null($heading)) && (tep_not_null($contents)) ) {
    echo '            <td width="25%" valign="top">' . "\n";

    $box = new box;
    echo $box->infoBox($heading, $contents);

    echo '            </td>' . "\n";
  }
?>
          </tr>
        </table></td>
      </tr>
    </table></td>
<!-- body_text_eof //-->
  </tr>
</table>
<!-- body_eof //-->

<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
<br>
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); ?>
