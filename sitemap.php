<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2008 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
//----------------------------------------------------------------------------
// Catalog: Sitemap file
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
  require('includes/application_top.php');
  $navigation->remove_current_page();
  require(DIR_WS_LANGUAGES . $language . '/' . basename($PHP_SELF));
  $breadcrumb->add(NAVBAR_TITLE, tep_href_link(basename($PHP_SELF)));
?>
<?php require('includes/objects/html_start_sub1.php'); ?>
<?php require('includes/objects/html_start_sub2.php'); ?>
<?php require('includes/objects/html_body_header.php'); ?>
      <div class="sixties floater">
        <div><h2><?php echo strtoupper(TEXT_INFO_CUSTOMER_SERVICE); ?></h2></div>
<?php
  $total_items = array(
                       array('link' => tep_href_link(FILENAME_SHOPPING_CART), 'name' => TEXT_INFO_SHOPPING_CART ), 
                       array('link' => tep_href_link(FILENAME_CONTACT_US, '', 'SSL'), 'name' => TEXT_INFO_CONTACT_US ),
                       array('link' => tep_href_link(FILENAME_ADVANCED_SEARCH, '', 'SSL'), 'name' => TEXT_INFO_SEARCH ),
                       array('link' => tep_href_link(FILENAME_REVIEWS), 'name' => strtoupper(TEXT_INFO_REVIEWS) ),
                       array('link' => tep_href_link( tep_session_is_registered('customer_id')?FILENAME_ACCOUNT:FILENAME_CREATE_ACCOUNT, '', 'SSL'), 'name' => TEXT_INFO_ACCOUNT ),                    
                     );

  if(  tep_session_is_registered('customer_id') ) {
    $total_items[] = array('link' => tep_href_link(FILENAME_ACCOUNT_HISTORY, '', 'SSL'), 'name' => TEXT_INFO_ORDERS );
    $total_items[] = array('link' => tep_href_link(FILENAME_ACCOUNT_NOTIFICATIONS, '', 'SSL'), 'name' => TEXT_INFO_NOTIFICATIONS );
    $total_items[] = array('link' => tep_href_link(FILENAME_WISHLIST), 'name' => TEXT_INFO_WISHLIST );
  }
  for( $i=0, $j=count($total_items); $i<$j; $i++ ) {
?>
        <div class="halfer floater"><?php echo '<a href="' . $total_items[$i]['link'] . '">' . strtoupper($total_items[$i]['name']) . '</a>'; ?></div>
<?php
  }
?>
        <div class="vpad cleaner"></div>
        <div><h2><?php echo strtoupper(STORE_NAME . ' ' . TEXT_INFO_COMPANY); ?></h2></div>
<?php
  $total_items = array();
  $gtext_query_raw = "select gtext_id, gtext_title from " . TABLE_GTEXT . " order by gtext_title";
  tep_query_to_array($gtext_query_raw, $total_items);

  for( $i=0, $j=count($total_items); $i<$j; $i++ ) {
?>
        <div class="halfer floater"><?php echo '<a href="' . tep_href_link(FILENAME_GENERIC_PAGES, 'gtext_id=' . $total_items[$i]['gtext_id']) . '">' . strtoupper($total_items[$i]['gtext_title']) . '</a>'; ?></div>
<?php
  }
?>
        <div class="vpad cleaner"></div>
        <div><h2><?php echo strtoupper(STORE_NAME . ' ' . TEXT_INFO_CATEGORIES); ?></h2></div>
<?php
  $total_items = array();
  $categories_query_raw = "select c.categories_id, cd.categories_name from " . TABLE_CATEGORIES . " c left join " . TABLE_CATEGORIES_DESCRIPTION . " cd on (c.categories_id=cd.categories_id) where cd.language_id = '" . (int)$languages_id . "' order by sort_order, cd.categories_name";
  tep_query_to_array($categories_query_raw, $total_items);

  for( $i=0, $j=count($total_items); $i<$j; $i++ ) {
    $tmp_array = explode(' ' , $total_items[$i]['categories_name']);
    if( is_array($tmp_array) && count($tmp_array) && strtoupper($tmp_array[count($tmp_array)-1]) == strtoupper(TEXT_PRODUCTS_POSTFIX) ) {
      unset($tmp_array[count($tmp_array)-1]);
      $total_items[$i]['categories_name'] = implode(' ', $tmp_array);
    }
?>
        <div class="halfer floater"><?php echo '<a href="' . tep_href_link(FILENAME_DEFAULT, 'cPath=' . tep_get_category_path($total_items[$i]['categories_id']) ) . '" title="' . $total_items[$i]['categories_name'] . ' ' . TEXT_PRODUCTS_POSTFIX . '">' . strtoupper($total_items[$i]['categories_name']) . '</a>'; ?></div>

<?php
  }
?>
      </div>
      <div class="forties floater">
<?php
  if( isset($g_filters) && is_object($g_filters) ) {
    $filter = $g_filters->get_filters_array();
?>
        <div><h2><?php echo strtoupper(TEXT_OUR_PRODUCTS . ' ' . TEXT_INFO_TYPES); ?></h2></div>
<?php
    foreach($filter as $key => $filter_array) {
      foreach($filter_array['select'] as $index => $select) {
?>
        <div><?php echo '<a href="' . tep_href_link(FILENAME_SHOP_BY_FILTER, $filter_array['parameter'] . '=' . $index) . '" title="' . $select . ' ' . TEXT_PRODUCTS_POSTFIX . '">' . strtoupper($select) . '</a>'; ?></div>
<?php
      }
    }
  }
?>
        <div class="vpad"></div>
        <div><h2><?php echo strtoupper(STORE_NAME . ' ' . TEXT_INFO_PRICES); ?></h2></div>
<?php
  $total_items = array();
  $ranges_query_raw = "select numeric_ranges_id, numeric_ranges_desc from " . TABLE_NUMERIC_RANGES . " order by numeric_ranges_min, numeric_ranges_id";
  tep_query_to_array($ranges_query_raw, $total_items);
  for( $i=0, $j=count($total_items); $i<$j; $i++ ) {
?>
        <div><?php echo '<a href="' . tep_href_link(FILENAME_SHOP_BY_PRICE, 'range_id=' . $total_items[$i]['numeric_ranges_id'] ) . '" title="' . TEXT_PRODUCTS_POSTFIX . ' ' . $total_items[$i]['numeric_ranges_desc'] . '">' . strtoupper($total_items[$i]['numeric_ranges_desc']) . '</a>'; ?></div>
<?php
  }
?>
        <div class="vpad"></div>
        <div><h2><?php echo strtoupper(STORE_NAME . ' ' . TEXT_INFO_PRODUCTS); ?></h2></div>
<?php
  $total_items = array();
  $products_query_raw = "select p.products_id, pd.products_name from " . TABLE_PRODUCTS . " p left join " . TABLE_PRODUCTS_DESCRIPTION . " pd on (pd.products_id=p.products_id) where pd.language_id = '" . (int)$languages_id . "' and p.products_display='1' order by p.products_status desc, p.products_ordered limit 14";
  tep_query_to_array($products_query_raw, $total_items);

  for( $i=0, $j=count($total_items); $i<$j; $i++ ) {
    $tmp_array = explode(' ' , $total_items[$i]['products_name']);
    if( is_array($tmp_array) && count($tmp_array) ) {
      for( $i2=0, $j2=count($tmp_array); $i2 < $j2; $i2++) {
        if( strtoupper($tmp_array[$i2]) == strtoupper(TEXT_PRODUCTS_POSTFIX)) {
          unset($tmp_array[$i2]);
          $total_items[$i]['products_name'] = implode(' ', $tmp_array);
          break;
        }
      }
    }
?>
        <div><?php echo '<a href="' . tep_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . $total_items[$i]['products_id'] ) . '" title="' . $total_items[$i]['products_name'] . ' ' . TEXT_PRODUCTS_POSTFIX . '">' . strtoupper($total_items[$i]['products_name']) . '</a>'; ?></div>
<?php
  }
?>

      </div>
      <div class="cleaner vpad"></div>
      <div class="buttonsRow vpad tspacer">
        <div class="floatend rspacer">

<?php
    $back = sizeof($navigation->path)-1;
    if (isset($navigation->path[$back])) {
      echo '<a href="' . tep_href_link($navigation->path[$back]['page'], tep_array_to_string($navigation->path[$back]['get'], array('action')), $navigation->path[$back]['mode'], true, false) . '">' . tep_image_button('button_continue_shopping.gif', IMAGE_BUTTON_CONTINUE_SHOPPING) . '</a>';
    } else {
      echo '<a href="' . tep_href_link() . '">' . tep_image_button('button_continue.gif', IMAGE_BUTTON_CONTINUE) . '</a>';
    }
?>
        </div>
      </div>
<?php require('includes/objects/html_end.php'); ?>
