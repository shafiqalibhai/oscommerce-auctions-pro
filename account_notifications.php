<?php
/*
  $Id: account_notifications.php,v 1.2 2003/05/22 14:24:54 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

//----------------------------------------------------------------------------
// Copyright (c) 2007-2010 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Catalog: Account Notifications page
//----------------------------------------------------------------------------
// I-Metrics Layer
//----------------------------------------------------------------------------
// Modifications:
// - 07/05/2007: PHP5 Register Globals and Long Arrays Off support added
// - 07/08/2007: PHP5 Long Arrays Off support added
// - 07/12/2007: Moved HTML Header/Footer to a common section
// - 08/31/2007: HTML Body Common Sections Added
// - 11/17/2009: Converted Tables to CSS
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/

  require('includes/application_top.php');

  if (!tep_session_is_registered('customer_id')) {
    $navigation->set_snapshot();
    tep_redirect(tep_href_link(FILENAME_LOGIN, '', 'SSL'));
  }

// needs to be included earlier to set the success message in the messageStack
  require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_ACCOUNT_NOTIFICATIONS);

  $action = (isset($_GET['action']) ? $_GET['action'] : '');

  $global_query = tep_db_query("select global_product_notifications from " . TABLE_CUSTOMERS_INFO . " where customers_info_id = '" . (int)$customer_id . "'");
  $global = tep_db_fetch_array($global_query);
  $product_global = $global['global_product_notifications'];

  switch($action) {
    case 'process':
      if( isset($_POST['product_global']) ) {
        $product_global = (int)$_POST['product_global'];
      } else {
        $product_global = 0;
      }
      tep_db_query("update " . TABLE_CUSTOMERS_INFO . " set global_product_notifications = '" . (int)$product_global . "' where customers_info_id = '" . (int)$customer_id . "'");
      if( !$product_global && isset($_POST['products']) && is_array($_POST['products']) ) {
        tep_db_query("delete from " . TABLE_PRODUCTS_NOTIFICATIONS . " where customers_id = '" . (int)$customer_id . "'");
        foreach($_POST['products'] as $key => $value ) {
          tep_db_query("insert into " . TABLE_PRODUCTS_NOTIFICATIONS . " (products_id, customers_id) values ('" . (int)$value . "', '" . (int)$customer_id . "')");
        }
      }
      $messageStack->add_session(tep_get_script_name(), SUCCESS_NOTIFICATIONS_UPDATED, 'success');
      tep_redirect(tep_href_link($g_script, '', 'SSL'));
      break;
    default:
      break;
  }

  $breadcrumb->add(NAVBAR_TITLE_1, tep_href_link(FILENAME_ACCOUNT, '', 'SSL'));
  $breadcrumb->add(NAVBAR_TITLE_2, tep_href_link(FILENAME_ACCOUNT_NOTIFICATIONS, '', 'SSL'));
?>
<?php require('includes/objects/html_start_sub1.php'); ?>
<?php require('includes/objects/html_start_sub2.php'); ?>
<?php require('includes/objects/html_body_header.php'); ?>

      <div class="bounder"><?php echo tep_draw_form('account_notifications', tep_href_link($g_script,  'action=process', 'SSL'), 'post'); ?><fieldset><legend><?php echo TEXT_INFO_FORM; ?></legend>
        <div><?php echo MY_NOTIFICATIONS_DESCRIPTION; ?></div>
        <div class="heavy vpad"><span class="floater rspacer"><?php echo tep_draw_checkbox_field('product_global', 1, '', 'id="products"'); ?></span><label for="products"><?php echo GLOBAL_NOTIFICATIONS_TITLE; ?></label></div>
        <div class="tspacer"><h2><?php echo NOTIFICATIONS_TITLE; ?></h2></div>
<?php
  $check_query = tep_db_query("select products_id from " . TABLE_PRODUCTS_NOTIFICATIONS . " where customers_id = '" . (int)$customer_id . "'");
  if( tep_db_num_rows($check_query) ) {
?>
        <div class="vpad"><?php echo NOTIFICATIONS_DESCRIPTION; ?></div>

<?php
    while($check_array = tep_db_fetch_array($check_query) ) {
      $products_name = tep_get_products_name($check_array['products_id']);
?>
        <div class="heavy vpad"><span class="floater rspacer"><?php echo tep_draw_checkbox_field('products[' . $check_array['products_id'] . ']', 1, true, 'id="products' . $check_array['products_id'] . '"'); ?></span><label for="products<?php echo $check_array['products_id']; ?>"><?php echo $products_name; ?></label></div>
<?php
    }
  } else {
?>
        <div><?php echo NOTIFICATIONS_NON_EXISTING; ?></div>
<?php
  }
?>
        <div class="buttonsRow vpad tspacer">
          <div class="floater lspacer"><?php echo '<a href="#" class="mbutton2 bsubmit">' . IMAGE_BUTTON_UPDATE . '</a>'; ?></div>
          <div class="floatend rspacer"><?php echo '<a href="' . tep_href_link(FILENAME_ACCOUNT, '', 'SSL') . '" class="mbutton3">' . IMAGE_BUTTON_ACCOUNT . '</a>'; ?></div>
        </div>
      </fieldset></form></div>
<?php require('includes/objects/html_end.php'); ?>
