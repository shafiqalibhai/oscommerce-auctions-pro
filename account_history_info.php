<?php
/*
  $Id: account_history_info.php,v 1.100 2003/06/09 23:03:52 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

//----------------------------------------------------------------------------
// Copyright (c) 2007-2010 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Catalog: Account History Information page
//----------------------------------------------------------------------------
// I-Metrics Layer
//----------------------------------------------------------------------------
// Modifications:
// - 07/05/2007: PHP5 Register Globals and Long Arrays Off support added
// - 07/08/2007: PHP5 Long Arrays Off support added
// - 07/12/2007: Moved HTML Header/Footer to a common section
// - 08/31/2007: HTML Body Common Sections Added
// - 11/17/2009: Converted Tables to CSS
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/

  require('includes/application_top.php');

  if (!tep_session_is_registered('customer_id')) {
    $navigation->set_snapshot();
    tep_redirect(tep_href_link(FILENAME_LOGIN, '', 'SSL'));
  }

  if (!isset($_GET['order_id']) || (isset($_GET['order_id']) && !is_numeric($_GET['order_id']))) {
    tep_redirect(tep_href_link(FILENAME_ACCOUNT_HISTORY, '', 'SSL'));
  }
  
  $customer_info_query = tep_db_query("select customers_id, orders_status from " . TABLE_ORDERS . " where orders_id = '". (int)$_GET['order_id'] . "'");
  $customer_info = tep_db_fetch_array($customer_info_query);
  if ($customer_info['customers_id'] != $customer_id) {
    tep_redirect(tep_href_link(FILENAME_ACCOUNT_HISTORY, '', 'SSL'));
  }

  require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_ACCOUNT_HISTORY_INFO);

  $breadcrumb->add(NAVBAR_TITLE_1, tep_href_link(FILENAME_ACCOUNT, '', 'SSL'));
  $breadcrumb->add(NAVBAR_TITLE_2, tep_href_link(FILENAME_ACCOUNT_HISTORY, '', 'SSL'));
  $breadcrumb->add(sprintf(NAVBAR_TITLE_3, $_GET['order_id']), tep_href_link(FILENAME_ACCOUNT_HISTORY_INFO, 'order_id=' . $_GET['order_id'], 'SSL'));

  require(DIR_WS_CLASSES . 'order.php');
  $order = new order($_GET['order_id']);
?>
<?php require('includes/objects/html_start_sub1.php'); ?>
<?php require('includes/objects/html_start_sub2.php'); ?>
<?php require('includes/objects/html_body_header.php'); ?>
      <div class="bounder">
        <div class="floater rspacer"><h2><?php echo sprintf(HEADING_ORDER_NUMBER, $_GET['order_id']); ?></h2></div>
        <div class="floater heavy"><?php echo $order->info['orders_status']; ?></div>
        <div class="floatend heavy"><?php echo HEADING_ORDER_DATE . ' ' . tep_date_long($order->info['date_purchased']); ?></div>
      </div>
      <table class="tabledata">
        <tr class="buttonsRow">
          <th><?php echo HEADING_PRODUCTS; ?></th>
<?php
  if (sizeof($order->info['tax_groups']) > 1) {
?>
          <th class="ralign"><?php echo HEADING_TAX; ?></th>
<?php
  }
?>
          <th class="ralign"><?php echo HEADING_TOTAL; ?></th>
        </tr>
        <tr>
<?php
  for ($i=0, $n=sizeof($order->products); $i<$n; $i++) {
    $attributes_string = '';
    if ( (isset($order->products[$i]['attributes'])) && (sizeof($order->products[$i]['attributes']) > 0) ) {
      for ($j=0, $n2=sizeof($order->products[$i]['attributes']); $j<$n2; $j++) {
        $attributes_string .= '<br /><i> - ' . $order->products[$i]['attributes'][$j]['option'] . ': ' . $order->products[$i]['attributes'][$j]['value'] . '</i>';
      }
    }
//-MS- Group Fields Added
    $groups_string = '';
    if( (isset($order->products[$i]['groups'])) && (sizeof($order->products[$i]['groups']) > 0) ) {
      for ($j=0, $n2=sizeof($order->products[$i]['groups']); $j<$n2; $j++) {
        $groups_string .= '<br /><i> - ' . $order->products[$i]['groups'][$j]['name'] . ': ' . $order->products[$i]['groups'][$j]['value'] . '</i>';
      }
    }
//-MS- Group Fields Added EOM
?>
          <td><?php echo $order->products[$i]['qty'] . 'x' . $order->products[$i]['name'] . $attributes_string . $groups_string; ?></td>
<?php
    if (sizeof($order->info['tax_groups']) > 1) {
?>
          <td class="ralign"><?php echo tep_display_tax_value($order->products[$i]['tax']) . '%'; ?></td>
<?php
    }
?>
          <td class="ralign"><?php echo $currencies->format(tep_add_tax($order->products[$i]['final_price'], $order->products[$i]['tax']) * $order->products[$i]['qty'], true, $order->info['currency'], $order->info['currency_value']); ?></td>
        </tr>
<?php
  }
?>
      </table>
      <div class="bounder">
<?php 
  for ($i=0, $n=sizeof($order->totals); $i<$n; $i++) {
?>
        <div class="cpad floatend">
          <div class="floater rspacer"><?php echo $order->totals[$i]['title']; ?></div>
          <div class="floater"><?php echo $order->totals[$i]['text']; ?></div>
        </div>
        <div class="cleaner"></div>
<?php
  }
?>
      </div>
      <div class="floater halfer bmargin">
        <div><h2><?php echo HEADING_BILLING_INFORMATION; ?></h2></div>
        <div class="heavy"><?php echo HEADING_BILLING_ADDRESS; ?></div>
        <div><?php echo tep_address_format($order->billing['format_id'], $order->billing, 1, ' ', '<br />'); ?></div>
        <div class="heavy"><?php echo HEADING_PAYMENT_METHOD; ?></div>
        <div><?php echo $order->info['payment_method']; ?></div>
      </div>
<?php
  if(tep_not_null($order->info['shipping_method'])) {
?>
      <div class="floater halfer bmargin">
        <div><h2><?php echo HEADING_DELIVERY_ADDRESS; ?></h2></div>
        <div class="heavy"><?php echo HEADING_SHIPPING_METHOD; ?></div>
        <div><?php echo $order->info['shipping_method']; ?></div>
        <div class="heavy"><?php echo HEADING_DELIVERY_ADDRESS; ?></div>
        <div><?php echo tep_address_format($order->delivery['format_id'], $order->delivery, 1, ' ', '<br />'); ?></div>
      </div>
<?php
  }
?>
      <div class="cleaner"><h2><?php echo HEADING_ORDER_HISTORY; ?></h2></div>
<?php
  $statuses_query = tep_db_query("select os.orders_status_name, osh.date_added, osh.comments from " . TABLE_ORDERS_STATUS . " os, " . TABLE_ORDERS_STATUS_HISTORY . " osh where osh.orders_id = '" . (int)$_GET['order_id'] . "' and osh.orders_status_id = os.orders_status_id and os.language_id = '" . (int)$languages_id . "' order by osh.date_added");
  while ($statuses = tep_db_fetch_array($statuses_query)) {
?>
      <div class="bounder">
        <div class="floater twenties"><?php echo tep_date_short($statuses['date_added']); ?></div>
        <div class="floater heavy tener"><?php echo $statuses['orders_status_name']; ?></div>
        <div class="floater seventies"><?php echo (empty($statuses['comments']) ? '&nbsp;' : nl2br(tep_output_string_protected($statuses['comments']))); ?></div>
      </div>
<?php
  }
  if (DOWNLOAD_ENABLED == 'true') include(DIR_WS_MODULES . 'downloads.php');
?>
      <div class="buttonsRow vpad vspacer">
        <div class="lspacer"><?php echo '<a href="' . tep_href_link(FILENAME_ACCOUNT_HISTORY, 'order_id=' . (int)$_GET['order_id'], 'SSL') . '" class="mbutton">' . IMAGE_BUTTON_BACK . '</a>'; ?></div>
      </div>
<?php require('includes/objects/html_end.php'); ?>
