<?php
/*
  $Id: shopping_cart.php,v 1.73 2003/06/09 23:03:56 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

//----------------------------------------------------------------------------
// Copyright (c) 2007 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Catalog: Shopping Cart page
// I-Metrics Variant
//----------------------------------------------------------------------------
// Modifications:
// - 07/05/2007: PHP5 Register Globals and Long Arrays Off support added
// - 07/08/2007: PHP5 Long Arrays Off support added
// - 07/12/2007: Moved HTML Header/Footer to a common section
// - 08/31/2007: HTML Body Common Sections Added
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/

  require("includes/application_top.php");
  $navigation->remove_current_page();
  require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_SHOPPING_CART);

  $breadcrumb->add(NAVBAR_TITLE, tep_href_link(FILENAME_SHOPPING_CART));
?>
<?php require('includes/objects/html_start_sub1.php'); ?>
<?php require('includes/objects/html_start_sub2.php'); ?>
<?php
  $heading_row = true;
  require('includes/objects/html_body_header.php');
?>
        <div><h1><?php echo HEADING_TITLE; ?></h1></div>
<?php
  if ($cart->count_contents() > 0) {
?>
        <div class="bounder"><?php echo tep_draw_form('cart_quantity', tep_href_link(FILENAME_SHOPPING_CART, 'action=update_product')); ?><table class="tabledata">
          <tr class="buttonsRow">
            <th class="tener calign"><?php echo TABLE_HEADING_REMOVE; ?></th>
            <th class="sixties"><?php echo TABLE_HEADING_PRODUCTS; ?></th>
            <th class="tener calign"></th>
            <th class="twenties ralign rspacer">
              <div class="rspacer"><?php echo TABLE_HEADING_TOTAL; ?></div>
            </th>
          </tr>

<?php
    $any_out_of_stock = 0;
    $products = $cart->get_products();

    for ($i=0, $n=sizeof($products); $i<$n; $i++) {
// Push all attributes information in an array
      if (isset($products[$i]['attributes']) && is_array($products[$i]['attributes'])) {
        while (list($option, $value) = each($products[$i]['attributes'])) {
          echo tep_draw_hidden_field('id[' . $products[$i]['id'] . '][' . $option . ']', $value);
          $attributes = tep_db_query("select popt.products_options_name, poval.products_options_values_name, pa.options_values_price, pa.price_prefix
                                      from " . TABLE_PRODUCTS_OPTIONS . " popt, " . TABLE_PRODUCTS_OPTIONS_VALUES . " poval, " . TABLE_PRODUCTS_ATTRIBUTES . " pa
                                      where pa.products_id = '" . (int)$products[$i]['id'] . "'
                                       and pa.options_id = '" . (int)$option . "'
                                       and pa.options_id = popt.products_options_id
                                       and pa.options_values_id = '" . (int)$value . "'
                                       and pa.options_values_id = poval.products_options_values_id
                                       and popt.language_id = '" . (int)$languages_id . "'
                                       and poval.language_id = '" . (int)$languages_id . "'");
          $attributes_values = tep_db_fetch_array($attributes);

          $products[$i][$option]['products_options_name'] = $attributes_values['products_options_name'];
          $products[$i][$option]['options_values_id'] = $value;
          $products[$i][$option]['products_options_values_name'] = $attributes_values['products_options_values_name'];
          $products[$i][$option]['options_values_price'] = $attributes_values['options_values_price'];
          $products[$i][$option]['price_prefix'] = $attributes_values['price_prefix'];
        }
      }
    }
    for ($i=0, $j=count($products); $i<$j; $i++) {
      $auction_flag = isset($products[$i]['auctions_history_id'])?true:false;
?>
          <tr>
            <td class="calign">
<?php
      if( !$auction_flag ) {
        echo tep_draw_checkbox_field('cart_delete[]', $products[$i]['id']); 
      }
?>
            </td>
            <td>
<?php
      $products_name = $products[$i]['name'];

      if (isset($products[$i]['attributes']) && is_array($products[$i]['attributes'])) {
        reset($products[$i]['attributes']);
        while (list($option, $value) = each($products[$i]['attributes'])) {
          $products_name .= '<br /><small><i> - ' . $products[$i][$option]['products_options_name'] . ' ' . $products[$i][$option]['products_options_values_name'] . '</i></small>';
        }
      }

//-MS- Group Fields Added
      if (isset($products[$i]['groups']) && is_array($products[$i]['groups'])) {
        $products_name .= '<br />';
        $tmp_array = $g_group_fields->get_names_from_group($products[$i]['groups']);
        foreach($tmp_array as $key => $value) {
          $products_name .= '<small><i>' . $products[$i]['groups'][$key] . '</i></small>&nbsp;';
        }
        trim($products_name);
      }
//-MS- Group Fields Added EOM

      $image = $products[$i]['image'];
      if( !tep_not_null($image) || !is_file(DIR_WS_IMAGES . $image) || filesize(DIR_WS_IMAGES . $image) <= 0 ) {
        $image_link = tep_image(DIR_WS_IMAGES . IMAGE_NOT_AVAILABLE, $products[$i]['name'], SMALL_IMAGE_WIDTH, SMALL_IMAGE_HEIGHT);
      } else {
        $image_link = tep_image(DIR_WS_IMAGES . $image, $products[$i]['name'], SMALL_IMAGE_WIDTH, SMALL_IMAGE_HEIGHT);
      }
?>
                <div>
<?php
      if( !$auction_flag ) {
        echo '<a href="' . tep_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . (int)$products[$i]['id']) . '">' . $image_link . '</a>';
      } else {
        echo $image_link;
      }
?>
                </div>
                <div><?php echo $products_name; ?></div>
<?php
/*
      echo '<a href="' . tep_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . (int)$products[$i]['id']) . '">' . $image . '</a><br />';
      echo $products_name;
      echo tep_draw_hidden_field('cart_quantity[]',1) . tep_draw_hidden_field('products_id[]', $products[$i]['id']);
*/
?>
            </td>
            <td class="calign">

            </td>
            <td class="ralign rspacer"><?php echo $currencies->display_price($products[$i]['final_price'], tep_get_tax_rate($products[$i]['tax_class_id']), $products[$i]['quantity']); ?></td>
          </tr>
<?php
    }
?>
        </table>
        <div class="heavy cleaner vpad ralign rspacer"><?php echo SUB_TITLE_SUB_TOTAL; ?> <?php echo $currencies->format($cart->show_total()); ?></div>
<?php
    if ($any_out_of_stock == 1) {
      if (STOCK_ALLOW_CHECKOUT == 'true') {
?>
        <div class="stockWarning calign"><?php echo OUT_OF_STOCK_CAN_CHECKOUT; ?></div>
<?php
      } else {
?>
        <div class="stockWarning calign"><?php echo OUT_OF_STOCK_CANT_CHECKOUT; ?></div>
<?php
      }
    } 
    //bof check total order multiple     
    $tellme = cart_qty_check();    
    if ( $tellme ) {
    ?>    <tr> 
         <td class="stockWarning" align="center"><br>The products in the following categorie(s) must be ordered in quantities of <?php echo ORDER_QUANTITY_MULTIPLE; ?>: <br>
         <?php 
          echo $tellme;
          ?>    
            </td>   
             </tr>
         <?php  
           }//eof
?>        
        <div class="buttonsRow vpad">
          <div class="floater quarter">
            <div class="lspacer"><?php echo '<a href="#" class="mbutton3 bsubmit">' . IMAGE_BUTTON_UPDATE_CART . '</a>'; ?></div>
          </div>
<?php 
    $back = count($navigation->path)-1;
    if (isset($navigation->path[$back])) {
?>
          <div class="floater halfer calign"><?php echo '<a href="' . tep_href_link($navigation->path[$back]['page'], tep_array_to_string($navigation->path[$back]['get'], array('action')), $navigation->path[$back]['mode']) . '" class="mbutton">' . IMAGE_BUTTON_CONTINUE_SHOPPING . '</a>'; ?></div>
<?php
    }
?>
          <div class="floater quarter ralign">
            <div class="rspacer"><?php echo '<a href="' . tep_href_link(FILENAME_CHECKOUT_SHIPPING, '', 'SSL') . '" class="mbutton2">' . IMAGE_BUTTON_CHECKOUT . '</a>'; ?></div>
          </div>
        </div>
        </form></div>
<?php
  } else {
?>
        <div class="vpad"><?php echo TEXT_CART_EMPTY; ?></div>
        <div class="buttonsRow vpad ralign">
          <div class="rspacer"><?php echo '<a href="' . tep_href_link() . '" class="mbutton">' . IMAGE_BUTTON_CONTINUE . '</a>'; ?></div>
        </div>
<?php
  }
?>
<?php require('includes/objects/html_end.php'); ?>
