<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
  
  Version 1.1 Edit By NEXUS
*/
	
	
	
  require('includes/application_top.php');
  $from_forum = (int)$_REQUEST['from_forum'];

  if (!tep_session_is_registered('customer_id')) {
    $navigation->set_snapshot();
    tep_redirect(tep_href_link(FILENAME_LOGIN, 'from_forum='.$from_forum, 'SSL'));
  }

  require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_FORUM_USERNAME);
  // Check that the customer can change their username (only once)
  // Further username changes can be done by phpBB admins
  $succesful_redirect = ($from_forum == 1) ? DIR_WS_FORUMS : FILENAME_DEFAULT;
  // Get group_id for osCommerce first time phpBB users
  // This comes from the initial script to create new phpBB users from osCommerce, or from create_account.php
  $group_name = 'osCommerce';
  $group_desc = 'osCommerce users who have not selected their usernames.';
  $new_phpBB_group_query = tep_db_query("SELECT group_id FROM ".TABLE_PHPBB_GROUPS." WHERE group_type = '2' AND group_name= '".tep_db_input($group_name)."' AND group_desc = '".tep_db_input($group_desc)."'");
  $new_phpBB_group = tep_db_fetch_array($new_phpBB_group_query);
  $new_group_id = $new_phpBB_group['group_id'];
  
  // Get user email
  $account_query = tep_db_query("select customers_email_address from " . TABLE_CUSTOMERS . " where customers_id = '" . (int)$customer_id . "'");
  $account = tep_db_fetch_array($account_query);
  
  // Check if user qualifies for the username change
  $phpBB_usergroup_query = tep_db_query("
    SELECT u.username, ug.user_id, ug.group_id
    FROM " . TABLE_PHPBB_USERS . " u, " . TABLE_PHPBB_USER_GROUPS . " ug 
    WHERE u.user_id = ug.user_id
        AND u.user_email = '" . $account['customers_email_address'] . "'");
  $user_group_ids = array();
  while ($phpBB_usergroup_array = tep_db_fetch_array($phpBB_usergroup_query)) {
    $user_group_ids[] = $phpBB_usergroup_array['group_id'];
    $phpBB_user_id    = $phpBB_usergroup_array['user_id'];
    $phpBB_username   = $phpBB_usergroup_array['username'];
  }
  
  // If not in custom osCommerce group, this user has already changed their username
  if (!in_array($new_group_id, $user_group_ids)) {
    tep_redirect(tep_href_link($succesful_redirect, '', 'SSL'));
  }
  $error = false;
  if (isset($HTTP_POST_VARS['action']) && ($HTTP_POST_VARS['action'] == 'process') && isset($HTTP_POST_VARS['formid']) && ($HTTP_POST_VARS['formid'] == $sessiontoken)) {
    // Check that username does not already exist, *except* if coincidentially it belongs to this user (first name + last name initial + number)
    $new_username = tep_db_prepare_input($HTTP_POST_VARS['new_username']);
    $new_username = preg_replace("/[^A-Za-z0-9 ]/", "", $new_username); // To make things easier on us, only allow letters, numbers and spaces 
    
    define('IN_PHPBB', true);
    $current_dir = getcwd();
    chdir(DIR_WS_FORUMS);
    $phpbb_root_path = './';
    $phpEx = 'php';
    include_once($phpbb_root_path . 'common.'.$phpEx);
    $new_username = $db->sql_escape(utf8_clean_string($new_username));
    $new_username_clean = $db->sql_escape(utf8_clean_string($new_username));
    chdir($current_dir);
    
    if (empty($new_username)) {
        $error = true;
        $messageStack->add('forum_username', TEXT_EMPTY_USERNAME);
    } else if ($new_username != $phpBB_username) {
        // Check that the username does not exist already
        $user_exists_query = tep_db_query("SELECT username, user_id FROM " . TABLE_PHPBB_USERS . " WHERE user_id != '" . (int)$phpBB_user_id . "'");
        $existing_usernames = array();
        while ($user_exists_array = tep_db_fetch_array($user_exists_query)) {
            $existing_usernames[] = $user_exists_array['username'];
        }
        if (!in_array($new_username, $existing_usernames)) {
            // Update username
            tep_db_query("UPDATE " . TABLE_PHPBB_USERS . " SET username = '" . tep_db_input($new_username) . "',  username_clean = '" . tep_db_input($new_username_clean) . "' WHERE user_id = '" . (int)$phpBB_user_id . "'");
            // Remove from osCommerce group
            tep_db_query("DELETE FROM " . TABLE_PHPBB_USER_GROUPS . " WHERE user_id = '" . (int)$phpBB_user_id . "' AND group_id = '" . (int)$new_group_id . "'");
            tep_redirect(tep_href_link($succesful_redirect, '', 'SSL'));
        } else {
            $error = true;
            $messageStack->add('forum_username', TEXT_USERNAME_EXISTS);
        }
    } else {
        // Same username as randomly assigned. Remove from osCommerce group
        tep_db_query("DELETE FROM " . TABLE_PHPBB_USER_GROUPS . " WHERE user_id = '" . (int)$phpBB_user_id . "' AND group_id = '" . (int)$new_group_id . "'");
        tep_redirect(tep_href_link($succesful_redirect, '', 'SSL'));
    }
  }

  
  $breadcrumb->add(NAVBAR_TITLE, tep_href_link(FILENAME_FORUM_USERNAME));
  require(DIR_WS_INCLUDES . 'template_top.php');

?>
<h1><?php echo HEADING_TITLE; ?></h1>

<?php
  if ($messageStack->size('forum_username') > 0) {
    echo $messageStack->output('forum_username');
  }
?>

<?php echo tep_draw_form('forum_username', tep_href_link(FILENAME_FORUM_USERNAME, '', 'SSL'), 'post', '', true) . tep_draw_hidden_field('action', 'process') . tep_draw_hidden_field('from_forum', $from_forum); ?>
<div class="contentContainer">
  <div class="contentText">
    <?php echo TEXT_INFORMATION; ?>
  </div>

    <table border="0" cellspacing="2" cellpadding="2" width="100%">  
      <tr>
        <td class="fieldKey"><?php echo ENTRY_USERNAME; ?></td>
        <td class="fieldValue"><?php echo tep_draw_input_field('new_username', ''); ?><? if ($error) {}?></td>
      </tr>
    </table>
  <div class="buttonSet">
    <span class="buttonAction"><?php echo tep_draw_button(IMAGE_BUTTON_CONTINUE, 'triangle-1-e'); ?></span>
  </div>
</div>
</form>
<?php
  require(DIR_WS_INCLUDES . 'template_bottom.php');
  require(DIR_WS_INCLUDES . 'application_bottom.php');
?>
