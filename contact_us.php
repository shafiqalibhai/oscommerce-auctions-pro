<?php
/*
  $Id: contact_us.php,v 1.42 2003/06/12 12:17:07 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

//----------------------------------------------------------------------------
// Copyright (c) 2007-2009 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Catalog: Contact us page
//----------------------------------------------------------------------------
// I-Metrics Layer
//----------------------------------------------------------------------------
// Modifications:
// - 07/05/2007: PHP5 Register Globals and Long Arrays Off support added
// - 07/08/2007: PHP5 Long Arrays Off support added
// - 07/12/2007: Moved HTML Header/Footer to a common section
// - 08/31/2007: HTML Body Common Sections Added
// - 04/12/2008: Helpdesk support added
// - 01/14/2009: Anti-Bot code added
//----------------------------------------------------------------------------
// Released under the GNU General Public License v3.00
//----------------------------------------------------------------------------
*/
  require('includes/application_top.php');
  require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_CONTACT_US);

  $action = (isset($_GET['action']) ? $_GET['action'] : '');
  $g_validator->post_validate(array('email', 'name', 'subject', 'enquiry'));

  $contact_form_name = 'contact_us';

  $error = false;
  switch($action) {
    case 'send':
      if( !strlen($_POST['enquiry']) ) {
        $messageStack->add(tep_get_script_name(), 'The enquiry field is empty');
        $error = true;
      }

	  $email_subject = $_POST['subject'] . ' ' . EMAIL_SUBJECT;

      if( !$error ) {
        if (tep_validate_email($_POST['email'])) {
//-MS- Help Desk Added
          $department_query = tep_db_query("select email_address, name from " . TABLE_HELPDESK_DEPARTMENTS . " where department_id = '" . (int)$_POST['department_id'] . "' and front='1'");
          if( $department = tep_db_fetch_array($department_query) ) {
            tep_mail($department['name'], $department['email_address'], $email_subject, $_POST['enquiry'], $_POST['name'], $_POST['email'], '');
            tep_redirect(tep_href_link(FILENAME_CONTACT_US, 'action=success'));
          } else {
            $error = true;
            $messageStack->add(tep_get_script_name(), ENTRY_EMAIL_ADDRESS_CHECK_ERROR);
          }
//-MS- Help Desk Added EOM
        } else {
          $error = true;
          $messageStack->add(tep_get_script_name(), ENTRY_EMAIL_ADDRESS_CHECK_ERROR);
        }
      }
      break;
    default:
      break;
  }

  $breadcrumb->add(NAVBAR_TITLE, tep_href_link(FILENAME_CONTACT_US));
?>
<?php require('includes/objects/html_start_sub1.php'); ?>
<?php require('includes/objects/html_start_sub2.php'); ?>
<?php require('includes/objects/html_body_header.php'); ?>
<?php
  if( $action == 'success' ) {
?>
      <div><?php echo TEXT_SUCCESS; ?></div>
      <div class="buttonsRow vpad tspacer">
        <div class="ralign rspacer"><?php echo '<a href="' . tep_href_link() . '" class="mbutton">' . IMAGE_BUTTON_CONTINUE . '</a>'; ?></div>
      </div>
<?php
  } else {
    $departments_array = array();
    $departments_query = tep_db_query("select department_id, title from " . TABLE_HELPDESK_DEPARTMENTS . " where front='1' order by title");
    while ($departments = tep_db_fetch_array($departments_query)) {
      $departments_array[] = array('id' => $departments['department_id'], 'text' => $departments['title']);
    }
?>
      <div class="bounder"><?php echo tep_draw_form($contact_form_name, tep_href_link(FILENAME_CONTACT_US, 'action=send', 'SSL') ); ?><fieldset><legend><?php echo TEXT_INFO_FORM; ?></legend>
        <div class="bounder vpad bspacer altrow heavy">
          <div class="floatend rspacer"><span class="frequired vpad"><?php echo FORM_REQUIRED_INFORMATION; ?></span></div>
        </div>
        <div class="vpad"><label for="department" class="halfer floater"><?php echo TEXT_SELECT_DEPARTMENT; ?></label><?php echo tep_draw_pull_down_menu('department_id', $departments_array, '', 'id="department"'); ?><span class="frequired cpad lspacer"></span></div>
<?php
  if( tep_session_is_registered('customer_id') ) {
    $customers_query = tep_db_query("select customers_email_address as email, concat(customers_firstname, ' ', customers_lastname) as fullname from " . TABLE_CUSTOMERS . " where customers_id = '" . (int)$customer_id . "'");
    $customers_array = tep_db_fetch_array($customers_query);
?>
        <div class="vpad"><label for="vname" class="halfer floater"><?php echo ENTRY_NAME; ?></label><?php echo tep_draw_hidden_field('name', $customers_array['fullname'], 'id="vname"') . '<b>' . $customers_array['fullname'] . '</b>'; ?></div>
        <div class="vpad"><label for="email" class="halfer floater"><?php echo ENTRY_EMAIL; ?></label><?php echo tep_draw_hidden_field('email', $customers_array['email'], 'id="email"') . '<b>' . $customers_array['email'] . '</b>'; ?></div>
<?php
  } else {
?>
        <div class="vpad"><label for="vname" class="halfer floater"><?php echo ENTRY_NAME; ?></label><?php echo tep_draw_input_field('name', '', 'id="vname"', 'text', true, (isset($error_array['name'])?$error_array['name']:false)); ?><span class="frequired cpad lspacer"></span></div>
        <div class="vpad"><label for="email" class="halfer floater"><?php echo ENTRY_EMAIL; ?></label><?php echo tep_draw_input_field('email', '', 'id="email"', 'text', true, (isset($error_array['email'])?$error_array['email']:false)); ?><span class="frequired cpad lspacer"></span></div>
<?php
  }
?>

        <div class="vpad"><label for="subject" class="halfer floater"><?php echo ENTRY_SUBJECT; ?></label><?php echo tep_draw_input_field('subject', '', 'id="subject"', 'text', true, (isset($error_array['subject'])?$error_array['subject']:false)); ?><span class="frequired cpad lspacer"></span></div>
        <div class="tpad"><label for="enquiry"><?php echo ENTRY_ENQUIRY; ?></label></div>
        <div class="rpad"><?php echo tep_draw_textarea_field('enquiry', 'soft', '50', '15', '', 'class="wider"'); ?></div>
        <div class="buttonsRow vpad tspacer">
          <div class="floater lspacer"><?php echo '<a href="#" class="mbutton2 bsubmit">' . IMAGE_BUTTON_SUBMIT . '</a>'; ?></div>
        </div>
      </fieldset></form></div>
<?php
  }
?>
<?php require('includes/objects/html_end.php'); ?>
