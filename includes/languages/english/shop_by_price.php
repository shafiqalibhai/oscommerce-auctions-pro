<?php
/*
  $Id: privacy.php,v 1.4 2002/11/19 01:48:08 dgw_ Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE', TEXT_PRODUCTS_POSTFIX . ' By Price');
define('HEADING_TITLE', strtoupper(TEXT_PRODUCTS_POSTFIX) . ' BY PRICE');
define('TEXT_NO_PRODUCTS', 'There are currently no products in the selected price range.');
?>