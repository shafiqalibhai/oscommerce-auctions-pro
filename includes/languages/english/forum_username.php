<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('NAVBAR_TITLE', 'Forum Username');
define('HEADING_TITLE', 'Forum Username');

define('ENTRY_USERNAME', 'Username:');
define('TEXT_INFORMATION', 'Please enter a username for forum use before continuing.');
define('TEXT_USERNAME_EXISTS', 'The username already exists. Please choose another one.');
define('TEXT_EMPTY_USERNAME', 'Please type a username.');

?>