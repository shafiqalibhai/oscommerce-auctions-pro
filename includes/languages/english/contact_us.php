<?php
/*
  $Id: contact_us.php,v 1.7 2002/11/19 01:48:08 dgw_ Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
*/

define('HEADING_TITLE', 'Contact Us');
define('NAVBAR_TITLE', 'Contact Us');
define('TEXT_SUCCESS', 'Your enquiry has been successfully sent to the Store Owner.');
define('EMAIL_SUBJECT', 'Enquiry from ' . STORE_NAME);

define('ENTRY_NAME', 'Full Name:');
define('ENTRY_EMAIL', 'E-Mail Address:');
define('ENTRY_ENQUIRY', 'Enquiry:');

//-MS- Added HelpDesk
define('TEXT_SELECT_DEPARTMENT', 'Please select a department you would like to get in contact with:');
define('ENTRY_SUBJECT', 'Subject:');
//-MS- Added HelpDesk EOM

define('TEXT_INFO_FORM', 'Fill in the Enquiry Details');
?>