/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2010 Asymmetric Software. Innovation & Excellence.
// Author: Mark Samios
// http://www.asymmetrics.com
// jQuery: New Account Form Script
//----------------------------------------------------------------------------
// I-Metrics CMS
//----------------------------------------------------------------------------
// Script is intended to be used with the following:
//
// osCommerce, Open Source E-Commerce Solutions
// http://www.oscommerce.com
// Copyright (c) 2003 osCommerce
//
// jQuery JavaScript Library
// http://jquery.com
// Copyright (c) 2009 John Resig
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
var new_account = {
  initialize: false,
  baseURL: false,
  replacementElement: false,

  // Ajax Queue Interface
  ajaxRequest: function (options){
    var request = {
      url: options.url,
      type: options.type || 'POST',
      cache: options.cache || false,
      dataType: options.dataType || 'html',
      load_show: options.type.wait_show || true,
      beforeSend: function (){
        if( options.load_show ) {
          new_account.showAjaxMessage(options.beforeSendMsg || 'Loading, please wait...');
          new_account.showAjaxLoader();
        }
      },

      //dataType: 'json',
      async: options.async || false,
      contentType: options.contentType || 'application/x-www-form-urlencoded; charset=utf-8',
      data: options.data || false,
      success: options.success,
      error: options.error,
      complete: function(robj,result){
        if( document.ajaxq.q['imageProcess'].length <= 0 ) {
          if( options.load_show ) {
            new_account.hideAjaxLoader();
          }
        }
      }
    };
    //$.ajax(request);
    $.ajaxq('imageProcess', request);
    //html = result.responseText;
    //return html;
  },
  showAjaxLoader: function (){
    $('#ajaxLoader').dialog({ 
      autoOpen: true,
      resizable: false,
      modal: true
    });
    $('#ajaxLoader').dialog('open');
  },
  hideAjaxLoader: function (){
    $('#ajaxLoader').dialog('close');
  },
  showAjaxMessage: function (message){
    $('#ajaxMsg').html(message);
  },

  resetSelects: function() {
    $('select[id="country"], select[id="country2"]').each(function(index) {
      $(this).unbind('change').bind('change',function(){
        var index = $(this).val();
        var country_switch = '#switch_'+this.name;
        new_account.setStates(this.name, index, country_switch);
      });
    });
  },

  flipShipping: function($sel) {

    $(':input', $('#flip_shipping')).each(function(index) {
      if( $sel.is(':checked') ) {
        $(this).attr('disabled', true);
      } else {
        $(this).removeAttr('disabled');
        var tmp_name = this.name;
        var billing = '#'+tmp_name.substring(0,tmp_name.lastIndexOf('2'));
        $(this).val($(billing).val());
      }
    });

    if( !$sel.is(':checked') ) {
      $('select[name="country2"]').change();
      var index = $('select[name="state"]').val();
      $('select[name="state2"] option[value="'+index+'"]').attr('selected', true);
    }

    //$sel.css('background', '#C80000');
    //$sel.css('color', '#FFF');
  },

  setStates: function(sel_name, index, sel_out) {
    var $url = 'ajax_control.php';
    var $data = 'module=countries&'+sel_name+'='+index;
    new_account.ajaxRequest({
      type: 'POST',
      url: $url,
      data: $data,
      complete: function(msg){
      },
      success: function(msg) {
        $(sel_out).html(msg);
        new_account.resetSelects();
      }
    });
  },

  
  setSearchLists: function() {
    var $url = 'gallery_replace.php?action=search';
    var $data = $(':select', $('#search_box')).serialize();
    new_account.ajaxRequest({
      type: 'POST',
      url: $url,
      data: $data,
      complete: function(msg){
      },
      success: function(msg) {
        $('#search_box').html(msg);
        new_account.resetSelects();
      }
    });
  },

  setSearchResults: function() {
    var $url = 'gallery_replace.php?action=list';
    var $data = $(':select', $('#search_box')).serialize();
    new_account.ajaxRequest({
      type: 'POST',
      url: $url,
      data: $data,
      complete: function(msg){
      },
      success: function(msg) {
        if(msg == 'error') {
          new_account.setContent();
        } else {
          $('#rightpane').html(msg);
        }
      }
    });
  },

  setContent: function() {
    var $url = 'gallery_replace.php?action=content';
    new_account.ajaxRequest({
      type: 'GET',
      url: $url,
      complete: function(msg){
      },
      success: function(msg) {
        $('#rightpane').html(msg);
      }
    });
  },

  launch: function(source, destination) {
    var wrapper = '';
    var $modal =  $('#modalBox');
    if( !$modal ) {
      $('body').append(
        wrapper = $('<div id="modalBox" title="Image Selection" style="display:none; overflow: hidden;">Loading...Please Wait</div>')
      );
      var inner = $('<div id="ajaxLoader" title="Image Manager" style="display:none;"><img src="includes/javascript/jquery/themes/smoothness/images/ajax_load.gif"><p id="ajaxMsg" class="main">Updating, please wait...</p><hr /></div>').appendTo(wrapper);
    }
    $('#hide_country').css('display', 'none');
    $('#hide_country2').css('display', 'none');
    new_account.resetSelects();
/*
    $('select[id="country"], select[id="country2"]').change(function () {
       var index = $(this).val();
       var country_switch = '#switch_'+this;
       new_account.setStates(this, index, country_switch);
       alert('test');
    });
*/
    $(':checkbox[name="same_address"]').click(function () { 
      new_account.flipShipping($(this));
      if( !new_account.initialize ) {
        return false;
      }
    });

    $(':checkbox[name="same_address"]').click();
    new_account.initialize = true;
  }
}

