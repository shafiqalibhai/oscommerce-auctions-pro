/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2010 Asymmetric Software. Innovation & Excellence.
// Author: Mark Samios
// http://www.asymmetrics.com
// jQuery: Add a pair of name/email
//----------------------------------------------------------------------------
// I-Metrics CMS
//----------------------------------------------------------------------------
// Script is intended to be used with the following:
//
// osCommerce, Open Source E-Commerce Solutions
// http://www.oscommerce.com
// Copyright (c) 2003 osCommerce
//
// jQuery JavaScript Library
// http://jquery.com
// Copyright (c) 2009 John Resig
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
var email_add = {
  button: '#add_pair',
  placeholder: '#customer_invite',
  launch: function() {
    $(email_add.button).click(function() {
      var extra = $(email_add.placeholder).html();
      extra = extra.replace(new RegExp( "\\*", "g" ), '');
      $(email_add.placeholder).append(extra);
      return false;
    });
  }
}
