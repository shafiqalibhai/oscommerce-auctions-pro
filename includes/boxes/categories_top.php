<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2007 Asymmetric Software. Innovation & Excellence.
// Author: Mark Samios
// http://www.asymmetrics.com
// ---------------------------------------------------------------------------
// Catalog: Top Categories box
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
  $categories_query = tep_db_query("select c.categories_id, cd.categories_name from " . TABLE_CATEGORIES . " c left join " . TABLE_CATEGORIES_DESCRIPTION . " cd on (c.categories_id=cd.categories_id) where c.parent_id = '0' and cd.language_id='" . (int)$languages_id ."' order by c.sort_order, cd.categories_name");
  if( tep_db_num_rows($categories_query) ) {
?>

          <tr>
            <td class="infoBoxBorders">
<?php
    $info_box_contents = array();
    $info_box_contents[] = array('text' =>  '<a href="' . tep_href_link(FILENAME_PRODUCTS_NEW) . '" class="infoBoxHeading">' . 'SELECTION' . '</a>');
    new infoBoxHeading($info_box_contents);

    $info_box_contents = array();
    while($categories_array = tep_db_fetch_array($categories_query) ) {
      $info_box_contents[] = array('text' => '<a href="' . tep_href_link(FILENAME_DEFAULT, 'cPath=' . tep_get_category_path($categories_array['categories_id']) ) . '" title="' . $categories_array['categories_name'] . '" class="infoBoxContents">' . $categories_array['categories_name'] . '</a>');
    }
    new infoBox($info_box_contents);
?>
            </td>
          </tr>
<?php
    if(LAYOUT_LEFT_COLUMN_BOX_SEPARATOR > 0) {
?>
          <tr>
            <td><?php echo tep_draw_separator('pixel_trans.gif', '100%', LAYOUT_LEFT_COLUMN_BOX_SEPARATOR); ?></td>
          </tr>
<?php
    }
  }
?>