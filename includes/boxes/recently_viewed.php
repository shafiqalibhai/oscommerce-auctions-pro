<?php
/*
  $Id: whats_new.php,v 1.31 2003/02/10 22:31:09 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

  //$recently_viewed_query = tep_db_query("select p.products_id, pd.products_name from " . TABLE_PRODUCTS . " p left join " . TABLE_PRODUCTS_DESCRIPTION . " pd on (pd.products_id=p.products_id) where language_id = '" . (int)$languages_id . "' order by pd.date_viewed desc limit 10");
  $recently_viewed_query = tep_db_query("select p.products_id, pd.products_name from " . TABLE_PRODUCTS . " p left join " . TABLE_PRODUCTS_DESCRIPTION . " pd on (pd.products_id=p.products_id) where language_id = '" . (int)$languages_id . "' order by rand() limit 4");
  if( tep_db_num_rows($recently_viewed_query) ) {
?>
<!-- reently_viewed //-->
          <tr>
            <td>
<?php

    $info_box_contents = array();
    $info_box_contents[] = array(
                                 'text' => BOX_HEADING_RECENTLY_VIEWED
                                );

    new infoBoxHeading($info_box_contents, false, false);

    $info_box_contents = array();
    while($products_viewed = tep_db_fetch_array($recently_viewed_query) ) {
      $info_box_contents[] = array(
                                   'text' => '<a href="' . tep_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . $products_viewed['products_id'], 'NONSSL') . '" class="cat_passive">' . $products_viewed['products_name'] . '</a>',
                                   'params' => 'class="cat_passive"'
                                  );
    }

    new rowBox($info_box_contents);
?>
            </td>
          </tr>
<!-- whats_new_eof //-->
<?php
  }
?>
