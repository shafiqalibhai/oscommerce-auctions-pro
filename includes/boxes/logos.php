<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2007 Asymmetric Software. Innovation & Excellence.
// Author: Mark Samios
// http://www.asymmetrics.com
//----------------------------------------------------------------------------
// Catalog: Box with Logos
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
------------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
?>
          <tr>
            <td>
<?php
  $info_box_contents = array();
  $info_box_contents[] = array('text' => '<a href="' . tep_href_link(FILENAME_CHECKOUT_SHIPPING, '', 'SSL') . '">' . tep_image(DIR_WS_IMAGES . 'rm_free_shipping.jpg', 'Free Shipping') . '</a>');
  new plainBox($info_box_contents);
  if(LAYOUT_RIGHT_COLUMN_BOX_SEPARATOR > 0) {
?>
            </td>
          </tr>
          <tr>
            <td><?php echo tep_draw_separator('pixel_trans.gif', '100%', LAYOUT_RIGHT_COLUMN_BOX_SEPARATOR); ?></td>
          </tr>
          <tr>
            <td>
<?php
  }
  $info_box_contents = array();
  $info_box_contents[] = array('text' => '<a href="' . tep_href_link(FILENAME_DEFAULT, 'cPath=79') . '">' . tep_image(DIR_WS_IMAGES . 'rm_ladies_choice.jpg', 'Ladies Choice') . '</a>');
  new plainBox($info_box_contents);
?>
            </td>
          </tr>
