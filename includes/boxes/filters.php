<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2008 Asymmetric Software. Innovation & Excellence.
// Author: Mark Samios
// http://www.asymmetrics.com
// ---------------------------------------------------------------------------
// Catalog: Products by Filter box
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
  if( !$current_product_id && isset($g_filters) && is_object($g_filters) ) {
    $filter_array = $g_filters->get_layout(array(2,3));
    foreach($filter_array as $key => $value) {
      $sub_entries = count($value);
      if( $sub_entries ) {
        $param_key = $g_filters->get_filters_parameter($key);
        $name = $g_filters->get_filters_name($key);
?>
          <tr>
            <td class="infoBoxBorders">
<?php
        $info_box_contents = array();
        //$info_box_contents[] = array('text' =>  '<a href="' . tep_href_link(FILENAME_SHOP_BY_FILTER, 'filter_id=' . $key) . '" class="infoBoxHeading">' . strtoupper(TEXT_PRODUCTS_POSTFIX) . ' BY ' . strtoupper($name) . '</a>');
        $info_box_contents[] = array('text' =>  strtoupper(TEXT_PRODUCTS_POSTFIX) . ' BY ' . strtoupper($name));
        new infoBoxHeading($info_box_contents);
        $info_box_contents = array();

        foreach($value as $key2 => $value2) {
          $info_box_contents[] = array('text' =>  '<a href="' . tep_href_link(FILENAME_SHOP_BY_FILTER, $param_key . '=' . $key2) . '" class="infoBoxContents">' . $value2 . '</a>');
        }
        new infoBox($info_box_contents);
?>
            </td>
          </tr>
<?php
        if(LAYOUT_LEFT_COLUMN_BOX_SEPARATOR > 0) {
?>
          <tr>
            <td><?php echo tep_draw_separator('pixel_trans.gif', '100%', LAYOUT_LEFT_COLUMN_BOX_SEPARATOR); ?></td>
          </tr>
<?php
        }
      }
    }
    unset($name, $value, $key, $key2, $value2, $filter, $box_categories);
  }
?>