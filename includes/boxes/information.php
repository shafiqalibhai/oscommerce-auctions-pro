<?php
/*
  $Id: information.php,v 1.6 2003/02/10 22:31:00 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/
?>
<!-- information //-->
          <tr>
            <td>
<?php
  $info_box_contents = array();
  $info_box_contents[] = array('text' => BOX_HEADING_INFORMATION);

  new infoBoxHeading($info_box_contents, false, false);

  $info_box_contents = array();
  $cText = new gtext_front();
  $gtext_array = $cText->get_gtext_entries(DEFAULT_LEFT_TEXT_ZONE_ID);
  foreach( $gtext_array as $key => $value ) {

    if( isset($_GET['gtext_id']) && $_GET['gtext_id'] == $value['gtext_id'] ) {
      $info_box_contents[] = array('text' => $value['gtext_title'],
                                   'params' => 'class="cat_active"');
    } else {
      $info_box_contents[] = array('text' => '<a href="' . tep_href_link(FILENAME_GENERIC_PAGES, 'abz_id=' . DEFAULT_FOOTER_TEXT_ZONE1_ID . '&gtext_id=' . $value['gtext_id']) . '" class="cat_passive">' . $value['gtext_title'] . '</a>',
                                   'params' => 'class="cat_passive"');
    }
  }
  new rowBox($info_box_contents);
  unset($cText, $gtext_array, $info_box_contents);

/*
  $info_box_contents[] = array('text' => '<a href="' . tep_href_link(FILENAME_SHIPPING) . '" class="cat_passive" title="more about ' . BOX_INFORMATION_SHIPPING . '">' . BOX_INFORMATION_SHIPPING . '</a>',
                               'params' => 'class="cat_passive"');
  $info_box_contents[] = array('text' => '<a href="' . tep_href_link(FILENAME_PRIVACY) . '" class="cat_passive">' . BOX_INFORMATION_PRIVACY . '</a>',
                               'params' => 'class="cat_passive"');
  $info_box_contents[] = array('text' => '<a href="' . tep_href_link(FILENAME_CONDITIONS) . '" class="cat_passive">' . BOX_INFORMATION_CONDITIONS . '</a>',
                               'params' => 'class="cat_passive"');
  $info_box_contents[] = array('text' => '<a href="' . tep_href_link(FILENAME_CONTACT_US) . '" class="cat_passive">' . BOX_INFORMATION_CONTACT . '</a>',
                               'params' => 'class="cat_passive"');
  new rowBox($info_box_contents);
*/

/*
  $info_box_contents = array();
  $info_box_contents[] = array('text' => '<a href="' . tep_href_link(FILENAME_SHIPPING) . '">' . BOX_INFORMATION_SHIPPING . '</a><br>' .
                                         '<a href="' . tep_href_link(FILENAME_PRIVACY) . '">' . BOX_INFORMATION_PRIVACY . '</a><br>' .
                                         '<a href="' . tep_href_link(FILENAME_CONDITIONS) . '">' . BOX_INFORMATION_CONDITIONS . '</a><br>' .
                                         '<a href="' . tep_href_link(FILENAME_CONTACT_US) . '">' . BOX_INFORMATION_CONTACT . '</a>');

  new infoBox($info_box_contents);
*/
?>
            </td>
          </tr>
<!-- information_eof //-->
