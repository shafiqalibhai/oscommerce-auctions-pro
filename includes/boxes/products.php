<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2007 Asymmetric Software. Innovation & Excellence.
// Author: Mark Samios
// http://www.asymmetrics.com
// ---------------------------------------------------------------------------
// Catalog: Products in Boxes grouped by categories
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
  $categories_query_raw = "select cd.categories_id, cd.categories_name from " . TABLE_CATEGORIES_DESCRIPTION . " cd where cd.language_id='" . (int)$languages_id ."'";
  $total_items = array();
  tep_query_to_array($categories_query_raw, $total_items);
  for( $i=0, $j=count($total_items); $i<$j; $i++) {

    if( $current_product_id ) {
      $path = tep_get_product_path($current_product_id);
      $category_array = explode('_', $path);
      if( $category_array[count($category_array)-1] != $total_items[$i]['categories_id'] ) {
        continue;
      }
    }

    $products_query_raw = tep_get_collection($total_items[$i]['categories_id']);
    $total_products = array();
    tep_query_to_array($products_query_raw, $total_products);
    $y = count($total_products);
    if(!$y) continue;
?>
          <tr>
            <td class="infoBoxBorders">
<?php
    for($x=0, $y=count($total_products); $x<$y; $x++) {
      $total_products[$x]['products_name'] = tep_get_products_name($total_products[$x]['products_id']);
      $total_products[$x]['products_name'] = tep_create_safe_string($total_products[$x]['products_name']);
    }

    $sort_array = array();
    for($x=0, $y=count($total_products); $x<$y; $x++) {
      $sort_array[$total_products[$x]['products_name'] . $i] = $total_products[$x];
    }
    ksort($sort_array);
    $total_products = array_values($sort_array);

    $info_box_contents = array();
    $keyword = strtoupper(tep_create_safe_string($total_items[$i]['categories_name']));
    $info_box_contents[] = array(
                                 'text' => $keyword,
                                );
    new infoBoxHeading($info_box_contents);

    $info_box_contents = array();
    for($x=0, $y=count($total_products); $x<$y; $x++) {
      $tmp_array = explode(' ' , $total_products[$x]['products_name']);
      if( is_array($tmp_array) && count($tmp_array) && isset($tmp_array[count($tmp_array)-1]) && strtoupper($tmp_array[count($tmp_array)-1]) == $keyword) {
        unset($tmp_array[count($tmp_array)-1]);
        $total_products[$x]['products_name'] = implode(' ', $tmp_array);
      }

      $info_box_contents[] = array('text' => '<a href="' . tep_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . $total_products[$x]['products_id']) . '" title="' . $total_products[$x]['products_name'] . '" class="infoBoxContents">' . $total_products[$x]['products_name'] . '</a>');
    }
    new infoBox($info_box_contents);
?>
            </td>
          </tr>
<?php
    if(LAYOUT_LEFT_COLUMN_BOX_SEPARATOR > 0) {
?>
          <tr>
            <td><?php echo tep_draw_separator('pixel_trans.gif', '100%', LAYOUT_LEFT_COLUMN_BOX_SEPARATOR); ?></td>
          </tr>
<?php
    }
  }
?>