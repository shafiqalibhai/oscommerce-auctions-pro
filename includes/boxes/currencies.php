<?php
/*
  $Id: currencies.php,v 1.16 2003/02/12 20:27:31 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

  $currencies_count = count($currencies->currencies);
  if( $currencies_count > 1 ) {
?>
          <tr>
            <td class="infoBoxBorders">
<?php
    $info_box_contents = array();
    $info_box_contents[] = array('text' => BOX_HEADING_CURRENCIES);

    new infoBoxHeading($info_box_contents);

    $currencies_array = array();
    $tmp_array = array_values($currencies->currencies);
    for($i=0; $i<$currencies_count; $i++ ) {
      $currencies_array[] = array('id' => $i, 'text' => $tmp_array[$i]['title']);
    }

    $info_box_contents = array();
    $info_box_contents[] = array('form' => tep_draw_form('currencies', tep_href_link(basename($PHP_SELF), '', $request_type)),
                                 'align' => 'center',
                                 'text' => tep_draw_pull_down_menu('currency', $currencies_array, $currency, 'onChange="this.form.submit();" style="width: 100%"')
                                );

    new infoBox($info_box_contents);
?>
            </td>
          </tr>
<?php
    if(LAYOUT_LEFT_COLUMN_BOX_SEPARATOR > 0) {
?>
          <tr>
            <td><?php echo tep_draw_separator('pixel_trans.gif', '100%', LAYOUT_LEFT_COLUMN_BOX_SEPARATOR); ?></td>
          </tr>
<?php
    }
  }
?>