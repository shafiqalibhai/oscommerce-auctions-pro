<?php
/*
  $Id: categories.php,v 1.25 2003/07/09 01:13:58 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

  $ranges_query = tep_db_query("select numeric_ranges_id, numeric_ranges_desc from " . TABLE_NUMERIC_RANGES . " where status_id = '1' order by sort_id");
  if( tep_db_num_rows($ranges_query) ) {
?>
          <tr>
            <td valign="top">
<?php
    $info_box_contents = array();
    $info_box_contents[] = array(
                                 'text' => '<a href="' . tep_href_link(FILENAME_SHOP_BY_PRICE) . '">' . strtoupper(BOX_HEADING_SHOP_BY_PRICE) . '</a>'
                                );

    new infoBoxHeading($info_box_contents, false, false);

    $info_box_contents = array();

    while( $ranges_array = tep_db_fetch_array($ranges_query) ) {

      if( isset($_GET['range_id']) && $ranges_array['numeric_ranges_id'] == $_GET['range_id'] ) {
        $info_box_contents[] = array(
                                     'text' => '<span class="infoBoxSelected">' . $ranges_array['numeric_ranges_desc'] . '</span>',
                                    );
      } else {
        $info_box_contents[] = array(
                                     'text' => '<a href="' . tep_href_link(FILENAME_SHOP_BY_PRICE, 'range_id=' . $ranges_array['numeric_ranges_id']) . '" title="' . $ranges_array['numeric_ranges_desc'] . '" class="infoBoxContents">' . $ranges_array['numeric_ranges_desc'] . '</a>',
                                    );
      }
    }
    new infoBox($info_box_contents);
?>
            </td>
          </tr>

<?php
    if(LAYOUT_LEFT_COLUMN_BOX_SEPARATOR > 0) {
?>
          <tr>
            <td><?php echo tep_draw_separator('pixel_trans.gif', '100%', LAYOUT_LEFT_COLUMN_BOX_SEPARATOR); ?></td>
          </tr>
<?php
    }
  }
?>
