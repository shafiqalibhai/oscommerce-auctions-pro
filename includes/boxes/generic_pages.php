<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2008 Asymmetric Software. Innovation & Excellence.
// Author: Mark Samios
// http://www.asymmetrics.com
//----------------------------------------------------------------------------
// Catalog: Generic Pages Box
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
------------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
//
*/

  $zones_array = array('Customer Service');
  $cText = new gtext_front();
?>
          <tr>
            <td class="infoBoxBorders">
<?php
  foreach($zones_array as $id => $zone) {  
    $text_entries = $cText->get_gtext_entries($zone);
    if( count($text_entries) ) {

      $id = $cText->get_zone_id($zone);
      $info_box_contents = array();
      $info_box_contents[] = array(
                                   'text' => strtoupper($zone)
                                  );

      new infoBoxHeading($info_box_contents);
      $info_box_contents = array();

      foreach($text_entries as $key => $value) {
        $info_box_contents[] = array('text' => '<a href="' . tep_href_link(FILENAME_GENERIC_PAGES, 'gtext_id=' . $key) . '" class="infoBoxContents" rel="nofollow">' . $value['gtext_title'] . '</a>');
      }
      new infoBox($info_box_contents);
    }
  }
?>
            </td>
          </tr>
<?php
  if(LAYOUT_LEFT_COLUMN_BOX_SEPARATOR > 0) {
?>
          <tr>
            <td><?php echo tep_draw_separator('pixel_trans.gif', '100%', LAYOUT_LEFT_COLUMN_BOX_SEPARATOR); ?></td>
          </tr>
<?php
  }
?>