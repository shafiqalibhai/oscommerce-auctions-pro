<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2008 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Catalog: Groups Front class
// Manages the Group fields for products
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
  class groups_front {
    var $group_array, $group_types_array;
// class constructor
    function groups_front() {
      $this->group_array = array();
      $this->group_types_array = array(
                             '1' => 'STATIC-TEXT',
                             '2' => 'RADIO',
                             '3' => 'RADIO-IMAGE',
                             '4' => 'DROP-DOWN',
                             '5' => 'INPUT-LINE',
                             '6' => 'TEXT-AREA',
                             '7' => 'CHECK-BOX',
                            );
    }

    function get_product_groups($product_id) {
      if( isset($this->group_array[$product_id]) ) {
        return $this->group_array[$product_id];
      }
      $group_array = array();
      $group_query_raw = "select p2gf.group_fields_id, gf.group_fields_name, gf.group_fields_description, gf.limit_id from " . TABLE_PRODUCTS_TO_GROUP_FIELDS . " p2gf left join " . TABLE_GROUP_FIELDS . " gf on (gf.group_fields_id=p2gf.group_fields_id) where p2gf.products_id = '" . (int)$product_id . "' and gf.status_id='1' order by gf.sort_id";
      tep_query_to_array($group_query_raw, $group_array);
      if( count($group_array) ) {
        $this->group_array[$product_id] = $group_array;
      }
      return $group_array;
    }

    function group_fields_request($request_array, &$error_array) {
      $results_array = array();
      if( !is_array($error_array) ) {
        $error_array = array();
      }
      $option_value_flags_array = array();
      if( isset($request_array['gfid']) && is_array($request_array['gfid']) ) {
        foreach($request_array['gfid']  as $group_key => $group) {
          $options_query_raw = "select group_options_id, group_types_id, group_options_name, price_status, stock_status, image_status, limit_id from " . TABLE_GROUP_OPTIONS . " where group_fields_id = '" . (int)$group_key . "' and status_id = '1' order by sort_id";
          tep_query_to_array($options_query_raw, $options_array);
          foreach($options_array as $option_key => $option) {
            if( !isset($this->group_types_array[$option['group_types_id']])) continue;
            $html_text = $this->group_types_array[$option['group_types_id']];
            switch($html_text) {
              case 'RADIO':
              case 'RADIO-IMAGE':
                if( isset($option_value_flags_array[$option['group_options_id']]) ) {
                  break;
                }

                if( !isset($request_array['radio']) || !is_array($request_array['radio']) ) {
                  $error_array[$option['group_options_id']] = 'You must select an option from: ' . $option['group_options_name'];
                  break;
                }

                foreach($request_array['radio'] as $radio_key => $radio) {
                  if( $radio_key == $option['group_options_id'] ) {
                    $tmp_array = explode('(',$radio);
                    if( !is_array($tmp_array) ) {
                      $error_array[$option['group_options_id']] = 'Invalid Selection for ' . $option['group_options_name'];
                      continue;
                    }
                    $radio = trim($tmp_array[0]);
                    $check_query = tep_db_query("select group_values_id, group_values_name from " . TABLE_GROUP_VALUES . " where group_fields_id = '" . (int)$group_key . "' and group_options_id = '" . (int)$option['group_options_id'] . "' and group_values_name = '" . tep_db_input(tep_db_prepare_input($radio)) . "' and status_id = '1'");
                    if( $check_array = tep_db_fetch_array($check_query) ) {
                      $results_array[$check_array['group_values_id']] = $check_array['group_values_name'];
                      unset($error_array[$option['group_options_id']]);
                      $option_value_flags_array[$option['group_options_id']] = $option['group_options_id'];
                      break;
                    }
                  }
                }
                if( !isset($option_value_flags_array[$option['group_options_id']]) ) {
                  $error_array[$option['group_options_id']] = 'Invalid Selection for ' . $option['group_options_name'];
                }
                break;
              case 'DROP-DOWN':
                if( isset($option_value_flags_array[$option['group_options_id']]) ) {
                  break;
                }
                if( !isset($request_array['dropdown']) || !is_array($request_array['dropdown']) ) {
                  $error_array[$option['group_options_id']] = 'You must select an option from: ' . $option['group_options_name'];
                  break;
                }
                foreach($request_array['dropdown'] as $dropdown_key => $dropdown) {
                  if( $dropdown_key == $option['group_options_id'] ) {
                    $check_query = tep_db_query("select group_values_id, group_values_name from " . TABLE_GROUP_VALUES . " where group_fields_id = '" . (int)$group_key . "' and group_options_id = '" . (int)$option['group_options_id'] . "' and group_values_id = '" . (int)$dropdown . "' and status_id = '1'");
                    if( $check_array = tep_db_fetch_array($check_query) ) {
                      $results_array[$check_array['group_values_id']] = $check_array['group_values_name'];
                      unset($error_array[$option['group_options_id']]);
                      $option_value_flags_array[$option['group_options_id']] = $option['group_options_id'];
                      break;
                    }
                  }
                }
                if( !isset($option_value_flags_array[$option['group_options_id']]) ) {
                  $error_array[$option['group_options_id']] = 'Invalid Selection for ' . $option['group_options_name'];
                }
                break;
              default:
                break;

            }
            if( isset($option_value_flags_array[$option['group_options_id']]) ) {
              continue;
            }

            $values_query_raw = "select group_values_id, group_values_name from " . TABLE_GROUP_VALUES . " where group_fields_id = '" . (int)$group_key . "' and group_options_id = '" . (int)$option['group_options_id'] . "' and status_id = '1' order by sort_id";
            tep_query_to_array($values_query_raw, $values_array, 'group_values_id');
            $total_values = count($values_array);
            $count_values = 0;
            foreach($values_array as $value_key => $value ) {
              $value_flag = false;
              switch($html_text) {
                case 'CHECK-BOX':
                  if( !isset($request_array['checkbox']) || !is_array($request_array['checkbox']) ) {
                    //$error_array[$value['group_values_id']] = 'Invalid Selection for ' . $value['group_values_name'];
                    break;
                  }
                  if( $value_key == $request_array['checkbox'][$value_key] ) {
                    $results_array[$value['group_values_id']] = $value['group_values_name'];
                    $value_flag = true;
                  }
                  break;
                case 'INPUT-LINE':
                  if( !isset($request_array['input']) || !is_array($request_array['input']) ) {
                    $error_array[$value['group_values_id']] = 'Invalid Entry for ' . $value['group_values_name'];
                    break;
                  }
// Custom for numeric values
                  if( isset($request_array['input'][$value_key]) && !empty($request_array['input'][$value_key]) &&
                    $request_array['input'][$value_key] > 0 &&  $request_array['input'][$value_key] < 50 ) {
                    $results_array[$value['group_values_id']] = tep_db_prepare_input($request_array['input'][$value_key]);
                    $value_flag = true;
                  }
                  break;
                case 'TEXT_AREA':
                  if( !isset($request_array['textarea']) || !is_array($request_array['textarea']) ) {
                    $error_array[$value['group_values_id']] = 'Invalid Entry for ' . $value['group_values_name'];
                    break;
                  }
                  if( $value_key == $request_array['textarea'][$value_key] ) {
                    $results_array[$value['group_values_id']] = $value['group_values_name'];
                    $value_flag = true;
                  }
                  break;
                default:
                  break;

              }
              if( $value_flag == false ) {
                $error_array[$value['group_values_id']] = 'Invalid Selection for ' . $value['group_values_name'];
              } else {
                $count_values++;
              }
            }
            if( $count_values != $total_values ) {
              $error_array[$value['group_values_id']] = 'Missing entries or Invalid entries specified';
            }
          }
        }
      }
      return $results_array;
    }

    function get_price_from_group($ids_array) {
      $total = 0;
      if( !is_array($ids_array) || !count($ids_array) ) {
        return $total;
      }
      $tmp_array = array_keys($ids_array);
      $values_query = tep_db_query("select group_values_price from " . TABLE_GROUP_VALUES . " where group_values_id in ('" . implode("','", $tmp_array) . "') and status_id = '1'");
      while($values_array = tep_db_fetch_array($values_query)) {
        $total += $values_array['group_values_price'];
      }
      return $total;
    }

    function get_group_prices($ids_array) {
      $prices_array = array();

      if( !is_array($ids_array) || !count($ids_array) ) {
        return $prices_array;
      }
      $tmp_array = array_keys($ids_array);
      $values_query = tep_db_query("select group_values_id, group_values_price from " . TABLE_GROUP_VALUES . " where group_values_id in ('" . implode("','", $tmp_array) . "') and status_id = '1'");
      while($values_array = tep_db_fetch_array($values_query)) {
        $prices_array[$values_array['group_values_id']] = $values_array['group_values_price'];
      }
      return $prices_array;
    }

    function get_names_from_group($ids_array) {
      $names_array = array();
      if( !is_array($ids_array) || !count($ids_array) ) {
        return $names_array;
      }
      $tmp_array = array_keys($ids_array);
      $values_query = tep_db_query("select gv.group_values_id, gv.group_values_name, go.group_options_name from " . TABLE_GROUP_VALUES . " gv left join " . TABLE_GROUP_OPTIONS . " go on (go.group_options_id=gv.group_options_id) where gv.group_values_id in ('" . implode("','", $tmp_array) . "') and gv.status_id = '1' order by gv.group_fields_id, go.sort_id, gv.sort_id");
      while($values_array = tep_db_fetch_array($values_query)) {
        $names_array[$values_array['group_values_id']] = $values_array['group_options_name'] . ': ' . $values_array['group_values_name'];
      }
      return $names_array;
    }

  }
?>
