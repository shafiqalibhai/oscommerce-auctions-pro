<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2007 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
//----------------------------------------------------------------------------
// Generic Text Zones class for osCommerce Catalog
// This is a Bridge for the Abstract Zones front-end
// Support class for text pages via abstract zones
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/

  class gtext_front extends abstract_front {
    var $products_array, $text_array;

// class constructor
    function gtext_front() {
      parent::abstract_front();
      $this->products_array = array();
      $this->text_array = array();
    }

    function get_gtext_products($zone, $gtext_id) {
      global $languages_id;
      $this->products_array = array();
      $zone_id = $this->get_zone($zone);
      if( !$zone_id) {
        return $this->products_array;
      }

      $zone_query = tep_db_query("select p.products_id, pd.products_name, p.products_model, p.products_image, if(s.status, s.specials_new_products_price, p.products_price) as products_price, p.products_tax_class_id, p.products_status from " . TABLE_GTEXT_TO_PRODUCTS . " gt2p left join " . TABLE_PRODUCTS . " p on (p.products_id=gt2p.products_id) left join " . TABLE_PRODUCTS_DESCRIPTION . " pd on (p.products_id=pd.products_id) left join " . TABLE_SPECIALS . " s on (s.products_id=p.products_id) where gt2p.abstract_zone_id = '" . (int)$zone_id . "' and gt2p.gtext_id= '" . (int)$gtext_id . "' and pd.language_id = '" . (int)$languages_id . "' order by gt2p.sort_order");
      while( $zone = tep_db_fetch_array($zone_query) ) {
        $this->products_array[$zone['products_id']] = $zone;
      }
      return $this->products_array;
    }

    function get_gtext_entries($zone, $status='1') {
      $this->text_array = array();
      $zone_id = $this->get_zone($zone);
      if( !$zone_id) {
        return $this->text_array;
      }

      $status_string = '';
      if( $status ) {
        $status_string = " and gt.status= '1'";
      }
      $zone_query = tep_db_query("select gt.gtext_id, gt.gtext_title, gt.gtext_description from " . TABLE_GTEXT . " gt left join " . TABLE_GTEXT_TO_DISPLAY . " gt2d on (gt.gtext_id=gt2d.gtext_id) where gt2d.abstract_zone_id = '" . (int)$zone_id . "' " . $status_string . " order by gt2d.sequence_order");

      while( $zone = tep_db_fetch_array($zone_query) ) {
        $this->text_array[$zone['gtext_id']] = $zone;
      }
      return $this->text_array;
    }

/*

    function check_shipping_zone_by_name($zone_name, $mode) {
      $zone_id = $this->get_zone_id($zone_name)
      if( !$zone_id )
        return false;
      return $this->check_shipping_zone($zone_id, $mode);
    }



// Check if product in cart belong to the shipping zone passed
    function check_shipping_zone($zone_id, $mode) {

      if( isset($this->zones_array[$zone_id]) ) {
        return $this->check_mode($this->zones_array[$zone_id], $mode);
      }

      $key_array = array();
      $zone_query = tep_db_query("select sz2c.categories_id, sz2c.products_id from " . TABLE_PRODUCTS_ZONES_TO_CATEGORIES . " sz2c where az.abstract_zone_id = '" . $zone_id . "'");
      while( $zone = tep_db_fetch_array($zone_query) ) {

        if( !$zone['categories_id'] && !$zone['products_id'] )
          return true;

        if( !$zone['products_id'] ) {
          $this->set_key_products($key_array, $zone['categories_id']);
        } else {
          $key_array[$zone['products_id']] = $zone['products_id'];
        }
      }
      $result = $this->check_mode($key_array, $mode);
      $this->names_array[$zone_name] = $key_array;
      return $result;
    }

    function get_shipping_cost($zone_id) {
      if( isset($fields_array[$zone_id]) ) {
        return $this->fields_array[$zone_id]['cost'];
      }
      $this->set_shipping_fields($zone_id);
      return $this->fields_array[$zone_id]['cost'];
    }

    function set_shipping_fields($zone_id) {
      //$zone_query = tep_db_query("select sz2c.categories_id, sz2c.products_id from " . TABLE_ABSTRACT_ZONES . " az left join " . TABLE_SHIPPING_ZONES_TO_CATEGORIES . " sz2c on (sz2c.abstract_zone_id=az.abstract_zone_id) where sz2c.shipping_price in (" . implode(',', $products_parsed) . ") and az.abstract_zone_name = '" . $zone_name . "'");
      $zone_query = tep_db_query("select sz2c.* from " . TABLE_SHIPPING_ZONES_TO_CATEGORIES . " sz2c where az.abstract_zone_id = '" . $zone_id . "'");
      $fields_array = array();
      while( $zone = tep_db_fetch_array($zone_query) ) {
        if( !$zone['products_id'] ) {
          $products_query = tep_db_query("select products_id from " . TABLE_PRODUCTS_TO_CATEGORIES . " where categories_id = '" . (int)$categories_id . "'");
          while($products = tep_db_fetch_array($products_query) ) {
            $fields_array[$products['products_id']] = $zone;
          }
        } else {
          $fields_array[$zone['products_id']] = $zone;
        }
      }

      $shipping_cost = $max_cost = 0;
      $min_cost = -1;
      foreach( $this->products_array as $key=>$val) {
        if( isset($fields_array[$val['id']]) ) {
          switch($val['shipping_type']) {
            case 0:
              $shipping_cost += $val['shipping_price'];
              break;
            case 1:
              if( $max_cost < $val['shipping_price'] ) {
                $max_cost = $val['shipping_price'];
              }
              break;
            case 2:
              if( $min_cost < 0 || $min_cost > $val['shipping_price'] ) {
                $min_cost = $val['shipping_price'];
              }
              break;
            default:
              break;
          }
        }
      }
      if( $min_cost < 0 )
        $min_cost = 0;

      $shipping_cost += $max_cost+$min_cost;
      $this->fields_array[$zone_id]['cost'] = $shipping_cost;
    }
*/
  }
?>