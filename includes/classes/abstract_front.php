<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2007 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Abstract Zones root class for osCommerce Admin
// Controls relationships among products, categories, prices etc.
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/

  class abstract_front {
    var $cart_products_array, $zones_array;
// class constructor
    function abstract_front() {
      global $cart;
      $this->cart_products_array = $cart->get_products();
      if( !is_array($this->cart_products_array) ) {
        $this->cart_products_array = array();
      }
      $this->zones_array = array();
    }

// Get Zones by class
    function get_zones_by_class($class_name) {
      $this->zones_array = array();
      $zone_query = tep_db_query("select az.abstract_zone_id, az.abstract_zone_name from " . TABLE_ABSTRACT_ZONES . " az left join " . TABLE_ABSTRACT_TYPES . " abt on (abt.abstract_types_id=az.abstract_types_id) where abt.abstract_types_class = '" . tep_db_input(tep_db_prepare_input($class_name)) . "' order by az.abstract_zone_name");
      while($zone = tep_db_fetch_array($zone_query) ) {
        $this->zones_array[$zone['abstract_zone_id']] = $zone;
      }
      return $this->zones_array;
    }

    function get_zone($zone) {
      if( is_numeric($zone) ) {
        $zone_id = $zone;
      } else {
        $zone_id = $this->get_zone_id($zone);
      }

      if( !$zone_id) {
        return 0;
      }

      $zone_query = tep_db_query("select abstract_zone_id from " . TABLE_ABSTRACT_ZONES . " where abstract_zone_id = '" . (int)$zone_id . "'");
      if( tep_db_num_rows($zone_query) ) {
        return $zone_id;
      }
      return 0;
    }

// Get zone id from name
    function get_zone_name($zone_id) {
      $zone_query = tep_db_query("select abstract_zone_name from " . TABLE_ABSTRACT_ZONES . " where abstract_zone_id = '" . (int)$zone_id . "'");
      if( $zone = tep_db_fetch_array($zone_query) ) {
        return $zone['abstract_zone_name'];
      }
      return '';
    }

    function get_zone_data($zone) {
      $result_array = array();
      $zone_id = $this->get_zone($zone);
      if( !$zone_id ) 
        return $result_array;

      $zone_query = tep_db_query("select abstract_zone_name, abstract_zone_desc from " . TABLE_ABSTRACT_ZONES . " where abstract_zone_id = '" . (int)$zone_id . "'");
      if( $zone = tep_db_fetch_array($zone_query) ) {
        return $result_array = $zone;
      }
      return $result_array;
    }

// Get zone id from name
    function get_zone_id($zone_name) {
      $zone_query = tep_db_query("select abstract_zone_id from " . TABLE_ABSTRACT_ZONES . " where abstract_zone_name = '" . tep_db_input(tep_db_prepare_input($zone_name)) . "'");
      if( $zone = tep_db_fetch_array($zone_query) ) {
        return $zone['abstract_zone_id'];
      }
      return 0;
    }

    function set_key_products(&$key_array, $categories_id) {
      $products_query = tep_db_query("select products_id from " . TABLE_PRODUCTS_TO_CATEGORIES . " where categories_id = '" . (int)$categories_id . "'");
      $count = tep_db_num_rows($products_query);
      while($products = tep_db_fetch_array($products_query) ) {
        $key_array[$products['products_id']] = $products['products_id'];
      }
      return $count;
    }

    function set_key_categories(&$key_array, $categories_id) {
      $products_query = tep_db_query("select products_id from " . TABLE_PRODUCTS_TO_CATEGORIES . " where categories_id = '" . (int)$categories_id . "'");
      while($products = tep_db_fetch_array($products_query) ) {
        $key_array[$products['products_id']] = $products['products_id'];
      }
    }

    function check_mode($key_array, $mode) {
      foreach( $this->cart_products_array as $key=>$val) {
        $check = in_array( (int)$val['id'], $key_array);
        if( $check && $mode == 'Inclusive')
          return true;
        elseif(!$check && $mode == 'Exclusive')
          return false;
      }
      return $mode=='Exclusive';
    }
  }
?>
