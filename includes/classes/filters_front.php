<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2008 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Catalog Filters Front class
// Filters products based on the product filters and product selected tables
//----------------------------------------------------------------------------
// I-Metrics Layer
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/

  class filters_front {
    var $filter_array, $parameter_array, $db_names_array, $names_array, $current_parameters_array, $categories_array, $layout_array;
// class constructor
    function filters_front() {
      $this->categories_array = array();
      $this->filter_array = array();
      $this->parameter_array = array();
      $this->current_parameters_array = array();
      $this->db_names_array = array();
      $layout_array = array();
      $filter_query = tep_db_query("select products_filter_id, products_filter_name, products_filter_db_name, products_filter_parameter, layout_id from " . TABLE_PRODUCTS_FILTERS . " where status_id = '1' order by sort_id");
      while($filter = tep_db_fetch_array($filter_query) ) {
        $this->parameter_array[$filter['products_filter_parameter']] = $filter['products_filter_id'];
        $this->db_names_array[$filter['products_filter_db_name']] = $filter['products_filter_id'];
        $this->names_array[$filter['products_filter_name']] = $filter['products_filter_id'];
        $this->filter_array[$filter['products_filter_id']] = array('name' => $filter['products_filter_name'], 'db_name' => $filter['products_filter_db_name'], 'parameter' => $filter['products_filter_parameter'], 'layout' => $filter['layout_id']);
        $this->filter_array[$filter['products_filter_id']]['select'] = array();
        $layout_array[$filter['products_filter_id']] = $filter['layout_id'];

        $select_query = tep_db_query("select products_select_id, products_select_name from " . TABLE_PRODUCTS_SELECT . " where products_select_id > 0 and products_filter_id = '" . (int)$filter['products_filter_id'] . "' and status_id = '1' order by sort_id");
        while($select = tep_db_fetch_array($select_query) ) {
          $this->filter_array[$filter['products_filter_id']]['select'][$select['products_select_id']] = $select['products_select_name'];
        }
      }
      $this->layout_array = tep_array_invert($layout_array);
      unset($filter_query, $select_query, $filter, $select, $layout_array);
    }

    function get_filters_from_parameters() {
      global $g_validator;

      $parameter_array = array();
      $this->current_parameters_array = array();
/*
      foreach($_GET as $key => $value) {
        if(isset($this->parameter_array[$key]) ) {
          $result = $this->validate_option($key, $value);
          if( !$result ) continue;
          $g_validator->update_get($key, $value);
          $parameter_array[$this->filter_array[$this->parameter_array[$key]]['db_name']] = (int)$value;
          $this->current_parameters_array[$this->filter_array[$this->parameter_array[$key]]['parameter']] = (int)$value;
        }
      }
*/
      foreach($this->parameter_array as $key => $value) {
        if(isset($_GET[$key]) ) {
          $result = $this->validate_option($key, $_GET[$key]);
          if( !$result ) continue;
          $g_validator->update_get($key, $_GET[$key]);
          $parameter_array[$this->filter_array[$this->parameter_array[$key]]['db_name']] = (int)$_GET[$key];
          $this->current_parameters_array[$this->filter_array[$this->parameter_array[$key]]['parameter']] = (int)$_GET[$key];
        }
      }

      return $parameter_array;
    }

    function get_current_parameters() {
      return $this->current_parameters_array;
    }

    function validate_option($param, $select_id) {
      $result = false;
      if( !isset($this->parameter_array[$param]) ) {
        return $result;
      }

      $filter_id = $this->parameter_array[$param];
      if( isset($this->filter_array[$filter_id]['select'][$select_id]) ) {
        $result = $select_id;
      }
      return $result;
    }

    function get_excluded_filters_from_parameters() {
      $parameter_array = $this->get_filters_from_parameters();
      $parameter_array = array_diff_assoc($parameter_array, $this->db_names_array);
      return $parameter_array;
    }

    function get_filters_array() {
      return $this->filter_array;
    }

    function get_filters_string_query() {
      $params_array = $this->get_filters_db_names();
      $select_string = '';
      foreach($params_array as $db_col => $index) {
        $select_string .= "p." . tep_db_input(tep_db_prepare_input($db_col)) . ", ";
      }
      return $select_string;
    }

    function get_filters_name($filter_id) {
      $result = '';
      if(isset($this->filter_array[$filter_id]) ) {
        $result = $this->filter_array[$filter_id]['name'];
      }
      return $result;
    }

    function get_filters_parameter($filter_id) {
      $result = '';
      if(isset($this->filter_array[$filter_id]) ) {
        $result = $this->filter_array[$filter_id]['parameter'];
      }
      return $result;
    }

    function get_filters_db_names($name='') {
      if( !tep_not_null($name) ) {
        return $this->db_names_array;
      }
      $tmp_array = array();
      if( isset($this->names_array[$name]) ) {
        $index = $this->names_array[$name];
        $tmp_array[$index] = array($this->db_names_array[$index]['db_name'] => $this->names_array[$name]);
      }
      return $tmp_array;
    }

    function get_option_name_from_db_column($db_col, $select_id) {
      $result = '';
      if( isset($this->db_names_array[$db_col]) ) {
        $index = $this->db_names_array[$db_col];
        if( isset($this->filter_array[$index]['select'][$select_id]) ) {
          $result = $this->filter_array[$index]['select'][$select_id];
        }
      }
      return $result;
    }

    function get_option_name($filter_id, $select_id) {
      $result = '';
      if(isset($this->filter_array[$filter_id]) && isset($this->filter_array[$filter_id]['select'][$select_id]) ) {
        $result = $this->filter_array[$filter_id]['select'][$select_id];

      }
      return $result;
    }
/*
    function get_option_name_from_product($name, $products_id) {
      $result = '';

      if( isset($this->names_array[$name]) ) {
        $filter_id = $this->names_array[$name];
        $filter_info = $this->filter_array[$filter_id];

      }
      if(isset($this->filter_array[$filter_id]) && isset($this->filter_array[$filter_id]['select'][$select_id]) ) {
        $result = $this->filter_array[$filter_id]['select'][$select_id];

      }
      return $result;
    }
*/
    function get_layout($id_array) {
      $result_array = array();
      for( $i=0, $j=count($id_array); $i<$j; $i++) {
        if( isset($this->layout_array[$id_array[$i]]) ) {
          foreach($this->layout_array[$id_array[$i]] as $key => $value) {
            $result_array[$value] = $this->filter_array[$value]['select'];
          }
        }
      }
      return $result_array;
    }

    function get_option_list($filter_id) {
      $tmp_array = array();
      if( isset($this->filter_array[$filter_id]) ) {
        $tmp_array = $this->filter_array[$filter_id]['select'];
      }
      return $tmp_array;
    }

    function get_option_list_from_name($name) {
      $tmp_array = array();
      if( isset($this->names_array[$name]) ) {
        $tmp_array = $this->get_option_list($this->names_array[$name]);
      }
      return $tmp_array;
    }

    function get_categories($filter_id, $index, $parent_id=1) {
      $result = '';
      if( isset($this->categories_array[$filter_id . '_' . $index . '_' . $parent_id]) ) {
        return ($this->categories_array[$filter_id . '_' . $index . '_' . $parent_id]);
      }
      $categories_array = array();
      $categories_query_raw = "select c.categories_id from " . TABLE_CATEGORIES . " c left join " . TABLE_PRODUCTS_TO_CATEGORIES . " p2c on (c.categories_id=p2c.categories_id) left join " . TABLE_PRODUCTS . " p on (p.products_id=p2c.products_id) where p." . tep_db_input(tep_db_prepare_input($this->filter_array[$filter_id]['db_name'])) . " = '" . (int)$index . "' and c.parent_id = '" . (int)$parent_id . "' group by c.categories_id order by c.sort_order";
      tep_query_to_array($categories_query_raw, $categories_array);
      $this->categories_array[$filter_id . '_' . $index . '_' . $parent_id] = $categories_array;
      return $categories_array;
    }
  }
?>
