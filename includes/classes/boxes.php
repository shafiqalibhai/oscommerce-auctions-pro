<?php
/*
  $Id: boxes.php,v 1.33 2003/06/09 22:22:50 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

//----------------------------------------------------------------------------
// Copyright (c) 2007-2010 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Front: Box Classes
//----------------------------------------------------------------------------
// I-Metrics CMS
//----------------------------------------------------------------------------
// Modifications:
// - Converted tables into DIVs
// - Removed unused classes and added new ones for the new stylesheet
// - Changed arguments order to simplify adding messages
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/

  class commonBox {
    var $common_border = '0';
    var $common_parameters = '';
    var $common_row_parameters = '';
    var $common_data_parameters = '';

    // class constructor
    function commonBox($contents, $direct_output = false, $lineflag=false) {
      $form_string = '';
      $commonBox_string = '';

      for ($i=0, $n=sizeof($contents); $i<$n; $i++) {
        if (isset($contents[$i]['form']) && tep_not_null($contents[$i]['form'])) {
          $form_string = $contents[$i]['form'] . "\n";
        }

        if (isset($contents[$i][0]) && is_array($contents[$i][0]) && count($contents[$i][0]) ) {
          for ($x=0, $n2=sizeof($contents[$i]); $x<$n2; $x++) {
            $tmp_string = $params_string = '';
            if (isset($contents[$i][$x]['text']) && tep_not_null($contents[$i][$x]['text'])) {
              if( isset($contents[$i][$x]['class']) && !empty($contents[$i][$x]['class']) ) {
                $params_string .= ' class="' . $contents[$i][$x]['class'] . '"';
              } elseif( isset($contents[$i][$x]['style']) && !empty($contents[$i][$x]['style']) ) {
                $params_string .= ' style="' . $contents[$i][$x]['style'] . '"';
              } elseif( isset($contents[$i][$x]['params']) && !empty($contents[$i][$x]['params']) ) {
                $params_string .= ' ' . $contents[$i][$x]['params'] . '"';
              }

              if( !empty($params_string) ) {
                $tmp_string = '      <div' . $params_string . '>' . "\n";
                if (isset($contents[$i][$x]['form']) && !empty($contents[$i][$x]['form'])) {
                  $form_string = $contents[$i][$x]['form'];
                }
                if( isset($contents[$i][$x]['text']) ) {
                  $tmp_string .= $contents[$i][$x]['text'] . "\n";
                }
                $tmp_string .= '      </div>' . "\n";
              } else {
                $tmp_string =  $contents[$i][$x]['text'];
              }
              $commonBox_string .= $tmp_string;
            }
          }
        } else {
          $tmp_string = $params_string = '';
          if( isset($contents[$i]['style']) && !empty($contents[$i]['style']) ) {
            $params_string .= ' style="' . $contents[$i]['style'] . '"';
          } elseif( isset($contents[$i]['class']) && !empty($contents[$i]['class']) ) {
            $params_string .= ' class="' . $contents[$i]['class'] . '"';
          } elseif( isset($contents[$i]['params']) && !empty($contents[$i]['params']) ) {
            $params_string .= ' ' . $contents[$i]['params'];
          }
          if( isset($contents[$i]['text']) ) {
            if( !empty($params_string) ) {
              $tmp_string = '      <div' . $params_string . '>' . "\n" . $contents[$i]['text'] . "\n" . '      </div>' . "\n";
            } else {
              $tmp_string =  $contents[$i]['text'];
            }
          }
          $commonBox_string .= $tmp_string;
        }
      }

      if( !empty($form_string)) {
         $commonBox_string = $form_string . $commonBox_string . '</form>';
      }

      if( !empty($this->common_parameters)) {
        $commonBox_string = '    <div ' . $this->common_parameters . '>' . $commonBox_string . '</div>' . "\n";
      }

      if ($direct_output == true) echo $commonBox_string;
      return $commonBox_string;
    }
  }

  class largeBox extends commonBox {
    
    function largeBox($contents) {
      $this->common_parameters = 'class="cbox"';
      $result_string = $this->commonBox($contents);
      $result_string = 
        '<div class="infobox">' . "\n" . 
        '  <div class="hbox"></div>' . "\n" . 
        $result_string . 
        '  <div class="fbox"></div>' . "\n" . 
        '</div>' . "\n";
      echo $result_string;
    }
  }

  class contentBox extends commonBox {
    function contentBox($contents, $class='') {

      if( !empty($class) ) {
        $this->common_parameters = 'class="' . $class . '"';
      }

      for($i=0, $j=count($contents); $i<$j; $i++) {
        if( isset($contents[$i]['text']) && !isset($contents[$i]['style']) && !isset($contents[$i]['params']) && !isset($contents[$i]['class']) && strpos($contents[$i]['text'], '<div ') === false ) {
          $contents[$i]['text'] = '<div>' . $contents[$i]['text'] . '</div>';
        }
      }

      $this->commonBox($contents, true);
    }

    function contentBoxContents($contents, $class) {
      $this->common_data_parameters = 'class="' . $class . '"';
      return $this->commonBox($contents);
    }
  }

  class contentBoxHeading extends commonBox {
    function contentBoxHeading($contents) {
      $this->commonBox($contents, true);
    }
  }


  class errorBox extends commonBox {
    function errorBox($contents) {
      $this->common_data_parameters = 'class="errorBox"';
      $this->commonBox($contents, true);
    }
  }

  class noticeBox extends commonBox {
    function noticeBox($contents) {

      $info_box_contents = array();
      $info_box_contents[] = array(
                                   array('params' => $contents['params'],
                                         'text' => $contents['text']
                                        )
                                  );
      $this->commonBox($info_box_contents, true);
    }
  }
?>
