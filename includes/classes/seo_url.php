<?php
/*
//----------------------------------------------------------------------------
//-------------- SEO-G by Asymmetrics (Renegade Edition) ---------------------
//----------------------------------------------------------------------------
// Copyright (c) 2006-2008 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// URL processing Front-End class
// Processes SEO tables and urls, generates SEO links
// Mods/Features:
// - SEO URLs Generator
// - Table driven and Auto Redirection methods
// - Auto Builder for SEO URLs.
// - Multi-Layer processing facility for products, categories, etc., segments
// - Priority processing facility for products, categories, etc., segments
// - Multi separators for SEO URL segments.
// - Multi URLs Extensions Decoding.
// - Support for Products, product reviews, Categories, Manufacturers
// - Support for Articles, Topics, Authors
// - Support for Link Categories
// - Support for Information Pages Unlimitied
// - Support for Generic Text
// - Support for Abstract Zones
// - Support for Page splitter
// - Support for Popup Images
// - Support for Numeric Ranges
// - META-G adaptive methods to include/exclude link components added
// - Proximity Redirection added
// - Privacy header on redirecs added
// - Periodic URL refresh added
// - Subfixes for secondary handlers added
// - Index for osc urls added
// - Cascade Path level added
//----------------------------------------------------------------------------
// I-Metrics Layer
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
  class seoURL {
    var $path, $query, $params_array, $error_level, $handler_flag, $osc_keys_array, $osc_key;

    function seoURL() {
      global $g_seo_flag, $g_server;
      $this->path = $this->query = '';
      $this->params_array = array();
      $this->query_array = array();
      $this->osc_keys_array = array();
      $this->error_level = 0;
      if( !isset($g_seo_flag) || $g_seo_flag !== true) {
        $this->check_redirection(0, $g_server . $_SERVER['REQUEST_URI']);
      }
      $this->osc_key = '';
    }

    function create_safe_string($string, $separator=SEO_DEFAULT_WORDS_SEPARATOR, $flat=false) {
      $string = preg_replace('/\s\s+/', ' ', trim($string));
      if( $flat )
        $string = preg_replace("/[^0-9a-z]+/i", $separator, strtolower($string));
	    //$string = preg_replace("/[^0-9a-z\-_]+/i", $separator, strtolower($string));
      else
	    $string = preg_replace("/[^0-9a-z\/]+/i", $separator, strtolower($string));
	    //$string = preg_replace("/[^0-9a-z\-_\/]+/i", $separator, strtolower($string));
      $string = trim($string, $separator);
      $string = str_replace($separator . $separator . $separator, $separator, $string);
      $string = str_replace($separator . $separator, $separator, $string);
      return $string;
    }

    function create_safe_name($string, $separator=SEO_DEFAULT_WORDS_SEPARATOR) {
      $string = $this->create_safe_string($string, $separator, true);
      $words_array = explode($separator, $string);

      // Apply META-G Inclusion Dictionary
      if( defined(META_USE_LEXICO) && META_USE_LEXICO == 'true' && SEO_METAG_INCLUSION == 'true' ) {
        if( is_array($words_array) && count($words_array) ) {
          $words_array = array_unique($words_array);
          $tmp_array = array();
          foreach($words_array as $key => $value) {
            $check_query = tep_db_query("select meta_lexico_text, sort_id from " . TABLE_META_LEXICO . " where meta_lexico_text like '%" . tep_db_input(tep_db_prepare_input($value)) . "%' and meta_lexico_status='1' order by sort_id limit " . SEO_METAG_INCLUSION_LIMIT);
            if( !tep_db_num_rows($check_query) )
              continue;
            unset($words_array[$key]);
            while( $check_array = tep_db_fetch_array($check_query) ) {
              $tmp_array[$check_array['sort_id']] = $this->create_safe_string($check_array['meta_lexico_text'], $separator);
            }
          }
          $words_array = array_merge($tmp_array,$words_array);
        }
      }

      // Adapt META-G Exclusion list
      if( defined(META_USE_LEXICO) && META_USE_LEXICO == 'true' && SEO_METAG_EXCLUSION == 'true' ) {
        if( is_array($words_array) ) {
          $tmp_array = array();
          foreach($words_array as $key => $value) {
            $tmp_array[] = md5($value);
          }

          $check_query = tep_db_query("select meta_exclude_text from " . TABLE_META_EXCLUDE . " where meta_exclude_key in ('" . implode("', '", $tmp_array ) . "')");
          $words_array = array_flip($words_array);
          while( $check_array = tep_db_fetch_array($check_query) ) {
            unset($words_array[$check_array['meta_exclude_text']]);
          }
          if(count($words_array)) {
            $words_array = array_flip($words_array);
          }
        }
      }

      // Filter by Length of words
      if(SEO_DEFAULT_WORD_LENGTH > 1) {
        if( is_array($words_array) ) {
          foreach( $words_array as $key => $value ) {
            if(strlen($value) < SEO_DEFAULT_WORD_LENGTH) {
              unset($words_array[$key]);
            }
          }
        }
      }

      if( is_array($words_array) && count($words_array) ) {
        $string = implode($separator, $words_array);
      }
      return $string;
    }


    function get_script($script) {
      $check_query = tep_db_query("select seo_name from " . TABLE_SEO_TO_SCRIPTS . " where script = '" . tep_db_prepare_input(tep_db_input($script)) . "'");
      if( $check_array = tep_db_fetch_array($check_query) ) {
        $result = $check_array['seo_name'];
      } else {
        $result = $this->create_safe_string($script, SEO_DEFAULT_WORDS_SEPARATOR);
      }
      return $result;
    }

    // Get osc url from a passed seo url
    function get_osc_url($seo_url, &$url, &$url_params, &$url_parse) {
      // Validate REQUEST_URI in case we got a redirect from a server script. May needed with some servers
      $this->validate_uri($seo_url);

      $url = $url_params = $url_parse = $result = false;
      $seo_left = explode('?', $seo_url);
      if( !is_array($seo_left) ) {
        $url_parse = parse_url($seo_url);
        return $result;
      }

      $seo_script = basename($seo_left[0]);
      $seo_script = str_replace('.', '_', $seo_script);
      if( isset($_GET[$seo_script]) )
        unset($_GET[$seo_script]);

      $key = md5($seo_left[0]);

      if( !isset($seo_left[1]) )
        $seo_left[1] = '';

      $this->check_redirection($key, $seo_left[1]);

      $check_query = tep_db_query("select seo_url_get, seo_url_org from " . TABLE_SEO_URL . " where seo_url_key = '" . tep_db_input($key) . "'");
      if( $seo_array = tep_db_fetch_array($check_query) ) {

        $url = $seo_array['seo_url_org'];
        $url_parse = parse_url($url);

        if( !isset($url_parse['query']) ) {
          $url_query = '';
        } else {
          $url_query = $url_parse['query'];
        }

        $url_params = explode('&', $url_query);
        if( !is_array($url_params) || empty($url_query) ) {
          $url_params = array();
        }
        //tep_db_query("update " . TABLE_SEO_URL . " set seo_url_hits = seo_url_hits+1, last_modified=now() where seo_url_key = '" . tep_db_input($key) . "'");
        $this->osc_key = $key;
        $result = true;
      } else {
        $url_parse = parse_url($seo_url);
      }
      return $result;
    }

    // Convert osc url to an html url. Do not pass the session name/id to this function
    function get_seo_url($url, &$separator, $store=true) {

      $org_url = $url;

      if( SEO_DEFAULT_ENABLE == 'false' ) {
        return $org_url;
      }

      $key_osc = md5($url);
      if( isset($this->osc_keys_array[$key_osc]) ) {
        $separator = $this->osc_keys_array[$key_osc]['sep'];
        return $this->osc_keys_array[$key_osc]['url'];
      }

      $force_update = false;
      // Check if the url already recorded, if so skip processing
      if( SEO_CONTINUOUS_CHECK == 'false' ) {
        $check_query = tep_db_query("select seo_url_key, seo_url_get, unix_timestamp(last_modified) as last_time from " . TABLE_SEO_URL . " where osc_url_key = '" . tep_db_input(tep_db_prepare_input($key_osc)) . "'");
        if( $check_array = tep_db_fetch_array($check_query) ) {
          $diff_time = time() - $check_array['last_time'];
          if( $diff_time < SEO_PERIODIC_REFRESH ) {
            $separator = '?';
            $this->osc_keys_array[$key_osc] = array('url' => $check_array['seo_url_get'], 'sep' => $separator);
            return $check_array['seo_url_get'];
          }
          $force_update = true;
          $old_key = $check_array['seo_url_key'];
        }
      }

      if( $store !== true ) {
        return $org_url;
      }

      $seo_url = '';
      $result = $this->parse_params($url, $seo_url);

      if( !$result ) {
        $this->osc_keys_array[$key_osc] = array('url' => $org_url, 'sep' => $separator);
        return $org_url;
      }

      $key = md5($seo_url);

      // Redirection double-check. Do not build url if a redirect exists.
      if( $this->check_redirection($key, '', true) ) {
        $this->osc_keys_array[$key_osc] = array('url' => $url, 'sep' => $separator);
        return $url;
      }

      $check_query = tep_db_query("select seo_url_get, seo_url_org, osc_url_key from " . TABLE_SEO_URL . " where seo_url_key = '" . tep_db_input($key) . "' or osc_url_key = '" . tep_db_input($key_osc) . "'");
      $key_rows = tep_db_num_rows($check_query);
      if($key_rows > 1 ) {
        $force_update = true;
      }
      if( $seo_array = tep_db_fetch_array($check_query) ) {
        // Note: SEO_CONTINUOUS_CHECK switch = true, should be used for short periods of time as it significantly increases latency.
        //if( $force_update || ($seo_array['seo_url_org'] != $url && SEO_CONTINUOUS_CHECK == 'true') ) {
        if( $force_update || ($seo_array['osc_url_key'] != $key_osc || SEO_CONTINUOUS_CHECK == 'true') ) {
          if( $force_update ) {
            if( $old_key != $key || $seo_array['osc_url_key'] != $key_osc) {
              tep_db_query("delete from " . TABLE_SEO_URL . " where seo_url_key = '" . tep_db_input($old_key) . "'");
              tep_db_query("delete from " . TABLE_SEO_URL . " where osc_url_key = '" . tep_db_input($key_osc) . "'");
              tep_db_query("delete from " . TABLE_SEO_URL . " where seo_url_key = '" . tep_db_input($key) . "'");
              $this->insert_record($key, $seo_url, $key_osc, $url);
            } else {
              $sql_data_array = array(
                                      'seo_url_org' => tep_db_prepare_input($url),
                                      'last_modified' => 'now()'
                                     );
              tep_db_perform(TABLE_SEO_URL, $sql_data_array, 'update', "seo_url_key = '" . tep_db_input($key) . "'");
            }
          } else {
            tep_db_query("delete from " . TABLE_SEO_URL . " where osc_url_key = '" . tep_db_input($key_osc) . "'");
            tep_db_query("delete from " . TABLE_SEO_URL . " where seo_url_key = '" . tep_db_input($key) . "'");
            $this->insert_record($key, $seo_url, $key_osc, $url);
          }
        }
      } else {
        $this->insert_record($key, $seo_url, $key_osc, $url);
      }
      $separator = '?';
      $this->osc_keys_array[$key_osc] = array('url' => $seo_url, 'sep' => $separator);
      return $seo_url;
    }


    function insert_record($key, $seo_url, $key_osc, $url) {
      $check_query = tep_db_query("select seo_url_key from " . TABLE_SEO_URL . " where seo_url_key = '" . tep_db_input($key) . "'");
      if( !tep_db_num_rows($check_query) ) {
        $sql_data_array = array(
                                'seo_url_key' => tep_db_prepare_input($key),
                                'seo_url_get' => tep_db_prepare_input($seo_url),
                                'osc_url_key' => tep_db_prepare_input($key_osc),
                                'seo_url_org' => tep_db_prepare_input($url),
                                'date_added' => 'now()',
                                'last_modified' => 'now()'
                               );
        tep_db_perform_insert(TABLE_SEO_URL, $sql_data_array);
      } else {
        $sql_data_array = array(
                                'seo_url_get' => tep_db_prepare_input($seo_url),
                                'osc_url_key' => tep_db_prepare_input($key_osc),
                                'seo_url_org' => tep_db_prepare_input($url),
                                'last_modified' => 'now()'
                               );
        tep_db_perform(TABLE_SEO_URL, $sql_data_array, 'update', "seo_url_key = '" . tep_db_input($key) . "'");
      }
    }

    function parse_params(&$url, &$seo_url) {
      $this->error_level = 0;
      $result = false;
      $seo_url = '';
      $url = trim($url, '&');
      $seo_array = parse_url($url);

      // Validate result
      if( !is_array($seo_array) || !isset($seo_array['path']) ) {
        return $result;
      }

      $this->path = basename($seo_array['path']);

      // Process the query part.
      $query = isset($seo_array['query'])?$seo_array['query']:'';

      if( tep_not_null($query) ) {
        $query = htmlspecialchars(urldecode($query));
        $query = str_replace('&amp;', '&', $query);
      }
      $this->query = $query;

      // Check exclusion list scripts and parameters
      if( $this->exclude_script() ) {
        return $result;
      }

      // Store original query
      $osc_query = $query;
      $fragment = isset($seo_array['fragment'])?$seo_array['fragment']:'';
      $osc_path = $path = $seo_array['path'];

      if( tep_not_null($query) ) {
        if( count($this->params_array) ) {
          $other = false;
          $result = $this->translate_params($other, $query);
          // Check if safe mode is on and unknown parameters were detected, in which case abort.
          if( $other && SEO_DEFAULT_SAFE_MODE == 'true') {
            return false;
          }
          if($result == 2) {
            $this->error_level = 2;
            return false;
          }
        }

        $query = $this->create_safe_string($query, SEO_DEFAULT_PARTS_SEPARATOR);
      }

      if( tep_not_null($fragment) ) {
        $fragment = SEO_DEFAULT_PARTS_SEPARATOR . $fragment;
      }
      if( tep_not_null($path) ) {
        if( tep_not_null($query) || tep_not_null($fragment) ) {
          if($result == 1) {
            $tmp_array = explode('/', $path);
            $count = is_array($tmp_array)?count($tmp_array):0;
            if( $count ) {
              unset($tmp_array[$count-1]);
              $path = implode('/', $tmp_array);
            } else {
              $path = '';
            }
            $path .= '/';
          } else {
            $path = str_replace('.php', SEO_DEFAULT_INNER_SEPARATOR, $path);
          }
        } else {
          $path = str_replace('.php', '', $path);
        }
      }
////////////// added
      $tmp_array = explode('/', $path);
      $count = is_array($tmp_array)?count($tmp_array):0;
      if( $count > 1 && tep_not_null($tmp_array[$count-1]) ) {
        //$tmp_array[$count-1] = $this->create_safe_string($tmp_array[$count-1], SEO_DEFAULT_WORDS_SEPARATOR);
        $tmp_array[$count-1] = $this->get_script($tmp_array[$count-1]);
        $path = implode('/', $tmp_array);
      }
/////////////

      if( tep_not_null($osc_query) ) {
        $this->eliminate_session();
        if( count($this->params_array) ) {
          $osc_query = '?' . implode('&', $this->params_array);
        } else {
          $osc_query = '';
        }
      }
      $url = $seo_array['scheme'] . '://' .  $seo_array['host'] . $osc_path . $osc_query;
      $ext_array = explode(',', SEO_DEFAULT_EXTENSION);
      if( !is_array($ext_array) ) {
        $ext_array = array('.html');
      }
      $seo_url = $seo_array['scheme'] . '://' .  $seo_array['host'] . $path . $query . $fragment . $ext_array[0];
      $seo_url = str_replace('___', '-', $seo_url);
      return true;
    }

    // Convert supported url parameters
    function translate_params(&$other, &$query) {
      global $g_filters;

      $this->handler_flag = $other = false;
      $result = 0;
      $flags_array = array('other' => false);
      $seo_params_array = array();
      $params_array = array();
      $array_and = $this->params_array;
      foreach ($array_and as $key => $value) {
        $inner = explode('=', $value);
        if( !is_array($inner) || count($inner) != 2) {
          if( SEO_STRICT_VALIDATION == 'false' ) {
            $this->assign_default($params_array, $value);
          }
          $flags_array['other'] = true;
          continue;
        }
        // No Sessions should ever passed to this class and this is going to be enforced.
        if( $inner[0] == tep_session_name() ) {
          continue;
        }

        switch($inner[0]) {
          //case 'pID':
          case 'products_id':
            if( isset($flags_array['products_id']) || !tep_not_null($inner[1]) || $inner[1] == '0') break;
            if( !is_numeric($inner[1]) ) {
              return 2;
            }

            $this->auto_builder($inner[0], $inner[1]);
            $params_query_raw = "select s2p.seo_name, st.sort_order, st.seo_types_linkage, st.seo_types_prefix, st.seo_types_handler, st.seo_types_subfix from " . TABLE_SEO_TO_PRODUCTS . " s2p, " . TABLE_SEO_TYPES . " st where st.seo_types_class='seo_products' and st.seo_types_status='1' and s2p.products_id = '" . (int)$inner[1] . "'";
            if( !$this->set_id($params_query_raw, $seo_params_array) ) {
              $this->assign_default($params_array, $value);
            }
            $flags_array['products_id'] = $inner[1];
            break;
          case 'cPath':
            if( isset($flags_array['cpath']) || !tep_not_null($inner[1]) || $inner[1] == '0') break;
            $path_flag = false;
            $path_link = explode('_', $inner[1]);
            $max_index = count($path_link);
            $preserve = SEO_PATH_CASCADE;
            if( $preserve < 1 ) $preserve = 1;

            if( $max_index > $preserve ) {
              $path_link = array_slice($path_link, $max_index-$preserve);
            }

            if( SEO_PATH_REVERSE == 'true' ) {
              $path_link = array_reverse($path_link);
            }

            $tmp_array = array();
            $depth = 0;
            $sort_order = 0;
            foreach ($path_link as $key2 => $value2 ) {
              if(!$value2) continue;
              if( !is_numeric($value2) ) {
                return 2;
              }
              $this->auto_builder($inner[0], $value2);
              $params_query_raw = "select s2c.seo_name, st.sort_order, st.seo_types_linkage, st.seo_types_prefix, st.seo_types_handler, st.seo_types_subfix from " . TABLE_SEO_TO_CATEGORIES . " s2c, " . TABLE_SEO_TYPES . " st where st.seo_types_class='seo_categories' and st.seo_types_status='1' and s2c.categories_id = '" . (int)$value2 . "'";
              $path_flag = $this->set_path($params_query_raw, $tmp_array, $depth, $sort_order);
              if( !$path_flag ) {
                break;
              }
            }
            if( $path_flag ) {
              $final_path = implode(SEO_DEFAULT_INNER_SEPARATOR, $tmp_array);
              $seo_params_array = array_merge( array($final_path => $sort_order), $seo_params_array);
            } else {
              $this->assign_default($params_array, $value);
            }
            $flags_array['cpath'] = $inner[1];
            break;
          case 'manufacturers_id':
            if( isset($flags_array['manufacturers_id']) || !tep_not_null($inner[1]) || $inner[1] == '0') break;
            if( !is_numeric($inner[1]) ) {
              return 2;
            }
            
            $this->auto_builder($inner[0], $inner[1]);
            $params_query_raw = "select s2m.seo_name, st.sort_order, st.seo_types_linkage, st.seo_types_prefix, st.seo_types_handler, st.seo_types_subfix from " . TABLE_SEO_TO_MANUFACTURERS . " s2m, " . TABLE_SEO_TYPES . " st where st.seo_types_class='seo_manufacturers' and st.seo_types_status='1' and s2m.manufacturers_id = '" . (int)$inner[1] . "'";
            if( !$this->set_id($params_query_raw, $seo_params_array) ) {
              $this->assign_default($params_array, $value);
            }
            $flags_array['manufacturers_id'] = $inner[1];
            break;

          case 'gtext_id':
            if( isset($flags_array['gtext_id']) || !tep_not_null($inner[1]) || $inner[1] == '0' ) break;
            if( !is_numeric($inner[1]) ) {
              return 2;
            }
            $this->auto_builder($inner[0], $inner[1]);
            $params_query_raw = "select s2g.seo_name, st.sort_order, st.seo_types_linkage, st.seo_types_prefix, st.seo_types_handler, st.seo_types_subfix from " . TABLE_SEO_TO_GTEXT . " s2g, " . TABLE_SEO_TYPES . " st where st.seo_types_class='seo_gtext' and st.seo_types_status='1' and s2g.gtext_id = '" . (int)$inner[1] . "'";
            if( !$this->set_id($params_query_raw, $seo_params_array) ) {
              $this->assign_default($params_array, $value);
            }
            $flags_array['gtext_id'] = $inner[1];
            break;
          case 'range_id':
            if( isset($flags_array['range_id']) || !tep_not_null($inner[1]) || $inner[1] == '0') break;
            if( !is_numeric($inner[1]) ) {
              return 2;
            }
            $this->auto_builder($inner[0], $inner[1]);
            $params_query_raw = "select s2r.seo_name, st.sort_order, st.seo_types_linkage, st.seo_types_prefix, st.seo_types_handler, st.seo_types_subfix from " . TABLE_SEO_TO_RANGES . " s2r, " . TABLE_SEO_TYPES . " st where st.seo_types_class='seo_ranges' and st.seo_types_status='1' and s2r.numeric_ranges_id = '" . (int)$inner[1] . "'";
            if( !$this->set_id($params_query_raw, $seo_params_array) ) {
              $this->assign_default($params_array, $value);
            }
            $flags_array['range_id'] = $inner[1];
            break;
          case 'abz_id':
            if( isset($flags_array['abz_id']) || !tep_not_null($inner[1]) || $inner[1] == '0') break;
            if( !is_numeric($inner[1]) ) {
              return 2;
            }
            $this->auto_builder($inner[0], $inner[1]);
            $params_query_raw = "select s2az.seo_name, st.sort_order, st.seo_types_linkage, st.seo_types_prefix, st.seo_types_handler, st.seo_types_subfix from " . TABLE_SEO_TO_ABSTRACT . " s2az, " . TABLE_SEO_TYPES . " st where st.seo_types_class='seo_abstract' and st.seo_types_status='1' and s2az.abstract_zone_id = '" . (int)$inner[1] . "'";
            if( !$this->set_id($params_query_raw, $seo_params_array) ) {
              $this->assign_default($params_array, $value);
            }
            $flags_array['abz_id'] = $inner[1];
            break;

          case 'auctions_id':
            if( isset($flags_array['auctions_id']) || !tep_not_null($inner[1]) || $inner[1] == '0') break;
            if( !is_numeric($inner[1]) ) {
              return 2;
            }
            $this->auto_builder($inner[0], $inner[1]);
            $params_query_raw = "select s2a.seo_name, st.sort_order, st.seo_types_linkage, st.seo_types_prefix, st.seo_types_handler, st.seo_types_subfix from " . TABLE_SEO_TO_AUCTIONS . " s2a, " . TABLE_SEO_TYPES . " st where st.seo_types_class='seo_auctions' and st.seo_types_status='1' and s2a.auctions_id = '" . (int)$inner[1] . "'";
            if( !$this->set_id($params_query_raw, $seo_params_array) ) {
              $this->assign_default($params_array, $value);
            }
            $flags_array['auctions_group_id'] = $inner[1];
            break;

          case 'auctions_group_id':
            if( isset($flags_array['auctions_group_id']) || !tep_not_null($inner[1]) || $inner[1] == '0') break;
            if( !is_numeric($inner[1]) ) {
              return 2;
            }
            $this->auto_builder($inner[0], $inner[1]);
            $params_query_raw = "select s2ag.seo_name, st.sort_order, st.seo_types_linkage, st.seo_types_prefix, st.seo_types_handler, st.seo_types_subfix from " . TABLE_SEO_TO_AUCTIONS_GROUP . " s2ag, " . TABLE_SEO_TYPES . " st where st.seo_types_class='seo_auctions_group' and st.seo_types_status='1' and s2ag.auctions_group_id = '" . (int)$inner[1] . "'";
            if( !$this->set_id($params_query_raw, $seo_params_array) ) {
              $this->assign_default($params_array, $value);
            }
            $flags_array['auctions_group_id'] = $inner[1];
            break;

          case 'page':
            if( isset($flags_array['page']) || !tep_not_null($inner[1]) || $inner[1] == '0') break;
            if( !is_numeric($inner[1]) ) {
              return 2;
            }
            $handler = '';
            if( !$this->handler_flag && count($flags_array) == 1 ) {
              $tmp_string = str_replace('.php', '', $this->path);
              $tmp_string = $this->get_script($tmp_string);
              $handler = $tmp_string . SEO_DEFAULT_INNER_SEPARATOR;

              //$handler = str_replace('.php', SEO_DEFAULT_INNER_SEPARATOR, $this->path);
              $this->handler_flag = true;
            }
            $seo_params_array[$handler . 'p' . $inner[1]] = '99' . '_' . '-1';
            $flags_array['page'] = $inner[1];
            break;

/*
          case 'reviews_id':
            if( isset($flags_array['reviews_id']) || !tep_not_null($inner[1]) || $inner[1] == '0') break;
            if( !is_numeric($inner[1]) ) {
              return 2;
            }
            $handler = '';
            if( !$this->handler_flag && count($flags_array) == 1 ) {
              $handler = str_replace('.php', SEO_DEFAULT_INNER_SEPARATOR, $this->path);
              $this->handler_flag = true;
            }
            $seo_params_array[$handler . 'r' . $inner[1]] = '98' . '_' . '-1';
            $flags_array['reviews_id'] = $inner[1];
            break;

//-MS- Use only if the articles manager is fully installed
          case 'articles_id':
            if( isset($flags_array['articles_id']) || !tep_not_null($inner[1]) ) break;
            if( !is_numeric($inner[1]) ) {
              return 2;
            }
            $this->auto_builder($inner[0], $inner[1]);
            $params_query_raw = "select s2a.seo_name, st.sort_order, st.seo_types_linkage, st.seo_types_prefix, st.seo_types_handler, st.seo_types_subfix from " . TABLE_SEO_TO_ARTICLES . " s2a left join " . TABLE_SEO_TYPES . " st on (s2a.seo_types_id=st.seo_types_id) where st.seo_types_status='1' and s2a.articles_id = '" . (int)$inner[1] . "'";
            if( !$this->set_id($params_query_raw, $seo_params_array) ) {
              $this->assign_default($params_array, $value);
            }
            $flags_array['articles_id'] = $inner[1];
            break;
          case 'authors_id':
            if( isset($flags_array['authors_id']) || !tep_not_null($inner[1]) ) break;
            if( !is_numeric($inner[1]) ) {
              return 2;
            }
            $this->auto_builder($inner[0], $inner[1]);
            $params_query_raw = "select s2a.seo_name, st.sort_order, st.seo_types_linkage, st.seo_types_prefix, st.seo_types_handler, st.seo_types_subfix from " . TABLE_SEO_TO_AUTHORS . " s2a left join " . TABLE_SEO_TYPES . " st on (s2a.seo_types_id=st.seo_types_id) where st.seo_types_status='1' and s2a.authors_id = '" . (int)$inner[1] . "'";
            if( !$this->set_id($params_query_raw, $seo_params_array) ) {
              $this->assign_default($params_array, $value);
            }
            $flags_array['authors_id'] = $inner[1];
            break;
          case 'tPath':
            if( isset($flags_array['tpath']) || !tep_not_null($inner[1]) || $inner[1] == '0' ) break;
            $path_link = explode('_', $inner[1]);
            $tmp_array = array();
            $depth = 0;
            $sort_order = 0;
            foreach ($path_link as $key2 => $value2 ) {
              if(!$value2) continue;
              if( !is_numeric($value2) ) {
                return 2;
              }
              $this->auto_builder($inner[0], $value2);
              $params_query_raw = "select s2t.seo_name, st.sort_order, st.seo_types_linkage, st.seo_types_prefix, st.seo_types_handler, st.seo_types_subfix from " . TABLE_SEO_TO_TOPICS . " s2t left join " . TABLE_SEO_TYPES . " st on (s2t.seo_types_id=st.seo_types_id) where st.seo_types_status='1' and s2t.topics_id = '" . (int)$value2 . "'";
              $path_flag = $this->set_path($params_query_raw, $tmp_array, $depth, $sort_order);
              if( !$path_flag ) {
                break;
              }
            }
            if( $path_flag ) {
              $final_path = implode(SEO_DEFAULT_INNER_SEPARATOR, $tmp_array);
              $seo_params_array = array_merge( array($final_path => $sort_order), $seo_params_array);
            } else {
              $this->assign_default($params_array, $value);
            }
            $flags_array['tpath'] = $inner[1];
            break;
//-MS- Use only if the articles manager is fully installed EOM

//-MS- Use only if the information pages unlimited is fully installed
          case 'info_id':
            if( isset($flags_array['info_id']) || !tep_not_null($inner[1]) ) break;
            if( !is_numeric($inner[1]) ) {
              return 2;
            }
            $this->auto_builder($inner[0], $inner[1]);
            $params_query_raw = "select s2i.seo_name, st.sort_order, st.seo_types_linkage, st.seo_types_prefix, st.seo_types_handler, st.seo_types_subfix from " . TABLE_SEO_TO_INFORMATION . " s2i left join " . TABLE_SEO_TYPES . " st on (s2i.seo_types_id=st.seo_types_id) where st.seo_types_status='1' and s2i.information_id = '" . (int)$inner[1] . "'";
            if( !$this->set_id($params_query_raw, $seo_params_array) ) {
              $this->assign_default($params_array, $value);
            }
            $flags_array['info_id'] = $inner[1];
            break;
//-MS- Use only if the information pages unlimited is fully installed EOM

//-MS- Use only if the Links Manager is fully installed
          case 'lPath':
            if( isset($flags_array['lpath']) || !tep_not_null($inner[1]) ) break;
            if( !is_numeric($inner[1]) ) {
              return 2;
            }
            $this->auto_builder($inner[0], $inner[1]);
            $params_query_raw = "select s2l.seo_name, st.sort_order, st.seo_types_linkage, st.seo_types_prefix, st.seo_types_handler, st.seo_types_subfix from " . TABLE_SEO_TO_LINKS . " s2l left join " . TABLE_SEO_TYPES . " st on (s2l.seo_types_id=st.seo_types_id) where st.seo_types_status='1' and s2l.link_categories_id = '" . (int)$inner[1] . "'";
            if( !$this->set_id($params_query_raw, $seo_params_array) ) {
              $this->assign_default($params_array, $value);
            }
            $flags_array['info_id'] = $inner[1];
            break;
//-MS- Use only if the Links Manager is fully installed EOM
*/
          default:
            $process_flag = false;
            if( isset($g_filters) && is_object($g_filters) ) { 
              $select_id = $g_filters->validate_option($inner[0], $inner[1]);
              if( $select_id > 0 ) {
                $this->auto_builder($inner[0], $inner[1], 'filter');
                $filter_id = $g_filters->parameter_array[$inner[0]];
                $params_query_raw = "select s2f.seo_name, st.sort_order, st.seo_types_linkage, st.seo_types_prefix, st.seo_types_handler, st.seo_types_subfix from " . TABLE_SEO_TO_FILTERS . " s2f, " . TABLE_SEO_TYPES . " st where st.seo_types_class='seo_filters' and st.seo_types_status='1' and s2f.products_filter_id = '" . (int)$filter_id . "' and s2f.products_select_id = '" . (int)$inner[1] . "'";
                if( !$this->set_id($params_query_raw, $seo_params_array) ) {
                  $this->assign_default($params_array, $value);
                }
                $flags_array[$inner[0]] = $inner[1];
                $process_flag = true;
              }
            }
            if( !$process_flag ) {
              $this->assign_default($params_array, $value);
              $flags_array['other'] = true;
            }
            break;
        }
      }
      if( count($seo_params_array) ) {
        $this->resolve_linkage($seo_params_array);
        asort($seo_params_array, SORT_NUMERIC);
        $seo_params_array = array_keys($seo_params_array);
        $params_array = array_merge($seo_params_array, $params_array);
        $result = 1;
      }
      $query = implode('&', $params_array);
      $other = $flags_array['other'];
      return $result;
    }

    function resolve_linkage(&$seo_params_array) {
      $tmp_array = array();
      foreach($seo_params_array as $key => $value) {
        list($sort, $link) = preg_split("/_/", $value, 2);
        $seo_params_array[$key] = $sort;
        $tmp_array[$key] = $link;
      }
      asort($tmp_array, SORT_NUMERIC);
      foreach($tmp_array as $key => $value) {
        if( $value < 0 )
          continue;

        if( !isset($reduce) ) {
          $reduce = $value;
          continue;
        }
        if($reduce != $value) {
          unset($seo_params_array[$key]);
        }
      }
    }

    function auto_builder($entity, $id, $extra='none') {
      global $g_filters;

      if( SEO_AUTO_BUILDER == 'false' )
        return;

      switch($entity) {
        case 'products_id':
          $check_query = tep_db_query("select products_id from " . TABLE_SEO_TO_PRODUCTS . " where products_id = '" . (int)$id . "'");
          if( tep_db_num_rows($check_query) ) 
            return;
          $name_query = tep_db_query("select products_name as name from " . TABLE_PRODUCTS_DESCRIPTION . " where products_id = '" . (int)$id . "' and language_id = '1'");
          if( $names_array = tep_db_fetch_array($name_query) ) {
            $types_query = tep_db_query("select seo_types_id from " . TABLE_SEO_TYPES . " where seo_types_class = 'seo_products' and seo_types_status='1'");
            if( $types_array = tep_db_fetch_array($types_query) ) {
              $seo_name = $this->create_safe_name($names_array['name']);
              $sql_data_array = array(
                                      'products_id' => (int)$id,
                                      'seo_name' => tep_db_prepare_input($seo_name),
                                      );
              tep_db_perform_insert(TABLE_SEO_TO_PRODUCTS, $sql_data_array);
            }
          }
          break;
        case 'cPath':
          $check_query = tep_db_query("select categories_id from " . TABLE_SEO_TO_CATEGORIES . " where categories_id = '" . (int)$id . "'");
          if( tep_db_num_rows($check_query) ) 
            return;
          $name_query = tep_db_query("select categories_name as name from " . TABLE_CATEGORIES_DESCRIPTION . " where categories_id = '" . (int)$id . "' and language_id = '1'");
          if( $names_array = tep_db_fetch_array($name_query) ) {
            $types_query = tep_db_query("select seo_types_id from " . TABLE_SEO_TYPES . " where seo_types_class = 'seo_categories' and seo_types_status='1'");
            if( $types_array = tep_db_fetch_array($types_query) ) {
              $seo_name = $this->create_safe_name($names_array['name']);
              $sql_data_array = array(
                                      'categories_id' => (int)$id,
                                      'seo_name' => tep_db_prepare_input($seo_name),
                                      );
              tep_db_perform_insert(TABLE_SEO_TO_CATEGORIES, $sql_data_array);
            }
          }
          break;
        case 'manufacturers_id':
          $check_query = tep_db_query("select manufacturers_id from " . TABLE_SEO_TO_MANUFACTURERS . " where manufacturers_id = '" . (int)$id . "'");
          if( tep_db_num_rows($check_query) ) 
            return;
          $name_query = tep_db_query("select manufacturers_name as name from " . TABLE_MANUFACTURERS . " where manufacturers_id = '" . (int)$id . "'");
          if( $names_array = tep_db_fetch_array($name_query) ) {
            $types_query = tep_db_query("select seo_types_id from " . TABLE_SEO_TYPES . " where seo_types_class = 'seo_manufacturers' and seo_types_status='1'");
            if( $types_array = tep_db_fetch_array($types_query) ) {
              $seo_name = $this->create_safe_name($names_array['name']);
              $sql_data_array = array(
                                      'manufacturers_id' => (int)$id,
                                      'seo_name' => tep_db_prepare_input($seo_name),
                                      );
              tep_db_perform_insert(TABLE_SEO_TO_MANUFACTURERS, $sql_data_array);
            }
          }
          break;

        case 'gtext_id':
          $check_query = tep_db_query("select gtext_id from " . TABLE_SEO_TO_GTEXT . " where gtext_id = '" . (int)$id . "'");
          if( tep_db_num_rows($check_query) ) 
            return;
          $name_query = tep_db_query("select gtext_title as name from " . TABLE_GTEXT . " where gtext_id = '" . (int)$id . "'");
          if( $names_array = tep_db_fetch_array($name_query) ) {
            $types_query = tep_db_query("select seo_types_id from " . TABLE_SEO_TYPES . " where seo_types_class = 'seo_gtext' and seo_types_status='1'");
            if( $types_array = tep_db_fetch_array($types_query) ) {
              $seo_name = $this->create_safe_name($names_array['name']);
              $sql_data_array = array(
                                      'gtext_id' => (int)$id,
                                      'seo_name' => tep_db_prepare_input($seo_name),
                                      );
              tep_db_perform_insert(TABLE_SEO_TO_GTEXT, $sql_data_array);
            }
          }
          break;
        case 'range_id':
          $check_query = tep_db_query("select numeric_ranges_id from " . TABLE_SEO_TO_RANGES . " where numeric_ranges_id = '" . (int)$id . "'");
          if( tep_db_num_rows($check_query) ) 
            return;
          $name_query = tep_db_query("select numeric_ranges_desc as name from " . TABLE_NUMERIC_RANGES . " where numeric_ranges_id = '" . (int)$id . "'");
          if( $names_array = tep_db_fetch_array($name_query) ) {
            $types_query = tep_db_query("select seo_types_id from " . TABLE_SEO_TYPES . " where seo_types_class = 'seo_ranges' and seo_types_status='1'");
            if( $types_array = tep_db_fetch_array($types_query) ) {
              $seo_name = $this->create_safe_name($names_array['name']);
              $sql_data_array = array(
                                      'numeric_ranges_id' => (int)$id,
                                      'seo_name' => tep_db_prepare_input($seo_name),
                                      );
              tep_db_perform_insert(TABLE_SEO_TO_RANGES, $sql_data_array);
            }
          }
          break;
        case 'abz_id':
          $check_query = tep_db_query("select abstract_zone_id from " . TABLE_SEO_TO_ABSTRACT . " where abstract_zone_id = '" . (int)$id . "'");
          if( tep_db_num_rows($check_query) ) 
            return;
          $name_query = tep_db_query("select abstract_zone_name as name from " . TABLE_ABSTRACT_ZONES . " where abstract_zone_id = '" . (int)$id . "'");
          if( $names_array = tep_db_fetch_array($name_query) ) {
            $types_query = tep_db_query("select seo_types_id from " . TABLE_SEO_TYPES . " where seo_types_class = 'seo_abstract' and seo_types_status='1'");
            if( $types_array = tep_db_fetch_array($types_query) ) {
              $seo_name = $this->create_safe_name($names_array['name']);
              $sql_data_array = array(
                                      'abstract_zone_id' => (int)$id,
                                      'seo_name' => tep_db_prepare_input($seo_name),
                                      );
              tep_db_perform_insert(TABLE_SEO_TO_ABSTRACT, $sql_data_array);
            }
          }
          break;

        case 'auctions_group_id':
          $check_query = tep_db_query("select auctions_group_id from " . TABLE_SEO_TO_AUCTIONS_GROUP . " where auctions_group_id = '" . (int)$id . "'");
          if( tep_db_num_rows($check_query) ) 
            return;
          $name_query = tep_db_query("select auctions_group_name as name from " . TABLE_AUCTIONS_GROUP . " where auctions_group_id = '" . (int)$id . "'");
          if( $names_array = tep_db_fetch_array($name_query) ) {
            $types_query = tep_db_query("select seo_types_id from " . TABLE_SEO_TYPES . " where seo_types_class = 'seo_auctions_group' and seo_types_status='1'");
            if( $types_array = tep_db_fetch_array($types_query) ) {
              $seo_name = $this->create_safe_name($names_array['name']);
              $sql_data_array = array(
                'auctions_group_id' => (int)$id,
                'seo_name' => tep_db_prepare_input($seo_name),
              );
              tep_db_perform_insert(TABLE_SEO_TO_AUCTIONS_GROUP, $sql_data_array);
            }
          }
          break;

        case 'auctions_id':
          $check_query = tep_db_query("select auctions_id from " . TABLE_SEO_TO_AUCTIONS . " where auctions_id = '" . (int)$id . "'");
          if( tep_db_num_rows($check_query) ) 
            return;
          $name_query = tep_db_query("select auctions_name as name from " . TABLE_AUCTIONS . " where auctions_id = '" . (int)$id . "'");
          if( $names_array = tep_db_fetch_array($name_query) ) {
            $types_query = tep_db_query("select seo_types_id from " . TABLE_SEO_TYPES . " where seo_types_class = 'seo_auctions' and seo_types_status='1'");
            if( $types_array = tep_db_fetch_array($types_query) ) {
              $seo_name = $this->create_safe_name($names_array['name']);
              $sql_data_array = array(
                'auctions_id' => (int)$id,
                'seo_name' => tep_db_prepare_input($seo_name),
              );
              tep_db_perform_insert(TABLE_SEO_TO_AUCTIONS, $sql_data_array);
            }
          }
          break;
/*
//-MS- Use only if the articles manager is fully installed
        case 'articles_id':
          $check_query = tep_db_query("select articles_id from " . TABLE_SEO_TO_ARTICLES . " where articles_id = '" . (int)$id . "'");
          if( tep_db_num_rows($check_query) ) 
            return;
          $name_query = tep_db_query("select articles_name as name from " . TABLE_ARTICLES_DESCRIPTION . " where articles_id = '" . (int)$id . "' and language_id = '1'");
          if( $names_array = tep_db_fetch_array($name_query) ) {
            $types_query = tep_db_query("select seo_types_id from " . TABLE_SEO_TYPES . " where seo_types_class = 'seo_articles' and seo_types_status='1'");
            if( $types_array = tep_db_fetch_array($types_query) ) {
              $seo_name = $this->create_safe_name($names_array['name']);
              $sql_data_array = array(
                                      'seo_types_id' => (int)$types_array['seo_types_id'],
                                      'articles_id' => (int)$id,
                                      'seo_name' => tep_db_prepare_input($seo_name),
                                      );
              tep_db_perform_insert(TABLE_SEO_TO_ARTICLES, $sql_data_array);
            }
          }
          break;
        case 'authors_id':
          $check_query = tep_db_query("select authors_id from " . TABLE_SEO_TO_AUTHORS . " where authors_id = '" . (int)$id . "'");
          if( tep_db_num_rows($check_query) ) 
            return;
          $name_query = tep_db_query("select authors_name as name from " . TABLE_AUTHORS . " where authors_id = '" . (int)$id . "'");
          if( $names_array = tep_db_fetch_array($name_query) ) {
            $types_query = tep_db_query("select seo_types_id from " . TABLE_SEO_TYPES . " where seo_types_class = 'seo_authors' and seo_types_status='1'");
            if( $types_array = tep_db_fetch_array($types_query) ) {
              $seo_name = $this->create_safe_name($names_array['name']);
              $sql_data_array = array(
                                      'seo_types_id' => (int)$types_array['seo_types_id'],
                                      'authors_id' => (int)$id,
                                      'seo_name' => tep_db_prepare_input($seo_name),
                                      );
              tep_db_perform_insert(TABLE_SEO_TO_AUTHORS, $sql_data_array);
            }
          }
          break;
        case 'tPath':
          $check_query = tep_db_query("select topics_id from " . TABLE_SEO_TO_TOPICS . " where topics_id = '" . (int)$id . "'");
          if( tep_db_num_rows($check_query) ) 
            return;
          $name_query = tep_db_query("select topcis_name as name from " . TABLE_TOPICS_DESCRIPTION . " where topics_id = '" . (int)$id . "' and language_id = '1'");
          if( $names_array = tep_db_fetch_array($name_query) ) {
            $types_query = tep_db_query("select seo_types_id from " . TABLE_SEO_TYPES . " where seo_types_class = 'seo_topics' and seo_types_status='1'");
            if( $types_array = tep_db_fetch_array($types_query) ) {
              $seo_name = $this->create_safe_name($names_array['name']);
              $sql_data_array = array(
                                      'seo_types_id' => (int)$types_array['seo_types_id'],
                                      'topics_id' => (int)$id,
                                      'seo_name' => tep_db_prepare_input($seo_name),
                                      );
              tep_db_perform_insert(TABLE_SEO_TO_TOPICS, $sql_data_array);
            }
          }
          break;
//-MS- Use only if the articles manager is fully installed EOM

//-MS- Use only if the information pages unlimited is fully installed
        case 'info_id':
          $check_query = tep_db_query("select information_id from " . TABLE_SEO_TO_INFORMATION . " where information_id = '" . (int)$id . "'");
          if( tep_db_num_rows($check_query) ) 
            return;
          $name_query = tep_db_query("select information_title as name from " . TABLE_INFORMATION . " where information_id = '" . (int)$id . "' and language_id = '1'");
          if( $names_array = tep_db_fetch_array($name_query) ) {
            $types_query = tep_db_query("select seo_types_id from " . TABLE_SEO_TYPES . " where seo_types_class = 'seo_information' and seo_types_status='1'");
            if( $types_array = tep_db_fetch_array($types_query) ) {
              $seo_name = $this->create_safe_name($names_array['name']);
              $sql_data_array = array(
                                      'seo_types_id' => (int)$types_array['seo_types_id'],
                                      'information_id' => (int)$id,
                                      'seo_name' => tep_db_prepare_input($seo_name),
                                      );
              tep_db_perform_insert(TABLE_SEO_TO_INFORMATION, $sql_data_array);
            }
          }
          break;
//-MS- Use only if the information pages unlimited is fully installed EOM

//-MS- Use only if the Links Manager is fully installed
        case 'lPath':
          $check_query = tep_db_query("select link_categories_id from " . TABLE_SEO_TO_LINKS . " where link_categories_id = '" . (int)$id . "'");
          if( tep_db_num_rows($check_query) ) 
            return;
          $name_query = tep_db_query("select link_categories_name as name from " . TABLE_LINK_CATEGORIES_DESCRIPTION . " where link_categories_id = '" . (int)$id . "' and language_id = '1'");
          if( $names_array = tep_db_fetch_array($name_query) ) {
            $types_query = tep_db_query("select seo_types_id from " . TABLE_SEO_TYPES . " where seo_types_class = 'seo_links' and seo_types_status='1'");
            if( $types_array = tep_db_fetch_array($types_query) ) {
              $seo_name = $this->create_safe_name($names_array['name']);
              $sql_data_array = array(
                                      'seo_types_id' => (int)$types_array['seo_types_id'],
                                      'link_categories_id' => (int)$id,
                                      'seo_name' => tep_db_prepare_input($seo_name),
                                      );
              tep_db_perform_insert(TABLE_SEO_TO_LINKS, $sql_data_array);
            }
          }
          break;
//-MS- Use only if the Links Manager is fully installed EOM
*/
        default:
          if( $extra == 'filter' && isset($g_filters) && is_object($g_filters) ) { 
            $filter_id = $g_filters->parameter_array[$entity];
            $check_query = tep_db_query("select seo_name from " . TABLE_SEO_TO_FILTERS . " where products_filter_id = '" . (int)$filter_id . "' and products_select_id = '" . (int)$id . "'");
            if( tep_db_num_rows($check_query) ) 
              return;

            $name1 = $g_filters->get_filters_name($filter_id);
            $name2 = $g_filters->get_option_name($filter_id, $id);
            if( tep_not_null($name1) && tep_not_null($name2) ) {
              //$seo_name = $name1 . ' ' . $name2;
              $seo_name = $name2;
              $types_query = tep_db_query("select seo_types_id from " . TABLE_SEO_TYPES . " where seo_types_class = 'seo_filters' and seo_types_status='1'");
              if( $types_array = tep_db_fetch_array($types_query) ) {
                $seo_name = $this->create_safe_name($seo_name);
                $sql_data_array = array(
                                        'products_filter_id' => (int)$filter_id,
                                        'products_select_id' => (int)$id,
                                        'seo_name' => tep_db_prepare_input($seo_name),
                                        );
                tep_db_perform_insert(TABLE_SEO_TO_FILTERS, $sql_data_array);
              }
            }
          }
          break;
      }
    }

    function set_id($query_raw, &$seo_params_array) {
      $result = $handler = false;
      $params_query = tep_db_query($query_raw);
      if( $entry = tep_db_fetch_array($params_query) ) {
        if( tep_not_null($entry['seo_types_subfix']) ) {
          $handler_array = explode(',', $entry['seo_types_handler']);
          $subfix_array = explode(',', $entry['seo_types_subfix']);
          foreach($handler_array as $key => $value ) {
            $value = trim($value);
            if( $this->path == $value ) {
              if( isset($subfix_array[$key]) ) {
                $handler = $subfix_array[$key];
              } else {
                $handler = $value;
                $handler = str_replace('.php', SEO_DEFAULT_INNER_SEPARATOR, $handler);
              }
              break;
            }
          }
        }
        if( $handler && !$this->handler_flag) {
          //$handler = str_replace('.php', SEO_DEFAULT_INNER_SEPARATOR, $handler);
          $seo_params_array[$entry['seo_name'] . SEO_DEFAULT_INNER_SEPARATOR . $handler] = $entry['sort_order'] . '_' . $entry['seo_types_linkage'];
          $this->handler_flag = true;
        } else {
          $seo_params_array[$entry['seo_types_prefix'] . $entry['seo_name']] = $entry['sort_order'] . '_' . $entry['seo_types_linkage'];
        }
        $result = true;
      }
      return $result;
    }

    function set_path($query_raw, &$tmp_array, &$depth, &$sort_order) {
      $result = $handler = false;
      $params_query = tep_db_query($query_raw);
      if( $entry = tep_db_fetch_array($params_query) ) {
        if( !$depth ) {
          if( tep_not_null($entry['seo_types_subfix']) ) {
            $handler_array = explode(',', $entry['seo_types_handler']);
            $subfix_array = explode(',', $entry['seo_types_subfix']);
            foreach($handler_array as $key => $value) {
              if( $this->path == $value ) {
                if( isset($subfix_array[$key]) ) {
                  $handler = $subfix_array[$key];
                } else {
                  $handler = $value;
                  $handler = str_replace('.php', SEO_DEFAULT_INNER_SEPARATOR, $handler);
                }
              }
            }
          }
          if( $handler && !$this->handler_flag ) {
            //$handler = str_replace('.php', SEO_DEFAULT_INNER_SEPARATOR, $handler);
            $tmp_array[] = $handler . $entry['seo_name'];
            $tmp_array[] = $entry['seo_name'] . SEO_DEFAULT_INNER_SEPARATOR . $handler;
            $this->handler_flag = true;
          } else {
            $tmp_array[] = $entry['seo_types_prefix'] . $entry['seo_name'];
          }
          $sort_order = $entry['sort_order'] . '_' . $entry['seo_types_linkage'];
        } else {
          $tmp_array[] = $entry['seo_name'];
        }
        $depth++;
        $result = true;
      }
      return $result;
    }

    function assign_default(&$params_array, $value) {
      $value = $this->create_safe_string($value);
      $params_array[$value] = $value;
    }

    function exclude_script() {
      // Make sure this is a php script otherwise exclude it.
      if( strlen($this->path) < 5 || substr($this->path, -4, 4) != '.php') {
        return true;
      }
      $result = false;
      $key = md5($this->path);

      $check_query = tep_db_query("select seo_exclude_key from " . TABLE_SEO_EXCLUDE . " where seo_exclude_key = '" . tep_db_input($key) . "'");
      if( tep_db_num_rows($check_query) ) {
         return true;
      }
      $this->params_array = explode('&', $this->query );
      return $result;
    }

    // Validate REQUEST_URI in case we got a redirect from a server script. May needed with some servers
    function validate_uri(&$seo_url) {
      global $g_relpath; 
      $request_uri = explode('?', $_SERVER['REQUEST_URI']);
      $self = basename($_SERVER['PHP_SELF']);
      $self_count = strlen($self);
      if( is_array($request_uri) && isset($request_uri[1]) && strlen($request_uri[0]) > $self_count && $self == substr($request_uri[0], -$self_count, $self_count) ) {
        $this->params_array = explode('&', $request_uri[1]);
        if( is_array($this->params_array) ) {
          $seo_url = $_SERVER['REQUEST_URI'] = $this->params_array[0];
          unset($this->params_array[0]);
          $query_string = implode('&',$this->params_array);
          if( $query_string != '' ) {
            $seo_url .= '?' . $query_string;
            $_SERVER['REQUEST_URI'] = $seo_url;
          }
          // Rectify seo url
          $seo_url = $g_relpath . $_SERVER['REQUEST_URI'];
        }
      }
    }

    // Scan redirection table for matches against incoming urls.
    function check_redirection($key, $seo_right, $check_only=false) {
      if( SEO_DEFAULT_ENABLE == 'false' || SEO_REDIRECT_TABLE == 'false' || !empty($_POST) ) {
        return false;
      }

      $key_osc = md5($seo_right);

      $update = true;
      if( $key ) {
        $check_query = tep_db_query("select seo_url_org, seo_redirect from " . TABLE_SEO_REDIRECT . " where seo_url_key = '" . tep_db_input($key) . "'");
        if( $seo_array = tep_db_fetch_array($check_query) ) {
          if( $check_only ) 
            return true;

          $separator = '';
          $url = $seo_array['seo_url_org'];
          $url_parse = parse_url($url);
          if( !isset($url_parse['query']) ) {
            if( $seo_right != '' ) {
              $separator = '?';
            }
            $url_query = '';
          } else {
            if( $seo_right != '' ) {
              $separator = '&';
            }
            $url_query = '?' . $url_parse['query'];
          }

          // Abort on duplicates
          $double_query = tep_db_query("select seo_url_key from " . TABLE_SEO_URL . " where seo_url_key = '" . tep_db_input($key) . "'");
          if(tep_db_num_rows($double_query))
            return false;
        } else {
          return false;
        }
      } else {
        $check_query = tep_db_query("select seo_url_key, seo_url_get from " . TABLE_SEO_URL . " where osc_url_key = '" . tep_db_input($key_osc) . "'");
        if( $seo_array = tep_db_fetch_array($check_query) ) {
          if( $check_only ) 
            return true;
          $update = false;
          $seo_array['seo_redirect'] = SEO_DEFAULT_ERROR_HEADER;
          $key = $seo_array['seo_url_key'];
          $url = $url_query = $separator = '';
          $seo_right = $seo_array['seo_url_get'];

        } else {
          $check_query = tep_db_query("select seo_url_key, seo_url_org, seo_redirect from " . TABLE_SEO_REDIRECT . " where seo_url_get = '" . tep_db_input($seo_right) . "'");
          if( $seo_array = tep_db_fetch_array($check_query) ) {
            if( $check_only ) 
              return true;

            $key = $seo_array['seo_url_key'];
            $url = $url_query = $separator = '';
            $seo_right = $seo_array['seo_url_org'];
          } else {
            $this->check_url_proximity($seo_right);
            return false;
          }
        }
      }

      if($update) {
        //tep_db_query("update " . TABLE_SEO_REDIRECT . " set seo_url_hits = seo_url_hits+1, last_modified=now() where seo_url_key = '" . tep_db_input($key) . "'");
      }
      $url_redirect = $url . $url_query . $separator . $seo_right;
      $this->issue_redirect($seo_array['seo_redirect'], $url_redirect);
      return false;
    }

    function eliminate_session($remove_name=false) {
      if( !$remove_name ) {
        $remove_name = tep_session_name();
      }
      if( is_array($this->params_array) ) {
        for($i=0, $j=count($this->params_array); $i<$j; $i++ ) {
          if(strpos($this->params_array[$i], $remove_name) !== false ) {
            unset($this->params_array[$i]);
          }
        }
      }
    }

    // Proximity redirect
    function check_url_proximity($seo_right) {
      $result = false;
      //$check_query = tep_db_query("select seo_url_key, seo_url_org from " . TABLE_SEO_URL . " where seo_url_get = '" . tep_db_input($seo_right) . "'");
      $key = md5($seo_right);
      $check_query = tep_db_query("select seo_url_key, seo_url_org from " . TABLE_SEO_URL . " where seo_url_key = '" . tep_db_input($key) . "'");

      if( !tep_db_num_rows($check_query) && SEO_PROXIMITY_CLEANUP == 'true' ) {
        $url_parse = parse_url($seo_right);
        if( !empty($url_parse['query']) || substr( basename($url_parse['path']), -strlen(SEO_DEFAULT_EXTENSION), strlen(SEO_DEFAULT_EXTENSION)) != SEO_DEFAULT_EXTENSION ) {
          return $result;
        }

        $seo_right = substr($seo_right, 0, -strlen(SEO_DEFAULT_EXTENSION) );

        if( strlen(basename($seo_right)) > SEO_PROXIMITY_THRESHOLD ) {
          $match = basename($seo_right);
          $pattern = substr($seo_right, 0, -strlen($match) );
          do {
            $check_query = tep_db_query("select seo_url_key, seo_url_get from " . TABLE_SEO_URL . " where seo_url_get like '" . tep_db_input($pattern.$match) . "%' order by auto_id desc limit 1");
            if( $seo_array = tep_db_fetch_array($check_query) ) {
              $seo_array['seo_redirect'] = SEO_DEFAULT_ERROR_HEADER;
              $key = $seo_array['seo_url_key'];
              $url = $url_query = $separator = '';
              $seo_right = $seo_array['seo_url_get'];
              $result = true;
              break;
            } else {
              $match = substr($match, 0, -1);
            }
          } while( strlen($match) > SEO_PROXIMITY_THRESHOLD);

          $match = basename($seo_right);
          if(!$result) do {
            $check_query = tep_db_query("select seo_url_key, seo_url_get from " . TABLE_SEO_URL . " where seo_url_get like '" . tep_db_input($pattern) . '%' . tep_db_input($match) . "%' order by auto_id desc limit 1");
            if( $seo_array = tep_db_fetch_array($check_query) ) {
              $seo_array['seo_redirect'] = SEO_DEFAULT_ERROR_HEADER;
              $key = $seo_array['seo_url_key'];
              $url = $url_query = $separator = '';
              $seo_right = $seo_array['seo_url_get'];
              $result = true;
              break;
            } else {
              $match = substr($match, 1);
            }
          } while( strlen($match) > SEO_PROXIMITY_THRESHOLD);

        }
      }
      if( $result ) {
        $url_redirect = $url . $url_query . $separator . $seo_right;
        $this->issue_redirect($seo_array['seo_redirect'], $url_redirect);
      }
      return $result;
    }

    function issue_redirect($type_redirect, $url_redirect) {
      // Issue Redirect
      header("HTTP/1.1 " . $type_redirect);
      header('P3P: CP="NOI ADM DEV PSAi COM NAV STP IND"');
      header('Location: ' . $url_redirect);
      exit();
    }

    function cache_urls() {
      if( SEO_CACHE_ENABLE == 'false' || $this->osc_key == '') {
        return;
      }
      $check_query = tep_db_query("select osc_url_key from " . TABLE_SEO_CACHE . " where osc_url_key = '" . tep_db_input(tep_db_prepare_input($this->osc_key)) . "'");
      if( !tep_db_num_rows($check_query) ) {
        $keys_array = array_keys($this->osc_keys_array);
        $keys_string = implode(',', $keys_array);
        unset($keys_array);
        $url_array = array();
        $sep_array = array();
        foreach($this->osc_keys_array as $key => $value ) {
          $url_array[] = $value['url'];
          $sep_array[] = $value['sep'];
        }
        $url_string = implode(',', $url_array);
        $sep_string = implode(',', $sep_array);
        unset($url_array, $sep_array);

        $keys_zip = base64_encode(gzdeflate($keys_string, 1));
        $url_zip = base64_encode(gzdeflate($url_string, 1));
        $sql_data_array = array(
                                'osc_url_key' => tep_db_prepare_input($this->osc_key),
                                'seo_cache_keys' => tep_db_prepare_input($keys_zip),
                                'seo_cache_urls' => tep_db_prepare_input($url_zip),
                                'seo_cache_separators' => tep_db_prepare_input($sep_string),
                                'date_added' => 'now()'
                               );
        tep_db_perform_insert(TABLE_SEO_CACHE, $sql_data_array);
      }
    }

    function cache_init($key) {
      if( SEO_CACHE_ENABLE == 'false' || $key == '') {
        return;
      }
      $check_query = tep_db_query("select seo_cache_keys, seo_cache_urls, seo_cache_separators from " . TABLE_SEO_CACHE . " where osc_url_key = '" . tep_db_input($key) . "'");
      if( tep_db_num_rows($check_query) ) {
        $check_array = tep_db_fetch_array($check_query);
        $keys_array = explode(',', gzinflate(base64_decode($check_array['seo_cache_keys'])));
        //$keys_array = explode(',', $check_array['seo_cache_keys']);
        $url_array = explode(',', gzinflate(base64_decode($check_array['seo_cache_urls'])));
        $sep_array = explode(',', $check_array['seo_cache_separators']);
        for($i=0, $j=count($keys_array); $i<$j; $i++ ) {
          $this->osc_keys_array[$keys_array[$i]] = array('url' => $url_array[$i], 'sep' => $sep_array[$i]);
        }
        unset($keys_array, $url_array, $sep_array);
        $past_time = strtotime(SEO_UPDATE_TIMOUT);
        $new_time = time()-86400;
        if( $new_time > $past_time) {
          tep_db_query("delete from " . TABLE_SEO_CACHE . " where (unix_timestamp(now()) - unix_timestamp(date_added)) > " . SEO_CACHE_REFRESH);
          tep_db_query("update " . TABLE_CONFIGURATION . " set configuration_value=now() where configuration_key = 'SEO_UPDATE_TIMOUT'");
          tep_db_query("alter table " . TABLE_SEO_CACHE . " ENGINE = InnoDB");
          tep_db_query("alter table " . TABLE_SEO_URL . " ENGINE = InnoDB");
        }
      }
    }
  }
?>
