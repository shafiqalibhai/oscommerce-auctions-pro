<?php

/*

//----------------------------------------------------------------------------

// Copyright (c) 2006-2011 Asymmetric Software. Innovation & Excellence.

// Author: Mark Samios

// http://www.asymmetrics.com

//----------------------------------------------------------------------------

// Catalog: Auction Detalis AJAX module

//----------------------------------------------------------------------------

// Script is intended to be used with:

// osCommerce, Open Source E-Commerce Solutions

// Copyright (c) 2003 osCommerce

------------------------------------------------------------------------------

// Released under the GNU General Public License

//----------------------------------------------------------------------------

*/

  $auction_status_array = array(

    array('id' => 0, 'text' => 'Coming Auction'),

    array('id' => 1, 'text' => 'Live Auction'),

    array('id' => 2, 'text' => 'Ended Auction'),

  );



  $auction_types_array = array(

    array('id' => 0, 'text' => 'Penny Auction'),

    array('id' => 1, 'text' => 'Lowest Unique Bid'),

    array('id' => 2, 'text' => 'Normal Auction'),

  );



  $action = isset($_POST['action'])?tep_db_prepare_input($_POST['action']):'';

  $auction_id = isset($_POST['auction_id'])?(int)$_POST['auction_id']:0;



  if( $action == 'details' && $auction_id) {

    $auction_query = tep_db_query("select auctions_name, auctions_type, auctions_overbid, countdown_bids, products_id, status_id, bid_step, start_price, cap_price, shipping_cost, date_start, date_expire, start_pause, end_pause, auto_timer, live_off from " . TABLE_AUCTIONS . " where auctions_id = '" . (int)$auction_id . "'");

    if( !tep_db_num_rows($auction_query) ) {

      tep_exit();

    }

    $auction = tep_db_fetch_array($auction_query);

?>

    <table class="tabledata" style="background: #FFF">

      <tr>

        <th colspan="2"><?php echo sprintf('Current Settings of %s (%s)', $auction['auctions_name'], tep_date_time_short(date('Y-m-d H:i:s'))); ?></th>

      </tr>

      <tr>

        <td><?php echo 'Status'; ?></td>

        <td class="heavy"><?php echo $auction_status_array[$auction['status_id']]['text']; ?></td>

      </tr>

      <tr>

        <td><?php echo 'Type'; ?></td>

        <td class="heavy"><?php echo $auction_types_array[$auction['auctions_type']]['text']; ?></td>

      </tr>

      <tr>

        <td><?php echo 'Required Bids per Turn'; ?></td>

        <td class="heavy"><?php echo $auction['bid_step']; ?></td>

      </tr>

      <tr>

        <td><?php echo 'Allow Overbid'; ?></td>

        <td class="heavy"><?php echo $auction['auctions_overbid']?'Yes':'No'; ?></td>

      </tr>

      <tr>

        <td><?php echo 'Auction Price Min'; ?></td>

        <td class="heavy">

<?php 

    if( $auction['start_price'] > 0 ) {

      echo $currencies->format($auction['start_price']);

    }  else {

      echo $currencies->format($auction['bid_step']/100);

    }

?>

        </td>

      </tr>

      <tr>

        <td><?php echo 'Auction Price Max'; ?></td>

        <td class="heavy">

<?php 

    if( $auction['cap_price'] > 0 ) {

      echo $currencies->format($auction['cap_price']);

    }  else {

      echo 'No Limit';

    }

?>

        </td>

      </tr>

      <tr>

        <td><?php echo 'Shipping Cost'; ?></td>

        <td class="heavy">

<?php 

    if( $auction['shipping_cost'] > 0 ) {

      echo $currencies->format($auction['shipping_cost']);

    }  else {

      echo 'Free Shipping';

    }

?>

        </td>

      </tr>

      <tr>

        <td><?php echo 'Auction Start'; ?></td>

        <td class="heavy">

<?php

    $start = tep_auction_check_start($auction);

    if( strlen($start) ) {

      echo $start;

    } else {

      echo TEXT_NA;

    }

?>

        </td>

      </tr>

      <tr>

        <td><?php echo 'Periodic Pause'; ?></td>

        <td class="heavy">

<?php

    if( $auction['start_pause'] && $auction['start_pause'] != '00:00:00' && $auction['end_pause'] && $auction['end_pause'] != '00:00:00') {

      echo $auction['start_pause'] . ' - ' . $auction['end_pause'];

    } else {

      echo TEXT_NA;

    }

?>

        </td>

      </tr>

      <tr>

        <td><?php echo 'Expires'; ?></td>

        <td class="heavy">

<?php

    $countdown_bids = false;

    if( $auction['countdown_bids']) {

      $check_query = tep_db_query("select bid_count, last_modified from " . TABLE_AUCTIONS_BID . " where auctions_id = '" . (int)$auction_id . "'");

      if( tep_db_num_rows($check_query) ) {

        $countdown_bids = true;

      }

    }

              //no of bids will on somany bids (bids remaining) //expires after 26 nids
              //or expires in somany days
              //coming soon (same)

    if( $countdown_bids ) {

      echo 'Ends when timer reaches zero';

    } else if( $auction['date_expire'] && $auction['date_expire'] != '0000-00-00 00:00:00') {

      echo tep_date_time_short($auction['date_expire']);

    } elseif( $auction['status_id'] == 1 && $auction['auto_timer'] && $auction['live_off'] != '0000-00-00 00:00:00') {

      echo tep_date_time_short($auction['live_off']);

    } else {

      echo TEXT_NA;
      echo $auction_id;

    }

?>

        </td>

      </tr>

      <tr>

        <th colspan="2"><?php echo 'Prizes'; ?></th>

      </tr>

      <tr class="moduleRowSelected">

        <td colspan="2" class="heavy"><?php echo '1. <a href="' . tep_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . $auction['products_id']) . '">' . tep_get_products_name($auction['products_id']) . '</a>'; ?></td>

      </tr>

<?php

    $tier_array = array();

    $tier_query_raw = "select products_id from " . TABLE_AUCTIONS_TIER . " where auctions_id = '" . (int)$auction_id . "' order by sort_id";

    tep_query_to_array($tier_query_raw, $tier_array);

    for( $i=0, $j=count($tier_array); $i<$j; $i++) {

?>

      <tr class="moduleRowSelected">

        <td colspan="2" class="heavy"><?php echo ($i+2) . '. <a href="' . tep_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . $tier_array[$i]['products_id']) . '">' . tep_get_products_name($tier_array[$i]['products_id']) . '</a>'; ?></td>

      </tr>

<?php

    }

    if( tep_session_is_registered('customer_id') && $auction['status_id'] == 1 ) {

?>

      <tr>

        <th colspan="2"><?php echo sprintf('Your participation in %s', $auction['auctions_name']); ?></th>

      </tr>

      <tr>

        <td><?php echo 'Your Current Bids'; ?></td>

<?php

      $stats_query = tep_db_query("select count(*) as total from " . TABLE_AUCTIONS_CUSTOMER . " where auctions_id = '" . (int)$auction_id . "' and customers_id = '" . (int)$customer_id . "'");

      $stats_array = tep_db_fetch_array($stats_query);

?>

        <td><?php echo $stats_array['total']; ?></td>

      </tr>

<?php

      if( $stats_array['total']) {

?>

      <tr>

        <th colspan="2"><?php echo sprintf('Your Bid Details (displaying up to %s Bids)', 10); ?></th>

      </tr>

<?php

        $bids_array = array();

        $bids_query = "select bid_price, date_added from " . TABLE_AUCTIONS_CUSTOMER . " where auctions_id = '" . (int)$auction_id . "' and customers_id = '" . (int)$customer_id . "' order by date_added desc limit 10";

        tep_query_to_array($bids_query, $bids_array);

        for( $i=0, $j=count($bids_array); $i<$j; $i++ ) {

?>

      <tr>

        <td><?php echo tep_date_time_short($bids_array[$i]['date_added']); ?></td>

        <td class="heavy"><?php echo $bids_array[$i]['bid_price']; ?></td>

      </tr>

<?php

        }

      }

      $bids_array = array();

      $bids_query = "select customers_id, bid_price, date_added from " . TABLE_AUCTIONS_CUSTOMER . " where auctions_id = '" . (int)$auction_id . "' order by date_added desc limit 10";

      tep_query_to_array($bids_query, $bids_array);

      $j = count($bids_array);

      if( $j ) {

?>

      <tr>

        <th colspan="2"><?php echo sprintf('Last Bids Entered by Players (displaying up to %s Bids)', 10); ?></th>

      </tr>

<?php

        $customers_array = array();

        for( $i=0; $i<$j; $i++ ) {

?>

      <tr>

        <td><?php echo tep_date_time_short($bids_array[$i]['date_added']); ?></td>

        <td class="heavy">

<?php

          if( $auction['auctions_type'] == 0 || $customer_id == $bids_array[$i]['customers_id'] ) {

            echo $bids_array[$i]['bid_price']; 

          } else {

            if( !isset($customers_array[$bids_array[$i]['customers_id']]) ) {

              $customers_array[$bids_array[$i]['customers_id']] = tep_auction_customer_nickname($bids_array[$i]['customers_id']);

            }

            echo '<span class="mark_red">' . $customers_array[$bids_array[$i]['customers_id']] . '</span>';

          }

?>

        </td>

      </tr>

<?php

        }

      }

    } elseif ( tep_session_is_registered('customer_id') && $auction['status_id'] == 2 ) {

?>

      <tr>

        <th colspan="2"><?php echo sprintf('Details of ended auction %s', $auction['auctions_name']); ?></th>

      </tr>

<?php

      $history_array = array();

      $history_query = "select customers_id, customers_nickname, auctions_price, products_name, winner_bids, bid_count, started, completed from " . TABLE_AUCTIONS_HISTORY . " where auctions_id = '" . (int)$auction_id . "'";

      tep_query_to_array($history_query, $history_array);

      $self_bid = false;



      for($i=0, $j=count($history_array); $i<$j; $i++) {

        $history = $history_array[$i];

        if( empty($history['customers_nickname']) ) {

          $history['customers_nickname'] = 'No Winner';

        }

        if( empty($history['products_name']) ) {

          $history['products_name'] = TEXT_NA;

        }

        if($history['customers_id'] == $customer_id && !$self_bid) {

?>

      <tr class="moduleRowSelected">

        <td class="heavy mark_red"><?php echo 'Your Bids as a Winner'; ?></td>

        <td class="heavy"><?php echo $history['winner_bids']; ?></td>

      </tr>

<?php

          $self_bid = true;

        }

?>

<?php

        if( !$i ) {

?>

      <tr>

        <td><?php echo 'Final Price'; ?></td>

        <td><?php echo $currencies->format($history['auctions_price']); ?></td>

      </tr>

      <tr>

        <td><?php echo 'Total Bids Placed'; ?></td>

        <td><?php echo $history['bid_count']; ?></td>

      </tr>

      <tr>

        <td><?php echo 'Auction Completed'; ?></td>

        <td><?php echo tep_date_time_short($history['completed']); ?></td>

      </tr>

      <tr>

        <th colspan="2"><?php echo 'Winners & Prizes'; ?></td>

      </tr>

<?php

        }

?>

<?php

        if($history['customers_id'] == $customer_id) {

?>

      <tr class="moduleRowSelected">

        <td class="heavy"><?php echo ($i+1) . '. You won'; ?></td>

        <td class="heavy mark_red"><?php echo $history['products_name']; ?></td>

      </tr>

<?php

        } else {

?>

      <tr>

        <td class="heavy"><?php echo ($i+1) . '. <span class="mark_blue">' . $history['customers_nickname'] . '</span>'; ?></td>

        <td class="heavy"><?php echo $history['products_name']; ?></td>

      </tr>

<?php

        }

      }

    }

?>

    </table>

<?php

  }

  tep_exit();

?>