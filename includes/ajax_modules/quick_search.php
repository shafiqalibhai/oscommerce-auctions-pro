<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2007 Asymmetric Software. Innovation & Excellence.
// Author: Mark Samios
// http://www.asymmetrics.com
//----------------------------------------------------------------------------
// Catalog: Countries switch module
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
------------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
//
*/
  $limit = 10;
  $error = '<div><a href="' .  tep_href_link(FILENAME_ADVANCED_SEARCH, '', 'NONSSL') . '">No results found - See other options</a></div>';
  $keywords = isset($_POST['keywords'])?tep_output_string_protected($_POST['keywords']):'';
  if( empty($keywords) ) {
    echo $error;
    return;
  }

  $results = array();
  $products_query = tep_db_query("select pd.products_id, pd.products_name from " . TABLE_PRODUCTS_DESCRIPTION . " pd left join " . TABLE_PRODUCTS . " p on (p.products_id = pd.products_id) where pd.products_name like '%" . tep_db_input($keywords) . "%' and p.products_display = '1' and pd.language_id = '" . (int)$languages_id . "' order by pd.products_name limit " . $limit);
  if( !tep_db_num_rows($products_query) ) {
    echo $error;
    return;
  }

  while($products_array = tep_db_fetch_array($products_query) ) {
   echo '<div><a href="' . tep_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . $products_array['products_id']) . '">' . $products_array['products_name'] . '</a></div>';
  }
?>

