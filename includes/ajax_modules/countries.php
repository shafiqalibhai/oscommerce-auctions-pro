<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2007 Asymmetric Software. Innovation & Excellence.
// Author: Mark Samios
// http://www.asymmetrics.com
//----------------------------------------------------------------------------
// Catalog: Countries switch module
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
------------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
//
*/
  if( isset($_POST['country']) ) {
    echo tep_draw_pull_down_menu('state', tep_get_country_zones($_POST['country']), '', 'id="state" style="width:180px;"');
  }
  if( isset($_POST['country2']) ) {
    echo tep_draw_pull_down_menu('state2', tep_get_country_zones($_POST['country2']), '', 'id="state" style="width:180px;"');
  }
?>

