<?php
/*
This file is part of includes/application_top.php v1.280

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

//----------------------------------------------------------------------------
// Copyright (c) 2006-2008 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Catalog: Early initialization process
//----------------------------------------------------------------------------
// I-Metrics Layer
//----------------------------------------------------------------------------
// Early initialization intends to perform and includes the following: 
// - Installation Service
// - Database connection and database support functions inclusion
// - Critical functions and definitions inclusion
// - Definitions for SEO-G so it can decode/translate incoming URLs
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
  if( !strlen(ini_get('date.timezone')) && function_exists('date_default_timezone_get')) {
    date_default_timezone_set(@date_default_timezone_get());
  }

  $user_agent = strtolower(getenv('HTTP_USER_AGENT'));
  $g_script = '';

  if( !isset($g_external) ) {
    if($_SERVER['REQUEST_METHOD'] != "GET" && $_SERVER['REQUEST_METHOD'] != "POST" ) {
      header("HTTP/1.1 405");
      header("Allow: GET, POST");
      exit();
    }

    $g_protocol = true;

    if( !isset($_SERVER['SERVER_PROTOCOL']) || $_SERVER['SERVER_PROTOCOL'] != 'HTTP/1.1') {
      $g_protocol = false;
      //header("HTTP/1.1 505");
      //header("Upgrade: HTTP/1.1");
      //exit();
    }

    $g_proxy = false;

    $tmp_array = $_SERVER;
    if( isset($tmp_array['HTTP_X_REQUESTED_WITH']) ) unset($tmp_array['HTTP_X_REQUESTED_WITH']);

    $proxy_string = implode(',', array_keys($tmp_array));
    //if( strpos($proxy_string, 'HTTP_X') !== false ) $g_proxy = true;

    if( strpos($proxy_string, 'PROXY') !== false ) $g_proxy = true;
    if( strpos($proxy_string, 'FORWARD') !== false ) $g_proxy = true;
    if( strpos($proxy_string, 'CLIENT') !== false ) $g_proxy = true;

    if( isset($_SERVER['HTTP_VIA']) || isset($_SERVER['HTTP_10_0_0_0']) || 
        isset($_SERVER['HTTP_SP_HOST']) || isset($_SERVER['HTTP_REMOTE_HOST_WP']) || 
        isset($_SERVER['HTTP_COMING_FROM']) || isset($_SERVER['ZHTTP_CACHE_CONTROL']) ) {
      $g_proxy = true;
    }
    unset($proxy_string, $tmp_array);


//  $g_connection = isset($_SERVER['HTTP_CONNECTION'])?strtolower($_SERVER['HTTP_CONNECTION']):'';
//  if($g_connection != 'keep-alive' ) {
//  }

    $g_encoding = isset($_SERVER['HTTP_ACCEPT_ENCODING'])?strtolower($_SERVER['HTTP_ACCEPT_ENCODING']):'none';
    if( strpos($g_encoding, 'gzip') === false ) {
      $g_encoding = false;
    } else {
      $g_encoding = true;
    }

    if( strlen($user_agent) > 255 ) {
      require('die.php');
      if( defined('BUFFERED_OUTPUT') ) {
        ob_end_flush();
      }
      exit();
    }

/*
    if(strpos($user_agent, 'http://') === false && !$g_protocol ) {
      require('die.php');
      if( defined('BUFFERED_OUTPUT') ) {
        ob_end_flush();
      }
      exit();
    }

    if( !$g_encoding || $g_proxy ) {
      require('die.php');
      if( defined('BUFFERED_OUTPUT') ) {
        ob_end_flush();
      }
      exit();
    }
*/
  }

  // start the timer for the page parse time log
  define('PAGE_PARSE_START_TIME', microtime());

    // set the level of error reporting
  error_reporting(E_ALL & ~E_NOTICE);
  //ini_set('error_reporting', E_ALL);
  //ini_set('display_errors', 1);

  // define the project version
  define('PROJECT_VERSION', 'I-Metrics Layer with MS2.2');

  // include server parameters
  require('includes/configure.php');

  if (strlen(DB_SERVER) < 1) {
    if (is_dir('install')) {
      header('Location: install/index.php');
      exit();
    }
    die('Invalid configuration file - Could not locate installation folder. ' . PROJECT_VERSION . ' cannot continue.');
  }

  // include the list of project filenames
  require_once(DIR_WS_INCLUDES . 'filenames.php');

  // include the list of project database tables
  require_once(DIR_WS_INCLUDES . 'database_tables.php');

  // include the database functions
  require_once(DIR_WS_FUNCTIONS . 'database.php');

  // make a connection to the database... now
  tep_db_connect() or die('Unable to connect to database server!');

  // set the application parameters
  $configuration_query = tep_db_query('select configuration_key as cfgKey, configuration_value as cfgValue from ' . TABLE_CONFIGURATION);
  while ($configuration = tep_db_fetch_array($configuration_query)) {
    if( !defined($configuration['cfgKey']) ) {
      define($configuration['cfgKey'], $configuration['cfgValue']);
    }
  }

  // set the type of request (secure or not)
  //  $request_type = (getenv('HTTPS') == 'on') ? 'SSL' : 'NONSSL';
  $request_type = (getenv('SERVER_PORT') == '443') ? 'SSL' : 'NONSSL';

  // set php_self in the local scope
  if (!isset($PHP_SELF)) $PHP_SELF = $_SERVER['PHP_SELF'];

  if ($request_type == 'NONSSL') {
    define('DIR_WS_CATALOG', DIR_WS_HTTP_CATALOG);
    $g_relpath = HTTP_SERVER . DIR_WS_CATALOG;
    $g_server = HTTP_SERVER;
  } else {
    define('DIR_WS_CATALOG', DIR_WS_HTTPS_CATALOG);
    $g_relpath = HTTPS_SERVER . DIR_WS_CATALOG;
    $g_server = HTTPS_SERVER;
  }

  if( !isset($g_external) && !file_exists(basename($PHP_SELF)) ) {
    // Issue Redirect
    header("HTTP/1.1 301");
    header('P3P: CP="NOI ADM DEV PSAi COM NAV STP IND"');
    header('Location: ' . $g_relpath);
    exit();
  }

  $g_script = basename($PHP_SELF);
  // define general functions used application-wide
  require_once(DIR_WS_FUNCTIONS . 'general.php');
  require_once(DIR_WS_FUNCTIONS . 'html_output.php');

  //-MS- SEO URLs Support Added
  if( file_exists(DIR_WS_CLASSES . 'seo_url.php') ) {
    require(DIR_WS_CLASSES . 'seo_url.php');
    $g_seo_url = new seoURL;
  }
  //-MS- SEO URLs Support Added EOM
?>
