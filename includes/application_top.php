<?php
/*
  $Id: application_top.php,v 1.280 2003/07/12 09:38:07 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce
         
//----------------------------------------------------------------------------
// Modifications by Asymmetrics
// Copyright (c) 2006-2008 Asymmetric Software. Innovation & Excellence.
// Author: Mark Samios
// http://www.asymmetrics.com
//----------------------------------------------------------------------------
// I-Metrics Layer
//----------------------------------------------------------------------------
// - PHP5 Register Globals and Long Arrays Off support added
// - Added SEO-G
// - Added META-G
// - Removed stock friendly URLs
// - Fix for navigation history
// - Relocated message stack early.
// - Added validator module
// - Added filter types module
// - Removed PHP3 dependencies
// - Added Cache HTML
// - Recoded cookie/session sent.
// - Added ecommerce switch
// - Added Abstract Zones support
// - Fix for non-terminated scripts having incorrect session info
// - Removed Local configuration dependency.
// - DBase - Do not process invalid parameters
// - Relocated nav history at the end of the file
// - Recoded Language/Currency settings
// - Discard requests with /POST when no valid session is present
// - Moved early initialization to a different file
// - Moved session process to a different file
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
// Initialize current global indices for supported entities
  $g_counter = 0;
  $current_category_id = 0;
  $current_range_id = 0;
  $current_manufacturer_id = 0;
  $current_testimonial_id = 0;
  $current_product_id = 0;
  $cPath = '';
  $current_abstract_id = 0;
  $current_gtext_id = 0;
  $current_page_id = 0;
  $g_ajax = isset($_SERVER['HTTP_X_REQUESTED_WITH'])?true:false;
  
  

// Initialize session parameters
  $session_started = false;
  $spider_flag = false;

//-MS- SEO-G Added
  if( !isset($g_seo_flag) || $g_seo_flag !== true) {
    // Initialize database and basic functions
    require('includes/init_early.php');
  } 
//-MS- SEO-G Added EOM

// some code to solve compatibility issues
  require(DIR_WS_FUNCTIONS . 'compatibility.php');

  // if gzip_compression is enabled, start to buffer the output
  if ( (GZIP_COMPRESSION == 'true') && ($ext_zlib_loaded = extension_loaded('zlib')) ) {
    if (($ini_zlib_output_compression = (int)ini_get('zlib.output_compression')) < 1) {
      ob_start('ob_gzhandler');
    } else {
      ini_set('zlib.output_compression_level', GZIP_LEVEL);
    }
  }

// set the cookie domain
  $cookie_domain = (($request_type == 'NONSSL') ? HTTP_COOKIE_DOMAIN : HTTPS_COOKIE_DOMAIN);
  $cookie_path = (($request_type == 'NONSSL') ? HTTP_COOKIE_PATH : HTTPS_COOKIE_PATH);

  //-MS- HTML Cache Support Added
  require(DIR_WS_CLASSES . 'cache_html.php');
  //-MS- HTML Cache Support Added EOM

  require(DIR_WS_CLASSES . 'currencies.php');
  require(DIR_WS_CLASSES . 'language.php');

// include shopping cart class
  require(DIR_WS_CLASSES . 'shopping_cart.php');

// include navigation history class
  require(DIR_WS_CLASSES . 'navigation_history.php');

// include the mail classes
  require(DIR_WS_CLASSES . 'mime.php');
  require(DIR_WS_CLASSES . 'email.php');

  require(DIR_WS_CLASSES . 'boxes.php');
  require(DIR_WS_CLASSES . 'message_stack.php');

//-MS- perform the session process
  require(DIR_WS_INCLUDES . 'init_sessions.php');

//-MS- Parameters Validator added
  require(DIR_WS_CLASSES . 'validator.php');
  $g_validator = new validator;
//-MS- Parameters Validator added EOM

// verify the ssl_session_id if the feature is enabled
  if ( ($request_type == 'SSL') && (SESSION_CHECK_SSL_SESSION_ID == 'True') && (ENABLE_SSL == true) && ($session_started == true) ) {
    $ssl_session_id = getenv('SSL_SESSION_ID');
    if (!tep_session_is_registered('SSL_SESSION_ID')) {
      $SESSION_SSL_ID = $ssl_session_id;
      tep_session_register('SESSION_SSL_ID');
    }

    if ($SESSION_SSL_ID != $ssl_session_id) {
      tep_session_destroy();
      tep_redirect(tep_href_link(FILENAME_SSL_CHECK));
    }
  }

// verify the browser user agent if the feature is enabled
  if (SESSION_CHECK_USER_AGENT == 'True') {
    $http_user_agent = getenv('HTTP_USER_AGENT');
    if (!tep_session_is_registered('SESSION_USER_AGENT')) {
      $SESSION_USER_AGENT = $http_user_agent;
      tep_session_register('SESSION_USER_AGENT');
    }

    if ($SESSION_USER_AGENT != $http_user_agent) {
      tep_session_destroy();
      tep_redirect(tep_href_link(FILENAME_LOGIN));
    }
  }

// verify the IP address if the feature is enabled
  if (SESSION_CHECK_IP_ADDRESS == 'True') {
    $ip_address = tep_get_ip_address();
    if (!tep_session_is_registered('SESSION_IP_ADDRESS')) {
      $SESSION_IP_ADDRESS = $ip_address;
      tep_session_register('SESSION_IP_ADDRESS');
    }

    if ($SESSION_IP_ADDRESS != $ip_address) {
      tep_session_destroy();
      tep_redirect(tep_href_link(FILENAME_LOGIN));
    }
  }

//-MS- Set HTML Cache for visitors via 304 after session is started
  if( $session_started == true && isset($g_sid) ) {
    if( !tep_session_is_registered('g_html_cache') || !is_object($g_html_cache) ) {
      tep_session_register('g_html_cache');
      $g_html_cache = new cacheHTML;
    }
    $g_html_cache->check_script();
  }
//-MS- Set HTML Cache for visitors via 304 after session is started EOM

// create the shopping cart & fix the cart if necesary
  if( !tep_session_is_registered('cart') || !is_object($cart)) {
    tep_session_register('cart');
    $cart = new shoppingCart;
  }

// include currencies class and create an instance
  $currencies = new currencies();


// Check for corrupt session and cleanup past errors those occured before the session was closed.
  if( isset($_SESSION['language']) && !tep_not_null($_SESSION['language']) ) {
    tep_session_destroy();
    tep_redirect(tep_href_link('', '', 'NONSSL', false));
  }


  if( !tep_session_is_registered('language') || !isset($language) ) {
    tep_session_register('language');
    tep_session_register('languages_id');
    $lng = new language();
    $language = $lng->language['directory'];
    $languages_id = $lng->language['id'];
  }

// include the language translations
  require(DIR_WS_LANGUAGES . $language . '.php');

// Currency Setup
  $tmp_array = $currencies->currencies;
  if( !tep_session_is_registered('currency') ) {
    tep_session_register('currency');
    $currency = (USE_DEFAULT_LANGUAGE_CURRENCY == 'true') ? LANGUAGE_CURRENCY : DEFAULT_CURRENCY;
  } elseif( isset($_POST['change_currency']) && isset($tmp_array[$_POST['change_currency']]) ) {
    $currency = tep_db_prepare_input($_POST['change_currency']);
  } else {
    foreach($tmp_array as $key => $value ) {
      if( isset($_POST[$key . '_x']) || isset($_POST[$key . '_y']) ) {
        $currency = $key;
      }
    }
  }
  unset($tmp_array, $key, $value);

// navigation history
  if(!tep_session_is_registered('navigation') || !is_object($navigation)) {
    tep_session_register('navigation');
    $navigation = new navigationHistory;
  }

//-MS- moved for early error control
  $messageStack = new messageStack;
//-MS- moved for early error control EOM

//-MS- Group Fields added
  require(DIR_WS_CLASSES . 'groups_front.php');
  $g_group_fields = new groups_front;
//-MS- Group Fields added

  require(DIR_WS_INCLUDES . 'init_params.php');

// include the who's online functions
  require(DIR_WS_FUNCTIONS . 'whos_online.php');
  tep_update_whos_online();

// include the password crypto functions
  require(DIR_WS_FUNCTIONS . 'password_funcs.php');

// include validation functions (right now only email address)
  require(DIR_WS_FUNCTIONS . 'validations.php');

// split-page-results
  require(DIR_WS_CLASSES . 'split_page_results.php');

// auto activate and expire banners
//  require(DIR_WS_FUNCTIONS . 'banner.php');
//  tep_activate_banners();
//  tep_expire_banners();

// auto expire special products
  require(DIR_WS_FUNCTIONS . 'specials.php');
  tep_expire_specials();

//  if (isset($g_validator->get_array['products_id']) && !isset($g_validator->get_array['manufacturers_id']) && !isset($g_validator->get_array['cPath']) ) {
//    $cPath = tep_get_product_path($g_validator->get_array['products_id']);
//  }

  if( $current_product_id > 0 ) {
    $cPath = tep_get_product_path($current_product_id);
  }

// include the breadcrumb class and start the breadcrumb trail
  require(DIR_WS_CLASSES . 'breadcrumb.php');
  $breadcrumb = new breadcrumb;
  $breadcrumb->add(HEADER_TITLE_CATALOG, tep_href_link());

// add category names or the manufacturer name to the breadcrumb trail
  if (isset($cPath_array) && is_array($cPath_array) ) {
    for ($i=0, $n=sizeof($cPath_array); $i<$n; $i++) {
      $categories_query = tep_db_query("select categories_name from " . TABLE_CATEGORIES_DESCRIPTION . " where categories_id = '" . (int)$cPath_array[$i] . "' and language_id = '" . (int)$languages_id . "'");
      if( $categories = tep_db_fetch_array($categories_query) ) {
        $breadcrumb->add($categories['categories_name'], tep_href_link(FILENAME_DEFAULT, 'cPath=' . implode('_', array_slice($cPath_array, 0, ($i+1)))));
      }
    }
  }

  if($current_manufacturer_id > 0) {
    $manufacturers_query = tep_db_query("select manufacturers_name from " . TABLE_MANUFACTURERS . " where manufacturers_id = '" . (int)$current_manufacturer_id . "'");
    if($manufacturers = tep_db_fetch_array($manufacturers_query) ) {
      $breadcrumb->add($manufacturers['manufacturers_name'], tep_href_link(FILENAME_MANUFACTURERS, 'manufacturers_id=' . $current_manufacturer_id));
    }
  }

// add the products model to the breadcrumb trail
  if($current_product_id > 0) {
    $model_query = tep_db_query("select products_model from " . TABLE_PRODUCTS . " where products_id = '" . (int)$current_product_id . "'");
    if( $model = tep_db_fetch_array($model_query) ) {
      if( tep_not_null($model['products_model']) ) {
        $breadcrumb->add($model['products_model'], tep_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . $current_product_id));
      }
    }
  }

// add the numeric range description to the breadcrumb trail
  if($current_range_id > 0) {
    $range_query = tep_db_query("select numeric_ranges_desc from " . TABLE_NUMERIC_RANGES . " where numeric_ranges_id = '" . (int)$current_range_id . "'");
    if( $range_array = tep_db_fetch_array($range_query) ) {
      $breadcrumb->add($range_array['numeric_ranges_desc'], tep_href_link(FILENAME_SHOP_BY_PRICE, 'range_id=' . $current_range_id));
    }
  }             
        
  require(DIR_WS_FUNCTIONS . 'auctions.php');

//-MS- Abstract zones added
  require(DIR_WS_CLASSES . 'abstract_front.php');
  require(DIR_WS_CLASSES . 'gtext_front.php');
  require(DIR_WS_CLASSES . 'product_front.php');
//-MS- Abstract zones added EOM

// set which precautions should be checked
  define('WARN_INSTALL_EXISTENCE', 'true');
  define('WARN_CONFIG_WRITEABLE', 'true');
  define('WARN_SESSION_DIRECTORY_NOT_WRITEABLE', 'true');
  define('WARN_SESSION_AUTO_START', 'true');
  define('WARN_DOWNLOAD_DIRECTORY_NOT_READABLE', 'true');

//-MS- Filters added
  require(DIR_WS_CLASSES . 'filters_front.php');
  $g_filters = new filters_front;
//-MS- Filters added EOM

//-MS- Moved to correct navigation history
  $navigation->add_current_page();
//-MS- Moved to correct navigation history EOM
?>
