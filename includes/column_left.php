<?php
/*
  $Id: column_left.php,v 1.15 2003/07/01 14:34:54 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/
  include(DIR_WS_BOXES . 'top_logo.php');
  switch(basename($PHP_SELF)) {
    case FILENAME_CHECKOUT:
    case FILENAME_GENERIC_PAGES:
    case FILENAME_CONTACT_US:
    case FILENAME_ACCOUNT:
    case FILENAME_ACCOUNT_EDIT:
    case FILENAME_ACCOUNT_HISTORY:
    case FILENAME_ACCOUNT_HISTORY_INFO:
    case FILENAME_ACCOUNT_NEWSLETTERS:
    case FILENAME_ACCOUNT_NOTIFICATIONS:
    case FILENAME_ACCOUNT_PASSWORD:
    case FILENAME_ADDRESS_BOOK:
    case FILENAME_ADDRESS_BOOK_PROCESS:
    case FILENAME_ADVANCED_SEARCH:
    case FILENAME_LOGIN:
    case FILENAME_LOGOFF:
    case FILENAME_CHECKOUT_SHIPPING:
    case FILENAME_CHECKOUT_PAYMENT:
    case FILENAME_CHECKOUT_CONFIRMATION:
    case FILENAME_CHECKOUT_SUCCESS:
    case FILENAME_SHOPPING_CART:
      include(DIR_WS_BOXES . 'generic_pages.php');
      break;
    default:
      //include(DIR_WS_BOXES . 'products.php');
      //include(DIR_WS_BOXES . 'filters.php');
      break;
  }
  //include(DIR_WS_BOXES . 'categories_top.php');
  //include(DIR_WS_BOXES . 'shop_by_price.php');
  //include(DIR_WS_BOXES . 'categories.php');
  //include(DIR_WS_BOXES . 'products.php');
  //include(DIR_WS_BOXES . 'filters.php');
/*
  include(DIR_WS_BOXES . 'shop_by_price.php');
  include(DIR_WS_BOXES . 'manufacturers.php');
  include(DIR_WS_BOXES . 'shop_by_style.php');
  include(DIR_WS_BOXES . 'accessories.php');
*/
//  include(DIR_WS_BOXES . 'categories.php');
//  if( DEFAULT_ECOMMERCE_MODE == 'ecommerce') {
//    require(DIR_WS_BOXES . 'information.php');
//  }
?>