<?php
/*
This file is part of includes/application_top.php v1.280

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

//----------------------------------------------------------------------------
// Copyright (c) 2006-2008 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Catalog: Session initialization process
//----------------------------------------------------------------------------
// I-Metrics Layer
//----------------------------------------------------------------------------
// Session initialization intends to validate and initiate a php session:
// The following modifications made:
// - Removed _GET dependencies
// - Added Multi-Level Extra Fields support
// - Added ECommerce, Maintenance modes
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
// Shopping cart actions
  if( isset($_GET['action']) ) {
// redirect the customer to a friendly cookie-must-be-enabled page if cookies are disabled
    if ($session_started == false) {
      tep_redirect(tep_href_link(FILENAME_COOKIE_USAGE));
    }

//-MS- Disable Cart
    if( DEFAULT_ECOMMERCE_MODE == 'site sense') {
      $goto = basename($PHP_SELF);
      tep_redirect(tep_href_link( $goto, $g_validator->convert_to_get(array('action')) ));
    }
//-MS- Disable Cart

    if (DISPLAY_CART == 'true') {
      $goto = FILENAME_SHOPPING_CART;
      $cart_parameters = '';
      $cart_request = 'NONSSL';
    } else {
      $goto = basename($PHP_SELF);
      $cart_parameters = $g_validator->convert_to_get(array('action'));
      $cart_request = $request_type;
    }

    switch ($_GET['action']) {
      // customer wants to update the product quantity in their shopping cart
      case 'update_product': 
        if( is_array($_POST['products_id']) ) {
          for ($i=0, $n=sizeof($_POST['products_id']); $i<$n; $i++) {
            if (in_array($_POST['products_id'][$i], (isset($_POST['cart_delete']) && is_array($_POST['cart_delete']) ? $_POST['cart_delete'] : array()))) {
              $cart->remove($_POST['products_id'][$i]);
            } else {
              $attributes = isset($_POST['id'][$_POST['products_id'][$i]])?$_POST['id'][$_POST['products_id'][$i]]:'';
              $cart->add_cart($_POST['products_id'][$i], $_POST['cart_quantity'][$i], $attributes, false);
            }
          }
        }
        tep_redirect(tep_href_link($goto, $cart_parameters, $cart_request));
        break;
      // customer adds a product from the products page

      case 'add_product' :
        //if (isset($_POST['products_id']) && is_numeric($_POST['products_id'])) {
        if (isset($_POST['products_id']) && $g_validator->get_products_id($_POST['products_id'], false) ) {

          $error_array = array();
          $groups = $g_group_fields->group_fields_request($_POST,$error_array);

          if( count($error_array) ) {
            foreach($error_array as $key => $value) {
              $messageStack->add('product_info', $value);
            }
            break;
          }

          $attributes = isset($_POST['id'])?$_POST['id']:'';
          $uprid = tep_get_uprid($_POST['products_id'], $attributes);
          $cart->add_cart($_POST['products_id'], 1, $attributes, true, $groups);

          tep_db_query("update " . TABLE_PRODUCTS_DESCRIPTION . " set products_viewed = products_viewed+1, last_accessed=now() where products_id = '" . (int)$_POST['products_id'] . "' and language_id = '" . (int)$languages_id . "'");
          if($goto == FILENAME_PRODUCT_INFO) {
            tep_redirect(tep_href_link($goto, 'products_id=' . $_POST['products_id']));
          } else {
            tep_redirect(tep_href_link($goto, $cart_parameters, $cart_request));
            //tep_redirect(tep_href_link($goto));
          }

        }
        tep_redirect(tep_href_link('', '', 'NONSSL', false), '301');
        break;

//-MS- Group Fields Added
      case 'add_group':
        if (isset($_POST['products_id']) && $g_validator->get_products_id($_POST['products_id'], false) ) {
          $error_array = array();
          $groups = $g_group_fields->group_fields_request($_POST,$error_array);
          if( count($error_array) ) {
            foreach($error_array as $key => $value) {
              $messageStack->add('product_info_options', $value);
            }
            break;
          }
//-MS- Group Fields Added EOM
          $attributes = isset($_POST['id'])?$_POST['id']:'';
          $uprid = tep_get_uprid($_POST['products_id'], $attributes);
          $cart->add_cart($_POST['products_id'], $cart->get_quantity($uprid)+1, $attributes, true, $groups);
          tep_redirect(tep_href_link(FILENAME_SHOPPING_CART, '', 'NONSSL'));
        }
        tep_redirect(tep_href_link('', '', 'NONSSL', false), '301');
        break;

//-MS- Group Fields Added EOM
      // performed by the 'buy now' button in product listings and review page
      case 'buy_now' :
        if (isset($_POST['products_id']) && is_numeric($_POST['products_id'])) {
          if (tep_has_product_attributes($_POST['products_id']) || tep_has_product_fields($_POST['products_id']) || count($g_group_fields->get_product_groups($_POST['products_id']))) {
            tep_redirect(tep_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . $_POST['products_id']));
          } else {
            $cart->add_cart($_POST['products_id'], $cart->get_quantity($_POST['products_id'])+1);
          }
          //tep_redirect(tep_href_link($goto));
          tep_redirect(tep_href_link($goto, $cart_parameters, $cart_request));
        }
        tep_redirect(tep_href_link('', '', 'NONSSL', false), '301');
        break;

      case 'notify':
        if (tep_session_is_registered('customer_id')) {
          if (isset($_GET['products_id'])) {
            $notify = $_GET['products_id'];
          } elseif (isset($_GET['notify'])) {
            $notify = $_GET['notify'];
          } elseif (isset($_POST['notify'])) {
            $notify = $_POST['notify'];
          } else {
            tep_redirect(tep_href_link(basename($PHP_SELF)));
          }
          if (!is_array($notify)) $notify = array($notify);
          for ($i=0, $n=sizeof($notify); $i<$n; $i++) {
            $check_query = tep_db_query("select count(*) as count from " . TABLE_PRODUCTS_NOTIFICATIONS . " where products_id = '" . (int)$notify[$i] . "' and customers_id = '" . (int)$customer_id . "'");
            $check = tep_db_fetch_array($check_query);
            if ($check['count'] < 1) {
              tep_db_query("insert into " . TABLE_PRODUCTS_NOTIFICATIONS . " (products_id, customers_id, date_added) values ('" . (int)$notify[$i] . "', '" . (int)$customer_id . "', now())");
            }
          }
          tep_redirect(tep_href_link(basename($PHP_SELF)));
        } else {
          $navigation->set_snapshot();
          tep_redirect(tep_href_link(FILENAME_LOGIN, '', 'SSL'));
        }
        break;
      case 'notify_remove':
        if (tep_session_is_registered('customer_id') && isset($_GET['products_id'])) {
          $check_query = tep_db_query("select count(*) as count from " . TABLE_PRODUCTS_NOTIFICATIONS . " where products_id = '" . (int)$_GET['products_id'] . "' and customers_id = '" . (int)$customer_id . "'");
          $check = tep_db_fetch_array($check_query);
          if ($check['count'] > 0) {
            tep_db_query("delete from " . TABLE_PRODUCTS_NOTIFICATIONS . " where products_id = '" . (int)$_GET['products_id'] . "' and customers_id = '" . (int)$customer_id . "'");
          }
          tep_redirect(tep_href_link(basename($PHP_SELF)));
        } else {
          $navigation->set_snapshot();
          tep_redirect(tep_href_link(FILENAME_LOGIN, '', 'SSL'));
        }
        tep_redirect(tep_href_link('', '', 'NONSSL', false), '301');
        break;
      case 'cust_order' :
        if (tep_session_is_registered('customer_id') && isset($_GET['pid'])) {
          if( tep_has_product_attributes($_GET['pid'])) {
            tep_redirect(tep_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . $_GET['pid']));
          } else {
            $cart->add_cart($_GET['pid'], $cart->get_quantity($_GET['pid'])+1);
          }
          tep_redirect(tep_href_link($goto));
        }
        tep_redirect(tep_href_link('', '', 'NONSSL', false), '301');
        break;
      default:
        break;
    }
  }
?>