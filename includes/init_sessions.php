<?php
/*
This file is part of includes/application_top.php v1.280

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

//----------------------------------------------------------------------------
// Copyright (c) 2006-2008 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Catalog: Session initialization process
//----------------------------------------------------------------------------
// I-Metrics Layer
//----------------------------------------------------------------------------
// Session initialization intends to validate and initiate a php session:
// The following modifications made:
// - Removed default GET/POST/COOKIE session setting to improve security
// - Added session validation on $POST. Failure initiates redirection
// - Added ECommerce, Maintenance modes
// - Send a session cookie if a session is started and no cookie is sent.
// - Added dedicated spider cookie.
// - Added HTML Cache
// - Added external script support for 3rd party calls like gateway IPNs.
// Use the $g_external = true; at the top of the 3rd party file to 
// immediately enforce a session startup.
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
  // If forcing cookies, disable transparent sessions to make sure the application fully controls sessions
  if (SESSION_FORCE_COOKIE_USE == 'True') {
    ini_set("url_rewriter.tags","");
    ini_set('session.use_trans_sid',0);
  }

// define how the session functions will be used
  require(DIR_WS_FUNCTIONS . 'sessions.php');

  // Iniialize handlers and session lifetime
  $SESS_LIFE = MAX_CATALOG_SESSION_TIME;
  if( SESSION_PHP_HANDLING == 'true') {
    session_set_save_handler('_sess_open', '_sess_close', '_sess_read', '_sess_write', '_sess_destroy', '_sess_gc');
  }

// set the session name and save path
  tep_session_name('osCsid');
  tep_session_save_path(SESSION_WRITE_DIRECTORY);

// set the session cookie parameters
  if (function_exists('session_set_cookie_params')) {
    session_set_cookie_params(0, $cookie_path, $cookie_domain);
  } elseif (function_exists('ini_set')) {
    ini_set('session.cookie_lifetime', '0');
    ini_set('session.cookie_path', $cookie_path);
    ini_set('session.cookie_domain', $cookie_domain);
  }


  if( DEFAULT_ECOMMERCE_MODE == 'maintenance' && basename($PHP_SELF) != FILENAME_DEFAULT) {
    tep_redirect(tep_href_link('', '', 'NONSSL', false));
  }

  unset($g_sid);
  if( isset($_COOKIE[tep_session_name()]) ) {
    $g_sid = $_COOKIE[tep_session_name()];
  } elseif( isset($_POST[tep_session_name()]) ) {
    $g_sid = $_POST[tep_session_name()];
  } elseif( isset($_GET[tep_session_name()]) ) {
    $g_sid = $_GET[tep_session_name()];
  }

  if( isset($g_sid) ) {
    $check_query = tep_db_query("select sesskey from " . TABLE_SESSIONS . " where sesskey = '" . tep_db_input(tep_db_prepare_input($g_sid)) . "'");
    if( !tep_db_num_rows($check_query) ) {
      unset($g_sid);
    }
  }

  if( count($_POST) && !isset($g_external) && !isset($g_sid) ) {
    if( SESSION_FORCE_COOKIE_USE == 'True' && isset($_COOKIE['cookie_test']) ) {
    } else {
      tep_redirect(tep_href_link(FILENAME_COOKIE_USAGE, '', 'NONSSL', false));
      //tep_redirect(tep_href_link('', '', 'NONSSL', false));
    }
  }

/*
  if( count($_POST) && (!isset($g_external) || $g_external != true) ) {
    if (SESSION_FORCE_COOKIE_USE == 'True') {
      if( !isset($_COOKIE[tep_session_name()]) ) {
        tep_redirect(tep_href_link('', '', 'NONSSL', false));
      }
      $g_sid = $_COOKIE[tep_session_name()];
    } elseif( isset($_COOKIE[tep_session_name()]) ) {
      $g_sid = $_COOKIE[tep_session_name()];
    } elseif( isset($_POST[tep_session_name()]) ) {
      $g_sid = $_POST[tep_session_name()];
    } elseif( isset($_GET[tep_session_name()]) ) {
      $g_sid = $_GET[tep_session_name()];
    } else {
      tep_redirect(tep_href_link('', '', 'NONSSL', false));
    }

    $check_query = tep_db_query("select sesskey from " . TABLE_SESSIONS . " where sesskey = '" . tep_db_input(tep_db_prepare_input($g_sid)) . "'");
    if( !tep_db_num_rows($check_query) ) {
      tep_redirect(tep_href_link('', '', 'NONSSL', false));
    }
  }
*/

// start the session
  if( DEFAULT_ECOMMERCE_MODE != 'ecommerce') {
    $spider_flag = true;
    //-MS- Enable Global cache when no session is present
    tep_check_modified_header();
    //-MS- Spiders Cache Check when no session is present EOM
  } else {
    if (isset($_COOKIE['spider'])) {
      //-MS- Enable Global cache when no session is present
      tep_check_modified_header();
      //-MS- Spiders Cache Check when no session is present EOM
      $spider_flag = true;
      $user_agent = strtolower(getenv('HTTP_USER_AGENT'));
      $spider_name = $user_agent;
      $g_cookie = true;
    } elseif ( isset($_COOKIE['cookie_test']) ) {
//    } elseif ( isset($_COOKIE['cookie_test']) || isset($_COOKIE[tep_session_name()]) ) {
      tep_session_start();
      $session_started = true;
      $g_cookie = true;
    } else {
      $user_agent = strtolower(getenv('HTTP_USER_AGENT'));
      if (tep_not_null($user_agent) ) {
        $spiders = file(DIR_WS_INCLUDES . 'spiders.txt');

        for ($i=0, $n=sizeof($spiders); $i<$n; $i++) {
          if (tep_not_null($spiders[$i])) {
            if (is_integer(strpos($user_agent, trim($spiders[$i])))) {
              tep_setcookie('spider', 'gotya', time()+60*60*24*30, $cookie_path, $cookie_domain);
              //-MS- Enable Global cache when no session is present
              tep_check_modified_header();
              //-MS- Spiders Cache Check when no session is present EOM
              $spider_name = $user_agent;
              $spider_flag = true;
              break;
            }
          }
        }
      }

      if($spider_flag == false) {
        if (SESSION_FORCE_COOKIE_USE == 'True') {
          tep_setcookie('cookie_test', 'session_cookie_required', time()+60*60*24*30, $cookie_path, $cookie_domain);

          //-MS- Handle 3rd party requests where a session must start on the first call
          //if( isset($g_external) && $g_external == true ) {
            tep_session_start();
            $session_started = true;
          //}
        } else  {
          tep_session_start();
          $session_started = true;
        }
      }
    }
  }

// Initialize SID. The SID definition is setup by PHP automatically when cookies are active should be left empty
  $SID = '';

//-MS- Accept POST vars with session only
  if( count($_POST) && $session_started == false ) {
    //tep_redirect(tep_href_link('', '', 'NONSSL', false));
    tep_redirect(tep_href_link(FILENAME_COOKIE_USAGE, '', 'NONSSL', false));
  }
//-MS- Accept POST vars with session only EOM

// If the initial cookie is not send re-send it.
  if( $session_started == true ){

    if( tep_session_is_registered('customer_id') ) {
      $check_query = tep_db_query("select customers_id from " . TABLE_CUSTOMERS . " where customers_id = '" . (int)$customer_id . "'");
      if( !tep_db_num_rows($check_query) ) {
        tep_session_destroy();
        tep_redirect(tep_href_link(FILENAME_COOKIE_USAGE, '', 'NONSSL', false));
      }
    }

    if( !isset($_COOKIE[tep_session_name()]) ) {
      tep_setcookie(tep_session_name(), tep_session_id(), 0, $cookie_path, $cookie_domain);
// Set SID once, even if empty. The SID definition is setup by PHP automatically
      if( defined('SID') ) {
        $SID = SID;
      }
    }
  }
?>