<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2007-2011 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Catalog: For auction callbacks - minimum application initialization
//----------------------------------------------------------------------------
// I-Metrics Layer
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
    // set the level of error reporting
//  error_reporting(E_ALL & ~E_NOTICE);
  if( !strlen(ini_get('date.timezone')) && function_exists('date_default_timezone_get')) {
    date_default_timezone_set(@date_default_timezone_get());
  }
  @setlocale(LC_TIME, 'en_US.UTF-8');

  ini_set('error_reporting', E_ALL);
  ini_set('display_errors', 1);

  if($_SERVER['REQUEST_METHOD'] != "GET" && $_SERVER['REQUEST_METHOD'] != "POST" ) {
    header("HTTP/1.1 405");
    header("Allow: GET, POST");
    exit();
  }

  require_once('includes/configure.php');

  $g_ajax = isset($_SERVER['HTTP_X_REQUESTED_WITH'])?true:false;

  if( !$g_ajax ) {
    exit();
  }

  $session_name = 'osCsid';
  if( !isset($_COOKIE[$session_name]) ) {
    exit();
  }

// some code to solve compatibility issues
  require_once(DIR_WS_FUNCTIONS . 'compatibility.php');

  require_once(DIR_WS_FUNCTIONS . 'general.php');
  // include the list of project database tables
  require_once(DIR_WS_INCLUDES . 'database_tables.php');
  // include the database functions
  require_once(DIR_WS_FUNCTIONS . 'database.php');

  // make a connection to the database... now
  tep_db_connect() or die('Unable to connect to database server!');

  $g_script = $PHP_SELF = tep_sanitize_string(basename($_SERVER['SCRIPT_NAME']));
  $languages_id = 1;

  $strings_file = 'includes/languages/english/' . $g_script;
  if( is_file($strings_file) ) {
    require_once($strings_file);
  }
  require(DIR_WS_FUNCTIONS . 'auctions.php');

  $check_query = tep_db_query("select value from " . TABLE_SESSIONS . " where sesskey = '" . tep_db_input($_COOKIE[$session_name]) . "' and expiry > '" . time() . "'");
  if( !tep_db_num_rows($check_query) ) {
    if( time()%10 ) {
      exit();
    }
    $output_result_array = array(
      'critical' => array( '<span class="heavy mark_red">' . sprintf(ERROR_AUCTION_SESSION_EXPIRED) . '</span>')
    );
    tep_auction_format_callback($output_result_array, true, true);
    //exit();
  }

  $check_array = tep_db_fetch_array($check_query);
  $session = unserialize($check_array['value']);
  $customer_id = isset($session['customer_id'])?(int)$session['customer_id']:0;
  $sticky_poll = isset($session['sticky_poll'])?(int)$session['sticky_poll']:1;
  $action = (isset($_GET['action']))?tep_sanitize_string($_GET['action']):'';
?>
