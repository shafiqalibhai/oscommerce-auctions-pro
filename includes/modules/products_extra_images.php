<?php
  /*
  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License
  
  Based on Extra images 1.4 from Mikel Williams
  Thanks to Mikel Williams, StuBo, moosey_jude and Randelia
  Modifications: Xav xpavyfr@yahoo.fr
  Further modifications: Sabaina Bukhari (sabainabukhari@yahoo.com)

  */
  $products_extra_images_query = tep_db_query("SELECT products_extra_image, products_extra_images_id FROM " . TABLE_PRODUCTS_EXTRA_IMAGES . " WHERE products_id='" . $product_info['products_id'] . "'");
  if (tep_db_num_rows($products_extra_images_query) >= 1){
    $rowcount_value=4;  //number of extra images per row    
    $rowcount=1;
?>
    <tr>
      <td class="smallText">Additional Images</td>
    </tr>

    <tr>
      <td class="b_borders"><table border="0" width="100%" cellspacing="0" cellpadding="0">
        <tr>
<?php   
                    
    //$products_extra_images_query = tep_db_query("SELECT products_extra_image, products_extra_images_id FROM " . TABLE_PRODUCTS_EXTRA_IMAGES . " WHERE products_id='" . $product_info['products_id'] . "'");
    while ($extra_images = tep_db_fetch_array($products_extra_images_query)) {
?>
          <td>
<script language="javascript"><!--
document.write('<?php echo '<a href="javascript:popupWindow(\\\'' . tep_href_link(FILENAME_POPUP_EXTRA_IMAGES, 'peiID=' . $extra_images['products_extra_images_id']) . '\\\')">' . tep_image(DIR_WS_IMAGES . $extra_images['products_extra_image'], addslashes($product_info['products_name']), SMALL_IMAGE_WIDTH/2, SMALL_IMAGE_HEIGHT/2) . '<\/a>'; ?>');
//--></script>
<noscript>
<?php echo '<a href="' . tep_href_link(DIR_WS_IMAGES . $extra_images['products_extra_images_id']) . '" target="_blank">' . tep_image(DIR_WS_IMAGES . $extra_images['products_extra_images_id'], $product_info['products_name'], SMALL_IMAGE_WIDTH/2, SMALL_IMAGE_HEIGHT/2) . '</a>'; ?>
</noscript>

<?php 
/*
      echo "<a href=\"javascript:popupWindow('" . tep_href_link(FILENAME_POPUP_EXTRA_IMAGES, 'peiID=' . $extra_images['products_extra_images_id']) . "')\">" . tep_image(DIR_WS_IMAGES . $extra_images['products_extra_image'], addslashes($product_info['products_name']),SMALL_IMAGE_WIDTH/2, SMALL_IMAGE_HEIGHT/2, "hspace='5' vspace='5'". " onMouseOver=\"javascript:window.document.prodimg.src='" . DIR_WS_IMAGES . $extra_images['products_extra_image'] . "'\"") . '</a>'; 
*/
?>
          </td>
<?php
      if ($rowcount == $rowcount_value) {
        echo '</tr><tr>'; $rowcount=1;
      } else {
        $rowcount=$rowcount+1;
      }
    }
?>  
        </tr>
      </table></td>
    </tr>

<?php
}
?>    
