<?php
/*
  $Id: product_listing.php,v 1.44 2003/06/09 22:49:59 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

//----------------------------------------------------------------------------
// Copyright (c) 2007 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Catalog: Account page
//----------------------------------------------------------------------------
// I-Metrics Variant
//----------------------------------------------------------------------------
// Modifications:
// - 07/05/2007: PHP5 Register Globals and Long Arrays Off support added
// - 07/08/2007: PHP5 Long Arrays Off support added
// - 07/12/2007: Moved HTML Header/Footer to a common section
// - 08/31/2007: HTML Body Common Sections Added
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
?>
  <table border="0" width="100%" cellspacing="0" cellpadding="0">  
<?php
/*
  $listing_filter_array = array(
                                array('id' => '1a', 'text' => 'Price (High to Low)'),
                                array('id' => '1b', 'text' => 'Price (Low to High)'),
                                array('id' => '4a', 'text' => 'Name'),
                              );

  $listing_filter_flat_array = tep_array_invert_from_element($listing_filter_array, 'id', 'text');
  if(!isset($_POST['filter_id']) ) {
    $filter_id = '4a';
  } else {
    $filter_id = $_POST['filter_id'];
  }

  if( !isset($listing_filter_flat_array[$filter_id]) ) {
    $filter_id = '4a';
  }

  $order_by = '';
  switch($filter_id) {
    case '1a':
      $order_by = 'p.products_price desc, p.products_status desc';
      break;
    case '1b':
      $order_by = 'p.products_price asc, p.products_status desc';
      break;
    default:
      break;
  }
*/
  $order_by = 'p.products_price desc, p.products_status desc';
  $split_params = '';
  if( tep_not_null($cPath) ) {
    $split_params = 'cPath=' . $cPath;
  }

  $params_array = $g_filters->get_filters_db_names();
  $listing_sql = tep_get_collection($current_category_id, $params_array, false, true, '', $order_by);
  unset($params_array);
  $listing_split = new splitPageResults($listing_sql, MAX_DISPLAY_SEARCH_RESULTS, 'p.products_id');
  if ( ($listing_split->number_of_rows > 0) && ( (PREV_NEXT_BAR_LOCATION == '1') || (PREV_NEXT_BAR_LOCATION == '3') ) ) {
?>
    <tr>
      <td class="all_borders"><table border="0" width="100%" cellspacing="0" cellpadding="2">
        <tr>
          <td class="smallText"><?php echo $listing_split->display_count(TEXT_DISPLAY_NUMBER_OF_PRODUCTS); ?></td>
          <td class="smallText" align="right"><?php echo TEXT_RESULT_PAGE . ' ' . $listing_split->display_links(MAX_DISPLAY_PAGE_LINKS, $split_params); ?></td>
        </tr>
      </table></td>
    </tr>
<?php
  }

  // Initialize vars
  $total_items = array();
  tep_query_to_array($listing_split->sql_query, $total_items);

  $j=count($total_items);

  if( $j > 0 ) {
?>
    <tr>
      <td><table border="0" width="100%" cellspacing="0" cellpadding="0">
        <tr>
<?php
    $keyword = tep_create_safe_string(tep_get_category_name($current_category_id));
    $info_box_contents = array();
    $info_box_contents[] = array('text' => '<h1>' . $keyword . '</h1>');

    echo '      <td class="hpadder">';
    new contentBoxHeading($info_box_contents);
    echo '      </td>';

    for($i=0, $j=count($total_items); $i<$j; $i++ ) {
      $total_items[$i]['products_name'] = tep_get_products_name($total_items[$i]['products_id']);
      $total_items[$i]['products_name'] = tep_create_safe_string($total_items[$i]['products_name']);
    }
?>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td>
<?php
    require('includes/objects/product_listing_common.php');
?>
        </td>
      </tr>
<?php
  } else {
?>
      <tr>
        <td><table border="0" width="100%" cellspacing="0" cellpadding="2">
          <tr>
            <td class="main"><?php echo TEXT_NO_PRODUCTS; ?></td>
          </tr>
          <tr>
            <td><?php echo tep_draw_separator('pixel_trans.gif', '100%', '10'); ?></td>
          </tr>
        </table></td>
      </tr>
<?php
  }

  if ( ($listing_split->number_of_rows > 0) && ((PREV_NEXT_BAR_LOCATION == '2') || (PREV_NEXT_BAR_LOCATION == '3')) ) {
?>
    <tr>
      <td><?php echo tep_draw_separator('pixel_trans.gif', '100%', '4'); ?></td>
    </tr>

    <tr>
      <td><table border="0" width="100%" cellspacing="0" cellpadding="2">
        <tr>
          <td class="smallText"><?php echo $listing_split->display_count(TEXT_DISPLAY_NUMBER_OF_PRODUCTS); ?></td>
          <td class="smallText" align="right"><?php echo TEXT_RESULT_PAGE . ' ' . $listing_split->display_links(MAX_DISPLAY_PAGE_LINKS, $split_params); ?></td>
        </tr>
      </table></td>
    </tr>
<?php
  }
?>
  </table>