<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2007 Asymmetric Software. Innovation & Excellence.
// Author: Mark Samios
// http://www.asymmetrics.com
//----------------------------------------------------------------------------
// Catalog: Other Module
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
------------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
//
*/
?>
          <tr>
            <td>
<?php
  $info_box_contents = array();
  $info_box_contents[] = array(
                               'params' => 'height="20" class="header_dark"',
                               'text' => 'Customer Service'
                              );
  //$info_box_contents[] = array('text' => BOX_HEADING_INFORMATION);

  //new contentBoxHeading($info_box_contents, false, false);

  $info_box_contents = array();
  $text_query = tep_db_query("select gtext_id from " . TABLE_GTEXT . " where gtext_title = 'Privacy Notice'");
  if( $text_array = tep_db_fetch_array($text_query) ) {
    $info_box_contents[] = array('text' => '<a href="' . tep_href_link(FILENAME_GENERIC_PAGES, 'gtext_id=' . $text_array['gtext_id']) . '" class="contentBoxContents" rel="nofollow">' .  'PRIVACY POLICY' . '</a>');
  }

  $text_query = tep_db_query("select gtext_id from " . TABLE_GTEXT . " where gtext_title = 'Terms of Use'");
  if( $text_array = tep_db_fetch_array($text_query) ) {
    $info_box_contents[] = array('text' => '<a href="' . tep_href_link(FILENAME_GENERIC_PAGES, 'gtext_id=' . $text_array['gtext_id']) . '" class="contentBoxContents" rel="nofollow">' . 'TERMS &amp; CONDITIONS' . '</a>');
  }

  $text_query = tep_db_query("select gtext_id from " . TABLE_GTEXT . " where gtext_title = 'Return Policy'");
  if( $text_array = tep_db_fetch_array($text_query) ) {
    $info_box_contents[] = array('text' => '<a href="' . tep_href_link(FILENAME_GENERIC_PAGES, 'gtext_id=' . $text_array['gtext_id']) . '" class="contentBoxContents" rel="nofollow">' . 'SHIPPING POLICY' . '</a>');
  }

  new contentBox($info_box_contents);
?>
            </td>
          </tr>