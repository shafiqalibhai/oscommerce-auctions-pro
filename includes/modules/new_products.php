<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2009 Asymmetric Software. Innovation & Excellence.
// Author: Mark Samios
// http://www.asymmetrics.com
//----------------------------------------------------------------------------
// Catalog: New Products Module driven by abstract zones
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
------------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
    $g_products_per_row = 3;

    $db_names_array = $g_filters->get_filters_db_names();
    $cProduct = new product_front();
    $new_products_array = $cProduct->get_group_products(DEFAULT_FRONT_ABSTRACT_ZONE_ID);
    $description_array = $cProduct->get_zone_data(DEFAULT_FRONT_ABSTRACT_ZONE_ID);
    $keys_array = array_keys($new_products_array);

    $listing_sql = tep_get_collection($current_category_id, $db_names_array, false, true, $keys_array);
    $listing_sql .= ' limit ' . MAX_DISPLAY_NEW_PRODUCTS;

    $tmp_array = $total_items = array();

    tep_query_to_array($listing_sql, $total_items, 'products_id');
    $keys_array = array_flip($keys_array);

    if( count($keys_array) == count($total_items) ) {
      foreach($keys_array as $key => $value) {
        if( isset($total_items[$key]) ) {
          $tmp_array[] = $total_items[$key];
        }
      }
      $total_items = $tmp_array;
    } else {
      $total_items = array_values($total_items);
    }

    unset($tmp_array, $keys_array, $new_products_array);
    $j = count($total_items);
    if( $j ) {
      $info_box_contents = array();
      $info_box_contents[] = array('text' => '<h2>' . $description_array['abstract_zone_name'] . '</h2>');

      new contentBoxHeading($info_box_contents);
      require('includes/objects/product_listing_common.php');
    }
?>
