<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2007 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
//----------------------------------------------------------------------------
// Products associate module for generic text pages for Catalog end
// Front End support for the generic text pages
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
?>
<!-- text_products //-->
<?php
  define('MAX_DISPLAY_TEXT_PER_ROW', '2');
  $abz_id = isset($_GET['abz_id'])?$_GET['abz_id']:0;
  $gtext_id = isset($_GET['gtext_id'])?$_GET['gtext_id']:0;

  $generic_query = tep_db_query("select gtext_title from " . TABLE_GTEXT . " where gtext_id='" . (int)$gtext_id . "'");
  if( tep_db_num_rows($generic_query) ) {
    $generic_array = tep_db_fetch_array($generic_query);
  } else {
    $generic_array = array('generic_title' => '');
  }

  $cText = new gtext_front();
  $products_array = $cText->get_gtext_products($abz_id, $gtext_id);
  $j = count($products_array);
  if( $j ) {

    $info_box_contents = array();
    //  $info_box_contents[] = array('text' => sprintf(TABLE_HEADING_NEW_PRODUCTS, strftime('%B')));
    $info_box_contents[] = array('text' => $generic_array['gtext_title'] . '<br>');
    echo '      <tr><td>';
    new contentBoxHeading($info_box_contents);
    echo '      </td></tr>';
    $total_items = array_values($products_array);
?>
      <tr>
        <td>
<?php
      require('includes/objects/product_listing_common.php');
?>
        </td>
      </tr>
<?php
  }
?>

