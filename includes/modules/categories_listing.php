<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2007 Asymmetric Software. Innovation & Excellence.
// Author: Mark Samios
// http://www.asymmetrics.com
// ---------------------------------------------------------------------------
// Catalog: Categories Listings module
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
  if( isset($current_category_id) ) {
    $categories_query = "select c.categories_id, cd.categories_name, c.logo_image from " . TABLE_CATEGORIES . " c left join " . TABLE_CATEGORIES_DESCRIPTION . " cd on (c.categories_id=cd.categories_id) where c.parent_id = '" . (int)$current_category_id . "' and cd.language_id = '" . (int)$languages_id . "' order by sort_order, cd.categories_name";
  } else {
    $categories_query = "select c.categories_id, cd.categories_name, c.logo_image from " . TABLE_CATEGORIES . " c left join " . TABLE_CATEGORIES_DESCRIPTION . " cd on (c.categories_id=cd.categories_id) where c.parent_id = '0' and cd.language_id = '" . (int)$languages_id . "' order by sort_order, cd.categories_name";
  }
  // Initialize vars
  $row0 = $row1 = $row2 = $row3 = $row4 = '';
  $col = 0;
  $final_string = '';
  $total_items = array();

  tep_query_to_array($categories_query, $total_items);
  $j = count($total_items);

  if( $j > 0 ) {
?>
      <tr>
        <td>
<?php
      // Prepare segment width
    $space_width = MAX_DISPLAY_NEW_SPACER;
    if($space_width <= 0 )
      $space_width = 1;

    $width = (int)((100 / MAX_DISPLAY_CATEGORIES_PER_ROW) - $space_width);
    $column_count = MAX_DISPLAY_CATEGORIES_PER_ROW;
    for( $i=0, $j; $i<$j; $i++ ) {
      $pictures = '<td align="center" valign="top"><a href="' . tep_href_link(FILENAME_DEFAULT, 'cPath=' . tep_get_category_path($total_items[$i]['categories_id']) ) . '">' . tep_image(DIR_WS_IMAGES . $total_items[$i]['logo_image'], $total_items[$i]['categories_name'], SMALL_IMAGE_WIDTH, SMALL_IMAGE_HEIGHT) . '</a></td>' . "\n";

      $row0 .= '<td class="pad_bf6">&nbsp;</td>' . "\n";
      $row1 .= $pictures;

      if( is_int($i / $column_count) && $i < $j && $i ) {
        $final_string .= '<tr><td colspan="' . (3*MAX_DISPLAY_CATEGORIES_PER_ROW) . '" class="borderLiteTop">&nbsp;</td></tr>' . "\n";
      }

      if( !$col ) {
         //$row1 = '<td width="' . $space_width . '%" />' . $row1 . "\n";
         //$row2 = '<td width="' . $space_width . '%" />' . $row2 . "\n";
         //$row3 = '<td width="' . $space_width . '%" />' . $row3 . "\n";
      }
      $col++;
      if( !is_int($col / $column_count) && $i < $j-1 ) {
        $row0 = $row0 . '<td class="borderLiteRight">&nbsp;</td>' . '<td class="pad_l2" />' . "\n";
        $row1 = $row1 . '<td class="borderLiteRight">&nbsp;</td>' . '<td class="pad_l2" />' . "\n";
      } elseif( is_int( ($col+1) / $column_count) && $i < $j-1 ) {
        // Do nothing
      } else {
        // Setup the rest of cells
        $last_col = false;
        $col_org = $col;
        while( !is_int($col / $column_count) ) {
          if( !$last_col && $col < $column_count ) {
            $last_col = true;
          } else {
          }
          $row0 .= '<td /><td>&nbsp;</td>';
          $col++;
        }
        $final_string .= '<tr>' . $row0 . '</tr>' . "\n";
        $final_string .= '<tr>' . $row1 . '</tr>' . "\n";
        $final_string .= '<tr>' . $row0 . '</tr>' . "\n";
        $final_string .= '<tr><td colspan="' . (3*MAX_DISPLAY_CATEGORIES_PER_ROW) . '" class="pad_bf6">&nbsp;</td></tr>' . "\n";

/*
        $final_string .= '<tr>' . "\n";
        for( $x=0; $x < $col_org; $x++ ) {
          if( !$x ) {
            $final_string .= '<td width="' . $space_width . '%" />';
          }
          if( $x < $column_count-1 )
          $final_string .= '<td /><td class="borderLiteRight">&nbsp;</td>' . '<td width="' . $space_width . '%">&nbsp;</td>' . "\n";
        }
        $final_string .= '</tr>' . "\n";
*/

        //$final_string .= '<tr>' . $row4 . '</tr>' . "\n";
        //$final_string .= '<tr><td colspan="' . (6*MAX_DISPLAY_CATEGORIES_PER_ROW+3) . '" class="borderLiteTop">&nbsp;</td></tr>' . "\n";

        $row0 = $row1 = $row2 = $row3 = $row4 = '';
        $col = 0;
      }
    }
    $final_string = '<table border="0" width="100%" cellspacing="0" cellpadding="0" class="smallText">' . $final_string . '</table>';
    $info_box_contents = array();
    $info_box_contents[] = array(
                                 'params' => 'class="smallText"',
                                 'text' => $final_string
                                );

    new contentBox($info_box_contents);
?>
        </td>
      </tr>
<?php
  }
?>
