<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2007 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Similar products module
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
  $similar_flag = false;
  $assigned_flag = false;
  $similar_query = tep_db_query("select p2s.similar_id, pd.products_name, p.products_image from " . TABLE_PRODUCTS_TO_SIMILAR_PRODUCTS . " p2s left join " . TABLE_PRODUCTS . " p on (p2s.similar_id = p.products_id) left join " . TABLE_PRODUCTS_DESCRIPTION . " pd on (p2s.similar_id=pd.products_id) where p.products_status='1' and p.products_display='1' and p2s.products_id = '" . (int)$current_product_id . "' and pd.language_id = '" . (int)$languages_id . "' limit 4");
  if( !tep_db_num_rows($similar_query) ) {
    $categories_array = array();
    $path = tep_get_product_path( $current_product_id);
    $product_path = explode('_', $path);
    for($i=count($product_path); $i>0; $i--) {
      $id = $product_path[$i-1];
      $similar_query = tep_db_query("select distinct p2c.products_id as similar_id, pd.products_name, p.products_image from " . TABLE_PRODUCTS_TO_CATEGORIES . " p2c left join " . TABLE_PRODUCTS . " p on (p2c.products_id = p.products_id) left join " . TABLE_PRODUCTS_DESCRIPTION . " pd on (p2c.products_id=pd.products_id) where p.products_status='1' and p2c.categories_id = '" . (int)$id . "' and p.products_id <> '" . (int)$current_product_id . "' and pd.language_id = '" . (int)$languages_id . "' order by rand() limit 4");
      if( tep_db_num_rows($similar_query) ) {        
        break;
      }
    }
  } else {
    $assigned_flag = true;
  }
  if( tep_db_num_rows($similar_query) ) {
    $similar_flag = true;
?>
            <tr>
              <td><?php echo tep_draw_separator('pixel_trans.gif', '100%', '10'); ?></td>
            </tr>
            <tr>
<?php
    $links_string = '<td align="center" class="all_borders"><table width="100%" border="0" cellpadding="0" cellspacing="0"><tr><td>';
    while($similar = tep_db_fetch_array($similar_query)) {
      //$links_string .= '<a href="' . tep_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . $similar['similar_id'], 'NONSSL') . '">' . tep_image(DIR_WS_IMAGES . $similar['products_image'], $similar['products_name'], '175', '125') . '</a><br><br><a href="' . tep_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . $similar['products_id']) . '">' . $similar['products_name'] . '</a>';
      $links_string .= '<a href="' . tep_href_link(FILENAME_PRODUCT_INFO, 'products_id=' . $similar['similar_id']) . '">' . $similar['products_name'] . '</a> - ';
    }
    $links_string = substr($links_string, 0, -2);
    $links_string .= '</td></tr></table></td>' . "\n";
    echo $links_string;
?>
            </tr>
            <tr>
              <td><?php echo tep_draw_separator('pixel_trans.gif', '100%', '10'); ?></td>
            </tr>
<?php
  }
?>
