<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2011 Asymmetric Software. Innovation & Excellence.
// Author: Mark Samios
// http://www.asymmetrics.com
//----------------------------------------------------------------------------
// Catalog: Contact Side module
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
------------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
?>
          <div id="address_right" class="extend">
<?php
  $info_box_contents = array();
  $info_box_contents[] = array(
   'text' => 'CUSTOMER SERVICE',
   'class' => 'heavy contentBoxHeadingRight'
  );
  new contentBoxHeading($info_box_contents);

  $info_box_contents = array();
  $info_box_contents[] = array(
    'text' => tep_image(DIR_WS_IMAGES . 'design/customer_service.jpg', 'Customer Service', 250, 250),
    'class' => 'calign vpad'
  );

  $info_box_contents[] = array(
    'class' => 'account_bullet',
    'text' => '<a href="' . tep_href_link(FILENAME_ACCOUNT_EDIT, '', 'SSL') . '">' . 'ACCOUNT INFORMATION' . '</a>'
  );
  $info_box_contents[] = array(
    'class' => 'account_bullet',
    'text' => '<a href="' . tep_href_link(FILENAME_ACCOUNT_HISTORY, '', 'SSL') . '">' . 'ACCOUNT ORDERS' . '</a>'
  );
  $info_box_contents[] = array(
    'class' => 'account_bullet',
    'text' => '<a href="' . tep_href_link(FILENAME_ADDRESS_BOOK) . '">' . 'PERSONAL ADDRESS BOOK' . '</a>'
  );
  $info_box_contents[] = array(
    'class' => 'account_bullet',
    'text' => '<a href="' . tep_href_link(FILENAME_CONTACT_US) . '">' . 'CONTACT US' . '</a>'
  );

  new contentBox($info_box_contents, 'contentBoxContents');
?>
          </div>