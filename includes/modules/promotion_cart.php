<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2007 Asymmetric Software. Innovation & Excellence.
// Author: Mark Samios
// http://www.asymmetrics.com
//----------------------------------------------------------------------------
// Catalog: Brands Module
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
------------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
//
*/
?>
          <div id="scart_left" class="extend">
<?php
  $info_box_contents = array();
  $info_box_contents[] = array(
   'text' => 'SHOPPING CART',
   'class' => 'heavy contentBoxHeading'
  );
  new contentBoxHeading($info_box_contents);

  $info_box_contents = array();

  $info_box_contents[] = array(
    'text' => tep_image(DIR_WS_IMAGES . 'design/shopping_cart.jpg', 'Auction Bids and Products', 250, 250),
    'class' => 'calign'
  );

  $info_box_contents[] = array(
    'text' => TEXT_INFO_INTRO,
  );
  new contentBox($info_box_contents, 'contentBoxContents');

?>
          </div>