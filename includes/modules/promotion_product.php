<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2006-2007 Asymmetric Software. Innovation & Excellence.
// Author: Mark Samios
// http://www.asymmetrics.com
//----------------------------------------------------------------------------
// Catalog: Brands Module
//----------------------------------------------------------------------------
// Script is intended to be used with:
// osCommerce, Open Source E-Commerce Solutions
// Copyright (c) 2003 osCommerce
------------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
//
*/
?>
          <div id="product_left" class="extend">
<?php
  $info_box_contents = array();
  $info_box_contents[] = array(
   'text' => 'PRODUCT INFORMATION',
   'class' => 'heavy contentBoxHeading'
  );
  new contentBoxHeading($info_box_contents);

  $info_box_contents = array();
  $info_box_contents[] = array(
    'text' => tep_image(DIR_WS_IMAGES . 'design/product_details.jpg', STORE_NAME, 220, 250),
    'class' => 'calign'
  );

  $info_box_contents[] = array(
    'text' => TEXT_INFO_INTRO,
    'style' => 'padding: 8px 0px; color: #FFF',
  );
  new contentBox($info_box_contents, 'contentBoxContents');
?>
          </div>