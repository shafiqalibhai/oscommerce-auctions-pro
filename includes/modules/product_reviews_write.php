<?php
/*
//----------------------------------------------------------------------------
// Copyright (c) 2007 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Catalog: Reviews Write module
//----------------------------------------------------------------------------
// I-Metrics Layer
//----------------------------------------------------------------------------
// Modifications:
// - 07/05/2007: PHP5 Register Globals and Long Arrays Off support added
// - 07/08/2007: PHP5 Long Arrays Off support added
// - 07/12/2007: Moved HTML Header/Footer to a common section
// - 08/31/2007: HTML Body Common Sections Added
//----------------------------------------------------------------------------
// Released under the GNU General Public License v3.00
//----------------------------------------------------------------------------
*/

define('BOX_REVIEWS_HEADER_TEXT_LEAVE', 'Leave a Comment for ' . $product_info['products_name']);
define('SUB_TITLE_RATING', 'Rating:&nbsp;&nbsp;&nbsp;&nbsp;');
define('TEXT_BAD', '<small><font color="#ff0000"><b>BAD</b></font></small>');
define('TEXT_GOOD', '<small><font color="#ff0000"><b>GOOD</b></font></small>');
define('TEXT_LIMIT', '<b><font color="#ff0000">Note:</font> Use our <a href="' . tep_href_link(FILENAME_CONTACT_US) . '" rel="nofollow"><font color="#ff0000">contact us form</font></a> for more information about <font color="#ff0000">' . $product_info['products_name'] . '</font> instead of leaving a question in the review section. Thank You.</b>');
?>
        <tr>
          <td>
<?php
  $info_box_header = array();
  $info_box_header[] = array('text' => strtoupper(BOX_REVIEWS_HEADER_TEXT_LEAVE));
  new contentBoxHeading($info_box_header);

  $info_box_contents = array();

//  $content = tep_draw_textarea_field('review', 'soft', 60, 5) . '<br>' . '<b>' . SUB_TITLE_RATING . '</b>' . TEXT_BAD . ' ' . tep_draw_radio_field('rating', '1') . ' ' . tep_draw_radio_field('rating', '2') . ' ' . tep_draw_radio_field('rating', '3') . ' ' . tep_draw_radio_field('rating', '4') . ' ' . tep_draw_radio_field('rating', '5') . ' ' . TEXT_GOOD;
  $form_name = 'products_info_review';
  $content_start = '<table border="0" width="100%" cellspacing="0" cellpadding="0">' . "\n" .
                   '  <tr>' . "\n" . 
                   '    <td>' . tep_draw_separator('pixel_trans.gif', '100%', '4') . '</td>' . "\n" .
                   '  </tr>' . "\n" . 
                   '  <tr>' . "\n" . 
                   '    <td>' . tep_draw_form($form_name, tep_href_link(basename($PHP_SELF), 'products_id=' . $_GET['products_id'] . '&action=process'), 'post', 'onsubmit="return checkForm();"') . "\n" . '<table border="0" width="100%" cellspacing="0" cellpadding="0">' . "\n";


  $content =       '      <tr>' . "\n" . 
                   '        <td colspan="2">' . tep_draw_textarea_field('review', 'soft', 60, 5) . '</td>' . "\n" .
                   '      </tr>' . "\n" . 
                   '      <tr>' . "\n" . 
                   '        <td colspan="2" class="main">' . TEXT_LIMIT . '</td>' . 
                   '      </tr>' . "\n" .
                   '       <tr>' . "\n" . 
                   '         <td colspan="2">' . tep_draw_separator('pixel_trans.gif', '100%', '8') . '</td>' . "\n" .
                   '      </tr>' . "\n" . 
                   '      <tr>' . "\n" . 
                   '        <td class="all_borders"><table border="0" width="100%" cellspacing="1" cellpadding="2" class="contentBox">' . "\n" . 
                   '          <tr class="contentBoxContents">' . "\n" . 
                   '            <td>' . tep_image_submit('button_review.gif', sprintf('Review %s', $product_info['products_name']), 'name="' . $form_name . '"') . '</td>' . "\n" .
                   '            <td align="right" class="main">' . '<font color="#ff0000"><b>' . SUB_TITLE_RATING . '</b></font>' . TEXT_BAD . ' ' . tep_draw_radio_field('rating', '1') . ' ' . tep_draw_radio_field('rating', '2') . ' ' . tep_draw_radio_field('rating', '3') . ' ' . tep_draw_radio_field('rating', '4') . ' ' . tep_draw_radio_field('rating', '5') . ' ' . TEXT_GOOD . '</td>' . "\n" .
                   '          </tr>' . "\n" . 
                   '        </table></td>' . "\n" . 
                   '      </tr>' . "\n";


  $content_end =   '    </table></form></td>' . "\n" . 
                   '  </tr>' . "\n" . 
                   '</table>' . "\n";

  $info_box_contents[] = array('align' => 'left',
                               'text' => $content_start . $content . $content_end
                               );
  new contentBox($info_box_contents);
?>
          </td>
        </tr>
