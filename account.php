<?php
/*
  $Id: account.php,v 1.61 2003/06/09 23:03:52 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

//----------------------------------------------------------------------------
// Copyright (c) 2007 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Catalog: Account page
//----------------------------------------------------------------------------
// I-Metrics Variant
//----------------------------------------------------------------------------
// Modifications:
// - 07/12/2007: Moved HTML Header/Footer to a common section
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/
  require('includes/application_top.php');

  if (!tep_session_is_registered('customer_id')) {
    $navigation->set_snapshot();
    tep_redirect(tep_href_link(FILENAME_LOGIN, '', 'SSL'));
  }
  require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_ACCOUNT);

  $breadcrumb->add(NAVBAR_TITLE, tep_href_link(FILENAME_ACCOUNT, '', 'SSL'));
?>
<?php require('includes/objects/html_start_sub1.php'); ?>
<?php require('includes/objects/html_start_sub2.php'); ?>
<?php
  $heading_row = true;
  require('includes/objects/html_body_header.php'); 
?>
    <div class="bounder">
      <div class="floater"><h1><?php echo HEADING_TITLE; ?></h1></div>
      <div class="floatend tmargin">
<?php
  $customer_query = tep_db_query("select customers_bids from " . TABLE_CUSTOMERS . " where customers_id = '" . (int)$customer_id . "'");
  $customer_array = tep_db_fetch_array($customer_query);
  $bids_string = '<a href="' . tep_href_link(FILENAME_DEFAULT, 'cPath=9', 'NONSSL') . '" class="deco_none"><span class="pad notice">' . sprintf(TEXT_INFO_AUCTION_BIDS_LEFT, '<span id="bids_notice">' . $customer_array['customers_bids'] . '</span>') . '</span></a>';
  echo $bids_string;
?>
      </div>
      <div class="cleaner"></div>
<?php
  if (tep_count_customer_orders() > 0) {
?>
      <div class="floater rspacer"><h2><?php echo OVERVIEW_TITLE; ?></h2></div>
      <div class="floater"><?php echo '<a href="' . tep_href_link(FILENAME_ACCOUNT_HISTORY, '', 'SSL') . '"><h2><u>' . OVERVIEW_SHOW_ALL_ORDERS . '</u></h2></a>'; ?></div>
      <div class="cleaner heavy"><?php echo OVERVIEW_PREVIOUS_ORDERS; ?></div>
      <div class="orders_overview bmargin"><table class="tabledata">
<?php
    $orders_query = tep_db_query("select o.orders_id, o.date_purchased, o.delivery_name, o.delivery_country, o.billing_name, o.billing_country, ot.text as order_total, s.orders_status_name from " . TABLE_ORDERS . " o, " . TABLE_ORDERS_TOTAL . " ot, " . TABLE_ORDERS_STATUS . " s where o.customers_id = '" . (int)$customer_id . "' and o.orders_id = ot.orders_id and ot.class = 'ot_total' and o.orders_status = s.orders_status_id and s.language_id = '" . (int)$languages_id . "' order by orders_id desc limit 3");
    while ($orders = tep_db_fetch_array($orders_query)) {
      if (tep_not_null($orders['delivery_name'])) {
        $order_name = $orders['delivery_name'];
        $order_country = $orders['delivery_country'];
      } else {
        $order_name = $orders['billing_name'];
        $order_country = $orders['billing_country'];
      }
?>
        <tr>
          <td><?php echo tep_date_short($orders['date_purchased']); ?></td>
          <td><?php echo '#' . $orders['orders_id']; ?></td>
          <td><?php echo tep_output_string_protected($order_name) . ', ' . $order_country; ?></td>
          <td><?php echo $orders['orders_status_name']; ?></td>
          <td><?php echo $orders['order_total']; ?></td>
          <td><?php echo '<a href="' . tep_href_link(FILENAME_ACCOUNT_HISTORY_INFO, 'order_id=' . $orders['orders_id'], 'SSL') . '" class="tbutton2">' . SMALL_IMAGE_BUTTON_VIEW . '</a>'; ?></td>
        </tr>
<?php
    }
?>
      </table></div>
<?php
  }
?>
      <div class="cleaner"><h2><?php echo MY_ACCOUNT_TITLE; ?></h2></div>
      <div class="account_personal bmargin">
        <div class="account_arrow"><?php echo '<a href="' . tep_href_link(FILENAME_ACCOUNT_EDIT, '', 'SSL') . '">' . MY_ACCOUNT_INFORMATION . '</a>'; ?></div>
        <div class="account_arrow"><?php echo '<a href="' . tep_href_link(FILENAME_ADDRESS_BOOK, '', 'SSL') . '">' . MY_ACCOUNT_ADDRESS_BOOK . '</a>'; ?></div>
        <div class="account_arrow"><?php echo '<a href="' . tep_href_link(FILENAME_ACCOUNT_PASSWORD, '', 'SSL') . '">' . MY_ACCOUNT_PASSWORD . '</a>'; ?></div>
      </div>
      <div><h2><?php echo MY_ORDERS_TITLE; ?></h2></div>
      <div class="account_orders bmargin">
        <div class="account_arrow"><?php echo '<a href="' . tep_href_link(FILENAME_ACCOUNT_HISTORY, '', 'SSL') . '">' . MY_ORDERS_VIEW . '</a>'; ?></div>
        <div class="account_arrow"><?php echo '<a href="' . tep_href_link(FILENAME_ACCOUNT_COUPONS, '', 'SSL') . '">' . MY_ACCOUNT_COUPONS . '</a>'; ?></div>
      </div>
      <div><h2><?php echo EMAIL_NOTIFICATIONS_TITLE; ?></h2></div>
      <div class="account_notifications bmargin">
        <div class="account_arrow"><?php echo '<a href="' . tep_href_link(FILENAME_ACCOUNT_NEWSLETTERS, '', 'SSL') . '">' . EMAIL_NOTIFICATIONS_NEWSLETTERS . '</a>'; ?></div>
        <div class="account_arrow"><?php echo '<a href="' . tep_href_link(FILENAME_ACCOUNT_NOTIFICATIONS, '', 'SSL') . '">' . EMAIL_NOTIFICATIONS_PRODUCTS . '</a>'; ?></div>
      </div>
    </div>
<?php require('includes/objects/html_end.php'); ?>