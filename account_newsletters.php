<?php
/*
  $Id: account_newsletters.php,v 1.3 2003/06/05 23:23:52 hpdl Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

//----------------------------------------------------------------------------
// Copyright (c) 2007-2010 Asymmetric Software - Innovation & Excellence
// Author: Mark Samios
// http://www.asymmetrics.com
// Catalog: Account History List page
//----------------------------------------------------------------------------
// I-Metrics Layer
//----------------------------------------------------------------------------
// Modifications:
// - 07/05/2007: PHP5 Register Globals and Long Arrays Off support added
// - 07/08/2007: PHP5 Long Arrays Off support added
// - 07/12/2007: Moved HTML Header/Footer to a common section
// - 08/31/2007: HTML Body Common Sections Added
// - 11/17/2009: Converted Tables to CSS
//----------------------------------------------------------------------------
// Released under the GNU General Public License
//----------------------------------------------------------------------------
*/

  require('includes/application_top.php');

  if (!tep_session_is_registered('customer_id')) {
    $navigation->set_snapshot();
    tep_redirect(tep_href_link(FILENAME_LOGIN, '', 'SSL'));
  }
  $action = (isset($_GET['action']) ? $_GET['action'] : '');

// needs to be included earlier to set the success message in the messageStack
  require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_ACCOUNT_NEWSLETTERS);

  $newsletter_query = tep_db_query("select customers_newsletter from " . TABLE_CUSTOMERS . " where customers_id = '" . (int)$customer_id . "'");
  $newsletter = tep_db_fetch_array($newsletter_query);
  $newsletter_general = $newsletter['customers_newsletter'];

  switch($action) {
    case 'process':     
      if( isset($_POST['newsletter_general']) ) {
        $newsletter_general = 1;
      } else {
        $newsletter_general = 0;
      }
      tep_db_query("update " . TABLE_CUSTOMERS . " set customers_newsletter = '" . (int)$newsletter_general . "' where customers_id = '" . (int)$customer_id . "'");
      $messageStack->add_session('account', SUCCESS_NEWSLETTER_UPDATED, 'success');
      tep_redirect(tep_href_link(FILENAME_ACCOUNT, '', 'SSL'));
      break;
    default:
      break;
  }

  $breadcrumb->add(NAVBAR_TITLE_1, tep_href_link(FILENAME_ACCOUNT, '', 'SSL'));
  $breadcrumb->add(NAVBAR_TITLE_2, tep_href_link(FILENAME_ACCOUNT_NEWSLETTERS, '', 'SSL'));
?>
<?php require('includes/objects/html_start_sub1.php'); ?>
<?php require('includes/objects/html_start_sub2.php'); ?>
<?php require('includes/objects/html_body_header.php'); ?>
      <div class="bounder"><?php echo tep_draw_form('account_newsletter', tep_href_link($g_script,  'action=process', 'SSL'), 'post'); ?><fieldset><legend><?php echo TEXT_INFO_FORM; ?></legend>
        <div><?php echo MY_NEWSLETTERS_GENERAL_NEWSLETTER_DESCRIPTION; ?></div>
        <div class="vspacer"><span class="floater rspacer"><?php echo tep_draw_checkbox_field('newsletter_general', 1, '', 'id="primary"'); ?></span><label for="primary"><?php echo MY_NEWSLETTERS_GENERAL_NEWSLETTER; ?></label></div>
        <div class="buttonsRow vpad tspacer">
          <div class="floater lspacer"><?php echo '<a href="#" class="mbutton2 bsubmit">' . IMAGE_BUTTON_UPDATE . '</a>'; ?></div>
          <div class="floatend rspacer"><?php echo '<a href="' . tep_href_link(FILENAME_ACCOUNT, '', 'SSL') . '" class="mbutton3">' . IMAGE_BUTTON_ACCOUNT . '</a>'; ?></div>
        </div>
      </fieldset></form></div>
<?php require('includes/objects/html_end.php'); ?>
